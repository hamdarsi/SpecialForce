'/*'
'******************************************************************************'
'*                                 Bomb.bas                                   *'
'*                                                                            *'
'*  Date: 09 Mordad 1391                                                      *'
'*  Author: Mahdi Hamdarsi                                                    *'
'*  Project: SpecialForce                                                     *'
'*  Comments: This is the core code for the bomb's main controller.           *'
'*            Written by Mahdi Hamdarsi inpired by the previous work of       *'
'*            Hojjat Dorudian.                                                *'
'*                                                                            *'
'******************************************************************************'
'*/'

$regfile "m16def.dat"
$crystal = 14745600
Baud = 4800



'/****************************************************************************/'
'/***************  Interrupt, Timer and Chip Configurations  *****************/'
'/****************************************************************************/'


Ddrc = 255
Ddra = 255
Ddrb.0 = 1
Ddrb.1 = 1
Ddrb.2 = 1
Ddrb.3 = 1
Ddrd.2 = 1                                                  ' baraye interrupt be hmtr'
Ddrd.3 = 0                                                  ' baraye neshan dadane amadegi ertebat az samte hmtr'
Ddrd.6 = 1                                                  ' Buzzer'
Ddrd.7 = 1                                                  ' LED'


' Handles timer decrement'
Config Timer1 = Timer , Prescale = 1024 , Compare A = Disconnect , Clear Timer = 1
On Oc1a UpdateTime
Enable Oc1a
Enable Timer1
Start Timer1
Compare1a = 14400

' Handles twinkler and buzzer updates'
Config Timer0 = Timer , Prescale = 1024
On Ovf0 UpdateBuzzer
Enable Timer0
Start Timer0

' Configure WatchDog to reset every 4 seconds'
Config Watchdog = 2048




'/****************************************************************************/'
'/************************  Open Communication Lines  ************************/'
'/****************************************************************************/'

Open "comd.4:9600,8,n,1,noinverted" For Output As #1
Open "comd.5:9600,8,n,1,noinverted" For Input As #2



'/****************************************************************************/'
'/******************************  Constants  *********************************/'
'/****************************************************************************/'

' Buzzer related constants'
Const BuzzerLengthLong = 100
Const BuzzerLengthMid = 70
Const BuzzerLengthShort = 40
Const BuzzerLengthCritical = 20
Const BuzzerLengthVeryCritical = 10


' Bomb state related constants'
Const WokenUp = 0
Const NotReady = 2
Const Ready = 1
Const Planted = 3
Const Diffused = 4
Const Exploded = 5


' Trembling window: If bomb is being activated, the user shall constantly'
' keep its firing line on the bomb. If firing line is disconnected for'
' more than the threshold below, then activation process is aborted.'
Const TremblingWindow = 30

' Activation window: If bomb is being activated, the user shall keep his'
' gun on the bomb for a specified amount of time. This time is represented'
' by the number below which is the number of bomb timer ticks.'
' 300 ~= 6.5 seconds
Const ActivationWindow = 300



'/****************************************************************************/'
'/******************************  Variables  *********************************/'
'/****************************************************************************/'

' Communication variables'
' =================================='

Dim Command As String * 20
  Command = ""

Dim Position As Byte
  Position = 0

Dim Key As Byte
  Key = 0

Dim Parameter As String * 3
  Parameter = ""

Dim TempByte As Byte
  TempByte = 0

Dim Communicating As Bit
  Communicating = 0


' Game Information'
' =================================='

Dim CurrentState As Byte
  CurrentState = WokenUp

Dim CheaterPlayer As Byte
  CheaterPlayer = 0

Dim TerroristTeam As Byte
  TerroristTeam = 0

Dim DefaultBombTimer As Byte
  DefaultBombTimer = 120

Dim BombTimer As Byte
  BombTimer = DefaultBombTimer

Dim Minute As Byte
  Minute = 0

Dim Second As Byte
  Second = 0

Dim Buz As Byte
  Buz = 0

Dim TwinkleCounter As Byte
  TwinkleCounter = 0

Dim BuzzerLength As Byte
  BuzzerLength = BuzzerLengthLong

Dim Percent As Single
  Percent = 0

Dim InterruptHMTR As Bit
  InterruptHMTR = 0

Dim SystemTime As Integer
  SystemTime = 0

Dim LatestHitTime As Integer
  LatestHitTime = 0

Dim FirstHitTime As Integer
  FirstHitTime = 0

Dim TempTime As Integer
  TempTime = 0

Dim TempPlayer As Byte
  TempPlayer = 0

Dim NumberString As String * 5
  NumberString = ""

Dim PlayerOnBomb As Byte
  PlayerOnBomb = 0

Dim ActivatingPlayer As Byte
  ActivatingPlayer = 0




'/****************************************************************************/'
'/*************************  Sub Routine Declerations  ***********************/'
'/****************************************************************************/'

Declare Sub InputPlayerNumber
Declare Sub BombPlanted
Declare Sub BombDiffused
Declare Sub UpdateLEDs
Declare Sub HMTR




'/****************************************************************************/
'/*****************************  Main Program  *******************************/
'/****************************************************************************/

Call UpdateLEDs
waitms 1300
InterruptHMTR = 1                                           ' Only for the first time to inform of possible reset
Start WatchDog

Do
  Reset WatchDog

  If CurrentState = Ready Then                              ' Check if bomb is planted'
    InputPlayerNumber                                       ' Input planter number from the sensor

    If ActivatingPlayer >= 1 AND ActivatingPlayer <= 10 And TerroristTeam = 0 Then
      Call BombPlanted
    ElseIf ActivatingPlayer >= 11 And ActivatingPlayer <= 20 And TerroristTeam = 1 Then
      Call BombPlanted
    End If

  ElseIf CurrentState = Planted Then                        ' Check if bomb is diffused'
    InputPlayerNumber                                       ' Input diffuser number from the sensor

    If ActivatingPlayer >= 11 And ActivatingPlayer <= 20 And TerroristTeam = 0 Then
      Call BombDiffused
    ElseIf ActivatingPlayer >= 1 AND ActivatingPlayer <= 10 And TerroristTeam = 1 Then
      Call BombDiffused
    End If

  End If

  Call UpdateLEDs
  Call HMTR
Loop

End



'/****************************************************************************/
'/********  If a player has shot the bomb, this will get his number  *********/
'/****************************************************************************/

Sub InputPlayerNumber
  NumberString = ""
  TempPlayer = 0

  If IsCharWaiting() = 1 Then
    Key = Inkey()
    If Key >= 65 AND Key <= 85 Then
      TempPlayer = Key - 65
      TempPlayer = TempPlayer + 1
      LatestHitTime = SystemTime
    End If
  End If

  If TempPlayer <> 0 And PlayerOnBomb = 0 Then
    PlayerOnBomb = TempPlayer
    FirstHitTime = SystemTime


  ElseIf TempPlayer = 0 AND PlayerOnBomb <> 0 Then
    TempTime = SystemTime - LatestHitTime

    If TempTime > TremblingWindow Then
      PlayerOnBomb = 0
      FirstHitTime = 0
    End If

  ElseIf TempPlayer <> 0 AND PlayerOnBomb <> 0 Then
    TempTime = SystemTime - FirstHitTime

    If TempTime > ActivationWindow Then
      ActivatingPlayer = PlayerOnBomb
      PlayerOnBomb = 0
      LatestHitTime = 0
      FirstHitTime = 0
    End If
  End If
End Sub


'/****************************************************************************/
'/*******************  Is Called Whenever Bomb Is Planted  *******************/
'/****************************************************************************/

Sub BombPlanted
  CurrentState = Planted
  BuzzerLength = BuzzerLengthLong
  InterruptHMTR = 1

  ' Restart timer
  Timer1 = 0
  Stop Timer1
  Start Timer1
End Sub




'/****************************************************************************/
'/******************  Is Called Whenever Bomb Is Diffused  *******************/
'/****************************************************************************/

Sub BombDiffused
  CurrentState = Diffused
  InterruptHMTR = 1
End Sub




'/****************************************************************************/
'/**************************  Updates Numeric LEDs  **************************/
'/****************************************************************************/

Sub UpdateLEDs
  If CurrentState = WokenUp Then
    PORTB = 9
    PORTA = 9 + 128
    PORTC = 9
  ElseIf CurrentState = NotReady Or CurrentState = Exploded Then
    PORTB = 15
    PORTA = 15 + 128
    PORTC = 15
  Else
    ' Update minutes
    Minute = BombTimer \ 60
    PORTB = Minute

    ' Update seconds
    Second = BombTimer MOD 60

    ' Second Digit of second'
    TempByte = Second MOD 10
    PORTC = TempByte

    ' First Digit of second'
    TempByte = Second \ 10
    If PORTA.7 = 1 Then
      PORTA = TempByte + 128
    Else
      PORTA = TempByte
    End If
  End If
End Sub




'/****************************************************************************/
'/****************************  Updates Bomb Timer  **************************/
'/****************************************************************************/

UpdateTime:
  If CurrentState = Planted Then
    Tcnt1h = 0
    Tcnt1l = 0

    Decr BombTimer                                          ' Update Timer
    If BombTimer = 0 Then                                   ' Check if bomb has been exploded'
      CurrentState = Exploded                               'Update bomb state
      InterruptHMTR = 1                                     ' Interrupt HMTR
    End If


    If BombTimer <= 5 Then
      BuzzerLength = BuzzerLengthVeryCritical

    ElseIf BombTimer <= 10 Then                             ' Update buzzer rate
      BuzzerLength = BuzzerLengthCritical
    Else
      Percent = 1.0 * BombTimer
      Percent = Percent / DefaultBombTimer

      If Percent > 0.66 Then
        BuzzerLength = BuzzerLengthLong
      ElseIf Percent > 0.33 Then
        BuzzerLength = BuzzerLengthMid
      Else
        BuzzerLength = BuzzerLengthShort
      End If
    End If
  End If
Return




'/****************************************************************************/
'/**********************  Updates Twinkler And Buzzer  ***********************/
'/****************************************************************************/

UpdateBuzzer:
  Incr SystemTime

  ' Update twinkler
  If CurrentState = NotReady Or CurrentState = Exploded Then
    PORTA.7 = 0
    Portd.7 = 0
    TwinkleCounter = 0
  Else
    Incr TwinkleCounter

    If TwinkleCounter < 8 AND CurrentState = Planted Then
      PORTA.7 = 1
      Portd.7 = 1
    ElseIf TwinkleCounter < 50 Then
      PORTA.7 = 0
      PORTD.7 = 0
    Else
      TwinkleCounter = 0
    End If

    If CurrentState <> Planted Then
      PORTA.7 = 1
      PORTD.7 = 1
    End If
  End If

  ' Update buzzer
  If CurrentState = Planted Then
    Incr Buz

    If Buz < 5 Then
      Portd.6 = 1                                           ' buzzer
    ElseIf Buz < BuzzerLength Then
      PORTD.6 = 0
    Else
      Buz = 0
    End If
  Else
    Buz = 0
    PORTD.6 = 0
  End If
Return




'/****************************************************************************/
'/****************************  Interrupt HMTR  ******************************/
'/****************************************************************************/

Sub HMTR
  If Communicating = 0 Then
    Communicating = 1

    If PIND.3 = 1 OR InterruptHMTR = 1 Then
      Disable Interrupts
      waitms 20
      InterruptHMTR = 0                                     ' Reset  Interrupt HMTR flag

      ' Get any passed commands and parse them'
      ' ======================================================
      Portd.2 = 1                                           ' Interrupt HMTR
      PORTD.2 = 0
      Input #2 , Command

      If Left(Command , 5) = "State" Then
        Parameter = Mid(Command , 7)
        CurrentState = Val(Parameter)

        If CurrentState = Ready Then
          ActivatingPlayer = 0
          PlayerOnBomb = 0
          TempPlayer = 0
        End If

      ElseIf Left(Command , 4) = "Conf" Then
        Parameter = Mid(Command , 6 , 1)
        TerroristTeam = Val(Parameter)

        Parameter = Mid(Command , 8)
        DefaultBombTimer = Val(Parameter)
        BombTimer = DefaultBombTimer

        If CurrentState = WokenUp Then
          CurrentState = NotReady
        End If

      ElseIf Left(Command , 4) = "Hack" Then
        Parameter = Mid(Command , 6 , 1)
        CurrentState = Val(Parameter)

        Parameter = Mid(Command , 8 , 2)
        ActivatingPlayer = Val(Parameter)

        If CurrentState = Planted Then
          Call BombPlanted
        ElseIf CurrentState = Diffused Then
          Call BombDiffused
        ElseIf CurrentState = Exploded Then
          BombTimer = 1
        End If

      ElseIf Left(Command , 6) = "ShutUp" Then
        If CurrentState = Planted Then
          BombTimer = DefaultBombTimer
        End If

      ElseIf Left(Command , 2) = "GS" Then
        Parameter = Mid(Command , 4 , 1)
        CurrentState = Val(Parameter)

        Parameter = Mid(Command , 6)
        BombTimer = Val(Parameter)

        If CurrentState = Planted Then
          Call BombPlanted
        End If
      End If

      ' Send current bomb state
      If CheaterPlayer <> 0 Then
        ActivatingPlayer = CheaterPlayer
        CheaterPlayer = 0
      End If

      waitms 50
      Print #1 , CurrentState ; "|" ; ActivatingPlayer ; Chr(13);
      ActivatingPlayer = 0

      ' Wait for wireless micro to send the new data if necessary'
      waitms 200
      Enable Interrupts
    End If

    Communicating = 0
  End If
End Sub