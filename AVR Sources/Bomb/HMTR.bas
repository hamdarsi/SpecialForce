'/*'
'******************************************************************************'
'*                                  HMTR.bas                                  *'
'*                                                                            *'
'*  Date: 09 Mordad 1391                                                      *'
'*  Author: Mahdi Hamdarsi                                                    *'
'*  Project: SpecialForce                                                     *'
'*  Comments: This is the wireless controller's code. This controller manages *'
'*            communication with application and main controller. Written by  *'
'*            Mahdi Hamdarsi, inspired by previous work of Hojjat Dorudian.   *'
'*                                                                            *'
'******************************************************************************'
'*/'

$regfile "m8def.dat"
$crystal = 14745600
Baud = 9600




'/****************************************************************************/'
'/*****************************  Configurations  *****************************/'
'/****************************************************************************/'

Ddrd.7 = 1                                                  ' Ready to communicate.'
Ddrc.0 = 1                                                  ' Stop MP3 Player'
Ddrd.2 = 1                                                  ' Start HMTR'
Portd.2 = 1

' Interrupt handler for Main controller'
Config Watchdog = 1024
Config Int1 = Rising
Enable Int1
On Int1 Mega16DataExchange

' Timer configuration
Config Timer1 = Timer , Prescale = 1024 , Compare A = Disconnect , Clear Timer = 1
On Oc1a TimerTicked
Enable Oc1a
Enable Timer1
Start Timer1
Compare1a = 14400




'/****************************************************************************/'
'/*************************  Open Communication Lines  ***********************/'
'/****************************************************************************/'

Open "comd.6:9600,8,n,1,noinverted" For Input As #1         ' Input from M16
Open "comd.5:9600,8,n,1,noinverted" For Output As #2        ' Output to M16
Open "comc.1:9600,8,n,1,noinverted" For Output As #3        ' MP3 Player


'/****************************************************************************/'
'/******************************  Constants  *********************************/'
'/****************************************************************************/'

Const NumberOfTries = 5



'/****************************************************************************/'
'/******************************  Variables  *********************************/'
'/****************************************************************************/'

' Number of the bomb. Change this for each bomb.'
' Only B1 and B2 are supported.'
' ===================================================='
Dim BombName As String * 3
  BombName = "B1|"


' Various flags in communication with main controller'
' ===================================================='

Dim ConfigFlag As Bit
  ConfigFlag = 0

Dim StateFlag As Bit
  StateFlag = 0

Dim HackFlag As Bit
  HackFlag = 0

Dim InformFlag As Bit
  InformFlag = 0

Dim GameStateFlag As Bit
  GameStateFlag = 0

Dim ShutUpFlag As Bit
  ShutUpFlag = 0

Dim SoundFile As Byte
  SoundFile = 0

Dim LastCommandIsForAll As Bit
  LastCommandIsForAll = 0

Dim UpdateMainMicro As Bit
  UpdateMainMicro = 0

Dim SendResetCounter As Byte
  SendResetCounter = 0

Dim SendStateCounter As Byte
  SendStateCounter = 0


' Variables to deal with messages and commands'
' ===================================================='

Dim Command As String * 100
  Command = ""

Dim CommandLength As Byte
  CommandLength = 0

Dim Message As String * 100
  Message = ""

Dim Parameters As String * 3
  Parameters = ""

Dim Key As String * 1
  Key = ""

Dim CurrentStateString As String * 50
  CurrentStateString = ""

Dim NewStateString As String * 50
  NewStateString = ""

Dim StateStringToSend As String * 50
  StateStringToSend = ""



'/****************************************************************************/'
'/****************************  Initialization  ******************************/'
'/****************************************************************************/'

waitms 100
Enable Interrupts
Start Timer1
Start WatchDog




'/****************************************************************************/'
'/*******************************  Main Module  ******************************/'
'/****************************************************************************/'

Do
  Reset WatchDog
  Disable INT1
  Command = ""
  CommandLength = 0
  If IsCharWaiting() = 1 Then
    Do
      Key = Inkey()
      If Key <> Chr(13) AND key <> Chr(10) Then
        Command = Command + Key
        CommandLength = Len(Command)
      End If
    Loop Until Key = Chr(13) OR key = Chr(10) OR CommandLength > 50

    If CommandLength > 50 Then
      Command = ""
      While IsCharWaiting() = 1
        Key = Inkey()
      Wend
    End If
  End If
  Reset WatchDog


  ' Parse the input command,
  If Left(Command , 3) = BombName OR Left(Command , 3) = "All" Then
    If Left(Command , 3) = "All" Then
      LastCommandIsForAll = 1
    End If

    Command = Mid(Command , 4)                              ' Parse the command'
    UpdateMainMicro = 1

    If Left(Command , 4) = "Ping" Then
      Print BombName ; "Pong" ; Chr(13);
      UpdateMainMicro = 0
      waitms 20

    ElseIf Left(Command , 5) = "BConf" Then                 ' Default bomb timer'
      Parameters = Mid(Command , 7)
      SendResetCounter = 0
      ConfigFlag = 1

    ElseIf Left(Command , 5) = "State" Then
      Parameters = Mid(Command , 7)
      StateFlag = 1                                         ' Init the system'

    ElseIf Left(Command , 6) = "Update" Then
      InformFlag = 1

    ElseIf Left(Command , 6) = "ShutUp" Then
      SendStateCounter = 0
      ShutUpFlag = 1

    ElseIf Left(Command , 4) = "Hack" Then
      Parameters = Mid(Command , 6)
      HackFlag = 1

    ElseIf Left(Command , 9) = "GameState" Then
      Parameters = Mid(Command , 11)
      GameStateFlag = 1

    ElseIf Left(Command , 4) = "Play" Then
      Print BombName ; "PlayDone" ; Chr(13);                ' Roger.'
      Parameters = Mid(Command , 6)                         ' Extract sound number'
      SoundFile = Val(Parameters)
      Portd.0 = 1
      Waitms 10
      Printbin #3 , SoundFile                               ' Play it'
      waitms 100
    End If

  End If

  ' Resume interrupts
  Enable INT1
  ' Signal interrupt request if needed'
  If UpdateMainMicro = 1 Then
    Portd.7 = 1                                             ' Send interrupt request to M16'
    UpdateMainMicro = 0
  End If
Loop

End                                                         'End of program'




'/****************************************************************************/'
'/*******************************  Timer Tick  *******************************/'
'/****************************************************************************/'

TimerTicked:
  If SendResetCounter > 0 Then
    Decr SendResetCounter
    Print BombName ; "ReqGameState" ; Chr(13);
    waitms 50
  End If

  If SendStateCounter > 0 Then
    Decr SendStateCounter
    Print BombName ; "Updated|" ; StateStringToSend ; Chr(13);       ' send current bomb state
  End If
Return




'/****************************************************************************/'
'/**************************  Mega16 Data Exchange  **************************/'
'/****************************************************************************/'

Mega16DataExchange:
  PORTD.7 = 0
  Message = ""

  ' Now pass on control panel commands'
  If ConfigFlag = 1 Then
    Print #2 , "Conf|" ; Parameters ; Chr(13);
    Message = "BConfDone"
    ConfigFlag = 0

  ElseIf StateFlag = 1 Then
    Print #2 , "State|" ; Parameters ; Chr(13);
    Message = "StateSet"
    StateFlag = 0

  ElseIf GameStateFlag = 1 Then
    Print #2 , "GS|" ; Parameters ; Chr(13);
    Message = "GameStateDone"
    GameStateFlag = 0

  ElseIf HackFlag = 1 Then
    Print #2 , "Hack|" ; Parameters ; Chr(13);
    Message = "HackDone"
    HackFlag = 0

  ElseIf ShutUpFlag = 1 Then
    Print #2 , "ShutUp" ; Chr(13);
    ShutUpFlag = 0

  Else
    Print #2 , "Hehe!" ; Chr(13);
  End If

  ' Update procedure'
  waitms 10                                                 ' To let the command actually reach the main controller '
  Input #1 , NewStateString                                 ' Get current state string from main'

  ' Check if state has changed'
  If Left(NewStateString , 1) <> Left(CurrentStateString , 1) Then
    If Left(NewStateString , 1) = "0" Then
      SendResetCounter = NumberOfTries
    Else
      SendStateCounter = NumberOfTries
      StateStringToSend = NewStateString
    End If
  End If

  ' Send reply if the command was intended for the bomb alone.'
  CurrentStateString = NewStateString
  If Message <> "" AND LastCommandIsForAll = 0 Then
    Print BombName ; Message ; Chr(13);
    waitms 50
  End If

  If InformFlag = 1 AND LastCommandIsForAll = 0 Then
    Print BombName ; "Updated|" ; NewStateString ; Chr(13); ' send current bomb state
    waitms 100                                              ' now wait for the new message to be also sent
    Print Chr(13);
    InformFlag = 0
  End If

  If LastCommandIsForAll = 1 Then
    LastCommandIsForAll = 0
  End If
Return