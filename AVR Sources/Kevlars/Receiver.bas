'/*'
'******************************************************************************'
'*                               Receiver.bas                                 *'
'*                                                                            *'
'*  Date: 09 Mordad 1391                                                      *'
'*  Author: Mahdi Hamdarsi                                                    *'
'*  Project: SpecialForce                                                     *'
'*  Comments: This is the laser receiver controller code. Receives shooter    *'
'*            kevlar numbers from sensors and sends them to main controller.  *'
'*            Written by Mahdi Hamdarsi, inspired by the previous work of     *'
'*            Hojjat Dorudian.                                                *'
'*                                                                            *'
'******************************************************************************'
'*/'

$regfile "m8def.dat"
$crystal = 14745600
Baud = 4800




'/****************************************************************************/'
'/**************************  Configure Timer  *******************************/'
'/****************************************************************************/'

' Updates time in regular intervals ~ 2-3 msec'
Config Timer1 = Timer , Prescale = 1
On Timer1 UpdateTime:
Enable Timer1
Start Timer1
Enable Interrupts




'/****************************************************************************/'
'/*************************  Declare Used Procedures  ************************/'
'/****************************************************************************/'

Declare Sub InputPlayerNumber




'/****************************************************************************/'
'/*****************************  Threshold Values  ***************************/'
'/****************************************************************************/'

Const ActivationWindow = 3                                  ' Required window for the received laser number to be accepted'
Const ShooterSwitchWindow = 50                              ' Required window for the received laser number to change'




'/****************************************************************************/'
'/*******************************  Variables  ********************************/'
'/****************************************************************************/'

Dim Key As Byte
  Key = 0

Dim TempPlayer As Byte
  TempPlayer = 0

Dim ShooterNumber As Byte
  ShooterNumber = 0

Dim CurrentShooterNumber As Byte
  CurrentShooterNumber = 0

Dim Signature As Byte
  Signature = 0

Dim LatestHitTime As Long
  LatestHitTime = 0

Dim SystemTime As Long
  SystemTime = 0

Dim FirstHitTime As Long
  FirstHitTime = 0

Dim TempTime As Long
  TempTime = 0




'/****************************************************************************/'
'/*****************************  Intialization  ******************************/'
'/****************************************************************************/'

Ddrb.0 = 1
Portb.0 = 0

Do
  ' Check for shooter number'
  Call InputPlayerNumber

  If ShooterNumber >= 1 And ShooterNumber <= 20 Then
    Portb.0 = 1
    Portb.0 = 0

    Open "comb.0:300,8,n,1,noinverted" For Output As #1     ' Open communication to main controller'
    waitms 30

    Signature = ShooterNumber * 5
    PrintBin #1 , ShooterNumber
    PrintBin #1 , Signature
    ShooterNumber = 0

    Portb.0 = 0
    Close #1                                                ' Close communication'
  End If

Loop

End



'/****************************************************************************/'
'/*************************  Inputs Shooter Number  **************************/'
'/****************************************************************************/'

Sub InputPlayerNumber
  TempPlayer = 0

  ' Capture the shooter'
  If IsCharWaiting() = 1 Then
    Key = Inkey()
    If Key >= 65 AND Key <= 85 Then
      TempPlayer = Key - 64
      LatestHitTime = SystemTime
    End If
  End If

  If TempPlayer <> 0 Then
    If CurrentShooterNumber = 0 Then
      FirstHitTime = SystemTime                             ' First time being shot'
      CurrentShooterNumber = TempPlayer

    ElseIf TempPlayer <> CurrentShooterNumber Then
      FirstHitTime = 0
      CurrentShooterNumber = 0                              ' Reset shooting process if someone else shot him too'

    Else
      TempTime = SystemTime - FirstHitTime

      If TempTime = ActivationWindow Then
        ShooterNumber = CurrentShooterNumber                ' Send the shooter number to main controller'
      End if
    End If

  Else                                                      ' TempPlayer = 0'
    TempTime = SystemTime - LatestHitTime

    If TempTime > ShooterSwitchWindow Then                  ' Reset shooting process if not being shot in a small while'
      CurrentShooterNumber = 0
      FirstHitTime = 0
    End If
  End If
End Sub




'/****************************************************************************/'
'/*******************  Updates Time in a Regular Interval  *******************/'
'/****************************************************************************/'

UpdateTime:
  Incr SystemTime
Return