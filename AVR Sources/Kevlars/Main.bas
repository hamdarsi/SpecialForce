'/*'
'******************************************************************************'
'*                                 Main.bas                                   *'
'*                                                                            *'
'*  Date: 09 Mordad 1391                                                      *'
'*  Author: Mahdi Hamdarsi                                                    *'
'*  Project: SpecialForce                                                     *'
'*  Comments: This is the core of project. Written by Mahdi Hamdarsi inpired  *'
'*            by the previous work of Hojjat Dorudian.                        *'
'*                                                                            *'
'******************************************************************************'
'*/'

$regfile "m16def.dat"
$crystal = 14745600




'/****************************************************************************/'
'/***************  Interrupt, Timer and Chip Configurations  *****************/'
'/****************************************************************************/'

' For round timer and bomb timer. Interrupts once every second'
Config Timer1 = Timer , Compare A = Disconnect , Prescale = 1024
Disable Oc1a
On Oc1a UpdateTime
Compare1a = 14400

' For kevlar twinkler. Twinkles a brief period of time'
Config Timer0 = Timer , Prescale = 1024
Disable Timer0
On Ovf0 KevlarTwinkler
Start Timer0

' Interrupts when a bullet has been conceded'
Config Int0 = Rising
Enable Int0
On Int0 PlayerIsShotAt

' Configure LCD size and options'
Config Lcd = 16 * 2
Config Lcdpin = Pin , Db4 = Porta.2 , Db5 = Porta.3 , Db6 = Porta.4 , Db7 = Porta.5 , E = Porta.1 , Rs = Porta.0
Cursor Off

' Configure WatchDog to reset every 4 seconds'
Config Watchdog = 2048




'/****************************************************************************/'
'/***************************  Configure Hardware  ***************************/'
'/****************************************************************************/'

Ddrd.0 = 1                                                  ' MP3 Player?!'
Ddra.6 = 1                                                  ' Back Light'

Ddra.7 = 1                                                  ' Kevlar LEDs'

Ddrd.4 = 0                                                  ' Mag button'
Ddrd.5 = 0                                                  ' Backlight button'
Ddrd.6 = 0                                                  ' Laser control'

Ddrc.5 = 1                                                  ' Laser?!'
Portc.5 = 0

Ddrc.6 = 1                                                  ' Kevlar vibration'
Ddrc.7 = 1                                                  ' Gun vibration'
Ddrd.7 = 0                                                  ' IR receiver?!'
Portd.7 = 1

Ddrc.2 = 1                                                  ' HP LED #1'
Ddrc.3 = 1                                                  ' HP LED #2'
Ddrc.4 = 1                                                  ' HP LED #3'

Ddrb.3 = 0                                                  ' Wireless controller signal port'
Ddrb.4 = 1                                                  ' Interrupt to wireless controller'

Ddrb.0 = 1                                                  ' Walkie talkie switch?!'

Portd.0 = 1                                                 ' Stop MP3 player?!'



'/****************************************************************************/'
'/******************************  Constants  *********************************/'
'/****************************************************************************/'

Const NumberOfBulletsToFire = 3                             ' With each shot, this number of bullets will be fired'
Const HitInterval = 2                                       ' Interval between accepting shootings from sensor'
Const ChangeMagInterval = 2                                 ' Inteval between accepting magazine change requests from player'

Const WokenUp = 0
Const TurnedOn = 1                                          ' State of being turned on'
Const TurnedOff = 2                                         ' State of being turned off'
Const Dead = 3                                              ' State of being dead'

Const ScreamingWaitTime = 1000                              ' Time to wait when kevlar is playing scream sound'
Const MagChangeWaitTime = 1000                              ' Time to wait when changing magazine'
Const PostFireWaitTime = 200                                ' Time to wait after a shot'
Const StartingFreezeTime = 15                               ' When game is started kevlars can not fire as set by this parameter'
Const MasterResetTimeOut = 0                                ' When turn on command is not received after this amount in seconds from config command, System will reset. '

Const BackLightDuration = 6                                 ' Seconds to keep back light on when its key is pressed'
Const BackLightDurationOnStart = 10                         ' Seconds to keep back light on when game starts'
Const MessageShowDuration = 5                               ' Seconds to keep showing the message to player while turned on'




'/****************************************************************************/'
'/******************************  Variables  *********************************/'
'/****************************************************************************/'

' Master control on the kevlar!'
' =================================='
Dim Debugging As Bit
  Debugging = 0

Dim MasterReset As Bit
  MasterReset = 0

Dim LastConfigTime As Integer
  LastConfigTime = 0

Dim ConfigAuxVar As Integer
  ConfigAuxVar = 0


' Kevlar properties'
' =================================='
Dim CurrentState As Byte
  CurrentState = WokenUp

Dim KevlarNumber As Byte
  KevlarNumber = 0

Dim KevlarCode As Byte
  KevlarCode = 0

Dim TwinkleCounter As Integer
  TwinkleCounter = 0

Dim BackLightTime As Byte
  BackLightTime = BackLightDuration

Dim UpdateWirelessMicro As Bit
  UpdateWirelessMicro = 1

Dim Communicating As Bit
  Communicating = 0


' HP related parameters'
' =================================='
Dim StartHP As Integer
  StartHP = 3

Dim HP As Integer
  HP = StartHP


' Magazine related parameters'
' =================================='
Dim LastChangeMagTime As Long
  LastChangeMagTime = 0

Dim ChangeMagTime As Long
  ChangeMagTime = 0

Dim StartMagazine As Integer
  StartMagazine = 5

Dim Magazine As Integer
  Magazine = StartMagazine


' Ammunition related parameters'
' =================================='

Dim AmmoPerMagazine As Integer
  AmmoPerMagazine = 30

Dim Ammo As Integer
  Ammo = AmmoPerMagazine


' Frag and death count'
' =================================='
Dim Frag As Integer
  Frag = 0

Dim DeathCount As Integer
  DeathCount = 0

Dim BlueTeamScore As Integer
  BlueTeamScore = 0

Dim GreenTeamScore As Integer
  GreenTeamScore = 0


' Game information'
' =================================='
Dim DefaultRoundTime As Integer
  DefaultRoundTime = 480

Dim RoundTime As Integer
  RoundTime = DefaultRoundTime

Dim DeltaTime As Integer
  DeltaTime = 0

Dim DefaultBombTime As Integer
  DefaultBombTime = 90

Dim BombTime As Integer
  BombTime = DefaultBombTime

Dim BombPlanted As Bit
  BombPlanted = 0

Dim DefaultTimeToReturn As Byte
  DefaultTimeToReturn = 0

Dim TimeToReturn As Byte
  TimeToReturn = DefaultTimeToReturn

Dim Minute As Integer
  Minute = 0

Dim Second As Integer
  Second = 0

Dim MessageLine1 As String * 16
  MessageLine1 = ""

Dim MessageLine2 As String * 16
  MessageLine2 = ""

Dim MessageCountDown As Byte
  MessageCountDown = 0


' Information related to being shot'
' =================================='
Dim PlayerShot As Bit
  PlayerShot = 0

Dim FiringNow As Bit
  FiringNow = 0

Dim Signature As Byte
  Signature = 0

Dim NewSignature As Byte
  NewSignature = 0

Dim ShooterNumber As Byte
  ShooterNumber = 0

Dim NewShooterNumber As Byte
  NewShooterNumber = 0

Dim CurrentTime As Long
  CurrentTime = 0

Dim SystemTicks As Long
  SystemTicks = 0

Dim TempTime As Long
  TempTime = 0

Dim FriendlyFire As Bit
  FriendlyFire = 1


' Sound output related information'
' =================================='
Dim SoundToPlay As Byte
  SoundToPlay = 0

Dim IsFiring As Bit
  IsFiring = 0

Dim IsPlayingFireSound As Bit
  IsPlayingFireSound = 0

Dim FireSoundStartTime As Long
  FireSoundStartTime = 0


' Auxilary variables'
' =================================='
Dim I As Byte
  I = 0

Dim ByteParameter As Byte
  ByteParameter = 0

Dim StringParameter As String * 10
  StringParameter = ""

Dim WirelessCommand As String * 100
  WirelessCommand = ""

Dim NumberToWrite As Integer
  NumberToWrite = 0

Dim TempVar As Integer
  TempVar = 0

Dim TempLCDVar As String * 8
  TempLCDVar = ""

Dim LCDPart1 As String * 8
Dim LCDPart2 As String * 8
Dim LCDPart3 As String * 8
Dim LCDPart4 As String * 8

Dim LCDLine1 As String * 16
  LCDLine1 = ""

Dim LCDLine2 As String * 16
  LCDLine2 = ""




'/****************************************************************************/'
'/***********************  Declerations Of Procedures  ***********************/'
'/****************************************************************************/'

Declare Sub UpdateLCD
Declare Sub UpdateHPLEDs
Declare Sub PlaySound
Declare Sub StopSound
Declare Sub UpdateFiringSoundState
Declare Sub DisablePlayer
Declare Sub ChatWithWireless




'/****************************************************************************/'
'/*************************  Initialization Code  ****************************/'
'/****************************************************************************/'

Call DisablePlayer                                          ' Completely stop the player'
CLS                                                         ' Clear garbled characters on LCD'
Enable Interrupts                                           ' Start all interrupts'
Enable Oc1a                                                 ' Start kevlar indicator timer'
Enable Timer0                                               ' Start second resolution timer'
waitms 500                                                  ' Wait till wireless gains conciousness'
Start Watchdog                                              ' Start the WatchDog'




'/****************************************************************************/'
'/**************************  Communication Lines  ***************************/'
'/****************************************************************************/'

Open "comc.5:4800,8,n,1,inverted" For Output As #2          ' Line to laser output to write kevlar code when firing'
Open "comc.1:9600,8,n,1,noinverted" For Input As #3         ' Line to receive from wireless controller'
Open "comc.0:9600,8,n,1,noinverted" For Output As #5        ' Line to send to wireless controller'
Open "coma.0:9600,8,n,1,noinverted" For Output As #6        ' Line to send to LCD controller'




'/****************************************************************************/'
'/*******************************  Main Loop  ********************************/'
'/****************************************************************************/'

' Start the main loop'
Do
  ' Update LCD in the main loop'
  If MasterReset = 0 Then
    Reset WatchDog
  End If

  Call UpdateHPLEDs
  Call UpdateLCD

  ' To Check if wireless controller wants to talk with main controller'
  If CurrentState <> TurnedOn Then
    Call ChatWithWireless
  EndIf



  ' Inner main loop'
  '======================================================='
  While CurrentState = TurnedOn
    Enable Interrupts

    ' Shooting Process'
    '======================================================='
    Call UpdateFiringSoundState
    If IsFiring = 1 Then
      DeltaTime = DefaultRoundTime - RoundTime

      If DeltaTime > StartingFreezeTime Then
        If Ammo > 0 Then
          If Ammo < NumberOfBulletsToFire Then              ' Decrease ammo'
            Ammo = 0
          Else
            Ammo = Ammo - NumberOfBulletsToFire
          End If

          Portc.5 = 0                                       ' Turn off the Laser'

          For I = 1 To 50                                   ' Fire'
            FiringNow = 1
            PrintBin #2 , KevlarCode
            Waitms 3
            FiringNow = 0
          Next I

          PORTC.5 = 1                                       ' Turn On The Laser'
          waitms PostFireWaitTime                           ' Wait if hardware is too fast!'
          If MasterReset = 0 Then
            Reset WatchDog
          End If
        End If
      End If
    End If




    ' Mag change process'
    '======================================================='
    If PIND.4 = 1 Then
      ChangeMagTime = CurrentTime - ChangeMagInterval
      If ChangeMagTime >= LastChangeMagTime And Ammo <> AmmoPerMagazine Then
        LastChangeMagTime = CurrentTime

        If Magazine > 0 Then
          ' Update magazine'
          Decr Magazine
          Ammo = AmmoPerMagazine

          ' Play the sound for changing magazine'
          SoundToPlay = 1
        Else
          SoundToPlay = 9
        End If

        ' Now start playing the sound'
        IsPlayingFireSound = 0
        Call PlaySound

        ' Wait for the sound to finish playing'
        Stop WatchDog
        waitms MagChangeWaitTime
        Start WatchDog
        If MasterReset = 0 Then
          Reset WatchDog
        End If
      End If
    End If




    ' Update laser pointer status'
    '======================================================='
    If CurrentState = TurnedOn Then
      If Pind.7 = 0 Then
        Portc.5 = 1                                         ' Turn on'
      Else
        Portc.5 = 0                                         ' Turn off'
      End If
    End If




    ' Update health status'
    '======================================================='
    If PlayerShot = 1 Then
      Portc.6 = 1                                           ' Kevlar Vibrator'
      PORTC.7 = 1                                           ' Gun vibrator'

      Decr HP                                               ' Decrease HP'
      Call UpdateHPLEDs                                     ' Update HP LEDs right away'

      ' Play a sound to inform of being shot'
      IsPlayingFireSound = 0
      If ShooterNumber > 10 AND KevlarNumber > 10 Then
        SoundToPlay = 3
      ElseIf ShooterNumber <= 10 AND KevlarNumber <= 10 Then
        SoundToPlay = 3
      Else
        SoundToPlay = 4
      End If
      Call PlaySound

      ' Wait for the sound to finish playing'
      Stop watchdog
      waitms ScreamingWaitTime
      Start watchdog
      If MasterReset = 0 Then
        Reset WatchDog
      End If

      PORTC.6 = 0                                           ' Turn Kevlar vibrator off'
      PORTC.7 = 0                                           ' Turn gun vibrator off'

      If HP = 0 Then
        CurrentState = Dead                                 ' Then he is fucking dead.'
        Call DisablePlayer
      End If

      PlayerShot = 0                                        ' Player shooting process is now finished'
      UpdateWirelessMicro = 1                               ' Send HP updates to wireless controller'
    End If




    ' Update LEDs and LCD'
    '======================================================='
    Call UpdateHPLEDs
    Call UpdateLCD



    ' Now its time to hear what wireless controller has to say'
    '========================================================='
    Call ChatWithWireless
    If MasterReset = 0 Then
      Reset WatchDog
    End If

  WEND                                                      ' While CurrentState = TurnedOn'

Loop                                                        ' Do Loop'
End                                                         ' Program ends here'





'/****************************************************************************/'
'/******************  Interrupts When A Bullet Is Conceded  ******************/'
'/****************************************************************************/'

PlayerIsShotAt:
  If CurrentState = TurnedOn AND PlayerShot = 0 Then
    Open "comd.2:300,8,n,1,noinverted" For Input As #4      ' Open communication line to read from receiver controller'
    InputBin #4 , NewShooterNumber                          ' Read shooter number'
    InputBin #4 , NewSignature                              ' Read the shot's signature'
    Close #4                                                ' Close communication line'

    Signature = NewSignature \ 5                            ' Signature is shooter number x 5'
    If FiringNow = 0 AND Signature = NewShooterNumber AND Signature <> 0 Then       ' Only accept shooter if its legitimate'
      ' OK. Player is shot. Is friendly fire?'
      If FriendlyFire = 0 Then                              ' Friendly fire is on. Accept the shot anyway'
        PlayerShot = 1
      Else
        If KevlarNumber <= 10 AND NewShooterNumber > 10 Then
          PlayerShot = 1                                    ' Player is for blue team and shooter is for green team'
        ElseIf KevlarNumber > 10 AND NewShooterNumber <= 10 Then
          PlayerShot = 1                                    ' Player is for green team and shooter is for blue team'
        End If
      End If

      If PlayerShot = 1 Then
        ShooterNumber = NewShooterNumber                    ' Update the shooter number'

        If Debugging = 1 Then                               ' Debugging purpose'
          PlayerShot = 0
          LCD ShooterNumber
        End If
      End If
    End If
  End If

  GIFR.INTF0 = 1
Return




'/****************************************************************************/'
'/*************************  Kevlar Indicator Twinkle  ***********************/'
'/****************************************************************************/'

KevlarTwinkler:
  Incr SystemTicks
  Incr TwinkleCounter

  ' Kevlar Twinkler'
  DeltaTime = DefaultRoundTime - RoundTime
  If DeltaTime > StartingFreezeTime And TwinkleCounter < 8 And CurrentState = TurnedOn Then
    Porta.7 = 1                                             ' Turn on the twinkler'
  Else
    If TwinkleCounter = 50 Then
      TwinkleCounter = 0                                    ' Reset counter'
    Else
      PORTA.7 = 0                                           ' Turn off the twinkler'
    End If
  End If

  ' LCD back light time'
  If PIND.5 = 1 Or CurrentState <> TurnedOn Then
    BackLightTime = BackLightDuration
  End If

  ' Turn on back light'
  If Debugging = 1 Then
    PORTA.6 = 1
  ElseIf BackLightTime > 0 AND PINA.6 = 0 Then
    PORTA.6 = 1
  End If
Return




'/****************************************************************************/'
'/*******************  Update Current Round And Bomb Time  *******************/'
'/****************************************************************************/'

UpdateTime:
  Tcnt1h = 0
  Tcnt1l = 0

  ' Checks to see if master reset is needed'
  ' Master reset is currently disabled by setting Timeout to 0'
  ConfigAuxVar = MasterResetTimeOut
  If ConfigAuxVar <> 0 And LastConfigTime <> 0 Then
    ConfigAuxVar = SystemTicks - LastConfigTime

    If ConfigAuxVar > MasterResetTimeOut OR LastConfigTime < 0 Then
      MasterReset = 1
    End If
  End If

  If CurrentState = Dead Then
    If TimeToReturn > 0 Then
      Decr TimeToReturn
    End If

  ElseIf CurrentState = TurnedOn Then
    Incr CurrentTime

    ' Bomb timer'
    If BombPlanted = 1 Then
      Decr BombTime

      If BombTime <= 0 Then
        CurrentState = TurnedOff                            ' Turn off kevlar when bomb is exploded'
        Call DisablePlayer
      End If

    Else
      Decr RoundTime

      ' Round timer'
      If RoundTime <= 0 Then
        CurrentState = TurnedOff                            ' Turn off kevar when round is finished'
        Call DisablePlayer
      End If
    End If

    ' BackLight time control'
    If BackLightTime > 0 Then
      Decr BackLightTime

      If BackLightTime = 0 AND Debugging = 0 Then
        PORTA.6 = 0                                         ' Turn Off The BackLight'
      End If
    End If

    If MessageCountDown > 0 Then
      Decr MessageCountDown
    End If
  End If
Return





'/****************************************************************************/'
'/**************************  Disables The Player  ***************************/'
'/****************************************************************************/'

Sub DisablePlayer
  PORTB.0 = 0                                               ' Turn off walkie talkie'
  Portc.5 = 0                                               ' Turn off gun laser'
  IsPlayingFireSound = 0                                    ' Stop all sounds that are being played'
  Call StopSound
End Sub





'/****************************************************************************/'
'/************************  Updates LCD OSD Information  *********************/'
'/****************************************************************************/'

Sub UpdateLCD
  ' Show sent message until expired'
  If MessageCountDown > 0 Then
    LCDLine1 = MessageLine1
    LCDLine2 = MessageLine2

  ElseIf CurrentState = Dead Then
    Locate 1 , 1
    LCDLine1 = "You died by     "
    If ShooterNumber <= 10 Then
      Mid(LCDLine1 , 12 , 3) = "B#0"
      ByteParameter = ShooterNumber
    Else
      Mid(LCDLine1 , 12 , 3) = "G#0"
      ByteParameter = ShooterNumber - 10
    End If

    Mid(LCDLine1 , 16 , 1) = Str(ByteParameter)

    Locate 2 , 1
    If TimeToReturn > 0 Then
      LCDLine2 = "Hurry to base:  "
      TempLCDVar = Str(TimeToReturn)
      TempLCDVar = Format(TempLCDVar , "00")
      Mid(LCDLine2 , 15 , 2) = TempLCDVar

    Else

      LCDLine2 = "Round in play..."
    End If

  ' Show regular information'
  ElseIf CurrentState = TurnedOn And Debugging = 0 Then
    ' Clean up the LCD'
    LCDLine1 = "                "
    LCDLine2 = "                "


    ' Frag'
    If Frag >= 0 Then
      Mid(LCDLine1 , 1 , 1) = "F"
      TempLCDVar = Str(Frag)
      TempLCDVar = Format(TempLCDVar , "00")
      Mid(LCDLine1 , 2 , 2) = TempLCDVar
    Else
      TempLCDVar = Str(Frag)
      TempLCDVar = Format(TempLCDVar , "00")
      Mid(LCDLine1 , 1 , 3) = TempLCDVar
    End If

    ' Death count'
    Mid(LCDLine2 , 1 , 1) = "D"
    TempLCDVar = Str(DeathCount)
    TempLCDVar = Format(TempLCDVar , "00")
    Mid(LCDLine2 , 2 , 2) = TempLCDVar

    ' Blue team score'
    Mid(LCDLine1 , 14 , 1) = "B"
    TempLCDVar = Str(BlueTeamScore)
    TempLCDVar = Format(TempLCDVar , "00")
    Mid(LCDLine1 , 15 , 2) = TempLCDVar

    ' Green team score'
    Mid(LCDLine2 , 14 , 1) = "G"
    TempLCDVar = Str(GreenTeamScore)
    TempLCDVar = Format(TempLCDVar , "00")
    Mid(LCDLine2 , 15 , 2) = TempLCDVar

    ' Time'
    ' Frame around the time and twinkling ":" sign in the middle
    If BombPlanted = 1 Then
      Minute = BombTime \ 60
      Second = BombTime MOD 60

      Mid(LCDLine1 , 5 , 1) = "*"
      Mid(LCDLine1 , 11 , 1) = "*"
    Else
      Minute = RoundTime \ 60
      Second = RoundTime MOD 60

      Mid(LCDLine1 , 5 , 1) = " "
      Mid(LCDLine1 , 11 , 1) = " "
    End If

    ' Time twinkler update'
    ByteParameter = SystemTicks MOD 60
    If ByteParameter >= 20 Then
      Mid(LCDLine1 , 8 , 1) = ":"
    Else
      Mid(LCDLine1 , 8 , 1) = " "
    End If

    ' Time's minute part'
    TempLCDVar = Str(Minute)
    TempLCDVar = Format(TempLCDVar , "00")
    Mid(LCDLine1 , 6 , 2) = TempLCDVar

    ' Time's second part'
    TempLCDVar = Str(Second)
    TempLCDVar = Format(TempLCDVar , "00")
    Mid(LCDLine1 , 9 , 2) = TempLCDVar

    ' HP '
    TempLCDVar = Str(HP)
    TempLCDVar = Format(TempLCDVar , "0")
    Mid(LCDLine2 , 5 , 1) = TempLCDVar
    Mid(LCDLine2 , 6 , 1) = CHR(255)

    ' Ammo '
    TempLCDVar = Str(Ammo)
    TempLCDVar = Format(TempLCDVar , "000")
    Mid(LCDLine2 , 7 , 3) = TempLCDVar
    Mid(LCDLine2 , 10 , 1) = CHR(255)

    ' Magazine'
    TempLCDVar = Str(Magazine)
    TempLCDVar = Format(TempLCDVar , "0")
    Mid(LCDLine2 , 11 , 1) = TempLCDVar

  ElseIf CurrentState = TurnedOff And Debugging = 0 Then
    LCDLine1 = "Please wait for "
    LCDLine2 = "game to start.  "

  ElseIf CurrentState = WokenUp And Debugging = 0 Then
    LCDLine1 = "Retrieving game "
    LCDLine2 = "information...  "
  End If

  For I = 1 To 16
    If Mid(LCDLine1 , I , 1) = " " Then
      Mid(LCDLine1 , I , 1) = "`"
    End If
    If Mid(LCDLine2 , I , 1) = " " Then
      Mid(LCDLine2 , I , 1) = "`"
    End If
  Next

  LCDPart1 = Mid(LCDLine1 , 1 , 8)
  LCDPart2 = Mid(LCDLine1 , 9 , 8)
  LCDPart3 = Mid(LCDLine2 , 1 , 8)
  LCDPart4 = Mid(LCDLine2 , 9 , 8)

  print #6 , "KIR" ; Chr(13);
  waitms 20
  print #6 , LCDPart1 ; Chr(13);
  waitms 20
  print #6 , LCDPart2 ; Chr(13);
  waitms 20
  print #6 , LCDPart3 ; Chr(13);
  waitms 20
  print #6 , LCDPart4 ; Chr(13);
  waitms 20
  print #6 , BackLightTime ; Chr(13);
  waitms 20
End Sub




'/****************************************************************************/'
'/*******************  Update HP LEDs Based On Current HP  *******************/'
'/****************************************************************************/'

Sub UpdateHPLEDs
  DeltaTime = DefaultRoundTime - RoundTime
  If CurrentState = TurnedOn AND DeltaTime > StartingFreezeTime Then
    ' PORTC.2 : First LED from left'
    ' PORTC.3 : Middle LED'
    ' PORTC.4 : Last LED from left'
    If HP = 0 Then
      Portc.2 = 0
      Portc.3 = 0
      Portc.4 = 0
    ElseIf HP = 1 Then
      Portc.2 = 0
      Portc.3 = 0
      Portc.4 = 1
    ElseIf HP = 2 Then
      Portc.2 = 0
      Portc.3 = 1
      Portc.4 = 1
    Else
      Portc.2 = 1
      Portc.3 = 1
      Portc.4 = 1
    End If
  Else
    PORTC.2 = 0
    PORTC.3 = 0
    PORTC.4 = 0
  End If
End Sub




'/****************************************************************************/'
'/*************************  Plays The Given Sound  **************************/'
'/****************************************************************************/'

Sub PlaySound
  Open "comd.1:9600,8,n,1,noinverted" For Output As #1      ' Open serial line for playback'

  Call StopSound                                            ' First stop mp3 player'
  Portd.0 = 1

  Printbin #1 , SoundToPlay                                 ' Play the given sound'
  Close #1
End Sub




'/****************************************************************************/'
'/**************************  Stops Playing Sound  ***************************/'
'/****************************************************************************/'

Sub StopSound
  Portd.0 = 1
  Waitms 50
  Portd.0 = 0
  Waitms 50
End Sub




'/****************************************************************************/'
'/**********************  Updates Shooting Sound State  **********************/'
'/****************************************************************************/'

Sub UpdateFiringSoundState
  ' Updates fire status. This is also used in shooting process'
  DeltaTime = DefaultRoundTime - RoundTime
  If Pind.7 = 0 And Pind.6 = 1 Then
    IsFiring = 1
  Else
    IsFiring = 0
  End If

  If IsFiring = 1 And DeltaTime > StartingFreezeTime Then
    If Ammo = 0 Then
      ' Play theempty ammo sound'
      IsPlayingFireSound = 0
      SoundToPlay = 9
      Call PlaySound

      ' Wait till the sounds is played'
      waitms 1000
    Else
      If IsPlayingFireSound = 0 Then
        IsPlayingFireSound = 1
        FireSoundStartTime = CurrentTime
        SoundToPlay = 7
        Call PlaySound
      End If
    End If
  Else
    If IsPlayingFireSound = 1 Then
      IsPlayingFireSound = 0
      Call StopSound
    End If
  End If

  ' Re-Play the shooting stream if its finished'
  If IsPlayingFireSound = 1 Then
    TempTime = CurrentTime - FireSoundStartTime
    If TempTime > 10 Then
      FireSoundStartTime = CurrentTime
      SoundToPlay = 7
      Call PlaySound
    End If
  End If
End Sub




'/****************************************************************************/'
'/*****************  Communication with wireless controller  *****************/'
'/****************************************************************************/'
Sub ChatWithWireless
  If Communicating = 0 Then
    Communicating = 1

    If PINB.3 = 1 OR UpdateWirelessMicro = 1 Then
      Disable Interrupts                                    ' Don't suspend comm if receiver has interrupted'
      waitms 20
      UpdateWirelessMicro = 0                               ' Now that control in interrupt request, just set it to no request'

      Portb.4 = 1                                           ' Send interrupt to HMTR'
      Portb.4 = 0                                           ' Reset interrupt'
      Input #3 , WirelessCommand                            ' Get command from HMTR'

      If Left(WirelessCommand , 4) = "Conf" Then
        'Config|17|03|05|100|04|02|480|090|1|02|04|20'
        'Number|HP|Mag|Ammo|Frag|Death|Round|Bomb|FriendlyFire|BlueTeamScore|GreenTeamScore|ReturnTime'
        LastConfigTime = SystemTicks

        StringParameter = Mid(WirelessCommand , 6 , 2)
        KevlarNumber = Val(StringParameter)
        KevlarCode = KevlarNumber + 64

        StringParameter = Mid(WirelessCommand , 9 , 2)
        StartHP = Val(StringParameter)
        HP = StartHP

        StringParameter = Mid(WirelessCommand , 12 , 2)
        StartMagazine = Val(StringParameter)
        Magazine = StartMagazine

        StringParameter = Mid(WirelessCommand , 15 , 3)
        AmmoPerMagazine = Val(StringParameter)
        Ammo = AmmoPerMagazine

        StringParameter = Mid(WirelessCommand , 19 , 2)
        Frag = Val(StringParameter)

        StringParameter = Mid(WirelessCommand , 22 , 2)
        DeathCount = Val(StringParameter)

        StringParameter = Mid(WirelessCommand , 25 , 3)
        DefaultRoundTime = Val(StringParameter)
        RoundTime = DefaultRoundTime

        StringParameter = Mid(WirelessCommand , 29 , 3)
        DefaultBombTime = Val(StringParameter)
        BombTime = DefaultBombTime

        StringParameter = Mid(WirelessCommand , 33 , 1)
        If StringParameter = "1" Then
          FriendlyFire = 1
        Else
          FriendlyFire = 0
        End If

        StringParameter = Mid(WirelessCommand , 35 , 2)
        BlueTeamScore = Val(StringParameter)

        StringParameter = Mid(WirelessCommand , 38 , 2)
        GreenTeamScore = Val(StringParameter)

        StringParameter = Mid(WirelessCommand , 41 , 2)
        DefaultTimeToReturn = Val(StringParameter)

        If CurrentState = WokenUp Then
          CurrentState = TurnedOff
        End If

      ElseIf Left(WirelessCommand , 4) = "Prms" Then
        StringParameter = Mid(WirelessCommand , 6)
        DefaultBombTime = Val(StringParameter)
        BombTime = DefaultBombTime

      ElseIf Left(WirelessCommand , 5) = "State" Then       ' Change in state'
        StringParameter = Mid(WirelessCommand , 7)
        CurrentState = Val(StringParameter)

        If CurrentState = TurnedOn Then
          ' Game properties'
          LastConfigTime = 0                                ' Reset last config time to abort master reset process'
          CurrentTime = 0                                   ' Initialize current time'
          HP = StartHP                                      ' Initialize HP'
          Magazine = StartMagazine                          ' Initialize Magazine'
          Ammo = AmmoPerMagazine                            ' Initialize Ammo'
          RoundTime = DefaultRoundTime                      ' Initialize Round Time'
          BombTime = DefaultBombTime                        ' Initialize BombTime'
          BombPlanted = 0                                   ' Ready the bomb'
          TimeToReturn = DefaultTimeToReturn                ' Time to return to base'

          ' Kevlar internal controls'
          LastChangeMagTime = 0                             ' Reset last time magazine changed'
          SoundToPlay = 0
          IsFiring = 0
          IsPlayingFireSound = 0
          FireSoundStartTime = 0

          Portb.0 = 1                                       ' Turn on the walkie talkie'
          BackLightTime = BackLightDurationOnStart          ' Turn on back light'
          MessageCountDown = 0                              ' Turn off showing messages prior to game start'

        ElseIf CurrentState = TurnedOff Then
          Call DisablePlayer                                ' Just disable the player'
        End If

      ElseIf Left(WirelessCommand , 5) = "Cheat" Then
        ' Cheat|03|05|100'
        ' HP|Mag|Ammo'

        StringParameter = Mid(WirelessCommand , 7 , 2)
        HP = Val(StringParameter)

        StringParameter = Mid(WirelessCommand , 10 , 2)
        Magazine = Val(StringParameter)

        StringParameter = Mid(WirelessCommand , 13)
        Ammo = Val(StringParameter)

      ElseIf Left(WirelessCommand , 3) = "Die" Then
        ' Die|17'
        ' Die|player'

        StringParameter = Mid(WirelessCommand , 5)
        ShooterNumber = Val(StringParameter)
        HP = 0                                              ' Fake the death by setting HP to zero'
        CurrentState = Dead
        Call DisablePlayer

      ElseIf Left(WirelessCommand , 4) = "Frag" Then
        StringParameter = Mid(WirelessCommand , 6)
        Frag = Val(StringParameter)

      ElseIf Left(WirelessCommand , 2) = "GS" Then
        StringParameter = Mid(WirelessCommand , 4 , 3)
        RoundTime = Val(StringParameter)
        StringParameter = Mid(WirelessCommand , 8 , 3)
        BombTime = Val(StringParameter)

        If RoundTime > 0 Then
          CurrentState = TurnedOn
        End If

        If BombTime <> DefaultBombTime Then
          BombPlanted = 1
        End If

      ElseIf Left(WirelessCommand , 3) = "Msg" Then
        MessageLine1 = Mid(WirelessCommand , 5 , 16)
        MessageLine2 = Mid(WirelessCommand , 21 , 16)
        MessageCountDown = MessageShowDuration
        BackLightTime = 2 * BackLightDuration

      ElseIf WirelessCommand = "Bomb" Then                  ' Bomb has been planted'
        BombPlanted = 1
        BackLightTime = BackLightDuration

      End If


      ' Now send current values in main controller to wireless controller'
      Waitms 50
      Print #5 , CurrentState ; "|" ; HP ; "|" ; Magazine ; "|" ; Ammo ; "|" ; ShooterNumber ; Chr(13);

      ' Wait for wireless micro tu send the new data if necessary'
      waitms 200

      Enable Interrupts                                     ' Resume interrupts'
    End If

    Communicating = 0
  End If
End Sub