'/*'
'******************************************************************************'
'*                               Receiver.bas                                 *'
'*                                                                            *'
'*  Date: 26 Farvardin 1394                                                   *'
'*  Author: Mahdi Hamdarsi                                                    *'
'*  Project: SpecialForce                                                     *'
'*  Comments: This is the LCD manager controller code. Gets text to display   *'
'*            from Main controller and turns it off and on.                   *'
'*                                                                            *'
'******************************************************************************'
'*/'

$regfile "m8def.dat"
$crystal = 14745600
Baud = 9600

' Configure LCD size and options'
Config Lcd = 16 * 2
Config Lcdpin = Pin , Db4 = Portc.3 , Db5 = Portc.2 , Db6 = Portc.1 , Db7 = Portc.0 , E = Portc.4 , Rs = Portc.5
Cursor Off

' For round timer and bomb timer. Interrupts once every second'
Config Timer1 = Timer , Prescale = 256
On Ovf1 UpdateTime
Enable Timer1
Start Timer1

' Interrupt to main controller'
Config Int1 = Rising
Enable Int1
On Int1 ResetLCD

' Communication line to main controller'
Open "comd.0:9600,8,n,1,noinverted" For Input As #2



' Variables
' =======================

DIM CurrentTime As Integer
  CurrentTime = 0

DIM UpdateLCD As Bit
  UpdateLCD = 1

Dim I As Byte
  I = 0

Dim BackLightTime As Byte
  BackLightTime = 5

DIM Temp1 As String * 70
DIM Temp2 As String * 8
DIM Temp3 As String * 8
DIM Temp4 As String * 8
DIM Temp5 As String * 8


' Subroutine Declerations
' =======================

Declare Sub WriteLCD


' Main Loop
' =======================

Stop WatchDog
Enable Interrupts


CLS
LCD "Started."
Locate 2 , 1
LCD "Waiting..."
wait 1

Do
  Do
    Input #2 , Temp1
  Loop Until Temp1 = "KIR"

  Input #2 , Temp2
  Input #2 , Temp3
  Input #2 , Temp4
  Input #2 , Temp5
  Input #2 , BackLightTime

  If BackLightTime = 0 Then
    PORTC.6 = 0
  Else
    PORTC.6 = 1
  End IF

  WriteLCD
Loop
End


Sub WriteLCD
  For I = 1 To 8
    If Mid(Temp2 , I , 1) = "`" Then
      Mid(Temp2 , I , 1) = " "
    End If
    If Mid(Temp3 , I , 1) = "`" Then
      Mid(Temp3 , I , 1) = " "
    End If
    If Mid(Temp4 , I , 1) = "`" Then
      Mid(Temp4 , I , 1) = " "
    End If
    If Mid(Temp5 , I , 1) = "`" Then
      Mid(Temp5 , I , 1) = " "
    End If
  Next

  Locate 1 , 1
  LCD Temp2
  waitms 50
  Locate 1 , 9
  LCD Temp3
  waitms 50
  Locate 2 , 1
  LCD Temp4
  waitms 50
  Locate 2 , 9
  LCD Temp5
  waitms 50
End Sub

' Timer handler
' ======================

UpdateTime:
  CurrentTime = CurrentTime + 1
  I = CurrentTime Mod 2
  If I = 1 Then
    PORTC.6 = 1
  Else
    PORTC.6 = 0
  End If
return


ResetLCD:
  PORTD.3 = 0
Return
