'/*'
'******************************************************************************'
'*                                Wireless.bas                                *'
'*                                                                            *'
'*  Date: 09 Mordad 1391                                                      *'
'*  Author: Mahdi Hamdarsi                                                    *'
'*  Project: SpecialForce                                                     *'
'*  Comments: This is the wireless controller's code. This controller manages *'
'*            communication with application and main controller. Written by  *'
'*            Mahdi Hamdarsi, inspired by previous work of Hojjat Dorudian.   *'
'*                                                                            *'
'******************************************************************************'
'*/'

$regfile "m8def.dat"
$crystal = 14745600
Baud = 9600




'/****************************************************************************/'
'/***************************  Chip Configurations  **************************/'
'/****************************************************************************/'

Ddrd.4 = 1
Ddrd.4 = 1
Ddrd.2 = 1

' Updates time in regular intervals ~ 50 msec'
Config Watchdog = 1024
Config Timer1 = Timer , Prescale = 256
On Ovf1 Timerticked
Start Timer1

' Interrupt to main controller'
Config Int1 = Rising
Enable Int1
On Int1 Mega16dataexchange


'/****************************************************************************/'
'/***********************  Communication Lines  ******************************/'
'/****************************************************************************/'

Portd.2 = 1
Open "comc.5:9600,8,n,1,noinverted" For Input As #1         ' Line to read from main controller'
Open "comc.4:9600,8,n,1,noinverted" For Output As #2        ' Line to write to main controller'



'/****************************************************************************/'
'/*************************  Variables and Constants  ************************/'
'/****************************************************************************/'

' Number of the kevlar. Change this for each kevlar.'
' Blue kevlars are in range [1..10] and'
' green kevlars are in range [11..20]'
' Master controls
' ===================================================='
Dim Kevlarname As String * 3
  Kevlarname = "05|"

Dim Debugging As Bit
  Debugging = 0


' Various flags in communicatiion with main controller'
' ===================================================='

Dim Bombplantedflag As Bit
  Bombplantedflag = 0

Dim Stateflag As Bit
  Stateflag = 0

Dim Configflag As Bit
  Configflag = 0

Dim Paramsflag As Bit
  Paramsflag = 0

Dim Cheatflag As Bit
  Cheatflag = 0

Dim Dieflag As Bit
  Dieflag = 0

Dim Fragflag As Bit
  Fragflag = 0

Dim Gamestateflag As Bit
  Gamestateflag = 0

Dim Informflag As Bit
  Informflag = 0

Dim Messageflag As Bit
  Messageflag = 0

Dim Lastcommandisforall As Bit
  Lastcommandisforall = 0

Dim Deadflag As Byte
  Deadflag = 0

Dim Sendrequestcounter As Byte
  Sendrequestcounter = 0

Dim Updatemainmicro As Bit
  Updatemainmicro = 0


' Variables to deal with messages and commands'
' ===================================================='

Dim Command As String * 60
  Command = ""

Dim Commandlength As Byte
  Commandlength = 0

Dim Message As String * 60
  Message = ""

Dim Currentstatestring As String * 50
  Currentstatestring = ""

Dim Newstatestring As String * 50
  Newstatestring = ""

Dim Parameters As String * 50
  Parameters = ""

Dim Key As String * 1
  Key = ""




'/****************************************************************************/'
'/****************************  Initialization  ******************************/'
'/****************************************************************************/'

Waitms 100
Enable Interrupts
Enable Timer1
Start Watchdog




'/****************************************************************************/'
'/*******************************  Main Loop  ********************************/'
'/****************************************************************************/'

Do
  ' Input the command character by character'
  Reset Watchdog
  Disable Int1
  Command = ""
  Commandlength = 0
  If Ischarwaiting() = 1 Then
    Do
      Key = Inkey()
      If Key <> Chr(13) And Key <> Chr(10) Then
        Command = Command + Key
        Commandlength = Len(command)
      End If
    Loop Until Key = Chr(13) Or Key = Chr(10) Or Commandlength > 50

    If Commandlength > 50 Then
      Command = ""
      While Ischarwaiting() = 1
        Key = Inkey()
      Wend
    End If
  End If


  ' Kevlar Specific Commands'
  ' Only process the command if it's intended for this kevlar'
  Reset Watchdog
  If Left(command , 3) = Kevlarname Or Left(command , 3) = "All" Then

    If Left(command , 3) = "All" Then                       ' Do not respond when the command is for all'
      Lastcommandisforall = 1
    End If

    Command = Mid(command , 4)                              ' Extract the command text from the given command'
    Updatemainmicro = 1

    If Len(kevlarname) <> 3 Then
      Updatemainmicro = 0
      Print "SpecialForce|WirelessOverflow" ; Chr(13);

    Elseif Left(command , 3) = "Die" Then
      Parameters = Mid(command , 5 , 2)
      Dieflag = 1

    Elseif Left(command , 11) = "BombPlanted" Then
      Bombplantedflag = 1

    Elseif Left(command , 4) = "Ping" Then
      Print Kevlarname ; "Pong" ; Chr(13);
      Updatemainmicro = 0
      Waitms 20

    Elseif Left(command , 5) = "State" Then
      Parameters = Mid(command , 7)
      Stateflag = 1

    Elseif Left(command , 5) = "Cheat" Then
      Parameters = Mid(command , 7)
      Cheatflag = 1

    Elseif Left(command , 6) = "Config" Then
      Parameters = Mid(command , 8)
      Configflag = 1
      Sendrequestcounter = 0

    Elseif Left(command , 6) = "Params" Then
      Parameters = Mid(command , 8)
      Paramsflag = 1

    Elseif Left(command , 4) = "Frag" Then
      Parameters = Mid(command , 6)
      Fragflag = 1

    Elseif Left(command , 9) = "GameState" Then
      Parameters = Mid(command , 11)
      Gamestateflag = 1

    Elseif Left(command , 6) = "Update" Then
      Informflag = 1

    Elseif Left(command , 4) = "Stop" Then
      If Debugging = 1 Then
        Print "SPForce|Stopped@" ; Deadflag ; Chr(13);
      End If
      Deadflag = 0

    Elseif Left(command , 3) = "Msg" Then
      Parameters = Command
      Messageflag = 1

    Else
      If Debugging = 1 Then
        Print "SPForce|What?|" ; Kevlarname ; "|" ; Command ; Chr(13);
      End If
      Updatemainmicro = 0
      Waitms 20
    End If
  End If

  Enable Int1
  If Updatemainmicro = 1 Then
    Portd.4 = 1
    Updatemainmicro = 0
  End If
Loop

End



'/****************************************************************************/'
'/*******************************  Timer Tick  *******************************/'
'/****************************************************************************/'

Timerticked:
  If Deadflag > 0 Then
    Decr Deadflag
    Print Kevlarname ; "Updated|" ; Currentstatestring ; Chr(13) ;
    Waitms 100
  End If

  If Sendrequestcounter > 0 Then
    Decr Sendrequestcounter
    Print Kevlarname ; "ReqGameState" ; Chr(13);
    Waitms 50
  End If
Return



'/****************************************************************************/'
'/*******************  Data exchange with main controller  *******************/'
'/****************************************************************************/'

Mega16dataexchange:
  Portd.4 = 0                                               'Disable data transfer'
  Message = ""

  If Stateflag = 1 Then
    Print #2 , "State|" ; Parameters ; Chr(13);
    Message = "StateSet"
    Stateflag = 0

  Elseif Configflag = 1 Then
    Print #2 , "Conf|" ; Left(kevlarname , 2) ; "|" ; Parameters ; Chr(13);
    Message = "ConfigDone"
    Configflag = 0

  Elseif Paramsflag = 1 Then
    Print #2 , "Prms|" ; Parameters ; Chr(13);
    Message = "ParamsDone"
    Paramsflag = 0

  Elseif Cheatflag = 1 Then
    Print #2 , "Cheat|" ; Parameters ; Chr(13);
    Message = "CheatDone"
    Cheatflag = 0

  Elseif Dieflag = 1 Then
    Print #2 , "Die|" ; Parameters ; Chr(13);
    Message = "FakedDeath"
    Dieflag = 0

  Elseif Bombplantedflag = 1 Then                           ' Bomb has been planted'
    Print #2 , "Bomb" ; Chr(13);
    Message = "BombRoger"
    Bombplantedflag = 0

  Elseif Fragflag = 1 Then
    Print #2 , "Frag|" ; Parameters ; Chr(13);
    Message = "FragDone"
    Fragflag = 0

  Elseif Gamestateflag = 1 Then
    Print #2 , "GS|" ; Parameters ; Chr(13);
    Message = "GameStateDone"
    Gamestateflag = 0

  Elseif Messageflag = 1 Then
    Print #2 , Parameters ; Chr(13);
    Message = "MsgDone"
    Messageflag = 0

  Else
   ' This execution was call from main controller to wireless controller to inform the
   ' wireless controller of kevlar parameter updates'
    Print #2 , "hehe!"
  End If

  ' Update procedure'
  Waitms 10                                                 ' Wait till the last event is sent to main'
  Input #1 , Newstatestring                                 ' Get the new state string from main'

  ' Check if state has changed'
  If Left(newstatestring , 4) <> Left(currentstatestring , 4) Then
    ' Check if player died'
    If Left(newstatestring , 1) = "3" Then
      Deadflag = 20

    Elseif Left(newstatestring , 1) = "0" Then
      Sendrequestcounter = 5

    Else
      Informflag = 1

    End If
  End If

  ' Update the state string even if life or state has not changed'
  Currentstatestring = Newstatestring

  If Message <> "" And Lastcommandisforall = 0 Then
    Print Kevlarname ; Message ; Chr(13);
    Waitms 50
  End If

  If Informflag = 1 And Lastcommandisforall = 0 Then
    Print Kevlarname ; "Updated|" ; Currentstatestring ; Chr(13);
    Waitms 100
    Print Chr(13);
    Informflag = 0
  End If

  If Lastcommandisforall = 1 Then
    Lastcommandisforall = 0
  End If
Return

' End of data exchange'