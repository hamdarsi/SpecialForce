; /*
; *********************************************************************************
; *                         Build.bat : BlackLight Project                        *
; *                                                                               *
; *  Date: 25 Sep 2011                                                            *
; *  Author: Mahdi Hamdarsi                                                       *
; *  Comments: Simple NSIS script to create Nvidia PhysX installers               *
; *                                                                               *
; *********************************************************************************
; */

; Global parameters
; =================================================================================

!include 'MUI2.nsh'
!include 'WinMessages.nsh'
!include 'LogicLib.nsh'
!include 'FontReg.nsh'
!include 'FontName.nsh'

!define MUI_ICON "..\SpecialForce\resources\ApplicationIcon.ico"
!define Name "SpecialForce"
!define ExeName "${Name}.exe"
!define Company "ALERPA"
!define LocalDataFolder "$LOCALAPPDATA\${Company}"
!define Version "1.5"
!define MySQLClient ""

Name "${Name} ${Version}"
OutFile "${Name} ${Version} Setup.exe"
InstallDir $PROGRAMFILES\${Name}
RequestExecutionLevel admin


; Declare used pages
; =================================================================================

!define MUI_ABORTWARNING
;!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_LANGUAGE "English"

; Declare all sections
; =================================================================================

; The main application
Section "${Name} Client"
  SectionIn RO

  ; Check if application is running
  DetailPrint "Checking for running application instance:"
  System::Call 'kernel32::OpenMutex(i 0x100000, b 0, t "ALERPA program for counter strike gaming") i .R0'
  ${IF} $R0 != 0
    DetailPrint "Found one. Please close the program and run setup afterwards."
    System::Call 'kernel32::CloseHandle(i $R0)'
    Abort
  ${ELSE}
    DetailPrint "No application instance found. Proceeding with setup."
  ${ENDIF}
  
  ; Install application files
  SetOutPath $INSTDIR
  File "..\SpecialForce\bin\Release\${ExeName}"
  File "..\SpecialForce\bin\Release\MySql.Data.dll"
  File "..\SpecialForce\bin\Release\ListViewPrinter.dll"
  File "..\SpecialForce\bin\Release\ObjectListView.dll"
  File "..\FingerPrintReader\build\src\Release\FingerPrintReader.dll"
  File "..\FingerPrintReader\SDK\bin\UCBioBSP.dll"

  ; Install Fonts
  DetailPrint "Installing fonts"
  File BNAZANB.TTF
  File BNAZANIN.TTF
  File BTITRBD.TTF
  File DejaVuSansMono.TTF
  StrCpy $FONT_DIR $FONTS
  !insertmacro InstallTTFFont 'BNAZANB.TTF'
  !insertmacro InstallTTFFont 'BNAZANIN.TTF'
  !insertmacro InstallTTFFont 'BTITRBD.TTF'
  !insertmacro InstallTTFFont 'DejaVuSansMono.TTF'
  Delete /REBOOTOK "$INSTDIR\BNAZANB.TTF"
  Delete /REBOOTOK "$INSTDIR\BNAZANIN.TTF"
  Delete /REBOOTOK "$INSTDIR\BTITRBD.TTF"
  Delete /REBOOTOK "$INSTDIR\DejaVuSansMono.TTF"

  ; Remove local app data folder to reset local settings
  RMDir /r "${LocalDataFolder}"
  DetailPrint "Local settings removed"

  ; Create shortcuts
  CreateShortCut "$SMPROGRAMS\${Name}.lnk" "$INSTDIR\${ExeName}"
  CreateShortCut "$DESKTOP\${Name}.lnk" "$INSTDIR\${ExeName}"

  ; Set an environmental variable to indicate path of the installed application
  WriteRegExpandStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "${Name}_ROOT" "$INSTDIR"
  SendMessage ${HWND_BROADCAST} ${WM_WININICHANGE} 0 "STR:Environment" /TIMEOUT=5000

  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "DisplayName" "${Name} ${Version}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "DisplayVersion" "${Version}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "DisplayIcon" "$INSTDIR\${ExeName}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "Comments" "${Name} control module"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "InstallLocation" "$INSTDIR"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "Publisher" "${Company}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "NoRepair" 1
  WriteUninstaller "uninstall.exe"
SectionEnd

; Uninstallation
Section "Uninstall"
  ; The environmental variable
  DeleteRegValue HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "${NAME}_ROOT"
  SendMessage ${HWND_BROADCAST} ${WM_WININICHANGE} 0 "STR:Environment" /TIMEOUT=5000

  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}"

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\${Name}.lnk"
  Delete "$DESKTOP\${Name}.lnk"
  Delete "$DESKTOP\On Screen KeyBoard.lnk"

  ; Remove directories used
  RMDir /r "$INSTDIR"
SectionEnd
