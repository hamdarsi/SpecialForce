# *********************************************************************************
# *                   FindUCBioBSPSDK.cmake : BlackLight Project                  *
# *                                                                               *
# *  Date: 11 Oct 2011                                                            *
# *  Author: Mahdi Hamdarsi                                                       *
# *  Comments: BlackLight build configuration with cmake:                         *
# *                                                                               *
# *            Tries to find UCBioBSP (Object Oriented Input System)              *
# *            http://sourceforge.net/projects/wgois/                             *
# *                                                                               *
# *            Once run this will define:                                         *
# *                                                                               *
# *              - UCBioBSP_FOUND                                                 *
# *              - UCBioBSP_INCLUDE_DIR                                           *
# *              - UCBioBSP_LIBRARY                                               *
# *              - UCBioBSP_RUNTIME_FILES                                         *
# *                                                                               *
# *********************************************************************************
# =================================================================================


# set search hint directories
set(
     UCBioBSP_POSSIBLE_ROOT_PATHS
     $ENV{UCBioBSP_ROOT}
     ${CMAKE_CURRENT_SOURCE_DIR}/SDK
   )


# find UCBioBSP include directory
# =================================================================================

find_path(
  UCBioBSP_INCLUDE_DIR
  NAME          UCBioAPI.h
  HINTS         ${UCBioBSP_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES "include"
)

if(NOT UCBioBSP_INCLUDE_DIR)
  message(STATUS "Checking for UCBioBSP... no")
  message(STATUS "Could not find include path for UCBioBSP, try setting UCBioBSP_ROOT")
  return()
endif()


# find UCBioBSP library
# =================================================================================

# DEBUG SINGLE LIB
find_library(
  UCBioBSP_LIBRARY
  NAMES          UCBioBSP
  HINTS          ${UCBioBSP_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES  "lib"
  DOC            "UCBioBSP library"
)


# check the result
if(NOT UCBioBSP_LIBRARY)
  message(STATUS "Checking for UCBioBSP... no")
  message(STATUS "UCBioBSP include directory: ${UCBioBSP_INCLUDE_DIR}")
  message(STATUS "Could not find UCBioBSP libraries")
  return()
endif()


# UCBioBSP runtime libraries
# =================================================================================

find_file(
           UCBioBSP_RUNTIME_CORE   UCBioBSP.dll
           HINTS                   ${UCBioBSP_POSSIBLE_ROOT_PATHS}
           PATH_SUFFIXES           "bin"
         )

find_file(
           UCBioBSP_RUNTIME_DEVICE UCDevice.dll
           HINTS                   ${UCBioBSP_POSSIBLE_ROOT_PATHS}
           PATH_SUFFIXES           "bin"
         )

set(UCBioBSP_RUNTIME_FILES "${UCBioBSP_RUNTIME_CORE};${UCBioBSP_RUNTIME_DEVICE}")


# everything is found. just finish up
# =================================================================================

set(UCBioBSP_FOUND TRUE CACHE BOOL "Whether UCBioBSP is found on the system or not")
set(UCBioBSP_INCLUDE_DIR ${UCBioBSP_INCLUDE_DIR} CACHE PATH "UCBioBSP include directory")
set(UCBioBSP_LIBRARY ${UCBioBSP_LIBRARY} CACHE PATH "UCBioBSP library")
set(UCBioBSP_RUNTIME_FILES ${UCBioBSP_RUNTIME_FILES} CACHE FILEPATH "Runtime files of the UCBioBSP library")

message(STATUS "Checking for UCBioBSP... yes")
