#include "Virdi.h"

#include <UCBioAPI_Export.h>

/** DeviceManager sigleton instance */
template<> DeviceManager* Klaus::Singleton<DeviceManager>::mInstance = 0;
string DeviceManager::mErrorMessage;
bool DeviceManager::mInitialized = false;

DeviceManager::DeviceManager()
{
  mMajorVersion = -1;
  mMinorVersion = -1;
  mFingerImage = NULL;
  mFingerData = NULL;

  UCBioAPI_RETURN err = UCBioAPI_Init(&mHandle);
  if(err != UCBioAPIERROR_NONE)
  {
    mErrorMessage = "Could not open UCBioAPI";
    return;
  }

  UCBioAPI_VERSION ver;
  memset(&ver, 0, sizeof(UCBioAPI_VERSION));
  if(UCBioAPI_GetVersion(mHandle, &ver) != UCBioAPIERROR_NONE)
  {
    mErrorMessage = "Could not get UCBioAPI version";
    return;
  }

  mMajorVersion = ver.Major;
  mMinorVersion = ver.Minor;

  UCBioAPI_INIT_INFO_0 init_info;
  memset(&init_info, 0, sizeof(UCBioAPI_INIT_INFO_0));
  if(UCBioAPI_GetInitInfo(mHandle, 0, &init_info) != UCBioAPIERROR_NONE)
  {
    mErrorMessage = "Could not get initialization information";
    return;
  }

  UCBioAPI_UINT32 device_count;
  UCBioAPI_DEVICE_ID* device_ids;
  UCBioAPI_DEVICE_INFO_EX* device_infos;
  if(UCBioAPI_EnumerateDevice(mHandle, &device_count, &device_ids, &device_infos))
  {
    mErrorMessage = "Could not eumerate devices";
    return;
  }

  if (device_count == 0)
  {
    mErrorMessage = "No device detected";
    return;
  }

  for(size_t i = 0; i < device_count; ++i)
  {
    DeviceInfo * dev = new DeviceInfo;
    dev->DeviceID = device_ids[i];
    dev->IsOpen = false;
    strcpy(dev->DeviceName, device_infos[i].Name);
    strcpy(dev->DeviceDescription, device_infos[i].Description);
    mDeviceInfos.push_back(dev);
  }

  mInitialized = true;
}

DeviceManager::~DeviceManager()
{
  if(mInitialized)
  {
    for(size_t i = 0; i < mDeviceInfos.size(); ++i)
      delete mDeviceInfos[i];
    mDeviceInfos.clear();

    if(mFingerImage)
      delete [] mFingerImage;

    if(mFingerData)
      delete [] mFingerData;

    UCBioAPI_Terminate(mHandle);

    mInitialized = false;
  }
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
DeviceManager * DeviceManager::getSingleton()
{
  if(!mInstance)
    mInstance = new DeviceManager;

  if(mInstance->initialized())
    return mInstance;
  else
  {
    freeSingleton();
    return NULL;
  }
}

/**
  This method frees the only instace of the this class
*/
void DeviceManager::freeSingleton()
{
  if(mInstance)
  {
    delete mInstance;
    mInstance = NULL;
  }
}

string DeviceManager::getErrorMessage()
{
  return mErrorMessage;
}

UCBioAPI_HANDLE DeviceManager::getHandle() const
{
  return mHandle;
}

bool DeviceManager::initialized()
{
  return mInitialized;
}

int DeviceManager::getMajorVersion() const
{
  return mMajorVersion;
}

int DeviceManager::getMinorVersion() const
{
  return mMinorVersion;
}

int DeviceManager::getDeviceCount() const
{
  return mDeviceInfos.size();
}

const DeviceInfo * DeviceManager::getDeviceInfo(int index) const
{
  if(index < 0 || index >= (int)mDeviceInfos.size())
    return NULL;
  else
    return mDeviceInfos[index];
}

bool DeviceManager::isDeviceOpen(int index) const
{
  if(index < 0 || (size_t)index >= mDeviceInfos.size())
    return false;
  else
    return mDeviceInfos[index]->IsOpen;
}

bool DeviceManager::getDeviceProperties(int index, DeviceProperties & props) const
{
  if(index < 0 || (size_t)index >= mDeviceInfos.size() || !mDeviceInfos[index]->IsOpen)
    return false;

  UCBioAPI_DEVICE_INFO_0 dev;
  memset(&dev, 0, sizeof(UCBioAPI_DEVICE_INFO_0));
  if(UCBioAPI_GetDeviceInfo(mHandle, mDeviceInfos[index]->DeviceID, 0, &dev) != UCBioAPIERROR_NONE)
    return false;

  props.Width = dev.ImageWidth;
  props.Height = dev.ImageHeight;
  props.Brightness = dev.Brightness;
  props.Contrast = dev.Contrast;
  props.Gain = dev.Gain;
  return true;
}

bool DeviceManager::openDevice(int index)
{
  if(index < 0 || (size_t)index >= mDeviceInfos.size())
    return false;

  if(UCBioAPI_OpenDevice(mHandle, mDeviceInfos[index]->DeviceID) != UCBioAPIERROR_NONE)
    return false;

  mDeviceInfos[index]->IsOpen = true;
  return true;
}

bool DeviceManager::closeDevice(int index)
{
  if(index < 0 || (size_t)index >= mDeviceInfos.size() || !mDeviceInfos[(size_t)index]->IsOpen)
    return false;

  if(UCBioAPI_CloseDevice(mHandle, mDeviceInfos[index]->DeviceID) != UCBioAPIERROR_NONE)
    return false;

  mDeviceInfos[index]->IsOpen = false;
  return true;
}

void DeviceManager::setDeviceOutputOptions(HWND output, int br, int bg, int bb, int fr, int fg, int fb)
{
  memset(&mWindowOptions, 0, sizeof(UCBioAPI_WINDOW_OPTION));
  mWindowOptions.FingerWnd = NULL;
  mWindowOptions.WindowStyle = UCBioAPI_WINDOW_STYLE_INVISIBLE;
  mWindowOptions.WindowStyle |= UCBioAPI_WINDOW_STYLE_NO_FPIMG;
  mWindowOptions.WindowStyle |= UCBioAPI_WINDOW_STYLE_NO_TOPMOST;
  mWindowOptions.FingerWnd = output;

  if(output != 0)
  {
    memset(&mAuxWindowOptions, 0, sizeof(mAuxWindowOptions));
   
    mAuxWindowOptions.FPForeColor[0] = fr;
    mAuxWindowOptions.FPForeColor[1] = fg;
    mAuxWindowOptions.FPForeColor[2] = fb;
    mAuxWindowOptions.FPBackColor[0] = br;
    mAuxWindowOptions.FPBackColor[1] = bg;
    mAuxWindowOptions.FPBackColor[2] = bb;
    mWindowOptions.Option2 = &mAuxWindowOptions;
  }
}

CaptureResult DeviceManager::capture(int index, CaptureData & finger)
{
  if(index < 0 || (size_t)index >= mDeviceInfos.size() || !mDeviceInfos[(size_t)index]->IsOpen)
    return NotCaptured;

  UCBioAPI_FIR_HANDLE fir_handle;
  UCBioAPI_FIR_HANDLE fir_audit_handle;
  UCBioAPI_RETURN res = UCBioAPI_Capture(mHandle, UCBioAPI_FIR_PURPOSE_VERIFY, &fir_handle, 2000, &fir_audit_handle, &mWindowOptions);
  if(res == UCBioAPIERROR_CAPTURE_TIMEOUT)
    return CaptureTimedOut;
  else if(res != UCBioAPIERROR_NONE)
    return NotCaptured;

  // initialize input fir
  UCBioAPI_INPUT_FIR fir_input;
  fir_input.Form = UCBioAPI_FIR_FORM_HANDLE;
  fir_input.InputFIR.FIRinBSP = &fir_handle;

  // export data to a string to send to main application as finger specific data
  if(mFingerData)
    delete [] mFingerData;
  UCBioAPI_EXPORT_DATA export_data;

  res = UCBioAPI_FIRToTemplate(mHandle, &fir_input, &export_data, UCBioAPI_TEMPLATE_TYPE_SIZE400);
  if(res != UCBioAPIERROR_NONE)
    return NotCaptured;

  finger.DataSize = export_data.FingerInfo[0].TemplateInfo[0].Length;
  mFingerData = new BYTE[finger.DataSize];
  memcpy(mFingerData, export_data.FingerInfo[0].TemplateInfo[0].Data, finger.DataSize);
  finger.Data = mFingerData;
  UCBioAPI_FreeExportData(&export_data);

  // export raw finger print image to a variable and send it to the caller application
  fir_input.InputFIR.FIRinBSP = &fir_audit_handle;

  if(mFingerImage)
    delete [] mFingerImage;
  UCBioAPI_EXPORT_AUDIT_DATA fir_audit;
  res = UCBioAPI_AuditFIRToImage(mHandle, &fir_input, &fir_audit);
  if(res != UCBioAPIERROR_NONE)
    return NotCaptured;

  finger.ImageWidth = fir_audit.ImageWidth;
  finger.ImageHeight = fir_audit.ImageHeight;
  finger.ImageSize = finger.ImageWidth * finger.ImageHeight;
  mFingerImage = new BYTE[finger.ImageSize];
  memcpy(mFingerImage, fir_audit.AuditData[0].Image->Data, finger.ImageSize);
  finger.Image = mFingerImage;
  UCBioAPI_FreeExportAuditData(&fir_audit);

  UCBioAPI_FreeFIRHandle(fir_handle);

  return CaptureDone;
}
