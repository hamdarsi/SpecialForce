#include "Virdi.h"
#include "FingerPrintDB.h"
#include <sstream>

/**
  This function is used to open a movie in the smart video filter
  @param strFileName File name of the movie
  @return True on successful load, false otherwise
*/
extern "C" __declspec( dllexport ) const char * WINAPI getDeviceManagerErrorString()
{
  static char error_string[512];
  strcpy(error_string, DeviceManager::getErrorMessage().c_str());
  return error_string;
}

extern "C" __declspec( dllexport ) const char * WINAPI getFastSearchDBErrorString()
{
  static char error_string[512];
  strcpy(error_string, FastSearchDB::getErrorMessage().c_str());
  return error_string;
}

extern "C" __declspec(dllexport) void WINAPI openDeviceManager()
{
  DeviceManager::getSingleton();
}

extern "C" __declspec(dllexport) void WINAPI closeDeviceManager()
{
  DeviceManager::freeSingleton();
}

extern "C" __declspec(dllexport) void WINAPI openFastSearchDB()
{
  FastSearchDB::getSingleton();
}

extern "C" __declspec(dllexport) void WINAPI closeFastSearchDB()
{
  FastSearchDB::freeSingleton();
}

extern "C" __declspec(dllexport) bool WINAPI isDeviceManagerInitialized()
{
  return DeviceManager::initialized();
}

extern "C" __declspec(dllexport) bool WINAPI isFastSearchDBInitialized()
{
  return FastSearchDB::initialized();
}

extern "C" __declspec(dllexport) int WINAPI getMajorVersion()
{
  return DeviceManager::getSingleton()->getMajorVersion();
}

extern "C" __declspec(dllexport) int WINAPI getMinorVersion()
{
  return DeviceManager::getSingleton()->getMinorVersion();
}

extern "C" __declspec(dllexport) int WINAPI getDeviceCount()
{
  return DeviceManager::getSingleton()->getDeviceCount();
}

extern "C" __declspec(dllexport) bool WINAPI getDeviceInfo(int index, DeviceInfo & inf)
{
  const DeviceInfo * result = DeviceManager::getSingleton()->getDeviceInfo(index);
  if(!result)
    return false;
 
  inf = *result;
  return true;
}

extern "C" __declspec(dllexport) bool WINAPI isDeviceOpen(int index)
{
  return DeviceManager::getSingleton()->isDeviceOpen(index);
}

extern "C" __declspec(dllexport) bool WINAPI getDeviceProperties(int index, DeviceProperties & props)
{
  return DeviceManager::getSingleton()->getDeviceProperties(index, props);
}

extern "C" __declspec(dllexport) bool WINAPI openDevice(int index)
{
  return DeviceManager::getSingleton()->openDevice(index);
}

extern "C" __declspec(dllexport) bool WINAPI closeDevice(int index)
{
  return DeviceManager::getSingleton()->closeDevice(index);
}

extern "C" __declspec(dllexport) void WINAPI setDeviceOutputOptions(HWND output, int br, int bg, int bb, int fr, int fg, int fb)
{
  DeviceManager::getSingleton()->setDeviceOutputOptions(output, br, bg, bb, fr, fg, fb);
}

extern "C" __declspec(dllexport) CaptureResult WINAPI capture(int index, CaptureData & finger)
{
  return DeviceManager::getSingleton()->capture(index, finger);
}

extern "C" __declspec(dllexport) void WINAPI addFingerPrintToDB(BYTE * data, int size, int user)
{
  FastSearchDB::getSingleton()->addFingerPrint(data, size, user);
}

extern "C" __declspec(dllexport) void WINAPI removeFingerPrintFromDB(int user)
{
  FastSearchDB::getSingleton()->removeFingerPrint(user);
}

extern "C" __declspec(dllexport) void WINAPI clearFastSearchDB()
{
  FastSearchDB::getSingleton()->clearDB();
}

extern "C" __declspec(dllexport) int WINAPI search(BYTE * data, int size)
{
  return FastSearchDB::getSingleton()->search(data, size);
}

extern "C" __declspec(dllexport) void WINAPI getSearchState(int & index, int & count)
{
  FastSearchDB::getSingleton()->getSearchState(index, count);
}

extern "C" __declspec(dllexport) void WINAPI stopSearch()
{
  FastSearchDB::getSingleton()->stopSearch();
}

extern "C" __declspec(dllexport) int WINAPI getFastSearchFPCount()
{
  return FastSearchDB::getSingleton()->getFingerPrintCount();
}

/**
  Standard DLL entry point for DLL function.
  @param 
*/
extern "C" __declspec( dllexport ) BOOL WINAPI DllMain(
    HINSTANCE hinstDLL,  // handle to DLL module
    DWORD fdwReason,     // reason for calling function
    LPVOID lpReserved )  // reserved
{
  // Perform actions based on the reason for calling.
  switch( fdwReason ) 
  { 
    case DLL_PROCESS_ATTACH:
      // Initialize once for each new process.
      // Return FALSE to fail DLL load.
      break;

    case DLL_THREAD_ATTACH:
      // Do thread-specific initialization.
      break;

    case DLL_THREAD_DETACH:
      // Do thread-specific cleanup.
      break;

    case DLL_PROCESS_DETACH:
      // Perform any necessary cleanup.
      break;
  }

  return TRUE;  // Successful DLL_PROCESS_ATTACH.
}
