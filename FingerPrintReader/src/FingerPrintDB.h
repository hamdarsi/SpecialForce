#ifndef FINGERPRINT_DB
#define FINGERPRINT_DB

#include "Singleton.h"
#include <string>
#include <Windows.h>
#include <UCBioAPI.h>

using namespace std;

class FastSearchDB : public Klaus::Singleton<FastSearchDB>
{
  private:
    UCBioAPI_HANDLE mHandle;
    static string mErrorMessage;
    static bool mInitialized;
    bool mStopSearch;
    int mSearchCount;
    int mCurrentIndex;

    FastSearchDB();
    ~FastSearchDB();
  public:
    static FastSearchDB * getSingleton();
    static void freeSingleton();
    static string getErrorMessage();

    static bool initialized();

    void addFingerPrint(BYTE * data, int size, int user);
    void removeFingerPrint(int user);
    int getFingerPrintCount() const;
    void clearDB();
    int search(BYTE * data, int size);
    void getSearchState(int & index, int & count);
    void stopSearch();

    bool shallStopSearch() const;
    void updateSearchState( int index, int count );
};


#endif // FINGERPRINT_DB
