#include "FingerPrintDB.h"
#include "Virdi.h"
#include <cstdlib>
#include <UCBioAPI_FastSearch.h>
#include <UCBioAPI_Export.h>

/** API sigleton instance */
template<> FastSearchDB* Klaus::Singleton<FastSearchDB>::mInstance = 0;
string FastSearchDB::mErrorMessage;
bool FastSearchDB::mInitialized = false;

FastSearchDB::FastSearchDB()
{
  mStopSearch = false;
  mSearchCount = 0;
  mCurrentIndex = 0;

  mHandle = DeviceManager::getSingleton()->getHandle();
  if(UCBioAPI_InitFastSearchEngine(mHandle) != UCBioAPIERROR_NONE)
    mErrorMessage = "Could not initialize fast search db";
  else
    mInitialized = true;
}

FastSearchDB::~FastSearchDB()
{
  if(mInitialized)
  {
    clearDB();
    UCBioAPI_TerminateFastSearchEngine(mHandle);

    mInitialized = false;
  }
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
FastSearchDB * FastSearchDB::getSingleton()
{
  if(!mInstance)
    mInstance = new FastSearchDB;

  if(mInstance->initialized())
    return mInstance;
  else
  {
    freeSingleton();
    return NULL;
  }
}

/**
  This method frees the only instace of the this class
*/
void FastSearchDB::freeSingleton()
{
  if(mInstance)
  {
    delete mInstance;
    mInstance = NULL;
  }
}

string FastSearchDB::getErrorMessage()
{
  return mErrorMessage;
}

bool FastSearchDB::initialized()
{
  return mInitialized;
}

void FastSearchDB::addFingerPrint(BYTE * data, int size, int user)
{
  UCBioAPI_FIR_HANDLE fir_handle;
  if(UCBioAPI_TemplateToFIR(mHandle, data, size, UCBioAPI_TEMPLATE_TYPE_SIZE400, UCBioAPI_FIR_PURPOSE_VERIFY, &fir_handle) != UCBioAPIERROR_NONE)
    return;

  // Create input fir data
  UCBioAPI_INPUT_FIR fir_input;
  fir_input.Form = UCBioAPI_FIR_FORM_HANDLE;
  fir_input.InputFIR.FIRinBSP = &fir_handle;

  // Add to fast search db
  if(UCBioAPI_AddFIRToFastSearchDB(mHandle, &fir_input, user, NULL) != UCBioAPIERROR_NONE)
    mErrorMessage = "Failed to add fir to db";

  UCBioAPI_FreeFIRHandle(fir_handle);
}

void FastSearchDB::removeFingerPrint(int user)
{
  UCBioAPI_RemoveUserFromFastSearchDB(mHandle, user);
}

int FastSearchDB::getFingerPrintCount() const
{
  if(!mInitialized)
    return -1;

  UCBioAPI_UINT32 c;
  UCBioAPI_GetFpCountFromFastSearchDB(mHandle, &c);
  return (int)c;
}

void FastSearchDB::clearDB()
{
  UCBioAPI_ClearFastSearchDB(mHandle);
}

UCBioAPI_RETURN WINAPI MyFastSearchCallBack(UCBioAPI_FASTSEARCH_CALLBACK_PARAM_PTR_0 callback_param, UCBioAPI_VOID_PTR user_param)
{
  FastSearchDB::getSingleton()->updateSearchState(callback_param->MatchedIndex, callback_param->TotalCount);
  if(FastSearchDB::getSingleton()->shallStopSearch())
    return UCBioAPI_FASTSEARCH_CALLBACK_STOP;
  else
    return UCBioAPI_FASTSEARCH_CALLBACK_OK;
}

int FastSearchDB::search(BYTE * data, int size)
{
  int uid = -1;
  UCBioAPI_RETURN result;
  UCBioAPI_FIR_HANDLE fir_handle;
  if(UCBioAPI_TemplateToFIR(mHandle, data, size, UCBioAPI_TEMPLATE_TYPE_SIZE400, UCBioAPI_FIR_PURPOSE_VERIFY, &fir_handle) != UCBioAPIERROR_NONE)
    return -1;

  // Create input fir data
  UCBioAPI_INPUT_FIR fir_input;
  fir_input.Form = UCBioAPI_FIR_FORM_HANDLE;
  fir_input.InputFIR.FIRinBSP = &fir_handle;
      
  UCBioAPI_FASTSEARCH_CALLBACK_INFO_0 callback_info;
  callback_info.CallBackType = 0;
  callback_info.CallBackFunction = MyFastSearchCallBack;
  callback_info.UserCallBackParam = NULL;
  UCBioAPI_FASTSEARCH_FP_INFO fs_info;
  result = UCBioAPI_IdentifyFIRFromFastSearchDB(mHandle, &fir_input, 5, &fs_info, &callback_info);
      
  if (result == UCBioAPIERROR_NONE)
    uid = fs_info.ID;
   
  UCBioAPI_FreeFIRHandle(fir_handle);

  return uid;
}

void FastSearchDB::getSearchState(int & index, int & count)
{
  index = mCurrentIndex;
  count = mSearchCount;
}

void FastSearchDB::stopSearch()
{
  mStopSearch = true;
}

bool FastSearchDB::shallStopSearch() const
{
  return mStopSearch;
}

void FastSearchDB::updateSearchState(int index, int count)
{
  mCurrentIndex = index;
  mSearchCount = count;
}
