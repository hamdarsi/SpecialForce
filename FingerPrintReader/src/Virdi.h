#ifndef VIRDI_WRAPPER
#define VIRDI_WRAPPER

#include <Windows.h>
#include <UCBioAPI.h>
#include <string>
#include <vector>
#include "Singleton.h"

using namespace std;

enum CaptureResult
{
  CaptureDone,
  CaptureTimedOut,
  NotCaptured
};

struct CaptureData
{
  int ImageWidth;
  int ImageHeight;
  int ImageSize;
  BYTE * Image;

  int DataSize;
  BYTE * Data;
};

struct DeviceInfo
{
  int DeviceID;
  char DeviceName[32];
  char DeviceDescription[256];
  bool IsOpen;

  typedef vector<DeviceInfo*> Array;
};

struct DeviceProperties
{
  int Width;
  int Height;
  int Brightness;
  int Contrast;
  int Gain;
};

class DeviceManager : public Klaus::Singleton<DeviceManager>
{
  private:
    UCBioAPI_HANDLE mHandle;
    UCBioAPI_WINDOW_OPTION mWindowOptions;
    UCBioAPI_WINDOW_OPTION_2 mAuxWindowOptions;

    static bool mInitialized;
    int mMajorVersion;
    int mMinorVersion;
    DeviceInfo::Array mDeviceInfos;
    static string mErrorMessage;
    BYTE * mFingerImage;
    BYTE * mFingerData;

    DeviceManager ();
    ~DeviceManager ();
  public:
    static DeviceManager * getSingleton();
    static void freeSingleton();
    static string getErrorMessage();

    UCBioAPI_HANDLE getHandle() const;

    static bool initialized();
    int getMajorVersion() const;
    int getMinorVersion() const;

    int getDeviceCount() const;
    const DeviceInfo * getDeviceInfo(int index) const;
    bool isDeviceOpen(int index) const;
    bool getDeviceProperties(int index, DeviceProperties & props) const;

    bool openDevice(int index);
    bool closeDevice(int index);
    void setDeviceOutputOptions(HWND output, int br, int bg, int bb, int fr, int fg, int fb);

    CaptureResult capture(int index, CaptureData & finger);
};

#endif // VIRDI_WRAPPER
