﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Globalization;
using System.IO;
using System.Media;

namespace SpecialForce
{
  public class Session
  {
    #region Enumerations
    public enum OpenResult
    {
      SiteNotSet,
      Succeeded
    }

    public enum LockReason
    {
      TimeOut,
      ManualLock
    }
    #endregion Enumerations


    #region Internal Data
    static private DB.Entrance mLogin;
    static private Control mSynchronizer = null;
    static private Locker mFilter = new Locker();
    #endregion Internal Data


    #region Properties
    static private DB.User mCurrentStaff = null;
    static public DB.User CurrentStaff
    {
      get { return mCurrentStaff; }
    }

    static public bool Open
    {
      get { return mCurrentStaff != null; }
    }

    static private int mActiveSiteID = -1;
    static public int ActiveSiteID
    {
      get { return mActiveSiteID; }
    }

    static public PersianDateTime LoginTime
    {
      get { return mLogin.Enter; }
    }

    static private string mErrorString;
    static public string ErrorString
    {
      get { return mErrorString; }
    }

    static private NumberFormatInfo mNumberFormat = new CultureInfo("en-US", false).NumberFormat;
    static public NumberFormatInfo NumberFormat
    {
      get { return mNumberFormat; }
    }

    /// <summary>
    /// Application wide property to indicate whether application
    /// is being debugged/executed under visual studio or not.
    /// Used primarily for hand coden components to tell them that 
    /// they are viewed in live time or just on form designer.
    /// </summary>
    static public bool DevelopMode
    {
      get { return System.Reflection.Assembly.GetExecutingAssembly().Location.Contains("VisualStudio"); }
    }

    /// <summary>
    /// Application wide property to tell whether build
    /// configuration type was release or debug.
    /// </summary>
    static public bool ReleaseBuild
    {
      get { return !System.Diagnostics.Debugger.IsAttached; }
    }

    /// <summary>
    /// Application wide property to indicate whether 
    /// execution is currently on the main thread or not.
    /// </summary>
    static public bool OnMainThread
    {
      get 
      {
        if (mSynchronizer == null)
          return true;
        else if (mSynchronizer.InvokeRequired)
          return false;
        else
          return true;
      }
    }

    /// <summary>
    /// Application wide property to indicate whether
    /// application is running in wizard mode.
    /// </summary>
    static public bool WizardMode
    {
      get
      {
        if (Application.OpenForms.Count == 0)
          return false;

        if (Application.OpenForms[0] is Wizards.WizardForm)
          return true;

        return false;
      }
    }

    /// <summary>
    /// Application wide property to use waiting cursor or not
    /// </summary>
    static public bool WaitCursor
    {
      get { return Application.UseWaitCursor; }
      set
      {
        Application.UseWaitCursor = value;
        Cursor.Position = Cursor.Position;
        Application.DoEvents();
      }
    }
    #endregion Properties


    #region Events
    static public event EventHandler<SessionChangingArgs> OnSessionChanging;
    static public event EventHandler<EventArgs> OnSessionChanged;
    static public event EventHandler<SessionLockingArgs> OnSessionLocking;
    static public event EventHandler<EventArgs> OnSessionLocked;
    #endregion Events


    #region Initialization and Finalization
    static public OpenResult Start()
    {
      mNumberFormat.CurrencyPositivePattern = 0;
      mNumberFormat.CurrencyNegativePattern = 2;
      mNumberFormat.CurrencySymbol = "";
      mNumberFormat.NegativeSign = "-";
      mNumberFormat.CurrencyDecimalDigits = 0;
      mNumberFormat.CurrencyDecimalSeparator = ".";
      mNumberFormat.CurrencyGroupSeparator = ",";
      mNumberFormat.CurrencyGroupSizes = new int[3] { 3, 3, 3 };

      mActiveSiteID = Properties.Settings.Default.SiteID;
      if (mActiveSiteID == -1)
      {
        Logger.Log("(Session) No site set as registered");
        return OpenResult.SiteNotSet;
      }

      Application.AddMessageFilter(mFilter);
      return OpenResult.Succeeded;
    }

    static public void End()
    {
      if (!Open)
        return;

      RecordLogout();
      mActiveSiteID = -1;
      Application.RemoveMessageFilter(mFilter);
    }
    #endregion Initialization and Finalization


    #region Login and Lock
    static public bool Login(int id)
    {
      // check for the user
      bool login = false;
      DB.User staff = DB.User.GetUserByID(id);
      if (staff == null)
        mErrorString = "کاربر مورد نظر پیدا نشد";
      else if (staff.Fired)
        mErrorString = "شما اخراج شده اید";
      else if (staff.IsLowlyUser() && Program.ExecutionMode != ExecutionMode.Kiosk)
        mErrorString = "کاربر ها حق ورود را ندارند";
      else
        login = true;

      if (!login)
        return false;

      // Emit session changing event to check if all open forms can work with the new staff
      EventHandler<SessionChangingArgs> ev1 = OnSessionChanging;
      SessionChangingArgs arg1 = new SessionChangingArgs(staff);
      if (ev1 != null)
        ev1(null, arg1);

      if (arg1.Cancel)
      {
        mErrorString = "ورود مجاز نشد";
        return false;
      }

      // record login information
      RecordLogin(staff);

      // Now emit session changed to update all open forms on the newly logged on staff
      EventHandler<EventArgs> ev2 = OnSessionChanged;
      if (ev2 != null)
        ev2(null, null);

      // enable the message filter.
      mFilter.Enabled = true;
      mErrorString = "";
      return true;
    }

    static public void Lock()
    {
      Synchronize(new MethodInvoker(delegate { Lock(true); }));
    }

    static private void Lock(bool manual)
    {
      // inform all windows that session is being locked
      // log out the current logged in user after all windows have been informed
      EventHandler<SessionLockingArgs> ev1 = OnSessionLocking;
      if (ev1 != null)
        ev1(null, new SessionLockingArgs(manual ? LockReason.ManualLock : LockReason.TimeOut));

      // logout the current logged in user
      RecordLogout();
      mFilter.Enabled = false;

      // now inform all that session is locked
      EventHandler<EventArgs> ev2 = OnSessionLocked;
      if (ev2 != null)
        ev2(null, null);
    }

    static private void RecordLogin(DB.User staff)
    {
      mLogin = null;
      mCurrentStaff = staff;
      mLogin = DB.Entrance.Login(mCurrentStaff);
    }

    static private void RecordLogout()
    {
      if(mLogin == null)
        throw new Exception("(Session) No user logged in to logout");

      mLogin.Logout();
      mCurrentStaff = null;
    }

    static public void ResetLockTimer()
    {
      if (mFilter.Enabled)
      {
        mFilter.Enabled = false;
        mFilter.Enabled = true;
      }
    }

    static private void TimerTick(Object Sender, EventArgs e)
    {
      Lock(false);
    }
    #endregion Login and Lock


    #region Synchornization
    static public void StartSynchronization(Control ctrl)
    {
      if (mSynchronizer == null)
        mSynchronizer = ctrl;
      else
        throw new Exception("(DB.Session) Previous synchronization instance not cleared");

      Logger.Log("(Session) Started synchronization");
    }

    static public void StopSynchronization()
    {
      if (mSynchronizer != null)
      {
        mSynchronizer = null;
        Logger.Log("(Session) Stopped synchronization");
      }
    }

    static public void Synchronize(Delegate method)
    {
      if(mSynchronizer == null)
        return;

      if (mSynchronizer.IsDisposed)
        throw new Exception("(Session) synchronizer instance is disposed");

      mSynchronizer.BeginInvoke(method);
    }
    #endregion Synchornization


    #region MessageBox!
    static public bool Ask(string strMessage, string strCaption = "اخطار")
    {
      return MessageBox.Show(
                              strMessage, 
                              strCaption, 
                              MessageBoxButtons.YesNo, 
                              MessageBoxIcon.None,
                              MessageBoxDefaultButton.Button1,
                              MessageBoxOptions.RightAlign | MessageBoxOptions.RtlReading
                            ) == DialogResult.Yes;
    }

    static public DialogResult AskAGirl(string strMessage, string strCaption = "اخطار")
    {
      return MessageBox.Show(
                              strMessage,
                              strCaption,
                              MessageBoxButtons.YesNoCancel,
                              MessageBoxIcon.None,
                              MessageBoxDefaultButton.Button1,
                              MessageBoxOptions.RightAlign | MessageBoxOptions.RtlReading
                            );
    }

    static private void ShowMessageBox(string strMessage, string strCaption)
    {
      MessageBox.Show(
                       strMessage,
                       strCaption,
                       MessageBoxButtons.OK,
                       MessageBoxIcon.None,
                       MessageBoxDefaultButton.Button1,
                       MessageBoxOptions.RightAlign | MessageBoxOptions.RtlReading
                     );
    }

    static public void ShowWarning(string strMessage)
    {
      ShowMessageBox(strMessage, "اخطار");
    }

    static public void ShowError(string strMessage)
    {
      ShowMessageBox(strMessage, "خطا");
    }

    static public void ShowMessage(string strMessage)
    {
      ShowMessageBox(strMessage, "پیام");
    }
    #endregion MessageBox!


    #region Sound Play
    static public void PlaySound(Stream snd)
    {
      SoundPlayer sp = new SoundPlayer(snd);
      sp.Play();
    }
    #endregion Sound Play
  }
}
