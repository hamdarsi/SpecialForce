﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce
{
  #region Session Events
  public class SessionChangingArgs : EventArgs
  {
    private bool mCancel;
    public bool Cancel
    {
      get { return mCancel; }
      set { mCancel = value; }
    }

    private DB.User mPersonel;
    public DB.User Personel
    {
      get { return mPersonel; }
    }

    public SessionChangingArgs(DB.User user)
    {
      mCancel = false;
      mPersonel = user;
    }
  }

  public class SessionLockingArgs : EventArgs
  {
    private Session.LockReason mReason;
    public Session.LockReason Reason
    {
      get { return mReason; }
    }

    public SessionLockingArgs(Session.LockReason reason)
    {
      mReason = reason;
    }
  }
  #endregion Session Events


  #region FingerPrint Events
  public class CaptureEventArgs : EventArgs
  {
    private byte[] mData;
    public byte[] Data
    {
      get { return mData; }
    }

    private Image mImage;
    public Image Image
    {
      get { return mImage; }
    }

    public CaptureEventArgs(byte[] data, Image image)
    {
      mData = data;
      mImage = image;
    }
  }

  public class SearchFinishEventArgs : EventArgs
  {
    private int mUserID;
    public int UserID
    {
      get { return mUserID; }
    }

    public DB.User User
    {
      get { return DB.User.GetUserByID(mUserID); }
    }

    private bool mSucceeded;
    public bool Succeeded
    {
      get { return mSucceeded; }
    }

    public SearchFinishEventArgs(int uid)
    {
      // search cancelled or no user found?
      mSucceeded = uid > 0;
      mUserID = uid;
    }
  }

  public class SearchUpdateEventArgs : EventArgs
  {
    private int mCurrentIndex;
    public int CurrentIndex
    {
      get { return mCurrentIndex; }
    }

    private int mTotalCount;
    public int TotalCount
    {
      get { return mTotalCount; }
    }

    public SearchUpdateEventArgs(int index, int count)
    {
      mCurrentIndex = index;
      mTotalCount = count;
    }
  }
  #endregion FingerPrint Events


  #region ControlTower Events
  public class MessageReceivedArgs : EventArgs
  {
    private string mMessage;
    public string Message
    {
      get { return mMessage; }
    }

    public MessageReceivedArgs(string msg)
    {
      mMessage = msg;
    }
  }

  public class CommandArgs : EventArgs
  {
    private Command mCommand;
    public Command SentCommand
    {
      get { return mCommand; }
    }

    public CommandArgs(Command cmd)
    {
      mCommand = cmd;
    }
  }

  public class PlayerDiedArgs : EventArgs
  {
    private ControlTower.Kevlar mKiller;
    public ControlTower.Kevlar KillerPlayer
    {
      get { return mKiller; }
    }

    private ControlTower.Kevlar mKilled;
    public ControlTower.Kevlar KilledPlayer
    {
      get { return mKilled; }
    }

    private DB.User mKillerUser;
    public DB.User KillerUser
    {
      get { return mKillerUser; }
    }

    private DB.User mKilledUser;
    public DB.User KilledUser
    {
      get { return mKilledUser; }
    }

    public PlayerDiedArgs(ControlTower.Kevlar killer, ControlTower.Kevlar killed, DB.User killer_user, DB.User killed_user)
    {
      mKiller = killer;
      mKilled = killed;
      mKillerUser = killer_user;
      mKilledUser = killed_user;
    }
  }

  public class BombStateChangedArgs : EventArgs
  {
    private ControlTower.Bomb mBomb;
    public ControlTower.Bomb UpdatedBomb
    {
      get { return mBomb; }
    }

    public BombStateChangedArgs(ControlTower.Bomb bomb)
    {
      mBomb = bomb;
    }
  }

  public class DeviceEventArgs : EventArgs
  {
    private ControlTower.DeviceBase mDevice = null;
    public ControlTower.DeviceBase Device
    {
      get { return mDevice; }
    }

    public ControlTower.Kevlar Kevlar
    {
      get { return mDevice as ControlTower.Kevlar; }
    }

    public ControlTower.Bomb Bomb
    {
      get { return mDevice as ControlTower.Bomb; }
    }

    public int Index
    {
      get { return Kevlars.IndexFromID(mDevice.ID); }
    }

    public DB.User Player
    {
      get { return Kevlar != null ? Kevlar.Player : null; }
    }

    public DeviceID ID
    {
      get { return mDevice.ID; }
    }

    public bool IsKevlar
    {
      get { return mDevice is ControlTower.Kevlar; }
    }

    public bool IsBomb
    {
      get { return mDevice is ControlTower.Bomb; }
    }

    public DeviceEventArgs(ControlTower.DeviceBase device)
    {
      mDevice = device;
    }
  }
  #endregion ControlTower Events


  #region GameManager Events
  public class GameDeleteArgs : EventArgs
  {
    private DB.Game mGame;
    public DB.Game Game
    {
      get { return mGame; }
    }

    public GameDeleteArgs(DB.Game game)
    {
      mGame = game;
    }
  }
  #endregion GameManager Events


  #region Schedule Events
  public class ScheduleEntryResizeArgs : EventArgs
  {
    /// <summary>
    /// New rectangle after resize. In screen coordinates
    /// </summary>
    private Rectangle mNewRect;
    public Rectangle NewRect
    {
      get { return mNewRect; }
    }

    public ScheduleEntryResizeArgs(Rectangle new_rect)
    {
      mNewRect = new_rect;
    }
  }

  public class ScheduleTableUpdatedArgs : EventArgs
  {
    private PersianDateTime mDate = null;
    public PersianDateTime Date
    {
      get { return mDate; }
      set { mDate = value; }
    }

    public ScheduleTableUpdatedArgs()
    {
    }
  }
  #endregion Schedule Events


  #region ToolButton Events
  public class DragObjectRequestArgs : EventArgs
  {
    private Object mObject = null;
    public Object Object
    {
      get { return mObject; }
      set { mObject = value; }
    }

    public DragObjectRequestArgs()
    {
    }
  }
  #endregion ToolButton Events


  #region Report Events
  public class ReportRowArgs : EventArgs
  {
    private object mData = null;
    public object Data
    {
      get { return mData; }
    }

    public ReportRowArgs(object data)
    {
      mData = data;
    }
  }
  #endregion Report Events


  #region Graphical Update System Events
  public class RecreateArgs : EventArgs
  {
    private Bitmap mView;
    public Bitmap View
    {
      get { return mView; }
    }

    private Graphics mCanvas;
    public Graphics Canvas
    {
      get { return mCanvas; }
      set { mCanvas = value; }
    }

    public RecreateArgs(Bitmap view, Graphics canvas)
    {
      mView = view;
      mCanvas = canvas;
    }
  }
  #endregion Graphical Update System Events


  #region Competition Chart System Events
  public class CompetitionChartClickArgs : EventArgs
  {
    private Competitions.Charts.MatchButton mMatchButton;
    public Competitions.Charts.MatchButton MatchButton
    {
      get { return mMatchButton; }
    }

    private MouseButtons mMouseButton;
    public MouseButtons MouseButton
    {
      get { return mMouseButton; }
    }

    public bool IsButtonClicked
    {
      get { return mMatchButton != null; }
    }

    public CompetitionChartClickArgs(Competitions.Charts.MatchButton btn, MouseButtons mbtn)
    {
      mMatchButton = btn;
      mMouseButton = mbtn;
    }
  }
  #endregion Competition Chart System Events
}
