﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SpecialForce
{
  static public class Logger
  {
    static private StreamWriter mLog = null;

    /// <summary>
    /// Tries to open the log output file. Removes old 
    /// instances of log files and creates the new one
    /// in the LocalData\AppName folder.
    /// </summary>
    static public void Open()
    {
      try
      {
        // Folder path
        string strFolder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        strFolder = Path.Combine(strFolder, Application.CompanyName);

        // Only 9 files can remain
        string[] files = Directory.GetFiles(strFolder);
        List<string> logs = new List<string>();
        for(int i = 0; i < files.Length; ++i)
          if (files[i].Substring(strFolder.Length + 1).StartsWith("Events, "))
            logs.Add(files[i]);

        // Remove excess files
        if (logs.Count >= Properties.Settings.Default.MaximumLogFiles)
        {
          logs.Sort();
          for (int i = 0; i <= logs.Count - Properties.Settings.Default.MaximumLogFiles; ++i)
            File.Delete(logs[i]);
        }

        // Create the file name!
        PersianDateTime now = PersianDateTime.Now;
        string strFile = now.ToString(DateTimeString.CompactDate);
        strFile = strFile.Replace('/', ',');

        strFile = strFile + ' ' + now.ToString(DateTimeString.Time);
        strFile = strFile.Replace(':', ',');
        strFile = "Events, " + strFile + ".txt";

        string strPath = Path.Combine(strFolder, strFile);

        // Finally create the log!
        mLog = new System.IO.StreamWriter(strPath, false);
      }
      catch
      {
      }

      Log("Logging started.");
    }

    /// <summary>
    /// Closes the log output file.
    /// </summary>
    static public void Close()
    {
      Log("Logging ended.");

      if (mLog != null)
      {
        mLog.Close();
        mLog = null;
      }
    }

    /// <summary>
    /// Logs the given string. Prefixes it with current time.
    /// </summary>
    /// <param name="log">The string to log</param>
    static public void Log(string log)
    {
      if (mLog == null)
        return;

      string time = PersianDateTime.Now.ToString(DateTimeString.Time);
      mLog.WriteLine("[" + time + "] " + log);
    }

    /// <summary>
    /// Logs the given exception completely.
    /// </summary>
    /// <param name="e">The exception to log</param>
    /// <param name="module">The module in which the exception has been thrown</param>
    /// <param name="detail">Optional detail relative to the exception</param>
    static public void LogException(Exception e, string module, string detail = "Nothing provided")
    {
      if (mLog == null)
        return;

      module = "(" + module + ") ";
      Log(module + "Cought exception");
      Log(module + "  Detail: " + detail);
      Log(module + "  Message: " + e.Message);
      Log(module + "  Stack trace: ");
      Log(e.StackTrace);
      Log("");

      // If an exception was throw, flush the buffer immediately
      mLog.Flush();
    }

    /// <summary>
    /// This method flushes the log cache to disk.
    /// </summary>
    static public void Flush()
    {
      if (mLog != null)
        mLog.Flush();
    }
  }
}
