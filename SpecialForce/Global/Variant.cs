﻿using System;

namespace SpecialForce
{
  public class Variant
  {
    #region Data
    private object mValue;
    private System.Type mType;
    #endregion Data


    #region Constructor
    public Variant(object value = null)
    {
      mValue = value;
      mType = value != null ? value.GetType() : null;
    }
    #endregion Constructor


    #region Boolean -> Variant
    public static implicit operator Variant(bool value)
    {
      return new Variant(value);
    }

    public static implicit operator bool(Variant value)
    {
      return Convert.ToBoolean(value.mValue);
    }
    #endregion Boolean -> Variant


    #region Integer -> Variant
    public static implicit operator Variant(int value)
    {
      return new Variant(value);
    }

    public static implicit operator int(Variant value)
    {
      return Convert.ToInt32(value.mValue);
    }
    #endregion Integer -> Variant


    #region Double -> Variant
    public static implicit operator Variant(double value)
    {
      return new Variant(value);
    }

    public static implicit operator double(Variant value)
    {
      return Convert.ToDouble(value.mValue);
    }
    #endregion Double -> Variant


    #region Currency -> Variant
    public static implicit operator Variant(Currency value)
    {
      return new Variant(value);
    }

    public static implicit operator Currency(Variant value)
    {
      return value.mValue as Currency;
    }
    #endregion Currency -> Variant


    #region String -> Variant
    public static implicit operator Variant(string value)
    {
      return new Variant(value);
    }

    public static implicit operator string(Variant value)
    {
      return value.mValue as string;
    }
    #endregion Integer -> Variant


    #region DateTime -> Variant
    public static implicit operator Variant(PersianDateTime value)
    {
      return new Variant(value);
    }

    public static implicit operator PersianDateTime(Variant value)
    {
      if (value.mValue is DateTime)
        return new PersianDateTime((DateTime)value.mValue);
      else if (value.mValue is PersianDateTime)
        return value.mValue as PersianDateTime;
      else if (value.mValue == DBNull.Value)
        return null;
      else if (value == null)
        return null;
      else
        throw new Exception("(Variant) No supported value to use as PersianDateTime: " + value.mValue.GetType().ToString());
    }
    #endregion DateTime -> Variant


    #region Variant -> String
    /// <summary>
    /// For non timestamp values, this will 
    /// returns the string representation of the value.
    /// </summary>
    public override string ToString()
    {
      if (mType == Types.Currency)
        return (mValue as Currency).ToString();
      else if (mType == Types.DateTime)
        throw new Exception("(Reports.Variant) can not handle PersianDateTime in Variant.ToString()");

      return mValue.ToString();
    }

    /// <summary>
    /// Assumes the contents of the Variant is a 
    /// timestamp and tries to convert it to a string.
    /// </summary>
    /// <param name="dts">Conversion mode</param>
    public string ToString(DateTimeString dts)
    {
      // If the date time value is not set, then type is also null.
      // So handle it in the mValue == null clause.
      if(mType != Types.DateTime && mType != null)
        throw new Exception("(Reports.Variant) can not handle any type except PersianDateTime in Variant.ToString(DTS)");

      if (mValue == null)
        return "-";

      return (mValue as PersianDateTime).ToString(dts);
    }
    #endregion Variant -> String


    #region Operator overloads
    public static bool operator <(Variant v1, Variant v2)
    {
      if(v1.mType != v2.mType)
        throw new Exception("(Reports.Variant) variants not of same type");

      if(v1.mType == Types.Integer)
        return (int)v1.mValue < (int)v2.mValue;

      else if(v1.mType == Types.Double)
        return (double)v1.mValue < (double)v2.mValue;

      else if (v1.mType == Types.Currency)
        return (v1.mValue as Currency).Value < (v2.mValue as Currency).Value;

      else if (v1.mType == Types.String)
        return (v1.mValue as string).CompareTo(v2.mValue as string) < 0;

      else if(v1.mType == Types.DateTime)
        return (v1.mValue as PersianDateTime) < (v2.mValue as PersianDateTime);

      throw new Exception("(Reports.Variant) unkown type: " + v1.mType);
    }

    public static bool operator > (Variant v1, Variant v2)
    {
      if (v1.mType != v2.mType)
        throw new Exception("(Reports.Variant) variants not of same type");

      if (v1.mType == Types.Integer)
        return (int)v1.mValue > (int)v2.mValue;

      else if (v1.mType == Types.Double)
        return (double)v1.mValue > (double)v2.mValue;

      else if (v1.mType == Types.Currency)
        return (v1.mValue as Currency).Value > (v2.mValue as Currency).Value;

      else if (v1.mType == Types.String)
        return (v1.mValue as string).CompareTo(v2.mValue as string) > 0;

      else if (v1.mType == Types.DateTime)
        return (v1.mValue as PersianDateTime) > (v2.mValue as PersianDateTime);

      throw new Exception("(Reports.Variant) unkown type: " + v1.mType);
    }
    #endregion Operator overloads
  }
}
