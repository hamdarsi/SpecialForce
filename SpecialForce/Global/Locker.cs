﻿using System;
using System.Windows.Forms;
using System.Security.Permissions;
using System.Collections.Generic;

namespace SpecialForce
{
  [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
  public class Locker : IMessageFilter
  {
    private Timer mTimer = new Timer();
    private DateTime mLastTime = DateTime.MinValue;

    public bool Enabled
    {
      get { return mTimer.Enabled; }
      set { mTimer.Enabled = value; }
    }

    public Locker()
    {
      mTimer.Interval = 5000;
      mTimer.Tick += CheckLock;
    }

    public bool PreFilterMessage(ref Message m)
    {
      // In case of recieving messages about mouse and keyboard buttons
      // timer will be reset
      switch(m.Msg)
      {
        case Win32.WM_LBUTTONDOWN:
        case Win32.WM_RBUTTONDOWN:
        case Win32.WM_SYSKEYDOWN:
        case Win32.WM_KEYDOWN:
          mLastTime = DateTime.Now;
          return false;

        default:
          return false;
      }
    }

    private void CheckLock(Object sender, EventArgs e)
    {
      if (mLastTime == DateTime.MinValue)
        mLastTime = DateTime.Now;

      DateTime now = DateTime.Now;
      if ((now - mLastTime).TotalMinutes >= Properties.Settings.Default.IdleMinutesToLock)
      {
        string str = "Now: " + new PersianDateTime(now).ToString(DateTimeString.DetailedTime);
        str += ", ";
        str += "Last: " + new PersianDateTime(now).ToString(DateTimeString.DetailedTime);
        str += ", ";
        str += "Duration: " + Properties.Settings.Default.IdleMinutesToLock;
        Logger.Log("(Locker) Locking system (" + str + ")");

        mLastTime = now;
        Session.Lock();
      }
    }
  }
}
