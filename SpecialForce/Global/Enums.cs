﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace SpecialForce
{
  #region Enumeration Declerations
  public enum CompetitionType
  {
    Dummy,
    FreeMatch,
    League,
    Cup,
    Elimination
  }

  public enum CompetitionState
  {
    Editing,
    Fixated,
    Finished
  }

  public enum DeviceLocation
  {
    Center,
    BlueBase,
    GreenBase
  }

  public enum WirelessState
  {
    Open,
    CenterNotWorking,
    BlueNotWorking,
    GreenNotWorking
  }

  public enum DeviceID
  {
    Unknown = -100,
    Bomb2 = -2,
    Bomb1 = -1,
    AllDevices = 0,
    KevlarBlue1 = 1,
    KevlarBlue2 = 2,
    KevlarBlue3 = 3,
    KevlarBlue4 = 4,
    KevlarBlue5 = 5,
    KevlarBlue6 = 6,
    KevlarBlue7 = 7,
    KevlarBlue8 = 8,
    KevlarBlue9 = 9,
    KevlarBlue10 = 10,
    KevlarGreen1 = 11,
    KevlarGreen2 = 12,
    KevlarGreen3 = 13,
    KevlarGreen4 = 14,
    KevlarGreen5 = 15,
    KevlarGreen6 = 16,
    KevlarGreen7 = 17,
    KevlarGreen8 = 18,
    KevlarGreen9 = 19,
    KevlarGreen10 = 20
  }

  public enum Commands
  {
    Unknown,
    Ping,

    State,
    RequestGameState,
    GameState,
    ConfigurePlayer,
    ConfigureParams,
    SendUpdate,
    StopUpdate,
    SendMessage,
    Frag,

    ConfigureBomb,
    BombUpdate,
    BombPlantBroadcast,
    PlaySound,

    CheatPlayer,
    CheatDie,
    CheatBomb
  }

  public enum Interactions
  {
    ConfigureKevlar,
    ConfigureBomb,
    TurnOn,
    CheckOn,
    CheckOff
  }

  public enum CommandStatus
  {
    Unknown,
    InQueue,
    Sent,
    Processed,
    TimedOut
  }

  public enum TeamType 
  { 
    Blue, 
    Green 
  }

  public enum GameState
  {
    Free,
    Reserved,
    InProgress,
    Finished,
    Aborted
  }

  public enum GameObject
  {
    NoObject,
    DeathMatch,
    Bombing
  }

  public enum WinnerType
  {
    NotPlayed = -1,
    NoWinner = -1,
    Blue = 0,
    Green = 1,
    Tie = 2,
  }

  public enum InvoiceType
  {
    UntypedPayment,
    BillWater,
    BillPower,
    BillGas,
    BillTax,
    BillSewer,
    BillPhone,
    UserGiftCharge,
    UserCreditCharge,
    UserCreditRefund,
    UserCreditUsage,
    Rent,
    CompetitionExpense,
    CompetitionAdmission,
    Clear
  }

  public enum InvoiceGroup
  {
    CupAdmission,
    LeagueAdmission,
    EliminationAdmission,
    UserCharges,
    CupExpenses,
    LeagueExpenses,
    EliminationExpenses,
    UserRefunds,
    GiftCharges,
    Rents,
    Bills,
    OthersExpenses
  }

  public enum RoundState
  {
    Pending,
    InProgress,
    Finished,
    Aborted
  }

  public enum Post
  {
    User,
    Operator,
    Administrator,
    God
  }

  public enum TreatType
  {
    BlackList,
    WhiteList,
    None
  }

  public enum CompareResult
  {
    Higher,
    Lower,
    Equal
  }

  public enum Gradients
  {
    Vertical,
    VerticalMirror
  }

  public enum RectangleEdgeFilter
  {
    None = 0,
    TopLeft = 1,
    TopRight = 2,
    BottomLeft = 4,
    BottomRight = 8,
    All = TopLeft | TopRight | BottomLeft | BottomRight
  }

  public enum EventTypes
  {
    SizeChanged,
    LocationChanged,
    FocusReceived,
    FocusLost,
    PaintBackGround,
    Paint,
    FontChanged,
    BackColorChanged,
    ForeColorChanged,
    VisibleChanged,
    EnableChanged,
    StyleChanged,
    MouseDown,
    MouseUp,
    MouseEnter,
    MouseLeave,
    KeyDown,
    KeyUp,
    TextChanged,
    RightToLeftChanged
  }

  public enum Alignment
  {
    TopLeft,
    TopMiddle,
    TopRight,
    MiddleLeft,
    MiddleMiddle,
    MiddleRight,
    BottomLeft,
    BottomMiddle,
    BottomRight
  }

  public enum BombState
  {
    WokenUp = 0,
    Disabled = 2,
    Enabled = 1,
    Planted = 3,
    Diffused = 4,
    Exploded = 5
  }

  public enum KevlarState
  {
    Working = 1,
    Disabled = 2,
    Dead = 3
  }

  public enum ExecutionMode
  {
    Wizard,
    Client,
    Kiosk,
    None
  }

  public enum UserNameState
  {
    Acceptable,
    InvalidCharacters,
    Empty,
    Duplicate
  }
  #endregion Enumeration Declerations

  #region Extensions
  static public class PostExtension
  {
    static public bool IsPrivileged(this Post post)
    {
      return post == Post.Administrator || post == Post.God;
    }

    static public bool IsGod(this Post post)
    {
      return post == Post.God;
    }

    static public bool IsAdmin(this Post post)
    {
      return post == Post.Administrator;
    }

    static public bool IsOperator(this Post post)
    {
      return post == Post.Operator;
    }

    static public bool IsUser(this Post post)
    {
      return post == Post.User;
    }
  }
  #endregion Extensions

  static class Enums
  {
    #region CompetitionType
    public static string ToString(CompetitionType t)
    {
      if (t == CompetitionType.Cup)
        return "کاپ";
      else if (t == CompetitionType.Elimination)
        return "حذفی";
      else if (t == CompetitionType.FreeMatch)
        return "عادی";
      else if (t == CompetitionType.League)
        return "لیگ";
      else if (t == CompetitionType.Dummy)
        return "تست";

      throw new Exception("(DB.Competition) no type given to convert to string");
    }

    public static CompetitionType ParseCompetitionType(string str)
    {
      if (str == "کاپ")
        return CompetitionType.Cup;
      else if (str == "حذفی")
        return CompetitionType.Elimination;
      else if (str == "عادی")
        return CompetitionType.FreeMatch;
      else if (str == "لیگ")
        return CompetitionType.League;
      else if (str == "تست")
        return CompetitionType.Dummy;

      throw new Exception("(DB.Competition) unknown competition type string: " + str);
    }
    #endregion CompetitionType


    #region CompetitionState
    static public string ToString(CompetitionState st)
    {
      if (st == CompetitionState.Editing)
        return "ثبت";
      else if (st == CompetitionState.Fixated)
        return "اجرا";
      else if (st == CompetitionState.Finished)
        return "تمام";

      throw new Exception("(DB.Competition) Unknown competition state: " + st.ToString());
    }

    static public CompetitionState ParseCompetitionState(string str)
    {
      if (str == "ثبت")
        return CompetitionState.Editing;
      else if (str == "اجرا")
        return CompetitionState.Fixated;
      else if (str == "تمام")
        return CompetitionState.Finished;

      throw new Exception("(DB.Competition) Unknown competition state: " + str);
    }
    #endregion CompetitionState


    #region DeviceLocation
    static public string ToString(DeviceLocation loc)
    {
      if (loc == DeviceLocation.BlueBase)
        return "Blue Base";
      else if (loc == DeviceLocation.Center)
        return "Center";
      else
        return "Green Base";
    }
    #endregion DeviceLocation


    #region GameObject
    static public string ToString(GameObject o)
    {
      if (o == GameObject.DeathMatch)
        return "عادی";
      else if (o == GameObject.Bombing)
        return "بمب گذاری";
      else if (o == GameObject.NoObject)
        return "بدون هدف";

      throw new Exception("(DB.Game) game object not known");
    }

    static public GameObject ParseGameObject(string o)
    {
      if (o == "عادی")
        return GameObject.DeathMatch;
      else if (o == "بمب گذاری")
        return GameObject.Bombing;
      else if (o == "بدون هدف")
        return GameObject.NoObject;

      throw new Exception("(DB.Game) game object not known: " + o);
    }
    #endregion GameObject


    #region GameState
    static public string ToString(GameState m)
    {
      if (m == GameState.Free)
        return "آزاد";
      else if (m == GameState.Reserved)
        return "رزرو شده";
      else if (m == GameState.InProgress)
        return "در حال اجرا";
      else if (m == GameState.Finished)
        return "تمام شده";
      else if (m == GameState.Aborted)
        return "قطع شده";

      throw new Exception("(DB.Game) match state not known: " + m);
    }

    static public GameState ParseGameState(string m)
    {
      if (m == "آزاد")
        return GameState.Free;
      else if (m == "رزرو شده")
        return GameState.Reserved;
      else if (m == "در حال اجرا")
        return GameState.InProgress;
      else if (m == "تمام شده")
        return GameState.Finished;
      else if (m == "قطع شده")
        return GameState.Aborted;

      throw new Exception("(DB.Game) match state not known: " + m);
    }
    #endregion GameState


    #region FriendlyFire
    static public int IndexFromFriendlyFire(bool ff)
    {
      return ff ? 1 : 0;
    }

    static public bool FriendlyFireFromString(string ff)
    {
      return ff == "بله" ? false : true;
    }
    #endregion FriendlyFire


    #region InvoiceType
    static public string ToString(InvoiceType type)
    {
      if (type == InvoiceType.BillWater)
        return "قبض آب";
      else if (type == InvoiceType.BillPower)
        return "قبض برق";
      else if (type == InvoiceType.BillGas)
        return "قبض گاز";
      else if (type == InvoiceType.BillPhone)
        return "قبض تلفن";
      else if (type == InvoiceType.BillTax)
        return "قبض مالیات";
      else if (type == InvoiceType.BillSewer)
        return "قبض فاضلاب";
      else if (type == InvoiceType.Rent)
        return "اجاره";
      else if (type == InvoiceType.UserGiftCharge)
        return "شارژ هدیه";
      else if (type == InvoiceType.UserCreditCharge)
        return "شارژ حساب بازیکن";
      else if (type == InvoiceType.UserCreditRefund)
        return "عودت شارژ بازیکن";
      else if (type == InvoiceType.UserCreditUsage)
        return "بازی با شارژ";
      else if (type == InvoiceType.Clear)
        return "برداشت";
      else if (type == InvoiceType.CompetitionExpense)
        return "هزینه مسابقه";
      else if (type == InvoiceType.CompetitionAdmission)
        return "ورودیه";
      else
        return "";
    }

    static public InvoiceType ParseInvoiceType(string str)
    {
      if (str == "قبض آب")
        return InvoiceType.BillWater;
      else if (str == "قبض برق")
        return InvoiceType.BillPower;
      else if (str == "قبض گاز")
        return InvoiceType.BillGas;
      else if (str == "قبض تلفن")
        return InvoiceType.BillPhone;
      else if (str == "قبض مالیات")
        return InvoiceType.BillTax;
      else if (str == "قبض فاضلاب")
        return InvoiceType.BillSewer;
      else if (str == "اجاره")
        return InvoiceType.Rent;
      else if (str == "شارژ هدیه")
        return InvoiceType.UserGiftCharge;
      else if (str == "شارژ حساب بازیکن")
        return InvoiceType.UserCreditCharge;
      else if (str == "عودت شارژ بازیکن")
        return InvoiceType.UserCreditRefund;
      else if (str == "بازی با شارژ")
        return InvoiceType.UserCreditUsage;
      else if (str == "برداشت")
        return InvoiceType.Clear;
      else if (str == "هزینه مسابقه")
        return InvoiceType.CompetitionExpense;
      else if (str == "ورودیه")
        return InvoiceType.CompetitionAdmission;
      else
        return InvoiceType.UntypedPayment;
    }
    #endregion InvoiceType


    #region RoundState
    static public string ToString(RoundState rs)
    {
      if (rs == RoundState.Finished)
        return "تمام شده";
      else if (rs == RoundState.Aborted)
        return "متوقف شده";
      else if (rs == RoundState.InProgress)
        return "در حال اجرا";
      else if (rs == RoundState.Pending)
        return "در انتظار";

      throw new Exception("(DB.Round) unknown round state" + rs.ToString());
    }

    static public RoundState ParseRoundState(string str)
    {
      if (str == "تمام شده")
        return RoundState.Finished;
      else if (str == "متوقف شده")
        return RoundState.Aborted;
      else if (str == "در حال اجرا")
        return RoundState.InProgress;
      else if (str == "در انتظار")
        return RoundState.Pending;

      throw new Exception("(DB.Round) unknown round state" + str);
    }
    #endregion RoundState


    #region Post
    static public string ToString(Post post)
    {
      if (post == Post.User)
        return "User";
      else if (post == Post.Operator)
        return "Operator";
      else if (post == Post.Administrator)
        return "Administrator";
      else if (post == Post.God)
        return "God";
      
      throw new Exception("(DB.User) unknown post: " + post.ToString());
    }

    static public Post ParsePost(string str)
    {
      if (str == "User")
        return Post.User;
      else if (str == "Operator")
        return Post.Operator;
      else if (str == "Administrator")
        return Post.Administrator;
      else if (str == "God")
        return Post.God;

      throw new Exception("(DB.User) unknown post: " + str);
    }

    static public string ToPersianString(Post post)
    {
      if (post == Post.User)
        return "کاربر";
      else if (post == Post.Operator)
        return "اپراتور";
      else if (post == Post.Administrator)
        return "مدیر";
      else if (post == Post.God)
        return "پروردگار";

      throw new Exception("(DB.User) unknown post: " + post.ToString());
    }

    static public Post ParsePersianPost(string post)
    {
      if (post == "کاربر")
        return Post.User;
      else if (post == "اپراتور")
        return Post.Operator;
      else if (post == "مدیر")
        return Post.Administrator;
      else if (post == "پروردگار")
        return Post.God;

      throw new Exception("(DB.User) unknown post: " + post.ToString());
    }
    #endregion Post


    #region TreatType
    static public string ToString(TreatType type)
    {
      if (type == TreatType.BlackList)
        return "BlackList";
      else if (type == TreatType.WhiteList)
        return "WhiteList";

      throw new Exception("(DB.UserSpecialTreat) Unknown special treat type: " + type.ToString());
    }

    static public TreatType ParseTreatType(string str)
    {
      if (str == "BlackList")
        return TreatType.BlackList;
      else if (str == "WhiteList")
        return TreatType.WhiteList;

      throw new Exception("(DB.UserSpecialTreat) Unknown special treat type: " + str);
    }
    #endregion TreatType


    #region InvoiceGroup
    static private string ToString(InvoiceGroup ig)
    {
      switch(ig)
      {
        case InvoiceGroup.CupAdmission: return "کاپ ها";
        case InvoiceGroup.LeagueAdmission: return "لیگ ها";
        case InvoiceGroup.EliminationAdmission: return "حذفی ها";
        case InvoiceGroup.UserCharges: return "شارژ کاربران";
        case InvoiceGroup.CupExpenses: return "کاپ ها";
        case InvoiceGroup.LeagueExpenses: return "لیگ ها";
        case InvoiceGroup.EliminationExpenses: return "حذفی ها";
        case InvoiceGroup.UserRefunds: return "عودت شارژ";
        case InvoiceGroup.GiftCharges: return "شارژ هدیه";
        case InvoiceGroup.Rents: return "اجاره ها";
        case InvoiceGroup.Bills: return "قبض ها";
        case InvoiceGroup.OthersExpenses: return "هزینه های متفرقه";

        default:
          throw new Exception("(DB.InvoiceGroup) Unknown invoice group: " + ig.ToString());
      }
    }

    static public List<string> ToStringList(SortedSet<InvoiceGroup> invoices, int width, Font font)
    {
      string strIncome = "";
      if (invoices.Contains(InvoiceGroup.CupAdmission) && invoices.Contains(InvoiceGroup.EliminationAdmission) && invoices.Contains(InvoiceGroup.LeagueAdmission) && invoices.Contains(InvoiceGroup.UserCharges))
        strIncome = "همه درآمد ها";
      else
      {
        if (invoices.Contains(InvoiceGroup.CupAdmission))
          strIncome = AddToken(strIncome, ToString(InvoiceGroup.CupAdmission));
        if (invoices.Contains(InvoiceGroup.EliminationAdmission))
          strIncome = AddToken(strIncome, ToString(InvoiceGroup.EliminationAdmission));
        if (invoices.Contains(InvoiceGroup.LeagueAdmission))
          strIncome = AddToken(strIncome, ToString(InvoiceGroup.LeagueAdmission));
        if (invoices.Contains(InvoiceGroup.UserCharges))
          strIncome = AddToken(strIncome, ToString(InvoiceGroup.UserCharges));
      }


      string strExpenses = "";
      if (invoices.Contains(InvoiceGroup.CupExpenses) && invoices.Contains(InvoiceGroup.LeagueExpenses) && invoices.Contains(InvoiceGroup.EliminationExpenses) && invoices.Contains(InvoiceGroup.UserRefunds) && invoices.Contains(InvoiceGroup.GiftCharges) && invoices.Contains(InvoiceGroup.Rents) && invoices.Contains(InvoiceGroup.Bills) && invoices.Contains(InvoiceGroup.OthersExpenses))
        strExpenses = "همه هزینه ها";
      else
      {
        if(invoices.Contains(InvoiceGroup.CupExpenses))
          strExpenses = AddToken(strExpenses, ToString(InvoiceGroup.CupExpenses));
        if(invoices.Contains(InvoiceGroup.LeagueExpenses))
          strExpenses = AddToken(strExpenses, ToString(InvoiceGroup.LeagueExpenses));
        if(invoices.Contains(InvoiceGroup.EliminationExpenses))
          strExpenses = AddToken(strExpenses, ToString(InvoiceGroup.EliminationExpenses));
        if(invoices.Contains(InvoiceGroup.UserRefunds))
          strExpenses = AddToken(strExpenses, ToString(InvoiceGroup.UserRefunds));
        if(invoices.Contains(InvoiceGroup.GiftCharges))
          strExpenses = AddToken(strExpenses, ToString(InvoiceGroup.GiftCharges));
        if(invoices.Contains(InvoiceGroup.Rents))
          strExpenses = AddToken(strExpenses, ToString(InvoiceGroup.Rents));
        if(invoices.Contains(InvoiceGroup.Bills))
          strExpenses = AddToken(strExpenses, ToString(InvoiceGroup.Bills));
        if(invoices.Contains(InvoiceGroup.OthersExpenses))
          strExpenses = AddToken(strExpenses, ToString(InvoiceGroup.OthersExpenses));
      }

      List<string> result = new List<string>();
      if (strIncome.Length > 0)
        result.AddRange(TextRenderer.FitStringInLines("درآمد ها: " + strIncome, width, font));
      if (strExpenses.Length > 0)
        result.AddRange(TextRenderer.FitStringInLines("هزینه ها: " + strExpenses, width, font));
      return result;
    }
    #endregion InvoiceGroup


    #region String Conversion Utilities
    static public string AddToken(string source, string token)
    {
      if (source.Length == 0)
        return token;
      else
        return source + "، " + token;
    }
    #endregion String Conversion Utilities


    #region BombState
    static public string ToString(BombState st)
    {
      if (st == BombState.WokenUp)
        return "WokenUp";
      else if (st == BombState.Disabled)
        return "Disabled";
      else if (st == BombState.Enabled)
        return "Enabled";
      else if (st == BombState.Planted)
        return "Planted";
      else if (st == BombState.Diffused)
        return "Diffused";
      else if (st == BombState.Exploded)
        return "Exploded";

      throw new Exception("(ControlTower.Bomb) unknown bomb state: " + st.ToString());
    }

    static public BombState BombStateFromString(string st)
    {
      if (st == "WokenUp")
        return BombState.WokenUp;
      else if (st == "Disabled")
        return BombState.Disabled;
      else if (st == "Enabled")
        return BombState.Enabled;
      else if (st == "Planted")
        return BombState.Planted;
      else if (st == "Diffused")
        return BombState.Diffused;
      else if (st == "Exploded")
        return BombState.Exploded;

      throw new Exception("(ControlTower.Bomb) unknown bomb state: " + st);
    }
    #endregion BombState


    #region KevlarState
    static public string ToString(KevlarState ks)
    {
      if (ks == KevlarState.Dead)
        return "مرده";
      else if (ks == KevlarState.Disabled)
        return "غیر فعال";
      else if (ks == KevlarState.Working)
        return "فعال";

      throw new Exception("(Kevlar) unknown state: " + ks.ToString());
    }
    #endregion Utility methods
  }
}
