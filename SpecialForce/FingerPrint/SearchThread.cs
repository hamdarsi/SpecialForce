﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SpecialForce
{
  public class SearchThread
  {
    private Thread mThread;
    private bool mFinished;
    private byte[] mFingerPrint;
    private int mUpdateInterval;
    private SearchUpdateEventArgs mSearchUpdateArgs = null;
    private SearchFinishEventArgs mSearchFinishArgs = null;
    private System.Windows.Forms.Timer mTimer;

    public int UpdateInterval
    {
      get { return mUpdateInterval; }
      set { mUpdateInterval = value; }
    }

    public event EventHandler<SearchUpdateEventArgs> OnSearchUpdate;
    public event EventHandler<SearchFinishEventArgs> OnSearchFinish;

    public SearchThread(byte[] data)
    {
      mFingerPrint = data;
      mThread = null;
      mFinished = false;
      mTimer = null;
      mUpdateInterval = 500;
    }

    public void Start()
    {
      mFinished = false;
      mThread = new Thread(new ThreadStart(BeginSearch));
      mThread.Start();
    }

    public void Stop()
    {
      Application.UseWaitCursor = true;
      FingerPrint.StopSearch();
      while(!mFinished)
        Application.DoEvents();
      Application.UseWaitCursor = false;
    }

    public void BeginSearch()
    {
      // prepare search update
      mTimer = new System.Windows.Forms.Timer();
      mTimer.Interval = mUpdateInterval;
      mTimer.Tick += TimerTick;
      mTimer.Start();

      // perform search
      int uid = FingerPrint.Search(mFingerPrint);
      mTimer.Stop();
      mFinished = true;

      // inform about search result
      mSearchFinishArgs = new SearchFinishEventArgs(uid);
      Session.Synchronize(new Action(BroadcastFinishEvent));
    }

    private void BroadcastUpdateEvent()
    {
      EventHandler<SearchUpdateEventArgs> ev = OnSearchUpdate;
      if (ev != null)
        ev(this, mSearchUpdateArgs);
    }

    private void BroadcastFinishEvent()
    {
      EventHandler<SearchFinishEventArgs> ev = OnSearchFinish;
      if (ev != null)
        ev(this, mSearchFinishArgs);
    }

    private void TimerTick(object sender, EventArgs e)
    {
      int cnt, index;
      FingerPrint.GetSearchState(out index, out cnt);
      mSearchUpdateArgs = new SearchUpdateEventArgs(index, cnt);
      Session.Synchronize(new Action(BroadcastUpdateEvent));
    }
  }
}
