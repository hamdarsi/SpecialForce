﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace SpecialForce
{
  public class FingerDevice
  {
    private FingerPrint.DeviceProperties mProperties;
    private int mIndex;
    private byte [] mFingerData;
    private Bitmap mFingerImage;

    public int Width
    {
      get
      {
        if (!FingerPrint.IsDeviceOpen(mIndex))
          throw new Exception("FingerPrint device is not open");

        return mProperties.Width;
      }
    }

    public int Height
    {
      get
      {
        if (!FingerPrint.IsDeviceOpen(mIndex))
          throw new Exception("FingerPrint device is not open");

        return mProperties.Height;
      }
    }

    public int Brightness
    {
      get
      {
        if (!FingerPrint.IsDeviceOpen(mIndex))
          throw new Exception("FingerPrint device is not open");

        return mProperties.Brightness;
      }
    }

    public int Contrast
    {
      get
      {
        if (!FingerPrint.IsDeviceOpen(mIndex))
          throw new Exception("FingerPrint device is not open");

        return mProperties.Contrast;
      }
    }

    public int Gain
    {
      get
      {
        if (!FingerPrint.IsDeviceOpen(mIndex))
          throw new Exception("FingerPrint device is not open");

        return mProperties.Gain;
      }
    }

    public byte [] FingerData
    {
      get { return mFingerData; }
    }

    public Bitmap FingerImage
    {
      get { return mFingerImage; }
    }

    public FingerDevice(int index)
    {
      mIndex = index;
      mFingerData = null;
      mFingerImage = null;
      if(!FingerPrint.GetDeviceProperties(mIndex, ref mProperties))
        throw new Exception("Could not get device properties");
    }

    public void Close()
    {
      if (FingerPrint.IsDeviceOpen(mIndex))
        FingerPrint.CloseDevice(mIndex);
    }

    public FingerPrint.CaptureResult Capture()
    {
      if (!FingerPrint.IsDeviceOpen(mIndex))
        throw new Exception("FingerPrint device is not open");

      FingerPrint.SetCaptureOutputOptions(IntPtr.Zero, Color.Black, Color.White);
      return FingerPrint.Capture(mIndex, out mFingerData, out mFingerImage);
    }

    public FingerPrint.CaptureResult Capture(IntPtr handle, Color back, Color fore)
    {
      if (!FingerPrint.IsDeviceOpen(mIndex))
        throw new Exception("FingerPrint device is not open");

      FingerPrint.SetCaptureOutputOptions(handle, back, fore);
      return FingerPrint.Capture(mIndex, out mFingerData, out mFingerImage);
    }
  }
}
