﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce
{
  public class CaptureThread
  {
    public enum CaptureState
    {
      Sleeping,
      Capturing,
      Aborting
    }

    private Thread mThread = null;
    private FingerDevice mDevice = null;
    private CaptureEventArgs mCapturedData = null;

    private Color mBackColor;
    public Color BackColor
    {
      get { return mBackColor; }
      set { mBackColor = value; }
    }

    private Color mForeColor;
    public Color ForeColor
    {
      get { return mForeColor; }
      set { mForeColor = value; }
    }

    private IntPtr mHandle;
    public IntPtr Output
    {
      set { mHandle = value; }
    }

    private CaptureState mState;
    public CaptureState State
    {
      get { return mState; }
    }

    private int mIndex;
    public int DeviceID
    {
      get { return mIndex; }
    }

    
    public event EventHandler<CaptureEventArgs> OnCapture;

    public CaptureThread(int index)
    {
      mThread = null;
      mIndex = index;
      mDevice = null;
      mHandle = IntPtr.Zero;
      mBackColor = Color.White;
      mForeColor = Color.Blue;
      mState = CaptureState.Sleeping;
    }

    private void BroadcastEvent()
    {
      EventHandler<CaptureEventArgs> ev = OnCapture;
      if (ev != null)
        ev(mDevice, mCapturedData);
    }

    public void Start()
    {
      // do not start the thread if it is already started
      if (mState != CaptureState.Sleeping)
        return;

      mDevice = FingerPrint.OpenDevice(mIndex);
      if (mDevice == null)
        throw new Exception("Could not open device with index" + mIndex.ToString());

      mThread = new Thread(new ThreadStart(BeginCapture));
      mThread.Name = "FingerPrint Capture";
      mThread.Start();
    }

    private void BeginCapture()
    {
      mState = CaptureState.Capturing;
      while (mState == CaptureState.Capturing)
      {
        if (mDevice.Capture(mHandle, mBackColor, mForeColor) == FingerPrint.CaptureResult.CaptureDone)
        {
          // Save the captured data
          mCapturedData = new CaptureEventArgs(mDevice.FingerData, mDevice.FingerImage);
          Session.Synchronize(new Action(BroadcastEvent));
        }
      }

      mState = CaptureState.Sleeping;
    }

    public void Stop()
    {
      if (mState == CaptureState.Capturing)
        mState = CaptureState.Aborting;

      while (mState != CaptureState.Sleeping)
        Application.DoEvents();

      if (mDevice != null)
      {
        mDevice.Close();
        mDevice = null;
      }
    }
  }
}
