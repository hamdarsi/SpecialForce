﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;

namespace SpecialForce
{
  public enum FingerPrintState
  {
    Deactive,
    FakeActive,
    Active
  };

  public static class FingerPrint
  {
    #region Enumerations and Data Structures
    public enum OpenResult
    {
      NotConfigured,
      Success
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct CaptureData
    {
      public int ImageWidth;
      public int ImageHeight;
      public int ImageSize;
      public IntPtr Image;

      public int DataSize;
      public IntPtr Data;
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct DeviceInfo
    {
      public System.Int32 DeviceID;

      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
      public string DeviceName;

      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
      public string DeviceDescription;

      public bool IsOpen;
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct DeviceProperties
    {
      public int Width;
      public int Height;
      public int Brightness;
      public int Contrast;
      public int Gain;
    };

    public enum CaptureResult
    {
      CaptureDone,
      CaptureTimedOut,
      NotCaptured
    };
    #endregion Enumerations and Data Structures


    #region External DLL routines
    [DllImport(@"FingerPrintReader.dll", EntryPoint = "getDeviceManagerErrorString", CharSet = CharSet.Auto)]
    private static extern IntPtr getDeviceManagerErrorString();

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "getFastSearchDBErrorString", CharSet = CharSet.Auto)]
    private static extern IntPtr getFastSearchDBErrorString();

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "isDeviceManagerInitialized", CharSet = CharSet.Auto)]
    [return:MarshalAs(UnmanagedType.I1)]
    private static extern bool isDeviceManagerInitialized();

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "isFastSearchDBInitialized", CharSet = CharSet.Auto)]
    [return: MarshalAs(UnmanagedType.I1)]
    private static extern bool isFastSearchDBInitialized();

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "getMajorVersion", CharSet = CharSet.Auto)]
    private static extern int getMajorVersion();

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "getMinorVersion", CharSet = CharSet.Auto)]
    private static extern int getMinorVersion();

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "getDeviceCount", CharSet = CharSet.Auto)]
    private static extern int getDeviceCount();

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "getDeviceInfo", CharSet = CharSet.Auto)]
    [return: MarshalAs(UnmanagedType.I1)]
    private static extern bool getDeviceInfo(int index, ref DeviceInfo inf);

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "openDeviceManager", CharSet = CharSet.Auto)]
    private static extern void openDeviceManager();

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "closeDeviceManager", CharSet = CharSet.Auto)]
    private static extern void closeDeviceManager();

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "openFastSearchDB", CharSet = CharSet.Auto)]
    private static extern void openFastSearchDB();

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "closeFastSearchDB", CharSet = CharSet.Auto)]
    private static extern void closeFingerPrintDB();

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "isDeviceOpen", CharSet = CharSet.Auto)]
    [return: MarshalAs(UnmanagedType.I1)]
    private static extern bool isDeviceOpen(int index);

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "getDeviceProperties", CharSet = CharSet.Auto)]
    [return: MarshalAs(UnmanagedType.I1)]
    private static extern bool getDeviceProperties(int index, ref DeviceProperties props);

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "openDevice", CharSet = CharSet.Auto)]
    private static extern bool openDevice(int index);

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "closeDevice", CharSet = CharSet.Auto)]
    [return: MarshalAs(UnmanagedType.I1)]
    private static extern bool closeDevice(int index);

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "setDeviceOutputOptions", CharSet = CharSet.Auto)]
    private static extern void setDeviceOutputOptions(IntPtr hwnd, int br, int bg, int bb, int fr, int fg, int fb);

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "capture", CharSet = CharSet.Auto)]
    private static extern CaptureResult capture(int index, ref CaptureData finger);

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "addFingerPrintToDB", CharSet = CharSet.Auto)]
    private static extern void addFingerPrintToDB(byte [] data, int size, int user);

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "removeFingerPrintFromDB", CharSet = CharSet.Auto)]
    private static extern void removeFingerPrintFromDB(int user);

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "clearFastSearchDB", CharSet = CharSet.Auto)]
    private static extern void clearFingerPrintDB();

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "search", CharSet = CharSet.Auto)]
    private static extern int search(byte [] data, int size);

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "getSearchState", CharSet = CharSet.Auto)]
    private static extern void getSearchState(out int index, out int count);

    [DllImport(@"FingerPrintReader.dll", EntryPoint = "stopSearch", CharSet = CharSet.Auto)]
    private static extern void stopSearch();
    #endregion External DLL routines


    #region Data and Instances
    static private FingerPrintState mState = FingerPrintState.Deactive;
    static private FingerDevice mNullDevice = null;
    static private CaptureThread mNullDeviceThread = null;
    static private List<CaptureThread> mDeviceThreads = new List<CaptureThread>();
    #endregion Data and Instances


    #region Properties
    /// <summary>
    /// Current state of fingerprint system.
    /// Active: FingerPrint device is externally located, attached and turned on.
    /// FakeActive: FingerPrint device is not externally located but is internally emulated.
    /// Deactive: No attempt has been done to start the system yet.
    /// </summary>
    static public FingerPrintState State
    {
      get { return mState; }
    }

    /// <summary>
    /// Major version of the FingerPrint driver
    /// </summary>
    static public int MajorVersion
    {
      get 
      {
        if (mState == FingerPrintState.Active)
          return getMajorVersion();
        else if (mState == FingerPrintState.FakeActive)
          return 1;
        else
          return -1;
      }
    }

    /// <summary>
    /// Minor version of the FingerPrint driver
    /// </summary>
    static public int MinorVersion
    {
      get
      {
        if (mState == FingerPrintState.Active)
          return getMinorVersion();
        else if (mState == FingerPrintState.FakeActive)
          return 1;
        else
          return -1;
      }
    }

    /// <summary>
    /// Error string returned by the device manager module.
    /// </summary>
    static public string DeviceManagerErrorString
    {
      get 
      {
        if (mState == FingerPrintState.Active)
          return Marshal.PtrToStringAnsi(getDeviceManagerErrorString());
        else 
          return "";
      }
    }

    /// <summary>
    /// Error string returned by the fast search system
    /// </summary>
    static public string FastSearchDBErrorString
    {
      get 
      {
        if (mState == FingerPrintState.Active)
          return Marshal.PtrToStringAnsi(getFastSearchDBErrorString());
        else 
          return "";
      }
    }

    /// <summary>
    /// FingerPrint system error string
    /// </summary>
    static public string ErrorString
    {
      get 
      {
        string e1 = DeviceManagerErrorString;
        string e2 = FastSearchDBErrorString;
        if (e1 == "")
          return e2;
        else if (e2 == "")
          return e1;
        else
          return e1 + "\n" + e2;
      }
    }

    /// <summary>
    /// Number of attached devices
    /// </summary>
    static public int DeviceCount
    {
      get 
      {
        if (mState == FingerPrintState.Active)
          return getDeviceCount();
        else if (mState == FingerPrintState.FakeActive)
          return 1;
        else
          return 0;
      }
    }
    #endregion Properties


    #region Initialization
    /// <summary>
    /// Opens the finger print system.
    /// </summary>
    /// <returns></returns>
    static public OpenResult OpenSystem()
    {
      // Not configured?
      if (!Properties.Settings.Default.FingerPrintModuleConfigured)
        return OpenResult.NotConfigured;

      // Already open?
      if (mState != FingerPrintState.Deactive)
        CloseSystem();

      // Configured, but not desired to enable.
      // Application is not dependant on the finger print
      // a fake activation is required only.
      if (!Properties.Settings.Default.FingerPrintModuleEnabled)
        return FakeActivation("FingerPrint not enabled in application settings.", false);

      // Actual activation.
      // 1: Device manager:
      openDeviceManager();
      if (!isDeviceManagerInitialized())
        return FakeActivation(DeviceManagerErrorString, true);

      // 2: Fast Search DB
      openFastSearchDB();
      if (!isFastSearchDBInitialized())
      {
        CloseDeviceManager();
        return FakeActivation(FastSearchDBErrorString, true);
      }

      // 3: Update Module State
      mState = FingerPrintState.Active;

      // 3: Load All Finger Prints
      LoadFingerPrints();

      // 4: Start A Real Capture Thread For The First Device
      OpenDeviceAsThread(0).Start();

      // Done.
      return OpenResult.Success;
    }

    static private OpenResult FakeActivation(string reason, bool fallback)
    {
      mState = FingerPrintState.FakeActive;

      if (mNullDevice == null)
      {
        mNullDevice = new FingerDevice(-1);
        mNullDeviceThread = new CaptureThread(-1);
        mNullDeviceThread.Start();
      }

      // Log it
      Logger.Log("(FingerPrint) Activated in fake mode.");
      Logger.Log("(FingerPrint) Reason: " + reason);

      // fall back mode
      if (fallback)
      {
        string str = "دستگاه اثر انگشت فعال نمی شود";
        str += Environment.NewLine;
        str += "برنامه بدون ماژول اثر انگشت ادامه خواهد داد.";
        Session.ShowMessage(str);
        Properties.Settings.Default.FingerPrintModuleEnabled = false;
      }

      return OpenResult.Success;
    }
    #endregion Initialization


    #region Finalization
    /// <summary>
    /// Closes finger print system.
    /// </summary>
    static public void CloseSystem()
    {
      if (mState == FingerPrintState.FakeActive)
        mNullDeviceThread.Stop();
      else if (mState == FingerPrintState.Active)
      {
        // Fast search db
        if (isFastSearchDBInitialized())
        {
          clearFingerPrintDB();
          closeFingerPrintDB();
        }

        // Device manager
        if (isDeviceManagerInitialized())
          CloseDeviceManager();
      }

      mState = FingerPrintState.Deactive;
    }

    /// <summary>
    /// Closes device manager and removes all 
    /// currently running devices.
    /// </summary>
    static private void CloseDeviceManager()
    {
      for (int i = 0; i < mDeviceThreads.Count; ++i)
        mDeviceThreads[i].Stop();
      mDeviceThreads.Clear();

      closeDeviceManager();
    }
    #endregion Finalization


    #region Utilities
    /// <summary>
    /// This method returns the thread handling capture from
    /// the given device
    /// </summary>
    /// <param name="index">Device index</param>
    /// <returns>Manager thread of the requested device</returns>
    static public CaptureThread getDeviceThread(int index)
    {
      if (mState == FingerPrintState.FakeActive)
        return mNullDeviceThread;

      if (index < 0 || index > mDeviceThreads.Count)
      {
        Session.ShowError("دستگاه مورد نظر فعال نیست.");
        return null;
      }

      return mDeviceThreads[index];
    }

    /// <summary>
    /// This method returns the device information
    /// </summary>
    /// <param name="index">Index of the device</param>
    /// <returns>Device information of the requested device</returns>
    static public DeviceInfo GetDeviceInfo(int index)
    {
      DeviceInfo inf = new DeviceInfo();
      inf.DeviceID = -1;

      if (mState == FingerPrintState.Active)
        getDeviceInfo(index, ref inf);
      else
      {
        inf.DeviceDescription = "Null Device";
        inf.DeviceName = "Virdi Null Device";
        inf.IsOpen = true;
      }

      return inf;
    }

    /// <summary>
    /// This method opens the requested device
    /// </summary>
    /// <param name="index">Device index to open</param>
    /// <returns>The device instance</returns>
    static public FingerDevice OpenDevice(int index)
    {
      // System not boot up?
      if (mState == FingerPrintState.Deactive)
        return null;
      
      // Fake activated?
      if (mState == FingerPrintState.FakeActive)
        return mNullDevice;

      // Activated? open the device.
      if (!openDevice(index))
          return null;

      return new FingerDevice(index);
    }

    /// <summary>
    /// Opens the given device, wraps a thread around
    /// it and returns the thread, but will not start.
    /// </summary>
    /// <param name="index">Device index to open</param>
    /// <returns>Managing thread for the requested device</returns>
    static public CaptureThread OpenDeviceAsThread(int index)
    {
      // System not booted?
      if (mState == FingerPrintState.Deactive)
        return null;

      // Fake start?
      if(mState == FingerPrintState.FakeActive)
        return mNullDeviceThread;

      // Already started the thread?
      for (int i = 0; i < mDeviceThreads.Count; ++i)
        if (mDeviceThreads[i].DeviceID == index)
          return mDeviceThreads[i];

      // Create a new thread for the device.
      CaptureThread thread = new CaptureThread(index);
      mDeviceThreads.Add(thread);
      return thread;
    }

    /// <summary>
    /// Close the requested device.
    /// </summary>
    /// <param name="index">The device index to close</param>
    /// <returns>True if device actually closed.</returns>
    static public bool CloseDevice(int index)
    {
      if (mState == FingerPrintState.Deactive)
        return false;
      else if (mState == FingerPrintState.FakeActive)
        return true;
      else
        return closeDevice(index);
    }

    /// <summary>
    /// Closes the given device thread
    /// </summary>
    /// <param name="index">The device's index</param>
    /// <returns>True on thread close, false otherwise</returns>
    static public bool CloseDeviceThread(int index)
    {
      if (mState == FingerPrintState.Deactive)
        return false;

      if (mState == FingerPrintState.Deactive)
        return true;

      for (int i = 0; i < mDeviceThreads.Count; ++i)
        if (mDeviceThreads[i].DeviceID == index)
        {
          mDeviceThreads[i].Stop();
          mDeviceThreads.RemoveAt(i);
          return true;
        }

      return false;
    }

    /// <summary>
    /// This method checks whether the given device is open or not
    /// </summary>
    /// <param name="index">The device index to check</param>
    /// <returns>True if the device is open, false otherwise</returns>
    static public bool IsDeviceOpen(int index)
    {
      if (mState == FingerPrintState.Deactive)
        return false;
      else if (mState == FingerPrintState.FakeActive)
        return true;
      else
        return isDeviceOpen(index);
    }

    /// <summary>
    /// This method finds out the properties for the given device
    /// </summary>
    /// <param name="index">Device index to check out</param>
    /// <param name="props">Properties of the given device</param>
    /// <returns>True on successfull property retreival, false otherwise</returns>
    static public bool GetDeviceProperties(int index, ref DeviceProperties props)
    {
      if (mState == FingerPrintState.Deactive)
        return false;

      if (mState == FingerPrintState.Active)
        return getDeviceProperties(index, ref props);

      // Fake activated
      props.Brightness = 255;
      props.Contrast = 0;
      props.Height = 320;
      props.Width = 240;
      props.Gain = 1;
      return true;
    }

    /// <summary>
    /// When finger print state is updated in settings, it 
    /// shall updated in finger print system too. This method
    /// makes sure of it. Reconfigures fingerprint system if
    /// needed.
    /// </summary>
    static public void ReLoad()
    {
      CloseSystem();
      OpenSystem();
    }
    #endregion Utilities


    #region Capture Procudures
    /// <summary>
    /// Sets the output options for all devices!
    /// </summary>
    /// <param name="handle">Output window handle</param>
    /// <param name="back">Back color</param>
    /// <param name="fore">Finger print's color</param>
    static public void SetCaptureOutputOptions(IntPtr handle, Color back, Color fore)
    {
      if(mState == FingerPrintState.Active)
        setDeviceOutputOptions(handle, back.R, back.G, back.B, fore.R, fore.G, fore.B);
    }

    /// <summary>
    /// Captures from the device
    /// </summary>
    /// <param name="index">Device index</param>
    /// <param name="data">Captured authentication code</param>
    /// <param name="image">Captured finerprint image</param>
    /// <returns>Capture result for the capture</returns>
    static public CaptureResult Capture(int index, out byte [] data, out Bitmap image)
    {
      data = null;
      image = null;
      if (mState == FingerPrintState.Deactive)
        return CaptureResult.NotCaptured;

      if (mState == FingerPrintState.FakeActive)
      {
        System.Threading.Thread.Sleep(200);
        return CaptureResult.CaptureTimedOut;
      }

      // Actual capture!
      CaptureData dt = new CaptureData();
      CaptureResult res = capture(index, ref dt);

      if (res == CaptureResult.CaptureDone)
      {
        byte amount, rg;
        image = new Bitmap(dt.ImageWidth, dt.ImageHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
        int i;

        data = new byte[dt.DataSize];
        Marshal.Copy(dt.Data, data, 0, dt.DataSize);

        byte [] temp = new byte[dt.ImageSize];
        byte [] rgbvalues = new byte[dt.ImageSize * 3];
        Marshal.Copy(dt.Image, temp, 0, dt.ImageSize);
        Rectangle rect = new Rectangle(0, 0, image.Width, image.Height);
        System.Drawing.Imaging.BitmapData bmpData = image.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, image.PixelFormat);
        IntPtr ptr = bmpData.Scan0;

        for (int y = 0; y < dt.ImageHeight; ++y)
          for (int x = 0; x < dt.ImageWidth; ++x)
          {
            i = y * dt.ImageWidth + x;
            amount = temp[i];
            rg = (byte)(amount / 255.0 * amount);
            if (amount < 220)
              amount = 220;

            i *= 3;
            rgbvalues[i + 2] = rg;
            rgbvalues[i + 1] = rg;
            rgbvalues[i + 0] = amount;
          }

        Marshal.Copy(rgbvalues, 0, ptr, dt.ImageSize * 3);
        image.UnlockBits(bmpData);
      }

      return res;
    }
    #endregion Capture Procudures


    #region Finger Print Fast Search
    /// <summary>
    /// This method adds the given fingerprint to fast search db
    /// </summary>
    /// <param name="data">Authentication code</param>
    /// <param name="user">User ID</param>
    static public void AddFingerPrintToDB(byte[] data, int user)
    {
      if (mState == FingerPrintState.Active)
        addFingerPrintToDB(data, data.Length, user);
    }

    /// <summary>
    /// Removes the user's authentication data from fast search db
    /// </summary>
    /// <param name="user">The user to remove his fingerprint</param>
    static public void RemoveFingerPrintFromDB(int user)
    {
      if (mState == FingerPrintState.Active)
        removeFingerPrintFromDB(user);
    }

    /// <summary>
    /// Loads all fingerprints from data base
    /// and injects them to fast search database
    /// </summary>
    static public void LoadFingerPrints()
    {
      if (mState != FingerPrintState.Active)
        return;

      foreach(DataRow row in DB.User.LoadAllFingerPrints().Rows)
      {
        byte[] data = (byte[])row[0];
        int user_id = row.Field<int>(1);
        AddFingerPrintToDB(data, user_id);
      }
    }

    /// <summary>
    /// Searches fast search database for the user having given data
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    static public int Search(byte[] data)
    {
      if (mState == FingerPrintState.Deactive)
        return -1;
      else if (mState == FingerPrintState.FakeActive)
        return -1;
      else
        return search(data, data.Length);
    }

    /// <summary>
    /// Creates a search thread to search for the given authentication data
    /// </summary>
    /// <param name="data">A User's fingerprint</param>
    /// <returns>A thread managing the search</returns>
    static public SearchThread SearchWithThread(byte[] data)
    {
      return new SearchThread(data);
    }

    /// <summary>
    /// This method returns the search state for the given search.
    /// </summary>
    /// <param name="index">Current index of the searched fingerprint</param>
    /// <param name="count">Total index of the searched fingerprint</param>
    static public void GetSearchState(out int index, out int count)
    {
      if (mState == FingerPrintState.Deactive || mState == FingerPrintState.FakeActive)
        index = count = 0;
      else
        getSearchState(out index, out count);
    }

    /// <summary>
    /// Stops searching for fingerprint user.
    /// </summary>
    static public void StopSearch()
    {
      if (mState == FingerPrintState.Active)
        stopSearch();
    }
    #endregion Finger Print Fast Search
  }
}
