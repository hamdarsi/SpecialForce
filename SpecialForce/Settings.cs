﻿using System.ComponentModel;

namespace SpecialForce.Properties
{
  public sealed partial class Settings 
  {
    private bool mUpdatesForFingerPrint = true;

    public Settings()
    {
    }

    private void FingerPrintModuleUpdated()
    {
      if (!mUpdatesForFingerPrint)
        return;

      // Start update
      mUpdatesForFingerPrint = false;

      // Configure FingerPrint Module
      FingerPrint.ReLoad();

      // End update
      mUpdatesForFingerPrint = true;
    }

    protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      base.OnPropertyChanged(sender, e);

      if (e.PropertyName == "FingerPrintModuleEnabled")
        FingerPrintModuleUpdated();
      else if (e.PropertyName == "IdleMinutesToLock")
        Session.ResetLockTimer();
      else if (e.PropertyName == "ScheduleCheckInterval")
        DB.GameManager.UpdateScheduleCheckInterval();
      else if (e.PropertyName == "CommandSendInterval")
        ControlTower.CommandQueue.UpdateTimerInterval();
    }
  }
}
