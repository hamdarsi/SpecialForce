﻿namespace SpecialForce
{
  partial class PersianDatePicker
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.txtDay = new System.Windows.Forms.NumericUpDown();
      this.txtYear = new System.Windows.Forms.NumericUpDown();
      this.txtMonth = new System.Windows.Forms.NumericUpDown();
      ((System.ComponentModel.ISupportInitialize)(this.txtDay)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtMonth)).BeginInit();
      this.SuspendLayout();
      // 
      // txtDay
      // 
      this.txtDay.ForeColor = System.Drawing.Color.Black;
      this.txtDay.Location = new System.Drawing.Point(98, 15);
      this.txtDay.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
      this.txtDay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtDay.Name = "txtDay";
      this.txtDay.Size = new System.Drawing.Size(37, 20);
      this.txtDay.TabIndex = 2;
      this.txtDay.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtDay.ValueChanged += new System.EventHandler(this.DateChanged);
      this.txtDay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxKeyDown);
      // 
      // txtYear
      // 
      this.txtYear.ForeColor = System.Drawing.Color.Black;
      this.txtYear.Location = new System.Drawing.Point(2, 15);
      this.txtYear.Maximum = new decimal(new int[] {
            1500,
            0,
            0,
            0});
      this.txtYear.Minimum = new decimal(new int[] {
            1300,
            0,
            0,
            0});
      this.txtYear.Name = "txtYear";
      this.txtYear.Size = new System.Drawing.Size(47, 20);
      this.txtYear.TabIndex = 0;
      this.txtYear.Value = new decimal(new int[] {
            1300,
            0,
            0,
            0});
      this.txtYear.ValueChanged += new System.EventHandler(this.DateChanged);
      this.txtYear.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxKeyDown);
      // 
      // txtMonth
      // 
      this.txtMonth.ForeColor = System.Drawing.Color.Black;
      this.txtMonth.Location = new System.Drawing.Point(55, 15);
      this.txtMonth.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
      this.txtMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtMonth.Name = "txtMonth";
      this.txtMonth.Size = new System.Drawing.Size(37, 20);
      this.txtMonth.TabIndex = 1;
      this.txtMonth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtMonth.ValueChanged += new System.EventHandler(this.DateChanged);
      this.txtMonth.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxKeyDown);
      // 
      // PersianDatePicker
      // 
      this.Controls.Add(this.txtDay);
      this.Controls.Add(this.txtYear);
      this.Controls.Add(this.txtMonth);
      this.Name = "PersianDatePicker";
      this.Size = new System.Drawing.Size(139, 37);
      this.Load += new System.EventHandler(this.PersianDateTimePicker_Load);
      ((System.ComponentModel.ISupportInitialize)(this.txtDay)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtMonth)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.NumericUpDown txtDay;
    private System.Windows.Forms.NumericUpDown txtYear;
    private System.Windows.Forms.NumericUpDown txtMonth;
  }
}
