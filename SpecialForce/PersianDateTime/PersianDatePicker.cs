﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce
{
  [DefaultEvent("OnDateChanged")]
  public partial class PersianDatePicker : ControlBase
  {
    private Font mFont = new Font("Tahoma", 8.25f);

    #region Properties
    private bool mActivateEvent = false;
    [DefaultValue(false)]
    public bool ActivateEvent
    {
      get { return mActivateEvent; }
      set { mActivateEvent = value; }
    }

    [Bindable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public int Year
    {
      get { return (int)(txtYear.Value); }
    }

    [Bindable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public int Month
    {
      get { return (int)(txtMonth.Value); }
    }

    [Bindable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public int Day
    {
      get { return (int)(txtDay.Value); }
    }

    [Bindable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public PersianDateTime Date
    {
      get { return PersianDateTime.Date(Year, Month, Day); }
      set 
      {
        bool prev_active = mActivateEvent;
        mActivateEvent = false;
        txtYear.Value = Math.Max(value.Year, txtYear.Minimum);
        txtMonth.Value = Math.Max(value.Month, txtMonth.Minimum);
        txtDay.Maximum = PersianDateTime.GetDaysInMonth(value.Year, value.Month);
        txtDay.Value = Math.Max(value.Day, txtDay.Minimum);
        mActivateEvent = prev_active;

        // instead of sending 3 times, send 1 time
        DateChanged(null, null);
      }
    }

    [DefaultValue(true)]
    public bool ShowYear
    {
      get { return txtYear.Visible; }
      set 
      {
        txtYear.Visible = value;
        ReOrder();
      }
    }

    [DefaultValue(true)]
    public bool ShowMonth
    {
      get { return txtMonth.Visible; }
      set
      {
        if (value == false)
          txtMonth.Value = 1;

        txtMonth.Visible = value;
        ReOrder();
      }
    }

    [DefaultValue(true)]
    public bool ShowDay
    {
      get { return txtDay.Visible; }
      set
      {
        if (value == false)
          txtDay.Value = 1;

        txtDay.Visible = value;
        ReOrder();
      }
    }

    private Color mTextColor = Color.White;
    [DefaultValue(typeof(Color), "0xFFFFFFFF")]
    public Color TextColor
    {
      get { return mTextColor; }
      set { RequestRepaint(); }
    }
    #endregion Properties


    #region Subscribable Events
    public event EventHandler<EventArgs> OnDateChanged;
    #endregion Subscribable Events


    #region Constructor
    public PersianDatePicker()
    {
      InitializeComponent();
    }
    #endregion Constructor


    #region Functionality
    private void ReOrder()
    {
      int left = 0;

      if (ShowYear)
      {
        txtYear.Left = left;
        left += txtYear.Width + 5;
      }

      if (ShowMonth)
      {
        txtMonth.Left = left;
        left += txtMonth.Width + 5;
      }

      if (ShowDay)
      {
        txtDay.Left = left;
        left += txtDay.Width + 1;
      }

      int right = Right;
      Width = left;
      if (Parent is Panel)
        Left = Right - Width;
    }

    private void TextBoxKeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode != Keys.Enter)
        return;

      e.Handled = true;
      e.SuppressKeyPress = true;
      if (sender == txtYear && (txtMonth.Visible || txtDay.Visible))
        SelectNextControl(txtYear, true, true, true, true);
      else if (sender == txtMonth && txtDay.Visible)
        SelectNextControl(txtMonth, true, true, true, true);
      else if (Parent != null)
        Parent.SelectNextControl(this, true, true, true, true);

      txtDay.Maximum = PersianDateTime.GetDaysInMonth((int)txtYear.Value, (int)txtMonth.Value);
    }

    private void PersianDateTimePicker_Load(object sender, EventArgs e)
    {
      ReOrder();
    }

    private void DateChanged(object sender, EventArgs e)
    {
      txtDay.Maximum = PersianDateTime.GetDaysInMonth(Year, Month);

      if (mActivateEvent)
      {
        EventHandler<EventArgs> ev = OnDateChanged;
        if (ev != null)
          ev(this, new EventArgs());
      }
    }
    #endregion Functionality


    #region Graphical Overrides
    protected override BehaviorParams CreateBehavior
    {
      get
      {
        BehaviorParams p = base.CreateBehavior;
        p.UsesAntiAlias();
        p.InvalidationEvents.Add(EventTypes.EnableChanged);
        p.InvalidationEvents.Add(EventTypes.VisibleChanged);
        return p;
      }
    }

    protected override void OnRecreateView(RecreateArgs e)
    {
      TextRenderer tr = new TextRenderer();
      tr.Canvas = e.Canvas;
      tr.Font = mFont;
      tr.Color = mTextColor;

      if(txtYear.Visible)
      {
        tr.Text = "سال";
        tr.Left = txtYear.Left;
        tr.Top = 0;
        tr.Width = txtYear.Width;
        tr.Height = tr.TextHeight;
        tr.Draw();
      }

      if (txtMonth.Visible)
      {
        tr.Text = "ماه";
        tr.Left = txtMonth.Left;
        tr.Top = 0;
        tr.Width = txtMonth.Width;
        tr.Height = tr.TextHeight;
        tr.Draw();
      }

      if (txtDay.Visible)
      {
        tr.Text = "روز";
        tr.Left = txtDay.Left;
        tr.Top = 0;
        tr.Width = txtDay.Width;
        tr.Height = tr.TextHeight;
        tr.Draw();
      }
    }
    #endregion Graphical Overrides
  }
}
