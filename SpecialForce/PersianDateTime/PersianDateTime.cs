﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace SpecialForce
{
  public enum DateTimeString
  {
    Date,
    DateTime,
    Time,
    TimeNoSecond,
    TimeNoHour,
    DayOfWeek,
    CompactDate,
    FullNumericDate,
    CompactDateTime,
    DayOfWeekAndDate,
    DetailedTime
  }

  public class PersianDateTime
  {
    /// <summary>
    /// This member is used to store the UTC/Gregorian date time.
    /// In case the information is needed to be used in persian
    /// type calendar, it will be converted
    /// </summary>
    private DateTime mDateTime;

    static PersianCalendar mPersianCalendar = new PersianCalendar();
    static GregorianCalendar mGregorianCalendar = new GregorianCalendar();

    public DateTime Value
    {
      get { return mDateTime; }
    }

    public int Year
    {
      get { return mPersianCalendar.GetYear(mDateTime); }
    }

    public int Month
    {
      get { return mPersianCalendar.GetMonth(mDateTime); }
    }

    public int Day
    {
      get { return mPersianCalendar.GetDayOfMonth(mDateTime); }
    }

    public DayOfWeek DayInWeek
    {
      get { return mPersianCalendar.GetDayOfWeek(mDateTime); }
    }

    public int Hour
    {
      get { return mPersianCalendar.GetHour(mDateTime); }
    }

    public int Minute
    {
      get { return mPersianCalendar.GetMinute(mDateTime); }
    }

    public int Second
    {
      get { return mPersianCalendar.GetSecond(mDateTime); }
    }

    public double MilliSecond
    {
      get { return mPersianCalendar.GetMilliseconds(mDateTime); }
    }

    public PersianDateTime NextDay
    {
      get 
      {
        PersianDateTime result = new PersianDateTime(this);
        result.TraverseDaysForward(1);
        return result;
      }
    }

    public PersianDateTime PreviousDay
    {
      get
      {
        PersianDateTime result = new PersianDateTime(this);
        result.TraverseDaysBackward(1);
        return result;
      }
    }

    public PersianDateTime NextWeek
    {
      get
      {
        PersianDateTime result = new PersianDateTime(this);
        result.TraverseDaysForward(7);
        return result;
      }
    }

    public PersianDateTime PreviousWeek
    {
      get
      {
        PersianDateTime result = new PersianDateTime(this);
        result.TraverseDaysBackward(7);
        return result;
      }
    }

    public PersianDateTime NextMonth
    {
      get
      {
        PersianDateTime result = new PersianDateTime(this);
        result.TraverseMonthsForward(1);
        return result;
      }
    }

    public PersianDateTime PreviousMonth
    {
      get
      {
        PersianDateTime result = new PersianDateTime(this);
        result.TraverseMonthsBackward(1);
        return result;
      }
    }

    public PersianDateTime NextYear
    {
      get
      {
        PersianDateTime result = new PersianDateTime(this);
        result.TraverseYearsForward(1);
        return result;
      }
    }

    public PersianDateTime PreviousYear
    {
      get
      {
        PersianDateTime result = new PersianDateTime(this);
        result.TraverseYearsBackward(1);
        return result;
      }
    }

    public Object DBFormat
    {
      get { return ToObject(this); }
    }


    /// <summary>
    /// Constructor of PersianDateTime with a given DateTime object.
    /// </summary>
    /// <param name="dt">The date/time to initialize from. Note that the DateTime is assumed to be in gregorian format</param>
    public PersianDateTime(DateTime dt)
    {
      mDateTime = dt;
    }

    public PersianDateTime()
    {
      mDateTime = new DateTime(1, 1, 1, mPersianCalendar);
    }

    public PersianDateTime(PersianDateTime dt)
    {
      mDateTime = new DateTime(dt.Value.Year,
                               dt.Value.Month,
                               dt.Value.Day,
                               dt.Value.Hour,
                               dt.Value.Minute,
                               dt.Value.Second,
                               dt.Value.Millisecond);
    }

    public PersianDateTime TraverseDaysForward(int day = 1)
    {
      int y = Year;
      int m = Month;
      int d = Day;
      int h = Hour, min = Minute, sec = Second;

      while (day != 0)
      {
        if (d + day > GetDaysInMonth(y, m))
        {
          day -= (GetDaysInMonth(y, m) - d + 1);
          if (m == 12)
          {
            ++y;
            m = 1;
            d = 1;
          }
          else
          {
            ++m;
            d = 1;
          }
        }
        else
        {
          d += day;
          day = 0;
        }
      }

      mDateTime = new DateTime(y, m, d, h, min, sec, mPersianCalendar);

      return this;
    }

    public PersianDateTime TraverseDaysBackward(int day = 1)
    {
      int y = Year, m = Month, d = Day;
      int h = Hour, min = Minute, sec = Second;

      while (day != 0)
      {
        if (d - day < 1)
        {
          day -= d;

          if (m == 1)
          {
            --y;
            m = 12;
          }
          else
            --m;

          d = GetDaysInMonth(y, m);
        }
        else
        {
          d -= day;
          day = 0;
        }
      }

      mDateTime = new DateTime(y, m, d, h, min, sec, mPersianCalendar);
      return this;
    }

    public PersianDateTime TraverseMonthsForward(int month = 1)
    {
      int y = Year, m = Month, d = Day;
      int h = Hour, min = Minute, sec = Second;

      while (month != 0)
      {
        if (month + m > 12)
        {
          month -= (12 - m + 1);
          ++y;
          m = 1;
        }
        else
        {
          m += month;
          month = 0;
        }
      }

      d = Math.Min(GetDaysInMonth(y, m), d);
      mDateTime = new DateTime(y, m, d, h, min, sec, mPersianCalendar);
      return this;
    }

    public PersianDateTime TraverseMonthsBackward(int month = 1)
    {
      int y = Year, m = Month, d = Day;
      int h = Hour, min = Minute, sec = Second;

      while (month != 0)
      {
        if (m < month)
        {
          month -= m;
          --y;
          m = 12;
        }
        else
        {
          m -= month;
          month = 0;
        }
      }

      d = Math.Min(GetDaysInMonth(y, m), d);
      mDateTime = new DateTime(y, m, d, h, min, sec, mPersianCalendar);
      return this;
    }

    public PersianDateTime TraverseYearsForward(int year = 1)
    {
      int y = Year, m = Month, d = Day;
      int h = Hour, min = Minute, sec = Second;

      if (m == 12 && d == 30 && (year - y) % 4 != 0)
        d = 29;

      mDateTime = new DateTime(Year + year, Month, Day, h, min, sec, mPersianCalendar);
      return this;
    }

    public PersianDateTime TraverseYearsBackward(int year = 1)
    {
      int y = Year, m = Month, d = Day;
      int h = Hour, min = Minute, sec = Second;

      if (m == 12 && d == 30 && (year - y) % 4 != 0)
        d = 29;

      mDateTime = new DateTime(Year - year, Month, Day, h, min, sec, mPersianCalendar);
      return this;
    }

    public override string ToString()
    {
      return ToString(DateTimeString.DateTime);
    }

    public void SetTo(int year, int month, int day, bool persian = true)
    {
      if (persian)
        mDateTime = new DateTime(year, month, day, mPersianCalendar);
      else
        mDateTime = new DateTime(year, month, day);
    }

    public void SetToNow()
    {
      mDateTime = DateTime.Now;
    }

    public PersianDateTime SetTime(PersianDateTime dt)
    {
      return SetTime(dt.Hour, dt.Minute, dt.Second);
    }

    public PersianDateTime SetTime(int hour, int minute, int second)
    {
      // normalize time for seconds
      int y = Year, m = Month, d = Day;

      while (second >= 60)
      {
        ++minute;
        second -= 60;
      }

      // normalize time for minutes
      while(minute >= 60)
      {
        ++hour;
        minute -= 60;
      }

      // normalize time for hours
      while (hour >= 24)
      {
        ++d;
        hour -= 24;
      }

      // normalize date for days
      while (d > GetDaysInMonth(y, m))
      {
        d -= GetDaysInMonth(y, m);
        ++m;
      }

      // normalize date for months
      while (m > 12)
      {
        ++y;
        m = 1;
      }

      mDateTime = new DateTime(y, m, d, hour, minute, second, mPersianCalendar);
      return this;
    }

    public string ToString(DateTimeString c)
    {
      if (c == DateTimeString.Date)
        return
          mPersianCalendar.GetDayOfMonth(mDateTime) + " " +
          GetPersianMonthName(mPersianCalendar.GetMonth(mDateTime)) + " " +
          mPersianCalendar.GetYear(mDateTime).ToString();

      else if (c == DateTimeString.DateTime)
        return
          mPersianCalendar.GetDayOfMonth(mDateTime) + " " +
          GetPersianMonthName(mPersianCalendar.GetMonth(mDateTime)) + " " +
          mPersianCalendar.GetYear(mDateTime).ToString() + " " +
          Hour.ToString("D2") + ":" +
          Minute.ToString("D2") + ":" +
          Second.ToString("D2");

      else if (c == DateTimeString.Time)
        return
          Hour.ToString("D2") + ":" +
          Minute.ToString("D2") + ":" +
          Second.ToString("D2");

      else if (c == DateTimeString.TimeNoSecond)
        return
          Hour.ToString("D2") + ":" +
          Minute.ToString("D2");

      else if (c == DateTimeString.TimeNoHour)
        return
          Minute.ToString("D2") + ":" +
          Second.ToString("D2");

      else if(c == DateTimeString.DetailedTime)
        return
          Hour.ToString("D2") + ":" +
          Minute.ToString("D2") + ":" +
          Second.ToString("D2") + "." +
          TinyMath.Round(MilliSecond).ToString("D3");

      else if (c == DateTimeString.DayOfWeek)
        return
          GetDayNameInWeek(DayInWeek);

      else if (c == DateTimeString.CompactDate)
        return
          (Year % 100).ToString("D2") + '/' +
          Month.ToString() + '/' +
          Day.ToString();

      else if(c == DateTimeString.FullNumericDate)
        return
          Year.ToString("D4") + '/' +
          Month.ToString("D2") + '/' +
          Day.ToString("D2");

      else if (c == DateTimeString.CompactDateTime)
        return
          ToString(DateTimeString.TimeNoSecond) + 
          "  " +
          ToString(DateTimeString.CompactDate);

      else // Conversion.DayOfWeekAndDate
        return
          ToString(DateTimeString.DayOfWeek) + " " +
          ToString(DateTimeString.CompactDate);
    }

    /// <summary>
    /// This method returns the absolute time difference between the given 
    /// timestamp and current timestamp. The result is in seconds and has 
    /// floating point accuracy. This will not take in account date part, 
    /// only time part is considered.
    /// </summary>
    public double GetTimeDifference(PersianDateTime dt)
    {
      DateTime d1 = new DateTime(1, 1, 1, mDateTime.Hour, mDateTime.Minute, mDateTime.Second, mDateTime.Millisecond);
      DateTime d2 = new DateTime(1, 1, 1, dt.mDateTime.Hour, dt.mDateTime.Minute, dt.mDateTime.Second, mDateTime.Millisecond);
      return d1 > d2 ? (d1 - d2).TotalSeconds : (d2 - d1).TotalSeconds;
    }

    /// <summary>
    /// This method returns the absolute time difference between the given 
    /// timestamp and current timestamp. The result is in seconds and has 
    /// floating point accuracy.
    /// </summary>
    public double GetDateTimeDifference(PersianDateTime dt)
    {
      DateTime d1 = mDateTime;
      DateTime d2 = dt.mDateTime;
      return d1 > d2 ? (d1 - d2).TotalSeconds : (d2 - d1).TotalSeconds;
    }

    public int GetNrOfDaysTo(PersianDateTime dt)
    {
      DateTime self = new DateTime(mDateTime.Year, mDateTime.Month, mDateTime.Day);
      DateTime param = new DateTime(dt.mDateTime.Year, dt.mDateTime.Month, dt.mDateTime.Day);
      return (param - self).Days;
    }

    public PersianDateTime GetElapsedSeconds(int origin)
    {
      int secs = Math.Abs(Hour * 3600 + Minute * 60 + Second - origin);
      return PersianDateTime.Time(0, 0, secs);
    }

    public bool LiesBetween(PersianDateTime start, PersianDateTime end)
    {
      return this > start && this <= end;
    }

    public override bool Equals(object obj)
    {
      return mDateTime.Equals(obj);
    }

    public override int GetHashCode()
    {
      return mDateTime.GetHashCode();
    }

    public static bool operator > (PersianDateTime dt1, PersianDateTime dt2)
    {
      return dt1.mDateTime > dt2.mDateTime;
    }

    public static bool operator < (PersianDateTime dt1, PersianDateTime dt2)
    {
      return dt1.mDateTime < dt2.mDateTime;
    }

    public static bool operator >= (PersianDateTime dt1, PersianDateTime dt2)
    {
      return dt1.mDateTime >= dt2.mDateTime;
    }

    public static bool operator <= (PersianDateTime dt1, PersianDateTime dt2)
    {
      return dt1.mDateTime <= dt2.mDateTime;
    }

    public static bool operator == (PersianDateTime dt1, PersianDateTime dt2)
    {
      if (IsNull(dt1) && IsNull(dt2))
        return true;
      else if (IsNull(dt1) != IsNull(dt2))
        return false;
      else
        return dt1.mDateTime == dt2.mDateTime;
    }

    public static bool operator !=(PersianDateTime dt1, PersianDateTime dt2)
    {
      if (IsNull(dt1) && IsNull(dt2))
        return false;
      else if (IsNull(dt1) != IsNull(dt2))
        return true;
      else
        return dt1.mDateTime != dt2.mDateTime;
    }

    static private bool IsNull(PersianDateTime dt)
    {
      return Object.Equals(dt, null);
    }

    static public PersianDateTime Date(int year, int month, int day, bool persian = true)
    {
      PersianDateTime dt = new PersianDateTime();
      dt.mDateTime = persian ?
        new DateTime(year, month, day, mPersianCalendar) :
        new DateTime(year, month, day);

      return dt;
    }

    static public PersianDateTime Time(int hour, int minute = 0, int second = 0)
    {
      return Today.SetTime(hour, minute, second);
    }

    static public PersianDateTime FromObject(object obj)
    {
      return
        obj == DBNull.Value || obj == null
        ? null
        : new PersianDateTime((DateTime)obj);
    }

    static public object ToObject(PersianDateTime dt)
    {
      if (dt == null)
        return null;
      else
        return dt.Value;
    }

    static public string ToString(DateTime dt)
    {
      return new PersianDateTime(dt).ToString();
    }

    static public string ToString(Object obj)
    {
      return PersianDateTime.FromObject(obj).ToString();
    }

    static public PersianDateTime Now
    {
      get 
      {
        PersianDateTime dt = new PersianDateTime();
        dt.SetToNow();
        return dt;
      }
    }

    static public PersianDateTime Today
    {
      get 
      {
        DateTime now = DateTime.Now;
        PersianDateTime dt = PersianDateTime.Date(now.Year, now.Month, now.Day, false);
        return dt;
      }
    }

    static public PersianDateTime Tomorrow
    {
      get 
      {
        PersianDateTime result = PersianDateTime.Today;
        result.TraverseDaysForward(1);
        return result;
      }
    }

    static public PersianDateTime Yesterday
    {
      get 
      {
        PersianDateTime result = PersianDateTime.Today;
        result.TraverseDaysBackward(1);
        return result;
      }
    }

    static public PersianDateTime ThisWeek
    {
      get 
      {
        PersianDateTime result = PersianDateTime.Today;
        while (result.DayInWeek != DayOfWeek.Saturday)
          result.TraverseDaysBackward();
        return result;
      }
    }

    static public PersianDateTime ThisMonth
    {
      get { return PersianDateTime.Date(Today.Year, Today.Month, 1); }
    }

    static public PersianDateTime ThisYear
    {
      get { return PersianDateTime.Date(Today.Year, 1, 1); }
    }

    static private PersianDateTime mMinimum = PersianDateTime.Date(1, 1, 1);
    static public PersianDateTime Minimum
    {
      get { return new PersianDateTime(mMinimum); }
    }

    static public PersianDateTime mMaximum = PersianDateTime.Date(9378, 10, 10);
    static public PersianDateTime Maximum
    {
      get { return mMaximum; }
    }

    static public int GetDaysInMonth(int year, int month)
    {
      if (year < 1)
        throw new Exception("(PersianDateTime) There is no such year " + year.ToString());

      if (month < 1 || month > 12)
        throw new Exception("(PersianDateTime) There is no such month " + month.ToString());

      if (month <= 6)
        return 31;
      else if (month < 12)
        return 30;
      else if (year % 4 == 3)
        return 30;
      else
        return 29;
    }

    static private string GetPersianMonthName(int month)
    {
      switch (month)
      {
        case 1: return "فروردین";
        case 2: return "اردیبهشت";
        case 3: return "خرداد";
        case 4: return "تیر";
        case 5: return "مرداد";
        case 6: return "شهریور";
        case 7: return "مهر";
        case 8: return "آبان";
        case 9: return "آذر";
        case 10: return "دی";
        case 11: return "بهمن";
        case 12: return "اسفند";
        default:
          throw new Exception("ماه ذکر شده وجود ندارد: " + month.ToString());
      }
    }

    static private string GetDayNameInWeek(DayOfWeek day)
    {
      switch (day)
      {
        case DayOfWeek.Saturday: return "شنبه";
        case DayOfWeek.Sunday: return "یک شنبه";
        case DayOfWeek.Monday: return "دو شنبه";
        case DayOfWeek.Tuesday: return "سه شنبه";
        case DayOfWeek.Wednesday: return "چهار شنبه";
        case DayOfWeek.Thursday: return "پنج شنبه";
        default: return "جمعه";
      }
    }
  }
}
