﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce
{
  [DefaultEvent("OnTimeChanged")]
  public partial class PersianTimePicker : ControlBase
  {
    private Font mFont = new Font("Tahoma", 8.25f);

    #region Properties
    private bool mActivateEvent = false;
    [DefaultValue(false)]
    public bool ActivateEvent
    {
      get { return mActivateEvent; }
      set { mActivateEvent = value; }
    }

    [Bindable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public int Hour
    {
      get { return (int)(txtHour.Value); }
    }

    [Bindable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public int Minute
    {
      get { return (int)(txtMinute.Value); }
    }

    [Bindable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public int Second
    {
      get { return (int)(txtSecond.Value); }
    }

    [Bindable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public PersianDateTime Time
    {
      get { return PersianDateTime.Time(Hour, Minute, Second); }
      set
      {
        bool prev_active = mActivateEvent;
        mActivateEvent = false;
        txtHour.Value = Math.Max(value.Hour, txtHour.Minimum);
        txtMinute.Value = Math.Max(value.Minute, txtMinute.Minimum);
        txtSecond.Value = Math.Max(value.Second, txtSecond.Minimum);
        mActivateEvent = prev_active;

        // instead of sending 3 times, send 1 time
        TimeChanged(null, null);
      }
    }

    [DefaultValue(true)]
    public bool ShowHour
    {
      get { return txtHour.Visible; }
      set
      {
        if (value == false)
          txtHour.Value = 0;

        txtHour.Visible = value;
        ReOrder();
      }
    }

    [DefaultValue(true)]
    public bool ShowMinute
    {
      get { return txtMinute.Visible; }
      set
      {
        if (value == false)
          txtMinute.Value = 0;

        txtMinute.Visible = value;
        ReOrder();
      }
    }

    [DefaultValue(true)]
    public bool ShowSecond
    {
      get { return txtSecond.Visible; }
      set
      {
        if (value == false)
          txtSecond.Value = 0;

        txtSecond.Visible = value;
        ReOrder();
      }
    }

    private Color mTextColor = Color.White;
    [DefaultValue(typeof(Color), "0xFFFFFFFF")]
    public Color TextColor
    {
      get { return mTextColor; }
      set { RequestRepaint(); }
    }
    #endregion Properties


    #region Subscribable Events
    public event EventHandler<EventArgs> OnTimeChanged;
    #endregion Subscribable Events


    #region Constructor
    public PersianTimePicker()
    {
      InitializeComponent();
    }
    #endregion Constructor


    #region Functionality
    private void ReOrder()
    {
      int left = 0;

      if (ShowHour)
      {
        lblColon1.Visible = txtMinute.Visible || txtSecond.Visible;
        txtHour.Left = left;
        lblColon1.Left = txtHour.Right;
        left += lblColon1.Width + txtHour.Width;
      }
      else
        lblColon1.Visible = false;

      if (ShowMinute)
      {
        lblColon2.Visible = txtSecond.Visible;
        txtMinute.Left = left;
        lblColon2.Left = txtMinute.Right;
        left += lblColon2.Width + txtMinute.Width;
      }
      else
        lblColon2.Visible = false;

      if (ShowSecond)
      {
        txtSecond.Left = left;
        left += txtSecond.Width + 5;
      }
      else
        lblColon2.Visible = false;

      int right = Right;
      Width = left;
      if (Parent is Panel)
        Left = Right - Width;
    }

    private void TextBoxKeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode != Keys.Enter)
        return;

      e.Handled = true;
      e.SuppressKeyPress = true;
      if (sender == txtHour && (txtMinute.Visible || txtSecond.Visible))
        SelectNextControl(txtHour, true, true, true, true);
      else if (sender == txtMinute && txtSecond.Visible)
        SelectNextControl(txtMinute, true, true, true, true);
      else if (Parent != null)
        Parent.SelectNextControl(this, true, true, true, true);
    }

    private void PersianTimePicker_Load(object sender, EventArgs e)
    {
      ReOrder();
    }

    private void TimeChanged(Object sender, EventArgs e)
    {
      if (mActivateEvent)
      {
        EventHandler<EventArgs> ev = OnTimeChanged;
        if (ev != null)
          ev(this, new EventArgs());
      }
    }
    #endregion Functionality


    #region Graphical Overrides
    protected override BehaviorParams CreateBehavior
    {
      get
      {
        BehaviorParams p = base.CreateBehavior;
        p.UsesAntiAlias();
        return p;
      }
    }

    protected override void OnRecreateView(RecreateArgs e)
    {
      TextRenderer tr = new TextRenderer();
      tr.Canvas = e.Canvas;
      tr.Font = mFont;
      tr.Color = mTextColor;

      if (txtHour.Visible)
      {
        tr.Text = "ساعت";
        tr.Left = txtHour.Left;
        tr.Top = 0;
        tr.Width = txtHour.Width;
        tr.Height = tr.TextHeight;
        tr.Draw();
      }

      if (txtMinute.Visible)
      {
        tr.Text = "دقیقه";
        tr.Left = txtMinute.Left;
        tr.Top = 0;
        tr.Width = txtMinute.Width;
        tr.Height = tr.TextHeight;
        tr.Draw();
      }

      if (txtSecond.Visible)
      {
        tr.Text = "ثانیه";
        tr.Left = txtSecond.Left;
        tr.Top = 0;
        tr.Width = txtSecond.Width;
        tr.Height = tr.TextHeight;
        tr.Draw();
      }
    }
    #endregion Graphical Overrides
  }
}
