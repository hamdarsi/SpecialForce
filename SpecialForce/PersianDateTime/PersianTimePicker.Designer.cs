﻿namespace SpecialForce
{
  partial class PersianTimePicker
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersianTimePicker));
      this.txtSecond = new System.Windows.Forms.NumericUpDown();
      this.txtHour = new System.Windows.Forms.NumericUpDown();
      this.txtMinute = new System.Windows.Forms.NumericUpDown();
      this.lblColon1 = new SpecialForce.TransparentLabel();
      this.lblColon2 = new SpecialForce.TransparentLabel();
      ((System.ComponentModel.ISupportInitialize)(this.txtSecond)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtHour)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtMinute)).BeginInit();
      this.SuspendLayout();
      // 
      // txtSecond
      // 
      this.txtSecond.Location = new System.Drawing.Point(93, 15);
      this.txtSecond.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
      this.txtSecond.Name = "txtSecond";
      this.txtSecond.Size = new System.Drawing.Size(32, 20);
      this.txtSecond.TabIndex = 32;
      this.txtSecond.ValueChanged += new System.EventHandler(this.TimeChanged);
      this.txtSecond.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxKeyDown);
      // 
      // txtHour
      // 
      this.txtHour.Location = new System.Drawing.Point(4, 15);
      this.txtHour.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
      this.txtHour.Name = "txtHour";
      this.txtHour.Size = new System.Drawing.Size(32, 20);
      this.txtHour.TabIndex = 30;
      this.txtHour.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
      this.txtHour.ValueChanged += new System.EventHandler(this.TimeChanged);
      this.txtHour.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxKeyDown);
      // 
      // txtMinute
      // 
      this.txtMinute.Location = new System.Drawing.Point(48, 15);
      this.txtMinute.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
      this.txtMinute.Name = "txtMinute";
      this.txtMinute.Size = new System.Drawing.Size(32, 20);
      this.txtMinute.TabIndex = 31;
      this.txtMinute.ValueChanged += new System.EventHandler(this.TimeChanged);
      this.txtMinute.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxKeyDown);
      // 
      // lblColon1
      // 
      this.lblColon1.Location = new System.Drawing.Point(37, 18);
      this.lblColon1.Name = "lblColon1";
      this.lblColon1.Size = new System.Drawing.Size(7, 14);
      this.lblColon1.TabIndex = 36;
      this.lblColon1.TabStop = false;
      this.lblColon1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblColon1.Texts")));
      // 
      // lblColon2
      // 
      this.lblColon2.Location = new System.Drawing.Point(81, 18);
      this.lblColon2.Name = "lblColon2";
      this.lblColon2.Size = new System.Drawing.Size(7, 14);
      this.lblColon2.TabIndex = 37;
      this.lblColon2.TabStop = false;
      this.lblColon2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblColon2.Texts")));
      // 
      // PersianTimePicker
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lblColon2);
      this.Controls.Add(this.lblColon1);
      this.Controls.Add(this.txtSecond);
      this.Controls.Add(this.txtHour);
      this.Controls.Add(this.txtMinute);
      this.Name = "PersianTimePicker";
      this.Size = new System.Drawing.Size(129, 37);
      this.Load += new System.EventHandler(this.PersianTimePicker_Load);
      ((System.ComponentModel.ISupportInitialize)(this.txtSecond)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtHour)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtMinute)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.NumericUpDown txtSecond;
    private System.Windows.Forms.NumericUpDown txtHour;
    private System.Windows.Forms.NumericUpDown txtMinute;
    private SpecialForce.TransparentLabel lblColon1;
    private SpecialForce.TransparentLabel lblColon2;
  }
}
