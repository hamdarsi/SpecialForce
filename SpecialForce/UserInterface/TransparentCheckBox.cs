﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce
{
  [DefaultEvent("CheckedChanged")]
  internal partial class TransparentCheckBox : ControlBase
  {
    #region Internal Data
    private Image mCheckMark = Properties.Resources.CheckBox_Unchecked;
    #endregion Internal Data


    #region Properties
    CheckState mState = CheckState.Unchecked;
    [DefaultValue(CheckState.Unchecked)]
    public CheckState CheckState
    {
      get { return mState; }
      set { AssignCheckState(value); }
    }

    [DefaultValue(false)]
    public bool Checked
    {
      get { return mState == CheckState.Checked; }
      set { AssignCheckState(value ? CheckState.Checked : CheckState.Unchecked); }
    }

    private bool mEnterWorksAsTab = false;
    [DefaultValue(false)]
    public bool EnterWorksAsTab
    {
      get { return mEnterWorksAsTab; }
      set { mEnterWorksAsTab = value; }
    }
    #endregion Properties


    #region Events
    public event EventHandler CheckedChanged;
    #endregion Events


    #region Constructor
    public TransparentCheckBox()
    {
      InitializeComponent();
    }
    #endregion Constructor


    #region Internal Affairs
    private void AssignCheckState(CheckState value)
    {
      if (mState == value)
        return;

      mState = value;
      mCheckMark = mState == CheckState.Checked ?
        Properties.Resources.CheckBox_Checked :
        Properties.Resources.CheckBox_Unchecked;

      RequestRepaint();
    }
    #endregion Internal Affairs


    #region Overrides
    protected override BehaviorParams CreateBehavior
    {
      get
      {
        BehaviorParams prms = base.CreateBehavior;
        prms.RendersText();
        prms.InvalidationEvents.Add(EventTypes.FocusReceived);
        prms.InvalidationEvents.Add(EventTypes.FocusLost);
        return prms;
      }
    }

    protected override void OnRecreateView(RecreateArgs e)
    {
      int w = ClientRectangle.Width;

      // Draw check mark
      int x = RightToLeft ? Width - mCheckMark.Width - 1 : 0;
      int y = (Height - mCheckMark.Height) / 2;
      e.Canvas.DrawImageUnscaled(x, y, mCheckMark);
      w -= mCheckMark.Width;

      // draw text
      DrawText(e.Canvas, 0, 0, w - 5, Height);
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);

      if (e.KeyCode == Keys.Space)
        Checked = !Checked;
      else if (e.KeyCode == Keys.Enter && mEnterWorksAsTab)
        Parent.SelectNextControl(this, true, true, true, true);
      else
        return;

      e.Handled = true;
      e.SuppressKeyPress = true;
    }

    protected override void OnMouseDown(MouseEventArgs e)
    {
      Focus();
      base.OnMouseDown(e);
    }

    protected override void OnClick(EventArgs e)
    {
      base.OnClick(e);
      AssignCheckState(Checked ? CheckState.Unchecked : CheckState.Checked);

      EventHandler ev = CheckedChanged;
      if (ev != null)
        ev(this, new EventArgs());
    }

    protected override void OnDoubleClick(EventArgs e)
    {
      base.OnDoubleClick(e);
      OnClick(e);
    }
    #endregion Overrides
  }
}
