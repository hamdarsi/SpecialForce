﻿namespace SpecialForce
{
  partial class TinyPicker
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TinyPicker));
      this.lblStaffPick = new SpecialForce.TransparentLabel();
      this.lblGamePick = new SpecialForce.TransparentLabel();
      this.tbMain = new SpecialForce.ToolBar();
      this.SuspendLayout();
      // 
      // lblStaffPick
      // 
      this.lblStaffPick.ForeColor = System.Drawing.Color.White;
      this.lblStaffPick.Location = new System.Drawing.Point(309, 6);
      this.lblStaffPick.Name = "lblStaffPick";
      this.lblStaffPick.Size = new System.Drawing.Size(0, 0);
      this.lblStaffPick.TabIndex = 23;
      this.lblStaffPick.TabStop = false;
      this.lblStaffPick.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblStaffPick.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblStaffPick.Texts")));
      // 
      // lblGamePick
      // 
      this.lblGamePick.ForeColor = System.Drawing.Color.White;
      this.lblGamePick.Location = new System.Drawing.Point(3, 6);
      this.lblGamePick.Name = "lblGamePick";
      this.lblGamePick.Size = new System.Drawing.Size(0, 0);
      this.lblGamePick.TabIndex = 22;
      this.lblGamePick.TabStop = false;
      this.lblGamePick.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblGamePick.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblGamePick.Texts")));
      this.lblGamePick.Click += new System.EventHandler(this.OnMouseClicked);
      // 
      // tbMain
      // 
      this.tbMain.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbMain.BorderWidth = 0.5F;
      this.tbMain.CornerRadius = 4;
      this.tbMain.Location = new System.Drawing.Point(3, 2);
      this.tbMain.Name = "tbMain";
      this.tbMain.Size = new System.Drawing.Size(421, 25);
      this.tbMain.TabIndex = 21;
      // 
      // TinyPicker
      // 
      this.Controls.Add(this.lblStaffPick);
      this.Controls.Add(this.lblGamePick);
      this.Controls.Add(this.tbMain);
      this.Name = "TinyPicker";
      this.Size = new System.Drawing.Size(427, 30);
      this.ResumeLayout(false);

    }

    #endregion

    private ToolBar tbMain;
    private TransparentLabel lblGamePick;
    private TransparentLabel lblStaffPick;
  }
}
