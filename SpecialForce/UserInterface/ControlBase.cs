﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce
{
  public class ControlBase : UserControl
  {
    #region Internal Data
    private TextRenderer mTextRenderer = new TextRenderer();
    private Rectangle mSavedBounds;
    #endregion Internal Data


    #region Properties
    private Alignment mTextAlign = Alignment.MiddleRight;
    [DefaultValue(Alignment.MiddleRight)]
    public Alignment TextAlign
    {
      get { return mTextAlign; }
      set { AssignTextAlign(value); }
    }

    private bool mRightToLeft = true;
    [DefaultValue(true)]
    public new bool RightToLeft
    {
      get { return mRightToLeft; }
      set { AssignRightToLeft(value); }
    }

    private Color mBackColor = Color.Transparent;
    [DefaultValue(typeof(Color), "0x00FFFFFF")]
    public new Color BackColor
    {
      get { return mBackColor; }
      set { AssignBackColor(value); }
    }

    protected Size TextSize
    {
      get { return mTextRenderer.TextSize; }
    }

    // This seems necessary. Text has been fucked up in UserControl
    [Browsable(true)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
    [EditorBrowsable(EditorBrowsableState.Always)]
    [Bindable(true)]
    public new string Text
    {
      get { return mTextRenderer.Text; }
      set { AssignText(null, value); }
    }

    public List<string> Texts
    {
      get { return mTextRenderer.Texts; }
      set { AssignText(value, null); }
    }
    #endregion Properties


    #region Subscribable Events
    public event EventHandler<EventArgs> OnFocusReceived;
    public event EventHandler<EventArgs> OnFocusLost;
    #endregion Subscribable Events


    #region Constructor
    protected ControlBase()
    {
      SetStyle(ControlStyles.SupportsTransparentBackColor, true);
      SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

      // Initialize text renderer with default values
      mTextRenderer.Font = Font; 
      mTextRenderer.Color = ForeColor;
      mTextRenderer.Text = "";
      mTextRenderer.Justify = mTextAlign;
    }
    #endregion Constructor


    #region Overrides
    protected override CreateParams CreateParams
    {
      get
      {
        CreateParams prm = base.CreateParams;
        prm.ExStyle |= 0x20;
        return prm;
      }
    }

    protected override void OnHandleCreated(EventArgs e)
    {
      base.OnHandleCreated(e);
      mBehavior = CreateBehavior;
    }

    protected override void OnSizeChanged(EventArgs e)
    {
      base.OnSizeChanged(e);

      if (ClientRectangle != mSavedBounds)
      {
        InvalidateParents(mSavedBounds);
        mSavedBounds = this.GetFormCoordinates();
      }
        
      Size sz = QueryPrefferedSize();
      if (sz.Width != 0 && sz != Size)
        Size = sz;

      InvalidateForReason(EventTypes.SizeChanged);
    }

    protected override void OnLocationChanged(EventArgs e)
    {
      base.OnLocationChanged(e);

      if (mSavedBounds != ClientRectangle)
      {
        InvalidateParents(mSavedBounds);
        mSavedBounds = this.GetFormCoordinates();
      }

      InvalidateForReason(EventTypes.LocationChanged);
    }

    protected override void OnGotFocus(EventArgs e)
    {
      base.OnGotFocus(e);

      EventHandler<EventArgs> ev = OnFocusReceived;
      if (ev != null)
        ev(this, e);

      InvalidateForReason(EventTypes.FocusReceived);
    }

    protected override void OnLostFocus(EventArgs e)
    {
      base.OnLostFocus(e);

      EventHandler<EventArgs> ev = OnFocusLost;
      if (ev != null)
        ev(this, e);

      InvalidateForReason(EventTypes.FocusLost);
    }

    protected override void OnPaintBackground(PaintEventArgs e)
    {
      InvalidateForReason(EventTypes.PaintBackGround);
    }

    protected override void OnFontChanged(EventArgs e)
    {
      base.OnFontChanged(e);
      mTextRenderer.Font = Font;

      InvalidateForReason(EventTypes.FontChanged);
    }

    protected override void OnForeColorChanged(EventArgs e)
    {
      base.OnForeColorChanged(e);
      mTextRenderer.Color = ForeColor;

      InvalidateForReason(EventTypes.ForeColorChanged);
    }

    protected override void OnVisibleChanged(EventArgs e)
    {
      base.OnVisibleChanged(e);

      InvalidateForReason(EventTypes.VisibleChanged);
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      base.OnEnabledChanged(e);

      InvalidateForReason(EventTypes.EnableChanged);
    }

    protected override void OnStyleChanged(EventArgs e)
    {
      base.OnStyleChanged(e);
      InvalidateForReason(EventTypes.StyleChanged);
    }

    protected override void OnMouseDown(MouseEventArgs e)
    {
      base.OnMouseDown(e);
      InvalidateForReason(EventTypes.MouseDown);
    }

    protected override void OnMouseUp(MouseEventArgs e)
    {
      base.OnMouseUp(e);
      InvalidateForReason(EventTypes.MouseUp);
    }

    protected override void OnMouseEnter(EventArgs e)
    {
      base.OnMouseEnter(e);
      InvalidateForReason(EventTypes.MouseEnter);
    }

    protected override void OnMouseLeave(EventArgs e)
    {
      base.OnMouseLeave(e);
      InvalidateForReason(EventTypes.MouseLeave);
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);
      InvalidateForReason(EventTypes.KeyDown);
    }

    protected override void OnKeyUp(KeyEventArgs e)
    {
      base.OnKeyUp(e);
      InvalidateForReason(EventTypes.KeyUp);
    }

    protected override void OnInvalidated(InvalidateEventArgs e)
    {
      base.OnInvalidated(e);
      mValidationPending = true;
    }
    #endregion Overrides


    #region Internal Utilities
    /// <summary>
    /// Assigns the given text to control.
    /// Also updates the control
    /// </summary>
    private void AssignText(List<string> texts, string text)
    {
      // Update text renderer.
      if (texts != null)
        mTextRenderer.Texts = texts;
      else if (text != null)
        mTextRenderer.Text = text;
      else
        return;

      // Inform child classes of the text change event
      OnControlTextChanged();
    }

    /// <summary>
    /// Assigns the given back color to control.
    /// Also updates the control.
    /// </summary>
    private void AssignBackColor(Color value)
    {
      mBackColor = value;
      InvalidateForReason(EventTypes.BackColorChanged);
    }

    /// <summary>
    /// Modifies the text alignment. Also updates the control
    /// </summary>
    private void AssignTextAlign(Alignment value)
    {
      mTextAlign = value;
      mTextRenderer.Justify = mTextAlign;
      InvalidateForReason(EventTypes.TextChanged);
    }

    /// <summary>
    /// Updates control's right to left layout.
    /// Also updates the control.
    /// </summary>
    /// <param name="value"></param>
    private void AssignRightToLeft(bool value)
    {
      mRightToLeft = value;
      mTextRenderer.RightToLeft = value;
      mTextAlign = mRightToLeft ? Alignment.MiddleRight : Alignment.MiddleLeft;
      InvalidateForReason(EventTypes.RightToLeftChanged);
    }
    #endregion Internal Utilities


    #region Utilities For Subclasses
    /// <summary>
    /// Called to get a hint for control's size. If specified other
    /// than (0, 0) then controls size will be fixated to the given
    /// value.
    /// </summary>
    protected virtual Size QueryPrefferedSize()
    {
      return new Size(0, 0);
    }

    /// <summary>
    /// Draws the control's text on the given canvas and output rect.
    /// </summary>
    /// <param name="g">Canvas to paint on</param>
    /// <param name="l">Output rectangle's left</param>
    /// <param name="t">Output rectangle's top</param>
    /// <param name="w">Output rectangle's width</param>
    /// <param name="h">Output rectangle's height</param>
    protected void DrawText(Graphics g, int l = 0, int t = 0, int w = 0, int h = 0)
    {
      if (w == 0)
        w = Width;

      if (h == 0)
        h = Height;

      mTextRenderer.Canvas = g;
      mTextRenderer.Left = l;
      mTextRenderer.Top = t;
      mTextRenderer.Width = w;
      mTextRenderer.Height = h;
      mTextRenderer.Draw();
    }

    /// <summary>
    /// Called whenever the control's text property has changed.
    /// </summary>
    protected virtual void OnControlTextChanged()
    {
      // Create control view if specified
      InvalidateForReason(EventTypes.TextChanged);
    }
    #endregion Utilities For Subclasses


    #region Graphical Update System
    #region Internal Procedures
    private bool mValidationPending = true;
    private Graphics mCanvas = null;
    private BehaviorParams mBehavior = null;

    private Bitmap mView = null;
    /// <summary>
    /// Returns the rendered image of the control. Will not waste
    /// memory and performance, returns just the instance.
    /// </summary>
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public Image RenderedView
    {
      get { return mView; }
    }

    /// <summary>
    /// This method checks for the given reason to be logical
    /// for invalidation. If so control will be invalidated.
    /// </summary>
    /// <param name="reason">Invalidation reason</param>
    private void InvalidateForReason(EventTypes reason)
    {
      if (mBehavior != null && mBehavior.InvalidationEvents.Contains(reason))
        RequestRepaint();
    }
    #endregion Interal Procedures


    #region Overridable Functionality
    /// <summary>
    /// Creates a default graphical behavior for the control.
    /// </summary>
    protected virtual BehaviorParams CreateBehavior
    {
      get 
      {
        BehaviorParams result = new BehaviorParams();
        result.InvalidationEvents.Add(EventTypes.SizeChanged);
        return result;
      }
    }

    /// <summary>
    /// This method is called by RecreateControlView to actually create
    /// control's output view. Whatever drawn on the given canvas, will
    /// be shown whenever the control is invalidated.
    /// </summary>
    /// <param name="e">Recreation arguments. Holding output bitmap and canvas.</param>
    protected virtual void OnRecreateView(RecreateArgs e)
    {
      // Simple output for the base class.
      // Clears background and draws a rect.
      e.Canvas.FillRectangle(Brushes.White, new Rectangle(0, 0, ClientSize.Width - 1, ClientSize.Height - 1));
      e.Canvas.DrawRectangle(Pens.Black, new Rectangle(0, 0, ClientSize.Width - 1, ClientSize.Height - 1));
    }
    #endregion Overridable Functionality


    #region Functionality
    /// <summary>
    /// This method repaints visible areas starting 
    /// from Form and ending with the control.
    /// The given coordinates shall be in Form coordinates.
    /// </summary>
    private void InvalidateParents(Rectangle rect)
    {
      // Invalidate the clip rectangle for the control
      SessionAwareForm form = this.GetParentForm();
      if (form != null)
        form.Invalidate(rect, true);
    }

    /// <summary>
    /// This method is used to signal the Graphical Update System to
    /// actually draw the control on its form.
    /// </summary>
    protected void RequestRepaint()
    {
      // Signal validation is needed
      mValidationPending = true;
      InvalidateParents(this.GetFormCoordinates());
    }

    protected void RecreateBehavior()
    {
      mBehavior = CreateBehavior;
      RequestRepaint();
    }

    /// <summary>
    /// This method is called whenever the control needs to
    /// actually repaint itself and create new user interface
    /// view. Automatically allocates all needed resources and
    /// calls OnRecreateControlView() to actaully paint the control.
    /// </summary>
    private void RecreateView()
    {
      try
      {
        // The control will not draw unless its handle is created.
        if (!IsHandleCreated)
          return;

        // What to draw when not visible?
        if (!Visible)
          return;

        // 1 - Clean up
        if (mCanvas != null)
        {
          mCanvas.Dispose();
          mView.Dispose();
        }

        // 2 - Check for proper size of the control
        if (Width <= 0 || Height <= 0)
          return;

        // 3 - Create graphical tools
        mView = new Bitmap(Width, Height);
        mCanvas = Graphics.FromImage(mView);

        // 4 - Enable anti aliasing
        if (mBehavior.RequiresAntiAlias)
        {
          // Copy the control's background. make it so that when
          // operations using anti aliasing are going to be done
          // they know what color is behind each pixel.
          SessionAwareForm form = this.GetParentForm();
          if (form != null)
          {
            VisualRenderer vr = new VisualRenderer(this, new Rectangle(0, 0, Width, Height));
            vr.RenderToTexture(mCanvas);
          }
        }

        // 4 - Paint the control
        OnRecreateView(new RecreateArgs(mView, mCanvas));

        // 5 - Invalidate control
        mValidationPending = false;
        InvalidateParents(this.GetFormCoordinates());
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "ControlBase", "RecreateControlView");
        throw;
      }
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      // Does it need validating?
      if (mValidationPending || mView == null)
        RecreateView();

      if (mView != null)
        e.Graphics.DrawImageUnscaled(0, 0, mView);
      else
      {
        // In case not validated
        e.Graphics.FillRectangle(new SolidBrush(BackColor), new Rectangle(0, 0, ClientSize.Width - 1, ClientSize.Height - 1));
        e.Graphics.DrawRectangle(new Pen(new SolidBrush(ForeColor)), new Rectangle(0, 0, ClientSize.Width - 1, ClientSize.Height - 1));
      }
    }
    #endregion Functionality
    #endregion Graphical Update System
  }
}
