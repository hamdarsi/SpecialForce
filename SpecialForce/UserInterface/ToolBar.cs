﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SpecialForce
{
  public partial class ToolBar : Panel
  {
    #region Resources
    private Bitmap mImage = null;
    #endregion Resources


    #region Properties
    private int mCornerRadius = 6;
    [DefaultValue(6)]
    public int CornerRadius
    {
      get { return mCornerRadius; }
      set { mCornerRadius = value; DrawBar(); }
    }

    private bool mShowBorder = true;
    [DefaultValue(true)]
    public bool ShowBorder
    {
      get { return mShowBorder; }
      set { mShowBorder = value; DrawBar(); }
    }

    private float mBorderWidth = 2.0f;
    [DefaultValue(2.0f)]
    public float BorderWidth
    {
      get { return mBorderWidth; }
      set { mBorderWidth = value; DrawBar(); }
    }

    private Color mBorderColor = Color.FromArgb(255, 255, 255, 255);
    [DefaultValue(typeof(Color), "0xFFFFFFFF")]
    public Color BorderColor
    {
      get { return mBorderColor; }
      set { mBorderColor = value; DrawBar(); }
    }

    private bool mShowGradient = false;
    [DefaultValue(false)]
    public bool ShowGradient
    {
      get { return mShowGradient; }
      set { mShowGradient = value; DrawBar(); }
    }

    private Color mGradientStartColor = Color.FromArgb(255, 211, 224, 239);
    [DefaultValue(typeof(Color), "0xFFD3E0EF")]
    public Color GradientStartColor
    {
      get { return mGradientStartColor; }
      set { mGradientStartColor = value; DrawBar(); }
    }

    private Color mGradientEndColor = Color.FromArgb(255, 43, 92, 140);
    [DefaultValue(typeof(Color), "0xFF2B5C8C")]
    public Color GradientEndColor
    {
      get { return mGradientEndColor; }
      set { mGradientEndColor = value; DrawBar(); }
    }
    #endregion Properties


    #region Constructor
    public ToolBar()
    {
    }
    #endregion Constructor


    #region Overrides
    protected override CreateParams CreateParams
    {
      get
      {
        CreateParams prm = base.CreateParams;
        prm.ExStyle |= 0x20;
        return prm;
      }
    }

    protected override void OnBackColorChanged(EventArgs e)
    {
 	    base.OnBackColorChanged(e);
      DrawBar();
    }

    protected override void OnFontChanged(EventArgs e)
    {
      base.OnFontChanged(e);
      DrawBar();
    }

    protected override void OnSizeChanged(EventArgs e)
    {
 	    base.OnSizeChanged(e);
      DrawBar();
    }

    protected override void OnForeColorChanged(EventArgs e)
    {
      base.OnForeColorChanged(e);
      DrawBar();
    }

    protected override void OnInvalidated(InvalidateEventArgs e)
    {
      base.OnInvalidated(e);
      foreach (Control ctrl in Controls)
        if (ctrl is ControlBase)
          ctrl.Invalidate();
    }

    protected override void OnPaintBackground(PaintEventArgs e)
    {
      // Please, do not paint your background!
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      // Render the toolbar
      VisualRenderer vr = new VisualRenderer(this, e.ClipRectangle);
      vr.Render(e.Graphics);

      // Invalidate controls under it.
      foreach (Control ctrl in Controls)
        if (ctrl is CheckedListBox)
          ctrl.Invalidate();
        else if (ctrl is ListView)
          ctrl.Refresh();
        else if (ctrl is ListBox)
          ctrl.Invalidate();
        else if (ctrl is TextBox)
          ctrl.Invalidate();
    }
    #endregion Overrides


    #region Utilities
    private void DrawBar()
    {
      if (mImage != null)
        mImage.Dispose();

      mImage = new Bitmap(Width, Height);
      Graphics canvas = Graphics.FromImage(mImage);
      float delta = mBorderWidth / 2;

      if (mShowGradient)
      {
        Point start = new Point(Width / 2, 0);
        Point end = new Point(Width / 2, Height);
        Brush gradient = new LinearGradientBrush(start, end, mGradientStartColor, mGradientEndColor);
        canvas.FillRoundedRectangle(gradient, delta - 1, delta - 1, Width - mBorderWidth - 1, Height - BorderWidth - 1, mCornerRadius);
      }

      if (mShowBorder)
      {
        Pen border_pen = new Pen(mBorderColor, mBorderWidth);
        canvas.DrawRoundedRectangle(border_pen, delta, delta, Width - mBorderWidth - 1, Height - mBorderWidth - 1, mCornerRadius);
      }

      if (Parent != null)
        Parent.Invalidate(true);
      else
        Invalidate();
    }

    /// <summary>
    /// Returns the panel's drawn background.
    /// Will not waste memory and performance, returns just the instance.
    /// </summary>
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public Image RenderedView
    {
      get { return mImage; }
    }
    #endregion Utilities
  }
}
