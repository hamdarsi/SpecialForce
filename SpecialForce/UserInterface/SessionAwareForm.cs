﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce
{
  public partial class SessionAwareForm : Form
  {
    #region Data
    // Background base image will be assigned only once, since its a huge image
    static private Image mBackGroundBaseImage = Properties.Resources.BackGround;
    private SessionAwareForm mParentForm = null;
    private Bitmap mBackGround = null;
    #endregion Data


    #region Properties
    public int TitleBarHeight
    {
      get
      {
        Rectangle rect = RectangleToScreen(this.ClientRectangle);
        return rect.Top - this.Top;
      }
    }

    public int BorderWidth
    {
      get
      {
        Rectangle rect = RectangleToScreen(this.ClientRectangle);
        return 2 * Math.Abs(rect.Left - this.Left);
      }
    }

    private bool mEmptyBackground = false;
    public bool EmptyBackground
    {
      get { return mEmptyBackground; }
      set { mEmptyBackground = true; Invalidate(); }
    }

    private Bitmap mSelectedBackGround;
    public Bitmap BackGroundPicture
    {
      get { return mSelectedBackGround; }
      set { mSelectedBackGround = value; }
    }
    #endregion Properties


    #region Constructor
    public SessionAwareForm()
    {
      Session.OnSessionChanged += OnSessionChanged;

      InitializeComponent();
    }

    ~SessionAwareForm()
    {
      Session.OnSessionChanged -= OnSessionChanged;
    }
    #endregion Constructor


    #region Overrides
    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);

      Icon = Properties.Resources.ApplicationIcon;
      if (mParentForm == null && !Session.DevelopMode)
        CenterScreen();
    }

    protected override void OnClosing(CancelEventArgs e)
    {
      base.OnClosing(e);
      if (mParentForm != null && mParentForm.IsDisposed == false)
      {
        mParentForm.Left = (Screen.PrimaryScreen.WorkingArea.Width - mParentForm.Width) / 2;
        mParentForm.Top = (Screen.PrimaryScreen.WorkingArea.Height - mParentForm.Height) / 2;
      }
    }

    protected override void OnSizeChanged(EventArgs e)
    {
      if (Width > 0 && Height > 0 && WindowState != FormWindowState.Minimized)
      {
        if (mBackGround != null)
          mBackGround.Dispose();

        if (mSelectedBackGround != null)
          mBackGround = mSelectedBackGround.Resize(ClientSize);
        else
          mBackGround = mBackGroundBaseImage.Resize(ClientSize);

        // Invalidate all control base controls
        foreach (Control ctrl in Controls)
          if (ctrl is ControlBase || ctrl is Panel)
            ctrl.Invalidate(true);
      }
      base.OnSizeChanged(e);
    }

    protected override void OnControlRemoved(ControlEventArgs e)
    {
      base.OnControlRemoved(e);
      Invalidate(true);
    }

    protected override void OnControlAdded(ControlEventArgs e)
    {
      base.OnControlAdded(e);
      Invalidate(true);
    }

    protected override void OnShown(EventArgs e)
    {
      try
      {
        base.OnShown(e);

        if (!Session.DevelopMode && Session.CurrentStaff != null)
          ApplyPermissions(Session.CurrentStaff.Post);
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "SessionAwareForm", "OnShown");
      }
    }

    protected override void OnPaintBackground(PaintEventArgs e)
    {
      if(mEmptyBackground)
        e.Graphics.Clear(BackColor);
      else
        RenderFormBackGround(e);
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      if (mEmptyBackground)
        e.Graphics.Clear(BackColor);
      else
        RenderFormBackGround(e);
    }
    #endregion Overrides


    #region Subscribed Event Handlers
    private void OnSessionChanged(object sender, EventArgs e)
    {
      ApplyPermissions(Session.CurrentStaff.Post);
    }

    private void ParentFormClosing(Object sender, EventArgs e)
    {
      mParentForm.FormClosing -= ParentFormClosing;
      Close();
    }
    #endregion Subscribed Event Handlers


    #region Abstractions
    protected virtual void ApplyPermissions(Post post)
    {
    }
    #endregion Abstractions


    #region Graphical Update System
    /// <summary>
    /// Checks whether drawing needs to reverse origin coordinates or not
    /// </summary>
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Browsable(false)]
    public bool RightToLeftDrawing
    {
      get { return RightToLeft == RightToLeft.Yes && RightToLeftLayout; }
    }

    /// <summary>
    /// Transforms the given x to a globally LTR x.
    /// </summary>
    public int LTRLeft(int left, int w = 0)
    {
      return
        RightToLeftDrawing ?
        ClientSize.Width - left - w :
        left;
    }

    /// <summary>
    /// Transforms the given rectangle if needed, in such a way
    /// that the resulting rectangle would be in Left-To-Right
    /// direction coordinates.
    /// </summary>
    public Rectangle GetDrawTarget(Rectangle rect)
    {
      return new Rectangle(
        LTRLeft(rect.Left, rect.Width),
        rect.Top,
        rect.Width,
        rect.Height);
    }

    /// <summary>
    /// Returns the background image of the form. It's "View"
    /// Returns the actual instance. Does not waste a copy.
    /// </summary>
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public Image RenderedView
    {
      get { return mBackGround; }
    }

    /// <summary>
    /// This method is called by OnPaint and OnPaintBackGround
    /// events to render form background area.
    /// </summary>
    private void RenderFormBackGround(PaintEventArgs e)
    {
      // Master transformation!
      if (RightToLeftDrawing)
      {
        e.Graphics.TranslateTransform(ClientSize.Width - 1, 0);
        e.Graphics.ScaleTransform(-1.0f, 1.0f);
      }

      // Render the background
      VisualRenderer vr = new VisualRenderer(this, e.ClipRectangle);
      vr.Render(e.Graphics);
    }
    #endregion Graphical Update System


    #region Utilities
    /// <summary>
    /// Centers the form on the screen.
    /// </summary>
    public void CenterScreen()
    {
      Left = (Screen.PrimaryScreen.WorkingArea.Width - Width) / 2;
      Top = (Screen.PrimaryScreen.WorkingArea.Height - Height) / 2;
    }

    /// <summary>
    /// This given Form instance will be shown modally, while this instance
    /// is hidden. When the shown form is modally closed, its result will be
    /// returned and the form will close. If specified, after the modal form
    /// being closed, this instance will close too, otherwise it will be shown
    /// in screen center.
    /// </summary>
    /// <param name="frm">The modal form</param>
    /// <param name="close_after">To close this form after the modal form or just show it.</param>
    /// <returns>The modal form's modal result.</returns>
    public DialogResult ShowModal(Form frm, bool close_after)
    {
      Hide();
      DialogResult result = frm.ShowDialog();
      if (close_after)
        Close();
      else if (!IsDisposed)
      {
        CenterScreen();
        Show();
      }

      return result;
    }

    /// <summary>
    /// Shows the form bounded to the given form instance. The given instance
    /// will be assumed caretaker of this one. This one will be shown right
    /// next to the caretaker and both forms will be centered on screen. Upon
    /// closing the caretaker, this one will be closed too. When this form is
    /// closed, the caretaker will be placed in screen center.
    /// </summary>
    /// <param name="parent">Caretaker form</param>
    public void ShowBounded(SessionAwareForm parent)
    {
      mParentForm = parent;

      Show();
      BringToFront();

      mParentForm.Left = (Screen.PrimaryScreen.WorkingArea.Width - Width - mParentForm.Width) / 2;
      Left = mParentForm.Right;
      mParentForm.Top = (Screen.PrimaryScreen.WorkingArea.Height - mParentForm.Height) / 2;
      Top = (Screen.PrimaryScreen.WorkingArea.Height - Height) / 2;

      mParentForm.FormClosing += ParentFormClosing;
    }
    #endregion Utilities
  }
}
