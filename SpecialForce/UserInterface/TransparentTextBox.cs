﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace SpecialForce
{
  public partial class TransparentTextBox : TextBox
  {
		private InternalPictureBox myPictureBox;
		private bool mValidated = false;
		private bool mCaretUpToDate = false;
		private Bitmap mBitmap;
		private Bitmap mAlphaBitmap;
		private int mFontHeight = 10;
		private Timer mTimer;
		private bool mCaretState = true;
		private bool mPaintedFirstTime = false;

    private Bitmap mBackImage = null;
    public Bitmap BackImage
    {
      get { return mBackImage; }
      set
      {
        mBackImage = value;
        InvalidateControl();
      }
    }

		private Color mBackColor = Color.White;
    public new Color BackColor
    {
      get
      {
        return Color.FromArgb(base.BackColor.R, base.BackColor.G, base.BackColor.B);
      }
      set
      {
        mBackColor = value;
        base.BackColor = value;
        InvalidateControl();
      }
    }

    [
      Category("Appearance"),
      Description("The alpha value used to blend the control's background. Valid values are 0 through 255."),
      Browsable(true),
      DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)
    ]
    private int mBackAlpha = 10;
    public int BackAlpha
    {
      get { return mBackAlpha; }
      set
      {
        mBackAlpha = Math.Min(value, 255);
        InvalidateControl();
      }
    }

    public new BorderStyle BorderStyle
    {
      get { return base.BorderStyle; }
      set
      {
        if (mPaintedFirstTime)
          SetStyle(ControlStyles.UserPaint, false);

        base.BorderStyle = value;

        if (mPaintedFirstTime)
          SetStyle(ControlStyles.UserPaint, true);

        mBitmap = null;
        mAlphaBitmap = null;
        InvalidateControl();
      }
    }

    public override bool Multiline
    {
      get { return base.Multiline; }
      set
      {
        if (mPaintedFirstTime)
          SetStyle(ControlStyles.UserPaint, false);

        base.Multiline = value;

        if (mPaintedFirstTime)
          SetStyle(ControlStyles.UserPaint, true);

        mBitmap = null;
        mAlphaBitmap = null;
        InvalidateControl();
      }
    }

    public TransparentTextBox()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			BackColor = mBackColor; 

			SetStyle(ControlStyles.UserPaint,false);
			SetStyle(ControlStyles.AllPaintingInWmPaint,true);
			SetStyle(ControlStyles.DoubleBuffer,true);

			myPictureBox = new InternalPictureBox();
			Controls.Add(myPictureBox);
			myPictureBox.Dock = DockStyle.Fill;
		}


		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			mBitmap = new Bitmap(ClientRectangle.Width,ClientRectangle.Height);
			mAlphaBitmap = new Bitmap(ClientRectangle.Width,ClientRectangle.Height);
			InvalidateControl();
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);
			InvalidateControl();
		}

		protected override void OnKeyUp(KeyEventArgs e)
		{
			base.OnKeyUp(e);
			InvalidateControl();
		}

		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			base.OnKeyPress(e);
			InvalidateControl();
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.OnMouseUp(e);
			InvalidateControl();
		}

		protected override void OnGiveFeedback(GiveFeedbackEventArgs gfbevent)
		{
			base.OnGiveFeedback(gfbevent);
			InvalidateControl();
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			Point cursor = FindForm().PointToClient(Cursor.Position); 
			if(!Bounds.Contains(cursor))
				base.OnMouseLeave(e);
		}		
		
		protected override void OnChangeUICues(UICuesEventArgs e)
		{
			base.OnChangeUICues(e);
			InvalidateControl();
		}
		
		protected override void OnGotFocus(EventArgs e)
		{
			base.OnGotFocus(e);
			mCaretUpToDate = false;
			InvalidateControl();
			
			mTimer = new System.Windows.Forms.Timer(components);
			mTimer.Interval =(int) Win32.GetCaretBlinkTime(); //  usually around 500;
			
			mTimer.Tick +=new EventHandler(myTimer1_Tick);
			mTimer.Enabled = true;
		}

		protected override void OnLostFocus(EventArgs e)
		{
			base.OnLostFocus(e);
			mCaretUpToDate = false;
			InvalidateControl();

			mTimer.Dispose();
		}

		protected override void OnFontChanged(EventArgs e)
		{
			if(mPaintedFirstTime)
				SetStyle(ControlStyles.UserPaint,false);

			base.OnFontChanged(e);

			if(mPaintedFirstTime)
				SetStyle(ControlStyles.UserPaint,true);

			mFontHeight = GetFontHeight();
			InvalidateControl();
		}

		protected override void OnTextChanged(EventArgs e)
		{
			base.OnTextChanged(e);
			InvalidateControl();
		}

		protected override void WndProc(ref Message m)
		{
			base.WndProc(ref m);

      switch (m.Msg)
      {
        case Win32.WM_PAINT:
				  mPaintedFirstTime = true;

				  if(!mValidated || !mCaretUpToDate)
					  GetBitmaps();

				  mValidated = true;
				  mCaretUpToDate = true;

				  if(myPictureBox.Image != null)
            myPictureBox.Image.Dispose();

				  myPictureBox.Image =(Image)mAlphaBitmap.Clone();
          break;

        case Win32.WM_HSCROLL:
        case Win32.WM_VSCROLL:
	        InvalidateControl();
          break;

        case Win32.WM_LBUTTONDOWN:
				case Win32.WM_RBUTTONDOWN:
        case Win32.WM_LBUTTONDBLCLK:
				  InvalidateControl();
          break;

        case Win32.WM_MOUSEMOVE:
				  if(m.WParam.ToInt32() != 0)  //shift key or other buttons
					  InvalidateControl();
          break;

        default:
          break;
      }
		}

		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private int GetFontHeight()
		{
			Graphics g = CreateGraphics();
			SizeF sf_font = g.MeasureString("X",Font);
			g.Dispose();
			return (int) sf_font.Height;
		}

    private void InvalidateControl()
    {
      mValidated = false;
      Invalidate();
    }

		private void GetBitmaps()
		{

			if(mBitmap == null ||
         mAlphaBitmap == null ||
         mBitmap.Width != Width || 
         mBitmap.Height != Height ||
         mAlphaBitmap.Width != Width ||
         mAlphaBitmap.Height != Height)
			{
				mBitmap = null;
				mAlphaBitmap = null;
			}

		  if(mBitmap == null)
			{
				mBitmap = new Bitmap(ClientRectangle.Width,ClientRectangle.Height);//(Width,Height);
				mValidated = false;
			}

			if(!mValidated)
			{
        //Capture the TextBox control window
				SetStyle(ControlStyles.UserPaint,false);
				
				Win32.CaptureWindow(this,ref mBitmap);

				SetStyle(ControlStyles.UserPaint,true);
				SetStyle(ControlStyles.SupportsTransparentBackColor,true);
				BackColor = Color.FromArgb(mBackAlpha,mBackColor);
			}
		
			Rectangle r2 = new Rectangle(0,0,ClientRectangle.Width,ClientRectangle.Height);
			ImageAttributes tempImageAttr = new ImageAttributes();
						
			//Found the color map code in the MS Help
			ColorMap[] tempColorMap = new ColorMap[1];
			tempColorMap[0] = new ColorMap();
			tempColorMap[0].OldColor = Color.FromArgb(255,mBackColor); 
			tempColorMap[0].NewColor = Color.FromArgb(mBackAlpha,mBackColor);
			tempImageAttr.SetRemapTable(tempColorMap);

			if(mAlphaBitmap != null)
				mAlphaBitmap.Dispose();
					
			mAlphaBitmap = new Bitmap(ClientRectangle.Width,ClientRectangle.Height);//(Width,Height);
			Graphics tempGraphics1 = Graphics.FromImage(mAlphaBitmap);

      if (mBackImage != null)
        tempGraphics1.DrawImage(mBackImage, 0, 0, ClientRectangle.Width, ClientRectangle.Height);

      tempGraphics1.DrawImage(mBitmap, r2, 0, 0, ClientRectangle.Width, ClientRectangle.Height, GraphicsUnit.Pixel, tempImageAttr);
			tempGraphics1.Dispose();

			if(Focused &&(SelectionLength == 0))
			{
				Graphics tempGraphics2 = Graphics.FromImage(mAlphaBitmap);
				if(mCaretState)
				{
					//Draw the caret
					Point caret = findCaret();
					Pen p = new Pen(ForeColor,3);
					tempGraphics2.DrawLine(p,caret.X,caret.Y + 0,caret.X,caret.Y + mFontHeight);
					tempGraphics2.Dispose();
				}
			}
		}

		private Point findCaret() 
		{
			/*  Find the caret translated from code at 
			 * http://www.vb-helper.com/howto_track_textbox_caret.html
			 * 
			 * and 
			 * 
			 * http://www.microbion.co.uk/developers/csharp/textpos2.htm
			 * 
			 * Changed to EM_POSFROMCHAR
			 * 
			 * This code still needs to be cleaned up and debugged
			 * */

			Point pointCaret = new Point(0);
			int i_char_loc = SelectionStart;
			IntPtr pi_char_loc = new IntPtr(i_char_loc);

			int i_point = Win32.SendMessage(Handle,Win32.EM_POSFROMCHAR,pi_char_loc,IntPtr.Zero);
			pointCaret = new Point(i_point);

			if(i_char_loc == 0) 
			{
				pointCaret = new Point(0);
			}
			else if(i_char_loc >= Text.Length)
			{
				pi_char_loc = new IntPtr(i_char_loc - 1);
				i_point = Win32.SendMessage(Handle,Win32.EM_POSFROMCHAR,pi_char_loc,IntPtr.Zero);
				pointCaret = new Point(i_point);

				Graphics g = CreateGraphics();
				String t1 = Text.Substring(Text.Length-1,1) + "X";
				SizeF sizet1 = g.MeasureString(t1,Font);
				SizeF sizex  = g.MeasureString("X",Font);
				g.Dispose();
				int xoffset =(int)(sizet1.Width - sizex.Width);
				pointCaret.X = pointCaret.X + xoffset;

				if(i_char_loc == Text.Length)
				{
					String slast = Text.Substring(Text.Length-1,1);
					if(slast == "\n")
					{
						pointCaret.X = 1;
						pointCaret.Y = pointCaret.Y + mFontHeight;
					}
				}
			}

			return pointCaret;
		}


		private void myTimer1_Tick(object sender, EventArgs e)
		{
			//Timer used to turn caret on and off for focused control
			mCaretState = !mCaretState;
			mCaretUpToDate = false;
			Invalidate();
		}

		private class InternalPictureBox : PictureBox 
		{
			public InternalPictureBox() 
			{
				SetStyle(ControlStyles.Selectable,false);
				SetStyle(ControlStyles.UserPaint,true);
				SetStyle(ControlStyles.AllPaintingInWmPaint,true);
				SetStyle(ControlStyles.DoubleBuffer,true);

				Cursor = null;
				Enabled = true; 
				SizeMode = PictureBoxSizeMode.Normal;
			}

			protected override void WndProc(ref Message m)
			{
				if(m.Msg == Win32.WM_LBUTTONDOWN   ||
					  m.Msg == Win32.WM_RBUTTONDOWN   ||
					  m.Msg == Win32.WM_LBUTTONDBLCLK ||
					  m.Msg == Win32.WM_MOUSELEAVE    ||
					  m.Msg == Win32.WM_MOUSEMOVE)
				{
					//Send the above messages back to the parent control
					Win32.PostMessage(Parent.Handle,(uint) m.Msg,m.WParam,m.LParam);
				}

				else if(m.Msg == Win32.WM_LBUTTONUP)
				{
					//??  for selects and such
					Parent.Invalidate();
				}

				base.WndProc(ref m);
			}
		}
	}
}
