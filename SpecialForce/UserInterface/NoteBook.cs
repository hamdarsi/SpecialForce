﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SpecialForce.UserInterface
{
  public partial class NoteBook : ControlBase
  {
    #region Data
    private int mCurrentPage = -1;
    private List<DB.Note> mNotes = new List<DB.Note>();
    #endregion Data


    #region Resources
    private ToolButton mApplyButton;
    private ToolButton mDeleteButton;
    private ToolButton mNewButton;
    private ToolButton mPreviousButton;
    private ToolButton mNextButton;
    private TransparentTextBox mTextBox = new TransparentTextBox();
    #endregion Resources


    #region Properties
    private int mUserID = -1;
    public int UserID
    {
      get { return mUserID; }
      set { LoadUserNotes(value); }
    }
    #endregion Properties


    #region Constructor
    public NoteBook()
    {
      mTextBox.Parent = this;
      mTextBox.BorderStyle = BorderStyle.None;
      mTextBox.BackAlpha = 0;
      mTextBox.Multiline = true;
      mTextBox.Location = new Point(0, 24);
      mTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      mTextBox.TextChanged += TextBoxTextChanged;

      mApplyButton = CreateButton(4);
      mApplyButton.Image = Properties.Resources.NoteBook_Tick;
      mApplyButton.Click += SaveButtonClicked;
      mApplyButton.Hint = "ذخیره یادداشت";

      mNewButton = CreateButton(24);
      mNewButton.Image = Properties.Resources.NoteBook_Plus;
      mNewButton.Click += NewNoteClicked;
      mNewButton.Hint = "یادداشت جدید";

      mDeleteButton = CreateButton(44);
      mDeleteButton.Image = Properties.Resources.NoteBook_Delete;
      mDeleteButton.Click += DeleteClicked;
      mDeleteButton.Hint = "حذف یادداشت";

      mPreviousButton = CreateButton(64);
      mPreviousButton.Image = Properties.Resources.NoteBook_Next;
      mPreviousButton.Click += PreviousClicked;
      mPreviousButton.Hint = "یادداشت قبلی";

      mNextButton = CreateButton(84);
      mNextButton.Image = Properties.Resources.NoteBook_Previous;
      mNextButton.Click += NextClicked;
      mNextButton.Hint = "یادداشت بعدی";

      Size = new Size(400, 300);
      Font = new System.Drawing.Font("B Nazanin", 12F);
      TextAlign = Alignment.TopMiddle;
      UpdateButtonStates();
    }
    #endregion Constructor


    #region Graphical Implementations
    protected override BehaviorParams CreateBehavior
    {
      get
      {
        BehaviorParams prms = base.CreateBehavior;
        prms.InvalidationEvents.Add(EventTypes.BackColorChanged);
        prms.InvalidationEvents.Add(EventTypes.SizeChanged);
        prms.InvalidationEvents.Add(EventTypes.LocationChanged);
        prms.RendersText();
        return prms;
      }
    }

    protected override void OnRecreateView(RecreateArgs e)
    {
      LinearGradientBrush brush = new LinearGradientBrush(ClientRectangle, ControlPaint.LightLight(BackColor), BackColor, 90);
      e.Canvas.FillRectangle(brush, ClientRectangle);
      e.Canvas.FillRectangle(new SolidBrush(BackColor), 0, 0, ClientRectangle.Width, 24);
      DrawText(e.Canvas);
    }

    protected override void OnResize(EventArgs e)
    {
      mTextBox.Size = new Size(ClientRectangle.Width, ClientRectangle.Height - 24);
      mNextButton.Left = ClientRectangle.Width - 20;
      mPreviousButton.Left = mNextButton.Left - 20;
      base.OnResize(e);
    }

    private ToolButton CreateButton(int left)
    {
      ToolButton btn = new ToolButton();
      btn.Parent = this;
      btn.Left = left;
      btn.Top = 4;
      btn.ShowBorder = false;
      return btn;
    }
    #endregion Graphical Implementations


    #region Event Handlers
    /// <summary>
    /// Called whenever the textbox's text is changed.
    /// Updates button states.
    /// </summary>
    private void TextBoxTextChanged(object sender, EventArgs e)
    {
      UpdateButtonStates();
    }

    /// <summary>
    /// When the new note button is clicked. Switches to the new note page.
    /// </summary>
    private void NewNoteClicked(Object sender, EventArgs e)
    {
      string text = "";
      if (mCurrentPage == 0 && mTextBox.Text.Length != 0)
        if (!Session.Ask("در صفحه یادداشت جدید متنی را نوشته اید." + Environment.NewLine + " آیا میخواهید آن را پاک کنید؟"))
          text = mTextBox.Text;

      SetCurrentPage(0);
      if (text != "")
        mTextBox.Text = text;
    }

    /// <summary>
    /// Called when delete button is clicked. If the current
    /// pages is the first one, then just clean the text box.
    /// Otherwise save the note, mark it as deleted and 
    /// remove it from the list.
    /// </summary>
    private void DeleteClicked(Object sender, EventArgs e)
    {
      if (!Session.Ask("آیا میخواهید یادداشت را پاک کنید؟"))
        return;

      if (mCurrentPage != 0)
      {
        // Save the buffer
        mNotes[mCurrentPage].Memo = mTextBox.Text;
        mNotes[mCurrentPage].Apply();

        // Now mark it as deleted
        mNotes[mCurrentPage].Delete();
        mNotes.RemoveAt(mCurrentPage);

        // Change the current page
        if (mCurrentPage == mNotes.Count)
          SetCurrentPage(mCurrentPage - 1);
        else
          SetCurrentPage(mCurrentPage);
      }
      else
        mTextBox.Clear();
    }

    /// <summary>
    /// Called when apply button is clicked. If the
    /// current pages text is empty, it will be deleted.
    /// Otherwise is will be saved.
    /// </summary>
    private void SaveButtonClicked(Object sender, EventArgs e)
    {
      if (mTextBox.Text == "")
      {
        if (mCurrentPage == 0)
          return;

        DeleteClicked(sender, e);
        return;
      }

      mNotes[mCurrentPage].Memo = mTextBox.Text;
      mNotes[mCurrentPage].Apply();

      // It was a new note. Move it to the end of notes
      if (mCurrentPage == 0)
      {
        mNotes.Add(mNotes[0]);
        mNotes[0] = new DB.Note(mUserID);
        SetCurrentPage(mNotes.Count - 1);
      }

      Session.ShowMessage("یادداشت ذخیره شد");
    }

    /// <summary>
    /// Called when previous note button is clicked.
    /// </summary>
    private void PreviousClicked(Object sender, EventArgs e)
    {
      SetCurrentPage(mCurrentPage - 1);
    }

    /// <summary>
    /// Called when next button is clicked.
    /// </summary>
    private void NextClicked(Object sender, EventArgs e)
    {
      SetCurrentPage(mCurrentPage + 1);
    }
    #endregion Event Handlers


    #region Utilities
    /// <summary>
    /// This method loads notes for the given user. 
    /// Always inserts a new note in the beginning.
    /// </summary>
    /// <param name="id">User ID to load his notes</param>
    private void LoadUserNotes(int id)
    {
      mUserID = id;

      if (mUserID == -1)
      {
        mNotes.Clear();
        SetCurrentPage(-1);
      }
      else
      {
        mNotes = DB.Note.GetValidUserNotes(mUserID);
        mNotes.Insert(0, new DB.Note(mUserID));
        SetCurrentPage(mNotes.Count - 1);
      }
    }

    /// <summary>
    /// This method updates button states on the notebook
    /// </summary>
    private void UpdateButtonStates()
    {
      if (mUserID == -1)
      {
        mNextButton.Enabled = false;
        mPreviousButton.Enabled = false;
        mDeleteButton.Enabled = false;
        mNewButton.Enabled = false;
        mApplyButton.Enabled = false;
      }
      else
      {
        mNextButton.Enabled = mCurrentPage != mNotes.Count - 1;
        mPreviousButton.Enabled = mCurrentPage != 0;
        mDeleteButton.Enabled = mCurrentPage > 1 || mTextBox.Text.Length != 0;
        mApplyButton.Enabled = mTextBox.Text != mNotes[mCurrentPage].Memo;
        mNewButton.Enabled = mCurrentPage != 0 || mTextBox.Text.Length != 0;
      }
    }

    /// <summary>
    /// Sets the current page on the notebook. Shows its
    /// note's text and updates its title.
    /// </summary>
    /// <param name="index">Index of the page to show</param>
    private void SetCurrentPage(int index)
    {
      mCurrentPage = index;
      string strTitle = "";

      if (mCurrentPage != -1)
      {
        mTextBox.Text = mNotes[mCurrentPage].Memo;

        if (mCurrentPage != 0)
        {
          strTitle = "یادداشت ها: صفحه " + mCurrentPage.ToString();
          strTitle += " از " + (mNotes.Count - 1).ToString();
        }
        else
          strTitle = "یادداشت جدید";
      }
      else
        mTextBox.Text = "";

      Text = strTitle;
      UpdateButtonStates();
    }
    #endregion Utilities
  }
}
