﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SpecialForce.UserInterface
{
  public partial class Clock : ControlBase
  {
    #region Internal Data
    private const int mHalfSize = 62;
    private Point[] mMinute = new Point[3] { new Point(-3, 8), new Point(3, 8), new Point(0, -mHalfSize + 12) };
    private Point[] mHour = new Point[3] { new Point(-4, 8), new Point(4, 8), new Point(0, -mHalfSize + 30) };
    private Bitmap mFaceBitmap = Properties.Resources.ClockFace;
    private Bitmap mCenterBitmap = Properties.Resources.ClockCenter;
    private Timer mTimer = new Timer();
    #endregion Internal Data


    #region Constructors
    public Clock()
    {
      Size = mFaceBitmap.Size;
      mTimer.Interval = 10000;
      mTimer.Tick += TimerTicked;
    }
    #endregion Constructors


    #region Overrides
    protected override BehaviorParams CreateBehavior
    {
      get
      {
        BehaviorParams prms = base.CreateBehavior;
        prms.UsesAntiAlias();
        return prms;
      }
    }

    protected override Size QueryPrefferedSize()
    {
      return mFaceBitmap.Size;
    }

    protected override void OnRecreateView(RecreateArgs e)
    {
      e.Canvas.SmoothingMode = SmoothingMode.AntiAlias;
      e.Canvas.DrawImage(mFaceBitmap, 0, 0);

      DateTime dt = DateTime.Now;
      double h = dt.Hour + dt.Minute / 60.0, m = dt.Minute;
      if (h >= 12.0)
        h -= 12.0;

      double hour_angle = (h / 12.0) * (2 * Math.PI);
      double minute_angle = (m / 60.0) * (2 * Math.PI);
      Brush brush = new SolidBrush(Color.Black);

      Point[] hour = new Point[3] { Rotate(mHour[0], hour_angle), Rotate(mHour[1], hour_angle), Rotate(mHour[2], hour_angle) };
      e.Canvas.FillPolygon(brush, hour);

      Point[] minute = new Point[3] { Rotate(mMinute[0], minute_angle), Rotate(mMinute[1], minute_angle), Rotate(mMinute[2], minute_angle) };
      e.Canvas.FillPolygon(brush, minute);

      e.Canvas.DrawImageUnscaled(mHalfSize - 3, mHalfSize - 3, mCenterBitmap);
    }
    #endregion Overrides


    #region Utilities
    private Point Rotate(Point pt, double angle)
    {
      double r = Math.Sqrt(pt.X * pt.X + pt.Y * pt.Y);
      angle += Math.Atan2(pt.Y, pt.X);

      int x = TinyMath.Round(r * Math.Cos(angle)) + mHalfSize;
      int y = TinyMath.Round(r * Math.Sin(angle)) + mHalfSize;
      return new Point(x, y);
    }

    private void TimerTicked(object sender, EventArgs e)
    {
      RequestRepaint();
    }
    #endregion Utilities
  }
}
