﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce
{
  public partial class TransparentLabel : ControlBase
  {
    private bool mFixFromRight = false;
    [DefaultValue(false)]
    public bool FixFromRight
    {
      get { return mFixFromRight; }
      set { mFixFromRight = value; }
    }

    protected override BehaviorParams CreateBehavior
    {
      get
      {
        BehaviorParams p = base.CreateBehavior;
        p.RendersText();
        return p;
      }
    }

    public TransparentLabel()
    {
      InitializeComponent();
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      UpdateSize();
    }

    private void UpdateSize()
    {
      Size = TextSize;
    }

    protected override Size QueryPrefferedSize()
    {
      return TextSize;
    }

    protected override void OnFontChanged(EventArgs e)
    {
      UpdateSize();
      base.OnFontChanged(e);
    }

    protected override void OnControlTextChanged()
    {
      bool turn_back_on = Visible;
      if (mFixFromRight && Parent != null && Parent is Panel)
      {
        Visible = false;
        Left = Right - TextSize.Width;
      }

      UpdateSize();
      if (turn_back_on)
        Visible = true;

      base.OnControlTextChanged();
    }

    protected override void OnRecreateView(RecreateArgs e)
    {
      DrawText(e.Canvas);
    }
  }
}
