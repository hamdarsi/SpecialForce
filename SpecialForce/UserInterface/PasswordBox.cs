﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace SpecialForce.UserInterface
{
  public partial class PasswordBox : TextBox
  {
    public PasswordBox()
    {
      InitializeComponent();
    }

    protected override void WndProc(ref Message m)
    {
      if (m.Msg == 0x1503)
      { }
      else
        base.WndProc(ref m);
    }
  }
}
