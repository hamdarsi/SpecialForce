﻿using System;
using System.Drawing;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.ComponentModel;
using BrightIdeasSoftware;

namespace SpecialForce.UserInterface
{
  public partial class ListView : ObjectListView
  {
    private Hashtable mIndex = new Hashtable();

    private bool mAutoIndexRow = true;
    /// <summary>
    /// Sets whether to show index number on each row autmatically
    /// </summary>
    [DefaultValue(true)]
    public bool AutoIndexRow
    {
      get { return mAutoIndexRow; }
      set { mAutoIndexRow = value; }
    }

    public ListView()
    {
      ShowGroups = false;
      GridLines = true;
      RightToLeftLayout = true;
      FullRowSelect = true;
      AllowDrop = true;
      IsSimpleDragSource = true;
      ForeColor = Color.Black;

      TextOverlay textOverlay = EmptyListMsgOverlay as TextOverlay;
      textOverlay.Font = new System.Drawing.Font("B Nazanin", 12);
      textOverlay.TextColor = Color.Aqua;
      textOverlay.BackColor = Color.AntiqueWhite;
      textOverlay.BorderColor = Color.Black;
      textOverlay.BorderWidth = 1.25f;
      textOverlay.Rotation = -5;
    }

    private ContextMenuStrip mContextMenu = null;
    public override ContextMenuStrip ContextMenuStrip
    {
      get { return mContextMenu; }
      set { mContextMenu = value; }
    }

    public void SetAspect(int column_index, AspectGetterDelegate aspect)
    {
      AllColumns[column_index].AspectGetter = aspect;
    }

    public void SetEmptyMessage(string text, bool normal_view = true)
    {
      TextOverlay textOverlay = EmptyListMsgOverlay as TextOverlay;

      EmptyListMsg = text;
      if (normal_view)
      {
        textOverlay.TextColor = Color.Blue;
        textOverlay.Rotation = 0;
      }
      else
      {
        textOverlay.TextColor = Color.Firebrick;
        textOverlay.Rotation = -5;
      }
    }

    protected override void OnMouseDown(MouseEventArgs e)
    {
      base.OnMouseDown(e);
      if (mContextMenu != null && e.Button == System.Windows.Forms.MouseButtons.Right)
        mContextMenu.Show(Cursor.Position, ToolStripDropDownDirection.BelowLeft);
    }

    protected override void OnControlCreated()
    {
      base.OnControlCreated();

      foreach (OLVColumn c in AllColumns)
        c.TextAlign = HorizontalAlignment.Left;

      if (mAutoIndexRow && AllColumns.Count > 0)
        AllColumns[0].AspectGetter = o => (GetIndex(o) + 1).ToString();
    }

    /// <summary>
    /// SpecialForce addition: Indexes the given collection before passing to OLV
    /// </summary>
    /// <param name="collection">Collection to set</param>
    public override void SetObjects(System.Collections.IEnumerable collection)
    {
      int index = 0;
      mIndex.Clear();
      IEnumerator it = collection.GetEnumerator();
      while (it.MoveNext())
        mIndex.Add(it.Current, index++);

 	    base.SetObjects(collection);
    }

    /// <summary>
    /// Sets the given images as the image source for the specified column.
    /// </summary>
    /// <param name="index">Column index to display images</param>
    /// <param name="images">Array of images to be shown</param>
    public void SetImages(int index, Image [] images)
    {
      if (images.Length == 0)
        return;

      SmallImageList = new ImageList();
      LargeImageList = new ImageList();
      SmallImageList.ImageSize = images[0].Size;
      LargeImageList.ImageSize = images[0].Size;
      SmallImageList.Images.AddRange(images);
      LargeImageList.Images.AddRange(images);
      RowHeight = LargeImageList.ImageSize.Height;
      AllColumns[index].ImageGetter = obj => GetIndex(obj);
    }

    public void AddObject(object o, Image img = null)
    {
      mIndex.Add(o, mIndex.Count);

      if (img != null)
      {
        SmallImageList.Images.Add(img);
        LargeImageList.Images.Add(img);
      }

      AddObjects(new object[] { o });
    }

    /// <summary>
    /// SpecialForce addition: Returns the cached index for the given object.
    /// Throws an exception if the object is not found.
    /// </summary>
    /// <param name="o">Object to get index</param>
    /// <returns>Index of the given object</returns>
    public int GetIndex(object o)
    {
      if (mIndex.Contains(o))
        return (int)(mIndex[o]);

      throw new IndexOutOfRangeException("(ListView) Object not found in index: " + o.ToString());
    }
  }
}
