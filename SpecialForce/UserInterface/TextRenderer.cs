﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace SpecialForce
{
  public class TextRenderer
  {
    #region Shared Instances
    static private Graphics mDefaultCanvas = Graphics.FromImage(new Bitmap(1, 1));
    static private Font mDefaultFont = new Font("Microsoft Sans Serif", 7.0f);

    static public StringFormat RightToLeftFormat =
      new StringFormat(StringFormatFlags.DirectionRightToLeft);

    static public StringFormat NormalFormat =
      new StringFormat();

    static public StringFormat VerticalRightToLeftFormat =
      new StringFormat(StringFormatFlags.DirectionVertical | StringFormatFlags.DirectionRightToLeft);

    static public StringFormat VerticalFormat =
      new StringFormat(StringFormatFlags.DirectionVertical);
    #endregion Shared Instances


    #region Properties
    private List<string> mTexts = new List<string>();
    public string Text
    {
      get
      {
        string result = "";
        for(int i = 0; i < mTexts.Count; ++i)
        {
          result += mTexts[i];

          if (i < mTexts.Count - 1)
            result += Environment.NewLine;
        }
        return result;
      }

      set
      {
        mTexts.Clear();
        string [] texts = value.Split('\n');
        for (int i = 0; i < texts.Length; ++i)
          mTexts.Add(texts[i]);
      }
    }

    public List<string> Texts
    {
      get { return mTexts; }
      set { mTexts = value; }
    }

    protected Graphics mCanvas = null;
    public Graphics Canvas
    {
      get { return mCanvas; }
      set { mCanvas = value; }
    }

    private Point mLocation = new Point(0, 0);
    public Point Location
    {
      get { return mLocation; }
      set { mLocation = value; }
    }

    private Size mSize = new Size(0, 0);
    public Size Size
    {
      get { return mSize; }
      set { mSize = value; }
    }

    public int Left
    {
      get { return mLocation.X; }
      set { mLocation.X = value; }
    }

    public int Top
    {
      get { return mLocation.Y; }
      set { mLocation.Y = value; }
    }

    public int Right
    {
      get { return mLocation.X + mSize.Width; }
    }

    public int Bottom
    {
      get { return mLocation.Y + mSize.Height; }
    }

    public int Width
    {
      get { return mSize.Width; }
      set { mSize.Width = value; }
    }

    public int Height
    {
      get { return mSize.Height; }
      set { mSize.Height = value; }
    }

    protected Font mFont = mDefaultFont;
    public Font Font
    {
      get { return mFont; }
      set { mFont = value; }
    }

    private Color mColor = Color.Black;
    public Color Color
    {
      get { return mColor; }
      set { mColor = value; }
    }

    private Alignment mJustify = Alignment.MiddleMiddle;
    public Alignment Justify
    {
      get { return mJustify; }
      set { mJustify = value; }
    }

    private bool mRightToLeft = true;
    public bool RightToLeft
    {
      get { return mRightToLeft; }
      set { mRightToLeft = value; }
    }

    private bool mVertical = false;
    public bool Vertical
    {
      get { return mVertical; }
      set { mVertical = value; }
    }

    public int TextWidth
    {
      get
      {
        float result = 0.0f;
        foreach (string text in mTexts)
        {
          float width = GetTextSizeF(text).Width;
          if (width > result)
            result = width;
        }

        return TinyMath.Round(result);
      }
    }

    public int TextHeight
    {
      get
      {
        float result = 0.0f;
        foreach (string text in mTexts)
          result += GetTextSizeF(text).Height;

        return TinyMath.Round(result);
      }
    }

    public StringFormat Format
    {
      get
      {
        StringFormat result;
        if (mVertical)
        {
          result = mRightToLeft ? VerticalRightToLeftFormat : VerticalFormat;
          result.Alignment = StringAlignment.Near;
        }
        else
        {
          result = mRightToLeft ? RightToLeftFormat : NormalFormat;
          result.Alignment = mRightToLeft ? StringAlignment.Far : StringAlignment.Near;
        }

        return result;
      }
    }

    public Size TextSize
    {
      get { return new Size(TextWidth, TextHeight); }
    }
    #endregion Properties


    #region Utilities
    public SizeF GetTextSizeF(string str, int max_width = 0)
    {
      if (max_width == 0)
        max_width = 10000;

      return mDefaultCanvas.MeasureString(str, mFont, max_width, Format);
    }

    public void Clear()
    {
      mTexts.Clear();
    }

    public void AddText(string str)
    {
      mTexts.Add(str);
    }

    public override string ToString()
    {
      return Text;
    }

    static public string FitStringInWidth(string str, int width, Font font)
    {
      try
      {
        str = str.Split('\n')[0];
        float dots_width = mDefaultCanvas.MeasureString("...", font, 0, RightToLeftFormat).Width;

        if (mDefaultCanvas.MeasureString(str, font, 0, RightToLeftFormat).Width > width)
        {
          do
          {
            str = str.Remove(str.Length - 1);
          }
          while (mDefaultCanvas.MeasureString(str + "...", font, 0, RightToLeftFormat).Width > width);

          return str + "...";
        }
        else
          return str;
      }
      catch(Exception e)
      {
        Logger.LogException(e, "TextRenderer", "Error while constraining string");
        return "مشکلی پیش آمد";
      }
    }

    static public List<string> FitStringInLines(string str, int width, Font font)
    {
      List<string> result = new List<string>();
      try
      {
        str = str.Split('\n')[0];
        string [] words = str.Split(' ');
        float space_width = mDefaultCanvas.MeasureString(" ", font, 0, RightToLeftFormat).Width;

        int line_index = -1;
        List<List<int>> lines = new List<List<int>>();
        float line_width = 0;
        for (int i = 0; i < words.Length; ++i)
        {
          // Check if a new line shall be inserted
          bool add = false;
          float word_width = mDefaultCanvas.MeasureString(words[i], font, 0, RightToLeftFormat).Width;

          if (word_width > width)
          {
            add = true;
            words[i] = FitStringInWidth(words[i], width, font);
            line_width = width;
          }
          else if (line_width + word_width + space_width > width)
          {
            add = true;
            line_width = word_width + space_width;
          }
          else if (i == 0)
          {
            add = true;
            line_width = word_width + space_width;
          }
          else
            line_width += word_width + space_width;

          // Insert the new line if specified
          if (add)
          {
            ++line_index;
            lines.Add(new List<int>());
          }

          // Now add the word
          lines[line_index].Add(i);
        }

        // Create the result;
        for (int i = 0; i < lines.Count; ++i)
        {
          string strline = "";
          for (int j = 0; j < lines[i].Count; ++j)
            strline += words[lines[i][j]] + " ";
          result.Add(strline);
        }
      }
      catch (Exception e)
      {
        Logger.LogException(e, "TextRenderer", "Error while constraining string");
      }

      return result;
    }
    #endregion Utilities


    #region Drawing Functionality
    public void Draw()
    {
      if (mCanvas == null)
        return;

      Brush brush = new SolidBrush(mColor);
      List<float> widths = new List<float>();
      List<float> heights = new List<float>();
      float max_width = 0.0f;
      float total_height = 0.0f;

      for (int i = 0; i < mTexts.Count; ++i)
      {
        SizeF sz = GetTextSizeF(mTexts[i]);

        widths.Add(sz.Width);
        heights.Add(sz.Height);

        if (sz.Width > max_width)
          max_width = sz.Width;

        total_height += sz.Height;
      }

      float x, start_y, current_y = 0.0f;
      for (int i = 0; i < mTexts.Count; ++i)
      {
        if (mJustify == Alignment.BottomLeft || mJustify == Alignment.MiddleLeft || mJustify == Alignment.TopLeft)
        {
          x = mLocation.X;
        }
        else if (mJustify == Alignment.BottomMiddle || mJustify == Alignment.MiddleMiddle || mJustify == Alignment.TopMiddle)
        {
          x = mSize.Width == 0 ? mLocation.X : mLocation.X + (mSize.Width - widths[i]) / 2;
        }
        else
        {
          x = mSize.Width == 0 ? mLocation.X + widths[i] : mLocation.X + mSize.Width - widths[i];
        }

        if (mJustify == Alignment.BottomLeft || mJustify == Alignment.BottomMiddle || mJustify == Alignment.BottomRight)
        {
          start_y = mSize.Height == 0 ? mLocation.Y - heights[i] : mLocation.Y + mSize.Height - total_height;
        }
        else if (mJustify == Alignment.MiddleLeft || mJustify == Alignment.MiddleMiddle || mJustify == Alignment.MiddleRight)
        {
          start_y = mSize.Height == 0 ? mLocation.Y : mLocation.Y + (mSize.Height - total_height) / 2;
        }
        else
        {
          start_y = mLocation.Y;
        }

        if (current_y == 0.0f)
          current_y = start_y;

        mCanvas.DrawString(mTexts[i], mFont, brush, new PointF(x, current_y), Format);
        current_y += heights[i];
      }
    }
    #endregion Drawing Functionality
  }
}
