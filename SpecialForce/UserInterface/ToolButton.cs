﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SpecialForce
{
  [DefaultEvent("Click")]
  public partial class ToolButton : ControlBase
  {
    protected enum ButtonState
    {
      Normal,
      Pressed,
      Disabled,
      MouseHovering
    }

    #region Resources
    private ToolTip mToolTip = new ToolTip();
    private Bitmap mNormalImage = null;
    private Bitmap mPressedImage = null;
    private Bitmap mDisabledImage = null;
    private Bitmap mHoverImage = null;
    private Bitmap mEmptyImage = null;
    #endregion Resources


    #region Properties
    private ButtonState mState = ButtonState.Normal;
    protected ButtonState State
    {
      get { return mState; }
      set { mState = value; RequestRepaint(); }
    }

    private Bitmap mImage = null;
    public Bitmap Image
    {
      get { return mImage; }
      set { AssignImage(value); }
    }

    private string mHint = "";
    public string Hint
    {
      get { return mHint; }
      set
      {
        mHint = value;
        mToolTip.SetToolTip(this, mHint);
      }
    }

    private bool mShowBorder = true;
    [DefaultValue(true)]
    public bool ShowBorder
    {
      get { return mShowBorder; }
      set 
      { 
        mShowBorder = value;
        AssignImage(mImage);
      }
    }

    private Color mNormalBorderColor = Color.FromArgb(255, 192, 192, 192);
    [DefaultValue(typeof(Color), "0xFFC0C0C0")]
    public Color NormalBorderColor
    {
      get { return mNormalBorderColor; }
      set
      {
        mNormalBorderColor = value;
        AssignImage(mImage);
      }
    }

    private Color mPressedBorderColor = Color.FromArgb(255, 33, 70, 107);
    [DefaultValue(typeof(Color), "0xFF21466B")]
    public Color PressedBorderColor
    {
      get { return mPressedBorderColor; }
      set 
      {
        mPressedBorderColor = value;
        AssignImage(mImage);
      }
    }

    private Color mTransparentColor = Color.Transparent;
    [DefaultValue(typeof(Color), "0x00000000")]
    public Color TransparentColor
    {
      get { return mTransparentColor; }
      set 
      {
        mTransparentColor = value;
        AssignImage(mImage);
      }
    }
    #endregion Properties


    #region Events
    public new event EventHandler<MouseEventArgs> OnClick;
    public event EventHandler<MouseEventArgs> OnRightClick;
    public event EventHandler<DragObjectRequestArgs> OnDragStart;
    #endregion Events


    #region Constructor
    public ToolButton()
    {
      Width = 64;
      Height = 64;
      CreateEmptyImage();
    }
    #endregion Constructor


    #region Image Assignment
    private Graphics CreateCanvas(Bitmap bmp)
    {
      Graphics canvas = Graphics.FromImage(bmp);
      canvas.CompositingMode = CompositingMode.SourceOver;
      canvas.Clear(Color.Transparent);
      canvas.DrawImage(mImage, 0, 0);
      return canvas;
    }

    private void DrawBorder(Graphics g, bool narrow, Color color)
    {
      if (mShowBorder)
      {
        int l = narrow ? 0 : 2;
        int t = narrow ? 0 : 2;
        int w = narrow ? Width - 1 : Width - 5;
        int h = narrow ? Height - 1 : Height - 5;
        Pen p = new Pen(new SolidBrush(color), 2);

        g.DrawRoundedRectangle(p, new Rectangle(l, t, w, h), 4);
      }
    }

    private void CreateEmptyImage()
    {
      if (mEmptyImage != null)
        mEmptyImage.Dispose();

      mEmptyImage = new Bitmap(Math.Max(1, Width), Math.Max(1, Height));
      Graphics canvas = Graphics.FromImage(mEmptyImage);
      DrawBorder(canvas, true, mNormalBorderColor);
      canvas.Dispose();
    }

    private void AssignImage(Bitmap img)
    {
      // Nothing to do
      if (img == null)
        return;

      // fix sizes
      if (Width != img.Width || Height != img.Height)
        Size = img.Size;

      //0 : image instance
      mImage = new Bitmap(img);
      mImage.MakeTransparent(mTransparentColor);


      //1 : normal image. Just copy the image and draw a border for it
      if (mNormalImage != null)
        mNormalImage.Dispose();
      mNormalImage = new Bitmap(Width, Height);
      mNormalImage.MakeTransparent(mTransparentColor);
      Graphics canvas = CreateCanvas(mNormalImage);
      DrawBorder(canvas, true, mNormalBorderColor);
      canvas.Dispose();


      // 2: Pressed image
      if (mPressedImage != null)
        mPressedImage.Dispose();
      mPressedImage = new Bitmap(Width, Height);
      mPressedImage.MakeTransparent(mTransparentColor);
      Bitmap temp = new Bitmap(Width, Height);
      temp.MakeTransparent(mTransparentColor);
      canvas = Graphics.FromImage(temp);
      canvas.DrawImage(mImage, 2, 2, Width - 4, Height - 4);
      DrawBorder(canvas, false, mPressedBorderColor);
      DrawBorder(canvas, true, mNormalBorderColor);
      Graphics temp2 = Graphics.FromImage(mPressedImage);
      temp2.DrawHighlighted(temp);
      temp2.Dispose();
      temp.Dispose();
      canvas.Dispose();


      // 3: disabled image. grayscale the image
      if (mDisabledImage != null)
        mDisabledImage.Dispose();
      mDisabledImage = new Bitmap(Width, Height);
      mDisabledImage.MakeTransparent(mTransparentColor);
      temp = new Bitmap(Width, Height);
      canvas = CreateCanvas(temp);
      DrawBorder(canvas, true, mNormalBorderColor);
      canvas.Dispose();
      canvas = Graphics.FromImage(mDisabledImage);
      canvas.DrawGrayScale(temp);
      canvas.Dispose();
      temp.Dispose();


      // 4: Hovering image. Put a light on it
      if (mHoverImage != null)
        mHoverImage.Dispose();
      mHoverImage = new Bitmap(Width, Height);
      mHoverImage.MakeTransparent(mTransparentColor);
      temp = new Bitmap(Width, Height);
      canvas = CreateCanvas(temp);
      DrawBorder(canvas, true, mNormalBorderColor);
      canvas.Dispose();
      canvas = Graphics.FromImage(mHoverImage);
      canvas.DrawHighlighted(temp);
      canvas.Dispose();
      temp.Dispose();

      RequestRepaint();
    }
    #endregion Image Assignment


    #region Overrides
    protected override Size QueryPrefferedSize()
    {
      if (mImage != null)
        return mImage.Size;

      CreateEmptyImage();
      return mEmptyImage.Size;
    }

    protected override BehaviorParams CreateBehavior
    {
      get
      {
        BehaviorParams prms = base.CreateBehavior;
        prms.UtilizesKeyBoardInput();
        prms.UtilizesMouseInput();
        prms.UsesAntiAlias();
        return prms;
      }
    }

    protected override void OnRecreateView(RecreateArgs e)
    {
      Bitmap bmp = mNormalImage;
      if (mState == ButtonState.Pressed)
        bmp = mPressedImage;
      else if (mState == ButtonState.Disabled)
        bmp = mDisabledImage;
      else if (mState == ButtonState.MouseHovering)
        bmp = mHoverImage;

      if (bmp == null)
        bmp = mEmptyImage;

      e.Canvas.DrawImageUnscaled(0, 0, bmp);
    }

    protected override void OnMouseEnter(EventArgs e)
    {
      mToolTip.Active = true;
      State = ButtonState.MouseHovering;
      base.OnMouseEnter(e);
    }

    protected override void OnMouseLeave(EventArgs e)
    {
      mToolTip.Active = false;
      if (mState == ButtonState.MouseHovering)
        State = ButtonState.Normal;
      base.OnMouseLeave(e);
    }

    protected override void OnMouseDown(MouseEventArgs e)
    {
      if (e.Button == System.Windows.Forms.MouseButtons.Left)
        State = ButtonState.Pressed;
      base.OnMouseDown(e);
    }

    protected override void OnMouseUp(MouseEventArgs e)
    {
      if (mState == ButtonState.Pressed)
        State = ButtonState.Normal;
      base.OnMouseUp(e);

      if (!ClientRectangle.Contains(e.Location))
        return;

      if (e.Button == MouseButtons.Left)
      {
        EventHandler<MouseEventArgs> ev = OnClick;
        if (ev != null)
          ev(this, e);
      }
      else if (e.Button == MouseButtons.Right)
      {
        EventHandler<MouseEventArgs> ev = OnRightClick;
        if (ev != null)
          ev(this, e);
      }
    }

    protected override void OnMouseMove(MouseEventArgs e)
    {
      if (mState == ButtonState.Pressed)
      {
        DragObjectRequestArgs arg = new DragObjectRequestArgs();
        EventHandler<DragObjectRequestArgs> ev = OnDragStart;
        if (ev != null)
          ev(this, arg);

        if (arg.Object != null)
        {
          DoDragDrop(arg.Object, DragDropEffects.Move);
          State = ButtonState.Normal;
        }
      }

      base.OnMouseMove(e);
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      State = Enabled ? ButtonState.Normal : ButtonState.Disabled;
      base.OnEnabledChanged(e);
    }
    #endregion Overrides
  }
}
