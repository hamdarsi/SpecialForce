﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SpecialForce.UserInterface
{
  [ToolboxItem(false)]
  public partial class RadioButton : Control
  {
    private enum ButtonState
    {
      Normal,
      Pressed,
      Disabled,
      MouseHovering
    }

    private enum DrawingMethod
    {
      Lighten,
      Shrink,
      GrayScale,
      Normal
    }

    private Bitmap mNormalImage = null;
    private Bitmap mPressedImage = null;
    private Bitmap mDisabledImage = null;
    private Bitmap mHoverImage = null;
    private Bitmap mEmptyImage = null;

    private ButtonState mState = ButtonState.Normal;
    private ToolTip mToolTip = new ToolTip();

    private bool mPressed = false;
    public bool Pressed
    {
      get { return mPressed; }
      set 
      {
        if (mPressed != value)
        {
          mPressed = value;
          InvalidateControl();
        }
      }
    }

    private Bitmap mImage = null;
    public Bitmap Image
    {
      get { return mImage; }
      set { AssignImage(value); }
    }

    private string mHint = "";
    public string Hint
    {
      get { return mHint; }
      set
      {
        mHint = value;
        mToolTip.SetToolTip(this, mHint);
      }
    }

    private Color mTransparentColor;
    public Color TransparentColor
    {
      get { return mTransparentColor; }
      set 
      {
        mTransparentColor = value;
        AssignImage(mImage);
      }
    }

    public new event EventHandler<EventArgs> OnClick;

    public RadioButton()
    {
      mTransparentColor = Color.Transparent;

      Width = 64;
      Height = 64;
      CreateEmptyImage();

      InitializeComponent();
    }

    protected override CreateParams CreateParams
    {
      get
      {
        CreateParams prm = base.CreateParams;
        prm.ExStyle |= 0x20;
        return prm;
      }
    }

    private void AssignImage(Bitmap img)
    {
      if (img == null)
      {
        mImage = null;
        mNormalImage = null;
        mPressedImage = null;
        mDisabledImage = null;
        mHoverImage = null;
      }
      else
      {
        // Fix sizes
        if (Width != img.Width || Height != img.Height)
          Size = img.Size;

        // Prepare colors
        Color outer_border = Color.FromArgb(50, 80, 116);
        Color inner_border = Color.FromArgb(160, 182, 211);

        //0 : Main image instance
        mImage = new Bitmap(img);
        mImage.MakeTransparent(mTransparentColor);


        //1 : Normal image. Just copy the image and draw a border for it
        Graphics canvas = CreateCanvas(out mNormalImage, DrawingMethod.Normal);
        DrawBorder(canvas, true, outer_border);

        // 2: Pressed image, Draw smaller and darker
        canvas = CreateCanvas(out mPressedImage, DrawingMethod.Shrink);
        DrawBorder(canvas, false, inner_border);
        DrawBorder(canvas, true, outer_border);

        // 3: Disabled image. Grayscale the image
        canvas = CreateCanvas(out mDisabledImage, DrawingMethod.GrayScale);
        DrawBorder(canvas, true, outer_border);

        // 4: Hovering image. Put a light on it
        canvas = CreateCanvas(out mHoverImage, DrawingMethod.Lighten);
        DrawBorder(canvas, true, outer_border);
      }

      InvalidateControl();
    }

    private void CreateEmptyImage()
    {
      mEmptyImage = new Bitmap(Math.Max(1, Width), Math.Max(1, Height));
      DrawBorder(Graphics.FromImage(mEmptyImage), true, Color.DarkBlue);
    }

    protected override void OnSizeChanged(EventArgs e)
    {
      if (mImage == null)
        CreateEmptyImage();
      else if (Width != mImage.Width || Height != mImage.Height)
        Size = mImage.Size;

      base.OnSizeChanged(e);
    }

    private Graphics CreateCanvas(out Bitmap bmp, DrawingMethod method)
    {
      // First create the bitmap
      bmp = new Bitmap(Width, Height);
      bmp.MakeTransparent(mTransparentColor);

      // Then create its Graphics object and set its properties
      Graphics canvas = Graphics.FromImage(bmp);
      canvas.CompositingMode = CompositingMode.SourceOver;
      canvas.Clear(Color.Transparent);

      // Then copy the main image based on the given lighten factor
      if(method == DrawingMethod.GrayScale)
        canvas.DrawGrayScale(mImage);
      else if (method == DrawingMethod.Lighten)
        canvas.DrawHighlighted(mImage, 0.1f);
      else if(method == DrawingMethod.Shrink)
        canvas.DrawImage(mImage, 4, 4, Width - 8, Height - 8);
      else
        canvas.DrawImage(mImage, 0, 0, Width, Height);

      return canvas;
    }

    private void DrawBorder(Graphics g, bool narrow, Color color)
    {
      int l = narrow ? 1 : 4;
      int t = narrow ? 1 : 4;
      int w = narrow ? Width - 3 : Width - 9;
      int h = narrow ? Height - 3 : Height - 9;
      Pen p = new Pen(new SolidBrush(color), narrow ? 2 : 4);

      g.DrawRoundedRectangle(p, new Rectangle(l, t, w, h), 1);
    }

    protected void InvalidateControl()
    {
      Rectangle r = RectangleToScreen(ClientRectangle);
      Control o = this;
      while (o != null)
      {
        o = o.Parent;
        if (o as SessionAwareForm != null)
        {
          o.Invalidate(o.RectangleToClient(r), true);
          o.Update();
          return;
        }
      }

      Refresh();
    }

    protected override void OnPaintBackground(PaintEventArgs e)
    {
      // Please, do not paint your background!
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);

      Bitmap bmp = mPressed ? mPressedImage : mNormalImage;
      if (mState == ButtonState.Pressed)
        bmp = mPressedImage;
      else if (mState == ButtonState.Disabled)
        bmp = mDisabledImage;
      else if (mState == ButtonState.MouseHovering && !mPressed)
        bmp = mHoverImage;

      if (bmp == null)
        bmp = mEmptyImage;

      e.Graphics.DrawImage(bmp, 0, 0);
    }

    protected override void OnMouseEnter(EventArgs e)
    {
      base.OnMouseEnter(e);
      mState = ButtonState.MouseHovering;
      InvalidateControl();
    }

    protected override void OnMouseLeave(EventArgs e)
    {
      base.OnMouseLeave(e);
      if (mState == ButtonState.MouseHovering)
        mState = ButtonState.Normal;
      InvalidateControl();
    }

    protected override void OnMouseDown(MouseEventArgs e)
    {
      base.OnMouseDown(e);
      if (e.Button == System.Windows.Forms.MouseButtons.Left)
      {
        mState = ButtonState.Pressed;
        InvalidateControl();
      }
    }

    protected override void OnMouseUp(MouseEventArgs e)
    {
      base.OnMouseUp(e);
      if (mState == ButtonState.Pressed)
      {
        mState = ButtonState.Normal;
        InvalidateControl();
      }

      if (e.Button != System.Windows.Forms.MouseButtons.Left)
        return;

      if (!ClientRectangle.Contains(e.Location))
        return;

      // Send click event
      EventHandler<EventArgs> ev = OnClick;
      if (ev != null)
        ev(this, new EventArgs());
    }

    protected override void OnEnabledChanged(EventArgs e)
    {
      base.OnEnabledChanged(e);

      if (Enabled)
        mState = ButtonState.Normal;
      else
        mState = ButtonState.Disabled;

      InvalidateControl();
    }
  }
}
