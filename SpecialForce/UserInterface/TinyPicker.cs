﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce
{
  public partial class TinyPicker : ControlBase
  {
    public TinyPicker()
    {
      InitializeComponent();
    }

    #region Overrides
    protected override void OnHandleCreated(EventArgs e)
    {
      base.OnHandleCreated(e);

      Session.OnSessionChanged += UpdatePicker;
      DB.GameManager.OnScheduledGameUpdated += UpdatePicker;
      UpdatePicker();
    }

    protected override void OnHandleDestroyed(EventArgs e)
    {
      base.OnHandleDestroyed(e);

      Session.OnSessionChanged -= UpdatePicker;
      DB.GameManager.OnScheduledGameUpdated -= UpdatePicker;
    }

    private void OnMouseClicked(Object sender, EventArgs e)
    {
      if (DB.GameManager.CurrentGame != null)
        ControlTower.ControlPanel.MonitorGame();
    }

    protected override void OnSizeChanged(EventArgs e)
    {
      base.OnSizeChanged(e);

      Reposition();
    }

    protected override void OnLocationChanged(EventArgs e)
    {
      base.OnLocationChanged(e);
      if (Parent == null || Session.DevelopMode)
        return;

      int l = 14;
      int t = Parent.ClientRectangle.Height - Height - 14;
      if (Left != 14 || Top != t)
        Location = new Point(l, t);
    }

    protected override void OnVisibleChanged(EventArgs e)
    {
      base.OnVisibleChanged(e);
      lblGamePick.Visible = Visible;
      lblStaffPick.Visible = Visible;
    }
    #endregion Overrides


    #region Graphical Overrides and Presentation
    private void UpdatePicker(Object sender = null, EventArgs e = null)
    {
      // Show current logged in member
      if (Session.Open)
      {
        string str = Session.CurrentStaff.FullName;
        str += " | ورود: ";
        str += Session.LoginTime.ToString(DateTimeString.TimeNoSecond);
        str += " | سمت: ";
        str += Enums.ToPersianString(Session.CurrentStaff.Post);
        lblStaffPick.Text = str;
      }
      else
        lblStaffPick.Text = "برنامه در حال اجرا نیست!";

      // Show current game state
      if (DB.GameManager.CurrentGame != null)
      {
        lblGamePick.Text = "بازی در جریان است";
        lblGamePick.Cursor = Cursors.Hand;
      }
      else
      {
        lblGamePick.Text = "هیچ بازی در حال اجرا نیست";
        lblGamePick.Cursor = Cursors.Arrow;
      }

      // Reposition elements
      Reposition();
    }

    protected override Size QueryPrefferedSize()
    {
      Size result = new Size(0, 0);
      if (Parent != null)
      {
        result.Width = Parent.ClientRectangle.Width - 2 * 14;
        result.Height = 30;
      }

      return result;
    }

    private void Reposition()
    {
      tbMain.Width = Width;
      tbMain.Height = Height;
      tbMain.Left = 0;
      tbMain.Top = 0;

      lblStaffPick.Left = tbMain.Width - lblStaffPick.Width - 10;
      lblGamePick.Left = 10;
      lblStaffPick.Top = tbMain.Top + (tbMain.Height - lblStaffPick.Height) / 2;
      lblGamePick.Top = tbMain.Top + (tbMain.Height - lblGamePick.Height) / 2;

      RequestRepaint();
    }

    protected override BehaviorParams CreateBehavior
    {
      get
      {
        BehaviorParams prms = base.CreateBehavior;
        prms.UsesAntiAlias();
        return prms;
      }
    }

    protected override void OnRecreateView(RecreateArgs e)
    {
    }
    #endregion Graphical Overrides and Presentation
  }
}
