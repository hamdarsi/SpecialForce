﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Utilities
{
  [DefaultEvent("OnNumberEntered")]
  public partial class NumberBox : TextBox
  {
    public enum NumberMode
    {
      Integer,
      Float,
      Currency
    }

    #region Properties
    private NumberMode mNumberMode = NumberMode.Integer;
    [DefaultValue(NumberMode.Integer)]
    public NumberMode Mode
    {
      get { return mNumberMode; }
      set 
      {
        mNumberMode = value;
        UpdateText();
      }
    }

    private int mAccuracy = 0;
    [DefaultValue(0)]
    public int Accuracy
    {
      get { return mAccuracy; }
      set 
      {
        mAccuracy = value;
        UpdateText();
      }
    }

    private bool mSelectAfterEnter = true;
    [DefaultValue(true)]
    public bool SelectAfterEnter
    {
      get { return mSelectAfterEnter; }
      set { mSelectAfterEnter = value; }
    }

    private double mDefaultValue = 0.0;
    [DefaultValue(0.0)]
    public double DefaultValue
    {
      get { return mDefaultValue; }
      set { mDefaultValue = value; }
    }

    private double mValue = 0.0;
    [DefaultValue(0.0)]
    public double Value
    {
      get { return mValue; }
      set { mValue = value; UpdateText(); }
    }

    public int IntegerValue
    {
      get { return TinyMath.Round(mValue); }
    }
    #endregion Properties


    #region Events of NumberBox
    public event EventHandler<EventArgs> OnNumberEntered;
    public event EventHandler<EventArgs> OnNumberChanged;
    public event EventHandler<EventArgs> OnFocusReceived;
    public event EventHandler<EventArgs> OnFocusLost;
    #endregion Properties


    #region Constructor
    public NumberBox()
    {
      Value = mDefaultValue;
      InitializeComponent();
    }
    #endregion Constructor


    #region Event Handlers
    protected override void OnGotFocus(EventArgs e)
    {
      base.OnGotFocus(e);

      EventHandler<EventArgs> ev = OnFocusReceived;
      if (ev != null)
        ev(this, e);
    }

    protected override void OnLostFocus(EventArgs e)
    {
      base.OnLostFocus(e);

      EventHandler<EventArgs> ev = OnFocusLost;
      if (ev != null)
        ev(this, e);
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      e.SuppressKeyPress = !IsAcceptable(e.KeyCode);
      if (e.KeyCode == Keys.Enter)
      {
        if (mSelectAfterEnter)
          SelectAll();

        EventHandler<EventArgs> ev = OnNumberEntered;
        if (ev != null)
          ev(this, new EventArgs());
      }
      else if (e.KeyCode == Keys.Escape)
      {
        Value = mDefaultValue;
        SelectAll();
      }
      else if ((e.KeyCode == Keys.Decimal || e.KeyCode == Keys.OemPeriod) && ((Text.IndexOf(".") != -1) && (mNumberMode == NumberMode.Integer)))
        e.SuppressKeyPress = true;
      else if ((e.KeyCode == Keys.Subtract || e.KeyCode == Keys.OemMinus) && (Text.IndexOf("-") != -1 || SelectionStart != 0))
        e.SuppressKeyPress = true;
      else if (e.KeyCode == Keys.Delete && SelectionStart < Text.Length && mNumberMode == NumberMode.Currency && (Text[SelectionStart] == ',' || Text[SelectionStart] == '.'))
        e.SuppressKeyPress = true;
      base.OnKeyDown(e);
    }

    protected override void OnRightToLeftChanged(EventArgs e)
    {
      if(RightToLeft != RightToLeft.No)
        RightToLeft = RightToLeft.No;
    }

    protected override void OnTextChanged(EventArgs e)
    {
      base.OnTextChanged(e);

      if (Text.Length == 0)
        return;

      // First, trim the text
      string text = Text;
      for (int i = 0; i < text.Length; ++i)
        if (text[i] == ',')
          text = text.Remove(i--, 1);

      try
      {
        double value = Convert.ToDouble(text);
        if (mValue != value)
        {
          if (mNumberMode == NumberMode.Currency)
          {
            int len = Text.Length;
            int caret = SelectionStart;
            Value = value;
            if (Math.Abs(len - Text.Length) == 1)
            {
              if (Text.Length - caret < 2)
                SelectionStart = caret;
              else if (Text.Length < len)
                SelectionStart = caret - 1;
              else
                SelectionStart = caret + 1;
            }
            else
              SelectionStart = caret;
          }
          else
            Value = value;

          EventHandler<EventArgs> ev = OnNumberChanged;
          if (ev != null)
            ev(this, new EventArgs());
        }
      }
      catch
      {
        mValue = 0;
        return;
      }
    }
    #endregion Event Handlers


    #region Utilities
    private bool IsAcceptable(Keys key)
    {
      if (key >= Keys.NumPad0 && key <= Keys.NumPad9)
        return true;
      else if (key >= Keys.D0 && key <= Keys.D9)
        return true;
      else if (key == Keys.Left || key == Keys.Right || key == Keys.Tab)
        return true;
      else if (key == Keys.Decimal || key == Keys.OemPeriod)
        return true;
      else if (key == Keys.Subtract || key == Keys.OemMinus)
        return true;
      else if (key == Keys.Tab)
        return true;
      else if (key == Keys.Delete || key == Keys.Back)
        return true;
      else if (key == Keys.Home || key == Keys.End)
        return true;
      else
        return false;
    }

    private void UpdateText()
    {
      if (mNumberMode == NumberMode.Integer)
      {
        if (mAccuracy == 0)
          Text = IntegerValue.ToString();
        else
          Text = IntegerValue.ToString("D" + mAccuracy.ToString(), Session.NumberFormat);
      }
      else if (mNumberMode == NumberMode.Float)
      {
        if (mAccuracy == 0)
          Text = Value.ToString();
        else
          Text = Value.ToString("F" + mAccuracy.ToString(), Session.NumberFormat);
      }
      else
        Text = Value.ToString("C2", Session.NumberFormat);
    }
    #endregion Utilities
  }
}
