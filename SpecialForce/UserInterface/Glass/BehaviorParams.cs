﻿using System.Collections.Generic;

namespace SpecialForce
{
  public class BehaviorParams
  {
    /// <summary>
    /// Whether or not this control uses antialiasing.
    /// This property determines whether the control shall prepare
    /// its background before creating its view. Since anti aliasing
    /// only works when background is provided.
    /// </summary>
    private bool mRequiresAntiAlias = false;
    public bool RequiresAntiAlias
    {
      get { return mRequiresAntiAlias; }
    }

    private SortedSet<EventTypes> mInvalidationEvents = new SortedSet<EventTypes>();
    /// <summary>
    /// The control will be invalidated upon receiving these events.
    /// </summary>
    public SortedSet<EventTypes> InvalidationEvents
    {
      get
      {
        SortedSet<EventTypes> result = new SortedSet<EventTypes>();
        result.UnionWith(mInvalidationEvents);
        return result;
      }

      set
      {
        if (value != null)
          mInvalidationEvents = value;
      }
    }

    public void RendersText()
    {
      UsesAntiAlias();

      mInvalidationEvents.Add(EventTypes.TextChanged);
      mInvalidationEvents.Add(EventTypes.FontChanged);
      mInvalidationEvents.Add(EventTypes.ForeColorChanged);
    }

    public void UsesAntiAlias()
    {
      mRequiresAntiAlias = true;

      mInvalidationEvents.Add(EventTypes.BackColorChanged);
      mInvalidationEvents.Add(EventTypes.SizeChanged);
      mInvalidationEvents.Add(EventTypes.LocationChanged);
      mInvalidationEvents.Add(EventTypes.VisibleChanged);
    }

    public void UtilizesMouseInput()
    {
      mInvalidationEvents.Add(EventTypes.MouseEnter);
      mInvalidationEvents.Add(EventTypes.MouseLeave);
      mInvalidationEvents.Add(EventTypes.MouseDown);
      mInvalidationEvents.Add(EventTypes.MouseUp);
    }

    public void UtilizesKeyBoardInput()
    {
      mInvalidationEvents.Add(EventTypes.KeyDown);
      mInvalidationEvents.Add(EventTypes.KeyUp);
    }
  }
}
