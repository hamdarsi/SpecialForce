﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SpecialForce
{
  static public class GraphicsSupport
  {
    /// <summary>
    /// This method finds out the parent form of this control.
    /// If the control itself is the parent form, it will be
    /// returned too.
    /// </summary>
    static public SessionAwareForm GetParentForm(this Control ctrl)
    {
      Control aux = ctrl;
      while (aux != null)
        if (aux is SessionAwareForm)
          return aux as SessionAwareForm;
        else if (aux is Form)
          return null;
        else
          aux = aux.Parent;

      return null;
    }

    /// <summary>
    /// Finds out the given control's bounding rectangle in its parent
    /// form coordinates.
    /// </summary>
    static public Rectangle GetFormCoordinates(this Control ctrl)
    {
      // Do not bug with a Form
      if (ctrl is Form)
        return new Rectangle(0, 0, ctrl.ClientSize.Width, ctrl.ClientSize.Height);

      // Find parent form. Generate a list of passed controls
      Control aux = ctrl;
      Stack<Control> stack = new Stack<Control>();
      while (aux != null && !(aux is Form))
      {
        stack.Push(aux);
        aux = aux.Parent;
      }

      // Form instance
      SessionAwareForm form = aux as SessionAwareForm;
      // No parent form? No point in continuing.
      if (aux == null)
        return Rectangle.Empty;

      // Find out the location.
      int x = 0, y = 0;
      do
      {
        aux = stack.Pop();
        y += aux.Top;

        if (aux.Parent == null)
          x += aux.Left;
        else if(aux.Parent is Form)
          x += aux.Left;
        else if(!form.RightToLeftDrawing)
          x += aux.Left;
        else
          x += (aux.Parent.Width - aux.Right);
      } while (stack.Count != 0);

      // Generate resulting rectangle
      return new Rectangle(x, y, ctrl.Width, ctrl.Height);
    }

    /// <summary>
    /// This method returns the coordinates used for drawing
    /// the control on its form.
    /// </summary>
    static public Rectangle GetDrawingCoordinates(this Control ctrl)
    {
      SessionAwareForm form = GetParentForm(ctrl);
      return
        form == null ?
        Rectangle.Empty :
        form.GetDrawTarget(GetFormCoordinates(ctrl));
    }

    /// <summary>
    /// Normalizes a given clip rectangle for a given control. Strangely seen,
    /// some clip areas are invalid for a control when being drawn in right to
    /// left layout. This will 'normalize' them.
    /// This behavior is seen in .Net4 framework.
    /// </summary>
    static public Rectangle NormalizeClipRect(this Control ctrl, Rectangle clip)
    {
      int y = clip.Top, h = clip.Height;
      int x = 0, w = clip.Width;
      if (clip.Left > 0)
        w = ctrl.ClientSize.Width - clip.Left;
      else if (w < ctrl.ClientSize.Width)
        x = ctrl.ClientSize.Width - w;

      return new Rectangle(x, y, w, h);
    }

    /// <summary>
    /// This method globalizes the given relative clip rect by considering
    /// the owner control's coordinates and clipping areas of its parents.
    /// </summary>
    static public Rectangle GlobalizeClipRect(this Control ctrl, Rectangle clip)
    {
      // Clip -> Normalize -> To Form Coordinates
      Rectangle ctrl_area = GetFormCoordinates(ctrl);
      clip = NormalizeClipRect(ctrl, clip);
      clip.X += ctrl_area.X;
      clip.Y += ctrl_area.Y;

      // Now intersect with parent controls.
      // No need to go further for forms.
      if (ctrl is Form)
        return clip;

      // Narrow the clipping region, based on parent clip regions.
      Control aux = ctrl.Parent;
      while (aux != null && !(aux is Form))
      {
        clip.Intersect(aux.GetFormCoordinates());
        aux = aux.Parent;
      }

      // Return it.
      return clip;
    }
  }
}
