﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SpecialForce
{
  public class VisualRenderer
  {
    #region Internal Data
    private Graphics mCanvas;
    private SessionAwareForm mForm;
    private Control mControl;
    private Rectangle mAbsoluteClipRect;
    private Control mDeadEnd;
    #endregion Internal Data


    #region Contructor
    /// <summary>
    /// Constructor of VisualRenderer. Initializes parameters for
    /// rendering. Sets relative clipping area and calculates global
    /// clipping area based on the control's clip area and its parents.
    /// </summary>
    public VisualRenderer(Control ctrl, Rectangle clip)
    {
      mControl = ctrl;
      mForm = ctrl.GetParentForm();

      // Now, determine absolute clip rectangle for the given control and clip
      mAbsoluteClipRect = mControl.GlobalizeClipRect(clip);
    }
    #endregion Contructor


    #region Render Interface
    /// <summary>
    /// This method renders the control to the given canvas.
    /// All of control's backgrounds are drawn as well, so
    /// there is no need to invalidate them.
    /// </summary>
    public void Render(Graphics canvas)
    {
      mCanvas = canvas;
      if (mControl.Controls.Count != 0)
        mDeadEnd = mControl.Controls[0];
      RasterControl(mForm);
    }

    /// <summary>
    /// This method renders the control background to the given texture.
    /// Mainly used in pre-render stage for controls which use anti aliasing.
    /// </summary>
    /// <param name="canvas"></param>
    public void RenderToTexture(Graphics canvas)
    {
      mDeadEnd = mControl;
      mCanvas = canvas;
      RasterControl(mForm);
    }
    #endregion Render Interface


    #region Render Implementation
    /// <summary>
    /// Raseterizes the control to render and renders it. Also 
    /// recursively checks for other windows to be rendered.
    /// </summary>
    private void RasterControl(Control ctrl)
    {
      // Get out of here.
      if (ctrl == null || !ctrl.Visible)
        return;

      // Draw the control itself.
      RenderControl(ctrl);

      // Break rendering when reached to the ending control
      if (ctrl.Controls.Contains(mDeadEnd))
        return;

      // Draw children in the clip area
      foreach (Control child in ctrl.Controls)
      {
        // Only select children passing the hit test
        // Recurse into the children.
        if (mAbsoluteClipRect.IntersectsWith(child.GetFormCoordinates()))
          RasterControl(child);
      }
    }

    /// <summary>
    /// This method actually renders the given control. By assuming
    /// that the given canvas is origined on the primary control,
    /// translates the origin coordinates to be located on the
    /// current given control. Clipping and drawing are done in the
    /// raster control's coordinates.
    /// </summary>
    /// <param name="ctrl">Raster control</param>
    private void RenderControl(Control ctrl)
    {
      // Get the output image
      Image img = null;
      if (ctrl is SessionAwareForm)
        img = (ctrl as SessionAwareForm).RenderedView;
      else if (ctrl is ToolBar)
        img = (ctrl as ToolBar).RenderedView;
      else if (ctrl is ControlBase)
        img = (ctrl as ControlBase).RenderedView;

      // No image provided?
      if (img == null)
        return;

      // I can draw in two ways:
      // 1: Fix clip rect on [0, 0] and draw with relative coordinates
      // 2: Fix drawing origin on [0, 0] and set clip rect accordingly
      // Which way I choose? 
      // Don't give a fuck. 2 to save my wide asshole from becoming even a little tight.

      Rectangle draw_rect = ctrl.GetDrawingCoordinates();
      Rectangle clip_rect = mForm.GetDrawTarget(mAbsoluteClipRect);
      Rectangle ctrl_rect = mControl.GetDrawingCoordinates();

      // Transform the primary control's graphics origin to the current one's [0, 0]
      mCanvas.TranslateTransform(
        draw_rect.Left - ctrl_rect.Left,
        draw_rect.Top - ctrl_rect.Top,
        MatrixOrder.Append);

      // Clip it.
      clip_rect = new Rectangle(
        clip_rect.Left - draw_rect.Left,
        clip_rect.Top - draw_rect.Top,
        clip_rect.Width,
        clip_rect.Height);
      mCanvas.SetClip(clip_rect);

      // Draw it relative to the current control
      mCanvas.DrawImage(img, clip_rect, clip_rect, GraphicsUnit.Pixel);

      // Reset it.
      mCanvas.ResetTransform();
      mCanvas.ResetClip();
    }
    #endregion Render Implementation
  }
}
