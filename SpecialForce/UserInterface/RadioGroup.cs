﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;

namespace SpecialForce.UserInterface
{
  [DefaultEvent("Click")]
  public partial class RadioGroup : UserControl
  {
    private List<RadioButton> mButtons = new List<RadioButton>();

    private bool mFireEvent = true;
    [DefaultValue(true)]
    public bool FireEvent
    {
      get { return mFireEvent; }
      set { mFireEvent = value; }
    }

    private int mSelectedIndex = -1;
    public int SelectedIndex
    {
      get { return mSelectedIndex; }
      set 
      {
        if (mButtons.Count == 0)
          return;

        if(mButtons.Count <= value || value < 0)
          throw new Exception("(UserInteface.RadioGroup) index not valid");

        ButtonClicked(mButtons[value], null);
      }
    }

    private bool mIsHorizontal = true;
    [DefaultValue(true)]
    public bool IsHorizontal
    {
      get { return mIsHorizontal; }
      set 
      { 
        mIsHorizontal = value; 
        UpdateDesign(); 
      }
    }

    public new event EventHandler<EventArgs> Click;

    public RadioGroup()
    {
      InitializeComponent();
      ClearButtons();
      BackColor = Color.White;
    }

    public void ClearButtons()
    {
      foreach (RadioButton rb in mButtons)
      {
        rb.Parent = null;
        rb.Dispose();
      }

      mButtons.Clear();
      Width = 64;
      Height = 64;
      mSelectedIndex = -1;
      BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
    }

    public void AddButton(Bitmap img, string hint, bool select)
    {
      RadioButton rb = new RadioButton();
      rb.Parent = this;
      rb.Image = img;
      rb.Hint = hint;
      rb.Click += ButtonClicked;
      mButtons.Add(rb);
      UpdateDesign();

      bool fire = mFireEvent;
      mFireEvent = false;
      if (select)
        ButtonClicked(rb, null);
      mFireEvent = fire;
    }

    private void UpdateDesign()
    {
      if(mButtons.Count == 0)
        return;

      // Arrange buttons
      int size = 0;
      BorderStyle = System.Windows.Forms.BorderStyle.None;
      for(int i = 0; i < mButtons.Count; ++i)
        if (mIsHorizontal)
        {
          mButtons[i].Top = 0;
          mButtons[i].Left = i == 0 ? 0 : mButtons[i - 1].Right - 2;
          size = Math.Max(size, mButtons[i].Height);
        }
        else
        {
          mButtons[i].Left = 0;
          mButtons[i].Top = i == 0 ? 0 : mButtons[i - 1].Bottom - 2;
          size = Math.Max(size, mButtons[i].Width);
        }

      // Update component size
      int current_right = Right;      
      if (mIsHorizontal)
      {
        Width = mButtons[mButtons.Count - 1].Right;
        Height = size;
      }
      else
      {
        Width = size;
        Height = mButtons[mButtons.Count - 1].Bottom;
      }

      if (Parent as SessionAwareForm == null)
        return;

      Left = current_right - Width;
    }

    private void ButtonClicked(Object sender, EventArgs e)
    {
      mSelectedIndex = -1;
      for (int i = 0; i < mButtons.Count; ++i)
      {
        mButtons[i].Pressed = sender == mButtons[i];
        if(mButtons[i] == sender)
          mSelectedIndex = i;
      }

      if (mSelectedIndex == -1)
        throw new Exception("(UserInterface.RadioGroup) What the hell?");

      if (mFireEvent)
      {
        EventHandler<EventArgs> ev = Click;
        if (ev != null)
          ev(this, new EventArgs());
      }
    }
  }
}
