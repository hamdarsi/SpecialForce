﻿namespace SpecialForce.Options
{
  partial class FormOptions
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOptions));
      this.transparentLabel7 = new SpecialForce.TransparentLabel();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.btnSettingsDatabase = new SpecialForce.ToolButton();
      this.btnSettingsHardware = new SpecialForce.ToolButton();
      this.btnSettingsMessages = new SpecialForce.ToolButton();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.transparentLabel4 = new SpecialForce.TransparentLabel();
      this.transparentLabel5 = new SpecialForce.TransparentLabel();
      this.btnFinish = new System.Windows.Forms.Button();
      this.transparentLabel6 = new SpecialForce.TransparentLabel();
      this.transparentLabel8 = new SpecialForce.TransparentLabel();
      this.transparentLabel9 = new SpecialForce.TransparentLabel();
      this.btnSettingsProgram = new SpecialForce.ToolButton();
      this.transparentLabel10 = new SpecialForce.TransparentLabel();
      this.transparentLabel11 = new SpecialForce.TransparentLabel();
      this.toolBar1.SuspendLayout();
      this.SuspendLayout();
      // 
      // transparentLabel7
      // 
      this.transparentLabel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel7.Location = new System.Drawing.Point(-7, 314);
      this.transparentLabel7.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel7.Name = "transparentLabel7";
      this.transparentLabel7.Size = new System.Drawing.Size(451, 15);
      this.transparentLabel7.TabIndex = 107;
      this.transparentLabel7.TabStop = false;
      this.transparentLabel7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel7.Texts")));
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 1.5F;
      this.toolBar1.Controls.Add(this.transparentLabel2);
      this.toolBar1.Controls.Add(this.transparentLabel1);
      this.toolBar1.CornerRadius = 10;
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(-11, -8);
      this.toolBar1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(471, 82);
      this.toolBar1.TabIndex = 106;
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.FixFromRight = true;
      this.transparentLabel2.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel2.Location = new System.Drawing.Point(240, 42);
      this.transparentLabel2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(204, 26);
      this.transparentLabel2.TabIndex = 1;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.Location = new System.Drawing.Point(366, 15);
      this.transparentLabel1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(88, 27);
      this.transparentLabel1.TabIndex = 0;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // btnSettingsDatabase
      // 
      this.btnSettingsDatabase.Hint = "تنظیمات کلی سیستم در همه باشگاه ها";
      this.btnSettingsDatabase.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnSettingsDatabase.Image")));
      this.btnSettingsDatabase.Location = new System.Drawing.Point(42, 191);
      this.btnSettingsDatabase.Name = "btnSettingsDatabase";
      this.btnSettingsDatabase.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnSettingsDatabase.ShowBorder = false;
      this.btnSettingsDatabase.Size = new System.Drawing.Size(48, 48);
      this.btnSettingsDatabase.TabIndex = 108;
      this.btnSettingsDatabase.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnSettingsDatabase.Texts")));
      this.btnSettingsDatabase.TransparentColor = System.Drawing.Color.Transparent;
      this.btnSettingsDatabase.Click += new System.EventHandler(this.btnSettingsDatabase_Click);
      // 
      // btnSettingsHardware
      // 
      this.btnSettingsHardware.Hint = "تنظیمات برنامه در باشگاه";
      this.btnSettingsHardware.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnSettingsHardware.Image")));
      this.btnSettingsHardware.Location = new System.Drawing.Point(42, 83);
      this.btnSettingsHardware.Name = "btnSettingsHardware";
      this.btnSettingsHardware.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnSettingsHardware.ShowBorder = false;
      this.btnSettingsHardware.Size = new System.Drawing.Size(48, 48);
      this.btnSettingsHardware.TabIndex = 109;
      this.btnSettingsHardware.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnSettingsHardware.Texts")));
      this.btnSettingsHardware.TransparentColor = System.Drawing.Color.Transparent;
      this.btnSettingsHardware.Click += new System.EventHandler(this.btnSettingsHardware_Click);
      // 
      // btnSettingsMessages
      // 
      this.btnSettingsMessages.Hint = "تنظیمات پیام های مورد استفاده اپراتور";
      this.btnSettingsMessages.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnSettingsMessages.Image")));
      this.btnSettingsMessages.Location = new System.Drawing.Point(42, 245);
      this.btnSettingsMessages.Name = "btnSettingsMessages";
      this.btnSettingsMessages.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnSettingsMessages.ShowBorder = false;
      this.btnSettingsMessages.Size = new System.Drawing.Size(48, 48);
      this.btnSettingsMessages.TabIndex = 110;
      this.btnSettingsMessages.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnSettingsMessages.Texts")));
      this.btnSettingsMessages.TransparentColor = System.Drawing.Color.Transparent;
      this.btnSettingsMessages.Click += new System.EventHandler(this.btnSettingsMessages_Click);
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel3.Location = new System.Drawing.Point(116, 137);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(145, 24);
      this.transparentLabel3.TabIndex = 111;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // transparentLabel4
      // 
      this.transparentLabel4.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel4.Location = new System.Drawing.Point(116, 191);
      this.transparentLabel4.Name = "transparentLabel4";
      this.transparentLabel4.Size = new System.Drawing.Size(99, 24);
      this.transparentLabel4.TabIndex = 112;
      this.transparentLabel4.TabStop = false;
      this.transparentLabel4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel4.Texts")));
      // 
      // transparentLabel5
      // 
      this.transparentLabel5.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel5.Location = new System.Drawing.Point(116, 245);
      this.transparentLabel5.Name = "transparentLabel5";
      this.transparentLabel5.Size = new System.Drawing.Size(203, 24);
      this.transparentLabel5.TabIndex = 113;
      this.transparentLabel5.TabStop = false;
      this.transparentLabel5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel5.Texts")));
      // 
      // btnFinish
      // 
      this.btnFinish.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnFinish.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnFinish.ForeColor = System.Drawing.Color.Black;
      this.btnFinish.Location = new System.Drawing.Point(365, 341);
      this.btnFinish.Name = "btnFinish";
      this.btnFinish.Size = new System.Drawing.Size(75, 23);
      this.btnFinish.TabIndex = 114;
      this.btnFinish.Text = "پایان";
      this.btnFinish.UseVisualStyleBackColor = true;
      this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
      // 
      // transparentLabel6
      // 
      this.transparentLabel6.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel6.Location = new System.Drawing.Point(135, 161);
      this.transparentLabel6.Name = "transparentLabel6";
      this.transparentLabel6.Size = new System.Drawing.Size(192, 21);
      this.transparentLabel6.TabIndex = 111;
      this.transparentLabel6.TabStop = false;
      this.transparentLabel6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel6.Texts")));
      // 
      // transparentLabel8
      // 
      this.transparentLabel8.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel8.Location = new System.Drawing.Point(135, 215);
      this.transparentLabel8.Name = "transparentLabel8";
      this.transparentLabel8.Size = new System.Drawing.Size(165, 21);
      this.transparentLabel8.TabIndex = 115;
      this.transparentLabel8.TabStop = false;
      this.transparentLabel8.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel8.Texts")));
      // 
      // transparentLabel9
      // 
      this.transparentLabel9.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel9.Location = new System.Drawing.Point(135, 266);
      this.transparentLabel9.Name = "transparentLabel9";
      this.transparentLabel9.Size = new System.Drawing.Size(224, 21);
      this.transparentLabel9.TabIndex = 116;
      this.transparentLabel9.TabStop = false;
      this.transparentLabel9.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel9.Texts")));
      // 
      // btnSettingsProgram
      // 
      this.btnSettingsProgram.Hint = "تنظیمات کلی سیستم در همه باشگاه ها";
      this.btnSettingsProgram.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnSettingsProgram.Image")));
      this.btnSettingsProgram.Location = new System.Drawing.Point(42, 137);
      this.btnSettingsProgram.Name = "btnSettingsProgram";
      this.btnSettingsProgram.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnSettingsProgram.ShowBorder = false;
      this.btnSettingsProgram.Size = new System.Drawing.Size(48, 48);
      this.btnSettingsProgram.TabIndex = 117;
      this.btnSettingsProgram.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnSettingsProgram.Texts")));
      this.btnSettingsProgram.TransparentColor = System.Drawing.Color.Transparent;
      this.btnSettingsProgram.Click += new System.EventHandler(this.btnSettingsProgram_Click);
      // 
      // transparentLabel10
      // 
      this.transparentLabel10.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel10.Location = new System.Drawing.Point(135, 107);
      this.transparentLabel10.Name = "transparentLabel10";
      this.transparentLabel10.Size = new System.Drawing.Size(134, 21);
      this.transparentLabel10.TabIndex = 118;
      this.transparentLabel10.TabStop = false;
      this.transparentLabel10.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel10.Texts")));
      // 
      // transparentLabel11
      // 
      this.transparentLabel11.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel11.Location = new System.Drawing.Point(116, 83);
      this.transparentLabel11.Name = "transparentLabel11";
      this.transparentLabel11.Size = new System.Drawing.Size(111, 24);
      this.transparentLabel11.TabIndex = 119;
      this.transparentLabel11.TabStop = false;
      this.transparentLabel11.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel11.Texts")));
      // 
      // FormOptions
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnFinish;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.transparentLabel10);
      this.Controls.Add(this.transparentLabel11);
      this.Controls.Add(this.btnSettingsProgram);
      this.Controls.Add(this.transparentLabel9);
      this.Controls.Add(this.transparentLabel8);
      this.Controls.Add(this.btnFinish);
      this.Controls.Add(this.transparentLabel5);
      this.Controls.Add(this.transparentLabel4);
      this.Controls.Add(this.transparentLabel6);
      this.Controls.Add(this.transparentLabel3);
      this.Controls.Add(this.btnSettingsMessages);
      this.Controls.Add(this.btnSettingsHardware);
      this.Controls.Add(this.btnSettingsDatabase);
      this.Controls.Add(this.transparentLabel7);
      this.Controls.Add(this.toolBar1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormOptions";
      this.Text = "تنظیمات";
      this.toolBar1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private TransparentLabel transparentLabel7;
    private ToolBar toolBar1;
    private TransparentLabel transparentLabel2;
    private TransparentLabel transparentLabel1;
    private ToolButton btnSettingsDatabase;
    private ToolButton btnSettingsHardware;
    private ToolButton btnSettingsMessages;
    private TransparentLabel transparentLabel3;
    private TransparentLabel transparentLabel4;
    private TransparentLabel transparentLabel5;
    private System.Windows.Forms.Button btnFinish;
    private TransparentLabel transparentLabel6;
    private TransparentLabel transparentLabel8;
    private TransparentLabel transparentLabel9;
    private ToolButton btnSettingsProgram;
    private TransparentLabel transparentLabel10;
    private TransparentLabel transparentLabel11;


  }
}