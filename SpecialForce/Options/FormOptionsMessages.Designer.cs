﻿namespace SpecialForce.Options
{
  partial class FormOptionsMessages
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOptionsMessages));
      this.btnCancel = new System.Windows.Forms.Button();
      this.transparentLabel7 = new SpecialForce.TransparentLabel();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.tbTexts = new SpecialForce.ToolBar();
      this.btnUndo = new SpecialForce.ToolButton();
      this.btnApply = new SpecialForce.ToolButton();
      this.lblMessageLine2 = new SpecialForce.TransparentLabel();
      this.transparentLabel9 = new SpecialForce.TransparentLabel();
      this.txtTitle = new System.Windows.Forms.TextBox();
      this.transparentLabel8 = new SpecialForce.TransparentLabel();
      this.transparentLabel10 = new SpecialForce.TransparentLabel();
      this.txtMessageLine2 = new System.Windows.Forms.TextBox();
      this.lblMessageLine1 = new SpecialForce.TransparentLabel();
      this.transparentLabel6 = new SpecialForce.TransparentLabel();
      this.transparentLabel5 = new SpecialForce.TransparentLabel();
      this.txtMessageLine1 = new System.Windows.Forms.TextBox();
      this.transparentLabel4 = new SpecialForce.TransparentLabel();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.transparentLabel11 = new SpecialForce.TransparentLabel();
      this.tbMessages = new SpecialForce.ToolBar();
      this.btnDelete = new SpecialForce.ToolButton();
      this.btnNew = new SpecialForce.ToolButton();
      this.lbMessages = new System.Windows.Forms.ListBox();
      this.toolBar1.SuspendLayout();
      this.tbTexts.SuspendLayout();
      this.tbMessages.SuspendLayout();
      this.SuspendLayout();
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(369, 341);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 100;
      this.btnCancel.Text = "بازگشت";
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // transparentLabel7
      // 
      this.transparentLabel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel7.Location = new System.Drawing.Point(-7, 314);
      this.transparentLabel7.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel7.Name = "transparentLabel7";
      this.transparentLabel7.Size = new System.Drawing.Size(451, 15);
      this.transparentLabel7.TabIndex = 102;
      this.transparentLabel7.TabStop = false;
      this.transparentLabel7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel7.Texts")));
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 1.5F;
      this.toolBar1.Controls.Add(this.transparentLabel2);
      this.toolBar1.Controls.Add(this.transparentLabel1);
      this.toolBar1.CornerRadius = 10;
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(-11, -8);
      this.toolBar1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(471, 82);
      this.toolBar1.TabIndex = 101;
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel2.Location = new System.Drawing.Point(182, 42);
      this.transparentLabel2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(272, 26);
      this.transparentLabel2.TabIndex = 1;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.Location = new System.Drawing.Point(366, 15);
      this.transparentLabel1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(88, 27);
      this.transparentLabel1.TabIndex = 0;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // tbTexts
      // 
      this.tbTexts.AllowDrop = true;
      this.tbTexts.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbTexts.BorderWidth = 0.5F;
      this.tbTexts.Controls.Add(this.btnUndo);
      this.tbTexts.Controls.Add(this.btnApply);
      this.tbTexts.Controls.Add(this.lblMessageLine2);
      this.tbTexts.Controls.Add(this.transparentLabel9);
      this.tbTexts.Controls.Add(this.txtTitle);
      this.tbTexts.Controls.Add(this.transparentLabel8);
      this.tbTexts.Controls.Add(this.transparentLabel10);
      this.tbTexts.Controls.Add(this.txtMessageLine2);
      this.tbTexts.Controls.Add(this.lblMessageLine1);
      this.tbTexts.Controls.Add(this.transparentLabel6);
      this.tbTexts.Controls.Add(this.transparentLabel5);
      this.tbTexts.Controls.Add(this.txtMessageLine1);
      this.tbTexts.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbTexts.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbTexts.Location = new System.Drawing.Point(170, 83);
      this.tbTexts.Name = "tbTexts";
      this.tbTexts.Size = new System.Drawing.Size(274, 189);
      this.tbTexts.TabIndex = 103;
      // 
      // btnUndo
      // 
      this.btnUndo.Enabled = false;
      this.btnUndo.Hint = "بازگردانی تغییرات";
      this.btnUndo.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnUndo.Image")));
      this.btnUndo.Location = new System.Drawing.Point(66, 151);
      this.btnUndo.Name = "btnUndo";
      this.btnUndo.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnUndo.ShowBorder = false;
      this.btnUndo.Size = new System.Drawing.Size(47, 34);
      this.btnUndo.TabIndex = 57;
      this.btnUndo.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnUndo.Texts")));
      this.btnUndo.TransparentColor = System.Drawing.Color.Transparent;
      this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
      // 
      // btnApply
      // 
      this.btnApply.Hint = "اعمال تغییرات";
      this.btnApply.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnApply.Image")));
      this.btnApply.Location = new System.Drawing.Point(13, 151);
      this.btnApply.Name = "btnApply";
      this.btnApply.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnApply.ShowBorder = false;
      this.btnApply.Size = new System.Drawing.Size(47, 34);
      this.btnApply.TabIndex = 56;
      this.btnApply.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnApply.Texts")));
      this.btnApply.TransparentColor = System.Drawing.Color.Transparent;
      this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
      // 
      // lblMessageLine2
      // 
      this.lblMessageLine2.FixFromRight = true;
      this.lblMessageLine2.Font = new System.Drawing.Font("DejaVu Sans Mono", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblMessageLine2.Location = new System.Drawing.Point(24, 124);
      this.lblMessageLine2.Name = "lblMessageLine2";
      this.lblMessageLine2.RightToLeft = false;
      this.lblMessageLine2.Size = new System.Drawing.Size(113, 14);
      this.lblMessageLine2.TabIndex = 53;
      this.lblMessageLine2.TabStop = false;
      this.lblMessageLine2.TextAlign = SpecialForce.Alignment.MiddleLeft;
      this.lblMessageLine2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblMessageLine2.Texts")));
      // 
      // transparentLabel9
      // 
      this.transparentLabel9.FixFromRight = true;
      this.transparentLabel9.Font = new System.Drawing.Font("B Nazanin", 9.75F);
      this.transparentLabel9.Location = new System.Drawing.Point(140, 120);
      this.transparentLabel9.Name = "transparentLabel9";
      this.transparentLabel9.Size = new System.Drawing.Size(113, 21);
      this.transparentLabel9.TabIndex = 52;
      this.transparentLabel9.TabStop = false;
      this.transparentLabel9.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel9.Texts")));
      // 
      // txtTitle
      // 
      this.txtTitle.Location = new System.Drawing.Point(119, 10);
      this.txtTitle.Name = "txtTitle";
      this.txtTitle.Size = new System.Drawing.Size(100, 21);
      this.txtTitle.TabIndex = 0;
      this.txtTitle.TextChanged += new System.EventHandler(this.txtTitle_TextChanged);
      // 
      // transparentLabel8
      // 
      this.transparentLabel8.FixFromRight = true;
      this.transparentLabel8.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel8.Location = new System.Drawing.Point(236, 7);
      this.transparentLabel8.Name = "transparentLabel8";
      this.transparentLabel8.Size = new System.Drawing.Size(28, 26);
      this.transparentLabel8.TabIndex = 54;
      this.transparentLabel8.TabStop = false;
      this.transparentLabel8.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel8.Texts")));
      // 
      // transparentLabel10
      // 
      this.transparentLabel10.FixFromRight = true;
      this.transparentLabel10.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel10.Location = new System.Drawing.Point(225, 98);
      this.transparentLabel10.Name = "transparentLabel10";
      this.transparentLabel10.Size = new System.Drawing.Size(39, 26);
      this.transparentLabel10.TabIndex = 51;
      this.transparentLabel10.TabStop = false;
      this.transparentLabel10.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel10.Texts")));
      // 
      // txtMessageLine2
      // 
      this.txtMessageLine2.Location = new System.Drawing.Point(119, 99);
      this.txtMessageLine2.Name = "txtMessageLine2";
      this.txtMessageLine2.Size = new System.Drawing.Size(100, 21);
      this.txtMessageLine2.TabIndex = 2;
      this.txtMessageLine2.TextChanged += new System.EventHandler(this.txtMessageLine2_TextChanged);
      // 
      // lblMessageLine1
      // 
      this.lblMessageLine1.FixFromRight = true;
      this.lblMessageLine1.Font = new System.Drawing.Font("DejaVu Sans Mono", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblMessageLine1.Location = new System.Drawing.Point(99, 78);
      this.lblMessageLine1.Name = "lblMessageLine1";
      this.lblMessageLine1.RightToLeft = false;
      this.lblMessageLine1.Size = new System.Drawing.Size(38, 14);
      this.lblMessageLine1.TabIndex = 49;
      this.lblMessageLine1.TabStop = false;
      this.lblMessageLine1.TextAlign = SpecialForce.Alignment.MiddleLeft;
      this.lblMessageLine1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblMessageLine1.Texts")));
      // 
      // transparentLabel6
      // 
      this.transparentLabel6.FixFromRight = true;
      this.transparentLabel6.Font = new System.Drawing.Font("B Nazanin", 9.75F);
      this.transparentLabel6.Location = new System.Drawing.Point(140, 74);
      this.transparentLabel6.Name = "transparentLabel6";
      this.transparentLabel6.Size = new System.Drawing.Size(113, 21);
      this.transparentLabel6.TabIndex = 48;
      this.transparentLabel6.TabStop = false;
      this.transparentLabel6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel6.Texts")));
      // 
      // transparentLabel5
      // 
      this.transparentLabel5.FixFromRight = true;
      this.transparentLabel5.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel5.Location = new System.Drawing.Point(225, 52);
      this.transparentLabel5.Name = "transparentLabel5";
      this.transparentLabel5.Size = new System.Drawing.Size(39, 26);
      this.transparentLabel5.TabIndex = 47;
      this.transparentLabel5.TabStop = false;
      this.transparentLabel5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel5.Texts")));
      // 
      // txtMessageLine1
      // 
      this.txtMessageLine1.Location = new System.Drawing.Point(119, 53);
      this.txtMessageLine1.Name = "txtMessageLine1";
      this.txtMessageLine1.Size = new System.Drawing.Size(100, 21);
      this.txtMessageLine1.TabIndex = 1;
      this.txtMessageLine1.TextChanged += new System.EventHandler(this.txtMessageLine1_TextChanged);
      // 
      // transparentLabel4
      // 
      this.transparentLabel4.FixFromRight = true;
      this.transparentLabel4.Font = new System.Drawing.Font("DejaVu Sans Mono", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel4.Location = new System.Drawing.Point(44, 298);
      this.transparentLabel4.Name = "transparentLabel4";
      this.transparentLabel4.Size = new System.Drawing.Size(352, 14);
      this.transparentLabel4.TabIndex = 55;
      this.transparentLabel4.TabStop = false;
      this.transparentLabel4.TextAlign = SpecialForce.Alignment.MiddleLeft;
      this.transparentLabel4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel4.Texts")));
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.FixFromRight = true;
      this.transparentLabel3.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel3.Location = new System.Drawing.Point(12, 278);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(331, 21);
      this.transparentLabel3.TabIndex = 54;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // transparentLabel11
      // 
      this.transparentLabel11.FixFromRight = true;
      this.transparentLabel11.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel11.Location = new System.Drawing.Point(66, 0);
      this.transparentLabel11.Name = "transparentLabel11";
      this.transparentLabel11.Size = new System.Drawing.Size(86, 26);
      this.transparentLabel11.TabIndex = 104;
      this.transparentLabel11.TabStop = false;
      this.transparentLabel11.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel11.Texts")));
      // 
      // tbMessages
      // 
      this.tbMessages.AllowDrop = true;
      this.tbMessages.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbMessages.BorderWidth = 0.5F;
      this.tbMessages.Controls.Add(this.btnDelete);
      this.tbMessages.Controls.Add(this.btnNew);
      this.tbMessages.Controls.Add(this.lbMessages);
      this.tbMessages.Controls.Add(this.transparentLabel11);
      this.tbMessages.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbMessages.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbMessages.Location = new System.Drawing.Point(6, 83);
      this.tbMessages.Name = "tbMessages";
      this.tbMessages.Size = new System.Drawing.Size(158, 189);
      this.tbMessages.TabIndex = 104;
      // 
      // btnDelete
      // 
      this.btnDelete.Enabled = false;
      this.btnDelete.Hint = "حذف پیام";
      this.btnDelete.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnDelete.Image")));
      this.btnDelete.Location = new System.Drawing.Point(65, 151);
      this.btnDelete.Name = "btnDelete";
      this.btnDelete.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnDelete.ShowBorder = false;
      this.btnDelete.Size = new System.Drawing.Size(47, 34);
      this.btnDelete.TabIndex = 105;
      this.btnDelete.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnDelete.Texts")));
      this.btnDelete.TransparentColor = System.Drawing.Color.Transparent;
      this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
      // 
      // btnNew
      // 
      this.btnNew.Hint = "پیام جدید";
      this.btnNew.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnNew.Image")));
      this.btnNew.Location = new System.Drawing.Point(12, 152);
      this.btnNew.Name = "btnNew";
      this.btnNew.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnNew.ShowBorder = false;
      this.btnNew.Size = new System.Drawing.Size(47, 34);
      this.btnNew.TabIndex = 55;
      this.btnNew.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnNew.Texts")));
      this.btnNew.TransparentColor = System.Drawing.Color.Transparent;
      this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
      // 
      // lbMessages
      // 
      this.lbMessages.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.lbMessages.FormattingEnabled = true;
      this.lbMessages.Location = new System.Drawing.Point(12, 29);
      this.lbMessages.Name = "lbMessages";
      this.lbMessages.Size = new System.Drawing.Size(134, 108);
      this.lbMessages.TabIndex = 3;
      this.lbMessages.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lbMessages_DrawItem);
      this.lbMessages.SelectedIndexChanged += new System.EventHandler(this.lbMessages_SelectedIndexChanged);
      // 
      // FormOptionsMessages
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.tbMessages);
      this.Controls.Add(this.transparentLabel4);
      this.Controls.Add(this.tbTexts);
      this.Controls.Add(this.transparentLabel3);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.transparentLabel7);
      this.Controls.Add(this.toolBar1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormOptionsMessages";
      this.Text = "تنظیمات پیام ها";
      this.Load += new System.EventHandler(this.FormOptionsMessages_Load);
      this.toolBar1.ResumeLayout(false);
      this.tbTexts.ResumeLayout(false);
      this.tbTexts.PerformLayout();
      this.tbMessages.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnCancel;
    private TransparentLabel transparentLabel7;
    private ToolBar toolBar1;
    private TransparentLabel transparentLabel2;
    private TransparentLabel transparentLabel1;
    private ToolBar tbTexts;
    private TransparentLabel lblMessageLine2;
    private TransparentLabel transparentLabel9;
    private TransparentLabel transparentLabel10;
    private System.Windows.Forms.TextBox txtMessageLine2;
    private TransparentLabel lblMessageLine1;
    private TransparentLabel transparentLabel6;
    private TransparentLabel transparentLabel5;
    private System.Windows.Forms.TextBox txtMessageLine1;
    private TransparentLabel transparentLabel4;
    private TransparentLabel transparentLabel3;
    private TransparentLabel transparentLabel8;
    private System.Windows.Forms.TextBox txtTitle;
    private TransparentLabel transparentLabel11;
    private ToolBar tbMessages;
    private ToolButton btnUndo;
    private ToolButton btnApply;
    private ToolButton btnDelete;
    private ToolButton btnNew;
    private System.Windows.Forms.ListBox lbMessages;
  }
}