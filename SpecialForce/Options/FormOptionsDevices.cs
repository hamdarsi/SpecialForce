﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SpecialForce.Options
{
  public partial class FormOptionsDevices : SessionAwareForm
  {
    private List<string> mPortNames;

    public FormOptionsDevices()
    {
      InitializeComponent();
    }

    protected override void ApplyPermissions(Post post)
    {
      base.ApplyPermissions(post);
      cmbFingerPrint.Enabled = Session.CurrentStaff.IsPrivileged();
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void SelectComboIndex(ComboBox cmb, string value)
    {
      for(int i = 0; i < cmb.Items.Count; ++i)
        if (cmb.Items[i].ToString() == value)
        {
          cmb.SelectedIndex = i;
          return;
        }

      cmb.SelectedIndex = 0;
    }

    private void FormOptionsDevices_Load(object sender, EventArgs e)
    {
      mPortNames = ControlTower.Wireless.AllPortNames;
      cmbFingerPrint.SelectedIndex = Properties.Settings.Default.FingerPrintModuleEnabled ? 0 : 1;

      ComboBox [] cmbs = new ComboBox [] {cmbWirelessCenter, cmbWirelessBlueBase, cmbWirelessGreenBase};
      foreach (ComboBox cmb in cmbs)
      {
        cmb.Items.Add("");
        for (int i = 0; i < mPortNames.Count; ++i)
          cmb.Items.Add(mPortNames[i]);
      }

      SelectComboIndex(cmbWirelessCenter, Properties.Settings.Default.WirelessPortCenter);
      SelectComboIndex(cmbWirelessBlueBase, Properties.Settings.Default.WirelessPortBlue);
      SelectComboIndex(cmbWirelessGreenBase, Properties.Settings.Default.WirelessPortGreen);
    }

    private void btnTestClick(object sender, EventArgs e)
    {
      ComboBox cmb = null;
      if(sender == btnTestCenter)
        cmb = cmbWirelessCenter;
      else if(sender == btnTestBlue)
        cmb = cmbWirelessBlueBase;
      else if(sender == btnTestGreen)
        cmb = cmbWirelessGreenBase;
      
      string port = cmb != null ? cmb.SelectedItem.ToString() : "";
      if(!ControlTower.Wireless.TestDevice(port))
        Session.ShowMessage("دستگاه فعال نشد.");
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      // Fingerprint
      bool fpEnable = cmbFingerPrint.SelectedItem.ToString() == "بله";
      if (fpEnable != Properties.Settings.Default.FingerPrintModuleEnabled)
      {
        Properties.Settings.Default.FingerPrintModuleEnabled = fpEnable;
        if (fpEnable && FingerPrint.State != FingerPrintState.Active)
          return;
      }

      // Wireless
      string center = cmbWirelessCenter.SelectedItem.ToString();
      string blue = cmbWirelessBlueBase.SelectedItem.ToString();
      string green = cmbWirelessGreenBase.SelectedItem.ToString();
      bool wl_enabled = center != "";
      WirelessState wls = ControlTower.CommandQueue.AssignPorts(center, blue, green);
      if (wl_enabled && (center == blue || center == green || (blue == green && blue != "")))
        Session.ShowMessage("ماژول ها نباید هم نام باشند.");
      else if (wls == WirelessState.CenterNotWorking && wl_enabled)
        Session.ShowMessage("ماژول وسط زمین فعال نمی شود.");
      else if (wls == WirelessState.BlueNotWorking && wl_enabled)
        Session.ShowMessage("ماژول پایگاه تیم آبی فعال نمی شود.");
      else if (wls == WirelessState.GreenNotWorking && wl_enabled)
        Session.ShowMessage("ماژول پایگاه تیم سبز فعال نمی شود.");
      else
      {
        // Done
        if (wl_enabled)
        {
          Properties.Settings.Default.WirelessPortCenter = center;
          Properties.Settings.Default.WirelessPortBlue = blue;
          Properties.Settings.Default.WirelessPortGreen = green;
          DB.GameManager.StartDaemon(true);
        }
        else
        {
          Properties.Settings.Default.WirelessPortCenter = "";
          Properties.Settings.Default.WirelessPortBlue = "";
          Properties.Settings.Default.WirelessPortGreen = "";
          DB.GameManager.EndDaemon(true);
        }

        Properties.Settings.Default.Save();
        DialogResult = DialogResult.OK;
        Close();
      }
    }
  }
}
