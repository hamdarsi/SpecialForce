﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Options
{
  public partial class FormOptionsMessages : SessionAwareForm
  {
    DB.Message mCurrentMsg = null;

    public FormOptionsMessages()
    {
      InitializeComponent();
    }

    private void LoadAllMessages(int msg_id = -1)
    {
      int index = -1;
      lbMessages.Items.Clear();
      lbMessages.SelectedItems.Clear();

      List<DB.Message> msgs = DB.Message.GetAllMessages();
      for(int i = 0; i < msgs.Count; ++i)
      {
        lbMessages.Items.Add(msgs[i]);
        if (msgs[i].ID == msg_id)
          index = i;
      }

      lbMessages.SelectedIndex = index;
    }

    private void SelectMessage(DB.Message msg)
    {
      txtTitle.Text = msg == null ? "" : msg.Title;
      txtMessageLine1.Text = msg == null ? "" : msg.Line1;
      txtMessageLine2.Text = msg == null ? "" : msg.Line2;

      mCurrentMsg = msg;
      btnUndo.Enabled = false;
      tbTexts.Enabled = mCurrentMsg == null || mCurrentMsg.Deleted == false;
    }

    private void lbMessages_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (lbMessages.SelectedItems.Count == 0)
        SelectMessage(null);
      else
        SelectMessage(lbMessages.SelectedItem as DB.Message);

      btnDelete.Enabled = lbMessages.SelectedIndex != -1;
    }

    private void btnNew_Click(object sender, EventArgs e)
    {
      lbMessages.SelectedIndex = -1;
      mCurrentMsg = null;
    }

    private void btnUndo_Click(object sender, EventArgs e)
    {
      SelectMessage(mCurrentMsg);
    }

    private void btnApply_Click(object sender, EventArgs e)
    {
      if (txtTitle.Text.Trim().Length == 0)
        Session.ShowError("تیتر پیام باید تنظیم شود.");
      else if (txtMessageLine1.Text.Trim().Length == 0 && txtMessageLine2.Text.Trim().Length == 0)
        Session.ShowError("پیامی برای فرستادن نوشته نشده است/");
      else
      {
        if (mCurrentMsg == null)
          mCurrentMsg = new DB.Message();

        mCurrentMsg.Title = txtTitle.Text;
        mCurrentMsg.Line1 = txtMessageLine1.Text;
        mCurrentMsg.Line2 = txtMessageLine2.Text;
        mCurrentMsg.Apply();

        LoadAllMessages(mCurrentMsg.ID);
      }
    }

    private void FormOptionsMessages_Load(object sender, EventArgs e)
    {
      LoadAllMessages();
      txtMessageLine1.RightToLeft = System.Windows.Forms.RightToLeft.No;
      txtMessageLine2.RightToLeft = System.Windows.Forms.RightToLeft.No;

      txtMessageLine1_TextChanged(null, null);
      txtMessageLine2_TextChanged(null, null);
    }

    private void txtMessageLine1_TextChanged(object sender, EventArgs e)
    {
      lblMessageLine1.Text = "\"" + Command.CorrectMessage(txtMessageLine1.Text) + "\"";
      btnUndo.Enabled = true;
    }

    private void txtMessageLine2_TextChanged(object sender, EventArgs e)
    {
      lblMessageLine2.Text = "\"" + Command.CorrectMessage(txtMessageLine2.Text) + "\"";
      btnUndo.Enabled = true;
    }

    private void txtTitle_TextChanged(object sender, EventArgs e)
    {
      btnUndo.Enabled = true;
    }

    private void btnDelete_Click(object sender, EventArgs e)
    {
      if (mCurrentMsg.Deleted)
        Session.ShowError("پیام حذف شده است.");
      else if (!Session.Ask("آیا می خواهید پیام " + mCurrentMsg.Title + " را حذف کنید؟"))
        return;
      else
      {
        mCurrentMsg.Delete();
        LoadAllMessages();
      }
    }

    private void lbMessages_DrawItem(object sender, DrawItemEventArgs e)
    {
      e.DrawBackground();

      bool selected = (e.State & DrawItemState.Selected) == DrawItemState.Selected;

      int index = e.Index;
      if (index >= 0 && index < lbMessages.Items.Count)
      {
        DB.Message msg = lbMessages.Items[index] as DB.Message;

        // Draw item background
        Color color = Color.White;
        if (selected)
          color = Color.FromKnownColor(KnownColor.Highlight);
        else if (msg.Deleted)
          color = Color.FromArgb(255, 150, 150);
        else if (index % 2 == 0)
          color = Color.FromArgb(230, 230, 230);
        e.Graphics.FillRectangle(new SolidBrush(color), e.Bounds);

        // Draw the message title
        TextRenderer tr = new TextRenderer();
        tr.Font = lbMessages.Font;
        tr.Location = e.Bounds.Location;
        tr.Size = e.Bounds.Size;
        tr.Color = selected ? Color.White : Color.Black;
        tr.Canvas = e.Graphics;
        tr.Text = msg.Title;
        tr.Justify = Alignment.MiddleRight;
        tr.Draw();
      }

      e.DrawFocusRectangle();
    }
  }
}
