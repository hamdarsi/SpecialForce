﻿namespace SpecialForce.Options
{
  partial class FormOptionsDB
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOptionsDB));
      this.txtKillFrag = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel12 = new SpecialForce.TransparentLabel();
      this.transparentLabel13 = new SpecialForce.TransparentLabel();
      this.txtDiffuseFrag = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel10 = new SpecialForce.TransparentLabel();
      this.transparentLabel11 = new SpecialForce.TransparentLabel();
      this.txtPlantFrag = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel8 = new SpecialForce.TransparentLabel();
      this.transparentLabel9 = new SpecialForce.TransparentLabel();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnSave = new System.Windows.Forms.Button();
      this.transparentLabel7 = new SpecialForce.TransparentLabel();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.label4 = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.label2 = new SpecialForce.TransparentLabel();
      this.txtFreeCharge = new SpecialForce.Utilities.NumberBox();
      this.txtNeededCharge = new SpecialForce.Utilities.NumberBox();
      this.label5 = new SpecialForce.TransparentLabel();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.txtFriendlyKillFrag = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel4 = new SpecialForce.TransparentLabel();
      this.pbFocus = new System.Windows.Forms.PictureBox();
      this.txtTeamMembers = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel5 = new SpecialForce.TransparentLabel();
      this.transparentLabel6 = new SpecialForce.TransparentLabel();
      this.txtMaxTeamNameChars = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel14 = new SpecialForce.TransparentLabel();
      this.transparentLabel15 = new SpecialForce.TransparentLabel();
      this.transparentLabel16 = new SpecialForce.TransparentLabel();
      this.txtRefundAcception = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel17 = new SpecialForce.TransparentLabel();
      this.toolBar1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbFocus)).BeginInit();
      this.SuspendLayout();
      // 
      // txtKillFrag
      // 
      this.txtKillFrag.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtKillFrag.Location = new System.Drawing.Point(269, 183);
      this.txtKillFrag.Name = "txtKillFrag";
      this.txtKillFrag.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtKillFrag.Size = new System.Drawing.Size(68, 21);
      this.txtKillFrag.TabIndex = 4;
      this.txtKillFrag.Text = "1";
      this.txtKillFrag.Value = 1D;
      this.txtKillFrag.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtNeededCharge_OnNumberEntered);
      this.txtKillFrag.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.txtFriendlyKillFrag_OnFocusReceived);
      // 
      // transparentLabel12
      // 
      this.transparentLabel12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel12.Location = new System.Drawing.Point(345, 186);
      this.transparentLabel12.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel12.Name = "transparentLabel12";
      this.transparentLabel12.Size = new System.Drawing.Size(24, 15);
      this.transparentLabel12.TabIndex = 93;
      this.transparentLabel12.TabStop = false;
      this.transparentLabel12.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel12.Texts")));
      // 
      // transparentLabel13
      // 
      this.transparentLabel13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel13.Location = new System.Drawing.Point(36, 186);
      this.transparentLabel13.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel13.Name = "transparentLabel13";
      this.transparentLabel13.Size = new System.Drawing.Size(65, 15);
      this.transparentLabel13.TabIndex = 92;
      this.transparentLabel13.TabStop = false;
      this.transparentLabel13.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel13.Texts")));
      // 
      // txtDiffuseFrag
      // 
      this.txtDiffuseFrag.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtDiffuseFrag.Location = new System.Drawing.Point(269, 157);
      this.txtDiffuseFrag.Name = "txtDiffuseFrag";
      this.txtDiffuseFrag.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtDiffuseFrag.Size = new System.Drawing.Size(68, 21);
      this.txtDiffuseFrag.TabIndex = 3;
      this.txtDiffuseFrag.Text = "3";
      this.txtDiffuseFrag.Value = 3D;
      this.txtDiffuseFrag.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtNeededCharge_OnNumberEntered);
      this.txtDiffuseFrag.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.txtFriendlyKillFrag_OnFocusReceived);
      // 
      // transparentLabel10
      // 
      this.transparentLabel10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel10.Location = new System.Drawing.Point(345, 160);
      this.transparentLabel10.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel10.Name = "transparentLabel10";
      this.transparentLabel10.Size = new System.Drawing.Size(24, 15);
      this.transparentLabel10.TabIndex = 90;
      this.transparentLabel10.TabStop = false;
      this.transparentLabel10.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel10.Texts")));
      // 
      // transparentLabel11
      // 
      this.transparentLabel11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel11.Location = new System.Drawing.Point(35, 160);
      this.transparentLabel11.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel11.Name = "transparentLabel11";
      this.transparentLabel11.Size = new System.Drawing.Size(94, 15);
      this.transparentLabel11.TabIndex = 89;
      this.transparentLabel11.TabStop = false;
      this.transparentLabel11.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel11.Texts")));
      // 
      // txtPlantFrag
      // 
      this.txtPlantFrag.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtPlantFrag.Location = new System.Drawing.Point(269, 131);
      this.txtPlantFrag.Name = "txtPlantFrag";
      this.txtPlantFrag.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtPlantFrag.Size = new System.Drawing.Size(68, 21);
      this.txtPlantFrag.TabIndex = 2;
      this.txtPlantFrag.Text = "3";
      this.txtPlantFrag.Value = 3D;
      this.txtPlantFrag.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtNeededCharge_OnNumberEntered);
      this.txtPlantFrag.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.txtFriendlyKillFrag_OnFocusReceived);
      // 
      // transparentLabel8
      // 
      this.transparentLabel8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel8.Location = new System.Drawing.Point(345, 134);
      this.transparentLabel8.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel8.Name = "transparentLabel8";
      this.transparentLabel8.Size = new System.Drawing.Size(24, 15);
      this.transparentLabel8.TabIndex = 87;
      this.transparentLabel8.TabStop = false;
      this.transparentLabel8.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel8.Texts")));
      // 
      // transparentLabel9
      // 
      this.transparentLabel9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel9.Location = new System.Drawing.Point(35, 134);
      this.transparentLabel9.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel9.Name = "transparentLabel9";
      this.transparentLabel9.Size = new System.Drawing.Size(88, 15);
      this.transparentLabel9.TabIndex = 86;
      this.transparentLabel9.TabStop = false;
      this.transparentLabel9.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel9.Texts")));
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(369, 341);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 9;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // btnSave
      // 
      this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnSave.ForeColor = System.Drawing.Color.Black;
      this.btnSave.Location = new System.Drawing.Point(287, 341);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(75, 23);
      this.btnSave.TabIndex = 8;
      this.btnSave.Text = "ثبت";
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      // 
      // transparentLabel7
      // 
      this.transparentLabel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel7.Location = new System.Drawing.Point(-7, 314);
      this.transparentLabel7.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel7.Name = "transparentLabel7";
      this.transparentLabel7.Size = new System.Drawing.Size(451, 15);
      this.transparentLabel7.TabIndex = 83;
      this.transparentLabel7.TabStop = false;
      this.transparentLabel7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel7.Texts")));
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 1.5F;
      this.toolBar1.Controls.Add(this.transparentLabel2);
      this.toolBar1.Controls.Add(this.transparentLabel1);
      this.toolBar1.CornerRadius = 10;
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(-11, -8);
      this.toolBar1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(471, 82);
      this.toolBar1.TabIndex = 72;
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel2.Location = new System.Drawing.Point(182, 42);
      this.transparentLabel2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(272, 26);
      this.transparentLabel2.TabIndex = 1;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.Location = new System.Drawing.Point(366, 15);
      this.transparentLabel1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(88, 27);
      this.transparentLabel1.TabIndex = 0;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // label4
      // 
      this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label4.Location = new System.Drawing.Point(344, 108);
      this.label4.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(28, 15);
      this.label4.TabIndex = 70;
      this.label4.TabStop = false;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // label1
      // 
      this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label1.Location = new System.Drawing.Point(31, 82);
      this.label1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(216, 15);
      this.label1.TabIndex = 68;
      this.label1.TabStop = false;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // label2
      // 
      this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label2.Location = new System.Drawing.Point(35, 108);
      this.label2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(57, 15);
      this.label2.TabIndex = 69;
      this.label2.TabStop = false;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // txtFreeCharge
      // 
      this.txtFreeCharge.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtFreeCharge.Location = new System.Drawing.Point(269, 105);
      this.txtFreeCharge.Mode = SpecialForce.Utilities.NumberBox.NumberMode.Currency;
      this.txtFreeCharge.Name = "txtFreeCharge";
      this.txtFreeCharge.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtFreeCharge.Size = new System.Drawing.Size(68, 21);
      this.txtFreeCharge.TabIndex = 1;
      this.txtFreeCharge.Text = "$5,000.00";
      this.txtFreeCharge.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtNeededCharge_OnNumberEntered);
      this.txtFreeCharge.OnNumberChanged += new System.EventHandler<System.EventArgs>(this.txtFreeCharge_OnNumberChanged);
      this.txtFreeCharge.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.txtFriendlyKillFrag_OnFocusReceived);
      // 
      // txtNeededCharge
      // 
      this.txtNeededCharge.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtNeededCharge.Location = new System.Drawing.Point(269, 79);
      this.txtNeededCharge.Mode = SpecialForce.Utilities.NumberBox.NumberMode.Currency;
      this.txtNeededCharge.Name = "txtNeededCharge";
      this.txtNeededCharge.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtNeededCharge.Size = new System.Drawing.Size(68, 21);
      this.txtNeededCharge.TabIndex = 0;
      this.txtNeededCharge.Text = "$50,000.00";
      this.txtNeededCharge.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtNeededCharge_OnNumberEntered);
      this.txtNeededCharge.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.txtFriendlyKillFrag_OnFocusReceived);
      // 
      // label5
      // 
      this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label5.Location = new System.Drawing.Point(345, 82);
      this.label5.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(28, 15);
      this.label5.TabIndex = 71;
      this.label5.TabStop = false;
      this.label5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label5.Texts")));
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel3.Location = new System.Drawing.Point(35, 212);
      this.transparentLabel3.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(127, 15);
      this.transparentLabel3.TabIndex = 95;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // txtFriendlyKillFrag
      // 
      this.txtFriendlyKillFrag.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtFriendlyKillFrag.Location = new System.Drawing.Point(269, 209);
      this.txtFriendlyKillFrag.Name = "txtFriendlyKillFrag";
      this.txtFriendlyKillFrag.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtFriendlyKillFrag.Size = new System.Drawing.Size(68, 21);
      this.txtFriendlyKillFrag.TabIndex = 5;
      this.txtFriendlyKillFrag.Text = "1";
      this.txtFriendlyKillFrag.Value = 1D;
      this.txtFriendlyKillFrag.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtNeededCharge_OnNumberEntered);
      this.txtFriendlyKillFrag.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.txtFriendlyKillFrag_OnFocusReceived);
      // 
      // transparentLabel4
      // 
      this.transparentLabel4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel4.Location = new System.Drawing.Point(345, 212);
      this.transparentLabel4.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel4.Name = "transparentLabel4";
      this.transparentLabel4.Size = new System.Drawing.Size(24, 15);
      this.transparentLabel4.TabIndex = 96;
      this.transparentLabel4.TabStop = false;
      this.transparentLabel4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel4.Texts")));
      // 
      // pbFocus
      // 
      this.pbFocus.BackColor = System.Drawing.Color.Transparent;
      this.pbFocus.Image = global::SpecialForce.Properties.Resources.Focus_Marker;
      this.pbFocus.Location = new System.Drawing.Point(12, 97);
      this.pbFocus.Name = "pbFocus";
      this.pbFocus.Size = new System.Drawing.Size(16, 16);
      this.pbFocus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pbFocus.TabIndex = 98;
      this.pbFocus.TabStop = false;
      // 
      // txtTeamMembers
      // 
      this.txtTeamMembers.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtTeamMembers.Location = new System.Drawing.Point(269, 261);
      this.txtTeamMembers.Name = "txtTeamMembers";
      this.txtTeamMembers.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtTeamMembers.Size = new System.Drawing.Size(68, 21);
      this.txtTeamMembers.TabIndex = 7;
      this.txtTeamMembers.Text = "4";
      this.txtTeamMembers.Value = 4D;
      this.txtTeamMembers.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtNeededCharge_OnNumberEntered);
      this.txtTeamMembers.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.txtFriendlyKillFrag_OnFocusReceived);
      // 
      // transparentLabel5
      // 
      this.transparentLabel5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel5.Location = new System.Drawing.Point(345, 264);
      this.transparentLabel5.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel5.Name = "transparentLabel5";
      this.transparentLabel5.Size = new System.Drawing.Size(17, 15);
      this.transparentLabel5.TabIndex = 101;
      this.transparentLabel5.TabStop = false;
      this.transparentLabel5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel5.Texts")));
      // 
      // transparentLabel6
      // 
      this.transparentLabel6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel6.Location = new System.Drawing.Point(35, 264);
      this.transparentLabel6.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel6.Name = "transparentLabel6";
      this.transparentLabel6.Size = new System.Drawing.Size(131, 15);
      this.transparentLabel6.TabIndex = 100;
      this.transparentLabel6.TabStop = false;
      this.transparentLabel6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel6.Texts")));
      // 
      // txtMaxTeamNameChars
      // 
      this.txtMaxTeamNameChars.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtMaxTeamNameChars.Location = new System.Drawing.Point(269, 235);
      this.txtMaxTeamNameChars.Name = "txtMaxTeamNameChars";
      this.txtMaxTeamNameChars.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtMaxTeamNameChars.Size = new System.Drawing.Size(68, 21);
      this.txtMaxTeamNameChars.TabIndex = 6;
      this.txtMaxTeamNameChars.Text = "15";
      this.txtMaxTeamNameChars.Value = 15D;
      this.txtMaxTeamNameChars.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtNeededCharge_OnNumberEntered);
      this.txtMaxTeamNameChars.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.txtFriendlyKillFrag_OnFocusReceived);
      // 
      // transparentLabel14
      // 
      this.transparentLabel14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel14.Location = new System.Drawing.Point(345, 238);
      this.transparentLabel14.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel14.Name = "transparentLabel14";
      this.transparentLabel14.Size = new System.Drawing.Size(26, 15);
      this.transparentLabel14.TabIndex = 104;
      this.transparentLabel14.TabStop = false;
      this.transparentLabel14.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel14.Texts")));
      // 
      // transparentLabel15
      // 
      this.transparentLabel15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel15.Location = new System.Drawing.Point(35, 238);
      this.transparentLabel15.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel15.Name = "transparentLabel15";
      this.transparentLabel15.Size = new System.Drawing.Size(130, 15);
      this.transparentLabel15.TabIndex = 103;
      this.transparentLabel15.TabStop = false;
      this.transparentLabel15.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel15.Texts")));
      // 
      // transparentLabel16
      // 
      this.transparentLabel16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel16.Location = new System.Drawing.Point(31, 290);
      this.transparentLabel16.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel16.Name = "transparentLabel16";
      this.transparentLabel16.Size = new System.Drawing.Size(148, 15);
      this.transparentLabel16.TabIndex = 105;
      this.transparentLabel16.TabStop = false;
      this.transparentLabel16.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel16.Texts")));
      // 
      // txtRefundAcception
      // 
      this.txtRefundAcception.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtRefundAcception.Location = new System.Drawing.Point(269, 287);
      this.txtRefundAcception.Name = "txtRefundAcception";
      this.txtRefundAcception.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtRefundAcception.Size = new System.Drawing.Size(68, 21);
      this.txtRefundAcception.TabIndex = 106;
      this.txtRefundAcception.Text = "72";
      this.txtRefundAcception.Value = 72D;
      this.txtRefundAcception.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtNeededCharge_OnNumberEntered);
      this.txtRefundAcception.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.txtFriendlyKillFrag_OnFocusReceived);
      // 
      // transparentLabel17
      // 
      this.transparentLabel17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel17.Location = new System.Drawing.Point(344, 289);
      this.transparentLabel17.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel17.Name = "transparentLabel17";
      this.transparentLabel17.Size = new System.Drawing.Size(35, 15);
      this.transparentLabel17.TabIndex = 107;
      this.transparentLabel17.TabStop = false;
      this.transparentLabel17.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel17.Texts")));
      // 
      // FormOptionsDB
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.transparentLabel17);
      this.Controls.Add(this.txtRefundAcception);
      this.Controls.Add(this.transparentLabel16);
      this.Controls.Add(this.txtMaxTeamNameChars);
      this.Controls.Add(this.transparentLabel14);
      this.Controls.Add(this.transparentLabel15);
      this.Controls.Add(this.txtTeamMembers);
      this.Controls.Add(this.transparentLabel5);
      this.Controls.Add(this.transparentLabel6);
      this.Controls.Add(this.pbFocus);
      this.Controls.Add(this.txtFriendlyKillFrag);
      this.Controls.Add(this.transparentLabel4);
      this.Controls.Add(this.transparentLabel3);
      this.Controls.Add(this.txtKillFrag);
      this.Controls.Add(this.transparentLabel12);
      this.Controls.Add(this.transparentLabel13);
      this.Controls.Add(this.txtDiffuseFrag);
      this.Controls.Add(this.transparentLabel10);
      this.Controls.Add(this.transparentLabel11);
      this.Controls.Add(this.txtPlantFrag);
      this.Controls.Add(this.transparentLabel8);
      this.Controls.Add(this.transparentLabel9);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnSave);
      this.Controls.Add(this.transparentLabel7);
      this.Controls.Add(this.toolBar1);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.txtFreeCharge);
      this.Controls.Add(this.txtNeededCharge);
      this.Controls.Add(this.label5);
      this.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormOptionsDB";
      this.Text = "تنظیمات کلی سیستم";
      this.Load += new System.EventHandler(this.FormOptionsDB_Load);
      this.toolBar1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pbFocus)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private Utilities.NumberBox txtKillFrag;
    private TransparentLabel transparentLabel12;
    private TransparentLabel transparentLabel13;
    private Utilities.NumberBox txtDiffuseFrag;
    private TransparentLabel transparentLabel10;
    private TransparentLabel transparentLabel11;
    private Utilities.NumberBox txtPlantFrag;
    private TransparentLabel transparentLabel8;
    private TransparentLabel transparentLabel9;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnSave;
    private TransparentLabel transparentLabel7;
    private ToolBar toolBar1;
    private TransparentLabel transparentLabel2;
    private TransparentLabel transparentLabel1;
    private TransparentLabel label4;
    private TransparentLabel label1;
    private TransparentLabel label2;
    private Utilities.NumberBox txtFreeCharge;
    private Utilities.NumberBox txtNeededCharge;
    private TransparentLabel label5;
    private TransparentLabel transparentLabel3;
    private Utilities.NumberBox txtFriendlyKillFrag;
    private TransparentLabel transparentLabel4;
    private System.Windows.Forms.PictureBox pbFocus;
    private Utilities.NumberBox txtTeamMembers;
    private TransparentLabel transparentLabel5;
    private TransparentLabel transparentLabel6;
    private Utilities.NumberBox txtMaxTeamNameChars;
    private TransparentLabel transparentLabel14;
    private TransparentLabel transparentLabel15;
    private TransparentLabel transparentLabel16;
    private Utilities.NumberBox txtRefundAcception;
    private TransparentLabel transparentLabel17;
  }
}