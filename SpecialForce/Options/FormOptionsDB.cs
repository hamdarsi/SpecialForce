﻿using System;
using System.Windows.Forms;

namespace SpecialForce.Options
{
  public partial class FormOptionsDB : SessionAwareForm
  {
    public FormOptionsDB()
    {
      InitializeComponent();
    }

    private void txtFriendlyKillFrag_OnFocusReceived(object sender, EventArgs e)
    {
      pbFocus.Top = (sender as Control).Top;
    }

    private void FormOptionsDB_Load(object sender, EventArgs e)
    {
      txtNeededCharge.Value = DB.Options.GiftCreditNecessaryCharge;
      txtFreeCharge.Value = DB.Options.GiftCreditAmount;
      txtPlantFrag.Value = DB.Options.BombPlantFrag;
      txtDiffuseFrag.Value = DB.Options.BombDiffuseFrag;
      txtKillFrag.Value = DB.Options.KillFrag;
      txtFriendlyKillFrag.Value = DB.Options.FriendlyKillFrag;
      txtMaxTeamNameChars.Value = DB.Options.MaximumTeamNameLength;
      txtTeamMembers.Value = DB.Options.TeamMinimumMembers;
      txtRefundAcception.Value = DB.Options.RefundableCancelDuration;

      txtNeededCharge.Enabled = txtFreeCharge.Value > 0;
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      if (txtNeededCharge.Value <= 0)
      {
        Session.ShowError("مقدار شارژ لازم برای شارژ هدیه نمی تواند صفر و یا کوچکتر باشد.");
        return;
      }

      DB.Options.GiftCreditNecessaryCharge = txtNeededCharge.Value;
      DB.Options.GiftCreditAmount = txtFreeCharge.Value;
      DB.Options.BombPlantFrag = txtPlantFrag.IntegerValue;
      DB.Options.BombDiffuseFrag = txtDiffuseFrag.IntegerValue;
      DB.Options.KillFrag = txtKillFrag.IntegerValue;
      DB.Options.FriendlyKillFrag = txtFriendlyKillFrag.IntegerValue;
      DB.Options.MaximumTeamNameLength = txtMaxTeamNameChars.IntegerValue;
      DB.Options.TeamMinimumMembers = txtTeamMembers.IntegerValue;
      DB.Options.RefundableCancelDuration = txtRefundAcception.IntegerValue;
      DB.Options.Apply();

      DialogResult = DialogResult.OK;
      Close();
    }

    private void txtNeededCharge_OnNumberEntered(object sender, EventArgs e)
    {
      SelectNextControl(sender as Control, true, true, false, true);
    }

    private void txtFreeCharge_OnNumberChanged(object sender, EventArgs e)
    {
      txtNeededCharge.Enabled = txtFreeCharge.Value > 0;
    }
  }
}
