﻿namespace SpecialForce.Options
{
  partial class FormOptionsProgram
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOptionsProgram));
      this.toolBar1 = new SpecialForce.ToolBar();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.txtMaxTries = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.transparentLabel4 = new SpecialForce.TransparentLabel();
      this.txtCommandInterval = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel5 = new SpecialForce.TransparentLabel();
      this.transparentLabel6 = new SpecialForce.TransparentLabel();
      this.transparentLabel7 = new SpecialForce.TransparentLabel();
      this.btnSave = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.transparentLabel8 = new SpecialForce.TransparentLabel();
      this.txtMaxLogs = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel9 = new SpecialForce.TransparentLabel();
      this.pbFocus = new System.Windows.Forms.PictureBox();
      this.txtLockTime = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel12 = new SpecialForce.TransparentLabel();
      this.transparentLabel13 = new SpecialForce.TransparentLabel();
      this.txtScheduleCheckInt = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel14 = new SpecialForce.TransparentLabel();
      this.transparentLabel15 = new SpecialForce.TransparentLabel();
      this.txtReturnTime = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel16 = new SpecialForce.TransparentLabel();
      this.transparentLabel17 = new SpecialForce.TransparentLabel();
      this.chkKevlarFullCheck = new SpecialForce.TransparentCheckBox();
      this.toolBar1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbFocus)).BeginInit();
      this.SuspendLayout();
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 1.5F;
      this.toolBar1.Controls.Add(this.transparentLabel2);
      this.toolBar1.Controls.Add(this.transparentLabel1);
      this.toolBar1.CornerRadius = 10;
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(-11, -8);
      this.toolBar1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(471, 82);
      this.toolBar1.TabIndex = 43;
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel2.Location = new System.Drawing.Point(182, 42);
      this.transparentLabel2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(272, 26);
      this.transparentLabel2.TabIndex = 1;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.Location = new System.Drawing.Point(366, 15);
      this.transparentLabel1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(88, 27);
      this.transparentLabel1.TabIndex = 0;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // txtMaxTries
      // 
      this.txtMaxTries.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtMaxTries.Location = new System.Drawing.Point(268, 103);
      this.txtMaxTries.Name = "txtMaxTries";
      this.txtMaxTries.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtMaxTries.Size = new System.Drawing.Size(68, 21);
      this.txtMaxTries.TabIndex = 0;
      this.txtMaxTries.Text = "0";
      this.txtMaxTries.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.OnNumberEntered);
      this.txtMaxTries.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.OnFocusReceived);
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel3.Location = new System.Drawing.Point(344, 105);
      this.transparentLabel3.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(14, 15);
      this.transparentLabel3.TabIndex = 47;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // transparentLabel4
      // 
      this.transparentLabel4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel4.Location = new System.Drawing.Point(55, 105);
      this.transparentLabel4.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel4.Name = "transparentLabel4";
      this.transparentLabel4.Size = new System.Drawing.Size(164, 15);
      this.transparentLabel4.TabIndex = 46;
      this.transparentLabel4.TabStop = false;
      this.transparentLabel4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel4.Texts")));
      // 
      // txtCommandInterval
      // 
      this.txtCommandInterval.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtCommandInterval.Location = new System.Drawing.Point(268, 131);
      this.txtCommandInterval.Mode = SpecialForce.Utilities.NumberBox.NumberMode.Float;
      this.txtCommandInterval.Name = "txtCommandInterval";
      this.txtCommandInterval.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtCommandInterval.Size = new System.Drawing.Size(68, 21);
      this.txtCommandInterval.TabIndex = 1;
      this.txtCommandInterval.Text = "4";
      this.txtCommandInterval.Value = 4D;
      this.txtCommandInterval.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.OnNumberEntered);
      this.txtCommandInterval.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.OnFocusReceived);
      // 
      // transparentLabel5
      // 
      this.transparentLabel5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel5.Location = new System.Drawing.Point(344, 133);
      this.transparentLabel5.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel5.Name = "transparentLabel5";
      this.transparentLabel5.Size = new System.Drawing.Size(24, 15);
      this.transparentLabel5.TabIndex = 50;
      this.transparentLabel5.TabStop = false;
      this.transparentLabel5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel5.Texts")));
      // 
      // transparentLabel6
      // 
      this.transparentLabel6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel6.Location = new System.Drawing.Point(54, 133);
      this.transparentLabel6.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel6.Name = "transparentLabel6";
      this.transparentLabel6.Size = new System.Drawing.Size(129, 15);
      this.transparentLabel6.TabIndex = 49;
      this.transparentLabel6.TabStop = false;
      this.transparentLabel6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel6.Texts")));
      // 
      // transparentLabel7
      // 
      this.transparentLabel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel7.Location = new System.Drawing.Point(-7, 314);
      this.transparentLabel7.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel7.Name = "transparentLabel7";
      this.transparentLabel7.Size = new System.Drawing.Size(451, 15);
      this.transparentLabel7.TabIndex = 56;
      this.transparentLabel7.TabStop = false;
      this.transparentLabel7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel7.Texts")));
      // 
      // btnSave
      // 
      this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnSave.ForeColor = System.Drawing.Color.Black;
      this.btnSave.Location = new System.Drawing.Point(283, 341);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(75, 23);
      this.btnSave.TabIndex = 8;
      this.btnSave.Text = "ثبت";
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(369, 341);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 9;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // transparentLabel8
      // 
      this.transparentLabel8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel8.Location = new System.Drawing.Point(55, 161);
      this.transparentLabel8.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel8.Name = "transparentLabel8";
      this.transparentLabel8.Size = new System.Drawing.Size(130, 15);
      this.transparentLabel8.TabIndex = 59;
      this.transparentLabel8.TabStop = false;
      this.transparentLabel8.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel8.Texts")));
      // 
      // txtMaxLogs
      // 
      this.txtMaxLogs.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtMaxLogs.Location = new System.Drawing.Point(268, 159);
      this.txtMaxLogs.Name = "txtMaxLogs";
      this.txtMaxLogs.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtMaxLogs.Size = new System.Drawing.Size(68, 21);
      this.txtMaxLogs.TabIndex = 2;
      this.txtMaxLogs.Text = "0";
      this.txtMaxLogs.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.OnNumberEntered);
      this.txtMaxLogs.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.OnFocusReceived);
      // 
      // transparentLabel9
      // 
      this.transparentLabel9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel9.Location = new System.Drawing.Point(344, 161);
      this.transparentLabel9.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel9.Name = "transparentLabel9";
      this.transparentLabel9.Size = new System.Drawing.Size(21, 15);
      this.transparentLabel9.TabIndex = 60;
      this.transparentLabel9.TabStop = false;
      this.transparentLabel9.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel9.Texts")));
      // 
      // pbFocus
      // 
      this.pbFocus.BackColor = System.Drawing.Color.Transparent;
      this.pbFocus.Image = global::SpecialForce.Properties.Resources.Focus_Marker;
      this.pbFocus.Location = new System.Drawing.Point(31, 80);
      this.pbFocus.Name = "pbFocus";
      this.pbFocus.Size = new System.Drawing.Size(16, 16);
      this.pbFocus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pbFocus.TabIndex = 61;
      this.pbFocus.TabStop = false;
      // 
      // txtLockTime
      // 
      this.txtLockTime.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtLockTime.Location = new System.Drawing.Point(268, 187);
      this.txtLockTime.Mode = SpecialForce.Utilities.NumberBox.NumberMode.Float;
      this.txtLockTime.Name = "txtLockTime";
      this.txtLockTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtLockTime.Size = new System.Drawing.Size(68, 21);
      this.txtLockTime.TabIndex = 3;
      this.txtLockTime.Text = "0";
      this.txtLockTime.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.OnNumberEntered);
      this.txtLockTime.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.OnFocusReceived);
      // 
      // transparentLabel12
      // 
      this.transparentLabel12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel12.Location = new System.Drawing.Point(344, 189);
      this.transparentLabel12.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel12.Name = "transparentLabel12";
      this.transparentLabel12.Size = new System.Drawing.Size(30, 15);
      this.transparentLabel12.TabIndex = 67;
      this.transparentLabel12.TabStop = false;
      this.transparentLabel12.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel12.Texts")));
      // 
      // transparentLabel13
      // 
      this.transparentLabel13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel13.Location = new System.Drawing.Point(54, 189);
      this.transparentLabel13.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel13.Name = "transparentLabel13";
      this.transparentLabel13.Size = new System.Drawing.Size(182, 15);
      this.transparentLabel13.TabIndex = 66;
      this.transparentLabel13.TabStop = false;
      this.transparentLabel13.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel13.Texts")));
      // 
      // txtScheduleCheckInt
      // 
      this.txtScheduleCheckInt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtScheduleCheckInt.Location = new System.Drawing.Point(268, 215);
      this.txtScheduleCheckInt.Mode = SpecialForce.Utilities.NumberBox.NumberMode.Float;
      this.txtScheduleCheckInt.Name = "txtScheduleCheckInt";
      this.txtScheduleCheckInt.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtScheduleCheckInt.Size = new System.Drawing.Size(68, 21);
      this.txtScheduleCheckInt.TabIndex = 4;
      this.txtScheduleCheckInt.Text = "0";
      this.txtScheduleCheckInt.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.OnNumberEntered);
      this.txtScheduleCheckInt.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.OnFocusReceived);
      // 
      // transparentLabel14
      // 
      this.transparentLabel14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel14.Location = new System.Drawing.Point(344, 217);
      this.transparentLabel14.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel14.Name = "transparentLabel14";
      this.transparentLabel14.Size = new System.Drawing.Size(24, 15);
      this.transparentLabel14.TabIndex = 70;
      this.transparentLabel14.TabStop = false;
      this.transparentLabel14.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel14.Texts")));
      // 
      // transparentLabel15
      // 
      this.transparentLabel15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel15.Location = new System.Drawing.Point(54, 217);
      this.transparentLabel15.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel15.Name = "transparentLabel15";
      this.transparentLabel15.Size = new System.Drawing.Size(126, 15);
      this.transparentLabel15.TabIndex = 69;
      this.transparentLabel15.TabStop = false;
      this.transparentLabel15.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel15.Texts")));
      // 
      // txtReturnTime
      // 
      this.txtReturnTime.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtReturnTime.Location = new System.Drawing.Point(268, 243);
      this.txtReturnTime.Name = "txtReturnTime";
      this.txtReturnTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtReturnTime.Size = new System.Drawing.Size(68, 21);
      this.txtReturnTime.TabIndex = 5;
      this.txtReturnTime.Text = "0";
      this.txtReturnTime.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.OnNumberEntered);
      this.txtReturnTime.OnFocusReceived += new System.EventHandler<System.EventArgs>(this.OnFocusReceived);
      // 
      // transparentLabel16
      // 
      this.transparentLabel16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel16.Location = new System.Drawing.Point(344, 245);
      this.transparentLabel16.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel16.Name = "transparentLabel16";
      this.transparentLabel16.Size = new System.Drawing.Size(24, 15);
      this.transparentLabel16.TabIndex = 73;
      this.transparentLabel16.TabStop = false;
      this.transparentLabel16.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel16.Texts")));
      // 
      // transparentLabel17
      // 
      this.transparentLabel17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel17.Location = new System.Drawing.Point(54, 245);
      this.transparentLabel17.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel17.Name = "transparentLabel17";
      this.transparentLabel17.Size = new System.Drawing.Size(112, 15);
      this.transparentLabel17.TabIndex = 72;
      this.transparentLabel17.TabStop = false;
      this.transparentLabel17.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel17.Texts")));
      // 
      // chkKevlarFullCheck
      // 
      this.chkKevlarFullCheck.Font = new System.Drawing.Font("Tahoma", 8.25F);
      this.chkKevlarFullCheck.ForeColor = System.Drawing.Color.White;
      this.chkKevlarFullCheck.Location = new System.Drawing.Point(55, 269);
      this.chkKevlarFullCheck.Name = "chkKevlarFullCheck";
      this.chkKevlarFullCheck.Size = new System.Drawing.Size(199, 21);
      this.chkKevlarFullCheck.TabIndex = 6;
      this.chkKevlarFullCheck.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkKevlarFullCheck.Texts")));
      // 
      // FormOptionsProgram
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.chkKevlarFullCheck);
      this.Controls.Add(this.txtReturnTime);
      this.Controls.Add(this.transparentLabel16);
      this.Controls.Add(this.transparentLabel17);
      this.Controls.Add(this.txtScheduleCheckInt);
      this.Controls.Add(this.transparentLabel14);
      this.Controls.Add(this.transparentLabel15);
      this.Controls.Add(this.txtLockTime);
      this.Controls.Add(this.transparentLabel12);
      this.Controls.Add(this.transparentLabel13);
      this.Controls.Add(this.pbFocus);
      this.Controls.Add(this.txtMaxLogs);
      this.Controls.Add(this.transparentLabel9);
      this.Controls.Add(this.transparentLabel8);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnSave);
      this.Controls.Add(this.transparentLabel7);
      this.Controls.Add(this.txtCommandInterval);
      this.Controls.Add(this.transparentLabel5);
      this.Controls.Add(this.transparentLabel6);
      this.Controls.Add(this.txtMaxTries);
      this.Controls.Add(this.transparentLabel3);
      this.Controls.Add(this.transparentLabel4);
      this.Controls.Add(this.toolBar1);
      this.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.Name = "FormOptionsProgram";
      this.Text = "تنظیمات برنامه";
      this.Load += new System.EventHandler(this.FormOptions_Load);
      this.toolBar1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pbFocus)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private ToolBar toolBar1;
    private TransparentLabel transparentLabel1;
    private TransparentLabel transparentLabel2;
    private Utilities.NumberBox txtMaxTries;
    private TransparentLabel transparentLabel3;
    private TransparentLabel transparentLabel4;
    private Utilities.NumberBox txtCommandInterval;
    private TransparentLabel transparentLabel5;
    private TransparentLabel transparentLabel6;
    private TransparentLabel transparentLabel7;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.Button btnCancel;
    private TransparentLabel transparentLabel8;
    private Utilities.NumberBox txtMaxLogs;
    private TransparentLabel transparentLabel9;
    private System.Windows.Forms.PictureBox pbFocus;
    private Utilities.NumberBox txtLockTime;
    private TransparentLabel transparentLabel12;
    private TransparentLabel transparentLabel13;
    private Utilities.NumberBox txtScheduleCheckInt;
    private TransparentLabel transparentLabel14;
    private TransparentLabel transparentLabel15;
    private Utilities.NumberBox txtReturnTime;
    private TransparentLabel transparentLabel16;
    private TransparentLabel transparentLabel17;
    private TransparentCheckBox chkKevlarFullCheck;
  }
}