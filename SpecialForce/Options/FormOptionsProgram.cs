﻿using System;
using System.Windows.Forms;

namespace SpecialForce.Options
{
  public partial class FormOptionsProgram : SessionAwareForm
  {
    static private FormOptionsProgram mInstance = null;
    static public FormOptionsProgram Instance
    {
      get
      {
        if (mInstance == null || mInstance.IsDisposed)
          mInstance = new FormOptionsProgram();

        return mInstance;
      }
    }

    public FormOptionsProgram()
    {
      InitializeComponent();
    }

    private void FormOptions_Load(object sender, EventArgs e)
    {
      txtCommandInterval.Value = Properties.Settings.Default.CommandSendInterval;
      txtMaxTries.Value = Properties.Settings.Default.CommandMaxTries;
      txtMaxLogs.Value = Properties.Settings.Default.MaximumLogFiles;
      txtLockTime.Value = Properties.Settings.Default.IdleMinutesToLock;
      txtScheduleCheckInt.Value = Properties.Settings.Default.ScheduleCheckInterval;
      txtReturnTime.Value = Properties.Settings.Default.TimeToReturnToBase;
      chkKevlarFullCheck.Checked = Properties.Settings.Default.KevlarFullCheck;
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      Properties.Settings.Default.CommandSendInterval = txtCommandInterval.Value;
      Properties.Settings.Default.CommandMaxTries = txtMaxTries.IntegerValue;
      Properties.Settings.Default.MaximumLogFiles = txtMaxLogs.IntegerValue;
      Properties.Settings.Default.IdleMinutesToLock = txtLockTime.Value;
      Properties.Settings.Default.ScheduleCheckInterval = txtScheduleCheckInt.IntegerValue;
      Properties.Settings.Default.TimeToReturnToBase = txtReturnTime.IntegerValue;
      Properties.Settings.Default.KevlarFullCheck = chkKevlarFullCheck.Checked;

      Properties.Settings.Default.Save();
      DialogResult = DialogResult.OK;
      Close();
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void OnNumberEntered(object sender, EventArgs e)
    {
      SelectNextControl(sender as Control, true, true, false, true);
    }

    private void OnFocusReceived(object sender, EventArgs e)
    {
      pbFocus.Top = (sender as Control).Top;
    }

    private void OnKeyPressed(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
        SelectNextControl(sender as Control, true, true, false, true);
    }
  }
}
