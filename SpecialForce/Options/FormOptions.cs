﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace SpecialForce.Options
{
  public partial class FormOptions : SessionAwareForm
  {
    static private FormOptions mInstance = null;
    static public FormOptions Instance
    {
      get
      {
        if (mInstance == null || mInstance.IsDisposed)
          mInstance = new FormOptions();

        return mInstance;
      }
    }

    private FormOptions()
    {
      InitializeComponent();
    }

    protected override void ApplyPermissions(Post post)
    {
      base.ApplyPermissions(post);

      btnSettingsProgram.Enabled = post.IsPrivileged();
      btnSettingsDatabase.Enabled = post.IsGod();
      btnSettingsMessages.Enabled = post.IsPrivileged();
    }

    private void btnFinish_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void btnSettingsHardware_Click(object sender, EventArgs e)
    {
      ShowModal(new FormOptionsDevices(), false);
    }

    private void btnSettingsProgram_Click(object sender, EventArgs e)
    {
      ShowModal(new FormOptionsProgram(), false);
    }

    private void btnSettingsDatabase_Click(object sender, EventArgs e)
    {
      ShowModal(new FormOptionsDB(), false);
    }

    private void btnSettingsMessages_Click(object sender, EventArgs e)
    {
      ShowModal(new FormOptionsMessages(), false);
    }
  }
}
