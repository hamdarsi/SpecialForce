﻿namespace SpecialForce.Options
{
  partial class FormOptionsDevices
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOptionsDevices));
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnSave = new System.Windows.Forms.Button();
      this.transparentLabel7 = new SpecialForce.TransparentLabel();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.btnTestCenter = new SpecialForce.ToolButton();
      this.btnTestBlue = new SpecialForce.ToolButton();
      this.btnTestGreen = new SpecialForce.ToolButton();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.cmbWirelessGreenBase = new System.Windows.Forms.ComboBox();
      this.transparentLabel4 = new SpecialForce.TransparentLabel();
      this.cmbWirelessBlueBase = new System.Windows.Forms.ComboBox();
      this.transparentLabel5 = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.lblWireless2 = new SpecialForce.TransparentLabel();
      this.lblWireless1 = new SpecialForce.TransparentLabel();
      this.cmbFingerPrint = new System.Windows.Forms.ComboBox();
      this.cmbWirelessCenter = new System.Windows.Forms.ComboBox();
      this.toolBar1.SuspendLayout();
      this.SuspendLayout();
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(369, 341);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 58;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnSave
      // 
      this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnSave.ForeColor = System.Drawing.Color.Black;
      this.btnSave.Location = new System.Drawing.Point(283, 341);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(75, 23);
      this.btnSave.TabIndex = 57;
      this.btnSave.Text = "ثبت";
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      // 
      // transparentLabel7
      // 
      this.transparentLabel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel7.Location = new System.Drawing.Point(-7, 314);
      this.transparentLabel7.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel7.Name = "transparentLabel7";
      this.transparentLabel7.Size = new System.Drawing.Size(451, 15);
      this.transparentLabel7.TabIndex = 60;
      this.transparentLabel7.TabStop = false;
      this.transparentLabel7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel7.Texts")));
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 1.5F;
      this.toolBar1.Controls.Add(this.transparentLabel2);
      this.toolBar1.Controls.Add(this.transparentLabel1);
      this.toolBar1.CornerRadius = 10;
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(-11, -8);
      this.toolBar1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(471, 82);
      this.toolBar1.TabIndex = 59;
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel2.Location = new System.Drawing.Point(182, 42);
      this.transparentLabel2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(272, 26);
      this.transparentLabel2.TabIndex = 1;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.Location = new System.Drawing.Point(366, 15);
      this.transparentLabel1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(88, 27);
      this.transparentLabel1.TabIndex = 0;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // btnTestCenter
      // 
      this.btnTestCenter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnTestCenter.Hint = "تست ماژول وسط زمین";
      this.btnTestCenter.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnTestCenter.Image")));
      this.btnTestCenter.Location = new System.Drawing.Point(372, 196);
      this.btnTestCenter.Name = "btnTestCenter";
      this.btnTestCenter.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnTestCenter.ShowBorder = false;
      this.btnTestCenter.Size = new System.Drawing.Size(16, 16);
      this.btnTestCenter.TabIndex = 73;
      this.btnTestCenter.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnTestCenter.Texts")));
      this.btnTestCenter.TransparentColor = System.Drawing.Color.Transparent;
      this.btnTestCenter.Click += new System.EventHandler(this.btnTestClick);
      // 
      // btnTestBlue
      // 
      this.btnTestBlue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnTestBlue.Hint = "تست ماژول پایگاه تیم آبی";
      this.btnTestBlue.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnTestBlue.Image")));
      this.btnTestBlue.Location = new System.Drawing.Point(372, 223);
      this.btnTestBlue.Name = "btnTestBlue";
      this.btnTestBlue.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnTestBlue.ShowBorder = false;
      this.btnTestBlue.Size = new System.Drawing.Size(16, 16);
      this.btnTestBlue.TabIndex = 72;
      this.btnTestBlue.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnTestBlue.Texts")));
      this.btnTestBlue.TransparentColor = System.Drawing.Color.Transparent;
      this.btnTestBlue.Click += new System.EventHandler(this.btnTestClick);
      // 
      // btnTestGreen
      // 
      this.btnTestGreen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnTestGreen.Hint = "تست ماژول پایگاه تیم سبز";
      this.btnTestGreen.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnTestGreen.Image")));
      this.btnTestGreen.Location = new System.Drawing.Point(372, 250);
      this.btnTestGreen.Name = "btnTestGreen";
      this.btnTestGreen.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnTestGreen.ShowBorder = false;
      this.btnTestGreen.Size = new System.Drawing.Size(16, 16);
      this.btnTestGreen.TabIndex = 71;
      this.btnTestGreen.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnTestGreen.Texts")));
      this.btnTestGreen.TransparentColor = System.Drawing.Color.Transparent;
      this.btnTestGreen.Click += new System.EventHandler(this.btnTestClick);
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.AutoSize = true;
      this.transparentLabel3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel3.Location = new System.Drawing.Point(49, 245);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(103, 15);
      this.transparentLabel3.TabIndex = 70;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // cmbWirelessGreenBase
      // 
      this.cmbWirelessGreenBase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbWirelessGreenBase.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.cmbWirelessGreenBase.FormattingEnabled = true;
      this.cmbWirelessGreenBase.Location = new System.Drawing.Point(284, 248);
      this.cmbWirelessGreenBase.Name = "cmbWirelessGreenBase";
      this.cmbWirelessGreenBase.Size = new System.Drawing.Size(82, 21);
      this.cmbWirelessGreenBase.TabIndex = 69;
      // 
      // transparentLabel4
      // 
      this.transparentLabel4.AutoSize = true;
      this.transparentLabel4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel4.Location = new System.Drawing.Point(49, 218);
      this.transparentLabel4.Name = "transparentLabel4";
      this.transparentLabel4.Size = new System.Drawing.Size(100, 15);
      this.transparentLabel4.TabIndex = 68;
      this.transparentLabel4.TabStop = false;
      this.transparentLabel4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel4.Texts")));
      // 
      // cmbWirelessBlueBase
      // 
      this.cmbWirelessBlueBase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbWirelessBlueBase.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.cmbWirelessBlueBase.FormattingEnabled = true;
      this.cmbWirelessBlueBase.Location = new System.Drawing.Point(284, 221);
      this.cmbWirelessBlueBase.Name = "cmbWirelessBlueBase";
      this.cmbWirelessBlueBase.Size = new System.Drawing.Size(82, 21);
      this.cmbWirelessBlueBase.TabIndex = 67;
      // 
      // transparentLabel5
      // 
      this.transparentLabel5.AutoSize = true;
      this.transparentLabel5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel5.Location = new System.Drawing.Point(52, 191);
      this.transparentLabel5.Name = "transparentLabel5";
      this.transparentLabel5.Size = new System.Drawing.Size(86, 15);
      this.transparentLabel5.TabIndex = 66;
      this.transparentLabel5.TabStop = false;
      this.transparentLabel5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel5.Texts")));
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label3.Location = new System.Drawing.Point(49, 100);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(158, 15);
      this.label3.TabIndex = 65;
      this.label3.TabStop = false;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // lblWireless2
      // 
      this.lblWireless2.AutoSize = true;
      this.lblWireless2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblWireless2.Location = new System.Drawing.Point(46, 164);
      this.lblWireless2.Name = "lblWireless2";
      this.lblWireless2.Size = new System.Drawing.Size(224, 15);
      this.lblWireless2.TabIndex = 64;
      this.lblWireless2.TabStop = false;
      this.lblWireless2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblWireless2.Texts")));
      // 
      // lblWireless1
      // 
      this.lblWireless1.AutoSize = true;
      this.lblWireless1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblWireless1.Location = new System.Drawing.Point(49, 149);
      this.lblWireless1.Name = "lblWireless1";
      this.lblWireless1.Size = new System.Drawing.Size(189, 15);
      this.lblWireless1.TabIndex = 63;
      this.lblWireless1.TabStop = false;
      this.lblWireless1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblWireless1.Texts")));
      // 
      // cmbFingerPrint
      // 
      this.cmbFingerPrint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbFingerPrint.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.cmbFingerPrint.FormattingEnabled = true;
      this.cmbFingerPrint.Items.AddRange(new object[] {
            "بله",
            "خیر"});
      this.cmbFingerPrint.Location = new System.Drawing.Point(283, 101);
      this.cmbFingerPrint.Name = "cmbFingerPrint";
      this.cmbFingerPrint.Size = new System.Drawing.Size(61, 21);
      this.cmbFingerPrint.TabIndex = 61;
      // 
      // cmbWirelessCenter
      // 
      this.cmbWirelessCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbWirelessCenter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.cmbWirelessCenter.FormattingEnabled = true;
      this.cmbWirelessCenter.Location = new System.Drawing.Point(284, 194);
      this.cmbWirelessCenter.Name = "cmbWirelessCenter";
      this.cmbWirelessCenter.Size = new System.Drawing.Size(82, 21);
      this.cmbWirelessCenter.TabIndex = 62;
      // 
      // FormOptionsDevices
      // 
      this.AcceptButton = this.btnSave;
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.btnTestCenter);
      this.Controls.Add(this.btnTestBlue);
      this.Controls.Add(this.btnTestGreen);
      this.Controls.Add(this.transparentLabel3);
      this.Controls.Add(this.cmbWirelessGreenBase);
      this.Controls.Add(this.transparentLabel4);
      this.Controls.Add(this.cmbWirelessBlueBase);
      this.Controls.Add(this.transparentLabel5);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.lblWireless2);
      this.Controls.Add(this.lblWireless1);
      this.Controls.Add(this.cmbFingerPrint);
      this.Controls.Add(this.cmbWirelessCenter);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnSave);
      this.Controls.Add(this.transparentLabel7);
      this.Controls.Add(this.toolBar1);
      this.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormOptionsDevices";
      this.Text = "تنظیمات سخت افزاری";
      this.Load += new System.EventHandler(this.FormOptionsDevices_Load);
      this.toolBar1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnSave;
    private TransparentLabel transparentLabel7;
    private ToolBar toolBar1;
    private TransparentLabel transparentLabel2;
    private TransparentLabel transparentLabel1;
    private ToolButton btnTestCenter;
    private ToolButton btnTestBlue;
    private ToolButton btnTestGreen;
    private TransparentLabel transparentLabel3;
    private System.Windows.Forms.ComboBox cmbWirelessGreenBase;
    private TransparentLabel transparentLabel4;
    private System.Windows.Forms.ComboBox cmbWirelessBlueBase;
    private TransparentLabel transparentLabel5;
    private TransparentLabel label3;
    private TransparentLabel lblWireless2;
    private TransparentLabel lblWireless1;
    private System.Windows.Forms.ComboBox cmbFingerPrint;
    private System.Windows.Forms.ComboBox cmbWirelessCenter;
  }
}