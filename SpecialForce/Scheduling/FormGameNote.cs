﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Scheduling
{
  public partial class FormGameNote : SessionAwareForm
  {
    private Color mDeletedBrush = Color.FromArgb(255, 85, 85);
    private Color mExpiredBrush = Color.FromArgb(195, 195, 195);
    private Color mNormalBrush = Color.FromArgb(255, 255, 255);

    private DB.Game mGame = null;
    public DB.Game Game
    {
      get { return mGame; }
      set 
      {
        if (mGame != null)
          mGame.OnAttributeUpdate -= GameUpdated;

        mGame = value;

        if(mGame != null)
          mGame.OnAttributeUpdate += GameUpdated;
      }
    }

    private DB.ScheduleNote mCurrentNote = null;

    public FormGameNote()
    {
      InitializeComponent();
      lvNotes.SetAspect(0, item => { return (item as DB.ScheduleNote).TimeStamp.ToString(DateTimeString.CompactDateTime); });
      lvNotes.SetAspect(1, item => { return DB.User.GetUserFullName((item as DB.ScheduleNote).UserID); });
    }

    protected override void OnFormClosing(FormClosingEventArgs e)
    {
      if (mGame != null)
        mGame.OnAttributeUpdate -= GameUpdated;
    }

    private void Reload()
    {
      lvNotes.Items.Clear();
      lvNotes.SetObjects(mGame.Notes);
      dtDue.Enabled = chkDue.Checked;
    }

    private void FormGameNote_Load(object sender, EventArgs e)
    {
      Reload();

      string str = "بازی " + Enums.ToString(mGame.Type) + " (";
      str += Enums.ToString(mGame.State);
      str += ") - ";
      str += "شروع: " + mGame.Start.ToString(DateTimeString.CompactDateTime);

      if (mGame.BlueTeamID != -1 && mGame.GreenTeamID != -1)
      {
        str += " بین: ";
        str += DB.Team.GetTeamName(mGame.BlueTeamID);
        str += " و ";
        str += DB.Team.GetTeamName(mGame.GreenTeamID);
      }
      lblGameInfo1.Text = str;

      if (mGame.CompetitionID != -1)
      {
        DB.Competition c = DB.CompetitionManager.GetCompetition(mGame.CompetitionID);

        str = "مسابقات ";
        str += c.Title;

        DB.CandidateMatch cm = mGame.CandidateMatch;
        if (c.Type != CompetitionType.League && cm != null)
        {
          str += "، ";
          if (cm.IsFinalMatch)
            str += "مسابقه فینال";
          else if (cm.IsLosersFinal)
            str += "مسابقه رده بندی";
          else if (c.Type == CompetitionType.Cup)
          {
            if (cm.Stage >= 1)
              str += "دور حذفی مرحله " + cm.Stage;
            else
              str += "دور گروهی در گروه " + (cm.Group + 1);
          }
          else if (c.Type == CompetitionType.Elimination)
            str += "مرحله " + (cm.Stage + 1);
        }
        lblGameInfo2.Text = str;
      }
    }

    private void btnNewNote_OnClick(object sender, EventArgs e)
    {
      mCurrentNote = null;
      txtNote.Text = "";
      chkDue.Checked = false;
      pnlDue.Enabled = true;
      pnlNote.Enabled = true;
      dtDue.Enabled = chkDue.Checked;
    }

    private void btnApplyNote_OnClick(object sender, EventArgs e)
    {
      mGame.AddNote(txtNote.Text, chkDue.Checked ? dtDue.Date : null);
      btnNewNote_OnClick(null, null);
    }

    private void chkDue_CheckedChanged(object sender, EventArgs e)
    {
      dtDue.Enabled = chkDue.Checked;
    }

    private void btnDeleteNote_OnClick(object sender, EventArgs e)
    {
      if (mCurrentNote != null)
        if (mCurrentNote.Deleted)
          Session.ShowError("یادداشت پاک شده است!");
        else if(Session.Ask("آیا میخواهید یادداشت را پاک کنید؟", "پیام"))
          mGame.DeleteNote(mCurrentNote.ID);
    }

    private void GameUpdated(object sender, EventArgs e)
    {
      Reload();
    }

    private void txtNote_TextChanged(object sender, EventArgs e)
    {
      btnApplyNote.Enabled = txtNote.Text.Length != 0;
    }

    private void lvNotes_SelectedIndexChanged(object sender, EventArgs e)
    {
      mCurrentNote = lvNotes.SelectedObject as DB.ScheduleNote;
      if(mCurrentNote == null)
        return;

      txtNote.Text = mCurrentNote.Note;
      chkDue.Checked = mCurrentNote.Due != null;
      dtDue.Date = mCurrentNote.Due != null ? mCurrentNote.Due : PersianDateTime.Today;
      pnlNote.Enabled = false;
      pnlDue.Enabled = false;
    }

    private void lvNotes_FormatRow(object sender, BrightIdeasSoftware.FormatRowEventArgs e)
    {
      DB.ScheduleNote note = e.Item.RowObject as DB.ScheduleNote;

      if (note.Deleted)
        e.Item.BackColor = mDeletedBrush;
      else if (note.Expired)
        e.Item.BackColor = mExpiredBrush;
      else
        e.Item.BackColor = mNormalBrush;
    }
  }
}
