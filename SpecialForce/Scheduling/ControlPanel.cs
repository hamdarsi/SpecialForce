﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace SpecialForce.Scheduling
{
  public partial class ControlPanel : SessionAwareForm
  {
    private DB.Game mGame = null;
    private PersianDateTime mCopiedTimeStamp = null;
    private PersianDateTime mMenuTimeStamp = null;

    static private ControlPanel mInstance = null;
    static public ControlPanel Instance
    {
      get 
      {
        if (mInstance == null || mInstance.IsDisposed)
          mInstance = new ControlPanel();

        return mInstance;
      }
    }

    private ControlPanel()
    {
      InitializeComponent();
    }

    protected override void ApplyPermissions(Post post)
    {
      btnBatchPrice.Enabled = post.IsPrivileged();
      btnAddGame.Enabled = post.IsPrivileged();
      pnlTime.Enabled = post.IsPrivileged();
      cmbSite.Enabled = post.IsGod();
    }

    private void ControlPanel_Load(object sender, EventArgs e)
    {
      btnAddGame.Hint += Environment.NewLine;
      btnAddGame.Hint += "برای ایجاد بازی جدید این دکمه را بکشید و در تاریخ و زمان مورد نظر بیندازید";

      int index = -1;
      List<DB.Site> sites = DB.Site.GetAllSites(false);
      for (int i = 0; i < sites.Count; ++i)
      {
        cmbSite.Items.Add(sites[i]);
        if (sites[i].ID == Session.ActiveSiteID)
          index = i;
      }

      cmbSite.SelectedIndex = index;
      dtStart.Date = PersianDateTime.Today;
      txtStartHour.Value = Properties.Settings.Default.ScheduleStartHour;
      txtEndHour.Value = Properties.Settings.Default.ScheduleEndHour;

      mGame = null;
      CurrentGameChanged();
    }

    private void btnAddGame_OnDragStart(object sender, DragObjectRequestArgs e)
    {
      e.Object = new ScheduleEntry();
    }

    private void dtStart_OnDateChanged(object sender, EventArgs e)
    {
      scheduleTable1.Date = dtStart.Date;
      lblDayInWeek.Text = dtStart.Date.ToString(DateTimeString.DayOfWeek);
    }

    private void cmbSite_SelectedIndexChanged(object sender, EventArgs e)
    {
      scheduleTable1.SiteID = (cmbSite.SelectedItem as DB.Site).ID;
    }

    private void txtStartHour_ValueChanged(object sender, EventArgs e)
    {
      scheduleTable1.StartHour = (int)txtStartHour.Value;
    }

    private void txtEndHour_ValueChanged(object sender, EventArgs e)
    {
      scheduleTable1.EndHour = (int)txtEndHour.Value;
    }

    private void ControlPanel_FormClosing(object sender, FormClosingEventArgs e)
    {
      Properties.Settings.Default.ScheduleStartHour = txtStartHour.Value;
      Properties.Settings.Default.ScheduleEndHour = txtEndHour.Value;
      Properties.Settings.Default.Save();
    }

    private void scheduleTable1_OnUpdated(object sender, ScheduleTableUpdatedArgs e)
    {
      dtStart.Date = e.Date;
    }

    private void chkDummies_Load(object sender, EventArgs e)
    {
      scheduleTable1.IncludeDummies = chkGameDummies.Checked;
    }

    private void scheduleTable1_OnEntryRightClicked(object sender, EventArgs e)
    {
      bool is_admin = Session.CurrentStaff.IsPrivileged();

      ScheduleEntry entry = sender as ScheduleEntry;
      mGame = entry != null ? entry.Game : null;
      CurrentGameChanged();

      mnuDeleteGame.Available = is_admin && mGame != null && (mGame.State == GameState.Free || (mGame.State == GameState.Reserved && mGame.CompetitionID != -1));
      mnuReserve.Available = mGame != null && mGame.State == GameState.Free;
      mnuCancel.Available = mGame != null && (mGame.Type == CompetitionType.FreeMatch || mGame.Type == CompetitionType.Dummy) && mGame.State == GameState.Reserved;
      mnuResumeRun.Available = mGame != null && mGame.State == GameState.Aborted && mGame.Finish > new PersianDateTime(PersianDateTime.Now.Value.Add(new TimeSpan(-1, 0, 0)));
      mnuReport.Available = true;
      mnuViewGame.Available = mGame != null && (mGame.State == GameState.InProgress || mGame.State == GameState.Finished);
      mnuSetBatchPrice.Available = is_admin;
      mnuGameNotes.Available = mGame != null;
      mnuAllNotes.Available = true;

      mMenuTimeStamp = scheduleTable1.GetCursorTimeStamp();
      mnuCopy.Available = is_admin && mMenuTimeStamp != null && mGame == null;
      mnuPaste.Available = is_admin && mCopiedTimeStamp != null && mGame == null;
      mnuSep.Available = mnuCopy.Available || mnuPaste.Available;

      mnuEntries.Show(Cursor.Position, ToolStripDropDownDirection.BelowLeft);
    }

    private void mnuDeleteGame_Click(object sender, EventArgs e)
    {
      DB.GameManager.DeleteGame(mGame.ID);
    }

    private void mnuReserve_Click(object sender, EventArgs e)
    {
      FormGameReserve frm = new FormGameReserve();
      frm.Game = mGame;
      ShowModal(frm, false);
    }

    private void mnuCancel_Click(object sender, EventArgs e)
    {
      double diff = mGame.Start.GetDateTimeDifference(PersianDateTime.Now);
      double acc_diff = DB.Options.RefundableCancelDuration * 3600;
      bool refund = diff > acc_diff;
      string str = "آیا واقعا میخواهید بازی را کنسل کنید؟";
      str += Environment.NewLine;
      if (!refund)
      {
        str += "بازی پس از مدت مجاز، کنسل می شود.";
        str += Environment.NewLine;
        str += "ورودیه سانس عودت داده نخواهد شد.";
      }
      else
      {
        str += "بازی در مدت مجاز کنسل می شود.";
        str += Environment.NewLine;
        str += "مبلغ " + mGame.AdmissionFee.ToCurrencyString() + " تومان باید به این بازیکنان عودت داده شود:";
        str += Environment.NewLine;

        foreach (DB.User player in mGame.BlueUsers)
        {
          str += Environment.NewLine;
          str += player.FullName;
        }

        foreach (DB.User player in mGame.GreenUsers)
        {
          str += Environment.NewLine;
          str += player.FullName;
        }
      }

      if(Session.Ask(str))
        mGame.CancelRegularMatch(refund);
    }

    private void mnuResumeRun_Click(object sender, EventArgs e)
    {
      if (Session.Ask("آیا مطمئن هستید؟"))
      {
        mGame.State = GameState.Reserved;
        mGame.Apply();
      }
    }

    private void mnuReport_Click(object sender, EventArgs e)
    {
      btnGameReport_Click(sender, e);
    }

    private void txtAdmission_OnNumberEntered(object sender, EventArgs e)
    {
      if (txtAdmission.Value > 0)
        mGame.AdmissionFee = txtAdmission.Value;
      else
      {
        Session.ShowError("مبلغ ورودیه صحیح نیست.");
        txtAdmission.Value = mGame.AdmissionFee;
      }

      txtAdmission.Focus();
      txtAdmission.SelectAll();
    }

    private void CurrentGameChanged()
    {
      pnlGame.Enabled = mGame != null && Session.CurrentStaff.IsPrivileged();
      txtAdmission.Value = mGame == null ? 0 : mGame.AdmissionFee;
      txtAdmission.Enabled = pnlGame.Enabled && mGame.State == GameState.Free;
      lblGameDateStart.Text = mGame == null ? "-" : mGame.Start.ToString(DateTimeString.CompactDate);
      lblGameDateEnd.Text = mGame == null ? "-" : mGame.Finish.ToString(DateTimeString.CompactDate);

      dtGameTimeFinish.ActivateEvent = dtGameTimeStart.ActivateEvent = false;
      if (mGame != null)
      {
        dtGameTimeStart.Time = mGame.Start;
        dtGameTimeFinish.Time = mGame.Finish;
        dtGameTimeStart.Enabled = mGame.State == GameState.Reserved || mGame.State == GameState.Free;
        dtGameTimeFinish.Enabled = mGame.State == GameState.Reserved || mGame.State == GameState.Free;
      }
      else
      {
        dtGameTimeStart.Enabled = false;
        dtGameTimeFinish.Enabled = false;
      }
      dtGameTimeFinish.ActivateEvent = dtGameTimeStart.ActivateEvent = true;
    }

    private void scheduleTable1_OnEntryClicked(object sender, MouseEventArgs e)
    {
      mGame = sender is ScheduleEntry ? (sender as ScheduleEntry).Game : null;
      CurrentGameChanged();
    }

    private void mnuGameNotes_Click(object sender, EventArgs e)
    {
      FormGameNote frm = new FormGameNote();
      frm.Game = mGame;
      ShowModal(frm, false);
    }

    private void chkGameDummies_CheckedChanged(object sender, EventArgs e)
    {
      scheduleTable1.IncludeDummies = chkGameDummies.Checked;
    }

    private void chkGameRegulars_CheckedChanged(object sender, EventArgs e)
    {
      scheduleTable1.IncludeRegulars = chkGameRegulars.Checked;
    }

    private void chkGameLeagues_CheckedChanged(object sender, EventArgs e)
    {
      scheduleTable1.IncludeLeagues = chkGameLeagues.Checked;
    }

    private void chkGameCups_CheckedChanged(object sender, EventArgs e)
    {
      scheduleTable1.IncludeCups = chkGameCups.Checked;
    }

    private void chkGameEliminations_CheckedChanged(object sender, EventArgs e)
    {
      scheduleTable1.IncludeElims = chkGameEliminations.Checked;
    }

    private void btnGameReport_OnClick(object sender, EventArgs e)
    {
      ShowModal(new FormGameReportSettings(), false);
    }

    private void btnNoteReports_OnClick(object sender, EventArgs e)
    {
      ShowModal(new FormGameNoteReportSettings(), false);
    }

    private void btnLockSystem_Click(object sender, EventArgs e)
    {
      Session.Lock();
    }

    private void btnGameReport_Click(object sender, EventArgs e)
    {
      ShowModal(new FormGameReportSettings(), false);
    }

    private void mnuViewGame_Click(object sender, EventArgs e)
    {
      List<DB.Game> games = new List<DB.Game>();
      games.Add(mGame);
      FormGameReport frm = new FormGameReport();
      frm.Games = games;
      frm.ShortState = mGame.State == GameState.InProgress;
      ShowModal(frm, false);
    }

    private void btnBatchPrice_Click(object sender, EventArgs e)
    {
      FormBatchPrice frm = new FormBatchPrice();
      List<DB.Site> sites = DB.Site.GetAllSites(false);

      if (Session.CurrentStaff.IsGod())
        for (int i = 0; i < sites.Count; ++i)
        {
          frm.cmbSite.Items.Add(sites[i]);
          if (sites[i].ID == scheduleTable1.SiteID)
            frm.cmbSite.SelectedIndex = i;
        }
      else
      {
        frm.cmbSite.Items.Add(cmbSite.SelectedItem);
        frm.cmbSite.SelectedIndex = 0;
      }

      frm.dtStart.Date = scheduleTable1.Date;
      frm.dtEnd.Date = new PersianDateTime(scheduleTable1.Date).TraverseDaysForward(scheduleTable1.RowsShown);
      frm.tmStart.Time = PersianDateTime.Today.SetTime(scheduleTable1.StartHour, 0, 0);

      if (scheduleTable1.EndHour == 24)
        frm.tmEnd.Time = PersianDateTime.Today.SetTime(23, 59, 59);
      else
        frm.tmEnd.Time = PersianDateTime.Today.SetTime(scheduleTable1.EndHour, 0, 0);
      ShowModal(frm, false);
    }

    private void mnuCopy_Click(object sender, EventArgs e)
    {
      mCopiedTimeStamp = new PersianDateTime(mMenuTimeStamp).SetTime(0, 0, 0);
    }

    private void mnuPaste_Click(object sender, EventArgs e)
    {
      if (mCopiedTimeStamp == null)
        return;

      PersianDateTime copy_date = new PersianDateTime(mCopiedTimeStamp);
      PersianDateTime paste_date = new PersianDateTime(mMenuTimeStamp);
      List<DB.Game> games = DB.GameManager.GetGames(copy_date, copy_date.NextDay, scheduleTable1.SiteID);
      for(int i = 0; i < games.Count; ++i)
      {
        PersianDateTime st = new PersianDateTime(paste_date);
        st.SetTime(games[i].Start.Hour, games[i].Start.Minute, games[i].Start.Second);
        PersianDateTime fn = new PersianDateTime(paste_date);
        fn.SetTime(games[i].Finish.Hour, games[i].Finish.Minute, games[i].Finish.Second);
        scheduleTable1.AddEmptyEntry(st, fn);
      }
    }

    private void dtGameTime_OnTimeChanged(object sender, EventArgs e)
    {
      PersianDateTime dt_start = mGame.Start.SetTime(dtGameTimeStart.Time);
      PersianDateTime dt_finish = mGame.Finish.SetTime(dtGameTimeFinish.Time);

      bool game_exists = DB.GameManager.GameExistsInTimeLine(dt_start, dt_finish, (cmbSite.SelectedItem as DB.Site).ID, mGame.ID);
      bool valid_time = dt_start < dt_finish;
      if (game_exists || !valid_time)
      {
        Session.ShowError("نمی توان زمان مورد نظر را برای بازی تعیین کرد.");
        CurrentGameChanged();
      }
      else
      {
        mGame.Start = dt_start;
        mGame.Finish = dt_finish;
        mGame.Apply();
        scheduleTable1.adjustEntries();
      }
    }
  }
}
