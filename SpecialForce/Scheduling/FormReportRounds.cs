﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Scheduling
{
  public partial class FormReportRounds : SessionAwareForm
  {
    private DB.Game mGame = null;
    public DB.Game Game
    {
      get { return mGame; }
      set { mGame = value; }
    }

    private List<DB.Round> mRounds = new List<DB.Round>();
    public List<DB.Round> Rounds
    {
      get { return mRounds; }
      set { mRounds = value; }
    }

    public FormReportRounds()
    {
      InitializeComponent();
    }

    private void FormReportRounds_Shown(object sender, EventArgs e)
    {
      report1.BeginUpdate();

      Reports.Table table = report1.AddTable();
      table.AddNewColumn(Types.Integer, "شماره", 50);
      table.AddNewColumn(Types.DateTime, "شروع", 100).DateTimeMode = DateTimeString.CompactDateTime;
      table.AddNewColumn(Types.DateTime, "پایان", 100).DateTimeMode = DateTimeString.CompactDateTime;
      table.AddNewColumn(Types.String, "تروریست ها", 90);
      table.AddNewColumn(Types.String, "ضد تروریست ها", 90);
      table.AddNewColumn(Types.String, " وضعیت راند", 120);
      table.AddNewColumn(Types.String, "وضعیت بمب", 120);
      table.OnRowDoubleClicked += RowDoubleClicked;

      Statistics<Stats.Round> stats = Stats.Generator.Generate(mRounds);
      foreach (Stats.Round st in stats.Data)
      {
        Reports.Row row = table.AddNewRow(st.Instance);
        row[0] = st.Number;
        row[1] = st.Start;
        row[2] = st.End;
        row[3] = st.Terrorists;
        row[4] = st.CounterTerrorists;
        row[5] = st.RoundState;
        row[6] = st.BombState;
      }

      table = report1.AddTable(false, 150, 300);
      if(mGame != null)
      {
        table.AddKeyValuePair("نوع بازی", Enums.ToString(mGame.Type));
        
        if(mGame.CompetitionID != -1)
          table.AddKeyValuePair("مسابقه", mGame.Competition.Title);

        table.AddKeyValuePair("نتیجه بازی", mGame.StateString);
      }
      else
        table.AddKeyValuePair("نتیجه بازی", "-");

      stats.PumpMetaData(table);
      report1.EndUpdate();
    }

    private void RowDoubleClicked(object sender, ReportRowArgs e)
    {
      FormRoundDetailReport frm = new FormRoundDetailReport();
      frm.Round = e.Data as DB.Round;
      ShowModal(frm, false);
    }
  }
}
