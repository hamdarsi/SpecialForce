﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Scheduling
{
  public partial class FormRoundDetailReport : SessionAwareForm
  {
    private DB.Round mRound = null;
    public DB.Round Round
    {
      get { return mRound; }
      set { mRound = value; }
    }

    public FormRoundDetailReport()
    {
      InitializeComponent();
    }

    private void FormRoundDetailReport_Shown(object sender, EventArgs e)
    {
      report1.BeginUpdate();

      Reports.Table table = report1.AddTable();
      table.AddNewColumn(Types.String, "نام بازیکن", 120);
      table.AddNewColumn(Types.String, "شماره لباس", 70);
      table.AddNewColumn(Types.String, "تیم", 80);
      table.AddNewColumn(Types.String, "وضعیت", 220);
      table.AddNewColumn(Types.Integer, "تعداد کشتن", 70);
      table.AddNewColumn(Types.Integer, "تعداد کشتن خودی", 90);

      Statistics<Stats.Player> stats = Stats.Generator.Generate(mRound);
      foreach (Stats.Player st in stats.Data)
      {
        Reports.Row row = table.AddNewRow(st.Instance);
        row[0] = st.FullName;
        row[1] = st.KevlarNumber;
        row[2] = st.TeamName;
        row[3] = st.State;
        row[4] = st.KillCount;
        row[5] = st.FriendlyKillCount;
      }

      // also add bomb state to the report
      string strBombLine1 = "بمب در راند استفاده نشد.";
      string strBombLine2 = "";
      if (mRound.BombState != BombState.Enabled && mRound.BombState != BombState.Disabled)
      {
        try
        {
          string num = (mRound.PlantedBomb + 1).ToString();
          strBombLine1 = "بمب شماره " + num + " توسط " + mRound.Planter.FullName + " در زمان " + DB.Round.TimeToString(mRound.PlantTime) + " گذاشته شد.";
        }
        catch
        {
          strBombLine1 = "بمب کاشته شد ولی در اطلاعات مشکل وجود دارد.";
        }

        try
        {
          if (mRound.BombState == BombState.Exploded)
            strBombLine2 = "بمب خنثی نشد و منفجر شد.";
          else if (mRound.BombState == BombState.Diffused)
            strBombLine2 = "بمب توسط " + mRound.Diffuser.FullName + " در زمان " + DB.Round.TimeToString(mRound.DiffuseTime) + " خنثی شد.";
          else
            strBombLine2 = "راند قبل از ترکیدن بمب تمام شد.";
        }
        catch
        {
          strBombLine2 = "";
        }
      }

      // fill in extra information
      table = report1.AddTable(false, 120, 600);
      table.AddKeyValuePair("وضعیت راند", mRound.StateString);
      table.AddKeyValuePair("شماره راند", mRound.Number.ToString());
      table.AddKeyValuePair("وضعیت بمب", strBombLine1);
      if (strBombLine2.Length > 0)
        table.AddKeyValuePair("", strBombLine2);
      stats.PumpMetaData(table);

      // Remove information of best survivor, best planter and best diffuser
      while (table.Rows.Count > 5)
        table.Rows.RemoveAt(5);

      report1.EndUpdate();
    }
  }
}
