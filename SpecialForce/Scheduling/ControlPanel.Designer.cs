﻿namespace SpecialForce.Scheduling
{
  partial class ControlPanel
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPanel));
      this.tinypicker1 = new SpecialForce.TinyPicker();
      this.scheduleTable1 = new SpecialForce.Scheduling.ScheduleTable();
      this.tbButtons = new SpecialForce.ToolBar();
      this.btnNoteReports = new SpecialForce.ToolButton();
      this.btnBatchPrice = new SpecialForce.ToolButton();
      this.btnLockSystem = new SpecialForce.ToolButton();
      this.btnGameReport = new SpecialForce.ToolButton();
      this.btnAddGame = new SpecialForce.ToolButton();
      this.pnlDateSite = new SpecialForce.ToolBar();
      this.lblDayInWeek = new SpecialForce.TransparentLabel();
      this.dtStart = new SpecialForce.PersianDatePicker();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.cmbSite = new System.Windows.Forms.ComboBox();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.pnlInclusions = new SpecialForce.ToolBar();
      this.chkGameDummies = new SpecialForce.TransparentCheckBox();
      this.chkGameEliminations = new SpecialForce.TransparentCheckBox();
      this.chkGameCups = new SpecialForce.TransparentCheckBox();
      this.chkGameLeagues = new SpecialForce.TransparentCheckBox();
      this.chkGameRegulars = new SpecialForce.TransparentCheckBox();
      this.transparentLabel4 = new SpecialForce.TransparentLabel();
      this.pnlTime = new SpecialForce.ToolBar();
      this.txtEndHour = new System.Windows.Forms.NumericUpDown();
      this.txtStartHour = new System.Windows.Forms.NumericUpDown();
      this.transparentLabel5 = new SpecialForce.TransparentLabel();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.mnuEntries = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mnuDeleteGame = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuReserve = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuCancel = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuResumeRun = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuViewGame = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuGameNotes = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuSetBatchPrice = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuReport = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuAllNotes = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuSep = new System.Windows.Forms.ToolStripSeparator();
      this.mnuCopy = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuPaste = new System.Windows.Forms.ToolStripMenuItem();
      this.txtAdmission = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel6 = new SpecialForce.TransparentLabel();
      this.pnlGame = new SpecialForce.ToolBar();
      this.dtGameTimeFinish = new SpecialForce.PersianTimePicker();
      this.dtGameTimeStart = new SpecialForce.PersianTimePicker();
      this.transparentLabel11 = new SpecialForce.TransparentLabel();
      this.transparentLabel7 = new SpecialForce.TransparentLabel();
      this.transparentLabel8 = new SpecialForce.TransparentLabel();
      this.lblGameDateEnd = new SpecialForce.TransparentLabel();
      this.lblGameDateStart = new SpecialForce.TransparentLabel();
      this.tbButtons.SuspendLayout();
      this.pnlDateSite.SuspendLayout();
      this.pnlInclusions.SuspendLayout();
      this.pnlTime.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtEndHour)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtStartHour)).BeginInit();
      this.mnuEntries.SuspendLayout();
      this.pnlGame.SuspendLayout();
      this.SuspendLayout();
      // 
      // tinypicker1
      // 
      this.tinypicker1.Location = new System.Drawing.Point(12, 557);
      this.tinypicker1.Name = "tinypicker1";
      this.tinypicker1.Size = new System.Drawing.Size(791, 30);
      this.tinypicker1.TabIndex = 4;
      this.tinypicker1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tinypicker1.Texts")));
      // 
      // scheduleTable1
      // 
      this.scheduleTable1.AllowDrop = true;
      this.scheduleTable1.Location = new System.Drawing.Point(107, 12);
      this.scheduleTable1.Name = "scheduleTable1";
      this.scheduleTable1.Size = new System.Drawing.Size(696, 350);
      this.scheduleTable1.TabIndex = 5;
      this.scheduleTable1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("scheduleTable1.Texts")));
      this.scheduleTable1.OnUpdated += new System.EventHandler<SpecialForce.ScheduleTableUpdatedArgs>(this.scheduleTable1_OnUpdated);
      this.scheduleTable1.OnEntryClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.scheduleTable1_OnEntryClicked);
      this.scheduleTable1.OnEntryRightClicked += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.scheduleTable1_OnEntryRightClicked);
      // 
      // tbButtons
      // 
      this.tbButtons.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbButtons.BorderWidth = 0.5F;
      this.tbButtons.Controls.Add(this.btnNoteReports);
      this.tbButtons.Controls.Add(this.btnBatchPrice);
      this.tbButtons.Controls.Add(this.btnLockSystem);
      this.tbButtons.Controls.Add(this.btnGameReport);
      this.tbButtons.Controls.Add(this.btnAddGame);
      this.tbButtons.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbButtons.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbButtons.Location = new System.Drawing.Point(14, 12);
      this.tbButtons.Name = "tbButtons";
      this.tbButtons.Size = new System.Drawing.Size(86, 496);
      this.tbButtons.TabIndex = 41;
      // 
      // btnNoteReports
      // 
      this.btnNoteReports.Hint = "گزارش تمام یادداشت ها";
      this.btnNoteReports.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnNoteReports.Image")));
      this.btnNoteReports.Location = new System.Drawing.Point(11, 151);
      this.btnNoteReports.Name = "btnNoteReports";
      this.btnNoteReports.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnNoteReports.Size = new System.Drawing.Size(64, 64);
      this.btnNoteReports.TabIndex = 16;
      this.btnNoteReports.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnNoteReports.Texts")));
      this.btnNoteReports.TransparentColor = System.Drawing.Color.White;
      this.btnNoteReports.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.btnNoteReports_OnClick);
      // 
      // btnBatchPrice
      // 
      this.btnBatchPrice.Hint = "تنظیم نرخ دسته جمعی بازی ها";
      this.btnBatchPrice.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnBatchPrice.Image")));
      this.btnBatchPrice.Location = new System.Drawing.Point(11, 81);
      this.btnBatchPrice.Name = "btnBatchPrice";
      this.btnBatchPrice.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnBatchPrice.Size = new System.Drawing.Size(64, 64);
      this.btnBatchPrice.TabIndex = 15;
      this.btnBatchPrice.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnBatchPrice.Texts")));
      this.btnBatchPrice.TransparentColor = System.Drawing.Color.White;
      this.btnBatchPrice.Click += new System.EventHandler(this.btnBatchPrice_Click);
      // 
      // btnLockSystem
      // 
      this.btnLockSystem.Hint = "قفل سیستم";
      this.btnLockSystem.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnLockSystem.Image")));
      this.btnLockSystem.Location = new System.Drawing.Point(11, 420);
      this.btnLockSystem.Name = "btnLockSystem";
      this.btnLockSystem.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnLockSystem.Size = new System.Drawing.Size(64, 64);
      this.btnLockSystem.TabIndex = 14;
      this.btnLockSystem.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnLockSystem.Texts")));
      this.btnLockSystem.TransparentColor = System.Drawing.Color.White;
      this.btnLockSystem.Click += new System.EventHandler(this.btnLockSystem_Click);
      // 
      // btnGameReport
      // 
      this.btnGameReport.Hint = "گزارش گیری از بازی ها";
      this.btnGameReport.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnGameReport.Image")));
      this.btnGameReport.Location = new System.Drawing.Point(11, 221);
      this.btnGameReport.Name = "btnGameReport";
      this.btnGameReport.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnGameReport.Size = new System.Drawing.Size(64, 64);
      this.btnGameReport.TabIndex = 13;
      this.btnGameReport.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnGameReport.Texts")));
      this.btnGameReport.TransparentColor = System.Drawing.Color.White;
      this.btnGameReport.Click += new System.EventHandler(this.btnGameReport_Click);
      // 
      // btnAddGame
      // 
      this.btnAddGame.Hint = "ایجاد بازی";
      this.btnAddGame.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnAddGame.Image")));
      this.btnAddGame.Location = new System.Drawing.Point(11, 11);
      this.btnAddGame.Name = "btnAddGame";
      this.btnAddGame.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnAddGame.Size = new System.Drawing.Size(64, 64);
      this.btnAddGame.TabIndex = 10;
      this.btnAddGame.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnAddGame.Texts")));
      this.btnAddGame.TransparentColor = System.Drawing.Color.White;
      this.btnAddGame.OnDragStart += new System.EventHandler<SpecialForce.DragObjectRequestArgs>(this.btnAddGame_OnDragStart);
      // 
      // pnlDateSite
      // 
      this.pnlDateSite.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlDateSite.BorderWidth = 0.5F;
      this.pnlDateSite.Controls.Add(this.lblDayInWeek);
      this.pnlDateSite.Controls.Add(this.dtStart);
      this.pnlDateSite.Controls.Add(this.transparentLabel2);
      this.pnlDateSite.Controls.Add(this.cmbSite);
      this.pnlDateSite.Controls.Add(this.transparentLabel1);
      this.pnlDateSite.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlDateSite.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlDateSite.Location = new System.Drawing.Point(108, 368);
      this.pnlDateSite.Name = "pnlDateSite";
      this.pnlDateSite.Size = new System.Drawing.Size(227, 183);
      this.pnlDateSite.TabIndex = 42;
      // 
      // lblDayInWeek
      // 
      this.lblDayInWeek.Location = new System.Drawing.Point(158, 149);
      this.lblDayInWeek.Name = "lblDayInWeek";
      this.lblDayInWeek.Size = new System.Drawing.Size(29, 15);
      this.lblDayInWeek.TabIndex = 4;
      this.lblDayInWeek.TabStop = false;
      this.lblDayInWeek.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblDayInWeek.Texts")));
      // 
      // dtStart
      // 
      this.dtStart.ActivateEvent = true;
      this.dtStart.Location = new System.Drawing.Point(15, 131);
      this.dtStart.Name = "dtStart";
      this.dtStart.Size = new System.Drawing.Size(132, 37);
      this.dtStart.TabIndex = 3;
      this.dtStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtStart.Texts")));
      this.dtStart.OnDateChanged += new System.EventHandler<System.EventArgs>(this.dtStart_OnDateChanged);
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Location = new System.Drawing.Point(122, 101);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(95, 15);
      this.transparentLabel2.TabIndex = 2;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // cmbSite
      // 
      this.cmbSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbSite.FormattingEnabled = true;
      this.cmbSite.Location = new System.Drawing.Point(15, 50);
      this.cmbSite.Name = "cmbSite";
      this.cmbSite.Size = new System.Drawing.Size(103, 21);
      this.cmbSite.TabIndex = 1;
      this.cmbSite.SelectedIndexChanged += new System.EventHandler(this.cmbSite_SelectedIndexChanged);
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Location = new System.Drawing.Point(30, 12);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(195, 15);
      this.transparentLabel1.TabIndex = 0;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // pnlInclusions
      // 
      this.pnlInclusions.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlInclusions.BorderWidth = 0.5F;
      this.pnlInclusions.Controls.Add(this.chkGameDummies);
      this.pnlInclusions.Controls.Add(this.chkGameEliminations);
      this.pnlInclusions.Controls.Add(this.chkGameCups);
      this.pnlInclusions.Controls.Add(this.chkGameLeagues);
      this.pnlInclusions.Controls.Add(this.chkGameRegulars);
      this.pnlInclusions.Controls.Add(this.transparentLabel4);
      this.pnlInclusions.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlInclusions.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlInclusions.Location = new System.Drawing.Point(339, 368);
      this.pnlInclusions.Name = "pnlInclusions";
      this.pnlInclusions.Size = new System.Drawing.Size(178, 183);
      this.pnlInclusions.TabIndex = 43;
      // 
      // chkGameDummies
      // 
      this.chkGameDummies.Checked = true;
      this.chkGameDummies.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkGameDummies.ForeColor = System.Drawing.Color.White;
      this.chkGameDummies.Location = new System.Drawing.Point(20, 44);
      this.chkGameDummies.Name = "chkGameDummies";
      this.chkGameDummies.Size = new System.Drawing.Size(121, 21);
      this.chkGameDummies.TabIndex = 5;
      this.chkGameDummies.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkGameDummies.Texts")));
      this.chkGameDummies.CheckedChanged += new System.EventHandler(this.chkGameDummies_CheckedChanged);
      this.chkGameDummies.Load += new System.EventHandler(this.chkDummies_Load);
      // 
      // chkGameEliminations
      // 
      this.chkGameEliminations.Checked = true;
      this.chkGameEliminations.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkGameEliminations.ForeColor = System.Drawing.Color.White;
      this.chkGameEliminations.Location = new System.Drawing.Point(20, 148);
      this.chkGameEliminations.Name = "chkGameEliminations";
      this.chkGameEliminations.Size = new System.Drawing.Size(121, 21);
      this.chkGameEliminations.TabIndex = 4;
      this.chkGameEliminations.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkGameEliminations.Texts")));
      this.chkGameEliminations.CheckedChanged += new System.EventHandler(this.chkGameEliminations_CheckedChanged);
      // 
      // chkGameCups
      // 
      this.chkGameCups.Checked = true;
      this.chkGameCups.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkGameCups.ForeColor = System.Drawing.Color.White;
      this.chkGameCups.Location = new System.Drawing.Point(20, 122);
      this.chkGameCups.Name = "chkGameCups";
      this.chkGameCups.Size = new System.Drawing.Size(121, 21);
      this.chkGameCups.TabIndex = 3;
      this.chkGameCups.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkGameCups.Texts")));
      this.chkGameCups.CheckedChanged += new System.EventHandler(this.chkGameCups_CheckedChanged);
      // 
      // chkGameLeagues
      // 
      this.chkGameLeagues.Checked = true;
      this.chkGameLeagues.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkGameLeagues.ForeColor = System.Drawing.Color.White;
      this.chkGameLeagues.Location = new System.Drawing.Point(20, 96);
      this.chkGameLeagues.Name = "chkGameLeagues";
      this.chkGameLeagues.Size = new System.Drawing.Size(121, 21);
      this.chkGameLeagues.TabIndex = 2;
      this.chkGameLeagues.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkGameLeagues.Texts")));
      this.chkGameLeagues.CheckedChanged += new System.EventHandler(this.chkGameLeagues_CheckedChanged);
      // 
      // chkGameRegulars
      // 
      this.chkGameRegulars.Checked = true;
      this.chkGameRegulars.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkGameRegulars.ForeColor = System.Drawing.Color.White;
      this.chkGameRegulars.Location = new System.Drawing.Point(20, 70);
      this.chkGameRegulars.Name = "chkGameRegulars";
      this.chkGameRegulars.Size = new System.Drawing.Size(121, 21);
      this.chkGameRegulars.TabIndex = 1;
      this.chkGameRegulars.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkGameRegulars.Texts")));
      this.chkGameRegulars.CheckedChanged += new System.EventHandler(this.chkGameRegulars_CheckedChanged);
      // 
      // transparentLabel4
      // 
      this.transparentLabel4.Location = new System.Drawing.Point(21, 11);
      this.transparentLabel4.Name = "transparentLabel4";
      this.transparentLabel4.Size = new System.Drawing.Size(157, 15);
      this.transparentLabel4.TabIndex = 0;
      this.transparentLabel4.TabStop = false;
      this.transparentLabel4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel4.Texts")));
      // 
      // pnlTime
      // 
      this.pnlTime.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlTime.BorderWidth = 0.5F;
      this.pnlTime.Controls.Add(this.txtEndHour);
      this.pnlTime.Controls.Add(this.txtStartHour);
      this.pnlTime.Controls.Add(this.transparentLabel5);
      this.pnlTime.Controls.Add(this.transparentLabel3);
      this.pnlTime.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlTime.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlTime.Location = new System.Drawing.Point(523, 368);
      this.pnlTime.Name = "pnlTime";
      this.pnlTime.Size = new System.Drawing.Size(133, 183);
      this.pnlTime.TabIndex = 44;
      // 
      // txtEndHour
      // 
      this.txtEndHour.Location = new System.Drawing.Point(14, 142);
      this.txtEndHour.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
      this.txtEndHour.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
      this.txtEndHour.Name = "txtEndHour";
      this.txtEndHour.Size = new System.Drawing.Size(41, 21);
      this.txtEndHour.TabIndex = 3;
      this.txtEndHour.Value = new decimal(new int[] {
            21,
            0,
            0,
            0});
      this.txtEndHour.ValueChanged += new System.EventHandler(this.txtEndHour_ValueChanged);
      // 
      // txtStartHour
      // 
      this.txtStartHour.Location = new System.Drawing.Point(14, 38);
      this.txtStartHour.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
      this.txtStartHour.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
      this.txtStartHour.Name = "txtStartHour";
      this.txtStartHour.Size = new System.Drawing.Size(41, 21);
      this.txtStartHour.TabIndex = 2;
      this.txtStartHour.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
      this.txtStartHour.ValueChanged += new System.EventHandler(this.txtStartHour_ValueChanged);
      // 
      // transparentLabel5
      // 
      this.transparentLabel5.Location = new System.Drawing.Point(61, 113);
      this.transparentLabel5.Name = "transparentLabel5";
      this.transparentLabel5.Size = new System.Drawing.Size(64, 15);
      this.transparentLabel5.TabIndex = 1;
      this.transparentLabel5.TabStop = false;
      this.transparentLabel5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel5.Texts")));
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.Location = new System.Drawing.Point(55, 9);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(70, 15);
      this.transparentLabel3.TabIndex = 0;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // mnuEntries
      // 
      this.mnuEntries.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDeleteGame,
            this.mnuReserve,
            this.mnuCancel,
            this.mnuResumeRun,
            this.mnuViewGame,
            this.mnuGameNotes,
            this.mnuSetBatchPrice,
            this.mnuReport,
            this.mnuAllNotes,
            this.mnuSep,
            this.mnuCopy,
            this.mnuPaste});
      this.mnuEntries.Name = "mnuEntries";
      this.mnuEntries.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      this.mnuEntries.Size = new System.Drawing.Size(200, 252);
      // 
      // mnuDeleteGame
      // 
      this.mnuDeleteGame.Name = "mnuDeleteGame";
      this.mnuDeleteGame.Size = new System.Drawing.Size(199, 22);
      this.mnuDeleteGame.Text = "حذف بازی";
      this.mnuDeleteGame.Click += new System.EventHandler(this.mnuDeleteGame_Click);
      // 
      // mnuReserve
      // 
      this.mnuReserve.Name = "mnuReserve";
      this.mnuReserve.Size = new System.Drawing.Size(199, 22);
      this.mnuReserve.Text = "رزرو";
      this.mnuReserve.Click += new System.EventHandler(this.mnuReserve_Click);
      // 
      // mnuCancel
      // 
      this.mnuCancel.Name = "mnuCancel";
      this.mnuCancel.Size = new System.Drawing.Size(199, 22);
      this.mnuCancel.Text = "کنسل";
      this.mnuCancel.Click += new System.EventHandler(this.mnuCancel_Click);
      // 
      // mnuResumeRun
      // 
      this.mnuResumeRun.Name = "mnuResumeRun";
      this.mnuResumeRun.Size = new System.Drawing.Size(199, 22);
      this.mnuResumeRun.Text = "ادامه اجرا";
      this.mnuResumeRun.Click += new System.EventHandler(this.mnuResumeRun_Click);
      // 
      // mnuViewGame
      // 
      this.mnuViewGame.Name = "mnuViewGame";
      this.mnuViewGame.Size = new System.Drawing.Size(199, 22);
      this.mnuViewGame.Text = "مشاهده نتیجه بازی";
      this.mnuViewGame.Click += new System.EventHandler(this.mnuViewGame_Click);
      // 
      // mnuGameNotes
      // 
      this.mnuGameNotes.Name = "mnuGameNotes";
      this.mnuGameNotes.Size = new System.Drawing.Size(199, 22);
      this.mnuGameNotes.Text = "یادداشت های این بازی";
      this.mnuGameNotes.Click += new System.EventHandler(this.mnuGameNotes_Click);
      // 
      // mnuSetBatchPrice
      // 
      this.mnuSetBatchPrice.Name = "mnuSetBatchPrice";
      this.mnuSetBatchPrice.Size = new System.Drawing.Size(199, 22);
      this.mnuSetBatchPrice.Text = "تنظیم نرخ کلی";
      this.mnuSetBatchPrice.Click += new System.EventHandler(this.btnBatchPrice_Click);
      // 
      // mnuReport
      // 
      this.mnuReport.Name = "mnuReport";
      this.mnuReport.Size = new System.Drawing.Size(199, 22);
      this.mnuReport.Text = "گزارش گیری از بازی ها";
      this.mnuReport.Click += new System.EventHandler(this.mnuReport_Click);
      // 
      // mnuAllNotes
      // 
      this.mnuAllNotes.Name = "mnuAllNotes";
      this.mnuAllNotes.Size = new System.Drawing.Size(199, 22);
      this.mnuAllNotes.Text = "گزارش گیری از یادداشت ها";
      this.mnuAllNotes.Click += new System.EventHandler(this.btnNoteReports_OnClick);
      // 
      // mnuSep
      // 
      this.mnuSep.Name = "mnuSep";
      this.mnuSep.Size = new System.Drawing.Size(196, 6);
      // 
      // mnuCopy
      // 
      this.mnuCopy.Name = "mnuCopy";
      this.mnuCopy.Size = new System.Drawing.Size(199, 22);
      this.mnuCopy.Text = "کپی روز انتخاب شده";
      this.mnuCopy.Click += new System.EventHandler(this.mnuCopy_Click);
      // 
      // mnuPaste
      // 
      this.mnuPaste.Name = "mnuPaste";
      this.mnuPaste.Size = new System.Drawing.Size(199, 22);
      this.mnuPaste.Text = "پیست در این روز";
      this.mnuPaste.Click += new System.EventHandler(this.mnuPaste_Click);
      // 
      // txtAdmission
      // 
      this.txtAdmission.Enabled = false;
      this.txtAdmission.Location = new System.Drawing.Point(11, 149);
      this.txtAdmission.Mode = SpecialForce.Utilities.NumberBox.NumberMode.Currency;
      this.txtAdmission.Name = "txtAdmission";
      this.txtAdmission.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtAdmission.Size = new System.Drawing.Size(57, 21);
      this.txtAdmission.TabIndex = 1;
      this.txtAdmission.Text = "$0.00";
      this.txtAdmission.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtAdmission_OnNumberEntered);
      // 
      // transparentLabel6
      // 
      this.transparentLabel6.Location = new System.Drawing.Point(78, 153);
      this.transparentLabel6.Name = "transparentLabel6";
      this.transparentLabel6.Size = new System.Drawing.Size(55, 15);
      this.transparentLabel6.TabIndex = 0;
      this.transparentLabel6.TabStop = false;
      this.transparentLabel6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel6.Texts")));
      // 
      // pnlGame
      // 
      this.pnlGame.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlGame.BorderWidth = 0.5F;
      this.pnlGame.Controls.Add(this.dtGameTimeFinish);
      this.pnlGame.Controls.Add(this.dtGameTimeStart);
      this.pnlGame.Controls.Add(this.transparentLabel11);
      this.pnlGame.Controls.Add(this.transparentLabel7);
      this.pnlGame.Controls.Add(this.transparentLabel8);
      this.pnlGame.Controls.Add(this.lblGameDateEnd);
      this.pnlGame.Controls.Add(this.lblGameDateStart);
      this.pnlGame.Controls.Add(this.txtAdmission);
      this.pnlGame.Controls.Add(this.transparentLabel6);
      this.pnlGame.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlGame.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlGame.Location = new System.Drawing.Point(662, 368);
      this.pnlGame.Name = "pnlGame";
      this.pnlGame.Size = new System.Drawing.Size(141, 183);
      this.pnlGame.TabIndex = 45;
      // 
      // dtGameTimeFinish
      // 
      this.dtGameTimeFinish.ActivateEvent = true;
      this.dtGameTimeFinish.Location = new System.Drawing.Point(11, 100);
      this.dtGameTimeFinish.Name = "dtGameTimeFinish";
      this.dtGameTimeFinish.ShowSecond = false;
      this.dtGameTimeFinish.Size = new System.Drawing.Size(79, 37);
      this.dtGameTimeFinish.TabIndex = 8;
      this.dtGameTimeFinish.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtGameTimeFinish.Texts")));
      this.dtGameTimeFinish.OnTimeChanged += new System.EventHandler<System.EventArgs>(this.dtGameTime_OnTimeChanged);
      // 
      // dtGameTimeStart
      // 
      this.dtGameTimeStart.ActivateEvent = true;
      this.dtGameTimeStart.Location = new System.Drawing.Point(11, 38);
      this.dtGameTimeStart.Name = "dtGameTimeStart";
      this.dtGameTimeStart.ShowSecond = false;
      this.dtGameTimeStart.Size = new System.Drawing.Size(79, 37);
      this.dtGameTimeStart.TabIndex = 7;
      this.dtGameTimeStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtGameTimeStart.Texts")));
      this.dtGameTimeStart.OnTimeChanged += new System.EventHandler<System.EventArgs>(this.dtGameTime_OnTimeChanged);
      // 
      // transparentLabel11
      // 
      this.transparentLabel11.Location = new System.Drawing.Point(110, 85);
      this.transparentLabel11.Name = "transparentLabel11";
      this.transparentLabel11.Size = new System.Drawing.Size(28, 15);
      this.transparentLabel11.TabIndex = 4;
      this.transparentLabel11.TabStop = false;
      this.transparentLabel11.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel11.Texts")));
      // 
      // transparentLabel7
      // 
      this.transparentLabel7.Location = new System.Drawing.Point(103, 23);
      this.transparentLabel7.Name = "transparentLabel7";
      this.transparentLabel7.Size = new System.Drawing.Size(35, 15);
      this.transparentLabel7.TabIndex = 2;
      this.transparentLabel7.TabStop = false;
      this.transparentLabel7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel7.Texts")));
      // 
      // transparentLabel8
      // 
      this.transparentLabel8.Location = new System.Drawing.Point(32, 1);
      this.transparentLabel8.Name = "transparentLabel8";
      this.transparentLabel8.Size = new System.Drawing.Size(78, 15);
      this.transparentLabel8.TabIndex = 0;
      this.transparentLabel8.TabStop = false;
      this.transparentLabel8.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel8.Texts")));
      // 
      // lblGameDateEnd
      // 
      this.lblGameDateEnd.Location = new System.Drawing.Point(11, 85);
      this.lblGameDateEnd.Name = "lblGameDateEnd";
      this.lblGameDateEnd.Size = new System.Drawing.Size(62, 15);
      this.lblGameDateEnd.TabIndex = 5;
      this.lblGameDateEnd.TabStop = false;
      this.lblGameDateEnd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblGameDateEnd.Texts")));
      // 
      // lblGameDateStart
      // 
      this.lblGameDateStart.Location = new System.Drawing.Point(8, 23);
      this.lblGameDateStart.Name = "lblGameDateStart";
      this.lblGameDateStart.Size = new System.Drawing.Size(62, 15);
      this.lblGameDateStart.TabIndex = 3;
      this.lblGameDateStart.TabStop = false;
      this.lblGameDateStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblGameDateStart.Texts")));
      // 
      // ControlPanel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Black;
      this.ClientSize = new System.Drawing.Size(815, 599);
      this.Controls.Add(this.pnlGame);
      this.Controls.Add(this.pnlDateSite);
      this.Controls.Add(this.pnlTime);
      this.Controls.Add(this.pnlInclusions);
      this.Controls.Add(this.tbButtons);
      this.Controls.Add(this.tinypicker1);
      this.Controls.Add(this.scheduleTable1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "ControlPanel";
      this.Text = "سانس ها";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ControlPanel_FormClosing);
      this.Load += new System.EventHandler(this.ControlPanel_Load);
      this.tbButtons.ResumeLayout(false);
      this.pnlDateSite.ResumeLayout(false);
      this.pnlInclusions.ResumeLayout(false);
      this.pnlTime.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.txtEndHour)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtStartHour)).EndInit();
      this.mnuEntries.ResumeLayout(false);
      this.pnlGame.ResumeLayout(false);
      this.pnlGame.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private TinyPicker tinypicker1;
    private ScheduleTable scheduleTable1;
    private ToolBar tbButtons;
    private ToolButton btnLockSystem;
    private ToolButton btnGameReport;
    private ToolButton btnAddGame;
    private ToolBar pnlDateSite;
    private PersianDatePicker dtStart;
    private TransparentLabel transparentLabel2;
    private System.Windows.Forms.ComboBox cmbSite;
    private TransparentLabel transparentLabel1;
    private ToolBar pnlInclusions;
    private TransparentCheckBox chkGameEliminations;
    private TransparentCheckBox chkGameCups;
    private TransparentCheckBox chkGameLeagues;
    private TransparentCheckBox chkGameRegulars;
    private TransparentLabel transparentLabel4;
    private TransparentLabel lblDayInWeek;
    private ToolBar pnlTime;
    private System.Windows.Forms.NumericUpDown txtEndHour;
    private System.Windows.Forms.NumericUpDown txtStartHour;
    private TransparentLabel transparentLabel5;
    private TransparentLabel transparentLabel3;
    private TransparentCheckBox chkGameDummies;
    private System.Windows.Forms.ContextMenuStrip mnuEntries;
    private System.Windows.Forms.ToolStripMenuItem mnuDeleteGame;
    private System.Windows.Forms.ToolStripMenuItem mnuReserve;
    private System.Windows.Forms.ToolStripMenuItem mnuCancel;
    private System.Windows.Forms.ToolStripMenuItem mnuReport;
    private System.Windows.Forms.ToolStripMenuItem mnuViewGame;
    private System.Windows.Forms.ToolStripMenuItem mnuSetBatchPrice;
    private Utilities.NumberBox txtAdmission;
    private TransparentLabel transparentLabel6;
    private ToolBar pnlGame;
    private TransparentLabel lblGameDateEnd;
    private TransparentLabel transparentLabel11;
    private TransparentLabel lblGameDateStart;
    private TransparentLabel transparentLabel7;
    private TransparentLabel transparentLabel8;
    private ToolButton btnBatchPrice;
    private ToolButton btnNoteReports;
    private System.Windows.Forms.ToolStripMenuItem mnuGameNotes;
    private System.Windows.Forms.ToolStripMenuItem mnuAllNotes;
    private System.Windows.Forms.ToolStripSeparator mnuSep;
    private System.Windows.Forms.ToolStripMenuItem mnuCopy;
    private System.Windows.Forms.ToolStripMenuItem mnuPaste;
    private System.Windows.Forms.ToolStripMenuItem mnuResumeRun;
    private PersianTimePicker dtGameTimeStart;
    private PersianTimePicker dtGameTimeFinish;

  }
}