﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SpecialForce.Scheduling
{
  [DefaultEvent("OnUpdated")]
  public partial class ScheduleTable : ControlBase
  {
    #region Data
    private bool mLastResizeAcceptable = false;
    private Point mInvalidPoint = new Point(-1, -1);
    private List<ScheduleEntry> mEntries = new List<ScheduleEntry>();
    #endregion Data


    #region Instances
    private ScheduleEntry mDragEntry = null;
    private ToolBar mArena = new ToolBar();
    #endregion Instances


    #region Resources
    private Bitmap mBackImage = null;
    private Font mNazanin = new Font("B Nazanin", 11);
    private Font mDejaVu = new Font("Deja Vu Sans Mono", 9);
    #endregion Resources


    #region Pens
    private Pen mBorderPen = new Pen(Color.FromArgb(255, 255, 255, 255));
    private Pen mGridVInPen = new Pen(Color.FromArgb(140, 255, 255, 255));
    private Pen mGridVOutPen = new Pen(Color.FromArgb(255, 255, 255, 255), 2.0f);
    private Pen mGridHPen = new Pen(Color.FromArgb(150, 255, 255, 255));
    private Pen mDragPen = new Pen(Color.FromArgb(255, 0, 101, 145));
    private Pen mResizePen = new Pen(Color.FromArgb(255, 255, 255, 255));
    #endregion Pens


    #region Brushes
    private Brush mRowBrush = new SolidBrush(Color.FromArgb(40, 255, 255, 255));
    private Brush mDragBrush = new SolidBrush(Color.FromArgb(70, 43, 173, 206));
    private Brush mResizeOKBrush = new SolidBrush(Color.FromArgb(60, 0, 255, 0));
    private Brush mResizeFailBrush = new SolidBrush(Color.FromArgb(60, 255, 0, 0));
    #endregion Brushes


    #region Hinting Properties
    private Point mHintPosition;
    private Size mHintSize;
    private Brush mHintBrush;
    private Pen mHintPen;
    #endregion Hinting Properties


    #region Properties
    private Rectangle mGameArea = new Rectangle(10, 10, 10, 10);
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public Rectangle GameArea
    {
      get { return RectangleToScreen(mGameArea); }
    }

    private int mStartHour = 8;
    [DefaultValue(8)]
    public int StartHour
    {
      get { return mStartHour; }
      set { mStartHour = value; NeedsRefresh(); }
    }

    private int mEndHour = 21;
    [DefaultValue(21)]
    public int EndHour
    {
      get { return mEndHour; }
      set { mEndHour = value; NeedsRefresh(); }
    }

    private PersianDateTime mDate = PersianDateTime.Today;
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public PersianDateTime Date
    {
      get { return new PersianDateTime(mDate); }
      set { mDate = value; NeedsRefresh(); }
    }

    private int mSiteID = -1;
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public int SiteID
    {
      get { return mSiteID; }
      set { mSiteID = value; NeedsRefresh(); }
    }

    private int mRowHeight = 32;
    [DefaultValue(32)]
    public int RowHeight
    {
      get { return mRowHeight; }
      set { mRowHeight = value; NeedsRefresh(); }
    }

    public int RowsShown
    {
      get 
      {
        double dresult = (double)mGameArea.Height / mRowHeight;
        int iresult = mGameArea.Height / mRowHeight;
        return dresult > iresult ? iresult + 1 : iresult;
      }
    }

    private bool mIncludeDummies = true;
    [DefaultValue(true)]
    public bool IncludeDummies
    {
      get { return mIncludeDummies; }
      set { mIncludeDummies = value; NeedsRefresh(); }
    }

    private bool mIncludeRegulars = true;
    [DefaultValue(true)]
    public bool IncludeRegulars
    {
      get { return mIncludeRegulars; }
      set { mIncludeRegulars = value; NeedsRefresh(); }
    }

    private bool mIncludeCups = true;
    [DefaultValue(true)]
    public bool IncludeCups
    {
      get { return mIncludeCups; }
      set { mIncludeCups = value; NeedsRefresh(); }
    }

    private bool mIncludeLeagues = true;
  
    [DefaultValue(true)]
    public bool IncludeLeagues
    {
      get { return mIncludeLeagues; }
      set { mIncludeLeagues = value; NeedsRefresh(); }
    }

    private bool mIncludeElims = true;
    [DefaultValue(true)]
    public bool IncludeElims
    {
      get { return mIncludeElims; }
      set { mIncludeElims = value; NeedsRefresh(); }
    }
    #endregion Properties


    #region Events
    public event EventHandler<ScheduleTableUpdatedArgs> OnUpdated;
    public event EventHandler<MouseEventArgs> OnEntryClicked;
    public event EventHandler<MouseEventArgs> OnEntryRightClicked;
    #endregion Events


    #region Constructor
    public ScheduleTable()
    {
      mGridVInPen.DashStyle = DashStyle.Dot;
      AllowDrop = true;
      Size = new Size(600, 400);
      mHintPosition = mInvalidPoint;
      mArena.Parent = this;
      mArena.ShowBorder = false;
      mArena.MouseDown += ArenaMousePressed;
    }
    #endregion Constructor


    #region Utility Section
    #region Calculation: TimeStamp -> Position
    /// <summary>
    /// This method calculates a local x coordinate representing the given time
    /// </summary>
    /// <param name="pdt">The time to convert to x coordinate</param>
    /// <returns>Local X coordinate of the given timestamp</returns>
    private int GetXFromTimeStamp(PersianDateTime pdt)
    {
      return GetXFromTimeStamp(pdt.Hour, pdt.Minute);
    }

    /// <summary>
    /// This method calculates a local x coordinate representing
    /// the given hour and minute.
    /// </summary>
    /// <param name="hour">Hour of the time</param>
    /// <param name="minute">Minute of the time</param>
    /// <returns>Local X coordinate of the given timestamp</returns>
    private int GetXFromTimeStamp(int hour, int minute = 0)
    {
      double time = mEndHour - (hour + minute / 60.0);
      double dDeltaTime = mEndHour - mStartHour;
      double x = mGameArea.Left + time * mGameArea.Width / dDeltaTime;

      // Translate x and return
      return TinyMath.Round(x);
    }

    /// <summary>
    /// This method calculates local y coordinate representing the given timestamp
    /// </summary>
    /// <param name="pdt">The time stamp to find its representing local y coordinate</param>
    /// <returns>Local Y coordinate of the given timestamp</returns>
    private int GetYFromTimeStamp(PersianDateTime pdt)
    {
      return GetYFromIndex(mDate.GetNrOfDaysTo(pdt));
    }

    /// <summary>
    /// This method calculates local y coordinate of the given row index
    /// </summary>
    /// <param name="index">Index of the row. 0 is the top most visible row</param>
    /// <returns>Local Y coordinate of the given row index</returns>
    private int GetYFromIndex(int index)
    {
      return index * RowHeight + mGameArea.Top;
    }

    /// <summary>
    /// This method calculates the position of the given timestamp on the table.
    /// </summary>
    /// <param name="ts">The time stamp to calculate its position</param>
    /// <param name="constrain">Wether or not to reject outside positions</param>
    /// <returns>Local position of the given timestamp</returns>
    private Point GetPointFromTimeStamp(PersianDateTime ts, bool constrain)
    {
      int x = GetXFromTimeStamp(ts);
      int y = GetYFromTimeStamp(ts);

      Point result = new Point(x, y);
      if(constrain && !mGameArea.Contains(result))
        return mInvalidPoint;
      else
        return result;
    }
    #endregion Calculation: TimeStamp -> Position

    #region Calculation: Position -> TimeStamp
    /// <summary>
    /// This method calculates a time value from the given x coordinate
    /// in the local frame. Will return a valid time. In case the given
    /// x value lies beyond 00:00 this method will constrain the result
    /// by assigning  0 to hour and minute. But if it lies out of 23:59
    /// then it will constrain the result by return true with values 23
    /// and 59.
    /// <remarks>
    /// Used formula: Ending Hour - (x - GameArea.Left) * DeltaHour / Width
    /// </remarks>
    /// </summary>
    /// <param name="x">X coordinate of the hit test in local coordinates</param>
    /// <param name="hour">Calculated hour will be set in this variable</param>
    /// <param name="minute">Calculated minute will be set in this variable</param>
    /// <returns>True if calculated time </returns>
    private void GetTimeFromX(int x, out int hour, out int minute)
    {
      double dDeltaHour = mEndHour - mStartHour;
      double dDeltaX = (double)x - mGameArea.Left;
      double time = mEndHour - dDeltaX * dDeltaHour / mGameArea.Width;

      // Now, time is in the correct range. Constrain it if exceeds
      time = Math.Min(time, 23.9999);
      time = Math.Max(time, 0.0);

      // Now, calculate hour and minute based on the time
      hour = (int)time;
      minute = TinyMath.Round((time - hour) * 60);

      // Fit minute to grid
      if (minute >= 45)
        minute = 45;
      else if (minute >= 30)
        minute = 30;
      else if (minute >= 15)
        minute = 15;
      else
        minute = 0;
    }

    /// <summary>
    /// Index of the row. Zero means the top visible row,
    /// negative values indicate rows upper than zero index
    /// which are not visible but possible and positive values
    /// indicate rows below the top row which are also not
    /// constrained to be in the table. They can be below
    /// the bottom visible row.
    /// </summary>
    /// <param name="y">Y coordinate of the hit test. It is in local coordinates.</param>
    /// <returns>A date value representing the given y coordinate.</returns>
    private PersianDateTime GetDateFromY(int y)
    {
      int index = (y - mGameArea.Top) / mRowHeight;
      PersianDateTime result = new PersianDateTime(mDate);

      if (index == 0)
        return Date;
      else if(index < 0)
        return Date.TraverseDaysBackward(-index);
      else
        return Date.TraverseDaysForward(index);
    }

    /// <summary>
    /// This method calculates the timestamp which the given global coordinate points to.
    /// </summary>
    /// <param name="x">X of the point in global coordinates</param>
    /// <param name="y">Y of the point in global coordinates</param>
    /// <param name="constrain">Whether or not to ignore timestamps
    /// that are not shown on the table</param>
    /// <returns>A timestamp value representing the given global coordinates.
    /// Null if the point was not constrained on the table.</returns>
    private PersianDateTime GetTimeStampFromPoint(int x, int y, bool constrain)
    {
      // Constrain
      Point local = PointToClient(new Point(x, y));
      if (constrain && !mGameArea.Contains(local))
        return null;

      // Time
      int hour, minute;
      GetTimeFromX(local.X, out hour, out minute);

      // Date
      PersianDateTime start = GetDateFromY(local.Y);

      // Result
      return start.SetTime(hour, minute, 0);
    }


    /// <summary>
    /// This method finds out the date and time on which mouse cursor currently rest.
    /// </summary>
    public PersianDateTime GetCursorTimeStamp()
    {
      Point pt = Win32.GetCursorPosition();
      return GetTimeStampFromPoint(pt.X, pt.Y, true);
    }
    #endregion Calculation: Position -> TimeStamp

    private void BroadcastTableUpdated(PersianDateTime date)
    {
      ScheduleTableUpdatedArgs args = new ScheduleTableUpdatedArgs();
      args.Date = date;
      EventHandler<ScheduleTableUpdatedArgs> ev = OnUpdated;
      if (ev != null)
        ev(this, args);
    }

    private void UpdateHinting(Point location, Size size)
    {
      if (location != mHintPosition || mHintSize != size)
      {
        mHintPosition = location;
        mHintSize = size;
        RequestRepaint();
      }
    }

    /// <summary>
    /// This method calculates an ending time for the given entry
    /// based on the entry's length and given start time. In case
    /// of candidate matches, its possible for the match to not fit
    /// in a day based on its starting time. In that case null will
    /// be returned as suggested ending time.
    /// </summary>
    /// <param name="start">Starting time of the entry</param>
    /// <param name="entry">The entry to suggest and ending time</param>
    private PersianDateTime SuggestEndingTime(PersianDateTime start, ScheduleEntry entry)
    {
      if (start == null)
        return null;

      DB.Competition competition = null;
      if(entry.CandidateMatch != null)
        competition = entry.CandidateMatch.Competition;
      else if(entry.Game != null && entry.Game.CompetitionID != -1)
        competition = entry.Game.Competition;

      double len;
      PersianDateTime end = new PersianDateTime(start);
      if(competition != null)
        len = competition.GameTime * 60.0;
      else if(entry.Game != null)
        len = entry.Game.Finish.GetTimeDifference(entry.Game.Start);
      else 
        len = 3600.0;

      int st = start.Hour * 3600 + start.Minute * 60 + start.Second;
      int max = 23 * 3600 + 59 * 60 + 59;

      // Candidate matches can not be trimmed to fit in table
      if (st + len >= max)
      {
        if (competition != null)
          end = null;
        else
          end.SetTime(23, 59, 59);
      }
      else
        end.SetTime(0, 0, st + (int)len);

      return end;
    }

    /// <summary>
    /// This method checks whether the suggested time 
    /// range is ok for the given schedule entry
    /// </summary>
    /// <param name="entry">The entry to check for the time range</param>
    /// <param name="start">Start of the entry</param>
    /// <param name="end">End of the entry</param>
    /// <returns>True if its acceptable, false otherwise</returns>
    private bool IsTimeRangeOK(ScheduleEntry entry, PersianDateTime start, PersianDateTime end)
    {
      if (start == null)
        return false;

      if(start <= new PersianDateTime(PersianDateTime.Now.Value.Add(new TimeSpan(0, -15, 0))))
        return false;

      if(end == null)
        return false;

      int ex = entry.Game != null ? entry.Game.ID : -1;
      if (DB.GameManager.GameExistsInTimeLine(start, end, mSiteID, ex))
        return false;

      return true;
    }
    #endregion Utility Section


    #region Event Handlers
    #region Table Internal Events
    protected override void OnResize(EventArgs e)
    {
      base.OnResize(e);
      mGameArea = new Rectangle(10, 60, Width - 120, Height - 62);
      mArena.Location = mGameArea.Location;
      mArena.Size = mGameArea.Size;
      NeedsRefresh();
    }

    protected override void OnHandleCreated(EventArgs e)
    {
      base.OnHandleCreated(e);
      DB.GameManager.OnGameDelete += GameDeleted;
    }

    protected override void OnHandleDestroyed(EventArgs e)
    {
      base.OnHandleDestroyed(e);
      DB.GameManager.OnGameDelete -= GameDeleted;
    }

    protected override void OnMouseEnter(EventArgs e)
    {
      base.OnMouseEnter(e);
      Focus();
    }

    protected override void OnMouseWheel(MouseEventArgs e)
    {
      base.OnMouseWheel(e);
      BroadcastTableUpdated(e.Delta < 0 ? mDate.NextDay : mDate.PreviousDay);
    }

    protected override void OnMouseDown(MouseEventArgs e)
    {
      base.OnMouseUp(e);

      if (!ClientRectangle.Contains(e.Location))
        return;
      else if (e.Button == MouseButtons.Right)
        EntryRightClicked(this, e);
      else if (e.Button == MouseButtons.Left)
        EntryClicked(this, e);
    }

    /// <summary>
    /// This event handler is called whenever a game is deleted. No
    /// difference deleting by the table itself or somewhere else.
    /// </summary>
    private void GameDeleted(object sender, GameDeleteArgs e)
    {
      for (int i = 0; i < mEntries.Count; ++i)
        if (mEntries[i].Game == e.Game)
        {
          DeleteEntry(i);
          RequestRepaint();
          break;
        }
    }

    private void ArenaMousePressed(object sender, MouseEventArgs e)
    {
      OnMouseDown(e);
    }
    #endregion Table Internal Events


    #region Clicks
    private void EntryClicked(object sender, MouseEventArgs e)
    {
      EventHandler<MouseEventArgs> ev = OnEntryClicked;
      if (ev != null)
        ev(sender, e);
    }

    private void EntryRightClicked(object sender, MouseEventArgs e)
    {
      EventHandler<MouseEventArgs> ev = OnEntryRightClicked;
      if (ev != null)
        ev(sender, e);
    }
    #endregion Clicks


    #region Drag Events
    /// <summary>
    /// Drag is entered. To Do list:
    /// Step 1: Check the drag objet type
    /// </summary>
    protected override void OnDragEnter(DragEventArgs drgevent)
    {
      mDragEntry = drgevent.Data.GetData(typeof(ScheduleEntry)) as ScheduleEntry;
    }

    /// <summary>
    /// Drag is left. To Do list:
    /// Step 1: Nullify the drag entry
    /// Step 2: Invalidate the control
    /// </summary>
    /// <param name="e"></param>
    protected override void OnDragLeave(EventArgs e)
    {
      mDragEntry = null;
      RequestRepaint();
    }

    /// <summary>
    /// Drag over. To Do list:
    /// Step 1: Calculate start timestamp with active constraints
    /// Step 2: Calculate ending timestamp
    /// Step 3: Check if start timestamp is valid and no other games exist during the entry
    /// Step 4: Adjust hinting location and size
    /// Step 5: Update hinting
    /// </summary>
    /// <param name="drgevent"></param>
    protected override void OnDragOver(DragEventArgs drgevent)
    {
      if (mDragEntry == null)
        return;

      PersianDateTime start = GetTimeStampFromPoint(drgevent.X, drgevent.Y, true);
      PersianDateTime end = SuggestEndingTime(start, mDragEntry);
      Point location = mInvalidPoint;
      Size size = mDragEntry.Size;

      // If start time is valid and no game exists in the drag item's duration
      if(IsTimeRangeOK(mDragEntry, start, end))
      {
        Point spoint = GetPointFromTimeStamp(start, false);
        location = GetPointFromTimeStamp(end, false);
        size = new Size(spoint.X - location.X, mRowHeight);
        drgevent.Effect = DragDropEffects.Move;
      }
      else
      {
        location = mInvalidPoint;
        drgevent.Effect = DragDropEffects.None;
      }

      mHintBrush = mDragBrush;
      mHintPen = mDragPen;
      UpdateHinting(location, size);
    }

    /// <summary>
    /// Drag drop. Item is dropped, no sanity check needed.
    /// To Do list:
    /// Step 1: Calculate start and end of the entry
    /// Step 2: Create a game or update it with the new start and end
    /// Step 3: Adjust the entry
    /// Step 4: Remove hinting
    /// </summary>
    /// <param name="drgevent"></param>
    protected override void OnDragDrop(DragEventArgs drgevent)
    {
      PersianDateTime start = GetTimeStampFromPoint(drgevent.X, drgevent.Y, false);
      PersianDateTime end = SuggestEndingTime(start, mDragEntry);

      // Type 1: ScheduleEntry is for a CandidateMatch which has no associated game
      ScheduleEntry entry;
      if (mDragEntry.CandidateMatch != null)
      {
        DB.Game game = new DB.Game(start, end, mSiteID);
        if (!mDragEntry.CandidateMatch.AssignGame(game))
        {
          DB.GameManager.DeleteGame(game.ID);
          UpdateHinting(mInvalidPoint, mHintSize);
          return;
        }

        mDragEntry.CandidateMatch = null;
        entry = LoadEntry(mDragEntry, game);
      }

      // Type 2: ScheduleEntry is a completely empty entry dragged on table
      else if (mDragEntry.Game == null)
        entry = LoadEntry(mDragEntry, new DB.Game(start, end, mSiteID));

      // Type 3: ScheduleEntry's game is for competition
      else if (mDragEntry.Game.CompetitionID != -1)
      {
        PersianDateTime old_start = mDragEntry.Game.Start;
        PersianDateTime old_finish = mDragEntry.Game.Finish;
        mDragEntry.Game.Start = start;
        mDragEntry.Game.Finish = end;

        if (!mDragEntry.Game.CandidateMatch.AssignGame(mDragEntry.Game))
        {
          mDragEntry.Game.Start = old_start;
          mDragEntry.Game.Finish = old_finish;
          return;
        }

        entry = mDragEntry;
        entry.Game.Apply();
      }

      // Type 4: A regular game
      else
      {
        entry = mDragEntry;
        entry.Game.Start = start;
        entry.Game.Finish = end;
        entry.Game.Apply();
      }

      // Adjust entry
      AdjustEntry(entry);

      // Remove hinting
      UpdateHinting(mInvalidPoint, mHintSize);
    }
    #endregion Drag Events


    #region Entry Resizing Events
    /// <summary>
    /// This method is called whenever an entry is being resized.
    /// It checks for resizing constraints and shows an appropriate hinting.
    /// </summary>
    private void EntryResizing(object sender, ScheduleEntryResizeArgs e)
    {
      PersianDateTime tstart = GetTimeStampFromPoint(e.NewRect.Right - 2, e.NewRect.Top, false);
      PersianDateTime tend = GetTimeStampFromPoint(e.NewRect.Left, e.NewRect.Top, false);

      // Check to see if intersects with any other entry
      ScheduleEntry entry = sender as ScheduleEntry;
      mLastResizeAcceptable = true;
      if (tstart == null)
        mLastResizeAcceptable = false;
      else if (tend <= tstart)
        mLastResizeAcceptable = false;
      else if (DB.GameManager.GameExistsInTimeLine(tstart, tend, mSiteID, entry.Game.ID))
        mLastResizeAcceptable = false;

      Point ps = GetPointFromTimeStamp(tstart, false);
      Point location = GetPointFromTimeStamp(tend, false);
      Size size = new Size(ps.X - location.X, mRowHeight);

      if (size.Width < 5)
        location = mInvalidPoint;

      mHintBrush = mLastResizeAcceptable ? mResizeOKBrush : mResizeFailBrush;
      mHintPen = mResizePen;
      UpdateHinting(location, size);
    }

    /// <summary>
    /// Event handler when resize is done. No need to double check.
    /// To Do List:
    /// Step 1: Update the entries start and end
    /// Step 2: Update its visual appearance
    /// Step 3: Disable hinting
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void EntryResized(object sender, ScheduleEntryResizeArgs e)
    {
      if (mLastResizeAcceptable)
      {
        ScheduleEntry entry = sender as ScheduleEntry;
        entry.Game.Finish = GetTimeStampFromPoint(e.NewRect.Left, e.NewRect.Top, false);
        entry.Game.Apply();
        AdjustEntry(entry);
      }

      UpdateHinting(mInvalidPoint, mHintSize);
    }
    #endregion Entry Resizing Events
    #endregion Event Handlers


    #region Entry Related Procedures
    /// <summary>
    /// This method initializes an entry and returns it.
    /// </summary>
    /// <param name="entry">Entry to be loaded and initialized</param>
    /// <param name="game">Game to assign to the entry</param>
    /// <returns>The entry itself</returns>
    private ScheduleEntry LoadEntry(ScheduleEntry entry, DB.Game game)
    {
      entry.Parent = mArena;
      entry.Game = game;
      entry.OnEntryResized += EntryResized;
      entry.OnEntryResizing += EntryResizing;
      entry.OnClick += EntryClicked;
      entry.OnRightClick += EntryRightClicked;
      mEntries.Add(entry);
      return entry;
    }

    /// <summary>
    /// This method updates visual properties of the given schedule 
    /// entry based on the entry's game information. To Do list:
    /// Step 1: Update the entry's visual rect based on its game's time
    /// Step 2: Set visibility based on its game type and allowed types.
    ///  Also consider siting filter.
    /// </summary>
    /// <param name="entry">The entry to update its location, size and visibility</param>
    private void AdjustEntry(ScheduleEntry entry)
    {
      // Calculate starting and ending time
      Point pright = GetPointFromTimeStamp(entry.Game.Start, false);
      entry.Location = GetPointFromTimeStamp(entry.Game.Finish, false);
      entry.Width = pright.X - entry.Left + 1 ;
      entry.Height = mRowHeight;
      entry.Left -= mGameArea.Left;
      entry.Top -= mGameArea.Top;
      entry.NeedsRefresh();

      // Adjust the visibility
      entry.Visible = entry.Game.SiteID == mSiteID;
      if (!entry.Visible)
        return;

      if (entry.Game.State == GameState.Free)
        entry.Visible = entry.Game.AdmissionFee == 0.0 ? mIncludeDummies : mIncludeRegulars;
      else if (entry.Game.Type == CompetitionType.Dummy)
        entry.Visible = mIncludeDummies;
      else if (entry.Game.Type == CompetitionType.FreeMatch)
        entry.Visible = mIncludeRegulars;
      else if (entry.Game.Type == CompetitionType.Cup)
        entry.Visible = mIncludeCups;
      else if (entry.Game.Type == CompetitionType.League)
        entry.Visible = mIncludeLeagues;
      else if (entry.Game.Type == CompetitionType.Elimination)
        entry.Visible = mIncludeElims;
    }

    /// <summary>
    /// This method finds the instance an entry having the given game.
    /// If no such entries are found an entry will be created for the game.
    /// </summary>
    /// <param name="game">The game to get an entry instance for it</param>
    /// <returns>Instance of the schedule entry containing the game. 
    /// This method always return a valid instance.</returns>
    private ScheduleEntry CreateGameEntry(DB.Game game)
    {
      for (int i = 0; i < mEntries.Count; ++i)
        if (mEntries[i].Game.ID == game.ID)
          return null;

      ScheduleEntry ent = new ScheduleEntry();
      LoadEntry(ent, game);
      return ent;
    }

    /// <summary>
    /// This method deletes the entry at given index.
    /// </summary>
    /// <param name="index">Index of the entry</param>
    private void DeleteEntry(int index)
    {
      mEntries[index].OnEntryResized -= EntryResized;
      mEntries[index].OnEntryResizing -= EntryResizing;
      mEntries[index].Parent = null;
      mEntries[index].Dispose();
      mEntries.RemoveAt(index);
    }

    /// <summary>
    /// This method updates game cache for the table's current
    /// date period and site. Removes dead entries and creates
    /// non existing ones.
    /// To Do List:
    /// Step 1: Remove dead entries
    /// Step 2: Get list of games for set date and site
    /// Step 3: Adjust and create all non existing entries.
    /// </summary>
    private void UpdateEntries()
    {
      // No need to checkout games when site has not been given.
      if (mSiteID == -1)
        return;

      // Remove dead entries
      for (int i = 0; i < mEntries.Count; ++i)
        if (mEntries[i].Game.ID == -1)
          DeleteEntry(i--);

      // Refresh game manager cache
      PersianDateTime end = new PersianDateTime(mDate);
      end.TraverseDaysForward(RowsShown + 1);
      List<DB.Site> sites = new List<DB.Site>();
      sites.Add(DB.Site.GetSiteByID(mSiteID));
      List<DB.Game> games = DB.GameManager.GetGames(
        mDate, 
        end, 
        sites,
        mIncludeDummies,
        mIncludeRegulars,
        mIncludeCups,
        mIncludeLeagues,
        mIncludeElims);

      // Make sure all the games are loaded
      for(int i = 0; i < games.Count; ++i)
        CreateGameEntry(games[i]);
      
      // Adjust all entries
      for(int i = 0; i < mEntries.Count; ++i)
        AdjustEntry(mEntries[i]);
    }

    /// <summary>
    /// This method creates a free game for the given time 
    /// period and adds an entry for it in the table.
    /// </summary>
    public bool AddEmptyEntry(PersianDateTime st, PersianDateTime fn)
    {
      if(DB.GameManager.GameExistsInTimeLine(st, fn, mSiteID, -1))
        return false;

      ScheduleEntry ent = CreateGameEntry(new DB.Game(st, fn, mSiteID));
      AdjustEntry(ent);
      return true;
    }

    public void adjustEntries()
    {
      foreach (ScheduleEntry ent in mEntries) 
        AdjustEntry(ent);
    }
    #endregion Entry Related Procedures


    #region Graphical representation
    protected override BehaviorParams CreateBehavior
    {
      get
      {
        BehaviorParams prms = base.CreateBehavior;
        prms.UtilizesMouseInput();
        prms.RendersText();
        return prms;
      }
    }

    /// <summary>
    /// Draws the control. To Do list:
    /// Step 1: Draw back image, the table itself.
    /// Step 2: Set clipping and draw all entries
    /// Step 3: Draw hinting if specified
    /// </summary>
    protected override void OnRecreateView(RecreateArgs e)
    {
      e.Canvas.DrawImageUnscaled(0, 0, mBackImage);
      if (mHintPosition != mInvalidPoint)
      {
        e.Canvas.FillRoundedRectangle(mHintBrush, new Rectangle(mHintPosition, mHintSize), 2);
        e.Canvas.DrawRoundedRectangle(mHintPen, new Rectangle(mHintPosition, mHintSize), 2);
      }
    }

    /// <summary>
    /// This method is responsible to completely refresh the table.
    /// To Do List:
    /// Step 1: Update list of schedule entries
    /// Step 2: Create the table's graphical background
    /// </summary>
    private void NeedsRefresh()
    {
      if (!Session.DevelopMode)
        UpdateEntries();

      CreateSchedule();
      RequestRepaint();
    }

    /// <summary>
    /// This method creates the schedule's background.
    /// To Do List:
    /// Step 1: Create resources
    /// Step 2: Draw table border
    /// Step 3: Clip the graphics so that further actions are drawn inside
    /// Step 4: Draw horizontal rows differently for odd and even rows and draw dates
    /// Step 5: Draw vertical lines, inner grid, times and time markers
    /// </summary>
    private void CreateSchedule()
    {
      // Create needed tools and initialize graphics
      mBackImage = new Bitmap(ClientSize.Width, ClientSize.Height);
      Graphics canvas = Graphics.FromImage(mBackImage);
      TextRenderer tr = new TextRenderer();
      tr.Canvas = canvas;

      // Draw client border
      canvas.DrawRoundedRectangle(mBorderPen, 0, 0, Width - 1, Height - 1, 6);

      // Clip the border
      canvas.SetClip(new Rectangle(3, 3, Width - 6, mGameArea.Bottom), CombineMode.Intersect);

      // Draw horizontal row lines and paint odd rows background
      // Do not draw bottom line for the last row
      // Also draw row dates
      tr.Font = mNazanin;
      tr.Color = Color.White;
      tr.Left = mGameArea.Right + 5;

      PersianDateTime date = new PersianDateTime(mDate);
      for(int row = 0; row < RowsShown; ++row)
      {
        int liney = GetYFromTimeStamp(date);
        // Draw only odd rows background. Transparent
        if (row % 2 == 1)
          canvas.FillRectangle(mRowBrush, new Rectangle(mGameArea.Left, liney, mGameArea.Width, RowHeight));

        // Draw upper line of the row
        canvas.DrawLine(mGridHPen, mGameArea.Left, liney, mGameArea.Right, liney);

        tr.Text = date.ToString(DateTimeString.DayOfWeekAndDate);
        tr.Top = liney + (RowHeight - tr.TextHeight) / 2;
        tr.Draw();
        date.TraverseDaysForward(1);
      }

      // Draw vertical lines, inner grid, times and time markers
      tr.Font = mDejaVu;
      tr.RightToLeft = false;
      tr.Vertical = true;
      for (int i = mStartHour; i <= mEndHour; ++i)
      {
        int linex = GetXFromTimeStamp(i);
        canvas.DrawLine(mGridVInPen, linex, mGameArea.Top, linex, mGameArea.Bottom);
        canvas.DrawLine(mGridVOutPen, linex, mGameArea.Top, linex, mGameArea.Top - 5);

        // No need to translate when drawing left to right.
        // Also no need to set size.
        tr.Text = PersianDateTime.Today.SetTime(i, 0, 0).ToString(DateTimeString.TimeNoSecond);
        tr.Left = linex - tr.TextWidth / 2; 
        tr.Top = mGameArea.Top - 10 - tr.TextHeight;
        tr.Draw();
      }

      // Free resources
      canvas.Dispose();
    }
    #endregion Graphical representation
  }
}
