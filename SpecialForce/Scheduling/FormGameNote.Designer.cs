﻿namespace SpecialForce.Scheduling
{
  partial class FormGameNote
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGameNote));
      this.txtNote = new System.Windows.Forms.TextBox();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.dtDue = new SpecialForce.PersianDatePicker();
      this.chkDue = new SpecialForce.TransparentCheckBox();
      this.pnlDue = new SpecialForce.ToolBar();
      this.btnNewNote = new SpecialForce.ToolButton();
      this.pnlNote = new SpecialForce.ToolBar();
      this.btnApplyNote = new SpecialForce.ToolButton();
      this.btnDeleteNote = new SpecialForce.ToolButton();
      this.transparentLabel4 = new SpecialForce.TransparentLabel();
      this.transparentLabel5 = new SpecialForce.TransparentLabel();
      this.lblGameInfo1 = new SpecialForce.TransparentLabel();
      this.lblGameInfo2 = new SpecialForce.TransparentLabel();
      this.lvNotes = new SpecialForce.UserInterface.ListView();
      this.olvColumn6 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn7 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.pnlDue.SuspendLayout();
      this.pnlNote.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lvNotes)).BeginInit();
      this.SuspendLayout();
      // 
      // txtNote
      // 
      this.txtNote.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtNote.Location = new System.Drawing.Point(12, 54);
      this.txtNote.Multiline = true;
      this.txtNote.Name = "txtNote";
      this.txtNote.Size = new System.Drawing.Size(268, 98);
      this.txtNote.TabIndex = 1;
      this.txtNote.TextChanged += new System.EventHandler(this.txtNote_TextChanged);
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel2.Location = new System.Drawing.Point(204, 11);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(76, 26);
      this.transparentLabel2.TabIndex = 2;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel3.Location = new System.Drawing.Point(215, 54);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(65, 26);
      this.transparentLabel3.TabIndex = 3;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // dtDue
      // 
      this.dtDue.Location = new System.Drawing.Point(12, 43);
      this.dtDue.Name = "dtDue";
      this.dtDue.Size = new System.Drawing.Size(132, 37);
      this.dtDue.TabIndex = 4;
      this.dtDue.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtDue.Texts")));
      // 
      // chkDue
      // 
      this.chkDue.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.chkDue.ForeColor = System.Drawing.Color.White;
      this.chkDue.Location = new System.Drawing.Point(61, 7);
      this.chkDue.Name = "chkDue";
      this.chkDue.Size = new System.Drawing.Size(219, 21);
      this.chkDue.TabIndex = 5;
      this.chkDue.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkDue.Texts")));
      this.chkDue.CheckedChanged += new System.EventHandler(this.chkDue_CheckedChanged);
      // 
      // pnlDue
      // 
      this.pnlDue.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlDue.Controls.Add(this.chkDue);
      this.pnlDue.Controls.Add(this.dtDue);
      this.pnlDue.Controls.Add(this.transparentLabel3);
      this.pnlDue.Location = new System.Drawing.Point(269, 259);
      this.pnlDue.Name = "pnlDue";
      this.pnlDue.Size = new System.Drawing.Size(293, 95);
      this.pnlDue.TabIndex = 6;
      // 
      // btnNewNote
      // 
      this.btnNewNote.Hint = "یادداشت جدید";
      this.btnNewNote.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnNewNote.Image")));
      this.btnNewNote.Location = new System.Drawing.Point(221, 97);
      this.btnNewNote.Name = "btnNewNote";
      this.btnNewNote.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnNewNote.Size = new System.Drawing.Size(32, 32);
      this.btnNewNote.TabIndex = 7;
      this.btnNewNote.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnNewNote.Texts")));
      this.btnNewNote.TransparentColor = System.Drawing.Color.Transparent;
      this.btnNewNote.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.btnNewNote_OnClick);
      // 
      // pnlNote
      // 
      this.pnlNote.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlNote.Controls.Add(this.btnApplyNote);
      this.pnlNote.Controls.Add(this.txtNote);
      this.pnlNote.Controls.Add(this.transparentLabel2);
      this.pnlNote.Location = new System.Drawing.Point(269, 85);
      this.pnlNote.Name = "pnlNote";
      this.pnlNote.Size = new System.Drawing.Size(293, 168);
      this.pnlNote.TabIndex = 8;
      // 
      // btnApplyNote
      // 
      this.btnApplyNote.Enabled = false;
      this.btnApplyNote.Hint = "ذخیره یادداشت";
      this.btnApplyNote.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnApplyNote.Image")));
      this.btnApplyNote.Location = new System.Drawing.Point(13, 12);
      this.btnApplyNote.Name = "btnApplyNote";
      this.btnApplyNote.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnApplyNote.Size = new System.Drawing.Size(32, 32);
      this.btnApplyNote.TabIndex = 8;
      this.btnApplyNote.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnApplyNote.Texts")));
      this.btnApplyNote.TransparentColor = System.Drawing.Color.Transparent;
      this.btnApplyNote.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.btnApplyNote_OnClick);
      // 
      // btnDeleteNote
      // 
      this.btnDeleteNote.Hint = "حذف یادداشت";
      this.btnDeleteNote.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnDeleteNote.Image")));
      this.btnDeleteNote.Location = new System.Drawing.Point(183, 97);
      this.btnDeleteNote.Name = "btnDeleteNote";
      this.btnDeleteNote.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnDeleteNote.Size = new System.Drawing.Size(32, 32);
      this.btnDeleteNote.TabIndex = 9;
      this.btnDeleteNote.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnDeleteNote.Texts")));
      this.btnDeleteNote.TransparentColor = System.Drawing.Color.Transparent;
      this.btnDeleteNote.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.btnDeleteNote_OnClick);
      // 
      // transparentLabel4
      // 
      this.transparentLabel4.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel4.Location = new System.Drawing.Point(13, 96);
      this.transparentLabel4.Name = "transparentLabel4";
      this.transparentLabel4.Size = new System.Drawing.Size(103, 26);
      this.transparentLabel4.TabIndex = 11;
      this.transparentLabel4.TabStop = false;
      this.transparentLabel4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel4.Texts")));
      // 
      // transparentLabel5
      // 
      this.transparentLabel5.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel5.Location = new System.Drawing.Point(12, 12);
      this.transparentLabel5.Name = "transparentLabel5";
      this.transparentLabel5.Size = new System.Drawing.Size(75, 26);
      this.transparentLabel5.TabIndex = 12;
      this.transparentLabel5.TabStop = false;
      this.transparentLabel5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel5.Texts")));
      // 
      // lblGameInfo1
      // 
      this.lblGameInfo1.FixFromRight = true;
      this.lblGameInfo1.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblGameInfo1.Location = new System.Drawing.Point(93, 12);
      this.lblGameInfo1.Name = "lblGameInfo1";
      this.lblGameInfo1.Size = new System.Drawing.Size(12, 26);
      this.lblGameInfo1.TabIndex = 13;
      this.lblGameInfo1.TabStop = false;
      this.lblGameInfo1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblGameInfo1.Texts")));
      // 
      // lblGameInfo2
      // 
      this.lblGameInfo2.FixFromRight = true;
      this.lblGameInfo2.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblGameInfo2.Location = new System.Drawing.Point(93, 44);
      this.lblGameInfo2.Name = "lblGameInfo2";
      this.lblGameInfo2.Size = new System.Drawing.Size(0, 0);
      this.lblGameInfo2.TabIndex = 14;
      this.lblGameInfo2.TabStop = false;
      this.lblGameInfo2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblGameInfo2.Texts")));
      // 
      // lvNotes
      // 
      this.lvNotes.AllColumns.Add(this.olvColumn6);
      this.lvNotes.AllColumns.Add(this.olvColumn7);
      this.lvNotes.AllowDrop = true;
      this.lvNotes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn6,
            this.olvColumn7});
      this.lvNotes.EmptyListMsgFont = new System.Drawing.Font("B Nazanin", 12F);
      this.lvNotes.ForeColor = System.Drawing.Color.Black;
      this.lvNotes.FullRowSelect = true;
      this.lvNotes.GridLines = true;
      this.lvNotes.IsSimpleDragSource = true;
      this.lvNotes.Location = new System.Drawing.Point(12, 140);
      this.lvNotes.Name = "lvNotes";
      this.lvNotes.RightToLeftLayout = true;
      this.lvNotes.ShowGroups = false;
      this.lvNotes.Size = new System.Drawing.Size(241, 214);
      this.lvNotes.TabIndex = 49;
      this.lvNotes.UseCompatibleStateImageBehavior = false;
      this.lvNotes.View = System.Windows.Forms.View.Details;
      this.lvNotes.FormatRow += new System.EventHandler<BrightIdeasSoftware.FormatRowEventArgs>(this.lvNotes_FormatRow);
      this.lvNotes.SelectedIndexChanged += new System.EventHandler(this.lvNotes_SelectedIndexChanged);
      // 
      // olvColumn6
      // 
      this.olvColumn6.CellPadding = null;
      this.olvColumn6.Text = "تاریخ";
      this.olvColumn6.Width = 96;
      // 
      // olvColumn7
      // 
      this.olvColumn7.CellPadding = null;
      this.olvColumn7.Text = "نویسنده";
      this.olvColumn7.Width = 122;
      // 
      // FormGameNote
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoScroll = true;
      this.ClientSize = new System.Drawing.Size(572, 370);
      this.Controls.Add(this.lvNotes);
      this.Controls.Add(this.lblGameInfo2);
      this.Controls.Add(this.lblGameInfo1);
      this.Controls.Add(this.transparentLabel5);
      this.Controls.Add(this.transparentLabel4);
      this.Controls.Add(this.btnDeleteNote);
      this.Controls.Add(this.pnlNote);
      this.Controls.Add(this.btnNewNote);
      this.Controls.Add(this.pnlDue);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormGameNote";
      this.Text = "یادداشت های بازی";
      this.Load += new System.EventHandler(this.FormGameNote_Load);
      this.pnlDue.ResumeLayout(false);
      this.pnlNote.ResumeLayout(false);
      this.pnlNote.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lvNotes)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TextBox txtNote;
    private TransparentLabel transparentLabel2;
    private TransparentLabel transparentLabel3;
    private PersianDatePicker dtDue;
    private TransparentCheckBox chkDue;
    private ToolBar pnlDue;
    private ToolButton btnNewNote;
    private ToolBar pnlNote;
    private ToolButton btnDeleteNote;
    private ToolButton btnApplyNote;
    private TransparentLabel transparentLabel4;
    private TransparentLabel transparentLabel5;
    private TransparentLabel lblGameInfo1;
    private TransparentLabel lblGameInfo2;
    private UserInterface.ListView lvNotes;
    private BrightIdeasSoftware.OLVColumn olvColumn6;
    private BrightIdeasSoftware.OLVColumn olvColumn7;
  }
}