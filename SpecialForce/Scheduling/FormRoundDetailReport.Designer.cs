﻿namespace SpecialForce.Scheduling
{
  partial class FormRoundDetailReport
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRoundDetailReport));
      this.report1 = new SpecialForce.Reports.Report();
      this.SuspendLayout();
      // 
      // report1
      // 
      this.report1.BackColor = System.Drawing.Color.White;
      this.report1.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.report1.ForeColor = System.Drawing.Color.Black;
      this.report1.Location = new System.Drawing.Point(0, 0);
      this.report1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.report1.MinimumSize = new System.Drawing.Size(0, 400);
      this.report1.Name = "report1";
      this.report1.Size = new System.Drawing.Size(712, 488);
      this.report1.TabIndex = 0;
      this.report1.Title = "اطلاعات راند";
      // 
      // FormRoundDetailReport
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoScroll = true;
      this.BackColor = System.Drawing.Color.White;
      this.ClientSize = new System.Drawing.Size(737, 491);
      this.Controls.Add(this.report1);
      this.EmptyBackground = true;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormRoundDetailReport";
      this.Text = "اطلاعات راند";
      this.Shown += new System.EventHandler(this.FormRoundDetailReport_Shown);
      this.ResumeLayout(false);

    }

    #endregion

    private Reports.Report report1;
  }
}