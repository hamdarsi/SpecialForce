﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Scheduling
{
  public partial class FormGameNoteReportSettings : SessionAwareForm
  {
    public FormGameNoteReportSettings()
    {
      InitializeComponent();
    }

    protected override void ApplyPermissions(Post post)
    {
      chkSites.Enabled = post.IsGod();
    }

    private void btnReport_Click(object sender, EventArgs e)
    {
      List<DB.Site> sites = new List<DB.Site>();
      for (int i = 0; i < chkSites.Items.Count; ++i)
        if (chkSites.GetItemChecked(i))
          sites.Add(chkSites.Items[i] as DB.Site);

      if (sites.Count == 0)
      {
        Session.ShowError("هیچ باشگاهی انتخاب نشده است.");
        return;
      }

      bool incCups = chkCups.Checked;
      bool incElims = chkEliminations.Checked;
      bool incLeagues = chkLeagues.Checked;
      bool incRegulars = chkRegularMatches.Checked;
      bool incDummies = chkDummies.Checked;

      if (!incCups && !incElims && !incLeagues && !incRegulars && !incDummies)
      {
        Session.ShowMessage("هیج نوع مسابقه ای انتخاب نشده است!");
        return;
      }

      PersianDateTime start, finish;
      string strTitle;
      if (cmbReportType.SelectedIndex == 0)
      {
        start = dtStart.Date;
        finish = dtStart.Date.SetTime(23, 59, 59);
        strTitle = "در " + start.ToString(DateTimeString.CompactDate);
      }
      else if (cmbReportType.SelectedIndex == 1)
      {
        start = dtStart.Date;
        finish = start.NextWeek;
        strTitle = "از " + start.ToString(DateTimeString.DayOfWeekAndDate) + " به مدت یک هفته";
      }
      else if (cmbReportType.SelectedIndex == 2)
      {
        start = dtStart.Date;
        finish = dtStart.Date.NextMonth;
        strTitle = start.ToString(DateTimeString.CompactDate) + " به مدت یک ماه";
      }
      else
      {
        start = dtStart.Date;
        finish = dtEnd.Date;
        strTitle = "از " + start.ToString(DateTimeString.CompactDate) + " تا " + finish.ToString(DateTimeString.CompactDate);
      }

      // show the report
      FormGameNoteReports frm = new FormGameNoteReports();
      frm.Title = "گزارش یادداشت بازی ها " + strTitle;
      frm.Games = DB.GameManager.GetGames(
        start,
        finish,
        sites, 
        incDummies,
        incRegulars,
        incCups,
        incLeagues,
        incElims);
      ShowModal(frm, true);
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void FormGameNoteReportSettings_Load(object sender, EventArgs e)
    {
      cmbReportType.SelectedIndex = 1;

      List<DB.Site> sites = DB.Site.GetAllSites(Session.CurrentStaff.Post.IsPrivileged());

      for (int i = 0; i < sites.Count; ++i)
      {
        chkSites.Items.Add(sites[i]);
        chkSites.SetItemChecked(i, sites[i].ID == Session.ActiveSiteID);
      }
      chkSites.Enabled = Session.CurrentStaff.Post.IsPrivileged();
    }

    private void cmbReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (cmbReportType.SelectedIndex == 0)
      {
        dtStart.Date = PersianDateTime.Today;
        dtEnd.Date = dtStart.Date.NextDay;
        lblStart.Text = "روز:";
      }
      else if (cmbReportType.SelectedIndex == 1)
      {
        dtStart.Date = PersianDateTime.ThisWeek;
        dtEnd.Date = dtStart.Date.NextWeek;
        lblStart.Text = "روز:";
      }
      else if (cmbReportType.SelectedIndex == 2)
      {
        dtStart.Date = PersianDateTime.ThisMonth;
        dtEnd.Date = dtStart.Date.NextMonth;
        lblStart.Text = "ماه:";
      }
      else if (cmbReportType.SelectedIndex == 3)
      {
        lblStart.Text = "از:";
        dtStart.Date = PersianDateTime.ThisMonth;
        dtEnd.Date = PersianDateTime.ThisMonth.NextMonth;
      }

      dtStart.ShowDay = cmbReportType.SelectedIndex != 2;
      dtEnd.Visible = cmbReportType.SelectedIndex == 3;
      lblEnd.Visible = dtEnd.Visible;
    }
  }
}
