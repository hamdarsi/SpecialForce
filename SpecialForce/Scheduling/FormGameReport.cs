﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Scheduling
{
  public partial class FormGameReport : SessionAwareForm
  {
    private List<DB.Game> mGames = null;
    public List<DB.Game> Games
    {
      get { return mGames; }
      set { mGames = value; }
    }

    private string mTitle = "گزارش بازی ها و سانس ها";
    public string Title
    {
      get { return mTitle; }
      set { mTitle = value; }
    }

    private bool mShortState = false;
    
    public bool ShortState
    {
      get { return mShortState; }
      set { mShortState = value; }
    }

    public FormGameReport()
    {
      InitializeComponent();
    }

    private void FormGameReport_Shown(object sender, EventArgs e)
    {
      // Start
      report1.BeginUpdate();
      Text = mTitle;
      report1.Title = mTitle;

      Reports.Table table = report1.AddTable();
      table.AutoIndexColumn = true;
      table.AddNewColumn(Types.DateTime, "شروع", 90).DateTimeMode = DateTimeString.CompactDateTime;
      table.AddNewColumn(Types.DateTime, "پایان", 90).DateTimeMode = DateTimeString.CompactDateTime;
      table.AddNewColumn(Types.String, "نوع", 70);
      table.AddNewColumn(Types.String, "هدف", 75);
      table.AddNewColumn(Types.String, "تیم آبی", 100);
      table.AddNewColumn(Types.String, "تیم سبز", 100);
      table.AddNewColumn(Types.String, "وضعیت", 100);
      table.OnRowDoubleClicked += RowDoubleClicked;

      Statistics<Stats.Game> stats = Stats.Generator.Generate(mGames);
      foreach(Stats.Game st in stats.Data)
      {
        Reports.Row row = table.AddNewRow(st.Instance);
        row[0] = st.Start;
        row[1] = st.End;
        row[2] = st.Type;
        row[3] = st.Object;
        row[4] = st.BlueTeamName;
        row[5] = st.GreenTeamName;
        row[6] = mShortState ? st.ShortState : st.State;
      }

      stats.PumpMetaData(report1.AddTable(false, 150, 300));

      // finished
      report1.EndUpdate();
    }

    private void RowDoubleClicked(object sender, ReportRowArgs e)
    {
      FormReportRounds frm = new FormReportRounds();
      frm.Game = e.Data as DB.Game;
      frm.Rounds = Session.CurrentStaff.IsLowlyUser() ? frm.Game.Rounds : frm.Game.AllRounds;
      ShowModal(frm, false);
    }
  }
}
