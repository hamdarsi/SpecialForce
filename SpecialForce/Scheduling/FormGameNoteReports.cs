﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Scheduling
{
  public partial class FormGameNoteReports : SessionAwareForm
  {
    private Brush mDeletedBrush = new SolidBrush(Color.FromArgb(255, 85, 85));
    private Brush mExpiredBrush = new SolidBrush(Color.FromArgb(195, 195, 195));
    private Brush mNormalBrush = new SolidBrush(Color.FromArgb(255, 255, 255));

    private List<DB.Game> mGames = null;
    public List<DB.Game> Games
    {
      get { return mGames; }
      set { mGames = value; }
    }

    private string mTitle = "";
    public string Title
    {
      get { return mTitle; }
      set { mTitle = value; }
    }

    public FormGameNoteReports()
    {
      InitializeComponent();
    }

    private void FormGameNoteReports_Shown(object sender, EventArgs e)
    {
      report1.BeginUpdate();
      report1.Title = mTitle;

      Reports.Table table = report1.AddTable();
      table.AutoIndexColumn = true;
      table.AddNewColumn(Types.String, "نویسنده", 90);
      table.AddNewColumn(Types.String, "باشگاه", 75);
      table.AddNewColumn(Types.String, "متن یادداشت", 220);
      table.AddNewColumn(Types.DateTime, "ایجاد", 90).DateTimeMode = DateTimeString.CompactDateTime;
      table.AddNewColumn(Types.String, "انقضا", 90);
      table.AddNewColumn(Types.String, "پاک شده", 50);
      table.OnRowDoubleClicked += NoteRequested;

      TextRenderer tr = new TextRenderer();
      foreach (DB.Game game in mGames)
        foreach(DB.ScheduleNote note in game.Notes)
          if(Session.CurrentStaff.IsPrivileged() || !note.Deleted)
          {
            Reports.Row row = table.AddNewRow(DB.GameManager.GetGameByID(note.GameID));
            row[0] = DB.User.GetUserFullName(note.UserID);
            row[1] = DB.Site.GetSiteName(game.SiteID);
            row[2] = TextRenderer.FitStringInWidth(note.Note, 220, table.Font);
            row[3] = note.TimeStamp;
            row[4] = note.Due == null ? " - " : note.Due.ToString(DateTimeString.CompactDateTime);
            row[5] = note.Deleted ? "بله" : "خیر";

            if (note.Deleted)
              row.BackBrush = mDeletedBrush;
            else if (note.Expired)
              row.BackBrush = mExpiredBrush;
            else
              row.BackBrush = mNormalBrush;
          }

      // finished
      report1.EndUpdate();
    }

    private void NoteRequested(Object sender, ReportRowArgs e)
    {
      FormGameNote frm = new FormGameNote();
      frm.Game = e.Data as DB.Game;
      ShowModal(frm, false);
    }
  }
}
