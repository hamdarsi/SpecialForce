﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Scheduling
{
  public partial class FormGameReserve : SessionAwareForm
  {
    private SessionAwareForm mChildForm = null;
    private ListBox mCurrentListBox = null;

    private DB.Game mGame;
    public DB.Game Game
    {
      set { mGame = value; }
    }

    public FormGameReserve()
    {
      InitializeComponent();

      cmbGameObject.Items.Add(Enums.ToString(GameObject.DeathMatch));
      cmbGameObject.Items.Add(Enums.ToString(GameObject.Bombing));
      cmbGameObject.SelectedIndex = 0;

      cmbFriendlyFire.SelectedIndex = Enums.IndexFromFriendlyFire(true);
      txtRestTime.Minimum = Session.ReleaseBuild ? 10 : 1;
    }

    private void lbFirstTeam_DragEnter(object sender, DragEventArgs e)
    {
      if (e.Data.GetDataPresent(typeof(DB.User)))
        e.Effect = DragDropEffects.Move;
      else if (e.Data.GetDataPresent(typeof(DB.Team)))
        e.Effect = DragDropEffects.Move;
    }

    private void AddUser(DB.User user, ListBox lb)
    {
      if(CheckPlayer(user, true))
        lb.Items.Add(user);
    }

    private void lbFirstTeam_DragDrop(object sender, DragEventArgs e)
    {
      ListBox lb = sender as ListBox;

      if (e.Data.GetDataPresent(typeof(DB.Team)))
      {
        lb.Items.Clear();
        DB.Team team = e.Data.GetData(typeof(DB.Team)) as DB.Team;
        foreach (DB.User user in team.ListMembers())
          AddUser(user, lb);
      }
      else if (e.Data.GetDataPresent(typeof(DB.User)))
        AddUser(e.Data.GetData(typeof(DB.User)) as DB.User, lb);

      btnReserve.Enabled = lbBlueTeam.Items.Count > 0 && lbGreenTeam.Items.Count > 0;
    }

    private void lbFirstTeam_KeyDown(object sender, KeyEventArgs e)
    {
      ListBox lb = (ListBox)sender;
      if (e.KeyCode == Keys.Delete && lb.SelectedItems.Count != 0)
        lb.Items.Remove(lb.SelectedItem);
    }

    private bool CheckPlayer(DB.User player, bool check_exists)
    {
      string strError;
      DB.UserSpecialTreat treat = player.GetCurrentSpecialTreat();
      if (check_exists && (lbGreenTeam.Items.Contains(player) || lbBlueTeam.Items.Contains(player)))
        strError = "این بازیکن در لیست بازیکنان این بازی وجود دارد.";

      else if (treat.IsValidBlackList())
      {
        strError = "این بازیکن در لیست سیاه قرار دارد.";
        strError += Environment.NewLine;
        strError += "دلیل: ";
        strError += treat.Reason;
      }

      else if (player.Fired)
      {
        strError = "این کاربر اخراج شده است. دلیل: ";
        strError += Environment.NewLine;
        strError += player.FireReason;
      }

      else if (treat.IsValidWhiteList())
        return true;

      else if (player.Credit >= mGame.AdmissionFee)
        return true;

      else
      {
        while (player.Credit < mGame.AdmissionFee)
        {
          strError = player.FullName + " به مقدار کافی شارژ ندارد.";
          strError += Environment.NewLine;
          strError += "شارژ فعلی: " + player.Credit.ToCurrencyString();
          strError += Environment.NewLine;
          strError += "شارژ مورد نیاز: " + mGame.AdmissionFee.ToCurrencyString();
          strError += Environment.NewLine;
          strError += "کسری: " + (mGame.AdmissionFee - player.Credit).ToCurrencyString();
          strError += Environment.NewLine;
          strError += Environment.NewLine;
          strError += "آیا میخواهید برای او شارژ کنید؟";
          if (!Session.Ask(strError))
            return false;

          Users.FormChargeUserAccount frm = new Users.FormChargeUserAccount();
          frm.User = player;
          frm.ShowDialog();
        }

        return true;
      }

      Session.ShowError(strError);
      return false;
    }

    private void btnReserve_Click(object sender, EventArgs e)
    {
      // Check for the length of the game
      double len = (double)(txtRoundCount.Value * txtRoundTime.Value * 60 +
                   (txtRoundCount.Value - 1) * txtRestTime.Value);
      double total = mGame.Start.GetTimeDifference(mGame.Finish);
      if (len > total)
      {
        Session.ShowError("مدت زمان اجرای بازی از زمان تعیین شده آن بیشتر میشود.");
        return;
      }

      // Check user credits to be enough for the game
      // And create a list of users to reserve the game for
      List<DB.User> blue_users = new List<DB.User>();
      List<DB.User> green_users = new List<DB.User>();

      foreach (Object o in lbBlueTeam.Items)
        if (CheckPlayer(o as DB.User, false))
          blue_users.Add(o as DB.User);
        else
          return;

      foreach (Object o in lbGreenTeam.Items)
        if (CheckPlayer(o as DB.User, false))
          green_users.Add(o as DB.User);
        else
          return;

      // Update game settings
      string strAsk = "برای بازی نرخ تعیین نشده است.";
      strAsk += Environment.NewLine;
      strAsk += "آیا میخواهید بازی را برای تست رزرو کنید؟";
      if (mGame.AdmissionFee == 0.0 && Session.CurrentStaff.IsPrivileged())
      {
        DialogResult res = Session.AskAGirl(strAsk, "سوال");
        if (res == System.Windows.Forms.DialogResult.Yes)
          mGame.Type = CompetitionType.Dummy;
        else if (res == System.Windows.Forms.DialogResult.No)
          mGame.Type = CompetitionType.FreeMatch;
        else
          return;
      }
      else
        mGame.Type = CompetitionType.FreeMatch;

      strAsk = "برای بازی نرخ تعیین نشده است.";
      strAsk += Environment.NewLine;
      strAsk += "بازی به صورت مجانی رزرو شد.";
      if (mGame.AdmissionFee == 0.0 && mGame.Type != CompetitionType.Dummy)
        Session.ShowMessage(strAsk);

      mGame.Object = Enums.ParseGameObject(cmbGameObject.SelectedItem.ToString());
      mGame.RoundCount = (int)txtRoundCount.Value;
      mGame.RoundTime = (int)txtRoundTime.Value * 60; // convert to seconds
      mGame.StartHP = (int)txtHP.Value;
      mGame.StartMagazine = (int)txtMagazineCount.Value;
      mGame.AmmoPerMagazine = (int)txtAmmoPerMagazine.Value;
      mGame.BombTime = (int)txtBombTimer.Value;
      mGame.RestTime = (int)txtRestTime.Value; // in minutes
      mGame.FriendlyFire = Enums.FriendlyFireFromString(cmbFriendlyFire.SelectedItem.ToString());

      // Reserve it.
      mGame.ReserveRegularMatch(blue_users, green_users);
      DialogResult = DialogResult.OK;
    }

    private void FormSessionReserve_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (mChildForm != null && mChildForm.IsDisposed == false)
      {
        mChildForm.Close();
        mChildForm.Dispose();
      }
    }

    private void tbAddUser_Click(object sender, EventArgs e)
    {
      mChildForm = Users.ControlPanel.Instance;
      mChildForm.ShowBounded(this);
    }

    private void tbAddTeam_Click(object sender, EventArgs e)
    {
      mChildForm = Teams.ControlPanel.Instance;
      mChildForm.ShowBounded(this);
    }

    private void tbRemoveUser_Click(object sender, EventArgs e)
    {
      if(mCurrentListBox != null)
        lbFirstTeam_KeyDown(mCurrentListBox, new KeyEventArgs(Keys.Delete));

      btnReserve.Enabled = lbBlueTeam.Items.Count > 0 && lbGreenTeam.Items.Count > 0;
    }

    private void lbGreenTeam_MouseDown(object sender, MouseEventArgs e)
    {
      mCurrentListBox = sender as ListBox;
    }

    private void cmbGameObject_SelectedIndexChanged(object sender, EventArgs e)
    {
      txtBombTimer.Enabled = Enums.ParseGameObject(cmbGameObject.SelectedItem.ToString()) == GameObject.Bombing;
      lblBombTimer.Enabled = txtBombTimer.Enabled;
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
