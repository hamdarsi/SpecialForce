﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Scheduling
{
  public partial class FormBatchPrice : SessionAwareForm
  {
    public FormBatchPrice()
    {
      InitializeComponent();
    }

    protected override void ApplyPermissions(Post post)
    {
      cmbSite.Enabled = Session.CurrentStaff.IsGod();
    }

    private void cmbSite_Enter(object sender, EventArgs e)
    {
      marker.Top = (sender as Control).Bottom - marker.Height - 5;
      marker.Parent = (sender as Control).Parent;
      Refresh();
    }

    private void FormBatchPrice_Shown(object sender, EventArgs e)
    {
      cmbSite.Focus();
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      if (txtAdmission.Value <= 0)
      {
        Session.ShowError("مبلغ ورودیه قابل قبول نیست.");
        return;
      }

      int count = 0;
      int site_id = (cmbSite.SelectedItem as DB.Site).ID;
      PersianDateTime start = PersianDateTime.Minimum.SetTime(tmStart.Time);
      PersianDateTime finish = PersianDateTime.Minimum.SetTime(tmEnd.Time);
      List<DB.Game> games = DB.GameManager.GetGames(dtStart.Date, dtEnd.Date.NextDay, site_id);
      foreach(DB.Game game in games)
      {
        if (game.State != GameState.Free)
          continue;

        if (game.AdmissionFee != 0)
          continue;

        PersianDateTime gstart = PersianDateTime.Minimum.SetTime(game.Start.Hour, game.Start.Minute, game.Start.Second);
        PersianDateTime gfinish = PersianDateTime.Minimum.SetTime(game.Finish.Hour, game.Finish.Minute, game.Finish.Second);
        if ((gstart >= start && gstart <= finish) ||
           (gfinish >= start && gfinish <= finish) ||
           (gstart < start && gfinish > finish))
        {
          ++count;
          game.AdmissionFee = txtAdmission.Value;
        }
      }

      Session.ShowMessage("ورودیه " + count.ToString() + " بازی به مقدار تنظیم شده تغییر یافت.");
      DialogResult = DialogResult.OK;
    }
  }
}
