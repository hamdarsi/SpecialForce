﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SpecialForce.Scheduling
{
  [ToolboxItem(false)]
  public class ScheduleEntry : ToolButton
  {
    #region Internal Data
    private bool mResizing = false;
    private Point mLastClickPosition = Point.Empty;
    private Font mNazanin = new Font("B Nazanin", 12.0f);
    #endregion


    #region Properties
    private Color mTextColor = Color.Yellow;
    [DefaultValue(typeof(Color), "0xFFFFFF00")]
    public Color TextColor
    {
      get { return mTextColor; }
      set { mTextColor = value; NeedsRefresh(); }
    }

    private DB.Game mGame;
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public DB.Game Game
    {
      get { return mGame; }
      set { AssignGame(value); }
    }

    private DB.CandidateMatch mCandidateMatch;
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public DB.CandidateMatch CandidateMatch
    {
      get { return mCandidateMatch; }
      set { mCandidateMatch = value; }
    }

    private bool Resizable
    {
      get 
      {
        if (mGame == null)
          return false;
        else if (mGame.State != GameState.Free && mGame.State != GameState.Reserved)
          return false;

        return true;
      }
    }

    private bool Draggable
    {
      get
      {
        if (mGame == null)
          return true;
        else if (mGame.CompetitionID == -1)
          return mGame.State == GameState.Free || mGame.State == GameState.Reserved;
        else // session is reserved for a competition
          return mGame.State == GameState.Reserved;
      }
    }
    #endregion Properties


    #region Events
    public event EventHandler<ScheduleEntryResizeArgs> OnEntryResizing;
    public event EventHandler<ScheduleEntryResizeArgs> OnEntryResized;
    #endregion Events


    #region Constructor
    public ScheduleEntry()
    {
    }
    #endregion Constructor


    #region Utilities
    /// <summary>
    /// This method is used to return a rectangle showing 
    /// the resized information of the entry
    /// </summary>
    /// <param name="new_x">The new X coordinate of the entry</param>
    /// <returns>A rectrangle holding new coordinates of the entry</returns>
    private ScheduleEntryResizeArgs GetResizedRect(int new_x)
    {
      new_x -= mLastClickPosition.X;
      Rectangle rect = new Rectangle(new_x, 0, Width - new_x, Height);
      rect = RectangleToScreen(rect);
      return new ScheduleEntryResizeArgs(rect);
    }

    /// <summary>
    /// This method assigns the given game to entry
    /// </summary>
    /// <param name="game"></param>
    private void AssignGame(DB.Game game)
    {
      if (mGame != null)
        mGame.OnAttributeUpdate -= GameUpdated;

      mGame = game;
      if(mGame != null)
        mGame.OnAttributeUpdate += GameUpdated;

      NeedsRefresh();
    }
    #endregion Utilities


    #region Event Handlers
    private void GameUpdated(object sender, EventArgs e)
    {
      NeedsRefresh();
    }

    protected override void OnSizeChanged(EventArgs e)
    {
      NeedsRefresh();
    }

    protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
    {
      mLastClickPosition = e.Location;
      if (e.X >= 5 || e.Button != MouseButtons.Left)
        base.OnMouseDown(e);
      else if (!mResizing && Resizable)
        mResizing = true;
    }

    protected override void OnMouseMove(System.Windows.Forms.MouseEventArgs e)
    {
      if (mResizing)
      {
        EventHandler<ScheduleEntryResizeArgs> ev = OnEntryResizing;
        if (ev != null)
          ev(this, GetResizedRect(e.X));
      }
      else if (State == ButtonState.Pressed && Draggable)
      {
        double dx = mLastClickPosition.X - e.X;
        double dy = mLastClickPosition.Y - e.Y;

        if (dx * dx + dy * dy >= 100)
        {
          DoDragDrop(this, DragDropEffects.Move);
          State = ButtonState.Normal;
        }
      }
      else if(Resizable)
        Cursor = e.X < 5 ? Cursors.SizeWE : Cursors.Arrow;
    }

    protected override void OnMouseUp(MouseEventArgs e)
    {
      if(mResizing)
      {
        EventHandler<ScheduleEntryResizeArgs> ev = OnEntryResized;
        if (ev != null)
          ev(this, GetResizedRect(e.X));

        mResizing = false;
      }
      else
        base.OnMouseUp(e);
    }
    #endregion Event Handlers


    #region Graphical Procedures
    /// <summary>
    /// This method creates the entry's graphical image and refreshes it.
    /// </summary>
    public void NeedsRefresh()
    {
      CreateEntry();
      RequestRepaint();
    }

    /// <summary>
    /// This method creates the graphical image. To Do List:
    /// Step 1: Create resources
    /// Step 2: Set text, background brush and hint
    /// Step 3: Paint the control
    /// Step 4: Add additional information to the hint
    /// </summary>
    private void CreateEntry()
    {
      // Create resources
      if (Parent == null || Width <= 0 || Height <= 0)
        return;

      try
      {
        Bitmap bmp = new Bitmap(Width, Height);
        Graphics canvas = Graphics.FromImage(bmp);
        VisualRenderer vr = new VisualRenderer(this, ClientRectangle);
        vr.RenderToTexture(canvas);
        TextRenderer tr = new TextRenderer();
        string strHint = "اطلاعات بازی:" + Environment.NewLine;

        // Assign properties
        tr.Canvas = canvas;
        tr.Font = mNazanin;
        tr.Color = mTextColor;
        tr.Size = Size;
        tr.Justify = Alignment.MiddleMiddle;
        Brush back;
        if (mGame == null)
        {
          tr.Text = "جدید";
          back = new SolidBrush(Color.FromArgb(90, 254, 255, 255));
          strHint += "بدون بازی";
        }
        else if (mGame.State == GameState.Free)
        {
          tr.Text = "آزاد";
          back = new SolidBrush(Color.FromArgb(90, 254, 255, 255));
          strHint += "سانس آماده برای رزرو";
        }
        else if (mGame.State == GameState.Reserved)
        {
          tr.Text = "رزرو";
          back = new SolidBrush(Color.FromArgb(30, 0, 255, 0));
          strHint += "سانس رزرو شده است";
        }
        else if (mGame.State == GameState.InProgress)
        {
          tr.Text = "اجرا";
          back = new SolidBrush(Color.FromArgb(30, 0, 0, 255));
          strHint += "بازی این سانس در حال اجراست";
        }
        else if (mGame.State == GameState.Finished)
        {
          tr.Text = "تمام";
          back = new SolidBrush(Color.FromArgb(255, 0, 0, 0));
          strHint += "بازی این سانس به اتمام رسیده است";
        }
        else
        {
          tr.Text = "کنسل";
          back = new HatchBrush(HatchStyle.DarkDownwardDiagonal, Color.FromArgb(150, 255, 255, 255), Color.FromArgb(0, 0, 0, 0));
          strHint += "بازی این سانس کنسل شده است";
        }

        canvas.FillRoundedRectangle(back, ClientRectangle, 2);
        tr.Draw();

        if (mGame != null)
        {
          // Any notes exists for the game?
          if (mGame.HasValidNotes())
          {
            Image img = Properties.Resources.Scheduling_Game_Has_Notes;
            canvas.DrawImageUnscaled(Width - img.Width, Height - img.Height, img);
          }

          strHint += Environment.NewLine;
          strHint += "شروع: ";
          strHint += mGame.Start.ToString(DateTimeString.CompactDateTime);
          strHint += Environment.NewLine;
          strHint += "پایان: ";
          strHint += mGame.Finish.ToString(DateTimeString.CompactDateTime);

          strHint += Environment.NewLine;
          strHint += "نرخ ورودیه: ";
          strHint += mGame.AdmissionFee.ToCurrencyString();
          strHint += " تومان به ازای هر ";
          strHint += mGame.CompetitionID == -1 ? "نفر" : "تیم";

          if (mGame.State != GameState.Free)
          {
            strHint += Environment.NewLine;
            strHint += "نوع مسابقه: ";
            strHint += Enums.ToString(mGame.Type);
          }

          if (mGame.CompetitionID != -1)
          {
            DB.Competition c = mGame.Competition;
            DB.CandidateMatch cm = mGame.CandidateMatch;

            strHint += Environment.NewLine;
            strHint += "نام مسابقه: ";
            strHint += c.Title;

            if (c.Type == CompetitionType.Elimination)
            {
              strHint += Environment.NewLine;
              strHint += "مرحله شماره ";
              strHint += (cm.Stage + 1);
            }
            else if (c.Type == CompetitionType.Cup)
            {
              if (cm.Stage == 0)
              {
                strHint += Environment.NewLine;
                strHint += "دور گروهی: ";
                strHint += DB.CodeName.CreateGroupName(cm.Group);
              }
              else if (cm.IsFinalMatch)
              {
                strHint += Environment.NewLine;
                strHint += "فینال";
              }
              else if (cm.IsLosersFinal)
              {
                strHint += Environment.NewLine;
                strHint += "رده بندی";
              }
              else
              {
                strHint += Environment.NewLine;
                strHint += "دور حذفی مرحله ";
                strHint += cm.Stage;
              }
            }
          }

          if (mGame.BlueTeamID != -1 && mGame.GreenTeamID != -1)
          {
            strHint += Environment.NewLine;
            strHint += "بین ";
            strHint += mGame.GetBlueTeamName();
            strHint += " و ";
            strHint += mGame.GetGreenTeamName();
          }
        }

        Hint = strHint;

        // Assign the image
        Image = bmp;
        canvas.Dispose();
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "ScheduleEntry", "CreateEntry");
      }
    }
    #endregion Graphical Procedures
  }
}
