﻿namespace SpecialForce.Scheduling
{
  partial class FormBatchPrice
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBatchPrice));
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnOK = new System.Windows.Forms.Button();
      this.toolBar2 = new SpecialForce.ToolBar();
      this.tmEnd = new SpecialForce.PersianTimePicker();
      this.tmStart = new SpecialForce.PersianTimePicker();
      this.transparentLabel10 = new SpecialForce.TransparentLabel();
      this.transparentLabel7 = new SpecialForce.TransparentLabel();
      this.transparentLabel8 = new SpecialForce.TransparentLabel();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.transparentLabel9 = new SpecialForce.TransparentLabel();
      this.dtStart = new SpecialForce.PersianDatePicker();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.dtEnd = new SpecialForce.PersianDatePicker();
      this.transparentLabel4 = new SpecialForce.TransparentLabel();
      this.pnlDateSite = new SpecialForce.ToolBar();
      this.marker = new SpecialForce.ToolButton();
      this.transparentLabel12 = new SpecialForce.TransparentLabel();
      this.txtAdmission = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel11 = new SpecialForce.TransparentLabel();
      this.cmbSite = new System.Windows.Forms.ComboBox();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.toolBar2.SuspendLayout();
      this.toolBar1.SuspendLayout();
      this.pnlDateSite.SuspendLayout();
      this.SuspendLayout();
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(140, 367);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 7;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // btnOK
      // 
      this.btnOK.ForeColor = System.Drawing.Color.Black;
      this.btnOK.Location = new System.Drawing.Point(221, 367);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 6;
      this.btnOK.Text = "تنظیم";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // toolBar2
      // 
      this.toolBar2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar2.BorderWidth = 0.5F;
      this.toolBar2.Controls.Add(this.tmEnd);
      this.toolBar2.Controls.Add(this.tmStart);
      this.toolBar2.Controls.Add(this.transparentLabel10);
      this.toolBar2.Controls.Add(this.transparentLabel7);
      this.toolBar2.Controls.Add(this.transparentLabel8);
      this.toolBar2.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar2.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar2.Location = new System.Drawing.Point(10, 243);
      this.toolBar2.Name = "toolBar2";
      this.toolBar2.Size = new System.Drawing.Size(286, 118);
      this.toolBar2.TabIndex = 45;
      // 
      // tmEnd
      // 
      this.tmEnd.Location = new System.Drawing.Point(17, 68);
      this.tmEnd.Name = "tmEnd";
      this.tmEnd.ShowSecond = false;
      this.tmEnd.Size = new System.Drawing.Size(79, 44);
      this.tmEnd.TabIndex = 5;
      this.tmEnd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tmEnd.Texts")));
      this.tmEnd.Enter += new System.EventHandler(this.cmbSite_Enter);
      // 
      // tmStart
      // 
      this.tmStart.Location = new System.Drawing.Point(17, 25);
      this.tmStart.Name = "tmStart";
      this.tmStart.ShowSecond = false;
      this.tmStart.Size = new System.Drawing.Size(79, 44);
      this.tmStart.TabIndex = 4;
      this.tmStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tmStart.Texts")));
      this.tmStart.Enter += new System.EventHandler(this.cmbSite_Enter);
      // 
      // transparentLabel10
      // 
      this.transparentLabel10.Location = new System.Drawing.Point(126, 7);
      this.transparentLabel10.Name = "transparentLabel10";
      this.transparentLabel10.Size = new System.Drawing.Size(74, 15);
      this.transparentLabel10.TabIndex = 9;
      this.transparentLabel10.TabStop = false;
      this.transparentLabel10.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel10.Texts")));
      // 
      // transparentLabel7
      // 
      this.transparentLabel7.Location = new System.Drawing.Point(198, 48);
      this.transparentLabel7.Name = "transparentLabel7";
      this.transparentLabel7.Size = new System.Drawing.Size(59, 15);
      this.transparentLabel7.TabIndex = 2;
      this.transparentLabel7.TabStop = false;
      this.transparentLabel7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel7.Texts")));
      // 
      // transparentLabel8
      // 
      this.transparentLabel8.Location = new System.Drawing.Point(205, 91);
      this.transparentLabel8.Name = "transparentLabel8";
      this.transparentLabel8.Size = new System.Drawing.Size(52, 15);
      this.transparentLabel8.TabIndex = 5;
      this.transparentLabel8.TabStop = false;
      this.transparentLabel8.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel8.Texts")));
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 0.5F;
      this.toolBar1.Controls.Add(this.transparentLabel9);
      this.toolBar1.Controls.Add(this.dtStart);
      this.toolBar1.Controls.Add(this.transparentLabel2);
      this.toolBar1.Controls.Add(this.dtEnd);
      this.toolBar1.Controls.Add(this.transparentLabel4);
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(10, 115);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(286, 120);
      this.toolBar1.TabIndex = 44;
      // 
      // transparentLabel9
      // 
      this.transparentLabel9.Location = new System.Drawing.Point(128, 5);
      this.transparentLabel9.Name = "transparentLabel9";
      this.transparentLabel9.Size = new System.Drawing.Size(72, 15);
      this.transparentLabel9.TabIndex = 8;
      this.transparentLabel9.TabStop = false;
      this.transparentLabel9.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel9.Texts")));
      // 
      // dtStart
      // 
      this.dtStart.ActivateEvent = true;
      this.dtStart.Location = new System.Drawing.Point(15, 25);
      this.dtStart.Name = "dtStart";
      this.dtStart.Size = new System.Drawing.Size(132, 37);
      this.dtStart.TabIndex = 2;
      this.dtStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtStart.Texts")));
      this.dtStart.Enter += new System.EventHandler(this.cmbSite_Enter);
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Location = new System.Drawing.Point(197, 43);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(60, 15);
      this.transparentLabel2.TabIndex = 2;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // dtEnd
      // 
      this.dtEnd.ActivateEvent = true;
      this.dtEnd.Location = new System.Drawing.Point(15, 70);
      this.dtEnd.Name = "dtEnd";
      this.dtEnd.Size = new System.Drawing.Size(132, 37);
      this.dtEnd.TabIndex = 3;
      this.dtEnd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtEnd.Texts")));
      this.dtEnd.Enter += new System.EventHandler(this.cmbSite_Enter);
      // 
      // transparentLabel4
      // 
      this.transparentLabel4.Location = new System.Drawing.Point(204, 88);
      this.transparentLabel4.Name = "transparentLabel4";
      this.transparentLabel4.Size = new System.Drawing.Size(53, 15);
      this.transparentLabel4.TabIndex = 5;
      this.transparentLabel4.TabStop = false;
      this.transparentLabel4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel4.Texts")));
      // 
      // pnlDateSite
      // 
      this.pnlDateSite.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlDateSite.BorderWidth = 0.5F;
      this.pnlDateSite.Controls.Add(this.marker);
      this.pnlDateSite.Controls.Add(this.transparentLabel12);
      this.pnlDateSite.Controls.Add(this.txtAdmission);
      this.pnlDateSite.Controls.Add(this.transparentLabel11);
      this.pnlDateSite.Controls.Add(this.cmbSite);
      this.pnlDateSite.Controls.Add(this.transparentLabel1);
      this.pnlDateSite.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlDateSite.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlDateSite.Location = new System.Drawing.Point(10, 12);
      this.pnlDateSite.Name = "pnlDateSite";
      this.pnlDateSite.Size = new System.Drawing.Size(286, 97);
      this.pnlDateSite.TabIndex = 43;
      // 
      // marker
      // 
      this.marker.Enabled = false;
      this.marker.Hint = "";
      this.marker.Image = ((System.Drawing.Bitmap)(resources.GetObject("marker.Image")));
      this.marker.Location = new System.Drawing.Point(264, 36);
      this.marker.Name = "marker";
      this.marker.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.marker.ShowBorder = false;
      this.marker.Size = new System.Drawing.Size(16, 16);
      this.marker.TabIndex = 12;
      this.marker.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("marker.Texts")));
      this.marker.TransparentColor = System.Drawing.Color.Transparent;
      // 
      // transparentLabel12
      // 
      this.transparentLabel12.Location = new System.Drawing.Point(17, 67);
      this.transparentLabel12.Name = "transparentLabel12";
      this.transparentLabel12.Size = new System.Drawing.Size(28, 15);
      this.transparentLabel12.TabIndex = 11;
      this.transparentLabel12.TabStop = false;
      this.transparentLabel12.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel12.Texts")));
      // 
      // txtAdmission
      // 
      this.txtAdmission.Location = new System.Drawing.Point(50, 64);
      this.txtAdmission.Mode = SpecialForce.Utilities.NumberBox.NumberMode.Currency;
      this.txtAdmission.Name = "txtAdmission";
      this.txtAdmission.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtAdmission.Size = new System.Drawing.Size(69, 21);
      this.txtAdmission.TabIndex = 1;
      this.txtAdmission.Text = "$0.00";
      this.txtAdmission.Enter += new System.EventHandler(this.cmbSite_Enter);
      // 
      // transparentLabel11
      // 
      this.transparentLabel11.Location = new System.Drawing.Point(202, 67);
      this.transparentLabel11.Name = "transparentLabel11";
      this.transparentLabel11.Size = new System.Drawing.Size(55, 15);
      this.transparentLabel11.TabIndex = 9;
      this.transparentLabel11.TabStop = false;
      this.transparentLabel11.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel11.Texts")));
      // 
      // cmbSite
      // 
      this.cmbSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbSite.FormattingEnabled = true;
      this.cmbSite.Location = new System.Drawing.Point(17, 32);
      this.cmbSite.Name = "cmbSite";
      this.cmbSite.Size = new System.Drawing.Size(102, 21);
      this.cmbSite.TabIndex = 0;
      this.cmbSite.Enter += new System.EventHandler(this.cmbSite_Enter);
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Location = new System.Drawing.Point(41, 11);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(224, 15);
      this.transparentLabel1.TabIndex = 0;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // FormBatchPrice
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(307, 397);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOK);
      this.Controls.Add(this.toolBar2);
      this.Controls.Add(this.toolBar1);
      this.Controls.Add(this.pnlDateSite);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormBatchPrice";
      this.Text = "تنظیم نرخ بازی ها";
      this.Shown += new System.EventHandler(this.FormBatchPrice_Shown);
      this.toolBar2.ResumeLayout(false);
      this.toolBar1.ResumeLayout(false);
      this.pnlDateSite.ResumeLayout(false);
      this.pnlDateSite.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private ToolBar pnlDateSite;
    private TransparentLabel transparentLabel2;
    private TransparentLabel transparentLabel1;
    private TransparentLabel transparentLabel12;
    private TransparentLabel transparentLabel11;
    private TransparentLabel transparentLabel4;
    private ToolBar toolBar1;
    private TransparentLabel transparentLabel9;
    private ToolBar toolBar2;
    private TransparentLabel transparentLabel10;
    private TransparentLabel transparentLabel7;
    private TransparentLabel transparentLabel8;
    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.Button btnCancel;
    private ToolButton marker;
    public PersianDatePicker dtStart;
    public System.Windows.Forms.ComboBox cmbSite;
    public Utilities.NumberBox txtAdmission;
    public PersianDatePicker dtEnd;
    public PersianTimePicker tmEnd;
    public PersianTimePicker tmStart;
  }
}