﻿namespace SpecialForce.Scheduling
{
  partial class FormGameReserve
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGameReserve));
      this.btnReserve = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.lbBlueTeam = new System.Windows.Forms.ListBox();
      this.lbGreenTeam = new System.Windows.Forms.ListBox();
      this.label2 = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.cmbFriendlyFire = new System.Windows.Forms.ComboBox();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.txtRestTime = new System.Windows.Forms.NumericUpDown();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.cmbGameObject = new System.Windows.Forms.ComboBox();
      this.txtBombTimer = new System.Windows.Forms.NumericUpDown();
      this.lblBombTimer = new SpecialForce.TransparentLabel();
      this.txtRoundCount = new System.Windows.Forms.NumericUpDown();
      this.txtRoundTime = new System.Windows.Forms.NumericUpDown();
      this.txtMagazineCount = new System.Windows.Forms.NumericUpDown();
      this.txtAmmoPerMagazine = new System.Windows.Forms.NumericUpDown();
      this.txtHP = new System.Windows.Forms.NumericUpDown();
      this.lblHP = new SpecialForce.TransparentLabel();
      this.label4 = new SpecialForce.TransparentLabel();
      this.lblRoundTime = new SpecialForce.TransparentLabel();
      this.lblAmmoPerMagazine = new SpecialForce.TransparentLabel();
      this.lblMagazineCount = new SpecialForce.TransparentLabel();
      this.tbAddTeam = new SpecialForce.ToolButton();
      this.tbAddUser = new SpecialForce.ToolButton();
      this.tbRemoveUser = new SpecialForce.ToolButton();
      this.toolBar1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtRestTime)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtBombTimer)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtRoundCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtRoundTime)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtMagazineCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtAmmoPerMagazine)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtHP)).BeginInit();
      this.SuspendLayout();
      // 
      // btnReserve
      // 
      this.btnReserve.Enabled = false;
      this.btnReserve.ForeColor = System.Drawing.Color.Black;
      this.btnReserve.Location = new System.Drawing.Point(128, 275);
      this.btnReserve.Name = "btnReserve";
      this.btnReserve.Size = new System.Drawing.Size(75, 23);
      this.btnReserve.TabIndex = 12;
      this.btnReserve.Text = "رزرو";
      this.btnReserve.UseVisualStyleBackColor = true;
      this.btnReserve.Click += new System.EventHandler(this.btnReserve_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(12, 275);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 11;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // lbBlueTeam
      // 
      this.lbBlueTeam.AllowDrop = true;
      this.lbBlueTeam.FormattingEnabled = true;
      this.lbBlueTeam.Location = new System.Drawing.Point(214, 112);
      this.lbBlueTeam.Name = "lbBlueTeam";
      this.lbBlueTeam.Size = new System.Drawing.Size(100, 186);
      this.lbBlueTeam.TabIndex = 17;
      this.lbBlueTeam.DragDrop += new System.Windows.Forms.DragEventHandler(this.lbFirstTeam_DragDrop);
      this.lbBlueTeam.DragEnter += new System.Windows.Forms.DragEventHandler(this.lbFirstTeam_DragEnter);
      this.lbBlueTeam.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbFirstTeam_KeyDown);
      this.lbBlueTeam.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbGreenTeam_MouseDown);
      // 
      // lbGreenTeam
      // 
      this.lbGreenTeam.AllowDrop = true;
      this.lbGreenTeam.FormattingEnabled = true;
      this.lbGreenTeam.Location = new System.Drawing.Point(333, 112);
      this.lbGreenTeam.Name = "lbGreenTeam";
      this.lbGreenTeam.Size = new System.Drawing.Size(100, 186);
      this.lbGreenTeam.TabIndex = 18;
      this.lbGreenTeam.DragDrop += new System.Windows.Forms.DragEventHandler(this.lbFirstTeam_DragDrop);
      this.lbGreenTeam.DragEnter += new System.Windows.Forms.DragEventHandler(this.lbFirstTeam_DragEnter);
      this.lbGreenTeam.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbFirstTeam_KeyDown);
      this.lbGreenTeam.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbGreenTeam_MouseDown);
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(211, 88);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(68, 15);
      this.label2.TabIndex = 19;
      this.label2.TabStop = false;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(331, 88);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(71, 15);
      this.label3.TabIndex = 20;
      this.label3.TabStop = false;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 0.5F;
      this.toolBar1.Controls.Add(this.cmbFriendlyFire);
      this.toolBar1.Controls.Add(this.transparentLabel2);
      this.toolBar1.Controls.Add(this.txtRestTime);
      this.toolBar1.Controls.Add(this.transparentLabel1);
      this.toolBar1.Controls.Add(this.label1);
      this.toolBar1.Controls.Add(this.cmbGameObject);
      this.toolBar1.Controls.Add(this.txtBombTimer);
      this.toolBar1.Controls.Add(this.lblBombTimer);
      this.toolBar1.Controls.Add(this.txtRoundCount);
      this.toolBar1.Controls.Add(this.txtRoundTime);
      this.toolBar1.Controls.Add(this.txtMagazineCount);
      this.toolBar1.Controls.Add(this.txtAmmoPerMagazine);
      this.toolBar1.Controls.Add(this.txtHP);
      this.toolBar1.Controls.Add(this.lblHP);
      this.toolBar1.Controls.Add(this.label4);
      this.toolBar1.Controls.Add(this.lblRoundTime);
      this.toolBar1.Controls.Add(this.lblAmmoPerMagazine);
      this.toolBar1.Controls.Add(this.lblMagazineCount);
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(12, 12);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(191, 252);
      this.toolBar1.TabIndex = 42;
      // 
      // cmbFriendlyFire
      // 
      this.cmbFriendlyFire.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbFriendlyFire.FormattingEnabled = true;
      this.cmbFriendlyFire.Items.AddRange(new object[] {
            "بله",
            "خیر"});
      this.cmbFriendlyFire.Location = new System.Drawing.Point(11, 223);
      this.cmbFriendlyFire.Name = "cmbFriendlyFire";
      this.cmbFriendlyFire.Size = new System.Drawing.Size(56, 21);
      this.cmbFriendlyFire.TabIndex = 39;
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Location = new System.Drawing.Point(119, 227);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(64, 15);
      this.transparentLabel2.TabIndex = 38;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // txtRestTime
      // 
      this.txtRestTime.Location = new System.Drawing.Point(11, 196);
      this.txtRestTime.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
      this.txtRestTime.Name = "txtRestTime";
      this.txtRestTime.Size = new System.Drawing.Size(56, 21);
      this.txtRestTime.TabIndex = 37;
      this.txtRestTime.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Location = new System.Drawing.Point(107, 200);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(75, 15);
      this.transparentLabel1.TabIndex = 36;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(134, 11);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(47, 15);
      this.label1.TabIndex = 35;
      this.label1.TabStop = false;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // cmbGameObject
      // 
      this.cmbGameObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbGameObject.FormattingEnabled = true;
      this.cmbGameObject.Location = new System.Drawing.Point(11, 7);
      this.cmbGameObject.Name = "cmbGameObject";
      this.cmbGameObject.Size = new System.Drawing.Size(103, 21);
      this.cmbGameObject.TabIndex = 34;
      this.cmbGameObject.SelectedIndexChanged += new System.EventHandler(this.cmbGameObject_SelectedIndexChanged);
      // 
      // txtBombTimer
      // 
      this.txtBombTimer.Enabled = false;
      this.txtBombTimer.Location = new System.Drawing.Point(11, 169);
      this.txtBombTimer.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
      this.txtBombTimer.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
      this.txtBombTimer.Name = "txtBombTimer";
      this.txtBombTimer.Size = new System.Drawing.Size(56, 21);
      this.txtBombTimer.TabIndex = 33;
      this.txtBombTimer.Value = new decimal(new int[] {
            180,
            0,
            0,
            0});
      // 
      // lblBombTimer
      // 
      this.lblBombTimer.Enabled = false;
      this.lblBombTimer.Location = new System.Drawing.Point(99, 173);
      this.lblBombTimer.Name = "lblBombTimer";
      this.lblBombTimer.Size = new System.Drawing.Size(83, 15);
      this.lblBombTimer.TabIndex = 32;
      this.lblBombTimer.TabStop = false;
      this.lblBombTimer.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBombTimer.Texts")));
      // 
      // txtRoundCount
      // 
      this.txtRoundCount.Location = new System.Drawing.Point(11, 34);
      this.txtRoundCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtRoundCount.Name = "txtRoundCount";
      this.txtRoundCount.Size = new System.Drawing.Size(56, 21);
      this.txtRoundCount.TabIndex = 31;
      this.txtRoundCount.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
      // 
      // txtRoundTime
      // 
      this.txtRoundTime.Location = new System.Drawing.Point(11, 61);
      this.txtRoundTime.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
      this.txtRoundTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtRoundTime.Name = "txtRoundTime";
      this.txtRoundTime.Size = new System.Drawing.Size(56, 21);
      this.txtRoundTime.TabIndex = 30;
      this.txtRoundTime.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
      // 
      // txtMagazineCount
      // 
      this.txtMagazineCount.Location = new System.Drawing.Point(11, 115);
      this.txtMagazineCount.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
      this.txtMagazineCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtMagazineCount.Name = "txtMagazineCount";
      this.txtMagazineCount.Size = new System.Drawing.Size(56, 21);
      this.txtMagazineCount.TabIndex = 29;
      this.txtMagazineCount.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
      // 
      // txtAmmoPerMagazine
      // 
      this.txtAmmoPerMagazine.Location = new System.Drawing.Point(11, 142);
      this.txtAmmoPerMagazine.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
      this.txtAmmoPerMagazine.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtAmmoPerMagazine.Name = "txtAmmoPerMagazine";
      this.txtAmmoPerMagazine.Size = new System.Drawing.Size(56, 21);
      this.txtAmmoPerMagazine.TabIndex = 28;
      this.txtAmmoPerMagazine.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
      // 
      // txtHP
      // 
      this.txtHP.Increment = new decimal(new int[] {
            3,
            0,
            0,
            0});
      this.txtHP.Location = new System.Drawing.Point(11, 88);
      this.txtHP.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtHP.Name = "txtHP";
      this.txtHP.Size = new System.Drawing.Size(56, 21);
      this.txtHP.TabIndex = 27;
      this.txtHP.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
      // 
      // lblHP
      // 
      this.lblHP.Location = new System.Drawing.Point(101, 92);
      this.lblHP.Name = "lblHP";
      this.lblHP.Size = new System.Drawing.Size(82, 15);
      this.lblHP.TabIndex = 26;
      this.lblHP.TabStop = false;
      this.lblHP.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblHP.Texts")));
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(121, 38);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(61, 15);
      this.label4.TabIndex = 25;
      this.label4.TabStop = false;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // lblRoundTime
      // 
      this.lblRoundTime.Location = new System.Drawing.Point(94, 65);
      this.lblRoundTime.Name = "lblRoundTime";
      this.lblRoundTime.Size = new System.Drawing.Size(89, 15);
      this.lblRoundTime.TabIndex = 24;
      this.lblRoundTime.TabStop = false;
      this.lblRoundTime.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblRoundTime.Texts")));
      // 
      // lblAmmoPerMagazine
      // 
      this.lblAmmoPerMagazine.Location = new System.Drawing.Point(77, 146);
      this.lblAmmoPerMagazine.Name = "lblAmmoPerMagazine";
      this.lblAmmoPerMagazine.Size = new System.Drawing.Size(106, 15);
      this.lblAmmoPerMagazine.TabIndex = 23;
      this.lblAmmoPerMagazine.TabStop = false;
      this.lblAmmoPerMagazine.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblAmmoPerMagazine.Texts")));
      // 
      // lblMagazineCount
      // 
      this.lblMagazineCount.Location = new System.Drawing.Point(102, 119);
      this.lblMagazineCount.Name = "lblMagazineCount";
      this.lblMagazineCount.Size = new System.Drawing.Size(81, 15);
      this.lblMagazineCount.TabIndex = 22;
      this.lblMagazineCount.TabStop = false;
      this.lblMagazineCount.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblMagazineCount.Texts")));
      // 
      // tbAddTeam
      // 
      this.tbAddTeam.Hint = "افزودن تیم";
      this.tbAddTeam.Image = ((System.Drawing.Bitmap)(resources.GetObject("tbAddTeam.Image")));
      this.tbAddTeam.Location = new System.Drawing.Point(214, 9);
      this.tbAddTeam.Name = "tbAddTeam";
      this.tbAddTeam.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbAddTeam.Size = new System.Drawing.Size(64, 64);
      this.tbAddTeam.TabIndex = 43;
      this.tbAddTeam.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tbAddTeam.Texts")));
      this.tbAddTeam.TransparentColor = System.Drawing.Color.White;
      this.tbAddTeam.Click += new System.EventHandler(this.tbAddTeam_Click);
      // 
      // tbAddUser
      // 
      this.tbAddUser.Hint = "افزودن کاربر";
      this.tbAddUser.Image = ((System.Drawing.Bitmap)(resources.GetObject("tbAddUser.Image")));
      this.tbAddUser.Location = new System.Drawing.Point(291, 9);
      this.tbAddUser.Name = "tbAddUser";
      this.tbAddUser.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbAddUser.Size = new System.Drawing.Size(64, 64);
      this.tbAddUser.TabIndex = 44;
      this.tbAddUser.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tbAddUser.Texts")));
      this.tbAddUser.TransparentColor = System.Drawing.Color.White;
      this.tbAddUser.Click += new System.EventHandler(this.tbAddUser_Click);
      // 
      // tbRemoveUser
      // 
      this.tbRemoveUser.Hint = "حذف کاربر";
      this.tbRemoveUser.Image = ((System.Drawing.Bitmap)(resources.GetObject("tbRemoveUser.Image")));
      this.tbRemoveUser.Location = new System.Drawing.Point(368, 9);
      this.tbRemoveUser.Name = "tbRemoveUser";
      this.tbRemoveUser.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbRemoveUser.Size = new System.Drawing.Size(64, 64);
      this.tbRemoveUser.TabIndex = 45;
      this.tbRemoveUser.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tbRemoveUser.Texts")));
      this.tbRemoveUser.TransparentColor = System.Drawing.Color.White;
      this.tbRemoveUser.Click += new System.EventHandler(this.tbRemoveUser_Click);
      // 
      // FormGameReserve
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(445, 311);
      this.Controls.Add(this.tbRemoveUser);
      this.Controls.Add(this.tbAddUser);
      this.Controls.Add(this.tbAddTeam);
      this.Controls.Add(this.toolBar1);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.lbGreenTeam);
      this.Controls.Add(this.lbBlueTeam);
      this.Controls.Add(this.btnReserve);
      this.Controls.Add(this.btnCancel);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormGameReserve";
      this.Text = "رزرو بازی";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSessionReserve_FormClosing);
      this.toolBar1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.txtRestTime)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtBombTimer)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtRoundCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtRoundTime)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtMagazineCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtAmmoPerMagazine)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtHP)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnReserve;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.ListBox lbBlueTeam;
    private System.Windows.Forms.ListBox lbGreenTeam;
    private SpecialForce.TransparentLabel label2;
    private SpecialForce.TransparentLabel label3;
    private ToolBar toolBar1;
    private TransparentLabel label1;
    private System.Windows.Forms.ComboBox cmbGameObject;
    private System.Windows.Forms.NumericUpDown txtBombTimer;
    private TransparentLabel lblBombTimer;
    private System.Windows.Forms.NumericUpDown txtRoundCount;
    private System.Windows.Forms.NumericUpDown txtRoundTime;
    private System.Windows.Forms.NumericUpDown txtMagazineCount;
    private System.Windows.Forms.NumericUpDown txtAmmoPerMagazine;
    private System.Windows.Forms.NumericUpDown txtHP;
    private TransparentLabel lblHP;
    private TransparentLabel label4;
    private TransparentLabel lblRoundTime;
    private TransparentLabel lblAmmoPerMagazine;
    private TransparentLabel lblMagazineCount;
    private ToolButton tbAddTeam;
    private ToolButton tbAddUser;
    private ToolButton tbRemoveUser;
    private System.Windows.Forms.NumericUpDown txtRestTime;
    private TransparentLabel transparentLabel1;
    private System.Windows.Forms.ComboBox cmbFriendlyFire;
    private TransparentLabel transparentLabel2;

  }
}