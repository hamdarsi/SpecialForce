﻿namespace SpecialForce.Desktop
{
  partial class Desktop
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Desktop));
      this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
      this.mnuTray = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mnuDesktop = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
      this.mnuUsers = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuTeams = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuCompetitions = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuSites = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuAccounting = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuViewGame = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuSessions = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
      this.mnuSettings = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuAbout = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuLock = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
      this.tinypicker1 = new SpecialForce.TinyPicker();
      this.clock1 = new SpecialForce.UserInterface.Clock();
      this.tbButtons = new SpecialForce.ToolBar();
      this.transparentLabel5 = new SpecialForce.TransparentLabel();
      this.transparentLabel4 = new SpecialForce.TransparentLabel();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.noteBook1 = new SpecialForce.UserInterface.NoteBook();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.btnScheduling = new SpecialForce.ToolButton();
      this.btnAccounting = new SpecialForce.ToolButton();
      this.btnControlTower = new SpecialForce.ToolButton();
      this.btnSettings = new SpecialForce.ToolButton();
      this.btnSites = new SpecialForce.ToolButton();
      this.btnTeams = new SpecialForce.ToolButton();
      this.btnUsers = new SpecialForce.ToolButton();
      this.btnCompetitions = new SpecialForce.ToolButton();
      this.mnuTray.SuspendLayout();
      this.tbButtons.SuspendLayout();
      this.toolBar1.SuspendLayout();
      this.SuspendLayout();
      // 
      // trayIcon
      // 
      this.trayIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
      this.trayIcon.ContextMenuStrip = this.mnuTray;
      this.trayIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.trayIcon_MouseDoubleClick);
      // 
      // mnuTray
      // 
      this.mnuTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDesktop,
            this.toolStripMenuItem1,
            this.mnuUsers,
            this.mnuTeams,
            this.mnuCompetitions,
            this.mnuSites,
            this.mnuAccounting,
            this.mnuViewGame,
            this.mnuSessions,
            this.toolStripMenuItem2,
            this.mnuSettings,
            this.mnuAbout,
            this.mnuLock,
            this.mnuExit});
      this.mnuTray.Name = "mnTrayIcon";
      this.mnuTray.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      this.mnuTray.Size = new System.Drawing.Size(163, 280);
      // 
      // mnuDesktop
      // 
      this.mnuDesktop.Name = "mnuDesktop";
      this.mnuDesktop.Size = new System.Drawing.Size(162, 22);
      this.mnuDesktop.Text = "میز کار";
      this.mnuDesktop.Click += new System.EventHandler(this.mnuDesktop_Click);
      // 
      // toolStripMenuItem1
      // 
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.Size = new System.Drawing.Size(159, 6);
      // 
      // mnuUsers
      // 
      this.mnuUsers.Name = "mnuUsers";
      this.mnuUsers.Size = new System.Drawing.Size(162, 22);
      this.mnuUsers.Text = "مدیریت کاربرها";
      this.mnuUsers.Click += new System.EventHandler(this.btnUsers_Click);
      // 
      // mnuTeams
      // 
      this.mnuTeams.Name = "mnuTeams";
      this.mnuTeams.Size = new System.Drawing.Size(162, 22);
      this.mnuTeams.Text = "مدیریت تیم ها";
      this.mnuTeams.Click += new System.EventHandler(this.btnTeams_Click);
      // 
      // mnuCompetitions
      // 
      this.mnuCompetitions.Name = "mnuCompetitions";
      this.mnuCompetitions.Size = new System.Drawing.Size(162, 22);
      this.mnuCompetitions.Text = "امور مسابقات";
      this.mnuCompetitions.Click += new System.EventHandler(this.btnCompetitions_Click);
      // 
      // mnuSites
      // 
      this.mnuSites.Name = "mnuSites";
      this.mnuSites.Size = new System.Drawing.Size(162, 22);
      this.mnuSites.Text = "امور باشگاه ها";
      this.mnuSites.Click += new System.EventHandler(this.btnSites_Click);
      // 
      // mnuAccounting
      // 
      this.mnuAccounting.Name = "mnuAccounting";
      this.mnuAccounting.Size = new System.Drawing.Size(162, 22);
      this.mnuAccounting.Text = "امور مالی";
      this.mnuAccounting.Click += new System.EventHandler(this.btnAccounting_Click);
      // 
      // mnuViewGame
      // 
      this.mnuViewGame.Name = "mnuViewGame";
      this.mnuViewGame.Size = new System.Drawing.Size(162, 22);
      this.mnuViewGame.Text = "تماشای بازی فعلی";
      this.mnuViewGame.Click += new System.EventHandler(this.btnTakavar_Click);
      // 
      // mnuSessions
      // 
      this.mnuSessions.Name = "mnuSessions";
      this.mnuSessions.Size = new System.Drawing.Size(162, 22);
      this.mnuSessions.Text = "مدیریت سانس ها";
      this.mnuSessions.Click += new System.EventHandler(this.btnScheduling_Click);
      // 
      // toolStripMenuItem2
      // 
      this.toolStripMenuItem2.Name = "toolStripMenuItem2";
      this.toolStripMenuItem2.Size = new System.Drawing.Size(159, 6);
      // 
      // mnuSettings
      // 
      this.mnuSettings.Name = "mnuSettings";
      this.mnuSettings.Size = new System.Drawing.Size(162, 22);
      this.mnuSettings.Text = "تنظیمات";
      this.mnuSettings.Click += new System.EventHandler(this.btnSettings_Click);
      // 
      // mnuAbout
      // 
      this.mnuAbout.Enabled = false;
      this.mnuAbout.Name = "mnuAbout";
      this.mnuAbout.Size = new System.Drawing.Size(162, 22);
      this.mnuAbout.Text = "درباره برنامه";
      // 
      // mnuLock
      // 
      this.mnuLock.Name = "mnuLock";
      this.mnuLock.Size = new System.Drawing.Size(162, 22);
      this.mnuLock.Text = "قفل سیستم";
      this.mnuLock.Click += new System.EventHandler(this.mnuLock_Click);
      // 
      // mnuExit
      // 
      this.mnuExit.Name = "mnuExit";
      this.mnuExit.Size = new System.Drawing.Size(162, 22);
      this.mnuExit.Text = "خروج";
      this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
      // 
      // tinypicker1
      // 
      this.tinypicker1.Location = new System.Drawing.Point(12, 443);
      this.tinypicker1.Name = "tinypicker1";
      this.tinypicker1.Size = new System.Drawing.Size(700, 30);
      this.tinypicker1.TabIndex = 45;
      this.tinypicker1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tinypicker1.Texts")));
      // 
      // clock1
      // 
      this.clock1.Location = new System.Drawing.Point(588, 117);
      this.clock1.Name = "clock1";
      this.clock1.Size = new System.Drawing.Size(124, 124);
      this.clock1.TabIndex = 44;
      this.clock1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("clock1.Texts")));
      // 
      // tbButtons
      // 
      this.tbButtons.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbButtons.BorderWidth = 0.5F;
      this.tbButtons.Controls.Add(this.transparentLabel5);
      this.tbButtons.Controls.Add(this.transparentLabel4);
      this.tbButtons.Controls.Add(this.transparentLabel3);
      this.tbButtons.Controls.Add(this.transparentLabel1);
      this.tbButtons.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbButtons.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbButtons.Location = new System.Drawing.Point(464, 265);
      this.tbButtons.Name = "tbButtons";
      this.tbButtons.Size = new System.Drawing.Size(248, 150);
      this.tbButtons.TabIndex = 43;
      // 
      // transparentLabel5
      // 
      this.transparentLabel5.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel5.Location = new System.Drawing.Point(109, 112);
      this.transparentLabel5.Name = "transparentLabel5";
      this.transparentLabel5.Size = new System.Drawing.Size(122, 26);
      this.transparentLabel5.TabIndex = 4;
      this.transparentLabel5.TabStop = false;
      this.transparentLabel5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel5.Texts")));
      // 
      // transparentLabel4
      // 
      this.transparentLabel4.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel4.Location = new System.Drawing.Point(7, 80);
      this.transparentLabel4.Name = "transparentLabel4";
      this.transparentLabel4.Size = new System.Drawing.Size(229, 26);
      this.transparentLabel4.TabIndex = 3;
      this.transparentLabel4.TabStop = false;
      this.transparentLabel4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel4.Texts")));
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel3.Location = new System.Drawing.Point(168, 3);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(61, 26);
      this.transparentLabel3.TabIndex = 2;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.Location = new System.Drawing.Point(104, 35);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(129, 26);
      this.transparentLabel1.TabIndex = 0;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // noteBook1
      // 
      this.noteBook1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
      this.noteBook1.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.noteBook1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
      this.noteBook1.Location = new System.Drawing.Point(12, 117);
      this.noteBook1.Name = "noteBook1";
      this.noteBook1.Size = new System.Drawing.Size(435, 298);
      this.noteBook1.TabIndex = 41;
      this.noteBook1.TextAlign = SpecialForce.Alignment.TopMiddle;
      this.noteBook1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("noteBook1.Texts")));
      this.noteBook1.UserID = -1;
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.Gray;
      this.toolBar1.Controls.Add(this.btnScheduling);
      this.toolBar1.Controls.Add(this.btnAccounting);
      this.toolBar1.Controls.Add(this.btnControlTower);
      this.toolBar1.Controls.Add(this.btnSettings);
      this.toolBar1.Controls.Add(this.btnSites);
      this.toolBar1.Controls.Add(this.btnTeams);
      this.toolBar1.Controls.Add(this.btnUsers);
      this.toolBar1.Controls.Add(this.btnCompetitions);
      this.toolBar1.CornerRadius = 10;
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(107)))), ((int)(((byte)(84)))));
      this.toolBar1.GradientStartColor = System.Drawing.Color.White;
      this.toolBar1.Location = new System.Drawing.Point(72, -17);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.ShowGradient = true;
      this.toolBar1.Size = new System.Drawing.Size(584, 108);
      this.toolBar1.TabIndex = 0;
      // 
      // btnScheduling
      // 
      this.btnScheduling.Hint = "برنامه ریزی و زمان بندی";
      this.btnScheduling.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnScheduling.Image")));
      this.btnScheduling.Location = new System.Drawing.Point(83, 29);
      this.btnScheduling.Name = "btnScheduling";
      this.btnScheduling.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnScheduling.Size = new System.Drawing.Size(64, 64);
      this.btnScheduling.TabIndex = 6;
      this.btnScheduling.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnScheduling.Texts")));
      this.btnScheduling.TransparentColor = System.Drawing.Color.White;
      this.btnScheduling.Click += new System.EventHandler(this.btnScheduling_Click);
      // 
      // btnAccounting
      // 
      this.btnAccounting.Hint = "امور مالی";
      this.btnAccounting.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnAccounting.Image")));
      this.btnAccounting.Location = new System.Drawing.Point(223, 29);
      this.btnAccounting.Name = "btnAccounting";
      this.btnAccounting.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnAccounting.Size = new System.Drawing.Size(64, 64);
      this.btnAccounting.TabIndex = 5;
      this.btnAccounting.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnAccounting.Texts")));
      this.btnAccounting.TransparentColor = System.Drawing.Color.White;
      this.btnAccounting.Click += new System.EventHandler(this.btnAccounting_Click);
      // 
      // btnControlTower
      // 
      this.btnControlTower.Hint = "کنترل بازی";
      this.btnControlTower.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnControlTower.Image")));
      this.btnControlTower.Location = new System.Drawing.Point(153, 29);
      this.btnControlTower.Name = "btnControlTower";
      this.btnControlTower.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnControlTower.Size = new System.Drawing.Size(64, 64);
      this.btnControlTower.TabIndex = 4;
      this.btnControlTower.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnControlTower.Texts")));
      this.btnControlTower.TransparentColor = System.Drawing.Color.White;
      this.btnControlTower.Click += new System.EventHandler(this.btnTakavar_Click);
      // 
      // btnSettings
      // 
      this.btnSettings.Hint = "تنظیمات";
      this.btnSettings.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnSettings.Image")));
      this.btnSettings.Location = new System.Drawing.Point(13, 29);
      this.btnSettings.Name = "btnSettings";
      this.btnSettings.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnSettings.Size = new System.Drawing.Size(64, 64);
      this.btnSettings.TabIndex = 3;
      this.btnSettings.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnSettings.Texts")));
      this.btnSettings.TransparentColor = System.Drawing.Color.White;
      this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
      // 
      // btnSites
      // 
      this.btnSites.Hint = "باشگاه ها";
      this.btnSites.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnSites.Image")));
      this.btnSites.Location = new System.Drawing.Point(293, 29);
      this.btnSites.Name = "btnSites";
      this.btnSites.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnSites.Size = new System.Drawing.Size(64, 64);
      this.btnSites.TabIndex = 3;
      this.btnSites.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnSites.Texts")));
      this.btnSites.TransparentColor = System.Drawing.Color.White;
      this.btnSites.Click += new System.EventHandler(this.btnSites_Click);
      // 
      // btnTeams
      // 
      this.btnTeams.Hint = "تیم ها";
      this.btnTeams.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnTeams.Image")));
      this.btnTeams.Location = new System.Drawing.Point(433, 29);
      this.btnTeams.Name = "btnTeams";
      this.btnTeams.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnTeams.Size = new System.Drawing.Size(64, 64);
      this.btnTeams.TabIndex = 2;
      this.btnTeams.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnTeams.Texts")));
      this.btnTeams.TransparentColor = System.Drawing.Color.White;
      this.btnTeams.Click += new System.EventHandler(this.btnTeams_Click);
      // 
      // btnUsers
      // 
      this.btnUsers.Hint = "کاربر ها";
      this.btnUsers.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnUsers.Image")));
      this.btnUsers.Location = new System.Drawing.Point(503, 29);
      this.btnUsers.Name = "btnUsers";
      this.btnUsers.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnUsers.Size = new System.Drawing.Size(64, 64);
      this.btnUsers.TabIndex = 1;
      this.btnUsers.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnUsers.Texts")));
      this.btnUsers.TransparentColor = System.Drawing.Color.White;
      this.btnUsers.Click += new System.EventHandler(this.btnUsers_Click);
      // 
      // btnCompetitions
      // 
      this.btnCompetitions.Hint = "مسابقات";
      this.btnCompetitions.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnCompetitions.Image")));
      this.btnCompetitions.Location = new System.Drawing.Point(363, 29);
      this.btnCompetitions.Name = "btnCompetitions";
      this.btnCompetitions.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnCompetitions.Size = new System.Drawing.Size(64, 64);
      this.btnCompetitions.TabIndex = 0;
      this.btnCompetitions.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnCompetitions.Texts")));
      this.btnCompetitions.TransparentColor = System.Drawing.Color.White;
      this.btnCompetitions.Click += new System.EventHandler(this.btnCompetitions_Click);
      // 
      // Desktop
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(724, 485);
      this.Controls.Add(this.tinypicker1);
      this.Controls.Add(this.clock1);
      this.Controls.Add(this.tbButtons);
      this.Controls.Add(this.noteBook1);
      this.Controls.Add(this.toolBar1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "Desktop";
      this.Text = "میز کار نیروی ویژه";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Desktop_FormClosing);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Desktop_FormClosed);
      this.Load += new System.EventHandler(this.Desktop_Load);
      this.mnuTray.ResumeLayout(false);
      this.tbButtons.ResumeLayout(false);
      this.toolBar1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private ToolBar toolBar1;
    private ToolButton btnAccounting;
    private ToolButton btnControlTower;
    private ToolButton btnSettings;
    private ToolButton btnSites;
    private ToolButton btnTeams;
    private ToolButton btnUsers;
    private ToolButton btnCompetitions;
    private ToolButton btnScheduling;
    private UserInterface.NoteBook noteBook1;
    private ToolBar tbButtons;
    private TransparentLabel transparentLabel5;
    private TransparentLabel transparentLabel4;
    private TransparentLabel transparentLabel3;
    private TransparentLabel transparentLabel1;
    private UserInterface.Clock clock1;
    private TinyPicker tinypicker1;
    private System.Windows.Forms.NotifyIcon trayIcon;
    private System.Windows.Forms.ContextMenuStrip mnuTray;
    private System.Windows.Forms.ToolStripMenuItem mnuDesktop;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem mnuUsers;
    private System.Windows.Forms.ToolStripMenuItem mnuTeams;
    private System.Windows.Forms.ToolStripMenuItem mnuCompetitions;
    private System.Windows.Forms.ToolStripMenuItem mnuSites;
    private System.Windows.Forms.ToolStripMenuItem mnuAccounting;
    private System.Windows.Forms.ToolStripMenuItem mnuViewGame;
    private System.Windows.Forms.ToolStripMenuItem mnuSessions;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
    private System.Windows.Forms.ToolStripMenuItem mnuSettings;
    private System.Windows.Forms.ToolStripMenuItem mnuAbout;
    private System.Windows.Forms.ToolStripMenuItem mnuExit;
    private System.Windows.Forms.ToolStripMenuItem mnuLock;
  }
}