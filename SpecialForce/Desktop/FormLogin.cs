﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;

namespace SpecialForce.Desktop
{
  public partial class FormLogin : SessionAwareForm
  {
    private CaptureThread mDeviceThread;
    private SearchThread mSearchThread;
    private double mSearchPercentage;

    private Desktop mDesktop = null;
    public Desktop Desktop
    {
      set { mDesktop = value; }
    }

    private Kiosk mKiosk = null;
    public Kiosk Kiosk
    {
      set { mKiosk = value; }
    }

    public FormLogin()
    {
      InitializeComponent();
    }

    private void txtUsername_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        e.SuppressKeyPress = true;
        e.Handled = true;
        txtPassword.Focus();
        txtPassword.SelectAll();
      }
    }

    private void txtPassword_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        e.SuppressKeyPress = true;
        e.Handled = true;
        btnNext_Click(sender, null);
      }
    }

    private void PostLogin()
    {
      DialogResult = System.Windows.Forms.DialogResult.OK;

      if (mDesktop != null)
        mDesktop.LoginAccepted();
      else if (mKiosk != null)
        mKiosk.LoginAccepted();

      if (!Modal)
        Close();
    }

    private void btnNext_Click(object sender, EventArgs e)
    {
      int user_id = DB.User.FindUserWithCredentials(txtUsername.Text, txtPassword.Text);
      if (user_id == -1)
        Session.ShowError("کاربری با مشخصات وارد شده در سیستم یافت نشد.");
      else if (Session.Login(user_id) == false)
        Session.ShowError(Session.ErrorString);
      else
      {
        PostLogin();
        return;
      }

      txtUsername.Focus();
      txtUsername.SelectAll();
    }

    public void FingerPrintCaptured(object sender, CaptureEventArgs e)
    {
      pbFingerPrint.Image = e.Image;
      lblSearchStatus.Text = "جستجو 0%"; 
      Enabled = false; 
      Application.UseWaitCursor = true;

      mSearchPercentage = 0.0;
      mSearchThread = new SearchThread(e.Data);
      mSearchThread.OnSearchUpdate += FPSearchUpdate;
      mSearchThread.OnSearchFinish += FPSearchFinished;
      mSearchThread.BeginSearch();
    }

    private void FPSearchUpdate(Object sender, SearchUpdateEventArgs e)
    {
      double p = 100.0 * e.CurrentIndex / e.TotalCount;
      if(p >= mSearchPercentage + 10.0)
      {
        mSearchPercentage = p;
        lblSearchStatus.Text = "جستجو " + mSearchPercentage.ToString() + "%";
      }
    }

    private void FPSearchFinished(Object sender, SearchFinishEventArgs e)
    {
      Application.UseWaitCursor = false;
      Enabled = true;

      if (!e.Succeeded)
        lblSearchStatus.Text = "اثر انگشت یافت نشد";
      else if (!Session.Login(e.UserID))
        Session.ShowError(Session.ErrorString);
      else
        PostLogin();
    }

    private void FormLogin_Load(object sender, EventArgs e)
    {
      Icon = Properties.Resources.ApplicationIcon;

      txtUsername.RightToLeft = RightToLeft.No;
      txtPassword.RightToLeft = RightToLeft.No;

      mDeviceThread = FingerPrint.OpenDeviceAsThread(0);
      mDeviceThread.OnCapture += FingerPrintCaptured;
    }

    private void FormLogin_FormClosed(object sender, FormClosedEventArgs e)
    {
      mDeviceThread.OnCapture -= FingerPrintCaptured;
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      if (DB.GameManager.CurrentGame != null)
      {
        Session.ShowError("بازی در حال اجراست. برنامه را نمی توان بست.");
        return;
      }

      if (mDesktop != null)
        mDesktop.LoginCancelled();
      else if (mKiosk != null)
        mKiosk.LoginCancelled();

      if (!Modal)
        Close();
    }

    private void FormLogin_FormClosing(object sender, FormClosingEventArgs e)
    {
      // If login was done proceed with closing
      if (Session.Open)
        return;

      // If a game is in progress dialog can not be closed
      if (DB.GameManager.CurrentGame != null)
      {
        Session.ShowError("بازی در حال اجراست. برنامه را نمی توان بست.");
        e.Cancel = true;
      }

      // Otherwise inform desktop that login has been cancelled.
      else if (mDesktop != null)
      {
        DialogResult = DialogResult.Cancel;
        mDesktop.LoginCancelled();
      }
      else if (mKiosk != null)
      {
        DialogResult = DialogResult.Cancel;
        mKiosk.LoginCancelled();
      }
    }
  }
}
