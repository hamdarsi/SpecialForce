﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace SpecialForce.Desktop
{
  public partial class Kiosk : SessionAwareForm
  {
    private string mSiteName = "";

    static private Kiosk mInstance = null;
    static public Kiosk Instance
    {
      get
      {
        if (mInstance == null)
          mInstance = new Kiosk();

        return mInstance;
      }
    }

    private Kiosk()
    {
      InitializeComponent();
    }

    private void Kiosk_Load(object sender, EventArgs e)
    {
      // Cache site name
      mSiteName = DB.Site.GetSiteName(Session.ActiveSiteID);

      // Synchronization starts here
      Session.StartSynchronization(btnLogout);

      // A blocking login prompt
      if (Session.CurrentStaff == null)
      {
        if (new FormLogin().ShowDialog() == DialogResult.Cancel)
        {
          Application.Exit();
          return;
        }
      }

      // Now non blockingly prompt for login whenever application is locked
      Session.OnSessionLocked += OnSessionLocked;

      // GameManager daemon starts here
      DB.GameManager.StartDaemon(false);

      // Show the form and its tray icon
      trayIcon.Icon = Properties.Resources.TrayIcon;
      trayIcon.BalloonTipTitle = Text;
      trayIcon.Visible = true;
      Wellcome();
    }

    private void Kiosk_FormClosed(object sender, FormClosedEventArgs e)
    {
      try
      {
        // Application is exiting. Do not check for further sessions
        Session.OnSessionLocked -= OnSessionLocked;

        // Stop session synchronization
        Session.StopSynchronization();
      }
      catch
      {
      }
    }

    private void OnSessionLocked(Object sender, EventArgs e)
    {
      // Non blockingly prompt for user login
      // First close all open forms except the desktop and critical modules
      // Do not use Application.OpenForms directly.
      // It updates everytime a form closes
      List<Form> forms = new List<Form>();
      foreach (Form openfrm in Application.OpenForms)
        forms.Add(openfrm);

      // Now close them in reverse order
      for (int i = forms.Count - 1; i >= 0; --i)
        if (forms[i] == null || forms[i].IsDisposed)
          continue;
        else if (forms[i] is Kiosk)
          forms[i].Hide();
        else
          forms[i].Close();

      // Tray icon menu
      trayIcon.ContextMenuStrip = null;
      trayIcon.ShowBalloonTip(11000, Text, "برنامه قفل شد", ToolTipIcon.Info);

      // Prompt login
      FormLogin frm = new FormLogin();
      frm.Kiosk = this;
      frm.Show();

      // And wait for FormLogin to return its result!
    }

    private void Wellcome()
    {
      string str = Session.CurrentStaff.Gender ? "آقای " : "خانم ";
      str += Session.CurrentStaff.FullName + Environment.NewLine;
      str += "بازی نیروی ویژه در خدمت شماست" + Environment.NewLine;
      str += "خوش آمدید";
      trayIcon.ShowBalloonTip(5000, Text, str, ToolTipIcon.Info);

      UpdateUserPane();
    }

    private void UpdateUserPane()
    {
      DB.User u = Session.CurrentStaff;
      lblFirstName.Text = u.FirstName;
      lblLastName.Text = u.LastName;
      lblUserName.Text = u.UserName;
      lblRegistrationDate.Text = u.RegistrationDate.ToString(DateTimeString.CompactDate);
      lblThisSite.Text = mSiteName;
      pbPhoto.Image = u.Photo.Resize(pbPhoto.Size);
    }

    public void LoginAccepted()
    {
      Show();
      Wellcome();
    }

    public void LoginCancelled()
    {
      Close();
    }

    private void PanelClosed(Object sender, EventArgs e)
    {
      if (IsDisposed)
        return;
      else if (!Session.Open)
        return;
      else if (!Visible)
        Show();
    }

    private void ShowSpecificPanel(SessionAwareForm frm)
    {
      Hide();
      frm.FormClosed += PanelClosed;
      frm.Show();
    }

    private void btnLogout_Click(object sender, EventArgs e)
    {
      Session.Lock();
    }

    private void btnUserEdit_Click(object sender, EventArgs e)
    {
      Users.FormUserAddEdit frm = new Users.FormUserAddEdit();
      frm.User = Session.CurrentStaff;
      if(ShowModal(frm, false) == DialogResult.OK)
        UpdateUserPane();
    }

    private void btnUserAccounting_Click(object sender, EventArgs e)
    {
      Users.FormUserReportSettings frm = new Users.FormUserReportSettings();
      frm.ReportType = Users.FormUserReportSettings.UserReportType.Credit;
      frm.User = Session.CurrentStaff;
      ShowModal(frm, false);
    }

    private void btnUserActivities_Click(object sender, EventArgs e)
    {
      Users.FormUserReportSettings frm = new Users.FormUserReportSettings();
      frm.ReportType = Users.FormUserReportSettings.UserReportType.Games;
      frm.User = Session.CurrentStaff;
      ShowModal(frm, false);
    }

    private void btnUserRankTimeline_Click(object sender, EventArgs e)
    {
      ShowModal(new Users.WizardRankingsInTimeLine(), false);
    }

    private void btnUserRankCompetitions_Click(object sender, EventArgs e)
    {
      ShowModal(new Users.WizardRankingsInCompetitions(), false);
    }

    private void btnTeams_Click(object sender, EventArgs e)
    {
      ShowSpecificPanel(Teams.ControlPanel.Instance);
    }

    private void btnCompetitionCharts_Click(object sender, EventArgs e)
    {
      WizardSelectCompetition wzd = new WizardSelectCompetition();
      if (wzd.ShowDialog() == DialogResult.OK)
      {
        Competitions.FormViewCompetition frm = new Competitions.FormViewCompetition();
        frm.Competition = wzd.SelectedCompetition;
        ShowModal(frm, false);
      }
    }

    private void btnCompetitionRankings_Click(object sender, EventArgs e)
    {
      WizardSelectCompetition wzd = new WizardSelectCompetition();
      if (wzd.ShowDialog() == DialogResult.OK)
      {
        Competitions.FormCompetitionReport frm = new Competitions.FormCompetitionReport();
        frm.Competition = wzd.SelectedCompetition;
        ShowModal(frm, false);
      }
    }

    private void btnCompetitionReports_Click(object sender, EventArgs e)
    {
      WizardSelectCompetition wzd = new WizardSelectCompetition();
      if (wzd.ShowDialog() == DialogResult.OK)
      {
        Scheduling.FormGameReport frm = new Scheduling.FormGameReport();
        frm.Games = wzd.SelectedCompetition.Games;
        frm.ShortState = true;
        frm.Title = "گزارش بازی های مسابقه " + wzd.SelectedCompetition.Title + " (" + Enums.ToString(wzd.SelectedCompetition.Type) + ")";
        ShowModal(frm, false);
      }
    }

    private void btnUserActivitySpecific_Click(object sender, EventArgs e)
    {
      Users.FormUserReportSettings frm = new Users.FormUserReportSettings();
      frm.ReportType = Users.FormUserReportSettings.UserReportType.GamesAndRounds;
      frm.User = Session.CurrentStaff;
      ShowModal(frm, false);
    }
  }
}
