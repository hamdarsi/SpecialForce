﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SpecialForce.Desktop
{
  public partial class WizardSelectCompetition : SessionAwareForm
  {
    private List<DB.Competition> mCompetitions = DB.CompetitionManager.AllCompetitions;

    private DB.Competition mSelectedCompetition;
    public DB.Competition SelectedCompetition
    {
      get { return mSelectedCompetition; }
    }

    public WizardSelectCompetition()
    {
      InitializeComponent();
    }

    private void WizardSelectCompetition_Load(object sender, EventArgs e)
    {
      for (int i = 0; i < mCompetitions.Count; ++i)
        lvCompetitions.Items.Add(mCompetitions[i].Title + "  (" + Enums.ToString(mCompetitions[i].Type) + ")");
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      if (lvCompetitions.SelectedIndices.Count == 0)
      {
        Session.ShowError("هیچ مسابقه ای انتخاب نشده است");
        return;
      }

      mSelectedCompetition = mCompetitions[lvCompetitions.SelectedIndices[0]];
      DialogResult = DialogResult.OK;
    }
  }
}
