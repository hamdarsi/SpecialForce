﻿namespace SpecialForce.Desktop
{
  partial class FormLogin
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLogin));
      this.pnlBanner = new SpecialForce.ToolBar();
      this.lblInfo = new SpecialForce.TransparentLabel();
      this.lblBanner = new SpecialForce.TransparentLabel();
      this.btnNext = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.lblFooter = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.label2 = new SpecialForce.TransparentLabel();
      this.txtUsername = new System.Windows.Forms.TextBox();
      this.txtPassword = new System.Windows.Forms.TextBox();
      this.label3 = new SpecialForce.TransparentLabel();
      this.pbFingerPrint = new System.Windows.Forms.PictureBox();
      this.lblSearchStatus = new SpecialForce.TransparentLabel();
      this.pnlBanner.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbFingerPrint)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlBanner
      // 
      this.pnlBanner.BackColor = System.Drawing.Color.White;
      this.pnlBanner.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlBanner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlBanner.Controls.Add(this.lblInfo);
      this.pnlBanner.Controls.Add(this.lblBanner);
      this.pnlBanner.Location = new System.Drawing.Point(-25, -6);
      this.pnlBanner.Name = "pnlBanner";
      this.pnlBanner.Size = new System.Drawing.Size(483, 84);
      this.pnlBanner.TabIndex = 7;
      // 
      // lblInfo
      // 
      this.lblInfo.AutoSize = true;
      this.lblInfo.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblInfo.Location = new System.Drawing.Point(8, 43);
      this.lblInfo.Name = "lblInfo";
      this.lblInfo.Size = new System.Drawing.Size(431, 24);
      this.lblInfo.TabIndex = 1;
      this.lblInfo.TabStop = false;
      this.lblInfo.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblInfo.Texts")));
      // 
      // lblBanner
      // 
      this.lblBanner.AutoSize = true;
      this.lblBanner.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblBanner.Location = new System.Drawing.Point(367, 15);
      this.lblBanner.Name = "lblBanner";
      this.lblBanner.Size = new System.Drawing.Size(86, 26);
      this.lblBanner.TabIndex = 0;
      this.lblBanner.TabStop = false;
      this.lblBanner.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBanner.Texts")));
      // 
      // btnNext
      // 
      this.btnNext.ForeColor = System.Drawing.Color.Black;
      this.btnNext.Location = new System.Drawing.Point(284, 341);
      this.btnNext.Name = "btnNext";
      this.btnNext.Size = new System.Drawing.Size(75, 23);
      this.btnNext.TabIndex = 4;
      this.btnNext.Text = "ورود";
      this.btnNext.UseVisualStyleBackColor = true;
      this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(365, 341);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 3;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // lblFooter
      // 
      this.lblFooter.AutoSize = true;
      this.lblFooter.Enabled = false;
      this.lblFooter.Location = new System.Drawing.Point(2, 314);
      this.lblFooter.Name = "lblFooter";
      this.lblFooter.Size = new System.Drawing.Size(458, 15);
      this.lblFooter.TabIndex = 4;
      this.lblFooter.TabStop = false;
      this.lblFooter.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblFooter.Texts")));
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(107, 109);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(55, 15);
      this.label1.TabIndex = 8;
      this.label1.TabStop = false;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(107, 136);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(46, 15);
      this.label2.TabIndex = 9;
      this.label2.TabStop = false;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // txtUsername
      // 
      this.txtUsername.Location = new System.Drawing.Point(243, 106);
      this.txtUsername.Name = "txtUsername";
      this.txtUsername.Size = new System.Drawing.Size(102, 21);
      this.txtUsername.TabIndex = 1;
      this.txtUsername.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUsername_KeyDown);
      // 
      // txtPassword
      // 
      this.txtPassword.Location = new System.Drawing.Point(243, 133);
      this.txtPassword.Name = "txtPassword";
      this.txtPassword.Size = new System.Drawing.Size(102, 21);
      this.txtPassword.TabIndex = 2;
      this.txtPassword.UseSystemPasswordChar = true;
      this.txtPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPassword_KeyDown);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(107, 225);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(56, 15);
      this.label3.TabIndex = 12;
      this.label3.TabStop = false;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // pbFingerPrint
      // 
      this.pbFingerPrint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbFingerPrint.Image = global::SpecialForce.Properties.Resources.FingerPrintGesture;
      this.pbFingerPrint.Location = new System.Drawing.Point(243, 180);
      this.pbFingerPrint.Name = "pbFingerPrint";
      this.pbFingerPrint.Size = new System.Drawing.Size(102, 110);
      this.pbFingerPrint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pbFingerPrint.TabIndex = 13;
      this.pbFingerPrint.TabStop = false;
      // 
      // lblSearchStatus
      // 
      this.lblSearchStatus.AutoSize = true;
      this.lblSearchStatus.Location = new System.Drawing.Point(240, 301);
      this.lblSearchStatus.Name = "lblSearchStatus";
      this.lblSearchStatus.Size = new System.Drawing.Size(0, 0);
      this.lblSearchStatus.TabIndex = 14;
      this.lblSearchStatus.TabStop = false;
      this.lblSearchStatus.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblSearchStatus.Texts")));
      // 
      // FormLogin
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.lblSearchStatus);
      this.Controls.Add(this.pbFingerPrint);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.txtPassword);
      this.Controls.Add(this.txtUsername);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.pnlBanner);
      this.Controls.Add(this.btnNext);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.lblFooter);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.MinimizeBox = false;
      this.Name = "FormLogin";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "نیروی ویژه";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormLogin_FormClosing);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormLogin_FormClosed);
      this.Load += new System.EventHandler(this.FormLogin_Load);
      this.pnlBanner.ResumeLayout(false);
      this.pnlBanner.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbFingerPrint)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private SpecialForce.ToolBar pnlBanner;
    protected SpecialForce.TransparentLabel lblInfo;
    protected SpecialForce.TransparentLabel lblBanner;
    private System.Windows.Forms.Button btnNext;
    private System.Windows.Forms.Button btnCancel;
    private SpecialForce.TransparentLabel lblFooter;
    private SpecialForce.TransparentLabel label1;
    private SpecialForce.TransparentLabel label2;
    private System.Windows.Forms.TextBox txtUsername;
    private System.Windows.Forms.TextBox txtPassword;
    private SpecialForce.TransparentLabel label3;
    private System.Windows.Forms.PictureBox pbFingerPrint;
    private SpecialForce.TransparentLabel lblSearchStatus;





  }
}