﻿namespace SpecialForce.Desktop
{
  partial class Kiosk
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Kiosk));
      this.toolBar1 = new SpecialForce.ToolBar();
      this.btnUserActivitySpecific = new SpecialForce.ToolButton();
      this.btnUserAccounting = new SpecialForce.ToolButton();
      this.btnUserEdit = new SpecialForce.ToolButton();
      this.btnTeams = new SpecialForce.ToolButton();
      this.btnCompetitionReports = new SpecialForce.ToolButton();
      this.btnCompetitionCharts = new SpecialForce.ToolButton();
      this.btnLogout = new SpecialForce.ToolButton();
      this.btnCompetitionRankings = new SpecialForce.ToolButton();
      this.btnUserRankTimeline = new SpecialForce.ToolButton();
      this.btnUserActivitiesOverall = new SpecialForce.ToolButton();
      this.btnUserRankCompetitions = new SpecialForce.ToolButton();
      this.toolBar2 = new SpecialForce.ToolBar();
      this.lblThisSite = new SpecialForce.TransparentLabel();
      this.lblRegistrationDate = new SpecialForce.TransparentLabel();
      this.lblLastName = new SpecialForce.TransparentLabel();
      this.lblUserName = new SpecialForce.TransparentLabel();
      this.lblFirstName = new SpecialForce.TransparentLabel();
      this.transparentLabel5 = new SpecialForce.TransparentLabel();
      this.transparentLabel4 = new SpecialForce.TransparentLabel();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.pbPhoto = new System.Windows.Forms.PictureBox();
      this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
      this.toolBar1.SuspendLayout();
      this.toolBar2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbPhoto)).BeginInit();
      this.SuspendLayout();
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.Gray;
      this.toolBar1.Controls.Add(this.btnUserActivitySpecific);
      this.toolBar1.Controls.Add(this.btnUserAccounting);
      this.toolBar1.Controls.Add(this.btnUserEdit);
      this.toolBar1.Controls.Add(this.btnTeams);
      this.toolBar1.Controls.Add(this.btnCompetitionReports);
      this.toolBar1.Controls.Add(this.btnCompetitionCharts);
      this.toolBar1.Controls.Add(this.btnLogout);
      this.toolBar1.Controls.Add(this.btnCompetitionRankings);
      this.toolBar1.Controls.Add(this.btnUserRankTimeline);
      this.toolBar1.Controls.Add(this.btnUserActivitiesOverall);
      this.toolBar1.Controls.Add(this.btnUserRankCompetitions);
      this.toolBar1.CornerRadius = 10;
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(107)))), ((int)(((byte)(84)))));
      this.toolBar1.GradientStartColor = System.Drawing.Color.White;
      this.toolBar1.Location = new System.Drawing.Point(22, -17);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.ShowGradient = true;
      this.toolBar1.Size = new System.Drawing.Size(793, 108);
      this.toolBar1.TabIndex = 1;
      // 
      // btnUserActivitySpecific
      // 
      this.btnUserActivitySpecific.Hint = "ریز بازی های بازیکن";
      this.btnUserActivitySpecific.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnUserActivitySpecific.Image")));
      this.btnUserActivitySpecific.Location = new System.Drawing.Point(504, 29);
      this.btnUserActivitySpecific.Name = "btnUserActivitySpecific";
      this.btnUserActivitySpecific.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnUserActivitySpecific.Size = new System.Drawing.Size(64, 64);
      this.btnUserActivitySpecific.TabIndex = 9;
      this.btnUserActivitySpecific.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnUserActivitySpecific.Texts")));
      this.btnUserActivitySpecific.TransparentColor = System.Drawing.Color.White;
      this.btnUserActivitySpecific.Click += new System.EventHandler(this.btnUserActivitySpecific_Click);
      // 
      // btnUserAccounting
      // 
      this.btnUserAccounting.Hint = "گزارش گیری از تراکنش های مالی";
      this.btnUserAccounting.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnUserAccounting.Image")));
      this.btnUserAccounting.Location = new System.Drawing.Point(644, 29);
      this.btnUserAccounting.Name = "btnUserAccounting";
      this.btnUserAccounting.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnUserAccounting.Size = new System.Drawing.Size(64, 64);
      this.btnUserAccounting.TabIndex = 8;
      this.btnUserAccounting.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnUserAccounting.Texts")));
      this.btnUserAccounting.TransparentColor = System.Drawing.Color.White;
      this.btnUserAccounting.Click += new System.EventHandler(this.btnUserAccounting_Click);
      // 
      // btnUserEdit
      // 
      this.btnUserEdit.Hint = "ویرایش مشخصات";
      this.btnUserEdit.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnUserEdit.Image")));
      this.btnUserEdit.Location = new System.Drawing.Point(714, 29);
      this.btnUserEdit.Name = "btnUserEdit";
      this.btnUserEdit.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnUserEdit.Size = new System.Drawing.Size(64, 64);
      this.btnUserEdit.TabIndex = 7;
      this.btnUserEdit.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnUserEdit.Texts")));
      this.btnUserEdit.TransparentColor = System.Drawing.Color.White;
      this.btnUserEdit.Click += new System.EventHandler(this.btnUserEdit_Click);
      // 
      // btnTeams
      // 
      this.btnTeams.Hint = "تیم ها";
      this.btnTeams.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnTeams.Image")));
      this.btnTeams.Location = new System.Drawing.Point(433, 29);
      this.btnTeams.Name = "btnTeams";
      this.btnTeams.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnTeams.Size = new System.Drawing.Size(64, 64);
      this.btnTeams.TabIndex = 6;
      this.btnTeams.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnTeams.Texts")));
      this.btnTeams.TransparentColor = System.Drawing.Color.White;
      this.btnTeams.Click += new System.EventHandler(this.btnTeams_Click);
      // 
      // btnCompetitionReports
      // 
      this.btnCompetitionReports.Hint = "اطلاعات بازی های مسابقات";
      this.btnCompetitionReports.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnCompetitionReports.Image")));
      this.btnCompetitionReports.Location = new System.Drawing.Point(153, 29);
      this.btnCompetitionReports.Name = "btnCompetitionReports";
      this.btnCompetitionReports.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnCompetitionReports.Size = new System.Drawing.Size(64, 64);
      this.btnCompetitionReports.TabIndex = 5;
      this.btnCompetitionReports.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnCompetitionReports.Texts")));
      this.btnCompetitionReports.TransparentColor = System.Drawing.Color.White;
      this.btnCompetitionReports.Click += new System.EventHandler(this.btnCompetitionReports_Click);
      // 
      // btnCompetitionCharts
      // 
      this.btnCompetitionCharts.Hint = "جدول مسابقات";
      this.btnCompetitionCharts.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnCompetitionCharts.Image")));
      this.btnCompetitionCharts.Location = new System.Drawing.Point(83, 29);
      this.btnCompetitionCharts.Name = "btnCompetitionCharts";
      this.btnCompetitionCharts.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnCompetitionCharts.Size = new System.Drawing.Size(64, 64);
      this.btnCompetitionCharts.TabIndex = 4;
      this.btnCompetitionCharts.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnCompetitionCharts.Texts")));
      this.btnCompetitionCharts.TransparentColor = System.Drawing.Color.White;
      this.btnCompetitionCharts.Click += new System.EventHandler(this.btnCompetitionCharts_Click);
      // 
      // btnLogout
      // 
      this.btnLogout.Hint = "خروج از سیستم";
      this.btnLogout.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnLogout.Image")));
      this.btnLogout.Location = new System.Drawing.Point(13, 29);
      this.btnLogout.Name = "btnLogout";
      this.btnLogout.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnLogout.Size = new System.Drawing.Size(64, 64);
      this.btnLogout.TabIndex = 3;
      this.btnLogout.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnLogout.Texts")));
      this.btnLogout.TransparentColor = System.Drawing.Color.White;
      this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
      // 
      // btnCompetitionRankings
      // 
      this.btnCompetitionRankings.Hint = "نتایج مسابقات";
      this.btnCompetitionRankings.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnCompetitionRankings.Image")));
      this.btnCompetitionRankings.Location = new System.Drawing.Point(223, 29);
      this.btnCompetitionRankings.Name = "btnCompetitionRankings";
      this.btnCompetitionRankings.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnCompetitionRankings.Size = new System.Drawing.Size(64, 64);
      this.btnCompetitionRankings.TabIndex = 3;
      this.btnCompetitionRankings.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnCompetitionRankings.Texts")));
      this.btnCompetitionRankings.TransparentColor = System.Drawing.Color.White;
      this.btnCompetitionRankings.Click += new System.EventHandler(this.btnCompetitionRankings_Click);
      // 
      // btnUserRankTimeline
      // 
      this.btnUserRankTimeline.Hint = "رتبه بندی کاربرها در بازه زمانی خاص";
      this.btnUserRankTimeline.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnUserRankTimeline.Image")));
      this.btnUserRankTimeline.Location = new System.Drawing.Point(363, 29);
      this.btnUserRankTimeline.Name = "btnUserRankTimeline";
      this.btnUserRankTimeline.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnUserRankTimeline.Size = new System.Drawing.Size(64, 64);
      this.btnUserRankTimeline.TabIndex = 2;
      this.btnUserRankTimeline.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnUserRankTimeline.Texts")));
      this.btnUserRankTimeline.TransparentColor = System.Drawing.Color.White;
      this.btnUserRankTimeline.Click += new System.EventHandler(this.btnUserRankTimeline_Click);
      // 
      // btnUserActivitiesOverall
      // 
      this.btnUserActivitiesOverall.Hint = "گزارش بازی های غیر رسمی";
      this.btnUserActivitiesOverall.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnUserActivitiesOverall.Image")));
      this.btnUserActivitiesOverall.Location = new System.Drawing.Point(574, 29);
      this.btnUserActivitiesOverall.Name = "btnUserActivitiesOverall";
      this.btnUserActivitiesOverall.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnUserActivitiesOverall.Size = new System.Drawing.Size(64, 64);
      this.btnUserActivitiesOverall.TabIndex = 1;
      this.btnUserActivitiesOverall.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnUserActivitiesOverall.Texts")));
      this.btnUserActivitiesOverall.TransparentColor = System.Drawing.Color.White;
      this.btnUserActivitiesOverall.Click += new System.EventHandler(this.btnUserActivities_Click);
      // 
      // btnUserRankCompetitions
      // 
      this.btnUserRankCompetitions.Hint = "رتبه بندی کاربر ها در مسابقات";
      this.btnUserRankCompetitions.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnUserRankCompetitions.Image")));
      this.btnUserRankCompetitions.Location = new System.Drawing.Point(293, 29);
      this.btnUserRankCompetitions.Name = "btnUserRankCompetitions";
      this.btnUserRankCompetitions.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnUserRankCompetitions.Size = new System.Drawing.Size(64, 64);
      this.btnUserRankCompetitions.TabIndex = 0;
      this.btnUserRankCompetitions.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnUserRankCompetitions.Texts")));
      this.btnUserRankCompetitions.TransparentColor = System.Drawing.Color.White;
      this.btnUserRankCompetitions.Click += new System.EventHandler(this.btnUserRankCompetitions_Click);
      // 
      // toolBar2
      // 
      this.toolBar2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar2.Controls.Add(this.lblThisSite);
      this.toolBar2.Controls.Add(this.lblRegistrationDate);
      this.toolBar2.Controls.Add(this.lblLastName);
      this.toolBar2.Controls.Add(this.lblUserName);
      this.toolBar2.Controls.Add(this.lblFirstName);
      this.toolBar2.Controls.Add(this.transparentLabel5);
      this.toolBar2.Controls.Add(this.transparentLabel4);
      this.toolBar2.Controls.Add(this.transparentLabel3);
      this.toolBar2.Controls.Add(this.transparentLabel2);
      this.toolBar2.Controls.Add(this.transparentLabel1);
      this.toolBar2.Controls.Add(this.pbPhoto);
      this.toolBar2.Location = new System.Drawing.Point(471, 376);
      this.toolBar2.Name = "toolBar2";
      this.toolBar2.Size = new System.Drawing.Size(344, 192);
      this.toolBar2.TabIndex = 2;
      // 
      // lblThisSite
      // 
      this.lblThisSite.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblThisSite.Location = new System.Drawing.Point(153, 147);
      this.lblThisSite.Name = "lblThisSite";
      this.lblThisSite.Size = new System.Drawing.Size(12, 26);
      this.lblThisSite.TabIndex = 10;
      this.lblThisSite.TabStop = false;
      this.lblThisSite.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblThisSite.Texts")));
      // 
      // lblRegistrationDate
      // 
      this.lblRegistrationDate.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblRegistrationDate.Location = new System.Drawing.Point(153, 114);
      this.lblRegistrationDate.Name = "lblRegistrationDate";
      this.lblRegistrationDate.Size = new System.Drawing.Size(12, 26);
      this.lblRegistrationDate.TabIndex = 9;
      this.lblRegistrationDate.TabStop = false;
      this.lblRegistrationDate.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblRegistrationDate.Texts")));
      // 
      // lblLastName
      // 
      this.lblLastName.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblLastName.Location = new System.Drawing.Point(153, 48);
      this.lblLastName.Name = "lblLastName";
      this.lblLastName.Size = new System.Drawing.Size(12, 26);
      this.lblLastName.TabIndex = 8;
      this.lblLastName.TabStop = false;
      this.lblLastName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblLastName.Texts")));
      // 
      // lblUserName
      // 
      this.lblUserName.Font = new System.Drawing.Font("DejaVu Sans Mono", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblUserName.Location = new System.Drawing.Point(152, 86);
      this.lblUserName.Name = "lblUserName";
      this.lblUserName.Size = new System.Drawing.Size(12, 17);
      this.lblUserName.TabIndex = 7;
      this.lblUserName.TabStop = false;
      this.lblUserName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblUserName.Texts")));
      // 
      // lblFirstName
      // 
      this.lblFirstName.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblFirstName.Location = new System.Drawing.Point(153, 15);
      this.lblFirstName.Name = "lblFirstName";
      this.lblFirstName.Size = new System.Drawing.Size(12, 26);
      this.lblFirstName.TabIndex = 6;
      this.lblFirstName.TabStop = false;
      this.lblFirstName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblFirstName.Texts")));
      // 
      // transparentLabel5
      // 
      this.transparentLabel5.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel5.Location = new System.Drawing.Point(253, 114);
      this.transparentLabel5.Name = "transparentLabel5";
      this.transparentLabel5.Size = new System.Drawing.Size(75, 26);
      this.transparentLabel5.TabIndex = 5;
      this.transparentLabel5.TabStop = false;
      this.transparentLabel5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel5.Texts")));
      // 
      // transparentLabel4
      // 
      this.transparentLabel4.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel4.Location = new System.Drawing.Point(240, 147);
      this.transparentLabel4.Name = "transparentLabel4";
      this.transparentLabel4.Size = new System.Drawing.Size(88, 26);
      this.transparentLabel4.TabIndex = 4;
      this.transparentLabel4.TabStop = false;
      this.transparentLabel4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel4.Texts")));
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel3.Location = new System.Drawing.Point(268, 81);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(60, 26);
      this.transparentLabel3.TabIndex = 3;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel2.Location = new System.Drawing.Point(257, 48);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(71, 26);
      this.transparentLabel2.TabIndex = 2;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.Location = new System.Drawing.Point(307, 15);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(21, 26);
      this.transparentLabel1.TabIndex = 1;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // pbPhoto
      // 
      this.pbPhoto.Location = new System.Drawing.Point(13, 15);
      this.pbPhoto.Name = "pbPhoto";
      this.pbPhoto.Size = new System.Drawing.Size(120, 160);
      this.pbPhoto.TabIndex = 0;
      this.pbPhoto.TabStop = false;
      // 
      // trayIcon
      // 
      this.trayIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
      // 
      // Kiosk
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackGroundPicture = global::SpecialForce.Properties.Resources.Kiosk_Background;
      this.ClientSize = new System.Drawing.Size(837, 580);
      this.Controls.Add(this.toolBar2);
      this.Controls.Add(this.toolBar1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "Kiosk";
      this.Text = "کیوسک";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Kiosk_FormClosed);
      this.Load += new System.EventHandler(this.Kiosk_Load);
      this.toolBar1.ResumeLayout(false);
      this.toolBar2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pbPhoto)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private ToolBar toolBar1;
    private ToolButton btnTeams;
    private ToolButton btnCompetitionReports;
    private ToolButton btnCompetitionCharts;
    private ToolButton btnLogout;
    private ToolButton btnCompetitionRankings;
    private ToolButton btnUserRankTimeline;
    private ToolButton btnUserActivitiesOverall;
    private ToolButton btnUserRankCompetitions;
    private ToolButton btnUserAccounting;
    private ToolBar toolBar2;
    private TransparentLabel lblThisSite;
    private TransparentLabel lblRegistrationDate;
    private TransparentLabel lblLastName;
    private TransparentLabel lblUserName;
    private TransparentLabel lblFirstName;
    private TransparentLabel transparentLabel5;
    private TransparentLabel transparentLabel4;
    private TransparentLabel transparentLabel3;
    private TransparentLabel transparentLabel2;
    private TransparentLabel transparentLabel1;
    private System.Windows.Forms.PictureBox pbPhoto;
    private ToolButton btnUserEdit;
    private System.Windows.Forms.NotifyIcon trayIcon;
    private ToolButton btnUserActivitySpecific;
  }
}