﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Desktop
{
  public partial class Desktop : SessionAwareForm
  {
    static private Desktop mInstance = null;
    static public Desktop Instance
    {
      get
      {
        if (mInstance == null)
          mInstance = new Desktop();

        return mInstance;
      }
    }

    private Desktop()
    {
      InitializeComponent();
    }

    protected override void ApplyPermissions(Post post)
    {
      btnSites.Enabled = post.IsPrivileged();
      mnuSites.Enabled = btnSites.Enabled;

      btnAccounting.Enabled = post.IsPrivileged();
      mnuAccounting.Enabled = post.IsPrivileged();

      noteBook1.UserID = Session.CurrentStaff.ID;
    }

    private void Desktop_Load(object sender, EventArgs e)
    {
      // Synchronization starts here
      Session.StartSynchronization(btnControlTower);

      // A blocking login prompt
      if (Session.CurrentStaff == null)
      {
        if (new FormLogin().ShowDialog() == DialogResult.Cancel)
        {
          Application.Exit();
          return;
        }
      }

      // Now non blockingly prompt for login whenever application is locked
      Session.OnSessionLocked += OnSessionLocked;

      // GameManager daemon starts here
      DB.GameManager.StartDaemon(false);

      // Show the form and its tray icon
      trayIcon.Icon = Properties.Resources.TrayIcon;
      trayIcon.BalloonTipTitle = Text;
      trayIcon.Visible = true;
      Wellcome();
    }

    private void Desktop_FormClosed(object sender, FormClosedEventArgs e)
    {
      try
      {
        // Application is exiting. Do not check for further sessions
        Session.OnSessionLocked -= OnSessionLocked;

        // Stop session synchronization
        Session.StopSynchronization();
      }
      catch
      {
      }
    }

    private void OnSessionLocked(Object sender, EventArgs e)
    {
      // Non blockingly prompt for user login
      // First close all open forms except the desktop and critical modules
      // Do not use Application.OpenForms directly.
      // It updates everytime a form closes
      List<Form> forms = new List<Form>();
      foreach (Form openfrm in Application.OpenForms)
        forms.Add(openfrm);

      // Now close them in reverse order
      for (int i = forms.Count - 1; i >= 0; --i)
        if (forms[i] == null || forms[i].IsDisposed)
          continue;
        else if (forms[i] is ControlTower.ControlPanel)
          continue;
        else if (forms[i] is ControlTower.FormGameStart)
          continue;
        else if (forms[i] is ControlTower.FormDeviceInteractions)
          continue;
        else if (forms[i] is Desktop)
          Hide();
        else
          forms[i].Close();

      // Tray icon menu
      trayIcon.ContextMenuStrip = null;
      trayIcon.ShowBalloonTip(11000, Text, "برنامه قفل شد", ToolTipIcon.Info);
      
      // Prompt login
      FormLogin frm = new FormLogin();
      frm.Desktop = this;
      frm.Show();

      // And wait for FormLogin to return its result!
    }

    private void Wellcome()
    {
      string str = Session.CurrentStaff.Gender ? "آقای " : "خانم ";
      str += Session.CurrentStaff.FullName + Environment.NewLine;
      str += "بازی نیروی ویژه در کنترل شماست" + Environment.NewLine;
      str += "خوش آمدید";
      trayIcon.ShowBalloonTip(5000, Text, str, ToolTipIcon.Info);
    }

    public void LoginAccepted()
    {
      Show();
      trayIcon.ContextMenuStrip = mnuTray;
      Wellcome();
    }

    public void LoginCancelled()
    {
      Close();
    }

    private void PanelClosed(Object sender, EventArgs e)
    {
      if (IsDisposed)
        return;
      else if (!Session.Open)
        return;
      else if(!Visible)
        Show();
    }

    private void ShowSpecificPanel(SessionAwareForm frm)
    {
      Hide();
      frm.FormClosed += PanelClosed;
      frm.Show();
    }

    private void btnCompetitions_Click(object sender, EventArgs e)
    {
      ShowSpecificPanel(Competitions.ControlPanel.Instance);
    }

    private void btnUsers_Click(object sender, EventArgs e)
    {
      ShowSpecificPanel(Users.ControlPanel.Instance);
    }

    private void btnTeams_Click(object sender, EventArgs e)
    {
      ShowSpecificPanel(Teams.ControlPanel.Instance);
    }

    private void btnSites_Click(object sender, EventArgs e)
    {
      ShowSpecificPanel(Sites.ControlPanel.Instance);
    }

    private void btnAccounting_Click(object sender, EventArgs e)
    {
      ShowSpecificPanel(Accounting.ControlPanel.Instance);
    }

    private void btnTakavar_Click(object sender, EventArgs e)
    {
      DB.Game game = DB.GameManager.CurrentGame;
      if (game == null || game.Rounds.Count == 0)
        Session.ShowMessage("هیچ بازی در حال اجرا نیست.");
      else if (game.Rounds[game.Rounds.Count - 1].State != RoundState.InProgress)
        Session.ShowMessage("هیچ راندی در حال اجرا نیست.");
      else
        ControlTower.ControlPanel.MonitorGame();
    }

    private void btnScheduling_Click(object sender, EventArgs e)
    {
      ShowSpecificPanel(Scheduling.ControlPanel.Instance);
    }

    private void btnSettings_Click(object sender, EventArgs e)
    {
      ShowSpecificPanel(Options.FormOptions.Instance);
    }

    private void mnuExit_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void mnuDesktop_Click(object sender, EventArgs e)
    {
      if (!Visible)
        Show();

      BringToFront();
      Focus();
    }

    private void mnuLock_Click(object sender, EventArgs e)
    {
      Session.Lock();
    }

    private void Desktop_FormClosing(object sender, FormClosingEventArgs e)
    {
      DB.Game game = DB.GameManager.CurrentGame;
      if (game != null && game.State == GameState.InProgress)
        Session.ShowError("در زمانیکه بازی در جریان است نمی توان برنامه را بست.");
      else
        return;

      e.Cancel = true;
    }

    private void trayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      mnuDesktop_Click(null, null);
    }
  }
}
