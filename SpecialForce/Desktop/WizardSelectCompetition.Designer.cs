﻿namespace SpecialForce.Desktop
{
  partial class WizardSelectCompetition
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WizardSelectCompetition));
      this.toolBar1 = new SpecialForce.ToolBar();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.lvCompetitions = new System.Windows.Forms.ListBox();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnOK = new System.Windows.Forms.Button();
      this.transparentLabel7 = new SpecialForce.TransparentLabel();
      this.toolBar1.SuspendLayout();
      this.SuspendLayout();
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 3F;
      this.toolBar1.Controls.Add(this.transparentLabel2);
      this.toolBar1.Controls.Add(this.transparentLabel1);
      this.toolBar1.CornerRadius = 10;
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(-11, -8);
      this.toolBar1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(471, 82);
      this.toolBar1.TabIndex = 65;
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.FixFromRight = true;
      this.transparentLabel2.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel2.Location = new System.Drawing.Point(254, 42);
      this.transparentLabel2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(191, 26);
      this.transparentLabel2.TabIndex = 1;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.FixFromRight = true;
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.Location = new System.Drawing.Point(367, 15);
      this.transparentLabel1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(87, 27);
      this.transparentLabel1.TabIndex = 0;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // lvCompetitions
      // 
      this.lvCompetitions.FormattingEnabled = true;
      this.lvCompetitions.Location = new System.Drawing.Point(36, 107);
      this.lvCompetitions.Name = "lvCompetitions";
      this.lvCompetitions.Size = new System.Drawing.Size(380, 173);
      this.lvCompetitions.TabIndex = 72;
      this.lvCompetitions.DoubleClick += new System.EventHandler(this.btnOK_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(365, 341);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 71;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // btnOK
      // 
      this.btnOK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnOK.ForeColor = System.Drawing.Color.Black;
      this.btnOK.Location = new System.Drawing.Point(284, 341);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 70;
      this.btnOK.Text = "گزارش";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // transparentLabel7
      // 
      this.transparentLabel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel7.Location = new System.Drawing.Point(-5, 314);
      this.transparentLabel7.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel7.Name = "transparentLabel7";
      this.transparentLabel7.Size = new System.Drawing.Size(451, 15);
      this.transparentLabel7.TabIndex = 69;
      this.transparentLabel7.TabStop = false;
      this.transparentLabel7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel7.Texts")));
      // 
      // WizardSelectCompetition
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.lvCompetitions);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOK);
      this.Controls.Add(this.transparentLabel7);
      this.Controls.Add(this.toolBar1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "WizardSelectCompetition";
      this.Text = "انتخاب مسابقه";
      this.Load += new System.EventHandler(this.WizardSelectCompetition_Load);
      this.toolBar1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private ToolBar toolBar1;
    private TransparentLabel transparentLabel2;
    private TransparentLabel transparentLabel1;
    private System.Windows.Forms.ListBox lvCompetitions;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnOK;
    private TransparentLabel transparentLabel7;
  }
}