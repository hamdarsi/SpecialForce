﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Teams
{
  public partial class FormReportCompetitions : SessionAwareForm
  {
    private DB.Team mTeam = null;
    public DB.Team Team
    {
      get { return mTeam; }
      set { mTeam = value; }
    }

    public FormReportCompetitions()
    {
      InitializeComponent();
    }

    private void FormReportCompetitions_Shown(object sender, EventArgs e)
    {
      report1.BeginUpdate();
      report1.Title = "گزارش فعالیت تیم " + mTeam.Name;

      Reports.Table table = report1.AddTable();
      table.AutoIndexColumn = true;
      table.AddNewColumn(Types.String, "مسابقه", 90);
      table.AddNewColumn(Types.String, "نوع", 50);
      table.AddNewColumn(Types.String, "شروع", 90);
      table.AddNewColumn(Types.String, "پایان", 90);
      table.AddNewColumn(Types.String, "رتبه", 100);
      table.OnRowDoubleClicked += RowDoubleClicked;

      Statistics<Stats.TeamInCompetition> stats = Stats.Generator.Generate(mTeam);
      foreach (Stats.TeamInCompetition st in stats.Data)
      {
        Reports.Row row = table.AddNewRow(st);
        row[0] = st.CompetitionName;
        row[1] = st.CompetitionType;
        row[2] = st.Start;
        row[3] = st.End;
        row[4] = st.Rank;
      }

      // fill in player information
      table = report1.AddTable(false, 90, 200);
      table.AddKeyValuePair("تاسیس", mTeam.RegistrationDate.ToString(DateTimeString.CompactDate));
      table.AddKeyValuePair("وضعیت", mTeam.IsActive() ? "فعال" : "تعداد به حد نصاب نیست");
      table.AddKeyValuePair("باشگاه خانگی", mTeam.RegistrationSite.FullName);
      table.AddKeyValuePair("اعضا", "");
      foreach (DB.User player in mTeam.ListMembers())
        table.AddKeyValuePair("", player.FullName);

      report1.EndUpdate();
    }

    private void RowDoubleClicked(object sender, ReportRowArgs e)
    {
      FormCompetitionDetailReport frm = new FormCompetitionDetailReport();
      Stats.TeamInCompetition stat = e.Data as Stats.TeamInCompetition;
      frm.Team = stat.TeamInstance;
      frm.Competition = stat.CompetitionInstance;
      ShowModal(frm, false);
    }
  }
}
