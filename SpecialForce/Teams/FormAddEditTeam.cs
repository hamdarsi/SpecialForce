﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using BrightIdeasSoftware;
using System.Windows.Forms;

namespace SpecialForce.Teams
{
  public partial class FormAddEditTeam : SessionAwareForm
  {
    private Bitmap mLogo = null;
    private int mTeamID = -1;
    private List<DB.User> mMembers = new List<DB.User>();

    public FormAddEditTeam(int teamid)
    {
      InitializeComponent();

      mTeamID = teamid;
      lvMembers.SetAspect(0, o => ((DB.User)o).FullName);
    }

    private void FormAddEditTeam_Load(object sender, EventArgs e)
    {
      if (mTeamID == -1)
      {
        Text = "ایجاد تیم جدید";
        mLogo = Properties.Resources.Team_Logo;
        return;
      }

      DB.Team team = DB.Team.GetTeamByID(mTeamID);
      Text = "ویرایش تیم " + team.Name;
      mLogo = new Bitmap(team.Logo);
      mMembers = team.ListMembers();

      txtName.Text = team.Name;
      lblDate.Text = team.RegistrationDate.ToString(DateTimeString.Date);
      btnLogo.Image = mLogo.Resize(btnLogo.Size);

      ReloadMembers();
    }

    private void ReloadMembers()
    {
      lvMembers.SetImages(0, mMembers.Select(user => user.Photo.Resize(64, 64)).ToArray());
      lvMembers.SetObjects(mMembers);
    }

    private void tbUserAdd_Click(object sender, EventArgs e)
    {
      Users.ControlPanel.Instance.ShowBounded(this);
    }

    private void lvMembers_DragEnter(object sender, DragEventArgs e)
    {
      if (e.Data.GetDataPresent(typeof(DB.User)))
        e.Effect = DragDropEffects.Move;
    }

    private void lvMembers_DragDrop(object sender, DragEventArgs e)
    {
      DB.User member = e.Data.GetData(typeof(DB.User)) as DB.User;

      // user already exists?
      foreach (object o in mMembers)
        if (member.ID == (o as DB.User).ID)
        {
          Session.ShowError(member.FirstName + ' ' + member.LastName + " در لیست وجود دارد.");
          return;
        }

      mMembers.Add(member);
      ReloadMembers();
    }

    private void tbUserRemove_Click(object sender, EventArgs e)
    {
      DB.User user = lvMembers.SelectedObject as DB.User;
      if (!mMembers.Contains(user))
        Session.ShowError("هیچ عضوی انتخاب نشده است.");
      else
      {
        mMembers.Remove(user);
        ReloadMembers();
      }
    }

    private void btnLogo_Click(object sender, EventArgs e)
    {
      if (OpenDialog.ShowDialog() == DialogResult.OK)
      {
        mLogo = new Bitmap(OpenDialog.FileName);
        btnLogo.Image = mLogo.Resize(btnLogo.Size);
      }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      DB.Team team;
      int size = DataBase.ImageToBlob(mLogo).Length * sizeof(byte);

      // team has user?
      string strName = txtName.Text.Trim();
      if (mMembers.Count < DB.Options.TeamMinimumMembers)
        Session.ShowError("تیم نمی تواند کمتر از " + DB.Options.TeamMinimumMembers.ToString() + " بازیکن داشته باشد.");
      else if (strName.Length == 0)
        Session.ShowError("نام تیم وارد نشده است.");
      else if (strName.Length > DB.Options.MaximumTeamNameLength)
        Session.ShowError("نام تیم بیش از حداکثر طول تعیین شده است.");
      else if (DB.Team.TeamExistsByName(strName, mTeamID))
        Session.ShowError("تیمی با نام ذکر شده وجود دارد.");
      else if (size > 200 * 1024)
        Session.ShowError("عکس انتخاب شده حجم بالایی دارد.");
      else
      {
        // check all users
        List<DB.User> members = new List<DB.User>();
        Dictionary<int, int> teams = new Dictionary<int, int>(); // TeamID => Member Count
        foreach(DB.User member in mMembers)
        {
          if (member.TeamID != -1 && member.TeamID != mTeamID)
          {
            // Get its dictionary instance
            if (!teams.ContainsKey(member.TeamID))
              teams[member.TeamID] = DB.Team.GetMemberCount(member.TeamID);

            // Decrease team member count
            teams[member.TeamID] = teams[member.TeamID] - 1;
            string str = member.FullName + " عضو تیم " + DB.Team.GetTeamName(member.TeamID);
            str += " است. آیا میخواهید او را از تیم خود خارج کنید؟";
            if (teams[member.TeamID] < DB.Options.TeamMinimumMembers)
            {
              str += Environment.NewLine;
              str += "در این صورت تعداد اعضا آن تیم از حد نصاب کم تر می شود";
            }

            if (!Session.Ask(str))
            {
              Session.ShowMessage("به روز رسانی تیم متوقف شد.");
              return;
            }
          }

          members.Add(member);
        }

        // assign team properties
        if (mTeamID == -1)
          team = DB.Team.CreateNewTeam(strName, mLogo);
        else
        {
          team = DB.Team.GetTeamByID(mTeamID);
          team.Name = strName;
          team.Logo = mLogo;
          team.Apply();
        }
        team.AssignMembers(members.Select(user => user.ID).ToList());
        DialogResult = System.Windows.Forms.DialogResult.OK;
      }
    }
  }
}
