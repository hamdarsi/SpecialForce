﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Teams
{
  public partial class FormCompetitionDetailReport : SessionAwareForm
  {
    private DB.Team mTeam = null;
    public DB.Team Team
    {
      get { return mTeam; }
      set { mTeam = value; }
    }

    private DB.Competition mCompetition = null;
    public DB.Competition Competition
    {
      get { return mCompetition; }
      set { mCompetition = value; }
    }

    public FormCompetitionDetailReport()
    {
      InitializeComponent();
    }

    private void FormCompetitionDetailReport_Shown(object sender, EventArgs e)
    {
      // Start
      report1.BeginUpdate();
      report1.Title = "گزارش بازی های تیم " + mTeam.Name + " در مسابقات " + mCompetition.Title;
      Reports.Table table = report1.AddTable();
      table.AutoIndexColumn = true;
      table.AddNewColumn(Types.String, "شروع", 100);
      table.AddNewColumn(Types.String, "پایان", 100);
      table.AddNewColumn(Types.String, "نوع", 90);
      table.AddNewColumn(Types.String, "هدف", 75);
      table.AddNewColumn(Types.String, "حریف", 100);
      table.AddNewColumn(Types.String, "نتیجه", 150);
      table.OnRowDoubleClicked += RowDoubleClicked;

      Statistics<Stats.TeamInGame> stats = Stats.Generator.Generate(mTeam, mCompetition);
      foreach (Stats.TeamInGame st in stats.Data)
      {
        Reports.Row row = table.AddNewRow(st.GameInstance);
        row[0] = st.Start;
        row[1] = st.End;
        row[2] = st.Type;
        row[3] = st.Object;
        row[4] = st.Opponent;
        row[5] = st.Result;
      }

      stats.PumpMetaData(report1.AddTable(false, 150, 300));

      // finished
      report1.EndUpdate();
    }

    private void RowDoubleClicked(object sender, ReportRowArgs e)
    {
      Scheduling.FormReportRounds frm = new Scheduling.FormReportRounds();
      frm.Game = e.Data as DB.Game;
      frm.Rounds = Session.CurrentStaff.IsLowlyUser() ? frm.Game.Rounds : frm.Game.AllRounds;
      ShowModal(frm, false);
    }
  }
}
