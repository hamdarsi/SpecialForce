﻿namespace SpecialForce.Teams
{
  partial class FormAddEditTeam
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAddEditTeam));
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnOK = new System.Windows.Forms.Button();
      this.OpenDialog = new System.Windows.Forms.OpenFileDialog();
      this.tbTeamInfo = new SpecialForce.ToolBar();
      this.lvMembers = new SpecialForce.UserInterface.ListView();
      this.colPhoto = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.btnUserAdd = new SpecialForce.ToolButton();
      this.btnUserRemove = new SpecialForce.ToolButton();
      this.btnLogo = new SpecialForce.ToolButton();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.txtName = new System.Windows.Forms.TextBox();
      this.transparentLabel5 = new SpecialForce.TransparentLabel();
      this.lblDate = new SpecialForce.TransparentLabel();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.tbTeamInfo.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lvMembers)).BeginInit();
      this.SuspendLayout();
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(349, 338);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 2;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnOK
      // 
      this.btnOK.ForeColor = System.Drawing.Color.Black;
      this.btnOK.Location = new System.Drawing.Point(430, 338);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 3;
      this.btnOK.Text = "تایید";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // OpenDialog
      // 
      this.OpenDialog.FileName = "openFileDialog1";
      this.OpenDialog.Filter = "Pictures|*.bmp;*.jpg;*.png;*.gif";
      this.OpenDialog.ShowReadOnly = true;
      this.OpenDialog.Title = "انتخاب لوگوی تیم";
      // 
      // tbTeamInfo
      // 
      this.tbTeamInfo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbTeamInfo.BorderWidth = 0.5F;
      this.tbTeamInfo.Controls.Add(this.lvMembers);
      this.tbTeamInfo.Controls.Add(this.btnUserAdd);
      this.tbTeamInfo.Controls.Add(this.btnUserRemove);
      this.tbTeamInfo.Controls.Add(this.btnLogo);
      this.tbTeamInfo.Controls.Add(this.transparentLabel2);
      this.tbTeamInfo.Controls.Add(this.txtName);
      this.tbTeamInfo.Controls.Add(this.transparentLabel5);
      this.tbTeamInfo.Controls.Add(this.lblDate);
      this.tbTeamInfo.Controls.Add(this.transparentLabel3);
      this.tbTeamInfo.Controls.Add(this.transparentLabel1);
      this.tbTeamInfo.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbTeamInfo.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbTeamInfo.Location = new System.Drawing.Point(12, 12);
      this.tbTeamInfo.Name = "tbTeamInfo";
      this.tbTeamInfo.Size = new System.Drawing.Size(493, 316);
      this.tbTeamInfo.TabIndex = 0;
      // 
      // lvMembers
      // 
      this.lvMembers.AllColumns.Add(this.colPhoto);
      this.lvMembers.AllowDrop = true;
      this.lvMembers.AutoIndexRow = false;
      this.lvMembers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colPhoto});
      this.lvMembers.ForeColor = System.Drawing.Color.Black;
      this.lvMembers.FullRowSelect = true;
      this.lvMembers.GridLines = true;
      this.lvMembers.IsSimpleDragSource = true;
      this.lvMembers.Location = new System.Drawing.Point(233, 99);
      this.lvMembers.Name = "lvMembers";
      this.lvMembers.OwnerDraw = true;
      this.lvMembers.RightToLeftLayout = true;
      this.lvMembers.ShowGroups = false;
      this.lvMembers.ShowHeaderInAllViews = false;
      this.lvMembers.Size = new System.Drawing.Size(243, 200);
      this.lvMembers.TabIndex = 22;
      this.lvMembers.TileSize = new System.Drawing.Size(168, 30);
      this.lvMembers.UseCompatibleStateImageBehavior = false;
      this.lvMembers.View = System.Windows.Forms.View.Tile;
      this.lvMembers.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvMembers_DragDrop);
      this.lvMembers.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvMembers_DragEnter);
      // 
      // colPhoto
      // 
      this.colPhoto.AspectName = "Photo";
      this.colPhoto.CellPadding = null;
      this.colPhoto.ImageAspectName = "";
      // 
      // btnUserAdd
      // 
      this.btnUserAdd.Hint = "افزودن کاربر به تیم";
      this.btnUserAdd.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnUserAdd.Image")));
      this.btnUserAdd.Location = new System.Drawing.Point(88, 11);
      this.btnUserAdd.Name = "btnUserAdd";
      this.btnUserAdd.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnUserAdd.Size = new System.Drawing.Size(64, 64);
      this.btnUserAdd.TabIndex = 10;
      this.btnUserAdd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnUserAdd.Texts")));
      this.btnUserAdd.TransparentColor = System.Drawing.Color.White;
      this.btnUserAdd.Click += new System.EventHandler(this.tbUserAdd_Click);
      // 
      // btnUserRemove
      // 
      this.btnUserRemove.Hint = "حذف کاربر از تیم";
      this.btnUserRemove.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnUserRemove.Image")));
      this.btnUserRemove.Location = new System.Drawing.Point(18, 11);
      this.btnUserRemove.Name = "btnUserRemove";
      this.btnUserRemove.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnUserRemove.Size = new System.Drawing.Size(64, 64);
      this.btnUserRemove.TabIndex = 11;
      this.btnUserRemove.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnUserRemove.Texts")));
      this.btnUserRemove.TransparentColor = System.Drawing.Color.White;
      this.btnUserRemove.Click += new System.EventHandler(this.tbUserRemove_Click);
      // 
      // btnLogo
      // 
      this.btnLogo.Hint = "برای تغییر لوگوی تیم اینجا کلیک کنید";
      this.btnLogo.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnLogo.Image")));
      this.btnLogo.Location = new System.Drawing.Point(18, 99);
      this.btnLogo.Name = "btnLogo";
      this.btnLogo.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnLogo.Size = new System.Drawing.Size(200, 200);
      this.btnLogo.TabIndex = 21;
      this.btnLogo.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnLogo.Texts")));
      this.btnLogo.TransparentColor = System.Drawing.Color.White;
      this.btnLogo.Click += new System.EventHandler(this.btnLogo_Click);
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Location = new System.Drawing.Point(167, 78);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(55, 15);
      this.transparentLabel2.TabIndex = 7;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // txtName
      // 
      this.txtName.Location = new System.Drawing.Point(262, 9);
      this.txtName.Name = "txtName";
      this.txtName.Size = new System.Drawing.Size(127, 21);
      this.txtName.TabIndex = 1;
      // 
      // transparentLabel5
      // 
      this.transparentLabel5.Location = new System.Drawing.Point(414, 78);
      this.transparentLabel5.Name = "transparentLabel5";
      this.transparentLabel5.Size = new System.Drawing.Size(62, 15);
      this.transparentLabel5.TabIndex = 4;
      this.transparentLabel5.TabStop = false;
      this.transparentLabel5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel5.Texts")));
      // 
      // lblDate
      // 
      this.lblDate.FixFromRight = true;
      this.lblDate.Location = new System.Drawing.Point(383, 43);
      this.lblDate.Name = "lblDate";
      this.lblDate.Size = new System.Drawing.Size(8, 15);
      this.lblDate.TabIndex = 3;
      this.lblDate.TabStop = false;
      this.lblDate.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblDate.Texts")));
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.Location = new System.Drawing.Point(409, 43);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(69, 15);
      this.transparentLabel3.TabIndex = 2;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Location = new System.Drawing.Point(437, 12);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(39, 15);
      this.transparentLabel1.TabIndex = 20;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // FormAddEditTeam
      // 
      this.AcceptButton = this.btnOK;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(519, 373);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOK);
      this.Controls.Add(this.tbTeamInfo);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormAddEditTeam";
      this.Text = "FormAddEditTeam";
      this.Load += new System.EventHandler(this.FormAddEditTeam_Load);
      this.tbTeamInfo.ResumeLayout(false);
      this.tbTeamInfo.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lvMembers)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private ToolButton btnUserRemove;
    private ToolButton btnUserAdd;
    private ToolBar tbTeamInfo;
    private TransparentLabel transparentLabel5;
    private TransparentLabel lblDate;
    private TransparentLabel transparentLabel3;
    private TransparentLabel transparentLabel1;
    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.TextBox txtName;
    private TransparentLabel transparentLabel2;
    private ToolButton btnLogo;
    private System.Windows.Forms.OpenFileDialog OpenDialog;
    private UserInterface.ListView lvMembers;
    private BrightIdeasSoftware.OLVColumn colPhoto;
  }
}