﻿namespace SpecialForce.Teams
{
  partial class ControlPanel
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPanel));
      this.label1 = new SpecialForce.TransparentLabel();
      this.txtSearch = new System.Windows.Forms.TextBox();
      this.tbButtons = new SpecialForce.ToolBar();
      this.btnExitForm = new SpecialForce.ToolButton();
      this.btnLockSystem = new SpecialForce.ToolButton();
      this.btnTeamReport = new SpecialForce.ToolButton();
      this.btnTeamEdit = new SpecialForce.ToolButton();
      this.btnTeamAdd = new SpecialForce.ToolButton();
      this.tinypicker1 = new SpecialForce.TinyPicker();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.lblHomeSite = new SpecialForce.TransparentLabel();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.tbTeamLogo = new SpecialForce.ToolButton();
      this.lblMembers = new SpecialForce.TransparentLabel();
      this.lblMembersText = new SpecialForce.TransparentLabel();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.cmbSite = new System.Windows.Forms.ComboBox();
      this.lvTeams = new SpecialForce.UserInterface.ListView();
      this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.tbButtons.SuspendLayout();
      this.toolBar1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lvTeams)).BeginInit();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(111, 17);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(44, 15);
      this.label1.TabIndex = 0;
      this.label1.TabStop = false;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // txtSearch
      // 
      this.txtSearch.Location = new System.Drawing.Point(163, 14);
      this.txtSearch.Name = "txtSearch";
      this.txtSearch.Size = new System.Drawing.Size(127, 21);
      this.txtSearch.TabIndex = 1;
      this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
      // 
      // tbButtons
      // 
      this.tbButtons.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbButtons.BorderWidth = 0.5F;
      this.tbButtons.Controls.Add(this.btnExitForm);
      this.tbButtons.Controls.Add(this.btnLockSystem);
      this.tbButtons.Controls.Add(this.btnTeamReport);
      this.tbButtons.Controls.Add(this.btnTeamEdit);
      this.tbButtons.Controls.Add(this.btnTeamAdd);
      this.tbButtons.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbButtons.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbButtons.Location = new System.Drawing.Point(12, 14);
      this.tbButtons.Name = "tbButtons";
      this.tbButtons.Size = new System.Drawing.Size(86, 372);
      this.tbButtons.TabIndex = 40;
      // 
      // btnExitForm
      // 
      this.btnExitForm.Hint = "خروج";
      this.btnExitForm.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnExitForm.Image")));
      this.btnExitForm.Location = new System.Drawing.Point(11, 233);
      this.btnExitForm.Name = "btnExitForm";
      this.btnExitForm.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnExitForm.Size = new System.Drawing.Size(64, 64);
      this.btnExitForm.TabIndex = 15;
      this.btnExitForm.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnExitForm.Texts")));
      this.btnExitForm.TransparentColor = System.Drawing.Color.White;
      this.btnExitForm.Visible = false;
      this.btnExitForm.Click += new System.EventHandler(this.btnExitForm_Click);
      // 
      // btnLockSystem
      // 
      this.btnLockSystem.Hint = "قفل سیستم";
      this.btnLockSystem.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnLockSystem.Image")));
      this.btnLockSystem.Location = new System.Drawing.Point(11, 300);
      this.btnLockSystem.Name = "btnLockSystem";
      this.btnLockSystem.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnLockSystem.Size = new System.Drawing.Size(64, 64);
      this.btnLockSystem.TabIndex = 14;
      this.btnLockSystem.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnLockSystem.Texts")));
      this.btnLockSystem.TransparentColor = System.Drawing.Color.White;
      this.btnLockSystem.Visible = false;
      this.btnLockSystem.Click += new System.EventHandler(this.btnLockSystem_Click);
      // 
      // btnTeamReport
      // 
      this.btnTeamReport.Hint = "گزارش های تیم";
      this.btnTeamReport.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnTeamReport.Image")));
      this.btnTeamReport.Location = new System.Drawing.Point(11, 151);
      this.btnTeamReport.Name = "btnTeamReport";
      this.btnTeamReport.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnTeamReport.Size = new System.Drawing.Size(64, 64);
      this.btnTeamReport.TabIndex = 13;
      this.btnTeamReport.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnTeamReport.Texts")));
      this.btnTeamReport.TransparentColor = System.Drawing.Color.White;
      this.btnTeamReport.Click += new System.EventHandler(this.btnTeamReport_Click);
      // 
      // btnTeamEdit
      // 
      this.btnTeamEdit.Hint = "ویرایش تیم";
      this.btnTeamEdit.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnTeamEdit.Image")));
      this.btnTeamEdit.Location = new System.Drawing.Point(11, 81);
      this.btnTeamEdit.Name = "btnTeamEdit";
      this.btnTeamEdit.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnTeamEdit.Size = new System.Drawing.Size(64, 64);
      this.btnTeamEdit.TabIndex = 12;
      this.btnTeamEdit.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnTeamEdit.Texts")));
      this.btnTeamEdit.TransparentColor = System.Drawing.Color.White;
      this.btnTeamEdit.Click += new System.EventHandler(this.btnTeamEdit_Click);
      // 
      // btnTeamAdd
      // 
      this.btnTeamAdd.Hint = "ثبت تیم جدید";
      this.btnTeamAdd.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnTeamAdd.Image")));
      this.btnTeamAdd.Location = new System.Drawing.Point(11, 11);
      this.btnTeamAdd.Name = "btnTeamAdd";
      this.btnTeamAdd.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnTeamAdd.Size = new System.Drawing.Size(64, 64);
      this.btnTeamAdd.TabIndex = 10;
      this.btnTeamAdd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnTeamAdd.Texts")));
      this.btnTeamAdd.TransparentColor = System.Drawing.Color.White;
      this.btnTeamAdd.Click += new System.EventHandler(this.btnTeamAdd_Click);
      // 
      // tinypicker1
      // 
      this.tinypicker1.Location = new System.Drawing.Point(12, 392);
      this.tinypicker1.Name = "tinypicker1";
      this.tinypicker1.Size = new System.Drawing.Size(438, 30);
      this.tinypicker1.TabIndex = 43;
      this.tinypicker1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tinypicker1.Texts")));
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 0.5F;
      this.toolBar1.Controls.Add(this.lblHomeSite);
      this.toolBar1.Controls.Add(this.transparentLabel3);
      this.toolBar1.Controls.Add(this.tbTeamLogo);
      this.toolBar1.Controls.Add(this.lblMembers);
      this.toolBar1.Controls.Add(this.lblMembersText);
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(111, 217);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(339, 169);
      this.toolBar1.TabIndex = 41;
      // 
      // lblHomeSite
      // 
      this.lblHomeSite.Location = new System.Drawing.Point(203, 11);
      this.lblHomeSite.Name = "lblHomeSite";
      this.lblHomeSite.Size = new System.Drawing.Size(128, 15);
      this.lblHomeSite.TabIndex = 17;
      this.lblHomeSite.TabStop = false;
      this.lblHomeSite.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblHomeSite.Texts")));
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.Location = new System.Drawing.Point(251, 14);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(0, 0);
      this.transparentLabel3.TabIndex = 16;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // tbTeamLogo
      // 
      this.tbTeamLogo.Hint = "";
      this.tbTeamLogo.Image = null;
      this.tbTeamLogo.Location = new System.Drawing.Point(10, 11);
      this.tbTeamLogo.Name = "tbTeamLogo";
      this.tbTeamLogo.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.tbTeamLogo.Size = new System.Drawing.Size(150, 150);
      this.tbTeamLogo.TabIndex = 15;
      this.tbTeamLogo.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tbTeamLogo.Texts")));
      this.tbTeamLogo.TransparentColor = System.Drawing.Color.White;
      // 
      // lblMembers
      // 
      this.lblMembers.FixFromRight = true;
      this.lblMembers.Location = new System.Drawing.Point(271, 49);
      this.lblMembers.Name = "lblMembers";
      this.lblMembers.Size = new System.Drawing.Size(42, 15);
      this.lblMembers.TabIndex = 1;
      this.lblMembers.TabStop = false;
      this.lblMembers.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblMembers.Texts")));
      // 
      // lblMembersText
      // 
      this.lblMembersText.Location = new System.Drawing.Point(295, 30);
      this.lblMembersText.Name = "lblMembersText";
      this.lblMembersText.Size = new System.Drawing.Size(29, 15);
      this.lblMembersText.TabIndex = 0;
      this.lblMembersText.TabStop = false;
      this.lblMembersText.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblMembersText.Texts")));
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Location = new System.Drawing.Point(296, 17);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(40, 15);
      this.transparentLabel2.TabIndex = 44;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // cmbSite
      // 
      this.cmbSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbSite.FormattingEnabled = true;
      this.cmbSite.Location = new System.Drawing.Point(342, 14);
      this.cmbSite.Name = "cmbSite";
      this.cmbSite.Size = new System.Drawing.Size(108, 21);
      this.cmbSite.TabIndex = 45;
      this.cmbSite.SelectedIndexChanged += new System.EventHandler(this.cmbSite_SelectedIndexChanged);
      // 
      // lvTeams
      // 
      this.lvTeams.AllColumns.Add(this.olvColumn1);
      this.lvTeams.AllColumns.Add(this.olvColumn2);
      this.lvTeams.AllColumns.Add(this.olvColumn3);
      this.lvTeams.AllowDrop = true;
      this.lvTeams.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this.olvColumn3});
      this.lvTeams.FullRowSelect = true;
      this.lvTeams.GridLines = true;
      this.lvTeams.IsSimpleDragSource = true;
      this.lvTeams.Location = new System.Drawing.Point(104, 41);
      this.lvTeams.Name = "lvTeams";
      this.lvTeams.RightToLeftLayout = true;
      this.lvTeams.ShowGroups = false;
      this.lvTeams.Size = new System.Drawing.Size(346, 170);
      this.lvTeams.TabIndex = 46;
      this.lvTeams.UseCompatibleStateImageBehavior = false;
      this.lvTeams.View = System.Windows.Forms.View.Details;
      this.lvTeams.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvTeams_ItemDrag);
      this.lvTeams.SelectedIndexChanged += new System.EventHandler(this.lvTeams_SelectedIndexChanged);
      this.lvTeams.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvTeams_MouseDoubleClick);
      // 
      // olvColumn1
      // 
      this.olvColumn1.CellPadding = null;
      this.olvColumn1.Text = "ردیف";
      this.olvColumn1.Width = 36;
      // 
      // olvColumn2
      // 
      this.olvColumn2.AspectName = "Name";
      this.olvColumn2.CellPadding = null;
      this.olvColumn2.Text = "نام";
      this.olvColumn2.Width = 121;
      // 
      // olvColumn3
      // 
      this.olvColumn3.CellPadding = null;
      this.olvColumn3.Text = "تاریخ تاسیس";
      this.olvColumn3.Width = 127;
      // 
      // ControlPanel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(466, 432);
      this.Controls.Add(this.lvTeams);
      this.Controls.Add(this.cmbSite);
      this.Controls.Add(this.transparentLabel2);
      this.Controls.Add(this.toolBar1);
      this.Controls.Add(this.tinypicker1);
      this.Controls.Add(this.tbButtons);
      this.Controls.Add(this.txtSearch);
      this.Controls.Add(this.label1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "ControlPanel";
      this.Text = "مدیریت تیم ها";
      this.Load += new System.EventHandler(this.ControlPanel_Load);
      this.tbButtons.ResumeLayout(false);
      this.toolBar1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.lvTeams)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private SpecialForce.TransparentLabel label1;
    private System.Windows.Forms.TextBox txtSearch;
    private ToolBar tbButtons;
    private TinyPicker tinypicker1;
    private ToolButton btnLockSystem;
    private ToolButton btnTeamReport;
    private ToolButton btnTeamEdit;
    private ToolButton btnTeamAdd;
    private ToolBar toolBar1;
    private TransparentLabel lblMembers;
    private TransparentLabel lblMembersText;
    private ToolButton tbTeamLogo;
    private TransparentLabel lblHomeSite;
    private TransparentLabel transparentLabel3;
    private TransparentLabel transparentLabel2;
    private System.Windows.Forms.ComboBox cmbSite;
    private ToolButton btnExitForm;
    private UserInterface.ListView lvTeams;
    private BrightIdeasSoftware.OLVColumn olvColumn1;
    private BrightIdeasSoftware.OLVColumn olvColumn2;
    private BrightIdeasSoftware.OLVColumn olvColumn3;

  }
}