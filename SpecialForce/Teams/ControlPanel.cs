﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Teams
{
  public partial class ControlPanel : SessionAwareForm
  {
    static private ControlPanel mInstance = null;
    static public ControlPanel Instance
    {
      get 
      {
        if (mInstance == null || mInstance.IsDisposed)
          mInstance = new ControlPanel();

        return mInstance;
      }
    }

    private ControlPanel()
    {
      InitializeComponent();
      lvTeams.SetAspect(2, (o) => { return ((DB.Team)o).RegistrationDate.ToString(DateTimeString.FullNumericDate); });
    }

    protected override void ApplyPermissions(Post post)
    {
      btnTeamAdd.Enabled = !post.IsUser();
      btnTeamEdit.Enabled = !post.IsUser();
    }

    private void ReloadTeams()
    {
      DB.Site site = cmbSite.SelectedItem as DB.Site;
      lvTeams.SetObjects(DB.Team.GetTeamsBySite(site != null ? site.ID : -1, txtSearch.Text));
      lvTeams_SelectedIndexChanged(null, null);
    }

    private void ControlPanel_Load(object sender, EventArgs e)
    {
      int index = 0;
      cmbSite.Items.Add("همه باشگاه ها");
      List<DB.Site> sites = DB.Site.GetAllSites(true);
      for (int i = 0; i < sites.Count; ++i)
      {
        cmbSite.Items.Add(sites[i]);
        if (sites[i].ID == Session.ActiveSiteID)
          index = i + 1;
      }

      // This also reloads teams
      cmbSite.SelectedIndex = index;

      btnLockSystem.Visible = !Properties.Settings.Default.KioskMode;
      btnExitForm.Visible = !btnLockSystem.Visible;
      btnExitForm.Location = btnLockSystem.Location;
    }

    private void btnTeamAdd_Click(object sender, EventArgs e)
    {
      FormAddEditTeam frm = new FormAddEditTeam(-1);
      if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        ReloadTeams();
    }

    private void btnTeamEdit_Click(object sender, EventArgs e)
    {
      DB.Team team = lvTeams.SelectedObject as DB.Team;
      if (Session.CurrentStaff.IsLowlyUser())
        return;

      if (team == null)
      {
        Session.ShowError("هیچ تیمی انتخاب نشده است.");
        return;
      }

      FormAddEditTeam frm = new FormAddEditTeam(team.ID);
      if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        ReloadTeams();
    }

    private void btnTeamReport_Click(object sender, EventArgs e)
    {
      DB.Team team = lvTeams.SelectedObject as DB.Team;
      if (team == null)
        Session.ShowError("هیچ تیمی انتخاب نشده است.");
      else
      {
        FormReportCompetitions frm = new FormReportCompetitions();
        frm.Team = team;
        ShowModal(frm, false);
      }
    }

    private void btnLockSystem_Click(object sender, EventArgs e)
    {
      Session.Lock();
    }

    private void txtSearch_TextChanged(object sender, EventArgs e)
    {
      int len = txtSearch.Text.Trim().Length;
      if (len > 2 || len == 0)
        ReloadTeams();
    }

    private void lvTeams_SelectedIndexChanged(object sender, EventArgs e)
    {
      DB.Team team = lvTeams.SelectedObject as DB.Team;

      if (team == null)
      {
        tbTeamLogo.Image = Properties.Resources.Team_Logo.Resize(tbTeamLogo.Size);
        lblMembers.Text = "تیمی را انتخاب کنید.";
        lblHomeSite.Text = "باشگاه خانگی: نام باشگاه";
      }
      else
      {
        List<string> names = team.ListMembers().Select(user => user.FullName).ToList();
        if (names.Count == 0)
          lblMembers.Text = "بدون عضو";
        else
          lblMembers.Texts = names;

        tbTeamLogo.Image = team.Logo.Resize(tbTeamLogo.Size);
        lblMembers.Visible = true;
        lblHomeSite.Text = "باشگاه خانگی: " + team.RegistrationSite.FullName;
        lblHomeSite.Visible = true;
      }

      lblHomeSite.Left = lblMembersText.Right - lblHomeSite.Width + 5;
    }

    private void cmbSite_SelectedIndexChanged(object sender, EventArgs e)
    {
      ReloadTeams();
    }

    private void btnExitForm_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.Cancel;
      Close();
    }

    private void lvTeams_ItemDrag(object sender, ItemDragEventArgs e)
    {
      DB.Team team = lvTeams.SelectedObject as DB.Team;

      if (team != null && team.IsActive())
        DoDragDrop(team, DragDropEffects.Move);
    }

    private void lvTeams_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      btnTeamEdit_Click(sender, e);
    }
  }
}
