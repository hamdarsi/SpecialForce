﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SpecialForce.Reports
{
  [DefaultEvent("OnRowDoubleClicked")]
  public partial class Table : UserControl
  {
    #region Internal Data
    private Bitmap mTableImage = null;
    private int mCurrentColumn = -1;
    private Rectangle mHighLightRect = Rectangle.Empty;
    private Brush mHighLightBrush = null;
    static Column mDefaultIndexColumn = new Column(-1, Types.Integer, 40);
    #endregion Internal Data


    #region Properties
    private Pen PenForBorder
    {
      get { return new Pen(new SolidBrush(Color.FromArgb(176, 176, 176))); }
    }

    private int mEvenRowsTaintAmount = 15;
    [DefaultValue(20)]
    public int TaintAmount
    {
      get { return mEvenRowsTaintAmount; }
      set { mEvenRowsTaintAmount = value; }
    }

    private bool mActive = true;
    [DefaultValue(true)]
    public bool Active
    {
      get { return mActive; }
      set { mActive = value; }
    }

    private int mRowHeight = 25;
    [DefaultValue(25)]
    public int RowHeight
    {
      get { return mRowHeight; }
      set { mRowHeight = value; DrawTable(); }
    }

    private bool mDrawGrid = true;
    [DefaultValue(true)]
    public bool DrawGrid
    {
      get { return mDrawGrid; }
      set { mDrawGrid = value; }
    }

    private bool mAutoIndexColumn = false;
    [DefaultValue(false)]
    public bool AutoIndexColumn
    {
      get { return mAutoIndexColumn; }
      set { mAutoIndexColumn = value; }
    }

    private Brush mHoverBrush = new SolidBrush(Color.FromArgb(50, 112, 146, 190));
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public Brush HoverBrush
    {
      get { return mHoverBrush; }
      set { mHoverBrush = value; }
    }

    private Brush mSelectedBrush = new SolidBrush(Color.FromArgb(100, 112, 146, 190));
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public Brush SelectedBrush
    {
      get { return mSelectedBrush; }
      set { mSelectedBrush = value; }
    }

    private int mSelectedRow = -1;
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public int SelectedRowIndex
    {
      get { return mSelectedRow; }
    }

    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public Row SelectedRow
    {
      get { return mSelectedRow != -1 ? mRows[mSelectedRow] : null; }
    }

    private List<Column> mColumns = new List<Column>();
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public List<Column> Columns
    {
      get { return mColumns; }
    }

    private List<Row> mRows = new List<Row>();
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public List<Row> Rows
    {
      get { return mRows; }
    }

    public int TopVisibleRowIndex
    {
      get;
      set;
    }

    public int BottomVisibleRowIndex
    {
      get;
      set;
    }

    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public Size RealSize
    {
      get
      {
        if (Parent is Report)
          return new Size((Parent as Report).TableWidthRule, mRows.Count * mRowHeight);
        else
          return new Size(Width, (mRows.Count + 1) * mRowHeight);
      }
    }
    #endregion Properties


    #region Events
    public event EventHandler<ReportRowArgs> OnRowClicked;
    public event EventHandler<ReportRowArgs> OnRowRightClicked;
    public event EventHandler<ReportRowArgs> OnRowDoubleClicked;
    #endregion Events


    #region Constructor
    static Table()
    {
      mDefaultIndexColumn.Text = "ردیف";
    }

    public Table()
    {
      BackColor = Color.White;
      ForeColor = Color.Black;
      Font = new Font("B Nazanin", 11, FontStyle.Regular);
      InitializeComponent();

      pbImage.Paint += PaintOnTable;
    }
    #endregion Constructor


    #region Sort Procedures
    private void SwapRows(int index1, int index2)
    {
      Row temp = mRows[index1];
      mRows[index1] = mRows[index2];
      mRows[index2] = temp;
    }

    private void QuickSort(int Left, int Right)
    {
      int LHold = Left;
      int RHold = Right;
      int Pivot = Left;
      ++Left;
 
      while (Right >= Left) 
      {
        if (mRows[Pivot][mCurrentColumn] < mRows[Left][mCurrentColumn] && mRows[Right][mCurrentColumn] < mRows[Pivot][mCurrentColumn])
          SwapRows(Left, Right);
        else if (mRows[Pivot][mCurrentColumn] < mRows[Left][mCurrentColumn])
          --Right;
        else if (mRows[Right][mCurrentColumn] < mRows[Pivot][mCurrentColumn])
          ++Left;
        else
        {
          --Right;
          ++Left;
        }
      } 
 
      SwapRows(Pivot, Right);
      Pivot = Right; 
      if (Pivot > LHold)
        QuickSort(LHold, Pivot);
      if (RHold > Pivot+1)
        QuickSort(Pivot+1, RHold);
    }

    public void Sort(int column = -1, bool ascend = true)
    {
      // If parameters are specified, then use them.
      if (column != -1)
      {
        mCurrentColumn = column;
        mColumns[mCurrentColumn].SortAscending = ascend;
      }

      // Nothing to do if zero or one rows only
      if (mRows.Count <= 1)
        return;

      // Sort the table
      QuickSort(0, mRows.Count - 1);

      // Reverse the list for descending sorts
      if (!mColumns[mCurrentColumn].SortAscending)
        for (int i = 0; i < mRows.Count / 2; ++i)
          SwapRows(i, mRows.Count - 1 - i);

      // Now draw the table again.
      DrawTable();
    }
    #endregion Sort Procedures


    #region Event Handlers
    protected override void OnResize(EventArgs e)
    {
      base.OnResize(e);
      pbImage.Size = Size;
    }

    private void PaintOnTable(object sender, PaintEventArgs e)
    {
      if (mHighLightRect != Rectangle.Empty && mHighLightBrush != null)
        e.Graphics.FillRectangle(mHoverBrush, mHighLightRect);
    }

    private void pbImage_MouseMove(object sender, MouseEventArgs e)
    {
      if (!mActive)
        return;

      int index = e.Y / mRowHeight;
      Bitmap bmp = mTableImage;
      mCurrentColumn = -1;

      // column's row -> get the column index
      if (index == 0)
      {
        int right = Width - 1;
        for (int i = -1; i < mColumns.Count; ++i)
        {
          Column column = GetColumn(i);
          if (column == null)
            continue;

          if (e.X <= right && e.X >= right - column.Width)
          {
            if (i != -1)
              mCurrentColumn = i;

            HighLight(mHoverBrush, new Rectangle(right - column.Width, 0, column.Width, mRowHeight));
            break;
          }
          else
            right -= column.Width;
        }
      }
      else if (index <= mRows.Count) // rows. get the row index
        HighLight(mHoverBrush, new Rectangle(0, index * mRowHeight, Width, mRowHeight));
    }

    private void pbImage_MouseDown(object sender, MouseEventArgs e)
    {
      if (!mActive)
        return;

      mSelectedRow = Math.Max(-1, e.Y / mRowHeight - 1);
      mSelectedRow = Math.Min(mSelectedRow, mRows.Count - 1);
      mSelectedRow += TopVisibleRowIndex;
      if (mSelectedRow != -1)
      {
        EventHandler<ReportRowArgs> ev = null;
        if (e.Button == MouseButtons.Left)
          ev = OnRowClicked;
        else if (e.Button == MouseButtons.Right)
          ev = OnRowRightClicked;

        if (ev != null)
          ev(this, new ReportRowArgs(mRows[mSelectedRow].Tag));
      }
      else if (mCurrentColumn != -1)
      {
        // sort the table based on the selected column
        mColumns[mCurrentColumn].SortAscending = !mColumns[mCurrentColumn].SortAscending;
        Sort();
      }

      DrawTable();
    }

    private void pbImage_DoubleClick(object sender, EventArgs e)
    {
      if (!mActive)
        return;

      if (mSelectedRow != -1)
      {
        EventHandler<ReportRowArgs> ev = OnRowDoubleClicked;
        if (ev != null)
          ev(this, new ReportRowArgs(mRows[mSelectedRow].Tag));
      }
    }
    #endregion Event Handlers


    #region Utilities
    private void HighLight(Brush brush, Rectangle rect)
    {
      // First, repaint the previous area with the actual image
      mHighLightBrush = null;
      pbImage.Invalidate(mHighLightRect, true);
      pbImage.Update();

      // Then highlight the new area
      mHighLightRect = rect;
      mHighLightBrush = brush;
      pbImage.Invalidate(rect, true);
    }

    public int _GetColumnIndex(string column_name)
    {
      for (int i = 0; i < mColumns.Count; ++i)
        if (mColumns[i].Text == column_name)
          return i;

      return -1;
    }

    public Column _GetColumn(string column_name)
    {
      return mColumns[_GetColumnIndex(column_name)];
    }

    public Column AddNewColumn(System.Type type, string caption, int width)
    {
      Column c = new Column(mColumns.Count, type, width);
      c.Text = caption;
      mColumns.Add(c);
      return c;
    }

    public Row AddNewRow(Object tag = null)
    {
      Row r = new Row(this, tag);
      mRows.Add(r);
      return r;
    }

    public void AddKeyValuePair(string strKey, string strValue)
    {
      if (mColumns.Count != 2 || mColumns[0].Type != Types.String || mColumns[1].Type != Types.String)
        throw new Exception("(Reports.Row) Table now suitable to add dictionary entry");

      Row row = AddNewRow(null);
      row[0] = strKey == "" ? "" : strKey + ':';
      row[1] = strValue;
    }

    private Column GetColumn(int index)
    {
      if (index != -1)
        return mColumns[index];

      if(mAutoIndexColumn)
        return mDefaultIndexColumn;

      return null;
    }
    #endregion Utilities


    #region Graphics
    private void DrawLine(Graphics canvas, int y)
    {
      if(mDrawGrid)
        canvas.DrawLine(PenForBorder, 0, y, Width, y);
    }

    public void DrawTable(Graphics output = null)
    {
      int width_rule = Parent is Report ? (Parent as Report).TableWidthRule : Width;
      int num_rows = BottomVisibleRowIndex - TopVisibleRowIndex + 1;
      if(mActive)
        ++num_rows;

      Size = new Size(width_rule, num_rows * mRowHeight);
      mTableImage = new Bitmap(Width, Height);
      Graphics canvas = Graphics.FromImage(mTableImage);
      canvas.Clear(BackColor);
      
      //draw column headers
      TextRenderer tr = new TextRenderer();
      tr.Canvas = canvas;
      tr.Font = Font;
      tr.RightToLeft = RightToLeft != System.Windows.Forms.RightToLeft.No;
      tr.Top = 0;

      int right = Width - 1;
      bool draw_header = false;
      foreach (Column c in mColumns)
        if (c.Text != "")
        {
          draw_header = true;
          break;
        }

      if (draw_header)
      {
        // Draw column titles
        for (int i = -1; i < mColumns.Count; ++i)
        {
          Column column = GetColumn(i);
          if (column == null)
            continue;

          // fill header background
          if (column.FillMode == FillingMode.Solid)
            canvas.FillRectangle(new SolidBrush(column.BackColor), right - column.Width, 0, column.Width, mRowHeight);
          else if (column.FillMode == FillingMode.Hatched)
            canvas.FillRectangle(new HatchBrush(HatchStyle.BackwardDiagonal, column.BackColor, BackColor), right - column.Width, 0, column.Width, mRowHeight);
          else if (column.FillMode == FillingMode.ElegantSolid)
            canvas.FillRectangle(new SolidBrush(column.BackColor), right - column.Width, 0, column.Width - 10, mRowHeight);

          // draw header text
          tr.Justify = column.CaptionAlign;
          tr.Text = column.Text;
          tr.Left = right - column.Width;
          tr.Color = column.ForeColor;
          tr.Size = new System.Drawing.Size(column.Width, mRowHeight);
          tr.Draw();
          right = tr.Left;
        }
      }

      // paint table inside
      int top = mActive ? mRowHeight : 0;
      for (int i = TopVisibleRowIndex; i <= BottomVisibleRowIndex; ++i)
      {
        // draw background
        if (i != mRows.Count)
        {
          canvas.FillRectangle(mRows[i].BackBrush, 0, top, Width, mRowHeight);

          if (i == mSelectedRow)
            canvas.FillRectangle(mSelectedBrush, 0, top, Width, mRowHeight);
          else if (i % 2 == 1 && mDrawGrid)
            canvas.FillRectangle(new SolidBrush(Color.FromArgb(mEvenRowsTaintAmount, 0, 0, 0)), 0, top, Width, mRowHeight);

          right = Width - 1;
          for (int j = -1; j < mColumns.Count; ++j)
          {
            Column column = GetColumn(j);
            if (column == null)
              continue;

            if (j == -1)
              tr.Text = (i + 1).ToString();
            else if (column.Type == Types.DateTime)
              tr.Text = mRows[i][j].ToString(column.DateTimeMode);
            else
            {
              String[] newline = new String[] { Environment.NewLine };
              String [] ss = mRows[i][j].ToString().Split(newline, StringSplitOptions.RemoveEmptyEntries);
              tr.Text = ss.Length > 0 ? ss[0] : "";
            }

            tr.Justify = column.CellAlign;
            tr.Left = right - column.Width;
            tr.Top = top;
            tr.Size = new System.Drawing.Size(column.Width, mRowHeight);
            canvas.SetClip(new Rectangle(tr.Location, tr.Size));
            tr.Draw();
            canvas.ResetClip();
            right = tr.Left;
          }
        }

        // draw border
        DrawLine(canvas, top);
        top += mRowHeight;
      }

      // Draw bottom line for the table
      DrawLine(canvas, top);

      // assign image
      pbImage.Size = Size;
      pbImage.Image = mTableImage;
      pbImage.Refresh();

      // Copy to output if needed
      if(output != null)
        output.DrawImageUnscaled(Left, Top, mTableImage);
    }
    #endregion Graphics
  }
}
