﻿using System;

namespace SpecialForce.Reports
{
  public class MetaItem
  {
    public enum ItemTypes
    {
      Table,
      Seperator
    }

    public ItemTypes ItemType
    {
      get;
      private set;
    }

    public int Height
    {
      get;
      private set;
    }

    public Table Table
    {
      get;
      private set;
    }

    public int TopRowIndex
    {
      get;
      private set;
    }

    public int BottomRowIndex
    {
      get;
      private set;
    }

    private MetaItem()
    { }

    static public MetaItem CreateSeperator(int height)
    {
      MetaItem item = new MetaItem();
      item.ItemType = ItemTypes.Seperator;
      item.Height = height;
      return item;
    }

    static public MetaItem CreateTable(Table tbl, int srow, int erow, int height)
    {
      MetaItem item = new MetaItem();
      item.ItemType = ItemTypes.Table;
      item.Table = tbl;
      item.TopRowIndex = srow;
      item.BottomRowIndex = erow;
      item.Height = height;
      return item;
    }
  }
}
