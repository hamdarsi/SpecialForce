﻿namespace SpecialForce.Reports
{
  partial class Report
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Report));
      this.SaveDialog = new System.Windows.Forms.SaveFileDialog();
      this.txtPageNumber = new System.Windows.Forms.TextBox();
      this.btnFirstPage = new SpecialForce.ToolButton();
      this.btnPrevPage = new SpecialForce.ToolButton();
      this.btnNextPage = new SpecialForce.ToolButton();
      this.btnLastPage = new SpecialForce.ToolButton();
      this.btnPrint = new SpecialForce.ToolButton();
      this.SuspendLayout();
      // 
      // SaveDialog
      // 
      this.SaveDialog.Filter = "Portable Network Graphics|*.png";
      this.SaveDialog.FilterIndex = 0;
      this.SaveDialog.Title = "ذخیره گزارش";
      // 
      // txtPageNumber
      // 
      this.txtPageNumber.Location = new System.Drawing.Point(51, 43);
      this.txtPageNumber.Name = "txtPageNumber";
      this.txtPageNumber.Size = new System.Drawing.Size(34, 30);
      this.txtPageNumber.TabIndex = 17;
      this.txtPageNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPageNumber_KeyDown);
      // 
      // btnFirstPage
      // 
      this.btnFirstPage.Hint = "صفحه اول";
      this.btnFirstPage.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnFirstPage.Image")));
      this.btnFirstPage.Location = new System.Drawing.Point(100, 79);
      this.btnFirstPage.Name = "btnFirstPage";
      this.btnFirstPage.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnFirstPage.ShowBorder = false;
      this.btnFirstPage.Size = new System.Drawing.Size(24, 24);
      this.btnFirstPage.TabIndex = 16;
      this.btnFirstPage.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnFirstPage.Texts")));
      this.btnFirstPage.TransparentColor = System.Drawing.Color.Transparent;
      this.btnFirstPage.Click += new System.EventHandler(this.btnFirstPage_Click);
      // 
      // btnPrevPage
      // 
      this.btnPrevPage.Hint = "صفحه قبلی";
      this.btnPrevPage.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnPrevPage.Image")));
      this.btnPrevPage.Location = new System.Drawing.Point(70, 79);
      this.btnPrevPage.Name = "btnPrevPage";
      this.btnPrevPage.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnPrevPage.ShowBorder = false;
      this.btnPrevPage.Size = new System.Drawing.Size(24, 24);
      this.btnPrevPage.TabIndex = 16;
      this.btnPrevPage.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnPrevPage.Texts")));
      this.btnPrevPage.TransparentColor = System.Drawing.Color.Transparent;
      this.btnPrevPage.Click += new System.EventHandler(this.btnPrevPage_Click);
      // 
      // btnNextPage
      // 
      this.btnNextPage.Hint = "صفحه بعدی";
      this.btnNextPage.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnNextPage.Image")));
      this.btnNextPage.Location = new System.Drawing.Point(40, 79);
      this.btnNextPage.Name = "btnNextPage";
      this.btnNextPage.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnNextPage.ShowBorder = false;
      this.btnNextPage.Size = new System.Drawing.Size(24, 24);
      this.btnNextPage.TabIndex = 16;
      this.btnNextPage.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnNextPage.Texts")));
      this.btnNextPage.TransparentColor = System.Drawing.Color.Transparent;
      this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
      // 
      // btnLastPage
      // 
      this.btnLastPage.Hint = "صفحه آخر";
      this.btnLastPage.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnLastPage.Image")));
      this.btnLastPage.Location = new System.Drawing.Point(10, 79);
      this.btnLastPage.Name = "btnLastPage";
      this.btnLastPage.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnLastPage.ShowBorder = false;
      this.btnLastPage.Size = new System.Drawing.Size(24, 24);
      this.btnLastPage.TabIndex = 16;
      this.btnLastPage.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnLastPage.Texts")));
      this.btnLastPage.TransparentColor = System.Drawing.Color.Transparent;
      this.btnLastPage.Click += new System.EventHandler(this.btnLastPage_Click);
      // 
      // btnPrint
      // 
      this.btnPrint.Hint = "ذخیره گزارش";
      this.btnPrint.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnPrint.Image")));
      this.btnPrint.Location = new System.Drawing.Point(10, 433);
      this.btnPrint.Name = "btnPrint";
      this.btnPrint.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnPrint.Size = new System.Drawing.Size(48, 48);
      this.btnPrint.TabIndex = 15;
      this.btnPrint.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnPrint.Texts")));
      this.btnPrint.TransparentColor = System.Drawing.Color.Transparent;
      this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
      // 
      // Report
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.White;
      this.Controls.Add(this.txtPageNumber);
      this.Controls.Add(this.btnFirstPage);
      this.Controls.Add(this.btnPrevPage);
      this.Controls.Add(this.btnNextPage);
      this.Controls.Add(this.btnLastPage);
      this.Controls.Add(this.btnPrint);
      this.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.ForeColor = System.Drawing.Color.Black;
      this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.Name = "Report";
      this.Size = new System.Drawing.Size(712, 488);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private ToolButton btnPrint;
    private System.Windows.Forms.SaveFileDialog SaveDialog;
    private ToolButton btnLastPage;
    private ToolButton btnNextPage;
    private ToolButton btnPrevPage;
    private ToolButton btnFirstPage;
    private System.Windows.Forms.TextBox txtPageNumber;


  }
}
