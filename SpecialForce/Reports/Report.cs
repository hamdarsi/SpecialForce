﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing.Printing;
using System.Collections.Generic;

namespace SpecialForce.Reports
{
  public partial class Report : UserControl
  {
    #region InternalData
    private bool mCreatingReport = false;
    private bool mPrintingReport = false;
    private int mCurrentPage = 0;
    private string mSiteAddress = "";
    private string mSitePhone = "";
    private string mDate = "";
    private string mOperator = "";
    private Bitmap mFooter = null;
    private Bitmap mHeader = null;
    private Size mOldSize = Size.Empty;
    private int WorkingHeight = 800;
    private List<Container> mPages = new List<Container>();

    private int ClipTop
    {
      get { return 150; }
    }

    private int ClipBottom
    {
      get { return FooterTop - 20; }
    }

    private int ClipHeight
    {
      get { return ClipBottom - ClipTop; }
    }

    private int FooterTop
    {
      get { return WorkingHeight - 158; }
    }
    #endregion InternalData


    #region Properties
    private int mTableMargins = 30;
    [DefaultValue(30)]
    public int TableMargins
    {
      get { return mTableMargins; }
      set { mTableMargins = value; }
    }

    private int mPageMargin = 20;
    [DefaultValue(20)]
    public int PageMargin
    {
      get { return mPageMargin; }
      set { mPageMargin = value; }
    }

    private string mTitle = "بدون عنوان";
    [DefaultValue("بدون عنوان")]
    public string Title
    {
      get { return mTitle; }
      set { mTitle = value; }
    }

    [DefaultValue(712)]
    public int TableWidthRule
    {
      get { return 712; }
    }

    private List<Table> mTables = new List<Table>();
    protected List<Table> Tables
    {
      get { return mTables; }
    }

    public int TableCount
    {
      get { return mTables.Count; }
    }

    public int PageCount
    {
      get { return mPages.Count; }
    }
    #endregion Properties


    #region Constructor
    public Report()
    {
      if (Session.DevelopMode)
      {
        mSiteAddress = "آدرس: -";
        mSitePhone = "تلفن: -";
        mDate = "برنامه در حال اجرا نیست";
        mOperator = "برنامه در حال اجرا نیست";
      }
      else
      {
        DB.Site site = DB.Site.GetSiteByID(Session.ActiveSiteID);
        mSiteAddress = "آدرس: " + site.Address;
        mSitePhone = "تلفن: " + site.Telephone;
        mDate = PersianDateTime.Today.ToString(DateTimeString.CompactDate);
        mOperator = Session.CurrentStaff.FullName;
      }


      Size = new System.Drawing.Size(TableWidthRule + 2 * mTableMargins, WorkingHeight);
      MinimumSize = new Size(0, 400); // Disable resizing to less than 400 height
      InitializeComponent();
      AutoScroll = false; // Force disable AutoScroll
    }
    #endregion Constructor


    #region Event Handlers
    private void CreateHeaderAndFooter()
    {
      // Draw header image
      // =================
      Bitmap header_image = Properties.Resources.Report_Company_Logo_Small;
      mHeader = new Bitmap(Width, ClipTop);
      Graphics canvas = Graphics.FromImage(mHeader);
      canvas.Clear(Color.White);

      // Draw company logo
      canvas.DrawImage(header_image, new Rectangle(10, 0, header_image.Width, header_image.Height), new Rectangle(0, 0, header_image.Width, header_image.Height), GraphicsUnit.Pixel);

      // Draw line under company logo
      canvas.DrawLine(new Pen(new SolidBrush(Color.FromArgb(105, 105, 105))), 10, 37, 481, 37);

      // Draw date
      int top = 50;
      TextRenderer tr = new TextRenderer();
      tr.Canvas = canvas;
      tr.Color = Color.Black;
      tr.Font = new Font("B Nazanin", 11.25f);
      tr.Text = "تاریخ:";
      tr.Location = new Point(Width - mTableMargins - tr.TextWidth - 10, top);
      tr.Draw();
      tr.Text = mDate;
      tr.Location = new Point(Width - mTableMargins - tr.TextWidth - 50, top);
      tr.Draw();

      // Draw title
      top += tr.TextHeight;
      tr.Text = "عنوان:";
      tr.Location = new Point(Width - mTableMargins - tr.TextWidth - 10, top);
      tr.Draw();
      tr.Text = mTitle;
      tr.Location = new Point(Width - mTableMargins - tr.TextWidth - 50, top);
      tr.Draw();

      // Draw operator
      if (Session.DevelopMode || Session.CurrentStaff.Post.IsUser() == false)
      {
        top += tr.TextHeight;
        tr.Text = "اپراتور:";
        tr.Location = new Point(Width - mTableMargins - tr.TextWidth - 10, top);
        tr.Draw();
        tr.Text = mOperator;
        tr.Location = new Point(Width - mTableMargins - tr.TextWidth - 50, top);
        tr.Draw();
      }

      // Draw page number
      if(mPrintingReport)
      { 
        tr.Text = "صفحه " + (mCurrentPage + 1).ToString() + " از " + PageCount.ToString();
        tr.Location = new Point(10, 40);
        tr.Draw();
      }
      else
      { 
        // Draw page number
        tr.Text = "صفحه:";
        tr.Location = new Point(89, 47);
        tr.Draw();
        tr.Text = "از " + PageCount.ToString();
        tr.Location = new Point(txtPageNumber == null ? 20 : txtPageNumber.Left - tr.TextWidth - 5, 46);
        tr.Draw();
      }


      // Draw footer image
      // =================
      Bitmap footer_image = Properties.Resources.Report_Footer;
      Bitmap logo_image = Properties.Resources.Report_Game_Logo_Small;
      top = FooterTop;

      mFooter = new Bitmap(Width, WorkingHeight - FooterTop);
      canvas = Graphics.FromImage(mFooter);
      canvas.Clear(Color.White);
      canvas.DrawImage(logo_image, new Rectangle((Width - logo_image.Width) / 2, 0, logo_image.Width, logo_image.Height), new Rectangle(0, 0, logo_image.Width, logo_image.Height), GraphicsUnit.Pixel);
      canvas.DrawImage(footer_image, new Rectangle(0, mFooter.Height - footer_image.Height, Width, footer_image.Height), new Rectangle(0, 0, footer_image.Width, footer_image.Height), GraphicsUnit.Pixel);

      // Game name label under the game logo
      top = logo_image.Height + 2;
      tr.Canvas = canvas;
      tr.Font = new Font("B Nazanin", 11.25f, FontStyle.Bold);
      tr.Text = "نیروی ویژه";
      tr.Location = new Point((Width - tr.TextWidth) / 2, top);
      tr.Draw();

      // Game site address
      tr.Color = Color.White;
      tr.Font = new Font("B Nazanin", 11.25f);
      top += tr.TextHeight + 7;
      tr.Text = mSiteAddress;
      tr.Location = new Point(Width - tr.TextWidth - 10, top);
      tr.Draw();

      // Print button
      if (btnPrint != null)
      {
        btnPrint.Left = 7;
        btnPrint.Top = WorkingHeight - btnPrint.Height - 6;
      }

      // Site telephone number
      top += tr.TextHeight + 2;
      tr.Text = mSitePhone;
      tr.Location = new Point(Width - tr.TextWidth - 10, top);
      tr.Draw();

      // Game web site address
      tr.RightToLeft = false;
      tr.Font = new Font("Arial", 11.25f, FontStyle.Italic);
      tr.Text = "http://www.spforce.ir/";
      tr.Location = new Point(mPrintingReport ? 10 : 65, top);
      tr.Draw();
    }

    protected override void OnResize(EventArgs e)
    {
      base.OnResize(e);
      if (Size != mOldSize)
      { 
        CreateHeaderAndFooter();
        mOldSize = Size;
      }
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);

      if (mHeader != null)
        e.Graphics.DrawImageUnscaled(0, 0, mHeader);

      if (mFooter != null)
        e.Graphics.DrawImageUnscaled(0, WorkingHeight - mFooter.Height, mFooter);
    }

    private void btnFirstPage_Click(object sender, EventArgs e)
    {
      mCurrentPage = 0;
      CreateCurrentPage();
    }

    private void btnPrevPage_Click(object sender, EventArgs e)
    {
      mCurrentPage = Math.Max(mCurrentPage - 1, 0);
      CreateCurrentPage();
    }

    private void btnNextPage_Click(object sender, EventArgs e)
    {
      mCurrentPage = Math.Min(mCurrentPage + 1, PageCount - 1);
      CreateCurrentPage();
    }

    private void btnLastPage_Click(object sender, EventArgs e)
    {
      mCurrentPage = PageCount - 1;
      CreateCurrentPage();
    }

    private void txtPageNumber_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode != Keys.Enter)
        return;

      e.Handled = true;
      e.SuppressKeyPress = true;
      try
      {
        mCurrentPage = Convert.ToInt32(txtPageNumber.Text) - 1;
        CreateCurrentPage();
      }
      catch
      { }
    }
    #endregion Event Handlers


    #region Graphics
    public void BeginUpdate(bool clear = true)
    {
      SuspendLayout();
      Application.UseWaitCursor = true;
      Application.DoEvents();
      if (clear)
        Clear();

      mCreatingReport = true;
    }

    public void EndUpdate()
    {
      // set state to finished updating
      mCreatingReport = false;

      // Fit all data in the report
      mPages.Clear();
      Container page = new Container(ClipHeight);
      mPages.Add(page);
      for(int i = 0; i < mTables.Count; ++i)
      {
        if(mTables[i].Rows.Count == 0)
          continue;

        int current_table_row = 0;
        while(current_table_row < mTables[i].Rows.Count)
        {
          int page_row_capacity = page.RemainingCapacity / mTables[i].RowHeight;
          int table_remaining_rows = mTables[i].Rows.Count - current_table_row;
          int header_rows = mTables[i].Active ? 1 : 0;
          int num_rows = Math.Min(page_row_capacity, table_remaining_rows);
          if(num_rows == page_row_capacity)
            num_rows -= header_rows;
          int used_height = (header_rows + num_rows) * mTables[i].RowHeight;

          // Whether the table was empty (which is handled in the for's begining
          // Or there was not sufficient space which is also not possible!
          if(num_rows == 0)
            throw new Exception("(Reports.Report) The impossible has happened! num_rows = 0");

          // Add the table's data to the page
          page.AddItem(MetaItem.CreateTable(mTables[i], current_table_row, current_table_row + num_rows - 1, used_height));

          // And update current table status
          current_table_row += num_rows;
          table_remaining_rows = mTables[i].Rows.Count - current_table_row;

          // Now that table's data is added to the page, if the table was depleted add a seperator
          if (table_remaining_rows == 0)
          {
            used_height = Math.Min(page.RemainingCapacity, mTableMargins);
            page.AddItem(MetaItem.CreateSeperator(used_height));
          }

          // Check whether there is enough space on the page for or should create a new one
          int current_row_height = mTables[i].RowHeight;
          if (table_remaining_rows == 0 && i < mTables.Count - 1)
            current_row_height = mTables[i + 1].RowHeight;
          if (page.RemainingCapacity < 2 * current_row_height)
          { 
            page = new Container(ClipHeight);
            mPages.Add(page);
          }
        }
      }

      // Reset size
      if(Width != TableWidthRule + 2 * mPageMargin || Height != WorkingHeight)
        Size = new Size(TableWidthRule + 2 * mPageMargin, WorkingHeight);
      else
        CreateHeaderAndFooter();

      // Now draw them all
      mCurrentPage = 0;
      CreateCurrentPage();

      // Finished
      ResumeLayout();

      // Control parent form's minimum width
      Form frm = Parent as Form;
      if(frm != null)
      { 
        frm.Width = Math.Max(frm.Width, Width + 30);
        frm.AutoScrollPosition = new Point(0, 0);
      }

      Application.UseWaitCursor = false;
      Application.DoEvents();
    }

    public void CreateCurrentPage()
    {
      if(mCurrentPage >= mPages.Count)
        return;

      int height = 0;
      mTables.ForEach(table => table.Visible = false);
      foreach(MetaItem item in mPages[mCurrentPage].Items)
      {
        if(item.ItemType == MetaItem.ItemTypes.Table)
        {
          item.Table.Visible = true;
          item.Table.SuspendLayout();
          item.Table.Location = new Point(0, height + ClipTop);
          item.Table.TopVisibleRowIndex = item.TopRowIndex;
          item.Table.BottomVisibleRowIndex = item.BottomRowIndex;
          item.Table.DrawTable();
          item.Table.ResumeLayout();
        }
        else if(item.ItemType == MetaItem.ItemTypes.Seperator)
        { }

        height += item.Height;
      }

      txtPageNumber.Text = (mCurrentPage + 1).ToString();
    }
    #endregion Graphics


    #region Table manipulation
    public void Clear()
    {
      // Clear all tables
      mTables.ForEach(table => table.Parent = null);
      mTables.Clear();

      EndUpdate();
    }

    public Table AddTable(bool active = true, int width1 = -1, int width2 = -1)
    {
      if (!mCreatingReport)
        return null;

      Table table = new Table();
      table.Parent = this;
      table.Left = mPageMargin;
      table.Active = active;
      table.DrawGrid = active;
      mTables.Add(table);

      if (!active && width1 != -1 && width2 != -1)
      {
        table.AddNewColumn(Types.String, "", width1);
        table.AddNewColumn(Types.String, "", width2);
      }

      return table;
    }

    public Table GetTable(int index)
    {
      return mTables[index];
    }
    #endregion Table manipulation


    #region Print functionality
    private void btnPrint_Click(object sender, EventArgs e)
    {
      PrintDocument doc = new PrintDocument();
      PrintDialog printer = new PrintDialog();
      doc.PrintPage += PrintPage;
      printer.Document = doc;
      if (printer.ShowDialog() == DialogResult.Cancel)
        return;

      try
      {
        mPrintingReport = true;
        WorkingHeight = 1007;
        EndUpdate();

        doc.PrinterSettings = printer.PrinterSettings;
        doc.Print();
      }
      finally
      {
        WorkingHeight = 800;
        mPrintingReport = false;

        // Force redrawing with non print properties
        EndUpdate();
      }
    }

    private void PrintPage(object sender, PrintPageEventArgs args)
    {
      CreateCurrentPage();

      // Create output bitmap
      Bitmap img = new Bitmap(Width, WorkingHeight);
      Graphics canvas = Graphics.FromImage(img);
      canvas.Clear(Color.White);

      // Draw Header
      canvas.DrawImageUnscaled(0, 0, mHeader);

      // Draw visible tables
      mTables.ForEach(table => { if(table.Visible) table.DrawTable(canvas); });

      // Draw footer
      canvas.DrawImageUnscaled(0, FooterTop, mFooter);

      // Calculate ouput size
      Rectangle output = args.MarginBounds;
      if (1.0 * img.Width / img.Height > 1.0 * output.Width / output.Height)
        output.Height = TinyMath.Round(1.0 * img.Height / img.Width * output.Width);
      else
        output.Width = TinyMath.Round(1.0 * img.Width / img.Height * output.Height);

      // Print generated bitmap
      args.Graphics.DrawImage(img, output);
      args.HasMorePages = mCurrentPage != PageCount - 1;

      // Determine next job
      if(args.HasMorePages)
      { 
        ++mCurrentPage;
        CreateCurrentPage();
      }
    }
    #endregion Print functionality
  }
}
