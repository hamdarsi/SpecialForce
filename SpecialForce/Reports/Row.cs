﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace SpecialForce.Reports
{
  public class Row
  {
    private Table mParent = null;
    public Table Parent
    {
      get { return mParent; }
    }

    private Object mTag = null;
    public Object Tag
    {
      get { return mTag; }
      set { mTag = value; }
    }

    private Brush mBackBrush = Brushes.White;
    public Brush BackBrush
    {
      get { return mBackBrush; }
      set { mBackBrush = value; }
    }

    private List<Variant> mValues = new List<Variant>();
    public Variant this[int index]
    {
      get { return mValues[index]; }
      set { mValues[index] = value; }
    }

    public Variant this[string column]
    {
      get { return mValues[mParent._GetColumnIndex(column)]; }
      set { mValues[mParent._GetColumnIndex(column)] = value; }
    }

    public Row(Table parent, Object tag)
    {
      mParent = parent;
      mTag = tag;
      for (int i = 0; i < mParent.Columns.Count; ++i)
        mValues.Add(new Variant());
    }
  }
}
