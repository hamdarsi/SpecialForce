﻿using System;
using System.Collections.Generic;

namespace SpecialForce.Reports
{
  public class Container
  {
    private List<MetaItem> mItems = new List<MetaItem>();
    public List<MetaItem> Items
    {
      get { return mItems; }
    }

    public int RemainingCapacity
    {
      get;
      private set;
    }

    public int TotalCapacity
    {
      get;
      private set;
    }

    public Container(int cap)
    {
      RemainingCapacity = cap;
      TotalCapacity = cap;
    }

    public void AddItem(MetaItem item)
    {
      if(item.Height > RemainingCapacity)
        throw new Exception("(Reports.Container) Items size is more than remaining capacity");

      mItems.Add(item);
      RemainingCapacity -= item.Height;
    }
  }
}
