﻿using System;
using System.Drawing;
using System.Globalization;

namespace SpecialForce
{
  public static class Types
  {
    public static System.Type Integer = typeof(int);
    public static System.Type Double = typeof(double);
    public static System.Type String = typeof(string);
    public static System.Type DateTime = typeof(PersianDateTime);
    public static System.Type Currency = typeof(Currency);
  }

  public enum FillingMode
  {
    NoFill,
    Solid,
    Hatched,
    ElegantSolid
  }
}

namespace SpecialForce.Reports
{
  public class Column
  {
    private int mIndex = -1;
    public int Index
    {
      get { return mIndex; }
    }

    private System.Type mType = typeof(int);
    public System.Type Type
    {
      get { return mType; }
    }

    private int mWidth = -1;
    public int Width
    {
      get { return mWidth; }
    }

    private string mText = "بدون نام";
    public string Text
    {
      get { return mText; }
      set { mText = value; }
    }

    private Alignment mCaptionAlign = Alignment.MiddleRight;
    public Alignment CaptionAlign
    {
      get { return mCaptionAlign; }
      set { mCaptionAlign = value; }
    }

    private Alignment mCellAlign = Alignment.MiddleRight;
    public Alignment CellAlign
    {
      get { return mCellAlign; }
      set { mCellAlign = value; }
    }

    private bool mSortAscending = false;
    public bool SortAscending
    {
      get { return mSortAscending; }
      set { mSortAscending = value; }
    }

    private Color mForeColor = Color.Black;
    public Color ForeColor
    {
      get { return mForeColor; }
      set { mForeColor = value; }
    }

    private Color mBackColor = Color.White;
    public Color BackColor
    {
      get { return mBackColor; }
      set { mBackColor = value; }
    }

    private FillingMode mFillMode = FillingMode.Solid;
    public FillingMode FillMode
    {
      get { return mFillMode; }
      set { mFillMode = value; }
    }

    private DateTimeString mDateTimeMode = DateTimeString.CompactDate;
    public DateTimeString DateTimeMode
    {
      get { return mDateTimeMode; }
      set { mDateTimeMode = value; }
    }

    public Column(int index, System.Type type, int width)
    {
      mIndex = index;
      mType = type;
      mWidth = width;
    }
  }
}
