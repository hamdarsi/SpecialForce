﻿using System;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace SpecialForce.Wizards
{
  public partial class WizardDataBase : WizardForm
  {
    #region Wizard Functionality
    public WizardDataBase()
    {
      InitializeComponent();
    }

    private void DataBase_Load(object sender, EventArgs e)
    {
      txtIP.RightToLeft = System.Windows.Forms.RightToLeft.No;
      txtPassword.RightToLeft = System.Windows.Forms.RightToLeft.No;

      txtIP.Text = Properties.Settings.Default.DataBaseIP;
      txtPassword.Text = Properties.Settings.Default.DataBasePassword;
    }

    public override bool ShallBeShown()
    {
      return DataBase.IsOpen == false;
    }

    protected override bool Apply()
    {
      Properties.Settings.Default.DataBaseIP = txtIP.Text;
      Properties.Settings.Default.DataBasePassword = txtPassword.Text;

      Application.UseWaitCursor = true;
      Application.DoEvents();
      DataBase.OpenResult result = DataBase.Open();
      Application.UseWaitCursor = false;

      if (result == DataBase.OpenResult.HostUnreachable)
        Session.ShowError("برنامه نمی تواند به پایگاه داده متصل شود.");
      else if (result == DataBase.OpenResult.AuthenticationFailure)
        Session.ShowError("رمز عبور پایگاه داده اشتباه وارد شده است.");
      else if (result == DataBase.OpenResult.NoPassword)
        Session.ShowError("لطفا رمز عبور را وارد نمایید.");
      else
      {
        if (result == DataBase.OpenResult.CreationNeeded)
          CreateDataBase();

        return DataBase.IsOpen;
      }

      return false;
    }
    #endregion Wizard Functionality


    #region DataBase Creation Functionality
    private MySqlConnection mDataBase = null;

    private void ExecuteSQL(string sql)
    {
      sql = sql.Replace("@DB_NAME@", DataBase.Name);
      MySqlCommand cmd = new MySqlCommand(sql, mDataBase);
      cmd.ExecuteNonQuery();
    }

    private void CreateDataBase()
    {
      string sql = "user=spf;host=" + txtIP.Text;
      sql += ";password=" + txtPassword.Text;
      sql += ";Charset=utf8;";

      try
      {
        Application.UseWaitCursor = true;
        lblCreating.Visible = true;
        Application.DoEvents();

        mDataBase = new MySqlConnection(sql);
        mDataBase.Open();

        // Database itself
        for(int i = 0; i < mDBMacros.Length; ++i)
          ExecuteSQL(mDBMacros[i]);

        // Tables
        for (int i = 0; i < mTablesMacros.Length; ++i)
          ExecuteSQL(mTablesMacros[i]);

        // Stored Procedures
        for (int i = 0; i < mSPMacros.Length; ++i)
          ExecuteSQL(mSPMacros[i]);

        mDataBase.Close();

        // Now load the global database
        DataBase.Open();
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "WizardDataBase", "Creating DataBase");
        Logger.Log("(WizardDB) Attempting to clean up:");

        try
        {
          MySqlConnection backup = new MySqlConnection(sql);
          new MySqlCommand(sql, backup).ExecuteNonQuery();
          ExecuteSQL("DROP SCHEMA `" + DataBase.Name + "`");
          Logger.Log("(WizardDB) Done.");
        }
        catch(Exception ex2)
        {
          Logger.LogException(ex2, "(WizardDB) WTF? Something really went wrong");
        }

        Session.ShowError("مشکلی در ساخت پایگاه داده پیش آمد.");
      }
      finally
      {
        lblCreating.Visible = false;
        Application.UseWaitCursor = false;
      }
    }
    #endregion DataBase Creation Functionality


    #region Creation Macros
    private string[] mDBMacros = new string[]
    {
      #region DataBase
      "SET NAMES utf8",
      "SET TIME_ZONE='+00:00'",
      "CREATE DATABASE IF NOT EXISTS `@DB_NAME@` DEFAULT CHARACTER SET utf8 COLLATE utf8_persian_ci",
      "Use `@DB_NAME@`",
      "SET character_set_client = utf8",
      #endregion DataBase
    };


    private string[] mTablesMacros = new string[]
    {
      #region Accounting
      @"CREATE TABLE `accounting` (
        `InvoiceID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `Amount` double NOT NULL,
        `TimeStamp` datetime NOT NULL,
        `Comment` varchar(512) COLLATE utf8_persian_ci NOT NULL,
        `Type` varchar(128) COLLATE utf8_persian_ci NOT NULL,
        `SiteID` int(11) NOT NULL,
        `OperatorID` int(11) NOT NULL,
        `CompetitionID` int(11) NOT NULL DEFAULT '-1',
        `TeamID` int(11) NOT NULL DEFAULT '-1',
        `UserID` int(11) NOT NULL DEFAULT '-1',
        `GameID` int(11) NOT NULL DEFAULT '-1',
        PRIMARY KEY (`InvoiceID`),
        UNIQUE KEY `AccountingID_UNIQUE` (`InvoiceID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion Accounting


      #region CandidateMatches
      @"CREATE TABLE `candidatematches` (
        `CandidateMatchID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `CompetitionID` int(11) NOT NULL,
        `GameID` int(11) NOT NULL,
        `BlueTeamCodeName` varchar(45) COLLATE utf8_persian_ci NOT NULL,
        `GreenTeamCodeName` varchar(45) COLLATE utf8_persian_ci NOT NULL,
        `WinnerCodeName` varchar(45) COLLATE utf8_persian_ci NOT NULL,
        `LoserCodeName` varchar(45) COLLATE utf8_persian_ci NOT NULL,
        `Stage` int(11) NOT NULL,
        `Valid` tinyint(1) NOT NULL,
        `GroupIndex` int(11) NOT NULL,
        PRIMARY KEY (`CandidateMatchID`),
        UNIQUE KEY `CompetitionMatchID_UNIQUE` (`CandidateMatchID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion CandidateMatches


      #region CompetitionCodeNames
      @"CREATE TABLE `competitioncodenames` (
        `CompetitionCodeNameID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `CompetitionID` int(11) NOT NULL,
        `CodeName` varchar(45) COLLATE utf8_persian_ci NOT NULL,
        `TeamID` int(11) NOT NULL DEFAULT '-1',
        `Stage` int(11) NOT NULL,
        PRIMARY KEY (`CompetitionCodeNameID`),
        UNIQUE KEY `CompetitionCodeNameID_UNIQUE` (`CompetitionCodeNameID`),
        KEY `CompetitionID` (`CompetitionID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion CompetitionCodeNames


      #region CompetitionGroupings
      @"CREATE TABLE `competitiongroupings` (
        `GroupingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `CompetitionID` int(11) NOT NULL,
        `GroupIndex` int(11) NOT NULL,
        `TeamIndex` int(11) NOT NULL,
        `TeamID` int(11) NOT NULL,
        PRIMARY KEY (`GroupingID`),
        UNIQUE KEY `GroupID_UNIQUE` (`GroupingID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion CompetitionGroupings


      #region Competitions
      @"CREATE TABLE `competitions` (
        `CompetitionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `CompetitionType` varchar(45) COLLATE utf8_persian_ci NOT NULL,
        `RoundRobin` tinyint(1) NOT NULL,
        `RoundCount` int(11) NOT NULL,
        `RoundTime` int(11) NOT NULL,
        `RestTime` int(11) NOT NULL,
        `StartHP` int(11) NOT NULL,
        `StartMagazine` int(11) NOT NULL,
        `AmmoPerMagazine` int(11) NOT NULL,
        `FriendlyFire` tinyint(1) NOT NULL,
        `GameObject` varchar(45) COLLATE utf8_persian_ci NOT NULL,
        `BombTime` int(11) NOT NULL,
        `Admission` double NOT NULL,
        `GameTime` int(11) NOT NULL,
        `Status` varchar(45) COLLATE utf8_persian_ci NOT NULL,
        `Title` varchar(256) COLLATE utf8_persian_ci NOT NULL,
        PRIMARY KEY (`CompetitionID`),
        UNIQUE KEY `CompetitionID_UNIQUE` (`CompetitionID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion Competitions


      #region CompetitionTeams
      @"CREATE TABLE `competitionteams` (
        `CompetitionTeamID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `CompetitionID` int(11) NOT NULL,
        `TeamID` int(11) NOT NULL,
        `Registration` datetime NOT NULL,
        PRIMARY KEY (`CompetitionTeamID`),
        UNIQUE KEY `CompetitionTeamID_UNIQUE` (`CompetitionTeamID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion CompetitionTeams


      #region Entrances
      @"CREATE TABLE `entrances` (
        `EntranceID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `UserID` int(11) NOT NULL,
        `Enter` datetime NOT NULL,
        `Dismiss` datetime DEFAULT NULL,
        `SiteID` int(11) NOT NULL,
        PRIMARY KEY (`EntranceID`),
        UNIQUE KEY `StaffLoginID_UNIQUE` (`EntranceID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci",
      #endregion Entrances


      #region Games
      @"CREATE TABLE `games` (
        `GameID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `Reservation` datetime DEFAULT NULL,
        `Start` datetime NOT NULL,
        `Finish` datetime NOT NULL,
        `State` varchar(128) COLLATE utf8_persian_ci NOT NULL,
        `AdmissionFee` double NOT NULL,
        `GameType` varchar(128) COLLATE utf8_persian_ci NOT NULL,
        `GameObject` varchar(128) COLLATE utf8_persian_ci NOT NULL,
        `BlueTeam` int(11) NOT NULL DEFAULT '-1',
        `GreenTeam` int(11) NOT NULL DEFAULT '-1',
        `RoundCount` int(11) NOT NULL DEFAULT '-1',
        `RoundTime` int(11) NOT NULL DEFAULT '-1',
        `RestTime` int(11) NOT NULL DEFAULT '0',
        `StartHP` int(11) NOT NULL DEFAULT '-1',
        `StartMagazine` int(11) NOT NULL DEFAULT '-1',
        `AmmoPerMagazine` int(11) NOT NULL DEFAULT '-1',
        `FriendlyFire` tinyint(1) NOT NULL DEFAULT '1',
        `Winner` int(11) NOT NULL DEFAULT '0',
        `BombTime` int(11) NOT NULL DEFAULT '-1',
        `SiteID` int(11) NOT NULL,
        `CompetitionID` int(11) NOT NULL DEFAULT '-1',
        `SmsSend` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`GameID`),
        UNIQUE KEY `SessionID_UNIQUE` (`GameID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion Games


      #region Kills
      @"CREATE TABLE `kills` (
        `KillID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `GameID` int(11) NOT NULL,
        `RoundID` int(11) NOT NULL,
        `KillerID` int(11) NOT NULL,
        `KilledID` int(11) NOT NULL,
        `TimeInRound` int(11) NOT NULL,
        `Legal` tinyint(1) NOT NULL,
        PRIMARY KEY (`KillID`),
        UNIQUE KEY `KillID_UNIQUE` (`KillID`),
        KEY `Killer` (`KillerID`),
        KEY `Killed` (`KilledID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion Kills


      #region Messages
      @"CREATE TABLE `messages` (
        `MessageID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `Title` varchar(256) COLLATE utf8_persian_ci NOT NULL,
        `Line1` varchar(16) COLLATE utf8_persian_ci NOT NULL,
        `Line2` varchar(16) COLLATE utf8_persian_ci NOT NULL,
        `OperatorID` int(11) NOT NULL,
        `CreatedOn` datetime NOT NULL,
        `DeletedOn` datetime DEFAULT NULL,
        PRIMARY KEY (`MessageID`),
        UNIQUE KEY `MessageID_UNIQUE` (`MessageID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion Messages


      #region Notes
      @"CREATE TABLE `notes` (
        `NoteID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `Note` mediumtext COLLATE utf8_persian_ci NOT NULL,
        `UserID` int(11) NOT NULL,
        `LastChange` datetime NOT NULL,
        `Deleted` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`NoteID`),
        UNIQUE KEY `NoteID_UNIQUE` (`NoteID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion Notes


      #region Options
      @"CREATE TABLE `options` (
        `OptionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `GiftCreditNecessaryCharge` double NOT NULL,
        `GiftCreditAmount` double NOT NULL,
        `BombPlantFrag` int(11) NOT NULL,
        `BombDiffuseFrag` int(11) NOT NULL,
        `KillFrag` int(11) NOT NULL,
        `FriendlyKillFrag` int(11) NOT NULL,
        `MaximumTeamNameLength` int(11) NOT NULL,
        `TeamMinimumMembers` int(11) NOT NULL,
        `RefundableCancelDuration` int(11) NOT NULL,
        PRIMARY KEY (`OptionID`),
        UNIQUE KEY `OptionID_UNIQUE` (`OptionID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion Options


      #region PlayersInGames
      @"CREATE TABLE `playersingames` (
        `PIG_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `GameID` int(11) NOT NULL,
        `UserID` int(11) NOT NULL,
        `PlayerIsForBlueTeam` tinyint(1) NOT NULL,
        PRIMARY KEY (`PIG_ID`),
        UNIQUE KEY `PIG_ID_UNIQUE` (`PIG_ID`),
        KEY `GameID` (`GameID`),
        KEY `UserID` (`UserID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion PlayersInGames


      #region PlayersInRounds
      @"CREATE TABLE `playersinrounds` (
        `PIR_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `RoundID` int(11) NOT NULL,
        `UserID` int(11) NOT NULL,
        `PlayerIsForBlueTeam` tinyint(1) NOT NULL,
        `KevlarNumber` int(11) NOT NULL,
        PRIMARY KEY (`PIR_ID`),
        UNIQUE KEY `PIR_ID_UNIQUE` (`PIR_ID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion PlayersInRounds


      #region Rounds
      @"CREATE TABLE `rounds` (
        `RoundID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `GameID` int(11) NOT NULL,
        `RoundNumber` int(11) NOT NULL,
        `Start` datetime DEFAULT NULL,
        `Finish` datetime DEFAULT NULL,
        `State` varchar(45) COLLATE utf8_persian_ci NOT NULL,
        `Winner` int(11) NOT NULL DEFAULT '-1',
        `TerroristsAreBlue` tinyint(1) DEFAULT NULL,
        `Diffuser` int(11) NOT NULL DEFAULT '-1',
        `DiffuseTime` int(11) DEFAULT '-1',
        `Planter` int(11) NOT NULL DEFAULT '-1',
        `PlantTime` int(11) DEFAULT '-1',
        `PlantedBomb` int(11) DEFAULT '-1',
        `BombState` varchar(45) COLLATE utf8_persian_ci NOT NULL DEFAULT 'Disabled',
        PRIMARY KEY (`RoundID`),
        UNIQUE KEY `RoundID_UNIQUE` (`RoundID`),
        KEY `GameID` (`GameID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion Rounds


      #region ScheduleNotes
      @"CREATE TABLE `schedulenotes` (
        `NoteID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `UserID` int(11) NOT NULL,
        `GameID` int(11) NOT NULL,
        `Note` mediumtext COLLATE utf8_persian_ci NOT NULL,
        `TimeStamp` datetime NOT NULL,
        `Due` datetime DEFAULT NULL,
        `Deleted` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`NoteID`),
        UNIQUE KEY `NoteID_UNIQUE` (`NoteID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion ScheduleNotes


      #region Sites
      @"CREATE TABLE `sites` (
        `SiteID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `FullName` varchar(256) COLLATE utf8_persian_ci NOT NULL,
        `ShortName` varchar(128) COLLATE utf8_persian_ci NOT NULL,
        `Address` varchar(256) COLLATE utf8_persian_ci NOT NULL,
        `Telephone` varchar(15) COLLATE utf8_persian_ci NOT NULL,
        `UsageStart` datetime NOT NULL,
        `UsageEnd` datetime DEFAULT NULL,
        PRIMARY KEY (`SiteID`),
        UNIQUE KEY `SiteID_UNIQUE` (`SiteID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion Sites


      #region TeamMemberShip
      @"CREATE TABLE `teammembership` (
        `TeamMembershipID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `TeamID` int(11) NOT NULL,
        `UserID` int(11) NOT NULL,
        `MembershipStart` datetime NOT NULL,
        `MembershipEnd` datetime DEFAULT NULL,
        PRIMARY KEY (`TeamMembershipID`),
        UNIQUE KEY `TeamMembershipID_UNIQUE` (`TeamMembershipID`),
        KEY `TeamID` (`TeamID`),
        KEY `UserID` (`UserID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion TeamMemberShip


      #region Teams
      @"CREATE TABLE `teams` (
        `TeamID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `Name` varchar(64) COLLATE utf8_persian_ci NOT NULL,
        `Logo` mediumblob,
        `RegistrationDate` datetime DEFAULT NULL,
        `RegistrationSiteID` int(11) NOT NULL,
        PRIMARY KEY (`TeamID`),
        UNIQUE KEY `TeamID_UNIQUE` (`TeamID`),
        KEY `Names` (`Name`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion Teams


      #region Users
      @"CREATE TABLE `users` (
        `UserID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `FirstName` varchar(64) COLLATE utf8_persian_ci DEFAULT NULL,
        `LastName` varchar(128) COLLATE utf8_persian_ci DEFAULT NULL,
        `Gender` tinyint(1) DEFAULT NULL,
        `Post` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
        `Address` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
        `MobilePrefix` varchar(3) COLLATE utf8_persian_ci DEFAULT NULL,
        `MobileNumber` varchar(9) COLLATE utf8_persian_ci DEFAULT NULL,
        `Telephone` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
        `BirthDate` datetime DEFAULT NULL,
        `Photo` mediumblob,
        `PhotoSize` int(11) DEFAULT NULL,
        `FingerPrintData` mediumblob,
        `FingerPrintDataSize` int(11) DEFAULT NULL,
        `FingerPrintImage` mediumblob,
        `FingerPrintImageSize` int(11) DEFAULT NULL,
        `UserName` varchar(128) COLLATE utf8_persian_ci DEFAULT NULL,
        `Password` varchar(128) COLLATE utf8_persian_ci DEFAULT NULL,
        `RegistrationDate` datetime NOT NULL,
        `RegistrationSite` int(11) NOT NULL,
        `FireDate` datetime DEFAULT NULL,
        `FireReason` varchar(256) COLLATE utf8_persian_ci DEFAULT '',
        `ChargedCredit` double NOT NULL DEFAULT '0',
        `RefundedCredit` double NOT NULL DEFAULT '0',
        `ReservedCredit` double NOT NULL DEFAULT '0',
        `UsedCredit` double NOT NULL DEFAULT '0',
        `LastFreeCreditFlag` double NOT NULL DEFAULT '0',
        `GiftedCredit` double NOT NULL DEFAULT '0',
        `TeamID` int(11) NOT NULL DEFAULT '-1',
        `Email` varchar(45) COLLATE utf8_persian_ci DEFAULT NULL,
        PRIMARY KEY (`UserID`),
        UNIQUE KEY `UserID_UNIQUE` (`UserID`),
        UNIQUE KEY `UserName_UNIQUE` (`UserName`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci COMMENT='This table contains information about all registered users'",
      #endregion Users


      #region UserSpecialTreats
      @"CREATE TABLE `userspecialtreats` (
        `TreatID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `AssignDate` datetime NOT NULL,
        `DueDate` datetime NOT NULL,
        `UserID` int(11) NOT NULL,
        `AssignerID` int(11) NOT NULL,
        `Reason` varchar(1024) COLLATE utf8_persian_ci NOT NULL,
        `Treat` varchar(45) COLLATE utf8_persian_ci NOT NULL,
        `CancellerID` int(11) NOT NULL DEFAULT '-1',
        `CancelDate` datetime DEFAULT NULL,
        `SiteID` int(11) NOT NULL,
        `SmsSend` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`TreatID`),
        UNIQUE KEY `SpecialDateID_UNIQUE` (`TreatID`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;",
      #endregion UserSpecialTreats
    };


    private string[] mSPMacros = new string[]
    {
      #region SP Configurations
      @"SET character_set_client  = utf8",
      @"SET character_set_results = utf8",
      @"SET collation_connection  = utf8_general_ci",
      @"SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'",
      #endregion SP Configurations


      #region CleanUp
@"CREATE DEFINER=`root`@`localhost` PROCEDURE `CleanUp`()
BEGIN
  DELETE FROM `Accounting`;
  DELETE FROM `CandidateMatches`;
  DELETE FROM `CompetitionCodeNames`;
  DELETE FROM `CompetitionGroupings`;
  DELETE FROM `Competitions`;
  DELETE FROM `CompetitionTeams`;
  DELETE FROM `Games`;
  DELETE FROM `Kills`;
  DELETE FROM `Messages`;
  DELETE FROM `Notes`;
  DELETE FROM `Options`;
  DELETE FROM `PlayersInGames`;
  DELETE FROM `PlayersInRounds`;
  DELETE FROM `ScheduleNotes`;
  DELETE FROM `Rounds`;
  DELETE FROM `Entrances`;
  DELETE FROM `UserSpecialTreats`;
  
  UPDATE Users SET ChargedCredit=0, ReservedCredit=0, RefundedCredit=0, UsedCredit=0, LastFreeCreditFlag=0, GiftedCredit=0;
END",
      #endregion CleanUp


      #region DeleteCompetition
@"CREATE DEFINER=`root`@`localhost` PROCEDURE `DeleteCompetition`(cid INT)
BEGIN
	DELETE FROM Competitions WHERE CompetitionID=cid;
	DELETE FROM CompetitionTeams WHERE CompetitionID=cid;
	DELETE FROM CompetitionCodeNames WHERE CompetitionID=cid;
	DELETE FROM CandidateMatches WHERE CompetitionID=cid;
	DELETE FROM Games WHERE CompetitionID=cid;
	DELETE FROM Accounting WHERE CompetitionID=cid;
END",
      #endregion DeleteCompetition


      #region GetAvailableCash
@"CREATE DEFINER=`root`@`localhost` PROCEDURE `GetAvailableCash`(sid INT)
BEGIN
  -- Declaration of holder variables
  DECLARE cBills DOUBLE DEFAULT 0;
  DECLARE cCompAdmissions DOUBLE DEFAULT 0;
  DECLARE cCompExpenses DOUBLE DEFAULT 0;
  DECLARE cOtherExpenses DOUBLE DEFAULT 0;
  DECLARE cRents DOUBLE DEFAULT 0;
  DECLARE cCharges DOUBLE DEFAULT 0;
  DECLARE cRefunds DOUBLE DEFAULT 0;
  DECLARE cClears DOUBLE DEFAULT 0;

  -- A cursor to select accounting on the given site
  DECLARE InvoiceAmount DOUBLE;
  DECLARE InvoiceType VARCHAR(128);
  DECLARE done INT DEFAULT FALSE;
  DECLARE cur CURSOR FOR SELECT `Amount`, `Type` FROM Accounting WHERE SiteID=sid;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  -- Start extracting data
  OPEN cur;
  read_loop: LOOP
    FETCH cur INTO InvoiceAmount, InvoiceType;
    IF done THEN
      LEAVE read_loop;
    END IF;

    If InvoiceType LIKE '%قبض%' THEN
	    SET cBills = cBills + InvoiceAmount;

    ElseIF InvoiceType = 'ورودیه' Then
      SET cCompAdmissions = cCompAdmissions + InvoiceAmount;

    ElseIF InvoiceType = 'هزینه مسابقه' Then
      SET cCompExpenses = cCompExpenses + InvoiceAmount;

    ElseIF InvoiceType = 'اجاره' Then
      SET cRents = cRents + InvoiceAmount;

    ElseIF InvoiceType = 'شارژ حساب بازیکن' Then
      SET cCharges = cCharges + InvoiceAmount;

    ElseIF InvoiceType = 'عودت شارژ بازیکن' Then
      SET cRefunds = cRefunds + InvoiceAmount;

    ElseIF InvoiceType = 'برداشت' Then
      SET cClears = cClears + InvoiceAmount;

    ElseIf InvoiceType != 'شارژ هدیه' Then
	    Set cOtherExpenses = cOtherExpenses + InvoiceAmount;

	  END IF;
  END LOOP;
  CLOSE cur;

  -- Create a temporary table to store extracted data
  DROP TABLE IF EXISTS Clearance;
  CREATE TEMPORARY TABLE Clearance
  (
    Available DOUBLE,
    Bills DOUBLE,
    CompetitionAdmissions DOUBLE,
    CompetitionExpenses DOUBLE,
    OtherExpenses DOUBLE,
    Rents DOUBLE,
    UserCharges DOUBLE,
    UserRefunds DOUBLE,
	  PreviousClears DOUBLE
  );

  -- Store the result
  INSERT INTO Clearance 
    (Available, Bills, CompetitionAdmissions, CompetitionExpenses, OtherExpenses, Rents, UserCharges, UserRefunds, PreviousClears)
    VALUES
    (
      cCompAdmissions + cCharges - cBills - cCompExpenses - cRents - cRefunds - cOtherExpenses - cClears,
      cBills, cCompAdmissions, cCompExpenses, cOtherExpenses, cRents, cCharges, cRefunds, cClears
    );

  -- And return it
  SELECT * FROM Clearance;
END",
#endregion GetAvailableCash
    };
    
    #endregion Creation Macros
  }
}
