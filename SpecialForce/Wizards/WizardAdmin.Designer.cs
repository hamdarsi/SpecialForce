﻿namespace SpecialForce.Wizards
{
  partial class WizardAdmin
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WizardAdmin));
      this.txtRepassword = new System.Windows.Forms.TextBox();
      this.txtPassword = new System.Windows.Forms.TextBox();
      this.txtUserName = new System.Windows.Forms.TextBox();
      this.label12 = new SpecialForce.TransparentLabel();
      this.label11 = new SpecialForce.TransparentLabel();
      this.txtLastName = new System.Windows.Forms.TextBox();
      this.txtFirstName = new System.Windows.Forms.TextBox();
      this.label2 = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.pdpBirthDate = new SpecialForce.PersianDatePicker();
      this.label6 = new SpecialForce.TransparentLabel();
      this.txtTelephone = new System.Windows.Forms.TextBox();
      this.label10 = new SpecialForce.TransparentLabel();
      this.txtMobileNumber = new System.Windows.Forms.MaskedTextBox();
      this.txtMobilePrefix = new System.Windows.Forms.MaskedTextBox();
      this.label4 = new SpecialForce.TransparentLabel();
      this.txtAddress = new System.Windows.Forms.TextBox();
      this.label3 = new SpecialForce.TransparentLabel();
      this.cmbGender = new System.Windows.Forms.ComboBox();
      this.label5 = new SpecialForce.TransparentLabel();
      this.FileDialog = new System.Windows.Forms.OpenFileDialog();
      this.pbFinger = new System.Windows.Forms.PictureBox();
      this.pbPhoto = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.pbFinger)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbPhoto)).BeginInit();
      this.SuspendLayout();
      // 
      // txtRepassword
      // 
      this.txtRepassword.Location = new System.Drawing.Point(190, 106);
      this.txtRepassword.Name = "txtRepassword";
      this.txtRepassword.PasswordChar = '*';
      this.txtRepassword.Size = new System.Drawing.Size(100, 21);
      this.txtRepassword.TabIndex = 4;
      // 
      // txtPassword
      // 
      this.txtPassword.Location = new System.Drawing.Point(84, 106);
      this.txtPassword.Name = "txtPassword";
      this.txtPassword.PasswordChar = '*';
      this.txtPassword.Size = new System.Drawing.Size(100, 21);
      this.txtPassword.TabIndex = 3;
      // 
      // txtUserName
      // 
      this.txtUserName.Location = new System.Drawing.Point(84, 83);
      this.txtUserName.Name = "txtUserName";
      this.txtUserName.Size = new System.Drawing.Size(100, 21);
      this.txtUserName.TabIndex = 2;
      // 
      // label12
      // 
      this.label12.Location = new System.Drawing.Point(11, 109);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(54, 15);
      this.label12.TabIndex = 45;
      this.label12.TabStop = false;
      this.label12.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label12.Texts")));
      // 
      // label11
      // 
      this.label11.Location = new System.Drawing.Point(11, 86);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(55, 15);
      this.label11.TabIndex = 44;
      this.label11.TabStop = false;
      this.label11.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label11.Texts")));
      // 
      // txtLastName
      // 
      this.txtLastName.Location = new System.Drawing.Point(84, 152);
      this.txtLastName.Name = "txtLastName";
      this.txtLastName.Size = new System.Drawing.Size(100, 21);
      this.txtLastName.TabIndex = 6;
      // 
      // txtFirstName
      // 
      this.txtFirstName.Location = new System.Drawing.Point(84, 129);
      this.txtFirstName.Name = "txtFirstName";
      this.txtFirstName.Size = new System.Drawing.Size(100, 21);
      this.txtFirstName.TabIndex = 5;
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(11, 155);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(68, 15);
      this.label2.TabIndex = 49;
      this.label2.TabStop = false;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(11, 132);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(21, 15);
      this.label1.TabIndex = 48;
      this.label1.TabStop = false;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // pdpBirthDate
      // 
      this.pdpBirthDate.ForeColor = System.Drawing.Color.Black;
      this.pdpBirthDate.Location = new System.Drawing.Point(84, 269);
      this.pdpBirthDate.Name = "pdpBirthDate";
      this.pdpBirthDate.Size = new System.Drawing.Size(132, 42);
      this.pdpBirthDate.TabIndex = 12;
      this.pdpBirthDate.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("pdpBirthDate.Texts")));
      // 
      // label6
      // 
      this.label6.Location = new System.Drawing.Point(12, 287);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(50, 15);
      this.label6.TabIndex = 51;
      this.label6.TabStop = false;
      this.label6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label6.Texts")));
      // 
      // txtTelephone
      // 
      this.txtTelephone.Location = new System.Drawing.Point(84, 198);
      this.txtTelephone.Name = "txtTelephone";
      this.txtTelephone.Size = new System.Drawing.Size(82, 21);
      this.txtTelephone.TabIndex = 9;
      // 
      // label10
      // 
      this.label10.Location = new System.Drawing.Point(11, 201);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(53, 15);
      this.label10.TabIndex = 56;
      this.label10.TabStop = false;
      this.label10.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label10.Texts")));
      // 
      // txtMobileNumber
      // 
      this.txtMobileNumber.Location = new System.Drawing.Point(84, 175);
      this.txtMobileNumber.Mask = "0000000";
      this.txtMobileNumber.Name = "txtMobileNumber";
      this.txtMobileNumber.Size = new System.Drawing.Size(49, 21);
      this.txtMobileNumber.TabIndex = 8;
      // 
      // txtMobilePrefix
      // 
      this.txtMobilePrefix.BeepOnError = true;
      this.txtMobilePrefix.Location = new System.Drawing.Point(141, 175);
      this.txtMobilePrefix.Mask = "000";
      this.txtMobilePrefix.Name = "txtMobilePrefix";
      this.txtMobilePrefix.Size = new System.Drawing.Size(25, 21);
      this.txtMobilePrefix.TabIndex = 7;
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(11, 178);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(60, 15);
      this.label4.TabIndex = 55;
      this.label4.TabStop = false;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // txtAddress
      // 
      this.txtAddress.Location = new System.Drawing.Point(84, 221);
      this.txtAddress.Name = "txtAddress";
      this.txtAddress.Size = new System.Drawing.Size(206, 21);
      this.txtAddress.TabIndex = 10;
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(11, 224);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(34, 15);
      this.label3.TabIndex = 58;
      this.label3.TabStop = false;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // cmbGender
      // 
      this.cmbGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbGender.FormattingEnabled = true;
      this.cmbGender.Items.AddRange(new object[] {
            "مرد",
            "زن"});
      this.cmbGender.Location = new System.Drawing.Point(84, 245);
      this.cmbGender.Name = "cmbGender";
      this.cmbGender.Size = new System.Drawing.Size(42, 21);
      this.cmbGender.TabIndex = 11;
      // 
      // label5
      // 
      this.label5.Location = new System.Drawing.Point(11, 248);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(46, 15);
      this.label5.TabIndex = 60;
      this.label5.TabStop = false;
      this.label5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label5.Texts")));
      // 
      // FileDialog
      // 
      this.FileDialog.FileName = "openFileDialog1";
      // 
      // pbFinger
      // 
      this.pbFinger.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbFinger.Image = global::SpecialForce.Properties.Resources.Wizard_Finger_Print;
      this.pbFinger.Location = new System.Drawing.Point(346, 209);
      this.pbFinger.Name = "pbFinger";
      this.pbFinger.Size = new System.Drawing.Size(90, 102);
      this.pbFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pbFinger.TabIndex = 62;
      this.pbFinger.TabStop = false;
      // 
      // pbPhoto
      // 
      this.pbPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbPhoto.Image = global::SpecialForce.Properties.Resources.Wizard_User_Photo;
      this.pbPhoto.Location = new System.Drawing.Point(346, 83);
      this.pbPhoto.Name = "pbPhoto";
      this.pbPhoto.Size = new System.Drawing.Size(90, 120);
      this.pbPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pbPhoto.TabIndex = 61;
      this.pbPhoto.TabStop = false;
      this.pbPhoto.Click += new System.EventHandler(this.pbPhoto_Click);
      // 
      // WizardAdmin
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.pbFinger);
      this.Controls.Add(this.pbPhoto);
      this.Controls.Add(this.cmbGender);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.txtAddress);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.txtTelephone);
      this.Controls.Add(this.label10);
      this.Controls.Add(this.txtMobileNumber);
      this.Controls.Add(this.txtMobilePrefix);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.pdpBirthDate);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.txtLastName);
      this.Controls.Add(this.txtFirstName);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.label12);
      this.Controls.Add(this.label11);
      this.Controls.Add(this.txtUserName);
      this.Controls.Add(this.txtRepassword);
      this.Controls.Add(this.txtPassword);
      this.Description = "اطلاعات مدیریت سیستم";
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "WizardAdmin";
      this.Text = "تنظیمات نیروی ویژه";
      this.Title = "مدیریت سیستم";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.WizardAdmin_FormClosed);
      this.Load += new System.EventHandler(this.WizardAdmin_Load);
      this.Controls.SetChildIndex(this.txtPassword, 0);
      this.Controls.SetChildIndex(this.txtRepassword, 0);
      this.Controls.SetChildIndex(this.txtUserName, 0);
      this.Controls.SetChildIndex(this.label11, 0);
      this.Controls.SetChildIndex(this.label12, 0);
      this.Controls.SetChildIndex(this.label1, 0);
      this.Controls.SetChildIndex(this.label2, 0);
      this.Controls.SetChildIndex(this.txtFirstName, 0);
      this.Controls.SetChildIndex(this.txtLastName, 0);
      this.Controls.SetChildIndex(this.label6, 0);
      this.Controls.SetChildIndex(this.pdpBirthDate, 0);
      this.Controls.SetChildIndex(this.label4, 0);
      this.Controls.SetChildIndex(this.txtMobilePrefix, 0);
      this.Controls.SetChildIndex(this.txtMobileNumber, 0);
      this.Controls.SetChildIndex(this.label10, 0);
      this.Controls.SetChildIndex(this.txtTelephone, 0);
      this.Controls.SetChildIndex(this.label3, 0);
      this.Controls.SetChildIndex(this.txtAddress, 0);
      this.Controls.SetChildIndex(this.label5, 0);
      this.Controls.SetChildIndex(this.cmbGender, 0);
      this.Controls.SetChildIndex(this.pbPhoto, 0);
      this.Controls.SetChildIndex(this.pbFinger, 0);
      ((System.ComponentModel.ISupportInitialize)(this.pbFinger)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbPhoto)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox txtRepassword;
    private System.Windows.Forms.TextBox txtPassword;
    private System.Windows.Forms.TextBox txtUserName;
    private SpecialForce.TransparentLabel label12;
    private SpecialForce.TransparentLabel label11;
    private System.Windows.Forms.TextBox txtLastName;
    private System.Windows.Forms.TextBox txtFirstName;
    private SpecialForce.TransparentLabel label2;
    private SpecialForce.TransparentLabel label1;
    private SpecialForce.PersianDatePicker pdpBirthDate;
    private SpecialForce.TransparentLabel label6;
    private System.Windows.Forms.TextBox txtTelephone;
    private SpecialForce.TransparentLabel label10;
    private System.Windows.Forms.MaskedTextBox txtMobileNumber;
    private System.Windows.Forms.MaskedTextBox txtMobilePrefix;
    private SpecialForce.TransparentLabel label4;
    private System.Windows.Forms.TextBox txtAddress;
    private SpecialForce.TransparentLabel label3;
    private System.Windows.Forms.ComboBox cmbGender;
    private SpecialForce.TransparentLabel label5;
    private System.Windows.Forms.PictureBox pbPhoto;
    private System.Windows.Forms.PictureBox pbFinger;
    private System.Windows.Forms.OpenFileDialog FileDialog;

  }
}