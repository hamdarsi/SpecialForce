﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Wizards
{
  public partial class WizardSite : WizardForm
  {
    public WizardSite()
    {
      InitializeComponent();
    }

    private void WizardSite_Load(object sender, EventArgs e)
    {
      List<DB.Site> sites = DB.Site.GetAllSites(false);
      foreach (DB.Site site in sites)
        cmbSite.Items.Add(site);
      cmbSite.Items.Add("باشگاه جدید");

      cmbSite.SelectedIndex = 0;
    }

    private void cmbSite_SelectedValueChanged(object sender, EventArgs e)
    {
      pnlNewSite.Visible = cmbSite.SelectedIndex == cmbSite.Items.Count - 1;
    }

    public override bool ShallBeShown()
    {
      int id = Properties.Settings.Default.SiteID;
      if(id == -1)
        return true;
      else if(DB.Site.GetSiteByID(id) == null)
        return true;
      else if (DB.Site.IsClosed(id))
        return true;
      else
        return false;
    }

    protected override bool Apply()
    {
      DB.Site site = cmbSite.SelectedItem as DB.Site;
      if (site == null)
      {
        if (txtName.Text == "" ||
           txtShortName.Text == "" ||
           txtAddress.Text == "" ||
           txtTelephone.Text == "")
        {
          Session.ShowError("لطفا اطلاعات مربوط به باشگاه را وارد کنید.");
          return false;
        }

        site = new DB.Site();
        site.FullName = txtName.Text;
        site.ShortName = txtShortName.Text;
        site.Telephone = txtTelephone.Text;
        site.Address = txtAddress.Text;
        site.Apply();
      }

      Properties.Settings.Default.SiteID = site.ID;
      Properties.Settings.Default.KioskMode = chkKiosk.Checked;
      return true;
    }
  }
}
