﻿namespace SpecialForce.Wizards
{
  partial class WizardSite
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WizardSite));
      this.pnlNewSite = new SpecialForce.ToolBar();
      this.txtAddress = new System.Windows.Forms.TextBox();
      this.txtTelephone = new System.Windows.Forms.TextBox();
      this.txtShortName = new System.Windows.Forms.TextBox();
      this.txtName = new System.Windows.Forms.TextBox();
      this.label4 = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.label2 = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.label5 = new SpecialForce.TransparentLabel();
      this.cmbSite = new System.Windows.Forms.ComboBox();
      this.chkKiosk = new SpecialForce.TransparentCheckBox();
      this.pnlNewSite.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlNewSite
      // 
      this.pnlNewSite.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlNewSite.Controls.Add(this.txtAddress);
      this.pnlNewSite.Controls.Add(this.txtTelephone);
      this.pnlNewSite.Controls.Add(this.txtShortName);
      this.pnlNewSite.Controls.Add(this.txtName);
      this.pnlNewSite.Controls.Add(this.label4);
      this.pnlNewSite.Controls.Add(this.label3);
      this.pnlNewSite.Controls.Add(this.label2);
      this.pnlNewSite.Controls.Add(this.label1);
      this.pnlNewSite.Location = new System.Drawing.Point(70, 159);
      this.pnlNewSite.Name = "pnlNewSite";
      this.pnlNewSite.ShowBorder = false;
      this.pnlNewSite.Size = new System.Drawing.Size(315, 154);
      this.pnlNewSite.TabIndex = 4;
      // 
      // txtAddress
      // 
      this.txtAddress.Location = new System.Drawing.Point(11, 84);
      this.txtAddress.Multiline = true;
      this.txtAddress.Name = "txtAddress";
      this.txtAddress.Size = new System.Drawing.Size(241, 57);
      this.txtAddress.TabIndex = 15;
      // 
      // txtTelephone
      // 
      this.txtTelephone.Location = new System.Drawing.Point(179, 57);
      this.txtTelephone.Name = "txtTelephone";
      this.txtTelephone.Size = new System.Drawing.Size(73, 21);
      this.txtTelephone.TabIndex = 14;
      // 
      // txtShortName
      // 
      this.txtShortName.Location = new System.Drawing.Point(179, 30);
      this.txtShortName.Name = "txtShortName";
      this.txtShortName.Size = new System.Drawing.Size(73, 21);
      this.txtShortName.TabIndex = 13;
      // 
      // txtName
      // 
      this.txtName.Location = new System.Drawing.Point(95, 3);
      this.txtName.Name = "txtName";
      this.txtName.Size = new System.Drawing.Size(157, 21);
      this.txtName.TabIndex = 12;
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(274, 60);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(29, 15);
      this.label4.TabIndex = 11;
      this.label4.TabStop = false;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(270, 87);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(34, 15);
      this.label3.TabIndex = 10;
      this.label3.TabStop = false;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(259, 33);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(47, 15);
      this.label2.TabIndex = 9;
      this.label2.TabStop = false;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(283, 6);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(21, 15);
      this.label1.TabIndex = 8;
      this.label1.TabStop = false;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // label5
      // 
      this.label5.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label5.Location = new System.Drawing.Point(28, 86);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(185, 24);
      this.label5.TabIndex = 5;
      this.label5.TabStop = false;
      this.label5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label5.Texts")));
      // 
      // cmbSite
      // 
      this.cmbSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbSite.FormattingEnabled = true;
      this.cmbSite.Location = new System.Drawing.Point(220, 87);
      this.cmbSite.Name = "cmbSite";
      this.cmbSite.Size = new System.Drawing.Size(121, 21);
      this.cmbSite.TabIndex = 6;
      this.cmbSite.SelectedValueChanged += new System.EventHandler(this.cmbSite_SelectedValueChanged);
      // 
      // chkKiosk
      // 
      this.chkKiosk.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.chkKiosk.ForeColor = System.Drawing.Color.White;
      this.chkKiosk.Location = new System.Drawing.Point(28, 120);
      this.chkKiosk.Name = "chkKiosk";
      this.chkKiosk.Size = new System.Drawing.Size(172, 21);
      this.chkKiosk.TabIndex = 7;
      this.chkKiosk.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkKiosk.Texts")));
      // 
      // WizardSite
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.chkKiosk);
      this.Controls.Add(this.pnlNewSite);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.cmbSite);
      this.Description = "اتصال برنامه به یکی از باشگاه های موجود";
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "WizardSite";
      this.Text = "تنظیمات نیروی ویژه";
      this.Title = "انتخاب باشگاه";
      this.Load += new System.EventHandler(this.WizardSite_Load);
      this.Controls.SetChildIndex(this.cmbSite, 0);
      this.Controls.SetChildIndex(this.label5, 0);
      this.Controls.SetChildIndex(this.pnlNewSite, 0);
      this.Controls.SetChildIndex(this.chkKiosk, 0);
      this.pnlNewSite.ResumeLayout(false);
      this.pnlNewSite.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private SpecialForce.ToolBar pnlNewSite;
    private System.Windows.Forms.TextBox txtAddress;
    private System.Windows.Forms.TextBox txtTelephone;
    private System.Windows.Forms.TextBox txtShortName;
    private System.Windows.Forms.TextBox txtName;
    private SpecialForce.TransparentLabel label4;
    private SpecialForce.TransparentLabel label3;
    private SpecialForce.TransparentLabel label2;
    private SpecialForce.TransparentLabel label1;
    private SpecialForce.TransparentLabel label5;
    private System.Windows.Forms.ComboBox cmbSite;
    private TransparentCheckBox chkKiosk;

  }
}