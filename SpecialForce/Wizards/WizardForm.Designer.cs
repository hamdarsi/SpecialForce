﻿namespace SpecialForce.Wizards
{
  partial class WizardForm
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WizardForm));
      this.lblFooter = new SpecialForce.TransparentLabel();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnNext = new System.Windows.Forms.Button();
      this.pnlBanner = new SpecialForce.ToolBar();
      this.lblInfo = new SpecialForce.TransparentLabel();
      this.lblBanner = new SpecialForce.TransparentLabel();
      this.pnlBanner.SuspendLayout();
      this.SuspendLayout();
      // 
      // lblFooter
      // 
      this.lblFooter.AutoSize = true;
      this.lblFooter.Enabled = false;
      this.lblFooter.Location = new System.Drawing.Point(-8, 314);
      this.lblFooter.Name = "lblFooter";
      this.lblFooter.Size = new System.Drawing.Size(458, 15);
      this.lblFooter.TabIndex = 0;
      this.lblFooter.TabStop = false;
      this.lblFooter.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblFooter.Texts")));
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(371, 341);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 1;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnNext
      // 
      this.btnNext.ForeColor = System.Drawing.Color.Black;
      this.btnNext.Location = new System.Drawing.Point(290, 341);
      this.btnNext.Name = "btnNext";
      this.btnNext.Size = new System.Drawing.Size(75, 23);
      this.btnNext.TabIndex = 0;
      this.btnNext.Text = "صفحه بعد >";
      this.btnNext.UseVisualStyleBackColor = true;
      this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
      // 
      // pnlBanner
      // 
      this.pnlBanner.BackColor = System.Drawing.Color.White;
      this.pnlBanner.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlBanner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlBanner.Controls.Add(this.lblInfo);
      this.pnlBanner.Controls.Add(this.lblBanner);
      this.pnlBanner.Location = new System.Drawing.Point(-6, -7);
      this.pnlBanner.Name = "pnlBanner";
      this.pnlBanner.Size = new System.Drawing.Size(465, 80);
      this.pnlBanner.TabIndex = 3;
      // 
      // lblInfo
      // 
      this.lblInfo.AutoSize = true;
      this.lblInfo.FixFromRight = true;
      this.lblInfo.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblInfo.Location = new System.Drawing.Point(377, 39);
      this.lblInfo.Name = "lblInfo";
      this.lblInfo.Size = new System.Drawing.Size(50, 24);
      this.lblInfo.TabIndex = 1;
      this.lblInfo.TabStop = false;
      this.lblInfo.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblInfo.Texts")));
      // 
      // lblBanner
      // 
      this.lblBanner.AutoSize = true;
      this.lblBanner.FixFromRight = true;
      this.lblBanner.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblBanner.Location = new System.Drawing.Point(409, 15);
      this.lblBanner.Name = "lblBanner";
      this.lblBanner.Size = new System.Drawing.Size(27, 26);
      this.lblBanner.TabIndex = 0;
      this.lblBanner.TabStop = false;
      this.lblBanner.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBanner.Texts")));
      // 
      // WizardForm
      // 
      this.AcceptButton = this.btnNext;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.pnlBanner);
      this.Controls.Add(this.btnNext);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.lblFooter);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.MinimizeBox = false;
      this.Name = "WizardForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WizardForm_FormClosing);
      this.Load += new System.EventHandler(this.WizardForm_Load);
      this.Shown += new System.EventHandler(this.WizardForm_Shown);
      this.pnlBanner.ResumeLayout(false);
      this.pnlBanner.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private SpecialForce.TransparentLabel lblFooter;
    private System.Windows.Forms.Button btnCancel;
    private SpecialForce.ToolBar pnlBanner;
    private System.Windows.Forms.Button btnNext;
    private TransparentLabel lblInfo;
    private TransparentLabel lblBanner;
  }
}
