﻿namespace SpecialForce.Wizards
{
  partial class WizardManager
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WizardManager));
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.SuspendLayout();
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel2.Location = new System.Drawing.Point(23, 116);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(205, 24);
      this.transparentLabel2.TabIndex = 6;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // WizardManager
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.transparentLabel2);
      this.Description = "تعدادی از تنظیمات برنامه نیاز به بازبینی و یا تنظیم اولیه دارند.";
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "WizardManager";
      this.Text = "تنظیمات نیروی ویژه";
      this.Title = "تنطیمات نرم افزار";
      this.Controls.SetChildIndex(this.transparentLabel2, 0);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private TransparentLabel transparentLabel2;
  }
}