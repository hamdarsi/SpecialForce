﻿namespace SpecialForce.Wizards
{
  partial class WizardWireless
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WizardWireless));
      this.cmbWirelessCenter = new System.Windows.Forms.ComboBox();
      this.cmbFingerPrint = new System.Windows.Forms.ComboBox();
      this.lblWireless1 = new SpecialForce.TransparentLabel();
      this.lblWireless2 = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.cmbWirelessBlue = new System.Windows.Forms.ComboBox();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.cmbWirelessGreen = new System.Windows.Forms.ComboBox();
      this.btnTestGreen = new SpecialForce.ToolButton();
      this.btnTestBlue = new SpecialForce.ToolButton();
      this.btnTestCenter = new SpecialForce.ToolButton();
      this.SuspendLayout();
      // 
      // cmbWirelessCenter
      // 
      this.cmbWirelessCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbWirelessCenter.FormattingEnabled = true;
      this.cmbWirelessCenter.Location = new System.Drawing.Point(304, 216);
      this.cmbWirelessCenter.Name = "cmbWirelessCenter";
      this.cmbWirelessCenter.Size = new System.Drawing.Size(61, 21);
      this.cmbWirelessCenter.TabIndex = 1;
      // 
      // cmbFingerPrint
      // 
      this.cmbFingerPrint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbFingerPrint.FormattingEnabled = true;
      this.cmbFingerPrint.Items.AddRange(new object[] {
            "بله",
            "خیر"});
      this.cmbFingerPrint.Location = new System.Drawing.Point(304, 119);
      this.cmbFingerPrint.Name = "cmbFingerPrint";
      this.cmbFingerPrint.Size = new System.Drawing.Size(61, 21);
      this.cmbFingerPrint.TabIndex = 0;
      // 
      // lblWireless1
      // 
      this.lblWireless1.AutoSize = true;
      this.lblWireless1.Location = new System.Drawing.Point(42, 171);
      this.lblWireless1.Name = "lblWireless1";
      this.lblWireless1.Size = new System.Drawing.Size(189, 15);
      this.lblWireless1.TabIndex = 9;
      this.lblWireless1.TabStop = false;
      this.lblWireless1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblWireless1.Texts")));
      // 
      // lblWireless2
      // 
      this.lblWireless2.AutoSize = true;
      this.lblWireless2.Location = new System.Drawing.Point(39, 186);
      this.lblWireless2.Name = "lblWireless2";
      this.lblWireless2.Size = new System.Drawing.Size(224, 15);
      this.lblWireless2.TabIndex = 10;
      this.lblWireless2.TabStop = false;
      this.lblWireless2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblWireless2.Texts")));
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(42, 122);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(158, 15);
      this.label3.TabIndex = 11;
      this.label3.TabStop = false;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.AutoSize = true;
      this.transparentLabel1.Location = new System.Drawing.Point(45, 213);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(86, 15);
      this.transparentLabel1.TabIndex = 12;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.AutoSize = true;
      this.transparentLabel2.Location = new System.Drawing.Point(42, 240);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(100, 15);
      this.transparentLabel2.TabIndex = 14;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // cmbWirelessBlue
      // 
      this.cmbWirelessBlue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbWirelessBlue.FormattingEnabled = true;
      this.cmbWirelessBlue.Location = new System.Drawing.Point(304, 243);
      this.cmbWirelessBlue.Name = "cmbWirelessBlue";
      this.cmbWirelessBlue.Size = new System.Drawing.Size(61, 21);
      this.cmbWirelessBlue.TabIndex = 13;
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.AutoSize = true;
      this.transparentLabel3.Location = new System.Drawing.Point(42, 267);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(103, 15);
      this.transparentLabel3.TabIndex = 16;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // cmbWirelessGreen
      // 
      this.cmbWirelessGreen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbWirelessGreen.FormattingEnabled = true;
      this.cmbWirelessGreen.Location = new System.Drawing.Point(304, 270);
      this.cmbWirelessGreen.Name = "cmbWirelessGreen";
      this.cmbWirelessGreen.Size = new System.Drawing.Size(61, 21);
      this.cmbWirelessGreen.TabIndex = 15;
      // 
      // btnTestGreen
      // 
      this.btnTestGreen.Hint = "تست ماژول پایگاه تیم سبز";
      this.btnTestGreen.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnTestGreen.Image")));
      this.btnTestGreen.Location = new System.Drawing.Point(371, 272);
      this.btnTestGreen.Name = "btnTestGreen";
      this.btnTestGreen.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnTestGreen.ShowBorder = false;
      this.btnTestGreen.Size = new System.Drawing.Size(16, 16);
      this.btnTestGreen.TabIndex = 17;
      this.btnTestGreen.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnTestGreen.Texts")));
      this.btnTestGreen.TransparentColor = System.Drawing.Color.Transparent;
      this.btnTestGreen.Click += new System.EventHandler(this.btnTestGreen_Click);
      // 
      // btnTestBlue
      // 
      this.btnTestBlue.Hint = "تست ماژول پایگاه تیم آبی";
      this.btnTestBlue.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnTestBlue.Image")));
      this.btnTestBlue.Location = new System.Drawing.Point(371, 245);
      this.btnTestBlue.Name = "btnTestBlue";
      this.btnTestBlue.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnTestBlue.ShowBorder = false;
      this.btnTestBlue.Size = new System.Drawing.Size(16, 16);
      this.btnTestBlue.TabIndex = 18;
      this.btnTestBlue.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnTestBlue.Texts")));
      this.btnTestBlue.TransparentColor = System.Drawing.Color.Transparent;
      this.btnTestBlue.Click += new System.EventHandler(this.btnTestBlue_Click);
      // 
      // btnTestCenter
      // 
      this.btnTestCenter.Hint = "تست ماژول وسط زمین";
      this.btnTestCenter.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnTestCenter.Image")));
      this.btnTestCenter.Location = new System.Drawing.Point(371, 218);
      this.btnTestCenter.Name = "btnTestCenter";
      this.btnTestCenter.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnTestCenter.ShowBorder = false;
      this.btnTestCenter.Size = new System.Drawing.Size(16, 16);
      this.btnTestCenter.TabIndex = 19;
      this.btnTestCenter.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnTestCenter.Texts")));
      this.btnTestCenter.TransparentColor = System.Drawing.Color.Transparent;
      this.btnTestCenter.Click += new System.EventHandler(this.btnTestCenter_Click);
      // 
      // WizardWireless
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.btnTestCenter);
      this.Controls.Add(this.btnTestBlue);
      this.Controls.Add(this.btnTestGreen);
      this.Controls.Add(this.transparentLabel3);
      this.Controls.Add(this.cmbWirelessGreen);
      this.Controls.Add(this.transparentLabel2);
      this.Controls.Add(this.cmbWirelessBlue);
      this.Controls.Add(this.transparentLabel1);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.lblWireless2);
      this.Controls.Add(this.lblWireless1);
      this.Controls.Add(this.cmbFingerPrint);
      this.Controls.Add(this.cmbWirelessCenter);
      this.Description = "استفاده از وسایل جانبی برنامه";
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "WizardWireless";
      this.Text = "تنظیمات نیروی ویژه";
      this.Title = "وسایل جانبی";
      this.Load += new System.EventHandler(this.WizardWireless_Load);
      this.Controls.SetChildIndex(this.cmbWirelessCenter, 0);
      this.Controls.SetChildIndex(this.cmbFingerPrint, 0);
      this.Controls.SetChildIndex(this.lblWireless1, 0);
      this.Controls.SetChildIndex(this.lblWireless2, 0);
      this.Controls.SetChildIndex(this.label3, 0);
      this.Controls.SetChildIndex(this.transparentLabel1, 0);
      this.Controls.SetChildIndex(this.cmbWirelessBlue, 0);
      this.Controls.SetChildIndex(this.transparentLabel2, 0);
      this.Controls.SetChildIndex(this.cmbWirelessGreen, 0);
      this.Controls.SetChildIndex(this.transparentLabel3, 0);
      this.Controls.SetChildIndex(this.btnTestGreen, 0);
      this.Controls.SetChildIndex(this.btnTestBlue, 0);
      this.Controls.SetChildIndex(this.btnTestCenter, 0);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ComboBox cmbWirelessCenter;
    private System.Windows.Forms.ComboBox cmbFingerPrint;
    private SpecialForce.TransparentLabel lblWireless1;
    private SpecialForce.TransparentLabel lblWireless2;
    private SpecialForce.TransparentLabel label3;
    private TransparentLabel transparentLabel1;
    private TransparentLabel transparentLabel2;
    private System.Windows.Forms.ComboBox cmbWirelessBlue;
    private TransparentLabel transparentLabel3;
    private System.Windows.Forms.ComboBox cmbWirelessGreen;
    private ToolButton btnTestGreen;
    private ToolButton btnTestBlue;
    private ToolButton btnTestCenter;
  }
}