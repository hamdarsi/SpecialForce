﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace SpecialForce.Wizards
{
  public partial class WizardAdmin : WizardForm
  {
    private CaptureThread mThread = null;
    private DB.User mAdmin = DB.User.New;
    private CaptureEventArgs mFPCapture = null;
    private Image mPhoto = null;

    public WizardAdmin()
    {
      InitializeComponent();
    }

    private void WizardAdmin_Load(object sender, EventArgs e)
    {
      cmbGender.SelectedIndex = 0;
      txtUserName.RightToLeft = RightToLeft.No;
      txtPassword.RightToLeft = RightToLeft.No;
      txtRepassword.RightToLeft = RightToLeft.No;

      if (Properties.Settings.Default.FingerPrintModuleConfigured)
      {
        if(FingerPrint.State == FingerPrintState.Deactive)
          FingerPrint.OpenSystem();

        if (FingerPrint.State != FingerPrintState.Active)
          return;

        mThread = FingerPrint.OpenDeviceAsThread(0);
        mThread.OnCapture += FingerPrintCaptured;
        mThread.Start();

        Session.StartSynchronization(this);
      }
    }

    private void WizardAdmin_FormClosed(object sender, FormClosedEventArgs e)
    {
      try
      {
        mThread.OnCapture -= FingerPrintCaptured;
        Session.StopSynchronization();
      }
      catch
      {
      }
    }

    private void pbPhoto_Click(object sender, EventArgs e)
    {
      if (FileDialog.ShowDialog() == DialogResult.OK)
      {
        pbPhoto.Image = Image.FromFile(FileDialog.FileName);
        mPhoto = pbPhoto.Image;

        if (FingerPrint.State == FingerPrintState.FakeActive)
          mFPCapture = new CaptureEventArgs(new byte[1] { 0 }, pbPhoto.Image);
      }
    }

    private void FingerPrintCaptured(object sender, SpecialForce.CaptureEventArgs e)
    {
      mFPCapture = e;
      pbFinger.Image = e.Image;
    }

    public override bool ShallBeShown()
    {
      return DB.User.GetNrOfGods() == 0;
    }

    protected override bool Apply()
    {
      UserNameState un_state = Authentication.CheckUserName(txtUserName.Text, mAdmin);

      if (txtFirstName.Text == "")
        Session.ShowError("نام نمی تواند خالی باشد.");
      else if (txtLastName.Text == "")
        Session.ShowError("نام خانوداگی نمی تواند خالی باشد.");
      else if (txtMobilePrefix.Text == "")
        Session.ShowError("کد تلفن همراه نمی تواند خالی باشد.");
      else if (txtMobileNumber.Text == "")
        Session.ShowError("شماره تلفن همراه نمی تواند خالی باشد.");
      else if (mPhoto == null)
        Session.ShowError("عکس کاربر حتما باید مشخص شود.");
      else if(!mAdmin.SetPhoto(mPhoto))
        Session.ShowError("عکس انتخاب شده حجم بالایی دارد.");
      else if (mFPCapture == null)
        Session.ShowError("اثر انگشت کاربر حتما باید تنظیم شود.");
      else if(!mAdmin.SetFingerPrint(mFPCapture.Data, mFPCapture.Image))
        Session.ShowError("عکس اثر انگشت حجم بالایی دارد.");
      else if (un_state == UserNameState.Duplicate)
        Session.ShowError("کاربر دیگری با نام کاربری وارد شده وجود دارد.");
      else if (un_state == UserNameState.Empty)
        Session.ShowError("نام کاربری وارد نشده است.");
      else if (un_state == UserNameState.InvalidCharacters)
      {
        string strInvalid = "نام کاربری شامل کارکتر های غیر مجاز است.";
        strInvalid += Environment.NewLine;
        strInvalid += "در نام کاربری خط فاصل نمی توان به کار برد.";
        Session.ShowError(strInvalid);
      }
      else if (txtPassword.Text.Length == 0)
        Session.ShowError("کلمه عبور وارد نشده است.");
      else if (txtRepassword.Text != txtPassword.Text)
        Session.ShowError("کلمه های عبور مانند هم نیستند.");
      else
      {
        mAdmin.FirstName = txtFirstName.Text;
        mAdmin.LastName = txtLastName.Text;
        mAdmin.Address = txtAddress.Text;
        mAdmin.MobilePrefix = txtMobilePrefix.Text;
        mAdmin.MobileNumber = txtMobileNumber.Text;
        mAdmin.Telephone = txtTelephone.Text;
        mAdmin.Gender = cmbGender.SelectedIndex == 0;
        mAdmin.Post = SpecialForce.Post.God;
        mAdmin.BirthDate = pdpBirthDate.Date;
        mAdmin.UserName = txtUserName.Text;
        mAdmin.Password = txtPassword.Text;
        mAdmin.SetFingerPrint(mFPCapture.Data, mFPCapture.Image);
        mAdmin.Apply();
        return true;
      }

      return false;
    }
  }
}
