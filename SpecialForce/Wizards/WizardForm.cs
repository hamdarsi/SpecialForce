﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Wizards
{
  public partial class WizardForm : SessionAwareForm
  {
    protected WizardManager mManager = null;
    public WizardManager Manager
    {
      set { mManager = value; }
    }

    [DefaultValue("تیتر")]
    public string Title
    {
      get { return lblBanner.Text; }
      set { lblBanner.Text = value; }
    }

    [DefaultValue("توضیحات")]
    public string Description
    {
      get { return lblInfo.Text; }
      set { lblInfo.Text = value; }
    }

    public WizardForm()
    {
      InitializeComponent();
    }

    private void WizardForm_Shown(object sender, EventArgs e)
    {
      lblBanner.Left = pnlBanner.Width - lblBanner.Width - 10;
      lblBanner.Visible = true;
      lblInfo.Left = pnlBanner.Width - lblInfo.Width - 20;
      lblInfo.Visible = true;

      if(mManager == null)
        return;

      Session.WaitCursor = true;
      if (mManager.GetNextPage(this) == null)
        btnNext.Text = "اتمام";
      Session.WaitCursor = false;
    }

    private void WizardForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (e.CloseReason == CloseReason.UserClosing)
      {
        if(Session.Ask("آیا میخواهید از تنظیم برنامه صرف نظر کنید؟"))
          Application.Exit();
        else
          e.Cancel = true;
      }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      WizardForm_FormClosing(null, new FormClosingEventArgs(CloseReason.UserClosing, false));
    }

    private void btnNext_Click(object sender, EventArgs e)
    {
      if (!Apply())
        return;

      Session.WaitCursor = true;
      WizardForm frm = mManager.GetNextPage(this);
      if (frm == null)
        Finish();
      else
      {
        Hide();
        frm.Show();
      }
      Session.WaitCursor = false;
    }

    virtual public bool ShallBeShown()
    {
      return false;
    }

    virtual protected bool Apply()
    {
      return true;
    }

    protected void Finish()
    {
      SpecialForce.Properties.Settings.Default.Save();
      Application.Exit();
    }

    private void WizardForm_Load(object sender, EventArgs e)
    {
      Icon = Properties.Resources.ApplicationIcon;
    }
  }
}
