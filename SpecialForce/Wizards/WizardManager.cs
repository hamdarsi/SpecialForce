﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Wizards
{
  public partial class WizardManager : WizardForm
  {
    private List<WizardForm> mPages = new List<WizardForm>();

    public WizardManager()
    {
      InitializeComponent();

      mPages.Add(this);
      mPages.Add(new WizardDataBase());
      mPages.Add(new WizardSite());
      mPages.Add(new WizardWireless());
      mPages.Add(new WizardAdmin());

      foreach (WizardForm frm in mPages)
        frm.Manager = this;
    }

    public WizardForm GetNextPage(WizardForm current)
    {
      for (int i = 0; i < mPages.Count; ++i)
        if (mPages[i] == current)
        {
          for (int j = i + 1; j < mPages.Count; ++j)
            if (mPages[j].ShallBeShown())
              return mPages[j];

          return null;
        }

      return null;
    }
  }
}
