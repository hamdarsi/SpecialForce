﻿namespace SpecialForce.Wizards
{
  partial class WizardDataBase
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WizardDataBase));
      this.txtPassword = new System.Windows.Forms.TextBox();
      this.txtIP = new System.Windows.Forms.TextBox();
      this.label2 = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.lblCreating = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.SuspendLayout();
      // 
      // txtPassword
      // 
      this.txtPassword.Location = new System.Drawing.Point(213, 203);
      this.txtPassword.Name = "txtPassword";
      this.txtPassword.Size = new System.Drawing.Size(100, 21);
      this.txtPassword.TabIndex = 15;
      this.txtPassword.UseSystemPasswordChar = true;
      // 
      // txtIP
      // 
      this.txtIP.Location = new System.Drawing.Point(213, 179);
      this.txtIP.Name = "txtIP";
      this.txtIP.Size = new System.Drawing.Size(100, 21);
      this.txtIP.TabIndex = 14;
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(109, 206);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(46, 15);
      this.label2.TabIndex = 13;
      this.label2.TabStop = false;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(109, 182);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(74, 15);
      this.label1.TabIndex = 12;
      this.label1.TabStop = false;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // label3
      // 
      this.label3.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label3.Location = new System.Drawing.Point(6, 98);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(277, 24);
      this.label3.TabIndex = 16;
      this.label3.TabStop = false;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // lblCreating
      // 
      this.lblCreating.Location = new System.Drawing.Point(15, 266);
      this.lblCreating.Name = "lblCreating";
      this.lblCreating.Size = new System.Drawing.Size(131, 15);
      this.lblCreating.TabIndex = 17;
      this.lblCreating.TabStop = false;
      this.lblCreating.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblCreating.Texts")));
      this.lblCreating.Visible = false;
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.Location = new System.Drawing.Point(4, 125);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(173, 24);
      this.transparentLabel1.TabIndex = 18;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // WizardDataBase
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.transparentLabel1);
      this.Controls.Add(this.lblCreating);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.txtPassword);
      this.Controls.Add(this.txtIP);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Description = "تنظیمات اتصال به پایگاه داده";
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "WizardDataBase";
      this.Text = "تنظیمات نیروی ویژه";
      this.Title = "پایگاه داده";
      this.Load += new System.EventHandler(this.DataBase_Load);
      this.Controls.SetChildIndex(this.label1, 0);
      this.Controls.SetChildIndex(this.label2, 0);
      this.Controls.SetChildIndex(this.txtIP, 0);
      this.Controls.SetChildIndex(this.txtPassword, 0);
      this.Controls.SetChildIndex(this.label3, 0);
      this.Controls.SetChildIndex(this.lblCreating, 0);
      this.Controls.SetChildIndex(this.transparentLabel1, 0);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox txtPassword;
    private System.Windows.Forms.TextBox txtIP;
    private SpecialForce.TransparentLabel label2;
    private SpecialForce.TransparentLabel label1;
    private SpecialForce.TransparentLabel label3;
    private TransparentLabel lblCreating;
    private TransparentLabel transparentLabel1;
  }
}