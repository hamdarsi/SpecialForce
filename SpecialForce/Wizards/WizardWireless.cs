﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Wizards
{
  public partial class WizardWireless : WizardForm
  {
    public WizardWireless()
    {
      InitializeComponent();
    }

    private void WizardWireless_Load(object sender, EventArgs e)
    {
      // Default to not use wireless
      cmbWirelessCenter.Items.Add("");
      cmbWirelessBlue.Items.Add("");
      cmbWirelessGreen.Items.Add("");

      List<string> ports = ControlTower.Wireless.AllPortNames;
      foreach (string port in ports)
      {
        cmbWirelessCenter.Items.Add(port);
        cmbWirelessBlue.Items.Add(port);
        cmbWirelessGreen.Items.Add(port);
      }

      cmbWirelessCenter.SelectedIndex = 0;
      cmbWirelessBlue.SelectedIndex = 0;
      cmbWirelessGreen.SelectedIndex = 0;

      // Default to not use finger print
      cmbFingerPrint.SelectedIndex = 1;

      if (Properties.Settings.Default.KioskMode)
      {
        btnTestCenter.Enabled = cmbWirelessCenter.Enabled = false;
        btnTestBlue.Enabled = cmbWirelessBlue.Enabled = false;
        btnTestGreen.Enabled = cmbWirelessGreen.Enabled = false;
      }
    }

    public override bool ShallBeShown()
    {
      return Properties.Settings.Default.WirelessPortCenter == "" ||
             !Properties.Settings.Default.FingerPrintModuleConfigured;
    }

    protected override bool Apply()
    {
      // Fingerprint
      Properties.Settings.Default.FingerPrintModuleConfigured = true;
      bool fpEnabled = cmbFingerPrint.SelectedItem.ToString() == "بله";
      Properties.Settings.Default.FingerPrintModuleEnabled = fpEnabled;
      if (fpEnabled)
      {
        if (FingerPrint.State != FingerPrintState.Active)
          cmbFingerPrint.SelectedIndex = 1;
        return FingerPrint.State == FingerPrintState.Active;
      }

      // Wireless
      string center = cmbWirelessCenter.SelectedItem.ToString();
      string blue = cmbWirelessBlue.SelectedItem.ToString();
      string green = cmbWirelessGreen.SelectedItem.ToString();
      bool wl_enabled = center != "";
      WirelessState wls = ControlTower.CommandQueue.AssignPorts(center, blue, green);
      if (wl_enabled && (center == blue || center == green || (blue == green && blue != "")))
        Session.ShowMessage("ماژول ها نباید هم نام باشند.");
      else if (wls == WirelessState.CenterNotWorking && wl_enabled)
        Session.ShowMessage("ماژول وسط زمین فعال نمی شود.");
      else if (wls == WirelessState.BlueNotWorking && wl_enabled)
        Session.ShowMessage("ماژول پایگاه تیم آبی فعال نمی شود.");
      else if (wls == WirelessState.GreenNotWorking && wl_enabled)
        Session.ShowMessage("ماژول پایگاه تیم سبز فعال نمی شود.");
      else
      {
        // Done
        if (wl_enabled)
        {
          Properties.Settings.Default.WirelessPortCenter = center;
          Properties.Settings.Default.WirelessPortBlue = blue;
          Properties.Settings.Default.WirelessPortGreen = green;
        }
        else
        {
          Properties.Settings.Default.WirelessPortCenter = "";
          Properties.Settings.Default.WirelessPortBlue = "";
          Properties.Settings.Default.WirelessPortGreen = "";
        }

        return true;
      }

      return false;
    }

    private void btnTestCenter_Click(object sender, EventArgs e)
    {
      ControlTower.Wireless.TestDevice(cmbWirelessCenter.SelectedItem.ToString());
    }

    private void btnTestBlue_Click(object sender, EventArgs e)
    {
      ControlTower.Wireless.TestDevice(cmbWirelessBlue.SelectedItem.ToString());
    }

    private void btnTestGreen_Click(object sender, EventArgs e)
    {
      ControlTower.Wireless.TestDevice(cmbWirelessGreen.SelectedItem.ToString());
    }
  }
}
