﻿namespace SpecialForce.Users
{
  partial class FormUserReportSettings
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUserReportSettings));
      this.btnCancel = new System.Windows.Forms.Button();
      this.toolBar2 = new SpecialForce.ToolBar();
      this.dtEnd = new SpecialForce.PersianDatePicker();
      this.dtStart = new SpecialForce.PersianDatePicker();
      this.lblEnd = new SpecialForce.TransparentLabel();
      this.lblStart = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.cmbReportType = new System.Windows.Forms.ComboBox();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.chkSites = new System.Windows.Forms.CheckedListBox();
      this.label7 = new SpecialForce.TransparentLabel();
      this.btnReport = new System.Windows.Forms.Button();
      this.toolBar2.SuspendLayout();
      this.toolBar1.SuspendLayout();
      this.SuspendLayout();
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(295, 245);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 59;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // toolBar2
      // 
      this.toolBar2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar2.BorderWidth = 0.5F;
      this.toolBar2.Controls.Add(this.dtEnd);
      this.toolBar2.Controls.Add(this.dtStart);
      this.toolBar2.Controls.Add(this.lblEnd);
      this.toolBar2.Controls.Add(this.lblStart);
      this.toolBar2.Controls.Add(this.label1);
      this.toolBar2.Controls.Add(this.cmbReportType);
      this.toolBar2.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar2.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar2.Location = new System.Drawing.Point(15, 14);
      this.toolBar2.Name = "toolBar2";
      this.toolBar2.Size = new System.Drawing.Size(436, 91);
      this.toolBar2.TabIndex = 58;
      // 
      // dtEnd
      // 
      this.dtEnd.Location = new System.Drawing.Point(13, 35);
      this.dtEnd.Name = "dtEnd";
      this.dtEnd.Size = new System.Drawing.Size(132, 43);
      this.dtEnd.TabIndex = 32;
      this.dtEnd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtEnd.Texts")));
      this.dtEnd.Visible = false;
      // 
      // dtStart
      // 
      this.dtStart.Location = new System.Drawing.Point(226, 35);
      this.dtStart.Name = "dtStart";
      this.dtStart.Size = new System.Drawing.Size(132, 43);
      this.dtStart.TabIndex = 29;
      this.dtStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtStart.Texts")));
      // 
      // lblEnd
      // 
      this.lblEnd.Location = new System.Drawing.Point(158, 51);
      this.lblEnd.Name = "lblEnd";
      this.lblEnd.Size = new System.Drawing.Size(14, 15);
      this.lblEnd.TabIndex = 28;
      this.lblEnd.TabStop = false;
      this.lblEnd.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblEnd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblEnd.Texts")));
      this.lblEnd.Visible = false;
      // 
      // lblStart
      // 
      this.lblStart.Location = new System.Drawing.Point(368, 51);
      this.lblStart.Name = "lblStart";
      this.lblStart.Size = new System.Drawing.Size(40, 15);
      this.lblStart.TabIndex = 27;
      this.lblStart.TabStop = false;
      this.lblStart.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblStart.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(367, 11);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(57, 15);
      this.label1.TabIndex = 4;
      this.label1.TabStop = false;
      this.label1.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // cmbReportType
      // 
      this.cmbReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbReportType.FormattingEnabled = true;
      this.cmbReportType.Items.AddRange(new object[] {
            "روزانه",
            "هفتگی",
            "ماهانه",
            "دلخواه"});
      this.cmbReportType.Location = new System.Drawing.Point(224, 8);
      this.cmbReportType.Name = "cmbReportType";
      this.cmbReportType.Size = new System.Drawing.Size(135, 21);
      this.cmbReportType.TabIndex = 3;
      this.cmbReportType.SelectedIndexChanged += new System.EventHandler(this.cmbReportType_SelectedIndexChanged);
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 0.5F;
      this.toolBar1.Controls.Add(this.chkSites);
      this.toolBar1.Controls.Add(this.label7);
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(15, 117);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(436, 119);
      this.toolBar1.TabIndex = 57;
      // 
      // chkSites
      // 
      this.chkSites.BackColor = System.Drawing.SystemColors.Window;
      this.chkSites.CheckOnClick = true;
      this.chkSites.ForeColor = System.Drawing.SystemColors.WindowText;
      this.chkSites.FormattingEnabled = true;
      this.chkSites.Location = new System.Drawing.Point(13, 24);
      this.chkSites.Name = "chkSites";
      this.chkSites.ScrollAlwaysVisible = true;
      this.chkSites.Size = new System.Drawing.Size(386, 84);
      this.chkSites.TabIndex = 38;
      // 
      // label7
      // 
      this.label7.Location = new System.Drawing.Point(243, 3);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(181, 15);
      this.label7.TabIndex = 37;
      this.label7.TabStop = false;
      this.label7.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label7.Texts")));
      // 
      // btnReport
      // 
      this.btnReport.ForeColor = System.Drawing.Color.Black;
      this.btnReport.Location = new System.Drawing.Point(376, 245);
      this.btnReport.Name = "btnReport";
      this.btnReport.Size = new System.Drawing.Size(75, 23);
      this.btnReport.TabIndex = 55;
      this.btnReport.Text = "گزارش گیری";
      this.btnReport.UseVisualStyleBackColor = true;
      this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
      // 
      // FormUserReportSettings
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoScroll = true;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(465, 280);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.toolBar2);
      this.Controls.Add(this.toolBar1);
      this.Controls.Add(this.btnReport);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormUserReportSettings";
      this.Text = "گزارش بازیکن";
      this.Load += new System.EventHandler(this.FormUserReportSettings_Load);
      this.toolBar2.ResumeLayout(false);
      this.toolBar1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnCancel;
    private ToolBar toolBar2;
    private PersianDatePicker dtEnd;
    private PersianDatePicker dtStart;
    private TransparentLabel lblEnd;
    private TransparentLabel lblStart;
    private TransparentLabel label1;
    private System.Windows.Forms.ComboBox cmbReportType;
    private ToolBar toolBar1;
    private System.Windows.Forms.CheckedListBox chkSites;
    private TransparentLabel label7;
    private System.Windows.Forms.Button btnReport;

  }
}