﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Users
{
  public partial class WizardRankingsInCompetitions : SessionAwareForm
  {
    private List<DB.Competition> mCompetitions = DB.CompetitionManager.AllCompetitions;

    public WizardRankingsInCompetitions()
    {
      InitializeComponent();
    }

    private void WizardCompetitionRankings_Load(object sender, EventArgs e)
    {
      for (int i = 0; i < mCompetitions.Count; ++i)
        lvCompetitions.Items.Add(mCompetitions[i].Title + "  (" + Enums.ToString(mCompetitions[i].Type) + ")");
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      if (lvCompetitions.SelectedIndices.Count == 0)
      {
        Session.ShowError("هیچ مسابقه ای انتخاب نشده است");
        return;
      }

      DB.Competition c = mCompetitions[lvCompetitions.SelectedIndices[0]];
      FormUserRankings frm = new FormUserRankings();
      frm.Title = "رتبه بندی کاربران در " + Enums.ToString(c.Type) + " " + c.Title;
      frm.GameIDs = DB.Competition.GetGamesForCompetition(c.ID);
      ShowModal(frm, true);
    }

    private void lvCompetitions_DoubleClick(object sender, EventArgs e)
    {
      btnOK_Click(sender, e);
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
