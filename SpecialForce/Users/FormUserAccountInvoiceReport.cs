﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace SpecialForce.Users
{
  public partial class FormUserAccountInvoiceReport : SessionAwareForm
  {
    private DB.User mUser;
    public DB.User User
    {
      get { return mUser; }
      set { mUser = value; }
    }

    private PersianDateTime mStart;
    public PersianDateTime Start
    {
      get { return mStart; }
      set { mStart = value; }
    }

    private PersianDateTime mFinish;
    public PersianDateTime Finish
    {
      get { return mFinish; }
      set { mFinish = value; }
    }

    private List<DB.Site> mSites = null;
    public List<DB.Site> Sites
    {
      get { return mSites; }
      set { mSites = value; }
    }

    public FormUserAccountInvoiceReport()
    {
      InitializeComponent();
    }

    private void FormUserAccountInvoiceReport_Shown(object sender, EventArgs e)
    {
      // start
      report1.BeginUpdate();

      DB.InvoiceReport report = new DB.InvoiceReport();
      report.Inclusions.Add(InvoiceGroup.UserCharges);
      report.Inclusions.Add(InvoiceGroup.UserRefunds);
      report.Inclusions.Add(InvoiceGroup.GiftCharges);
      foreach(DB.Site site in mSites)
        report.IncludeSite(site, mStart, mFinish);
      report.MakeReport();

      // Table for user information
      mUser.ReloadData();
      Reports.Table table = report1.AddTable(false, 77, 200);
      table.AddKeyValuePair("نام", mUser.FullName);
      table.AddKeyValuePair("تاریخ عضویت", mUser.RegistrationDate.ToString(DateTimeString.CompactDate));
      table.AddKeyValuePair("شارژ فعلی", mUser.Credit.ToCurrencyString() + " تومان");

      // Table for user credit history
      table = report1.AddTable();
      table.AutoIndexColumn = true;
      table.AddNewColumn(Types.Currency, "اعتبار", 70);
      table.AddNewColumn(Types.DateTime, "تاریخ", 70);
      table.AddNewColumn(Types.String, "نوع", 50);
      table.AddNewColumn(Types.String, "باشگاه", 100);

      foreach(DB.Invoice inv in report.Invoices)
      {
        if (inv.UserID != mUser.ID)
          continue;

        Reports.Row row = table.AddNewRow(inv);
        row[0] = inv.Amount.ToCurrency();
        row[1] = inv.TimeStamp;

        if (inv.Type == InvoiceType.UserCreditCharge)
          row[2] = "شارژ";
        else if (inv.Type == InvoiceType.UserCreditRefund)
          row[2] = "عودت";
        else if (inv.Type == InvoiceType.UserCreditUsage)
          row[2] = "استفاده";
        else if (inv.Type == InvoiceType.UserGiftCharge)
          row[2] = "هدیه";

        if (inv.SiteID != -1)
          row[3] = inv.SiteName;
        else
          row[3] = "اینترنتی";
      }

      // finished
      report1.EndUpdate();
    }
  }
}
