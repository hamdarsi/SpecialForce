﻿namespace SpecialForce.Users
{
  partial class FormUserRefund
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUserRefund));
      this.label4 = new SpecialForce.TransparentLabel();
      this.label5 = new SpecialForce.TransparentLabel();
      this.btnOK = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.pnlMain = new SpecialForce.ToolBar();
      this.txtRefund = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.pbFingerPrint = new System.Windows.Forms.PictureBox();
      this.lblAvailableCredit = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.pbUserPhoto = new System.Windows.Forms.PictureBox();
      this.lblPrompt = new SpecialForce.TransparentLabel();
      this.pnlMain.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbFingerPrint)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbUserPhoto)).BeginInit();
      this.SuspendLayout();
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(172, 222);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(112, 15);
      this.label4.TabIndex = 1;
      this.label4.TabStop = false;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // label5
      // 
      this.label5.Location = new System.Drawing.Point(44, 222);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(28, 15);
      this.label5.TabIndex = 3;
      this.label5.TabStop = false;
      this.label5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label5.Texts")));
      // 
      // btnOK
      // 
      this.btnOK.Enabled = false;
      this.btnOK.ForeColor = System.Drawing.Color.Black;
      this.btnOK.Location = new System.Drawing.Point(236, 279);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 3;
      this.btnOK.Text = "تایید";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(155, 279);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 2;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // pnlMain
      // 
      this.pnlMain.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlMain.BorderWidth = 0.5F;
      this.pnlMain.Controls.Add(this.txtRefund);
      this.pnlMain.Controls.Add(this.transparentLabel2);
      this.pnlMain.Controls.Add(this.transparentLabel1);
      this.pnlMain.Controls.Add(this.pbFingerPrint);
      this.pnlMain.Controls.Add(this.label5);
      this.pnlMain.Controls.Add(this.lblAvailableCredit);
      this.pnlMain.Controls.Add(this.label4);
      this.pnlMain.Controls.Add(this.label3);
      this.pnlMain.Controls.Add(this.pbUserPhoto);
      this.pnlMain.Controls.Add(this.lblPrompt);
      this.pnlMain.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlMain.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlMain.Location = new System.Drawing.Point(12, 12);
      this.pnlMain.Name = "pnlMain";
      this.pnlMain.Size = new System.Drawing.Size(299, 256);
      this.pnlMain.TabIndex = 43;
      // 
      // txtRefund
      // 
      this.txtRefund.Location = new System.Drawing.Point(78, 219);
      this.txtRefund.Mode = SpecialForce.Utilities.NumberBox.NumberMode.Currency;
      this.txtRefund.Name = "txtRefund";
      this.txtRefund.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtRefund.Size = new System.Drawing.Size(62, 21);
      this.txtRefund.TabIndex = 1;
      this.txtRefund.Text = "$0.00";
      this.txtRefund.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.btnOK_Click);
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Location = new System.Drawing.Point(45, 40);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(28, 15);
      this.transparentLabel2.TabIndex = 15;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Location = new System.Drawing.Point(210, 40);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(52, 15);
      this.transparentLabel1.TabIndex = 14;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // pbFingerPrint
      // 
      this.pbFingerPrint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbFingerPrint.Location = new System.Drawing.Point(188, 54);
      this.pbFingerPrint.Name = "pbFingerPrint";
      this.pbFingerPrint.Size = new System.Drawing.Size(96, 121);
      this.pbFingerPrint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pbFingerPrint.TabIndex = 13;
      this.pbFingerPrint.TabStop = false;
      // 
      // lblAvailableCredit
      // 
      this.lblAvailableCredit.FixFromRight = true;
      this.lblAvailableCredit.Location = new System.Drawing.Point(189, 194);
      this.lblAvailableCredit.Name = "lblAvailableCredit";
      this.lblAvailableCredit.Size = new System.Drawing.Size(8, 15);
      this.lblAvailableCredit.TabIndex = 12;
      this.lblAvailableCredit.TabStop = false;
      this.lblAvailableCredit.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblAvailableCredit.Texts")));
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(207, 194);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(79, 15);
      this.label3.TabIndex = 10;
      this.label3.TabStop = false;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // pbUserPhoto
      // 
      this.pbUserPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbUserPhoto.Location = new System.Drawing.Point(12, 54);
      this.pbUserPhoto.Name = "pbUserPhoto";
      this.pbUserPhoto.Size = new System.Drawing.Size(96, 121);
      this.pbUserPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pbUserPhoto.TabIndex = 8;
      this.pbUserPhoto.TabStop = false;
      // 
      // lblPrompt
      // 
      this.lblPrompt.FixFromRight = true;
      this.lblPrompt.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblPrompt.Location = new System.Drawing.Point(231, 10);
      this.lblPrompt.Name = "lblPrompt";
      this.lblPrompt.Size = new System.Drawing.Size(60, 21);
      this.lblPrompt.TabIndex = 7;
      this.lblPrompt.TabStop = false;
      this.lblPrompt.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblPrompt.Texts")));
      // 
      // FormUserRefund
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(323, 313);
      this.Controls.Add(this.pnlMain);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOK);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormUserRefund";
      this.Text = "عودت شارژ";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormUserRefund_FormClosing);
      this.Load += new System.EventHandler(this.FormUserRefund_Load);
      this.pnlMain.ResumeLayout(false);
      this.pnlMain.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbFingerPrint)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbUserPhoto)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private SpecialForce.TransparentLabel label4;
    private SpecialForce.TransparentLabel label5;
    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.Button btnCancel;
    private ToolBar pnlMain;
    private System.Windows.Forms.PictureBox pbFingerPrint;
    private TransparentLabel lblAvailableCredit;
    private TransparentLabel label3;
    private System.Windows.Forms.PictureBox pbUserPhoto;
    private TransparentLabel lblPrompt;
    private Utilities.NumberBox txtRefund;
    private TransparentLabel transparentLabel2;
    private TransparentLabel transparentLabel1;

  }
}