﻿namespace SpecialForce.Users
{
  partial class WizardRankingsInTimeLine
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WizardRankingsInTimeLine));
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnOK = new System.Windows.Forms.Button();
      this.transparentLabel7 = new SpecialForce.TransparentLabel();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.dtEnd = new SpecialForce.PersianDatePicker();
      this.dtStart = new SpecialForce.PersianDatePicker();
      this.lblEnd = new SpecialForce.TransparentLabel();
      this.lblStart = new SpecialForce.TransparentLabel();
      this.chkSites = new System.Windows.Forms.CheckedListBox();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.transparentLabel4 = new SpecialForce.TransparentLabel();
      this.chkCups = new SpecialForce.TransparentCheckBox();
      this.chkEliminations = new SpecialForce.TransparentCheckBox();
      this.chkLeagues = new SpecialForce.TransparentCheckBox();
      this.chkRegularMatches = new SpecialForce.TransparentCheckBox();
      this.pnlInfo = new SpecialForce.ToolBar();
      this.toolBar1.SuspendLayout();
      this.pnlInfo.SuspendLayout();
      this.SuspendLayout();
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(365, 341);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 71;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnOK
      // 
      this.btnOK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnOK.ForeColor = System.Drawing.Color.Black;
      this.btnOK.Location = new System.Drawing.Point(284, 341);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 70;
      this.btnOK.Text = "گزارش";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // transparentLabel7
      // 
      this.transparentLabel7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel7.Location = new System.Drawing.Point(-5, 314);
      this.transparentLabel7.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel7.Name = "transparentLabel7";
      this.transparentLabel7.Size = new System.Drawing.Size(451, 15);
      this.transparentLabel7.TabIndex = 69;
      this.transparentLabel7.TabStop = false;
      this.transparentLabel7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel7.Texts")));
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 3F;
      this.toolBar1.Controls.Add(this.transparentLabel2);
      this.toolBar1.Controls.Add(this.transparentLabel1);
      this.toolBar1.CornerRadius = 10;
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(-11, -8);
      this.toolBar1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(471, 82);
      this.toolBar1.TabIndex = 68;
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.FixFromRight = true;
      this.transparentLabel2.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel2.Location = new System.Drawing.Point(166, 42);
      this.transparentLabel2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(279, 26);
      this.transparentLabel2.TabIndex = 1;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.FixFromRight = true;
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.Location = new System.Drawing.Point(357, 15);
      this.transparentLabel1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(97, 27);
      this.transparentLabel1.TabIndex = 0;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // dtEnd
      // 
      this.dtEnd.Location = new System.Drawing.Point(255, 96);
      this.dtEnd.Name = "dtEnd";
      this.dtEnd.Size = new System.Drawing.Size(132, 43);
      this.dtEnd.TabIndex = 75;
      this.dtEnd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtEnd.Texts")));
      // 
      // dtStart
      // 
      this.dtStart.Location = new System.Drawing.Point(71, 96);
      this.dtStart.Name = "dtStart";
      this.dtStart.Size = new System.Drawing.Size(132, 43);
      this.dtStart.TabIndex = 74;
      this.dtStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtStart.Texts")));
      // 
      // lblEnd
      // 
      this.lblEnd.Location = new System.Drawing.Point(235, 109);
      this.lblEnd.Name = "lblEnd";
      this.lblEnd.Size = new System.Drawing.Size(14, 15);
      this.lblEnd.TabIndex = 73;
      this.lblEnd.TabStop = false;
      this.lblEnd.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblEnd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblEnd.Texts")));
      // 
      // lblStart
      // 
      this.lblStart.Location = new System.Drawing.Point(8, 109);
      this.lblStart.Name = "lblStart";
      this.lblStart.Size = new System.Drawing.Size(40, 15);
      this.lblStart.TabIndex = 72;
      this.lblStart.TabStop = false;
      this.lblStart.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblStart.Texts")));
      // 
      // chkSites
      // 
      this.chkSites.BackColor = System.Drawing.SystemColors.Window;
      this.chkSites.CheckOnClick = true;
      this.chkSites.ForeColor = System.Drawing.SystemColors.WindowText;
      this.chkSites.FormattingEnabled = true;
      this.chkSites.Location = new System.Drawing.Point(12, 173);
      this.chkSites.Name = "chkSites";
      this.chkSites.ScrollAlwaysVisible = true;
      this.chkSites.Size = new System.Drawing.Size(191, 132);
      this.chkSites.TabIndex = 76;
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.Location = new System.Drawing.Point(7, 152);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(108, 15);
      this.transparentLabel3.TabIndex = 77;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // transparentLabel4
      // 
      this.transparentLabel4.Location = new System.Drawing.Point(235, 152);
      this.transparentLabel4.Name = "transparentLabel4";
      this.transparentLabel4.Size = new System.Drawing.Size(182, 15);
      this.transparentLabel4.TabIndex = 82;
      this.transparentLabel4.TabStop = false;
      this.transparentLabel4.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel4.Texts")));
      // 
      // chkCups
      // 
      this.chkCups.Checked = true;
      this.chkCups.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkCups.ForeColor = System.Drawing.Color.White;
      this.chkCups.Location = new System.Drawing.Point(134, 16);
      this.chkCups.Name = "chkCups";
      this.chkCups.Size = new System.Drawing.Size(49, 18);
      this.chkCups.TabIndex = 81;
      this.chkCups.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkCups.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkCups.Texts")));
      // 
      // chkEliminations
      // 
      this.chkEliminations.Checked = true;
      this.chkEliminations.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkEliminations.ForeColor = System.Drawing.Color.White;
      this.chkEliminations.Location = new System.Drawing.Point(122, 44);
      this.chkEliminations.Name = "chkEliminations";
      this.chkEliminations.Size = new System.Drawing.Size(61, 18);
      this.chkEliminations.TabIndex = 80;
      this.chkEliminations.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkEliminations.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkEliminations.Texts")));
      // 
      // chkLeagues
      // 
      this.chkLeagues.Checked = true;
      this.chkLeagues.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkLeagues.ForeColor = System.Drawing.Color.White;
      this.chkLeagues.Location = new System.Drawing.Point(133, 72);
      this.chkLeagues.Name = "chkLeagues";
      this.chkLeagues.Size = new System.Drawing.Size(50, 18);
      this.chkLeagues.TabIndex = 79;
      this.chkLeagues.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkLeagues.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkLeagues.Texts")));
      // 
      // chkRegularMatches
      // 
      this.chkRegularMatches.Checked = true;
      this.chkRegularMatches.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkRegularMatches.ForeColor = System.Drawing.Color.White;
      this.chkRegularMatches.Location = new System.Drawing.Point(81, 100);
      this.chkRegularMatches.Name = "chkRegularMatches";
      this.chkRegularMatches.Size = new System.Drawing.Size(102, 18);
      this.chkRegularMatches.TabIndex = 78;
      this.chkRegularMatches.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkRegularMatches.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkRegularMatches.Texts")));
      // 
      // pnlInfo
      // 
      this.pnlInfo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlInfo.BorderWidth = 0.5F;
      this.pnlInfo.Controls.Add(this.chkRegularMatches);
      this.pnlInfo.Controls.Add(this.chkLeagues);
      this.pnlInfo.Controls.Add(this.chkCups);
      this.pnlInfo.Controls.Add(this.chkEliminations);
      this.pnlInfo.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlInfo.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlInfo.Location = new System.Drawing.Point(244, 173);
      this.pnlInfo.Name = "pnlInfo";
      this.pnlInfo.Size = new System.Drawing.Size(196, 132);
      this.pnlInfo.TabIndex = 83;
      // 
      // WizardRankingsInTimeLine
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(452, 376);
      this.Controls.Add(this.pnlInfo);
      this.Controls.Add(this.transparentLabel4);
      this.Controls.Add(this.transparentLabel3);
      this.Controls.Add(this.chkSites);
      this.Controls.Add(this.dtEnd);
      this.Controls.Add(this.dtStart);
      this.Controls.Add(this.lblEnd);
      this.Controls.Add(this.lblStart);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOK);
      this.Controls.Add(this.transparentLabel7);
      this.Controls.Add(this.toolBar1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "WizardRankingsInTimeLine";
      this.Text = "رتبه بندی کاربر ها";
      this.Load += new System.EventHandler(this.WizardRankingsInTimeLine_Load);
      this.toolBar1.ResumeLayout(false);
      this.pnlInfo.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnOK;
    private TransparentLabel transparentLabel7;
    private ToolBar toolBar1;
    private TransparentLabel transparentLabel2;
    private TransparentLabel transparentLabel1;
    private PersianDatePicker dtEnd;
    private PersianDatePicker dtStart;
    private TransparentLabel lblEnd;
    private TransparentLabel lblStart;
    private System.Windows.Forms.CheckedListBox chkSites;
    private TransparentLabel transparentLabel3;
    private TransparentLabel transparentLabel4;
    private TransparentCheckBox chkCups;
    private TransparentCheckBox chkEliminations;
    private TransparentCheckBox chkLeagues;
    private TransparentCheckBox chkRegularMatches;
    private ToolBar pnlInfo;
  }
}