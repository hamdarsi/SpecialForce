﻿using System;
using System.Windows.Forms;

namespace SpecialForce.Users
{
  public partial class FormUserRefund : SessionAwareForm
  {
    private DB.User mUser;
    private CaptureThread mThread;
    private double mAvailablePayment;

    public DB.User User
    {
      set { mUser = value; }
    }

    public FormUserRefund()
    {
      InitializeComponent();
    }

    private void FormUserRefund_Load(object sender, EventArgs e)
    {
      pbUserPhoto.Image = mUser.Photo.Resize(pbUserPhoto.Size);
      mAvailablePayment = mUser.Credit;
      txtRefund.Value = mAvailablePayment;
      lblAvailableCredit.Text = mAvailablePayment.ToCurrencyString() + " تومان";
      string str = "از " + (mUser.Gender ? "آقای " : "خانم ");
      str += mUser.FullName + " بخواهید تا ";
      str += "اثر انگشت خود را وارد کند.";
      lblPrompt.Text = str;

      mThread = FingerPrint.OpenDeviceAsThread(0);
      mThread.OnCapture += FingerPrintCaptured;
    }

    private void FingerPrintCaptured(Object sender, CaptureEventArgs e)
    {
      pbFingerPrint.Image = e.Image.Resize(pbFingerPrint.Size);

      if (FingerPrint.Search(e.Data) == mUser.ID)
        btnOK.Enabled = true;
      else
        Session.ShowError("این اثر انگشت مربوط به کاربر نیست.");
    }

    private void FormUserRefund_FormClosing(object sender, FormClosingEventArgs e)
    {
      mThread.OnCapture -= FingerPrintCaptured;
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      double payment = txtRefund.Value;
      if (payment > mAvailablePayment)
      {
        Session.ShowError("وجه وارد شده بیش از وجه قابل عودت است.");
        return;
      }

      if (payment <= 0)
      {
        Session.ShowError("وجه نمی تواند کوچکتر از صفر باشد.");
        return;
      }

      if(mUser.Refund(payment))
        Session.ShowMessage("وجه درخواستی عودت داده شد.");

      DialogResult = System.Windows.Forms.DialogResult.OK;
    }

    private void txtRefund_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        e.Handled = true;
        if (btnOK.Enabled)
          btnOK_Click(sender, e);
      }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
