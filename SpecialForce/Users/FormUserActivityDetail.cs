﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Users
{
  public partial class FormUserActivityDetail : SessionAwareForm
  {
    private DB.User mUser = null;
    public DB.User User
    {
      get { return mUser; }
      set { mUser = value; }
    }

    private DB.Game mGame = null;
    public DB.Game Game
    {
      get { return mGame; }
      set { mGame = value; }
    }


    public FormUserActivityDetail()
    {
      InitializeComponent();
    }

    private void FormUserActivityDetail_Shown(object sender, EventArgs e)
    {
      report1.BeginUpdate();
      report1.Title = "گزارش فعالیت " + mUser.FullName + " در بازی";

      Reports.Table table = report1.AddTable(false, 100, 100);
      table.AddKeyValuePair("فرگ", mGame.GetFrag(mUser).ToString());
      table.AddKeyValuePair("تعداد کشتن", mGame.GetKillCount(mUser).ToString());
      table.AddKeyValuePair("تعداد کشتن خودی", mGame.GetFriendlyKillCount(mUser).ToString());
      table.AddKeyValuePair("دفعات مردن", mGame.GetDeathCount(mUser).ToString());
      table.AddKeyValuePair("تعداد بمب گذاری", mGame.GetPlantCount(mUser).ToString());
      table.AddKeyValuePair("تعداد خنثی سازی", mGame.GetDiffuseCount(mUser).ToString());

      report1.EndUpdate();
    }
  }
}
