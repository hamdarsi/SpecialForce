﻿namespace SpecialForce.Users
{
  partial class FormUserAddEdit
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUserAddEdit));
      this.FileDialog = new System.Windows.Forms.OpenFileDialog();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.txtRePassword = new SpecialForce.UserInterface.PasswordBox();
      this.txtPassword = new SpecialForce.UserInterface.PasswordBox();
      this.btnUserPhoto = new SpecialForce.ToolButton();
      this.txtUserName = new System.Windows.Forms.TextBox();
      this.label12 = new SpecialForce.TransparentLabel();
      this.label11 = new SpecialForce.TransparentLabel();
      this.cmbPost = new System.Windows.Forms.ComboBox();
      this.label13 = new SpecialForce.TransparentLabel();
      this.txtTelephone = new System.Windows.Forms.TextBox();
      this.label10 = new SpecialForce.TransparentLabel();
      this.dtBirthDate = new SpecialForce.PersianDatePicker();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnOK = new System.Windows.Forms.Button();
      this.pbFinger = new System.Windows.Forms.PictureBox();
      this.txtMobileNumber = new System.Windows.Forms.MaskedTextBox();
      this.txtMobilePrefix = new System.Windows.Forms.MaskedTextBox();
      this.cmbGender = new System.Windows.Forms.ComboBox();
      this.txtAddress = new System.Windows.Forms.TextBox();
      this.txtLastName = new System.Windows.Forms.TextBox();
      this.txtFirstName = new System.Windows.Forms.TextBox();
      this.label6 = new SpecialForce.TransparentLabel();
      this.label5 = new SpecialForce.TransparentLabel();
      this.label4 = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.label2 = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.toolBar1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbFinger)).BeginInit();
      this.SuspendLayout();
      // 
      // FileDialog
      // 
      this.FileDialog.FileName = "openFileDialog1";
      this.FileDialog.Filter = "All Photos|*.jpg;*.bmp;*.png;*.gif";
      this.FileDialog.ShowReadOnly = true;
      this.FileDialog.Title = "عکس کاربری را انتخاب کنید";
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 0.5F;
      this.toolBar1.Controls.Add(this.txtRePassword);
      this.toolBar1.Controls.Add(this.txtPassword);
      this.toolBar1.Controls.Add(this.btnUserPhoto);
      this.toolBar1.Controls.Add(this.txtUserName);
      this.toolBar1.Controls.Add(this.label12);
      this.toolBar1.Controls.Add(this.label11);
      this.toolBar1.Controls.Add(this.cmbPost);
      this.toolBar1.Controls.Add(this.label13);
      this.toolBar1.Controls.Add(this.txtTelephone);
      this.toolBar1.Controls.Add(this.label10);
      this.toolBar1.Controls.Add(this.dtBirthDate);
      this.toolBar1.Controls.Add(this.btnCancel);
      this.toolBar1.Controls.Add(this.btnOK);
      this.toolBar1.Controls.Add(this.pbFinger);
      this.toolBar1.Controls.Add(this.txtMobileNumber);
      this.toolBar1.Controls.Add(this.txtMobilePrefix);
      this.toolBar1.Controls.Add(this.cmbGender);
      this.toolBar1.Controls.Add(this.txtAddress);
      this.toolBar1.Controls.Add(this.txtLastName);
      this.toolBar1.Controls.Add(this.txtFirstName);
      this.toolBar1.Controls.Add(this.label6);
      this.toolBar1.Controls.Add(this.label5);
      this.toolBar1.Controls.Add(this.label4);
      this.toolBar1.Controls.Add(this.label3);
      this.toolBar1.Controls.Add(this.label2);
      this.toolBar1.Controls.Add(this.label1);
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(12, 12);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(483, 307);
      this.toolBar1.TabIndex = 44;
      // 
      // txtRePassword
      // 
      this.txtRePassword.Location = new System.Drawing.Point(188, 216);
      this.txtRePassword.Name = "txtRePassword";
      this.txtRePassword.Size = new System.Drawing.Size(100, 21);
      this.txtRePassword.TabIndex = 11;
      this.txtRePassword.UseSystemPasswordChar = true;
      // 
      // txtPassword
      // 
      this.txtPassword.Location = new System.Drawing.Point(294, 216);
      this.txtPassword.Name = "txtPassword";
      this.txtPassword.Size = new System.Drawing.Size(100, 21);
      this.txtPassword.TabIndex = 10;
      this.txtPassword.UseSystemPasswordChar = true;
      // 
      // btnUserPhoto
      // 
      this.btnUserPhoto.Hint = "";
      this.btnUserPhoto.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnUserPhoto.Image")));
      this.btnUserPhoto.Location = new System.Drawing.Point(13, 13);
      this.btnUserPhoto.Name = "btnUserPhoto";
      this.btnUserPhoto.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.btnUserPhoto.Size = new System.Drawing.Size(108, 144);
      this.btnUserPhoto.TabIndex = 78;
      this.btnUserPhoto.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnUserPhoto.Texts")));
      this.btnUserPhoto.TransparentColor = System.Drawing.Color.White;
      this.btnUserPhoto.Click += new System.EventHandler(this.btnUserPhoto_Click);
      // 
      // txtUserName
      // 
      this.txtUserName.Location = new System.Drawing.Point(293, 189);
      this.txtUserName.Name = "txtUserName";
      this.txtUserName.Size = new System.Drawing.Size(100, 21);
      this.txtUserName.TabIndex = 9;
      // 
      // label12
      // 
      this.label12.Location = new System.Drawing.Point(417, 220);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(54, 15);
      this.label12.TabIndex = 77;
      this.label12.TabStop = false;
      this.label12.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label12.Texts")));
      // 
      // label11
      // 
      this.label11.Location = new System.Drawing.Point(416, 189);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(55, 15);
      this.label11.TabIndex = 76;
      this.label11.TabStop = false;
      this.label11.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label11.Texts")));
      // 
      // cmbPost
      // 
      this.cmbPost.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbPost.FormattingEnabled = true;
      this.cmbPost.Location = new System.Drawing.Point(145, 40);
      this.cmbPost.Name = "cmbPost";
      this.cmbPost.Size = new System.Drawing.Size(55, 21);
      this.cmbPost.TabIndex = 3;
      this.cmbPost.SelectedIndexChanged += new System.EventHandler(this.cmbPost_SelectedIndexChanged);
      // 
      // label13
      // 
      this.label13.Location = new System.Drawing.Point(242, 43);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(36, 15);
      this.label13.TabIndex = 72;
      this.label13.TabStop = false;
      this.label13.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label13.Texts")));
      // 
      // txtTelephone
      // 
      this.txtTelephone.Location = new System.Drawing.Point(146, 118);
      this.txtTelephone.Name = "txtTelephone";
      this.txtTelephone.Size = new System.Drawing.Size(81, 21);
      this.txtTelephone.TabIndex = 7;
      // 
      // label10
      // 
      this.label10.Location = new System.Drawing.Point(233, 121);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(53, 15);
      this.label10.TabIndex = 69;
      this.label10.TabStop = false;
      this.label10.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label10.Texts")));
      // 
      // dtBirthDate
      // 
      this.dtBirthDate.ForeColor = System.Drawing.Color.Black;
      this.dtBirthDate.Location = new System.Drawing.Point(262, 145);
      this.dtBirthDate.Name = "dtBirthDate";
      this.dtBirthDate.Size = new System.Drawing.Size(132, 37);
      this.dtBirthDate.TabIndex = 8;
      this.dtBirthDate.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtBirthDate.Texts")));
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(396, 270);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 13;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnOK
      // 
      this.btnOK.ForeColor = System.Drawing.Color.Black;
      this.btnOK.Location = new System.Drawing.Point(145, 270);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 12;
      this.btnOK.Text = "تایید";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // pbFinger
      // 
      this.pbFinger.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbFinger.Image = global::SpecialForce.Properties.Resources.FingerPrint;
      this.pbFinger.InitialImage = null;
      this.pbFinger.Location = new System.Drawing.Point(13, 164);
      this.pbFinger.Name = "pbFinger";
      this.pbFinger.Size = new System.Drawing.Size(109, 129);
      this.pbFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pbFinger.TabIndex = 48;
      this.pbFinger.TabStop = false;
      // 
      // txtMobileNumber
      // 
      this.txtMobileNumber.Location = new System.Drawing.Point(344, 118);
      this.txtMobileNumber.Mask = "0000000";
      this.txtMobileNumber.Name = "txtMobileNumber";
      this.txtMobileNumber.Size = new System.Drawing.Size(49, 21);
      this.txtMobileNumber.TabIndex = 6;
      this.txtMobileNumber.TextChanged += new System.EventHandler(this.txtMobilePrefix_TextChanged);
      // 
      // txtMobilePrefix
      // 
      this.txtMobilePrefix.BeepOnError = true;
      this.txtMobilePrefix.Location = new System.Drawing.Point(313, 118);
      this.txtMobilePrefix.Mask = "000";
      this.txtMobilePrefix.Name = "txtMobilePrefix";
      this.txtMobilePrefix.Size = new System.Drawing.Size(25, 21);
      this.txtMobilePrefix.TabIndex = 5;
      this.txtMobilePrefix.TextChanged += new System.EventHandler(this.txtMobilePrefix_TextChanged);
      // 
      // cmbGender
      // 
      this.cmbGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbGender.FormattingEnabled = true;
      this.cmbGender.Items.AddRange(new object[] {
            "مرد",
            "زن"});
      this.cmbGender.Location = new System.Drawing.Point(145, 16);
      this.cmbGender.Name = "cmbGender";
      this.cmbGender.Size = new System.Drawing.Size(55, 21);
      this.cmbGender.TabIndex = 2;
      // 
      // txtAddress
      // 
      this.txtAddress.Location = new System.Drawing.Point(146, 67);
      this.txtAddress.Multiline = true;
      this.txtAddress.Name = "txtAddress";
      this.txtAddress.Size = new System.Drawing.Size(247, 45);
      this.txtAddress.TabIndex = 4;
      // 
      // txtLastName
      // 
      this.txtLastName.Location = new System.Drawing.Point(293, 40);
      this.txtLastName.Name = "txtLastName";
      this.txtLastName.Size = new System.Drawing.Size(100, 21);
      this.txtLastName.TabIndex = 1;
      // 
      // txtFirstName
      // 
      this.txtFirstName.Location = new System.Drawing.Point(293, 13);
      this.txtFirstName.Name = "txtFirstName";
      this.txtFirstName.Size = new System.Drawing.Size(100, 21);
      this.txtFirstName.TabIndex = 0;
      // 
      // label6
      // 
      this.label6.Location = new System.Drawing.Point(421, 162);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(50, 15);
      this.label6.TabIndex = 56;
      this.label6.TabStop = false;
      this.label6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label6.Texts")));
      // 
      // label5
      // 
      this.label5.Location = new System.Drawing.Point(232, 16);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(46, 15);
      this.label5.TabIndex = 55;
      this.label5.TabStop = false;
      this.label5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label5.Texts")));
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(411, 121);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(60, 15);
      this.label4.TabIndex = 54;
      this.label4.TabStop = false;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(437, 69);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(34, 15);
      this.label3.TabIndex = 53;
      this.label3.TabStop = false;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(403, 42);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(68, 15);
      this.label2.TabIndex = 52;
      this.label2.TabStop = false;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(449, 16);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(21, 15);
      this.label1.TabIndex = 51;
      this.label1.TabStop = false;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // FormUserAddEdit
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(506, 333);
      this.Controls.Add(this.toolBar1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormUserAddEdit";
      this.Text = "مشخصات کابر";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UserAddEditForm_FormClosing);
      this.Load += new System.EventHandler(this.FormUserAddEdit_Load);
      this.toolBar1.ResumeLayout(false);
      this.toolBar1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbFinger)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.OpenFileDialog FileDialog;
    private ToolBar toolBar1;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.PictureBox pbFinger;
    private System.Windows.Forms.MaskedTextBox txtMobileNumber;
    private System.Windows.Forms.MaskedTextBox txtMobilePrefix;
    private System.Windows.Forms.ComboBox cmbGender;
    private System.Windows.Forms.TextBox txtAddress;
    private System.Windows.Forms.TextBox txtLastName;
    private System.Windows.Forms.TextBox txtFirstName;
    private TransparentLabel label6;
    private TransparentLabel label5;
    private TransparentLabel label4;
    private TransparentLabel label3;
    private TransparentLabel label2;
    private TransparentLabel label1;
    private PersianDatePicker dtBirthDate;
    private System.Windows.Forms.TextBox txtTelephone;
    private TransparentLabel label10;
    private System.Windows.Forms.ComboBox cmbPost;
    private TransparentLabel label13;
    private System.Windows.Forms.TextBox txtUserName;
    private TransparentLabel label12;
    private TransparentLabel label11;
    private ToolButton btnUserPhoto;
    private UserInterface.PasswordBox txtRePassword;
    private UserInterface.PasswordBox txtPassword;
  }
}