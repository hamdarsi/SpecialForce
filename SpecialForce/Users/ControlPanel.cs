﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Users
{
  public partial class ControlPanel : SessionAwareForm
  {
    private SearchThread mFingerSearchThread = null;
    private CaptureThread mFingerCaptureThread = new CaptureThread(0);
    private Hashtable mCachedTreats = new Hashtable(); // Maps TreatID => Treat
    private Hashtable mCachedIndex = new Hashtable();  // Maps User ID => Index

    static private ControlPanel mInstance = null;
    static public ControlPanel Instance
    {
      get
      {
        if (mInstance == null || mInstance.IsDisposed)
          mInstance = new ControlPanel();

        return mInstance;
      }
    }

    private ControlPanel()
    {
      InitializeComponent();
    }

    private void ControlPanel_Load(object sender, EventArgs e)
    {
      lvUsers.SetAspect(3, (o) => { return ((DB.User)o).RegistrationDate.ToString(DateTimeString.FullNumericDate); });
      lvUsers.SetAspect(4, (o) => { return ((DB.User)o).BirthDate.ToString(DateTimeString.FullNumericDate); });

      int index = -1;
      List<DB.Site> sites = DB.Site.GetAllSites(true);

      foreach (DB.Site site in sites)
      {
        cmbSites.Items.Add(site);
        if (site.ID == Session.ActiveSiteID)
          index = cmbSites.Items.Count - 1;
      }

      cmbSites.Items.Add("همه باشگاه ها");
      cmbSites.SelectedIndex = index;

      mFingerCaptureThread = FingerPrint.OpenDeviceAsThread(0);
      mFingerCaptureThread.OnCapture += FingerPrintCaptured;
    }

    public void RefreshUserList()
    {
      string time = Profiler.TimeString(() =>
      {
        // Cache Treat ID => Treat for highlighting backgrounds later
        mCachedTreats.Clear();
        foreach (DB.UserSpecialTreat treat in DB.UserSpecialTreat.GetAllActiveTreats())
          mCachedTreats.Add(treat.UserID, treat);

        // check for users with the given search information
        DB.Site site = cmbSites.SelectedItem as DB.Site;
        List<DB.User> users = DB.User.LoadSomeUsers(100, site != null ? site.ID : -1, txtSearch.Text);

        // Cache User ID => index for SelectUser() later
        mCachedIndex.Clear();
        for (int i = 0; i < users.Count; ++i)
          mCachedIndex.Add(users[i].ID, i);

        lvUsers.SetObjects(users);
      });

      Logger.Log("(Users.ControlPanel) Refresh done in " + time);
    }

    private void SelectUser(int id)
    {
      if (mCachedIndex.ContainsKey(id))
      {
        lvUsers.SelectedIndex = (int)(mCachedIndex[id]);
        lvUsers.EnsureVisible(lvUsers.SelectedIndex);
        lvUsers.Select();
      }
      else
        Logger.Log("(Users.ControlPanel) Could not find User with ID " + id.ToString() + " in index cache");
    }

    private void btnUserAdd_Click(object sender, EventArgs e)
    {
      if(ShowModal(new FormUserAddEdit(), false) == DialogResult.OK)
        RefreshUserList();
    }

    private void btnUserEdit_Click(object sender, EventArgs e)
    {
      DB.User user = lvUsers.SelectedObject as DB.User;
      if (user == null)
        Session.ShowError("هیچ کاربری انتخاب نشده است.");
      else if (user.Fired)
        Session.ShowError("کاربر مورد نظر اخراج شده است.");
      else if (Session.CurrentStaff.IsGod() == false && user.RegistrationSiteID != Session.ActiveSiteID)
        Session.ShowError("فقط مشخصات کاربران ثبت نامی همین باشگاه را می توانید ویرایش نمایید.");
      else
      {
        FormUserAddEdit frm = new FormUserAddEdit();
        frm.User = user;
        if (ShowModal(frm, false) == DialogResult.OK)
        {
          RefreshUserList();
          SelectUser(frm.User.ID);
        }
      }
    }

    private void btnUserReportFinancial_Click(object sender, EventArgs e)
    {
      DB.User user = lvUsers.SelectedObject as DB.User;
      EnableReportButtons(false);
      if (user == null)
        Session.ShowError("هیچ کاربری انتخاب نشده است.");
      else if (user.Fired)
        Session.ShowError("کاربر مورد نظر اخراج شده است.");
      else
      {
        FormUserReportSettings frm = new FormUserReportSettings();
        frm.ReportType = FormUserReportSettings.UserReportType.Credit;
        frm.User = user;
        ShowModal(frm, false);
      }
    }

    private void btnLogout_Click(object sender, EventArgs e)
    {
      Session.Lock();
    }

    private void UpdateUserInfoPane()
    {
      lblFirstName.Text = "-";
      lblLastName.Text = "-";
      lblStatus.Text = "-";
      lblTeamName.Text = "-";
      lblCredit.Text = "-";
      lblRegistrationSite.Text = "-";
      pbUserPhoto.Image = Properties.Resources.User_Photo_Place_Holder;
      DB.User user = lvUsers.SelectedObject as DB.User;

      if (user != null)
      {
        lblFirstName.Text = user.FirstName;
        lblLastName.Text = user.LastName;
        lblStatus.Text = "عادی";
        lblTeamName.Text = DB.Team.GetTeamName(user.TeamID, "در هیچ تیمی نیست");
        lblCredit.Text = user.Credit.ToCurrencyString() + " تومان";
        lblRegistrationSite.Text = DB.Site.GetSiteName(user.RegistrationSiteID);
        pbUserPhoto.Image = user.Photo.Resize(pbUserPhoto.Size);

        DB.UserSpecialTreat treat = user.GetCurrentSpecialTreat();
        if (user.Fired)
          lblStatus.Text = "اخراج شد در تاریخ " + user.FireDate.ToString(DateTimeString.DayOfWeekAndDate);
        else if (treat.IsValidBlackList())
          lblStatus.Text = "لیست سیاه: تا تاریخ " + treat.DueDate.ToString(DateTimeString.CompactDate);
        else if (treat.IsValidWhiteList())
          lblStatus.Text = "لیست سفید: تا تاریخ " + treat.DueDate.ToString(DateTimeString.CompactDate);
      }
    }

    private void FingerPrintCaptured(object sender, CaptureEventArgs e)
    {
      if (!Visible)
        return;

      mFingerSearchThread = FingerPrint.SearchWithThread(e.Data);
      mFingerSearchThread.OnSearchFinish += FingerPrintSearchFinish;
      mFingerSearchThread.Start();
    }

    public void FingerPrintSearchFinish(Object sender, SearchFinishEventArgs e)
    {
      if (e.Succeeded)
        SelectUser(e.UserID);
      else
        Session.ShowError("کاربری با اثر انگشت وارد شده یافت نشد.");
    }

    private void btnUserCharge_Click(object sender, EventArgs e)
    {
      DB.User user = lvUsers.SelectedObject as DB.User;
      if (user == null)
        Session.ShowError("هیج کاربری انتخاب نشده است.");
      else if (user.Fired)
        Session.ShowError("کاربر مورد نظر اخراج شده است.");
      else
      {
        FormChargeUserAccount frm = new FormChargeUserAccount();
        frm.User = user;
        ShowModal(frm, false);
        UpdateUserInfoPane();
      }
    }

    private void btnUserRefund_Click(object sender, EventArgs e)
    {
      DB.User user = lvUsers.SelectedObject as DB.User;
      if (user == null)
        Session.ShowError("هیچ کاربری انتخاب نشده است.");
      else if (user.GetCurrentSpecialTreat().IsValidBlackList())
        Session.ShowError("این کاربر در لیست سیاه قرار دارد.");
      else if (user.Fired)
        Session.ShowError("این کاربر اخراج شده است.");
      else
      {
        FormUserRefund frm = new FormUserRefund();
        frm.User = user;
        ShowModal(frm, false);
        UpdateUserInfoPane();
      }
    }

    private void ControlPanelUser_FormClosing(object sender, FormClosingEventArgs e)
    {
      mFingerCaptureThread.OnCapture -= FingerPrintCaptured;
    }

    private void txtSearch_TextChanged(object sender, EventArgs e)
    {
      RefreshUserList();
    }

    private void cmbSites_SelectedIndexChanged(object sender, EventArgs e)
    {
      RefreshUserList();
    }

    private void mnuChargeAccount_Click(object sender, EventArgs e)
    {
      btnUserCharge_Click(sender, e);
    }

    private void mnuRefundAccount_Click(object sender, EventArgs e)
    {
      btnUserRefund_Click(sender, e);
    }

    private void mnuEditUser_Click(object sender, EventArgs e)
    {
      btnUserEdit_Click(sender, e);
    }

    private void mnuRightClick_Opening(object sender, CancelEventArgs e)
    {
      DB.User user = lvUsers.SelectedObject as DB.User;
      if (user == null || user.Fired)
      {
        e.Cancel = true;
        return;
      }

      DB.UserSpecialTreat treat = user.GetCurrentSpecialTreat();

      bool bl = treat.IsValidBlackList();
      mnuRemoveBlackList.Enabled = bl;
      mnuBlackListOneWeek.Enabled = !bl;
      mnuBlackListOneMonth.Enabled = !bl;
      mnuBlackListThreeMonths.Enabled = !bl;
      mnuBlackListOneYear.Enabled = !bl;
      mnuBlackListForEver.Enabled = !bl;

      bool wl = treat.IsValidWhiteList();
      mnuRemoveWhiteList.Enabled = wl;
      mnuWhiteListOneWeek.Enabled = !wl;
      mnuWhiteListOneMonth.Enabled = !wl;
      mnuWhiteListOneYear.Enabled = !wl;
      mnuWhiteListForever.Enabled = !wl;

      mnuWhiteList.Available = Session.CurrentStaff.IsPrivileged();
      mnuFire.Available = Session.CurrentStaff.IsPrivileged();
      mnuFire.Available &= user.IsOperator();
      mnuReportLogins.Available = Session.CurrentStaff.IsPrivileged();
      mnuReportLoginsUser.Enabled = user != null;

      if (user.Fired)
      {
        mnuBlackList.Available = false;
        mnuWhiteList.Available = false;
        mnuFire.Available = false;
      }
    }

    private void mnuBlackListOneWeek_Click(object sender, EventArgs e)
    {
      DB.User user = lvUsers.SelectedObject as DB.User;
      if (user == null)
        return;

      if (user.GetCurrentSpecialTreat().IsValidWhiteList())
      {
        string strAsk = user.FullName;
        strAsk += " هم اکنون در لیست سفید قرار دارد.";
        strAsk += Environment.NewLine;
        strAsk += "آیا میخواهید او را از لیست سفید خارج کنید؟";
        if (!Session.Ask(strAsk))
          return;

        user.CancelCurrentSpecialTreat();
      }

      PersianDateTime dt = PersianDateTime.Today;
      if (sender == mnuBlackListOneWeek)
        dt.TraverseDaysForward(7);
      if (sender == mnuBlackListTwoWeeks)
        dt.TraverseDaysForward(14);
      else if (sender == mnuBlackListOneMonth)
        dt.TraverseMonthsForward(1);
      else if (sender == mnuBlackListThreeMonths)
        dt.TraverseMonthsForward(3);
      else if (sender == mnuBlackListOneYear)
        dt.TraverseYearsForward(1);
      else
        dt.SetTo(4000, 1, 1);

      FormBlackList frm = new FormBlackList();
      frm.User = user;
      frm.Date = dt;

      if(frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        RefreshUserList();
    }

    private void mnuRemoveFromBlackList_Click(object sender, EventArgs e)
    {
      DB.User user = lvUsers.SelectedObject as DB.User;
      if (user == null)
        return;

      user.CancelCurrentSpecialTreat();
      RefreshUserList();
    }

    private void mnuWhiteListOneWeek_Click(object sender, EventArgs e)
    {
      DB.User user = lvUsers.SelectedObject as DB.User;
      if (user == null)
        return;

      if (user.GetCurrentSpecialTreat().IsValidBlackList())
      {
        string strAsk = user.FullName;
        strAsk += " هم اکنون در لیست سیاه قرار دارد.";
        strAsk += Environment.NewLine;
        strAsk += "آیا میخواهید او را از لیست سیاه خارج کنید؟";
        if (!Session.Ask(strAsk))
          return;

        user.CancelCurrentSpecialTreat();
      }

      PersianDateTime dt = PersianDateTime.Today;
      if (sender == mnuWhiteListOneWeek)
        dt.TraverseDaysForward(7);
      else if (sender == mnuWhiteListTwoWeeks)
        dt.TraverseDaysForward(14);
      else if (sender == mnuWhiteListOneMonth)
        dt.TraverseMonthsForward(1);
      else if (sender == mnuWhiteListOneYear)
        dt.TraverseYearsForward(1);
      else
        dt.SetTo(4000, 1, 1);

      user.AssignSpecialTreat(TreatType.WhiteList, dt, "");
      RefreshUserList();
    }

    private void mnuRemoveWhiteList_Click(object sender, EventArgs e)
    {
      DB.User user = lvUsers.SelectedObject as DB.User;
      if (user != null)
      {
        user.CancelCurrentSpecialTreat();
        RefreshUserList();
      }
    }

    private void mnuFire_Click(object sender, EventArgs e)
    {
      DB.User user = lvUsers.SelectedObject as DB.User;
      if (user == null)
        Session.ShowError("هیچ کاربری انتخاب نشده است.");
      else if (user.Fired)
        Session.ShowError("کاربر مورد نظر اخراج شده است.");
      else
      {
        FormFire frm = new FormFire();
        frm.User = user;
        if (frm.ShowDialog() == DialogResult.OK)
          RefreshUserList();
      }
    }

    private void mnuFinancialReport_Click(object sender, EventArgs e)
    {
      btnUserReportFinancial_Click(sender, e);
    }

    private void mnuActivityReport_Click(object sender, EventArgs e)
    {
      btnReportActivities_Click(sender, e);
    }

    private void btnUserReport_MouseEnter(object sender, EventArgs e)
    {
      EnableReportButtons(true);
      EnableRankingButtons(false);
    }

    private void EnableReportButtons(bool enable)
    {
      btnReportActivities.Visible = enable;
      btnReportFinancial.Visible = enable;
    }

    private void EnableRankingButtons(bool enable)
    {
      btnRankCompetitions.Visible = enable;
      btnRankTimeline.Visible = enable;
    }

    private void btnUserRankings_MouseEnter(object sender, EventArgs e)
    {
      btnRankCompetitions.Location = btnReportFinancial.Location;
      btnRankTimeline.Location = btnReportActivities.Location;

      EnableRankingButtons(true);
      EnableReportButtons(false);
    }

    private void btnRankCompetitions_MouseLeave(object sender, EventArgs e)
    {
      EnableRankingButtons(false);
      EnableReportButtons(false);
    }

    private void btnRankCompetitions_Click(object sender, EventArgs e)
    {
      EnableRankingButtons(false);
      ShowModal(new WizardRankingsInCompetitions(), false);
    }

    private void btnRankTimeline_Click(object sender, EventArgs e)
    {
      EnableRankingButtons(false);
      ShowModal(new WizardRankingsInTimeLine(), false);
    }

    private void btnReportActivities_Click(object sender, EventArgs e)
    {
      DB.User user = lvUsers.SelectedObject as DB.User;
      EnableReportButtons(false);
      if (user == null)
        Session.ShowError("هیچ کاربری انتخاب نشده است.");
      else if (user.Fired)
        Session.ShowError("کاربر مورد نظر اخراج شده است.");
      else
      {
        FormUserReportSettings frm = new FormUserReportSettings();
        frm.ReportType = FormUserReportSettings.UserReportType.Games;
        frm.User = user;
        ShowModal(frm, false);
      }
    }

    private void mnuReportLoginsUser_Click(object sender, EventArgs e)
    {
      DB.User user = lvUsers.SelectedObject as DB.User;
      if (user == null)
        Session.ShowError("هیچ کاربری انتخاب نشده است.");
      else if (user.Fired)
        Session.ShowError("کاربر مورد نظر اخراج شده است.");
      else
      {
        FormUserReportSettings frm = new FormUserReportSettings();
        frm.ReportType = FormUserReportSettings.UserReportType.Logins;
        frm.User = user;
        ShowModal(frm, false);
      }
    }

    private void mnuReportLoginsAll_Click(object sender, EventArgs e)
    {
      FormUserReportSettings frm = new FormUserReportSettings();
      frm.ReportType = FormUserReportSettings.UserReportType.Logins;
      ShowModal(frm, false);
    }

    private void lvUsers_SelectedIndexChanged(object sender, EventArgs e)
    {
      UpdateUserInfoPane();
    }

    private void lvUsers_FormatRow(object sender, BrightIdeasSoftware.FormatRowEventArgs e)
    {
      DB.User user = e.Item.RowObject as DB.User;
      if (user.Fired)
        e.Item.BackColor = Color.Gray;
      else if (mCachedTreats.ContainsKey(user.ID))
      {
        DB.UserSpecialTreat treat = mCachedTreats[user.ID] as DB.UserSpecialTreat;
        if (treat.IsValidBlackList())
          e.Item.BackColor = Color.Red;
        else if (treat.IsValidWhiteList())
          e.Item.BackColor = Color.LightBlue;
      }
    }

    private void lvUsers_DoubleClick(object sender, EventArgs e)
    {
      btnUserCharge_Click(sender, e);
    }

    private void lvUsers_ItemDrag(object sender, ItemDragEventArgs e)
    {
      DB.User user = lvUsers.SelectedObject as DB.User;
      if (user == null)
        return;

      if (user.Fired || user.GetCurrentSpecialTreat().IsValidBlackList())
        return;

      DoDragDrop(user, DragDropEffects.Move);
    }
  }
}
