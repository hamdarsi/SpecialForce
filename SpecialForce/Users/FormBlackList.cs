﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Users
{
  public partial class FormBlackList : SessionAwareForm
  {
    private DB.User mUser = null;
    public DB.User User
    {
      get { return mUser; }
      set { mUser = value; }
    }

    private PersianDateTime mDate = null;
    public PersianDateTime Date
    {
      get { return mDate; }
      set { mDate = value; }
    }

    public FormBlackList()
    {
      InitializeComponent();
    }

    private void FormBlackList_Load(object sender, EventArgs e)
    {
      lblFirstName.Text = mUser.FirstName;
      lblLastName.Text = mUser.LastName;
      lblBirthDate.Text = mUser.BirthDate.ToString(DateTimeString.CompactDate);
      lblMembershipDate.Text = mUser.RegistrationDate.ToString(DateTimeString.CompactDate);
      lblCredit.Text = mUser.Credit.ToCurrencyString() + " تومان";
      lblRegistrationSite.Text = DB.Site.GetSiteByID(mUser.RegistrationSiteID).FullName;
      pbUserPhoto.Image = mUser.Photo.Resize(pbUserPhoto.Size);
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      mUser.AssignSpecialTreat(TreatType.BlackList, mDate, txtReason.Text);
      DialogResult = System.Windows.Forms.DialogResult.OK;
    }

    private void txtReason_TextChanged(object sender, EventArgs e)
    {
      btnOK.Enabled = txtReason.Text.Length != 0;
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
