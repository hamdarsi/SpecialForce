﻿namespace SpecialForce.Users
{
  partial class FormUserRankings
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUserRankings));
      this.report1 = new SpecialForce.Reports.Report();
      this.panel1 = new System.Windows.Forms.Panel();
      this.txtSearch = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // report1
      // 
      this.report1.BackColor = System.Drawing.Color.White;
      this.report1.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.report1.ForeColor = System.Drawing.Color.Black;
      this.report1.Location = new System.Drawing.Point(0, 42);
      this.report1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.report1.MinimumSize = new System.Drawing.Size(0, 400);
      this.report1.Name = "report1";
      this.report1.Size = new System.Drawing.Size(712, 488);
      this.report1.TabIndex = 0;
      this.report1.Title = "رتبه بندی کاربران";
      // 
      // panel1
      // 
      this.panel1.BackColor = System.Drawing.Color.White;
      this.panel1.Controls.Add(this.txtSearch);
      this.panel1.Controls.Add(this.label1);
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(712, 100);
      this.panel1.TabIndex = 1;
      // 
      // txtSearch
      // 
      this.txtSearch.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.txtSearch.Location = new System.Drawing.Point(382, 10);
      this.txtSearch.Name = "txtSearch";
      this.txtSearch.Size = new System.Drawing.Size(133, 30);
      this.txtSearch.TabIndex = 1;
      this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.BackColor = System.Drawing.Color.White;
      this.label1.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label1.ForeColor = System.Drawing.Color.Black;
      this.label1.Location = new System.Drawing.Point(523, 13);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(177, 23);
      this.label1.TabIndex = 0;
      this.label1.Text = "برای جستجو نام خود را وارد نمایید:";
      // 
      // FormUserRankings
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoScroll = true;
      this.BackColor = System.Drawing.Color.White;
      this.ClientSize = new System.Drawing.Size(740, 530);
      this.Controls.Add(this.report1);
      this.Controls.Add(this.panel1);
      this.EmptyBackground = true;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormUserRankings";
      this.Text = "رتبه بندی کاربران";
      this.Shown += new System.EventHandler(this.FormUserRankings_Shown);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private Reports.Report report1;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.TextBox txtSearch;
    private System.Windows.Forms.Label label1;
  }
}