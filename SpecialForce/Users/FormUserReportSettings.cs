﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Users
{
  public partial class FormUserReportSettings : SessionAwareForm
  {
    public enum UserReportType
    {
      Credit,
      Logins,
      Games,
      GamesAndRounds
    }

    private DB.User mUser = null;
    public DB.User User
    {
      get { return mUser; }
      set { mUser = value; }
    }

    private UserReportType mReportType;
    public UserReportType ReportType
    {
      get { return mReportType; }
      set { mReportType = value; }
    }

    public FormUserReportSettings()
    {
      InitializeComponent();
    }

    private void cmbReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (cmbReportType.SelectedIndex == 0)
      {
        dtStart.Date = PersianDateTime.Today;
        dtEnd.Date = dtStart.Date.NextDay;
        lblStart.Text = "روز:";
      }
      else if (cmbReportType.SelectedIndex == 1)
      {
        dtStart.Date = PersianDateTime.ThisWeek;
        dtEnd.Date = dtStart.Date.NextWeek;
        lblStart.Text = "شروع هفته:";
      }
      else if (cmbReportType.SelectedIndex == 2)
      {
        dtStart.Date = PersianDateTime.ThisMonth;
        dtEnd.Date = dtStart.Date.NextMonth;
        lblStart.Text = "ماه:";
      }
      else if (cmbReportType.SelectedIndex == 3)
      {
        lblStart.Text = "از:";
      }

      dtStart.ShowDay = cmbReportType.SelectedIndex != 2;
      dtEnd.Visible = cmbReportType.SelectedIndex == 3;
      lblEnd.Visible = dtEnd.Visible;
    }

    private void FormUserReportSettings_Load(object sender, EventArgs e)
    {
      if (mReportType == UserReportType.Credit)
        Text = "گزارش مالی بازیکن";
      else if (mReportType == UserReportType.Games)
        Text = "گزارش فعالیت بازیکن";
      else if (mReportType == UserReportType.GamesAndRounds)
        Text = "ریز بازی های بازیکن";
      else
        Text = "گزارش ورود و خروج کاربر";

      cmbReportType.SelectedIndex = 1;

      List<DB.Site> sites = DB.Site.GetAllSites(true);

      chkSites.Items.Clear();
      btnReport.Enabled = false;
      for (int i = 0; i < sites.Count; ++i)
      {
        chkSites.Items.Add(sites[i]);
        chkSites.SetItemChecked(i, Session.ActiveSiteID == sites[i].ID);
        if (chkSites.GetItemChecked(i))
          btnReport.Enabled = true;
      }
    }

    private void btnReport_Click(object sender, EventArgs e)
    {
      // generate list of sites
      List<DB.Site> sites = new List<DB.Site>();
      for (int i = 0; i < chkSites.Items.Count; ++i)
        if (chkSites.GetItemChecked(i))
          sites.Add(chkSites.Items[i] as DB.Site);

      if (sites.Count == 0)
      {
        Session.ShowError("هیچ باشگاهی انتخاب نشده است.");
        return;
      }

      PersianDateTime start = dtStart.Date;
      PersianDateTime finish;
      if (cmbReportType.SelectedIndex == 0)
        finish = start.NextDay;
      else if (cmbReportType.SelectedIndex == 1)
        finish = start.NextWeek;
      else if (cmbReportType.SelectedIndex == 2)
        finish = start.NextMonth;
      else
        finish = dtEnd.Date;

      if (mReportType == UserReportType.Logins)
      {
        FormUserReportLogins frm = new FormUserReportLogins();
        frm.User = mUser;
        frm.Start = start;
        frm.Finish = finish;
        frm.Sites = sites;
        ShowModal(frm, true);
        return;
      }
      else if (mReportType == UserReportType.Credit)
      {
        FormUserAccountInvoiceReport frm = new FormUserAccountInvoiceReport();
        frm.User = mUser;
        frm.Start = start;
        frm.Finish = finish;
        frm.Sites = sites;
        ShowModal(frm, true);
        return;
      }
      else if (mReportType == UserReportType.Games || mReportType == UserReportType.GamesAndRounds)
      {
        // query all games between the selected dates
        List<DB.Game> games = DB.GameManager.GetGames(
          start,
          finish,
          sites,
          false, // no dummies
          true,
          true,
          true,
          true);

        // Filter games which the user did not participate in them
        for (int i = 0; i < games.Count; ++i)
          if (games[i].BlueUserIDs.Contains(mUser.ID))
            continue;
          else if (games[i].GreenUserIDs.Contains(mUser.ID))
            continue;
          else
            games.RemoveAt(i--);


        // now extract competitions from the list of games
        SortedSet<int> comp_ids = new SortedSet<int>();
        for (int i = 0; i < games.Count; ++i)
          if (games[i].CompetitionID != -1)
          {
            if (!comp_ids.Contains(games[i].CompetitionID))
              comp_ids.Add(games[i].CompetitionID);

            games.RemoveAt(i--);
          }

        // Generate list of competitions
        List<DB.Competition> comps = new List<DB.Competition>();
        foreach (int id in comp_ids)
          comps.Add(DB.CompetitionManager.GetCompetition(id));

        // show the report only for regular games and competitions
        FormUserActivityReport frm = new FormUserActivityReport();
        frm.RoundsInstead = mReportType == UserReportType.GamesAndRounds;
        frm.User = mUser;
        frm.Games = games;
        frm.Competitions = comps;
        ShowModal(frm, true);
      }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
