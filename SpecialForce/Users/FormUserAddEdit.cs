﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace SpecialForce.Users
{
  public partial class FormUserAddEdit : SessionAwareForm
  {
    private CaptureThread mThread = null;
    private CaptureEventArgs mCapturedFP = null;

    private Bitmap mUserPhoto = null;
    private bool mCheckingGod = false;

    private DB.User mUser = null;
    public DB.User User
    {
      get { return mUser; }
      set { mUser = value; }
    }

    public FormUserAddEdit()
    {
      InitializeComponent();
    }

    protected override void ApplyPermissions(Post post)
    {
      cmbPost.Enabled = post.IsPrivileged();
    }

    public void FingerPrintCaptured(object sender, CaptureEventArgs e)
    {
      if (Session.CurrentStaff.IsLowlyUser())
        return;

      mCapturedFP = e;
      pbFinger.Image = e.Image;
    }

    private void UserAddEditForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      mThread.OnCapture -= FingerPrintCaptured;
    }

    private void txtPassword_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.CapsLock)
      {
        e.SuppressKeyPress = true;
        e.Handled = true;
      }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void FormUserAddEdit_Load(object sender, EventArgs e)
    {
      foreach (Post post in Enum.GetValues(typeof(Post)))
        cmbPost.Items.Add(Enums.ToPersianString(post));

      Text = mUser == null ? "اضافه کردن کاربر" : "تغییر مشخصات کاربر";
      if (mUser == null)
        mUser = DB.User.New;

      mCheckingGod = false;
      ResetUserPost();
      mCheckingGod = true;

      txtMobilePrefix.RightToLeft = RightToLeft.No;
      txtMobileNumber.RightToLeft = RightToLeft.No;
      txtUserName.RightToLeft = RightToLeft.No;
      txtPassword.RightToLeft = RightToLeft.No;
      txtRePassword.RightToLeft = RightToLeft.No;
      txtTelephone.RightToLeft = RightToLeft.No;

      txtFirstName.Text = mUser.FirstName;
      txtLastName.Text = mUser.LastName;
      cmbGender.SelectedIndex = mUser.Gender ? 0 : 1;
      txtAddress.Text = mUser.Address;
      txtMobilePrefix.Text = mUser.MobilePrefix;
      txtMobileNumber.Text = mUser.MobileNumber;
      txtTelephone.Text = mUser.Telephone;
      txtUserName.Text = mUser.UserName;
      txtPassword.Text = txtRePassword.Text = mUser.Password;

      mUserPhoto = mUser.ID == -1 ? null : new Bitmap(mUser.Photo);
      mCapturedFP = mUser.ID == -1 ? null : new CaptureEventArgs(mUser.Authentication, mUser.FingerPrintImage);

      if (mUser.ID != -1)
      {
        dtBirthDate.Date = mUser.BirthDate;
        btnUserPhoto.Image = mUserPhoto.Resize(btnUserPhoto.Size);
        pbFinger.Image = mCapturedFP.Image;
      }

      mThread = FingerPrint.OpenDeviceAsThread(0);
      mThread.OnCapture += FingerPrintCaptured;
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      UserNameState un_state = Authentication.CheckUserName(txtUserName.Text, mUser);

      if (txtFirstName.Text == "")
        Session.ShowError("نام نمی تواند خالی باشد.");
      else if (txtLastName.Text == "")
        Session.ShowError("نام خانوداگی نمی تواند خالی باشد.");
      else if (txtMobilePrefix.Text == "")
        Session.ShowError("کد تلفن همراه نمی تواند خالی باشد.");
      else if (txtMobileNumber.Text == "")
        Session.ShowError("شماره تلفن همراه نمی تواند خالی باشد.");
      else if (mUserPhoto == null)
        Session.ShowError("عکس کاربر حتما باید مشخص شود.");
      else if (!mUser.SetPhoto(mUserPhoto))
        Session.ShowError("عکس انتخاب شده حجم بالایی دارد.");
      else if (mCapturedFP == null)
        Session.ShowError("اثر انگشت کاربر حتما باید تنظیم شود.");
      else if (!mUser.SetFingerPrint(mCapturedFP.Data, mCapturedFP.Image))
        Session.ShowError("عکس اثر انگشت حجم بالای دارد.");
      else if (un_state == UserNameState.Duplicate)
        Session.ShowError("کاربر دیگری با نام کاربری وارد شده وجود دارد.");
      else if (un_state == UserNameState.Empty)
        Session.ShowError("نام کاربری وارد نشده است.");
      else if (un_state == UserNameState.InvalidCharacters)
      {
        string strInvalid = "نام کاربری شامل کارکتر های غیر مجاز است.";
        strInvalid += Environment.NewLine;
        strInvalid += "در نام کاربری خط فاصل نمی توان به کار برد.";
        Session.ShowError(strInvalid);
      }
      else if (txtPassword.Text.Length == 0)
        Session.ShowError("رمز عبور نمی تواند خالی باشد.");
      else if (txtPassword.Text != txtRePassword.Text)
        Session.ShowError("کلمه های عبور وارد شده متفاوت هستند.");
      else if (mUser.Post.IsGod() && cmbPost.SelectedIndex != 0 && DB.User.GetNrOfGods() == 1)
        Session.ShowError("در صورت تغییر سمت این کاربر دیگر هیچ پروردگاری در سیستم وجود نخواهد داشت.");
      else
      {
        // check for duplicate user names

        if (mUser.ID != -1)
          FingerPrint.RemoveFingerPrintFromDB(mUser.ID);

        mUser.FirstName = txtFirstName.Text;
        mUser.LastName = txtLastName.Text;
        mUser.Gender = cmbGender.SelectedIndex == 0;
        mUser.Post = Enums.ParsePersianPost(cmbPost.SelectedItem.ToString());
        mUser.Address = txtAddress.Text;
        mUser.MobilePrefix = txtMobilePrefix.Text;
        mUser.MobileNumber = txtMobileNumber.Text;
        mUser.Telephone = txtTelephone.Text;
        mUser.BirthDate = dtBirthDate.Date;
        mUser.UserName = txtUserName.Text;
        mUser.Password = txtPassword.Text;
        mUser.SetFingerPrint(mCapturedFP.Data, mCapturedFP.Image);
        mUser.Apply();

        // now re-insert fingerprint into fast search db
        FingerPrint.AddFingerPrintToDB(mUser.Authentication, mUser.ID);
        DialogResult = DialogResult.OK;
      }
    }

    private void btnUserPhoto_Click(object sender, EventArgs e)
    {
      if (FileDialog.ShowDialog() != DialogResult.OK)
        return;

      mUserPhoto = new Bitmap(FileDialog.FileName);
      btnUserPhoto.Image = mUserPhoto.Resize(btnUserPhoto.Size);
      if (FingerPrint.State == FingerPrintState.FakeActive)
        mCapturedFP = new CaptureEventArgs(new byte[1] { 0 }, new Bitmap(mUserPhoto));
    }

    private void ResetUserPost()
    {
      for (int i = 0; i < cmbPost.Items.Count; ++i)
        if (mUser.Post == Enums.ParsePersianPost(cmbPost.Items[i].ToString()))
        {
          cmbPost.SelectedIndex = i;
          break;
        }
    }

    private void cmbPost_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (!mCheckingGod)
        return;

      if (Session.CurrentStaff.IsGod())
        return;

      Post request = Enums.ParsePersianPost(cmbPost.SelectedItem.ToString());
      if ((request.IsGod() && !Session.CurrentStaff.IsGod()) || 
          (Session.CurrentStaff.IsGod() == false && request.IsGod() == false && mUser.Post.IsGod()))
      {
        Session.ShowError("شما نمی توانید این عملیات را انجام دهید.");
        ResetUserPost();
      }
    }

    private void txtMobilePrefix_TextChanged(object sender, EventArgs e)
    {
      if(mUser.ID == -1)
      {
        txtUserName.Text = txtMobilePrefix.Text + txtMobileNumber.Text;
      }
    }
  }
}
