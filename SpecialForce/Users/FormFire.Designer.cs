﻿namespace SpecialForce.Users
{
  partial class FormFire
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFire));
      this.pnlUserInfo = new SpecialForce.ToolBar();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.pbUserPhoto = new SpecialForce.ToolButton();
      this.lblRegistrationSite = new SpecialForce.TransparentLabel();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.lblCredit = new SpecialForce.TransparentLabel();
      this.lblMembershipDate = new SpecialForce.TransparentLabel();
      this.lblBirthDate = new SpecialForce.TransparentLabel();
      this.lblLastName = new SpecialForce.TransparentLabel();
      this.lblFirstName = new SpecialForce.TransparentLabel();
      this.label5 = new SpecialForce.TransparentLabel();
      this.label4 = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.label2 = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.txtReason = new System.Windows.Forms.TextBox();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.btnFire = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.pnlUserInfo.SuspendLayout();
      this.toolBar1.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlUserInfo
      // 
      this.pnlUserInfo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlUserInfo.BorderWidth = 0.5F;
      this.pnlUserInfo.Controls.Add(this.transparentLabel1);
      this.pnlUserInfo.Controls.Add(this.pbUserPhoto);
      this.pnlUserInfo.Controls.Add(this.lblRegistrationSite);
      this.pnlUserInfo.Controls.Add(this.transparentLabel2);
      this.pnlUserInfo.Controls.Add(this.lblCredit);
      this.pnlUserInfo.Controls.Add(this.lblMembershipDate);
      this.pnlUserInfo.Controls.Add(this.lblBirthDate);
      this.pnlUserInfo.Controls.Add(this.lblLastName);
      this.pnlUserInfo.Controls.Add(this.lblFirstName);
      this.pnlUserInfo.Controls.Add(this.label5);
      this.pnlUserInfo.Controls.Add(this.label4);
      this.pnlUserInfo.Controls.Add(this.label3);
      this.pnlUserInfo.Controls.Add(this.label2);
      this.pnlUserInfo.Controls.Add(this.label1);
      this.pnlUserInfo.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlUserInfo.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlUserInfo.Location = new System.Drawing.Point(12, 12);
      this.pnlUserInfo.Name = "pnlUserInfo";
      this.pnlUserInfo.Size = new System.Drawing.Size(443, 209);
      this.pnlUserInfo.TabIndex = 43;
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Location = new System.Drawing.Point(162, 12);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(169, 15);
      this.transparentLabel1.TabIndex = 25;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // pbUserPhoto
      // 
      this.pbUserPhoto.Enabled = false;
      this.pbUserPhoto.Hint = "";
      this.pbUserPhoto.Image = ((System.Drawing.Bitmap)(resources.GetObject("pbUserPhoto.Image")));
      this.pbUserPhoto.Location = new System.Drawing.Point(15, 52);
      this.pbUserPhoto.Name = "pbUserPhoto";
      this.pbUserPhoto.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pbUserPhoto.Size = new System.Drawing.Size(105, 140);
      this.pbUserPhoto.TabIndex = 24;
      this.pbUserPhoto.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("pbUserPhoto.Texts")));
      this.pbUserPhoto.TransparentColor = System.Drawing.Color.White;
      // 
      // lblRegistrationSite
      // 
      this.lblRegistrationSite.FixFromRight = true;
      this.lblRegistrationSite.Location = new System.Drawing.Point(259, 177);
      this.lblRegistrationSite.Name = "lblRegistrationSite";
      this.lblRegistrationSite.Size = new System.Drawing.Size(8, 15);
      this.lblRegistrationSite.TabIndex = 23;
      this.lblRegistrationSite.TabStop = false;
      this.lblRegistrationSite.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblRegistrationSite.Texts")));
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Location = new System.Drawing.Point(341, 177);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(88, 15);
      this.transparentLabel2.TabIndex = 22;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // lblCredit
      // 
      this.lblCredit.FixFromRight = true;
      this.lblCredit.Location = new System.Drawing.Point(259, 154);
      this.lblCredit.Name = "lblCredit";
      this.lblCredit.Size = new System.Drawing.Size(8, 15);
      this.lblCredit.TabIndex = 20;
      this.lblCredit.TabStop = false;
      this.lblCredit.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblCredit.Texts")));
      // 
      // lblMembershipDate
      // 
      this.lblMembershipDate.FixFromRight = true;
      this.lblMembershipDate.Location = new System.Drawing.Point(259, 130);
      this.lblMembershipDate.Name = "lblMembershipDate";
      this.lblMembershipDate.Size = new System.Drawing.Size(8, 15);
      this.lblMembershipDate.TabIndex = 19;
      this.lblMembershipDate.TabStop = false;
      this.lblMembershipDate.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblMembershipDate.Texts")));
      // 
      // lblBirthDate
      // 
      this.lblBirthDate.FixFromRight = true;
      this.lblBirthDate.Location = new System.Drawing.Point(259, 107);
      this.lblBirthDate.Name = "lblBirthDate";
      this.lblBirthDate.Size = new System.Drawing.Size(8, 15);
      this.lblBirthDate.TabIndex = 18;
      this.lblBirthDate.TabStop = false;
      this.lblBirthDate.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBirthDate.Texts")));
      // 
      // lblLastName
      // 
      this.lblLastName.FixFromRight = true;
      this.lblLastName.Location = new System.Drawing.Point(259, 82);
      this.lblLastName.Name = "lblLastName";
      this.lblLastName.Size = new System.Drawing.Size(8, 15);
      this.lblLastName.TabIndex = 17;
      this.lblLastName.TabStop = false;
      this.lblLastName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblLastName.Texts")));
      // 
      // lblFirstName
      // 
      this.lblFirstName.FixFromRight = true;
      this.lblFirstName.Location = new System.Drawing.Point(259, 58);
      this.lblFirstName.Name = "lblFirstName";
      this.lblFirstName.Size = new System.Drawing.Size(8, 15);
      this.lblFirstName.TabIndex = 16;
      this.lblFirstName.TabStop = false;
      this.lblFirstName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblFirstName.Texts")));
      // 
      // label5
      // 
      this.label5.Location = new System.Drawing.Point(370, 154);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(59, 15);
      this.label5.TabIndex = 15;
      this.label5.TabStop = false;
      this.label5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label5.Texts")));
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(363, 130);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(67, 15);
      this.label4.TabIndex = 14;
      this.label4.TabStop = false;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(380, 106);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(50, 15);
      this.label3.TabIndex = 13;
      this.label3.TabStop = false;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(363, 82);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(68, 15);
      this.label2.TabIndex = 12;
      this.label2.TabStop = false;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(408, 58);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(21, 15);
      this.label1.TabIndex = 11;
      this.label1.TabStop = false;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 0.5F;
      this.toolBar1.Controls.Add(this.txtReason);
      this.toolBar1.Controls.Add(this.transparentLabel3);
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(12, 236);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(443, 132);
      this.toolBar1.TabIndex = 44;
      // 
      // txtReason
      // 
      this.txtReason.Location = new System.Drawing.Point(15, 40);
      this.txtReason.Multiline = true;
      this.txtReason.Name = "txtReason";
      this.txtReason.Size = new System.Drawing.Size(414, 77);
      this.txtReason.TabIndex = 26;
      this.txtReason.TextChanged += new System.EventHandler(this.txtReason_TextChanged);
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.Location = new System.Drawing.Point(296, 13);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(133, 15);
      this.transparentLabel3.TabIndex = 25;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // btnFire
      // 
      this.btnFire.Enabled = false;
      this.btnFire.ForeColor = System.Drawing.Color.Black;
      this.btnFire.Location = new System.Drawing.Point(380, 378);
      this.btnFire.Name = "btnFire";
      this.btnFire.Size = new System.Drawing.Size(75, 23);
      this.btnFire.TabIndex = 45;
      this.btnFire.Text = "تایید";
      this.btnFire.UseVisualStyleBackColor = true;
      this.btnFire.Click += new System.EventHandler(this.btnFire_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(299, 378);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 46;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // FormFire
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(468, 412);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnFire);
      this.Controls.Add(this.toolBar1);
      this.Controls.Add(this.pnlUserInfo);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormFire";
      this.Text = "اخراج";
      this.Load += new System.EventHandler(this.FormFire_Load);
      this.pnlUserInfo.ResumeLayout(false);
      this.toolBar1.ResumeLayout(false);
      this.toolBar1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private ToolBar pnlUserInfo;
    private TransparentLabel transparentLabel1;
    private ToolButton pbUserPhoto;
    private TransparentLabel lblRegistrationSite;
    private TransparentLabel transparentLabel2;
    private TransparentLabel lblCredit;
    private TransparentLabel lblMembershipDate;
    private TransparentLabel lblBirthDate;
    private TransparentLabel lblLastName;
    private TransparentLabel lblFirstName;
    private TransparentLabel label5;
    private TransparentLabel label4;
    private TransparentLabel label3;
    private TransparentLabel label2;
    private TransparentLabel label1;
    private ToolBar toolBar1;
    private System.Windows.Forms.TextBox txtReason;
    private TransparentLabel transparentLabel3;
    private System.Windows.Forms.Button btnFire;
    private System.Windows.Forms.Button btnCancel;
  }
}