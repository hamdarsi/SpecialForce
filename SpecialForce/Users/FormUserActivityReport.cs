﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Users
{
  public partial class FormUserActivityReport : SessionAwareForm
  {
    private DB.User mUser = null;
    public DB.User User
    {
      get { return mUser; }
      set { mUser = value; }
    }

    private List<DB.Game> mGames = null;
    public List<DB.Game> Games
    {
      get { return mGames; }
      set { mGames = value; }
    }

    private List<DB.Competition> mCompetitions = null;
    public List<DB.Competition> Competitions
    {
      get { return mCompetitions; }
      set { mCompetitions = value; }
    }

    private bool mRoundsInstead = false;
    public bool RoundsInstead
    {
      get { return mRoundsInstead; }
      set { mRoundsInstead = value; }
    }

    public FormUserActivityReport()
    {
      InitializeComponent();
    }

    private void FormUserActivityReport_Shown(object sender, EventArgs e)
    {
      // Start
      report1.BeginUpdate();
      report1.Title = "گزارش فعالیت برای " + mUser.FullName;
      Reports.Table table = report1.AddTable();
      table.AutoIndexColumn = true;
      table.AddNewColumn(Types.String, "مسابقه", 250);
      table.AddNewColumn(Types.DateTime, "تاریخ", 70).DateTimeMode = DateTimeString.CompactDate;
      table.AddNewColumn(Types.String, "زمان", 70);
      table.AddNewColumn(Types.String, "نوع", 90);
      table.OnRowDoubleClicked += RowDoubleClicked;

      Statistics<Stats.UserOverall> stats = Stats.Generator.Generate(mUser, mGames, mCompetitions);
      foreach (Stats.UserOverall st in stats.Data)
      {
        Reports.Row row = table.AddNewRow(st);
        row[0] = st.Title;
        row[1] = st.Date;
        row[2] = st.Time;
        row[3] = st.Type;
      }

      stats.PumpMetaData(report1.AddTable(false, 150, 350));

      // finished
      report1.EndUpdate();
    }

    private void RowDoubleClicked(object sender, ReportRowArgs e)
    {
      Stats.UserOverall st = e.Data as Stats.UserOverall;
      if (st.GameInstance == null)
      {
        // its a competition
        // just show all the games of the competition
        // in which the user has played
        List<DB.Game> games = st.CompetitionInstance.Games;
        for(int i = 0; i < games.Count; ++i)
          if (!games[i].BlueUserIDs.Contains(mUser.ID) && !games[i].GreenUserIDs.Contains(mUser.ID))
          {
            games.RemoveAt(i);
            --i;
          }

        // show the report
        FormUserActivityReport frm = new FormUserActivityReport();
        frm.User = mUser;
        frm.Games = games;
        frm.Competitions = null;
        ShowModal(frm, false);
      }
      else
      {
        if (mRoundsInstead)
        {
          Scheduling.FormReportRounds frm = new Scheduling.FormReportRounds();
          frm.Game = st.GameInstance;
          frm.Rounds = Session.CurrentStaff.IsLowlyUser() ? frm.Game.Rounds : frm.Game.AllRounds;
          ShowModal(frm, false);
        }
        else
        {
          FormUserActivityDetail frm = new FormUserActivityDetail();
          frm.User = mUser;
          frm.Game = st.GameInstance;
          ShowModal(frm, false);
        }
      }
    }
  }
}
