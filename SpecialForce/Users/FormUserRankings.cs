﻿using System;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace SpecialForce.Users
{
  public partial class FormUserRankings : SessionAwareForm
  {
    private List<DB.UserStats> mStats = null;

    private List<int> mGameIDs = null;
    public List<int> GameIDs
    {
      get { return mGameIDs; }
      set { mGameIDs = value; }
    }

    private string mTitle = "";
    public string Title
    {
      get { return mTitle; }
      set { mTitle = value; }
    }

    public FormUserRankings()
    {
      InitializeComponent();
    }

    private void FormUserRankings_Shown(object sender, EventArgs e)
    {
      // Start
      mStats = DB.UserStats.GenerateStats(mGameIDs, true, true, true, true, true, false, true);
      ShowStats();
    }

    private void ShowStats()
    {
      report1.BeginUpdate();

      List<DB.UserStats> stats =
        txtSearch.Text.Length == 0 ?
        mStats : mStats.Where(stat => stat.FullName.Contains(txtSearch.Text)).ToList();

      report1.Title = mTitle;
      Reports.Table table = report1.TableCount == 0 ? report1.AddTable() : report1.GetTable(0);
      table.AddNewColumn(Types.Integer, "رتبه", 45);
      table.AddNewColumn(Types.String, "بازیکن", 150);
      table.AddNewColumn(Types.Integer, "فرگ", 50);
      table.AddNewColumn(Types.Integer, "کشتن", 50);
      table.AddNewColumn(Types.Integer, "کشتن خودی", 85);
      table.AddNewColumn(Types.Integer, "مردن", 50);
      table.AddNewColumn(Types.Integer, "بمب گذاری", 80);
      table.AddNewColumn(Types.Integer, "خنثی سازی", 80);

      foreach (DB.UserStats st in stats)
      {
        Reports.Row row = table.AddNewRow(st);
        row[0] = st.Rank;
        row[1] = st.FullName;
        row[2] = st.TotalFrag;
        row[3] = st.EnemyKills;
        row[4] = st.FriendlyKills;
        row[5] = st.TotalDeaths;
        row[6] = st.SuccessfulPlants;
        row[7] = st.Diffuses;
      }

      // finished
      report1.EndUpdate();

      // Sort on Rankings
      table.Sort(0);

      // Scroll to txtSearch
      ScrollControlIntoView(txtSearch);
      panel1.Width = report1.Width;
    }

    private void txtSearch_TextChanged(object sender, EventArgs e)
    {
      Enabled = false;
      ShowStats();
      Enabled = true;
    }
  }
}
