﻿using System;
using System.Windows.Forms;

namespace SpecialForce.Users
{
  public partial class FormChargeUserAccount : SessionAwareForm
  {
    private DB.User mUser = null;
    public DB.User User
    {
      set { mUser = value; }
    }

    public FormChargeUserAccount()
    {
      InitializeComponent();
    }

    private void FormChargeUserAccount_Load(object sender, EventArgs e)
    {
      if (mUser == null)
        throw new Exception("(FormChargeUserAccount) no user given to charge");

      lblFirstName.Text = mUser.FirstName;
      lblLastName.Text = mUser.LastName;
      lblMembershipDate.Text = mUser.RegistrationDate.ToString(DateTimeString.CompactDate);
      lblCurrentCredit.Text = mUser.Credit.ToCurrencyString() + " تومان";
      pbUserPhoto.Image = mUser.Photo.Resize(pbUserPhoto.Size);

      foreach (DB.Invoice invoice in mUser.GetRecentInvoiceHistory())
      {
        if (invoice.Type == InvoiceType.UserCreditUsage)
          continue;

        string type_str = "شارژ";
        if (invoice.Type == InvoiceType.UserGiftCharge)
          type_str = "هدیه";
        else if (invoice.Type == InvoiceType.UserCreditRefund)
          type_str = "عودت";

        ListViewItem it = new ListViewItem(invoice.TimeStamp.ToString(DateTimeString.CompactDateTime));
        it.SubItems.Add(invoice.Amount.ToCurrencyString() + " تومان");
        it.SubItems.Add(type_str);
        UserCharges.Items.Add(it);
      }
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      if (txtCredit.Value > 0)
      {
        mUser.Charge(txtCredit.Value);
        DialogResult = DialogResult.OK;
      }
      else
        Session.ShowError("مبلغ وارد شده صحیح نیست.");
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
