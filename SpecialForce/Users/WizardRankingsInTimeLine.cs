﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Users
{
  public partial class WizardRankingsInTimeLine : SessionAwareForm
  {
    public WizardRankingsInTimeLine()
    {
      InitializeComponent();
    }

    private void WizardRankingsInTimeLine_Load(object sender, EventArgs e)
    {
      dtStart.Date = PersianDateTime.ThisMonth;
      dtEnd.Date = dtStart.Date.NextMonth;

      chkSites.Items.Clear();
      List<DB.Site> sites = DB.Site.GetAllSites(true);
      for (int i = 0; i < sites.Count; ++i)
      {
        chkSites.Items.Add(sites[i]);
        chkSites.SetItemChecked(i, Session.ActiveSiteID == sites[i].ID);
      }
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      // generate list of sites
      List<DB.Site> sites = new List<DB.Site>();
      for (int i = 0; i < chkSites.Items.Count; ++i)
        if (chkSites.GetItemChecked(i))
          sites.Add(chkSites.Items[i] as DB.Site);

      if (sites.Count == 0)
      {
        Session.ShowError("هیچ باشگاهی انتخاب نشده است.");
        return;
      }

      bool incCups = chkCups.Checked;
      bool incElims = chkEliminations.Checked;
      bool incLeagues = chkLeagues.Checked;
      bool incRegulars = chkRegularMatches.Checked;

      if (!incCups && !incElims && !incLeagues && !incRegulars)
      {
        Session.ShowError("هیج نوع مسابقه ای انتخاب نشده است!");
        return;
      }

      string strTypes = "";
      if (incCups)
        strTypes = Enums.AddToken(strTypes, Enums.ToString(CompetitionType.Cup));
      if (incElims)
        strTypes = Enums.AddToken(strTypes, Enums.ToString(CompetitionType.Elimination));
      if (incLeagues)
        strTypes = Enums.AddToken(strTypes, Enums.ToString(CompetitionType.League));
      if (incRegulars)
        strTypes = Enums.AddToken(strTypes, Enums.ToString(CompetitionType.FreeMatch));

      string title = "رتبه بندی کاربران از ";
      title += dtStart.Date.ToString(DateTimeString.CompactDate);
      title += " تا ";
      title += dtEnd.Date.ToString(DateTimeString.CompactDate);
      title += " شامل: ";
      title += strTypes;

      // show the report
      FormUserRankings frm = new FormUserRankings();
      frm.Title = title;
      frm.GameIDs = DB.GameManager.GetGameIDs(
        dtStart.Date,
        dtEnd.Date,
        sites,
        false,
        incRegulars,
        incCups,
        incLeagues,
        incElims);
      ShowModal(frm, true);
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
