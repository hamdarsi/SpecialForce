﻿namespace SpecialForce.Users
{
  partial class ControlPanel
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPanel));
      this.mnuRightClick = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mnuChargeAccount = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuRefundAccount = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuFinancialReport = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuActivityReport = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuReportLogins = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuReportLoginsUser = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuReportLoginsAll = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
      this.mnuEditUser = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuBlackList = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuBlackListOneWeek = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuBlackListTwoWeeks = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuBlackListOneMonth = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuBlackListThreeMonths = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuBlackListOneYear = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuBlackListForEver = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
      this.mnuRemoveBlackList = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuWhiteList = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuWhiteListOneWeek = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuWhiteListOneMonth = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuWhiteListOneYear = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuWhiteListForever = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
      this.mnuRemoveWhiteList = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuFire = new System.Windows.Forms.ToolStripMenuItem();
      this.label11 = new SpecialForce.TransparentLabel();
      this.txtSearch = new System.Windows.Forms.TextBox();
      this.tbButtons = new SpecialForce.ToolBar();
      this.btnUserRankings = new SpecialForce.ToolButton();
      this.btnUserReport = new SpecialForce.ToolButton();
      this.btnEdit = new SpecialForce.ToolButton();
      this.toolButton6 = new SpecialForce.ToolButton();
      this.btnRefund = new SpecialForce.ToolButton();
      this.btnCharge = new SpecialForce.ToolButton();
      this.btnAdd = new SpecialForce.ToolButton();
      this.pnlUserInfo = new SpecialForce.ToolBar();
      this.pbUserPhoto = new SpecialForce.ToolButton();
      this.lblRegistrationSite = new SpecialForce.TransparentLabel();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.lblCredit = new SpecialForce.TransparentLabel();
      this.lblTeamName = new SpecialForce.TransparentLabel();
      this.lblStatus = new SpecialForce.TransparentLabel();
      this.lblLastName = new SpecialForce.TransparentLabel();
      this.lblFirstName = new SpecialForce.TransparentLabel();
      this.label5 = new SpecialForce.TransparentLabel();
      this.label4 = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.label2 = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.tinypicker1 = new SpecialForce.TinyPicker();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.cmbSites = new System.Windows.Forms.ComboBox();
      this.btnRankTimeline = new SpecialForce.ToolButton();
      this.btnRankCompetitions = new SpecialForce.ToolButton();
      this.btnReportActivities = new SpecialForce.ToolButton();
      this.btnReportFinancial = new SpecialForce.ToolButton();
      this.lvUsers = new SpecialForce.UserInterface.ListView();
      this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn5 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.mnuWhiteListTwoWeeks = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuRightClick.SuspendLayout();
      this.tbButtons.SuspendLayout();
      this.pnlUserInfo.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lvUsers)).BeginInit();
      this.SuspendLayout();
      // 
      // mnuRightClick
      // 
      this.mnuRightClick.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuChargeAccount,
            this.mnuRefundAccount,
            this.mnuFinancialReport,
            this.mnuActivityReport,
            this.mnuReportLogins,
            this.toolStripMenuItem3,
            this.mnuEditUser,
            this.mnuBlackList,
            this.mnuWhiteList,
            this.mnuFire});
      this.mnuRightClick.Name = "mnuRightClick";
      this.mnuRightClick.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      this.mnuRightClick.Size = new System.Drawing.Size(165, 208);
      this.mnuRightClick.Opening += new System.ComponentModel.CancelEventHandler(this.mnuRightClick_Opening);
      // 
      // mnuChargeAccount
      // 
      this.mnuChargeAccount.Name = "mnuChargeAccount";
      this.mnuChargeAccount.Size = new System.Drawing.Size(164, 22);
      this.mnuChargeAccount.Text = "شارژ";
      this.mnuChargeAccount.Click += new System.EventHandler(this.mnuChargeAccount_Click);
      // 
      // mnuRefundAccount
      // 
      this.mnuRefundAccount.Name = "mnuRefundAccount";
      this.mnuRefundAccount.Size = new System.Drawing.Size(164, 22);
      this.mnuRefundAccount.Text = "عودت";
      this.mnuRefundAccount.Click += new System.EventHandler(this.mnuRefundAccount_Click);
      // 
      // mnuFinancialReport
      // 
      this.mnuFinancialReport.Name = "mnuFinancialReport";
      this.mnuFinancialReport.Size = new System.Drawing.Size(164, 22);
      this.mnuFinancialReport.Text = "گزارش مالی";
      this.mnuFinancialReport.Click += new System.EventHandler(this.mnuFinancialReport_Click);
      // 
      // mnuActivityReport
      // 
      this.mnuActivityReport.Name = "mnuActivityReport";
      this.mnuActivityReport.Size = new System.Drawing.Size(164, 22);
      this.mnuActivityReport.Text = "گزارش فعالیت";
      this.mnuActivityReport.Click += new System.EventHandler(this.mnuActivityReport_Click);
      // 
      // mnuReportLogins
      // 
      this.mnuReportLogins.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuReportLoginsUser,
            this.mnuReportLoginsAll});
      this.mnuReportLogins.Name = "mnuReportLogins";
      this.mnuReportLogins.Size = new System.Drawing.Size(164, 22);
      this.mnuReportLogins.Text = "گزارش ورود و خروج";
      // 
      // mnuReportLoginsUser
      // 
      this.mnuReportLoginsUser.Name = "mnuReportLoginsUser";
      this.mnuReportLoginsUser.Size = new System.Drawing.Size(100, 22);
      this.mnuReportLoginsUser.Text = "کاربر";
      this.mnuReportLoginsUser.Click += new System.EventHandler(this.mnuReportLoginsUser_Click);
      // 
      // mnuReportLoginsAll
      // 
      this.mnuReportLoginsAll.Name = "mnuReportLoginsAll";
      this.mnuReportLoginsAll.Size = new System.Drawing.Size(100, 22);
      this.mnuReportLoginsAll.Text = "کلی";
      this.mnuReportLoginsAll.Click += new System.EventHandler(this.mnuReportLoginsAll_Click);
      // 
      // toolStripMenuItem3
      // 
      this.toolStripMenuItem3.Name = "toolStripMenuItem3";
      this.toolStripMenuItem3.Size = new System.Drawing.Size(161, 6);
      // 
      // mnuEditUser
      // 
      this.mnuEditUser.Name = "mnuEditUser";
      this.mnuEditUser.Size = new System.Drawing.Size(164, 22);
      this.mnuEditUser.Text = "تغییر مشخصات";
      this.mnuEditUser.Click += new System.EventHandler(this.mnuEditUser_Click);
      // 
      // mnuBlackList
      // 
      this.mnuBlackList.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuBlackListOneWeek,
            this.mnuBlackListTwoWeeks,
            this.mnuBlackListOneMonth,
            this.mnuBlackListThreeMonths,
            this.mnuBlackListOneYear,
            this.mnuBlackListForEver,
            this.toolStripMenuItem2,
            this.mnuRemoveBlackList});
      this.mnuBlackList.Name = "mnuBlackList";
      this.mnuBlackList.Size = new System.Drawing.Size(164, 22);
      this.mnuBlackList.Text = "لیست سیاه";
      // 
      // mnuBlackListOneWeek
      // 
      this.mnuBlackListOneWeek.Name = "mnuBlackListOneWeek";
      this.mnuBlackListOneWeek.Size = new System.Drawing.Size(159, 22);
      this.mnuBlackListOneWeek.Text = "برای 1 هفته";
      this.mnuBlackListOneWeek.Click += new System.EventHandler(this.mnuBlackListOneWeek_Click);
      // 
      // mnuBlackListTwoWeeks
      // 
      this.mnuBlackListTwoWeeks.Name = "mnuBlackListTwoWeeks";
      this.mnuBlackListTwoWeeks.Size = new System.Drawing.Size(159, 22);
      this.mnuBlackListTwoWeeks.Text = "برای 2 هفته";
      this.mnuBlackListTwoWeeks.Click += new System.EventHandler(this.mnuBlackListOneWeek_Click);
      // 
      // mnuBlackListOneMonth
      // 
      this.mnuBlackListOneMonth.Name = "mnuBlackListOneMonth";
      this.mnuBlackListOneMonth.Size = new System.Drawing.Size(159, 22);
      this.mnuBlackListOneMonth.Text = "برای 1 ماه";
      this.mnuBlackListOneMonth.Click += new System.EventHandler(this.mnuBlackListOneWeek_Click);
      // 
      // mnuBlackListThreeMonths
      // 
      this.mnuBlackListThreeMonths.Name = "mnuBlackListThreeMonths";
      this.mnuBlackListThreeMonths.Size = new System.Drawing.Size(159, 22);
      this.mnuBlackListThreeMonths.Text = "برای 3 ماه";
      this.mnuBlackListThreeMonths.Click += new System.EventHandler(this.mnuBlackListOneWeek_Click);
      // 
      // mnuBlackListOneYear
      // 
      this.mnuBlackListOneYear.Name = "mnuBlackListOneYear";
      this.mnuBlackListOneYear.Size = new System.Drawing.Size(159, 22);
      this.mnuBlackListOneYear.Text = "برای 1 سال";
      this.mnuBlackListOneYear.Click += new System.EventHandler(this.mnuBlackListOneWeek_Click);
      // 
      // mnuBlackListForEver
      // 
      this.mnuBlackListForEver.Name = "mnuBlackListForEver";
      this.mnuBlackListForEver.Size = new System.Drawing.Size(159, 22);
      this.mnuBlackListForEver.Text = "برای همیشه";
      this.mnuBlackListForEver.Click += new System.EventHandler(this.mnuBlackListOneWeek_Click);
      // 
      // toolStripMenuItem2
      // 
      this.toolStripMenuItem2.Name = "toolStripMenuItem2";
      this.toolStripMenuItem2.Size = new System.Drawing.Size(156, 6);
      // 
      // mnuRemoveBlackList
      // 
      this.mnuRemoveBlackList.Name = "mnuRemoveBlackList";
      this.mnuRemoveBlackList.Size = new System.Drawing.Size(159, 22);
      this.mnuRemoveBlackList.Text = "حذف از لیست سیاه";
      this.mnuRemoveBlackList.Click += new System.EventHandler(this.mnuRemoveFromBlackList_Click);
      // 
      // mnuWhiteList
      // 
      this.mnuWhiteList.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuWhiteListOneWeek,
            this.mnuWhiteListTwoWeeks,
            this.mnuWhiteListOneMonth,
            this.mnuWhiteListOneYear,
            this.mnuWhiteListForever,
            this.toolStripMenuItem1,
            this.mnuRemoveWhiteList});
      this.mnuWhiteList.Name = "mnuWhiteList";
      this.mnuWhiteList.Size = new System.Drawing.Size(164, 22);
      this.mnuWhiteList.Text = "لیست سفید";
      // 
      // mnuWhiteListOneWeek
      // 
      this.mnuWhiteListOneWeek.Name = "mnuWhiteListOneWeek";
      this.mnuWhiteListOneWeek.Size = new System.Drawing.Size(160, 22);
      this.mnuWhiteListOneWeek.Text = "برای 1 هفته";
      this.mnuWhiteListOneWeek.Click += new System.EventHandler(this.mnuWhiteListOneWeek_Click);
      // 
      // mnuWhiteListOneMonth
      // 
      this.mnuWhiteListOneMonth.Name = "mnuWhiteListOneMonth";
      this.mnuWhiteListOneMonth.Size = new System.Drawing.Size(160, 22);
      this.mnuWhiteListOneMonth.Text = "برای 1 ماه";
      this.mnuWhiteListOneMonth.Click += new System.EventHandler(this.mnuWhiteListOneWeek_Click);
      // 
      // mnuWhiteListOneYear
      // 
      this.mnuWhiteListOneYear.Name = "mnuWhiteListOneYear";
      this.mnuWhiteListOneYear.Size = new System.Drawing.Size(160, 22);
      this.mnuWhiteListOneYear.Text = "برای 1 سال";
      this.mnuWhiteListOneYear.Click += new System.EventHandler(this.mnuWhiteListOneWeek_Click);
      // 
      // mnuWhiteListForever
      // 
      this.mnuWhiteListForever.Name = "mnuWhiteListForever";
      this.mnuWhiteListForever.Size = new System.Drawing.Size(160, 22);
      this.mnuWhiteListForever.Text = "برای همیشه";
      this.mnuWhiteListForever.Click += new System.EventHandler(this.mnuWhiteListOneWeek_Click);
      // 
      // toolStripMenuItem1
      // 
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.Size = new System.Drawing.Size(157, 6);
      // 
      // mnuRemoveWhiteList
      // 
      this.mnuRemoveWhiteList.Name = "mnuRemoveWhiteList";
      this.mnuRemoveWhiteList.Size = new System.Drawing.Size(160, 22);
      this.mnuRemoveWhiteList.Text = "حذف از لیست سفید";
      this.mnuRemoveWhiteList.Click += new System.EventHandler(this.mnuRemoveWhiteList_Click);
      // 
      // mnuFire
      // 
      this.mnuFire.Name = "mnuFire";
      this.mnuFire.Size = new System.Drawing.Size(164, 22);
      this.mnuFire.Text = "اخراج";
      this.mnuFire.Click += new System.EventHandler(this.mnuFire_Click);
      // 
      // label11
      // 
      this.label11.Location = new System.Drawing.Point(98, 13);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(44, 15);
      this.label11.TabIndex = 3;
      this.label11.TabStop = false;
      this.label11.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label11.Texts")));
      // 
      // txtSearch
      // 
      this.txtSearch.Location = new System.Drawing.Point(147, 10);
      this.txtSearch.Name = "txtSearch";
      this.txtSearch.Size = new System.Drawing.Size(207, 21);
      this.txtSearch.TabIndex = 0;
      this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
      // 
      // tbButtons
      // 
      this.tbButtons.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbButtons.BorderWidth = 0.5F;
      this.tbButtons.Controls.Add(this.btnUserRankings);
      this.tbButtons.Controls.Add(this.btnUserReport);
      this.tbButtons.Controls.Add(this.btnEdit);
      this.tbButtons.Controls.Add(this.toolButton6);
      this.tbButtons.Controls.Add(this.btnRefund);
      this.tbButtons.Controls.Add(this.btnCharge);
      this.tbButtons.Controls.Add(this.btnAdd);
      this.tbButtons.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbButtons.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbButtons.Location = new System.Drawing.Point(11, 9);
      this.tbButtons.Name = "tbButtons";
      this.tbButtons.Size = new System.Drawing.Size(81, 546);
      this.tbButtons.TabIndex = 41;
      // 
      // btnUserRankings
      // 
      this.btnUserRankings.Hint = "رتبه بندی کاربران";
      this.btnUserRankings.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnUserRankings.Image")));
      this.btnUserRankings.Location = new System.Drawing.Point(8, 359);
      this.btnUserRankings.Name = "btnUserRankings";
      this.btnUserRankings.NormalBorderColor = System.Drawing.Color.DimGray;
      this.btnUserRankings.Size = new System.Drawing.Size(64, 64);
      this.btnUserRankings.TabIndex = 47;
      this.btnUserRankings.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnUserRankings.Texts")));
      this.btnUserRankings.TransparentColor = System.Drawing.Color.White;
      this.btnUserRankings.MouseEnter += new System.EventHandler(this.btnUserRankings_MouseEnter);
      // 
      // btnUserReport
      // 
      this.btnUserReport.Hint = "گزارش کاربر";
      this.btnUserReport.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnUserReport.Image")));
      this.btnUserReport.Location = new System.Drawing.Point(8, 289);
      this.btnUserReport.Name = "btnUserReport";
      this.btnUserReport.NormalBorderColor = System.Drawing.Color.DimGray;
      this.btnUserReport.Size = new System.Drawing.Size(64, 64);
      this.btnUserReport.TabIndex = 46;
      this.btnUserReport.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnUserReport.Texts")));
      this.btnUserReport.TransparentColor = System.Drawing.Color.White;
      this.btnUserReport.MouseEnter += new System.EventHandler(this.btnUserReport_MouseEnter);
      // 
      // btnEdit
      // 
      this.btnEdit.Hint = "ویرایش اطلاعات کاربر";
      this.btnEdit.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnEdit.Image")));
      this.btnEdit.Location = new System.Drawing.Point(8, 79);
      this.btnEdit.Name = "btnEdit";
      this.btnEdit.NormalBorderColor = System.Drawing.Color.DimGray;
      this.btnEdit.Size = new System.Drawing.Size(64, 64);
      this.btnEdit.TabIndex = 20;
      this.btnEdit.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnEdit.Texts")));
      this.btnEdit.TransparentColor = System.Drawing.Color.White;
      this.btnEdit.Click += new System.EventHandler(this.btnUserEdit_Click);
      this.btnEdit.MouseEnter += new System.EventHandler(this.btnRankCompetitions_MouseLeave);
      // 
      // toolButton6
      // 
      this.toolButton6.Hint = "قفل سیستم";
      this.toolButton6.Image = ((System.Drawing.Bitmap)(resources.GetObject("toolButton6.Image")));
      this.toolButton6.Location = new System.Drawing.Point(8, 476);
      this.toolButton6.Name = "toolButton6";
      this.toolButton6.NormalBorderColor = System.Drawing.Color.DimGray;
      this.toolButton6.Size = new System.Drawing.Size(64, 64);
      this.toolButton6.TabIndex = 18;
      this.toolButton6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("toolButton6.Texts")));
      this.toolButton6.TransparentColor = System.Drawing.Color.Transparent;
      this.toolButton6.Click += new System.EventHandler(this.btnLogout_Click);
      this.toolButton6.MouseEnter += new System.EventHandler(this.btnRankCompetitions_MouseLeave);
      // 
      // btnRefund
      // 
      this.btnRefund.Hint = "عودت شارژ به کاربر";
      this.btnRefund.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnRefund.Image")));
      this.btnRefund.Location = new System.Drawing.Point(8, 219);
      this.btnRefund.Name = "btnRefund";
      this.btnRefund.NormalBorderColor = System.Drawing.Color.DimGray;
      this.btnRefund.Size = new System.Drawing.Size(64, 64);
      this.btnRefund.TabIndex = 17;
      this.btnRefund.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnRefund.Texts")));
      this.btnRefund.TransparentColor = System.Drawing.Color.White;
      this.btnRefund.Click += new System.EventHandler(this.btnUserRefund_Click);
      this.btnRefund.MouseEnter += new System.EventHandler(this.btnRankCompetitions_MouseLeave);
      // 
      // btnCharge
      // 
      this.btnCharge.Hint = "شارژ حساب کاربر";
      this.btnCharge.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnCharge.Image")));
      this.btnCharge.Location = new System.Drawing.Point(8, 149);
      this.btnCharge.Name = "btnCharge";
      this.btnCharge.NormalBorderColor = System.Drawing.Color.DimGray;
      this.btnCharge.Size = new System.Drawing.Size(64, 64);
      this.btnCharge.TabIndex = 15;
      this.btnCharge.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnCharge.Texts")));
      this.btnCharge.TransparentColor = System.Drawing.Color.White;
      this.btnCharge.Click += new System.EventHandler(this.btnUserCharge_Click);
      this.btnCharge.MouseEnter += new System.EventHandler(this.btnRankCompetitions_MouseLeave);
      // 
      // btnAdd
      // 
      this.btnAdd.Hint = "ثبت کاربر جدید";
      this.btnAdd.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnAdd.Image")));
      this.btnAdd.Location = new System.Drawing.Point(8, 9);
      this.btnAdd.Name = "btnAdd";
      this.btnAdd.NormalBorderColor = System.Drawing.Color.DimGray;
      this.btnAdd.Size = new System.Drawing.Size(64, 64);
      this.btnAdd.TabIndex = 13;
      this.btnAdd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnAdd.Texts")));
      this.btnAdd.TransparentColor = System.Drawing.Color.White;
      this.btnAdd.Click += new System.EventHandler(this.btnUserAdd_Click);
      this.btnAdd.MouseEnter += new System.EventHandler(this.btnRankCompetitions_MouseLeave);
      // 
      // pnlUserInfo
      // 
      this.pnlUserInfo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlUserInfo.BorderWidth = 0.5F;
      this.pnlUserInfo.Controls.Add(this.pbUserPhoto);
      this.pnlUserInfo.Controls.Add(this.lblRegistrationSite);
      this.pnlUserInfo.Controls.Add(this.transparentLabel2);
      this.pnlUserInfo.Controls.Add(this.lblCredit);
      this.pnlUserInfo.Controls.Add(this.lblTeamName);
      this.pnlUserInfo.Controls.Add(this.lblStatus);
      this.pnlUserInfo.Controls.Add(this.lblLastName);
      this.pnlUserInfo.Controls.Add(this.lblFirstName);
      this.pnlUserInfo.Controls.Add(this.label5);
      this.pnlUserInfo.Controls.Add(this.label4);
      this.pnlUserInfo.Controls.Add(this.label3);
      this.pnlUserInfo.Controls.Add(this.label2);
      this.pnlUserInfo.Controls.Add(this.label1);
      this.pnlUserInfo.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlUserInfo.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlUserInfo.Location = new System.Drawing.Point(98, 397);
      this.pnlUserInfo.Name = "pnlUserInfo";
      this.pnlUserInfo.Size = new System.Drawing.Size(451, 158);
      this.pnlUserInfo.TabIndex = 42;
      // 
      // pbUserPhoto
      // 
      this.pbUserPhoto.Hint = "تصویر کاربر";
      this.pbUserPhoto.Image = ((System.Drawing.Bitmap)(resources.GetObject("pbUserPhoto.Image")));
      this.pbUserPhoto.Location = new System.Drawing.Point(12, 9);
      this.pbUserPhoto.Name = "pbUserPhoto";
      this.pbUserPhoto.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pbUserPhoto.Size = new System.Drawing.Size(105, 140);
      this.pbUserPhoto.TabIndex = 24;
      this.pbUserPhoto.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("pbUserPhoto.Texts")));
      this.pbUserPhoto.TransparentColor = System.Drawing.Color.White;
      // 
      // lblRegistrationSite
      // 
      this.lblRegistrationSite.FixFromRight = true;
      this.lblRegistrationSite.Location = new System.Drawing.Point(284, 129);
      this.lblRegistrationSite.Name = "lblRegistrationSite";
      this.lblRegistrationSite.Size = new System.Drawing.Size(8, 15);
      this.lblRegistrationSite.TabIndex = 23;
      this.lblRegistrationSite.TabStop = false;
      this.lblRegistrationSite.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblRegistrationSite.Texts")));
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Location = new System.Drawing.Point(349, 129);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(88, 15);
      this.transparentLabel2.TabIndex = 22;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // lblCredit
      // 
      this.lblCredit.FixFromRight = true;
      this.lblCredit.Location = new System.Drawing.Point(284, 106);
      this.lblCredit.Name = "lblCredit";
      this.lblCredit.Size = new System.Drawing.Size(8, 15);
      this.lblCredit.TabIndex = 20;
      this.lblCredit.TabStop = false;
      this.lblCredit.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblCredit.Texts")));
      // 
      // lblTeamName
      // 
      this.lblTeamName.FixFromRight = true;
      this.lblTeamName.Location = new System.Drawing.Point(284, 82);
      this.lblTeamName.Name = "lblTeamName";
      this.lblTeamName.Size = new System.Drawing.Size(8, 15);
      this.lblTeamName.TabIndex = 19;
      this.lblTeamName.TabStop = false;
      this.lblTeamName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblTeamName.Texts")));
      // 
      // lblStatus
      // 
      this.lblStatus.FixFromRight = true;
      this.lblStatus.Location = new System.Drawing.Point(284, 59);
      this.lblStatus.Name = "lblStatus";
      this.lblStatus.Size = new System.Drawing.Size(8, 15);
      this.lblStatus.TabIndex = 18;
      this.lblStatus.TabStop = false;
      this.lblStatus.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblStatus.Texts")));
      // 
      // lblLastName
      // 
      this.lblLastName.FixFromRight = true;
      this.lblLastName.Location = new System.Drawing.Point(284, 34);
      this.lblLastName.Name = "lblLastName";
      this.lblLastName.Size = new System.Drawing.Size(8, 15);
      this.lblLastName.TabIndex = 17;
      this.lblLastName.TabStop = false;
      this.lblLastName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblLastName.Texts")));
      // 
      // lblFirstName
      // 
      this.lblFirstName.FixFromRight = true;
      this.lblFirstName.Location = new System.Drawing.Point(284, 10);
      this.lblFirstName.Name = "lblFirstName";
      this.lblFirstName.Size = new System.Drawing.Size(8, 15);
      this.lblFirstName.TabIndex = 16;
      this.lblFirstName.TabStop = false;
      this.lblFirstName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblFirstName.Texts")));
      // 
      // label5
      // 
      this.label5.Location = new System.Drawing.Point(378, 106);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(59, 15);
      this.label5.TabIndex = 15;
      this.label5.TabStop = false;
      this.label5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label5.Texts")));
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(414, 82);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(23, 15);
      this.label4.TabIndex = 14;
      this.label4.TabStop = false;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(397, 59);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(41, 15);
      this.label3.TabIndex = 13;
      this.label3.TabStop = false;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(371, 34);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(68, 15);
      this.label2.TabIndex = 12;
      this.label2.TabStop = false;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(416, 10);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(21, 15);
      this.label1.TabIndex = 11;
      this.label1.TabStop = false;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // tinypicker1
      // 
      this.tinypicker1.Location = new System.Drawing.Point(11, 565);
      this.tinypicker1.Name = "tinypicker1";
      this.tinypicker1.Size = new System.Drawing.Size(538, 30);
      this.tinypicker1.TabIndex = 43;
      this.tinypicker1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tinypicker1.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Location = new System.Drawing.Point(394, 13);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(40, 15);
      this.transparentLabel1.TabIndex = 44;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // cmbSites
      // 
      this.cmbSites.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbSites.FormattingEnabled = true;
      this.cmbSites.Location = new System.Drawing.Point(440, 10);
      this.cmbSites.Name = "cmbSites";
      this.cmbSites.Size = new System.Drawing.Size(109, 21);
      this.cmbSites.TabIndex = 45;
      this.cmbSites.SelectedIndexChanged += new System.EventHandler(this.cmbSites_SelectedIndexChanged);
      // 
      // btnRankTimeline
      // 
      this.btnRankTimeline.Hint = "رتبه بندی کاربرها در زمان";
      this.btnRankTimeline.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnRankTimeline.Image")));
      this.btnRankTimeline.Location = new System.Drawing.Point(308, 327);
      this.btnRankTimeline.Name = "btnRankTimeline";
      this.btnRankTimeline.NormalBorderColor = System.Drawing.Color.DimGray;
      this.btnRankTimeline.Size = new System.Drawing.Size(64, 64);
      this.btnRankTimeline.TabIndex = 47;
      this.btnRankTimeline.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnRankTimeline.Texts")));
      this.btnRankTimeline.TransparentColor = System.Drawing.Color.White;
      this.btnRankTimeline.Visible = false;
      this.btnRankTimeline.Click += new System.EventHandler(this.btnRankTimeline_Click);
      // 
      // btnRankCompetitions
      // 
      this.btnRankCompetitions.Hint = "رتبه بندی کاربران در مسابقات";
      this.btnRankCompetitions.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnRankCompetitions.Image")));
      this.btnRankCompetitions.Location = new System.Drawing.Point(238, 327);
      this.btnRankCompetitions.Name = "btnRankCompetitions";
      this.btnRankCompetitions.NormalBorderColor = System.Drawing.Color.DimGray;
      this.btnRankCompetitions.Size = new System.Drawing.Size(64, 64);
      this.btnRankCompetitions.TabIndex = 46;
      this.btnRankCompetitions.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnRankCompetitions.Texts")));
      this.btnRankCompetitions.TransparentColor = System.Drawing.Color.White;
      this.btnRankCompetitions.Visible = false;
      this.btnRankCompetitions.Click += new System.EventHandler(this.btnRankCompetitions_Click);
      // 
      // btnReportActivities
      // 
      this.btnReportActivities.Hint = "گزارش فعالیت های کاربر";
      this.btnReportActivities.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnReportActivities.Image")));
      this.btnReportActivities.Location = new System.Drawing.Point(168, 327);
      this.btnReportActivities.Name = "btnReportActivities";
      this.btnReportActivities.NormalBorderColor = System.Drawing.Color.DimGray;
      this.btnReportActivities.Size = new System.Drawing.Size(64, 64);
      this.btnReportActivities.TabIndex = 19;
      this.btnReportActivities.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnReportActivities.Texts")));
      this.btnReportActivities.TransparentColor = System.Drawing.Color.White;
      this.btnReportActivities.Visible = false;
      this.btnReportActivities.Click += new System.EventHandler(this.btnReportActivities_Click);
      // 
      // btnReportFinancial
      // 
      this.btnReportFinancial.Hint = "گزارش مالی کاربر";
      this.btnReportFinancial.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnReportFinancial.Image")));
      this.btnReportFinancial.Location = new System.Drawing.Point(98, 327);
      this.btnReportFinancial.Name = "btnReportFinancial";
      this.btnReportFinancial.NormalBorderColor = System.Drawing.Color.DimGray;
      this.btnReportFinancial.Size = new System.Drawing.Size(64, 64);
      this.btnReportFinancial.TabIndex = 16;
      this.btnReportFinancial.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnReportFinancial.Texts")));
      this.btnReportFinancial.TransparentColor = System.Drawing.Color.White;
      this.btnReportFinancial.Visible = false;
      this.btnReportFinancial.Click += new System.EventHandler(this.btnUserReportFinancial_Click);
      // 
      // lvUsers
      // 
      this.lvUsers.AllColumns.Add(this.olvColumn1);
      this.lvUsers.AllColumns.Add(this.olvColumn2);
      this.lvUsers.AllColumns.Add(this.olvColumn3);
      this.lvUsers.AllColumns.Add(this.olvColumn4);
      this.lvUsers.AllColumns.Add(this.olvColumn5);
      this.lvUsers.AllowDrop = true;
      this.lvUsers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this.olvColumn3,
            this.olvColumn4,
            this.olvColumn5});
      this.lvUsers.ContextMenuStrip = this.mnuRightClick;
      this.lvUsers.EmptyListMsgFont = new System.Drawing.Font("B Nazanin", 12F);
      this.lvUsers.ForeColor = System.Drawing.Color.Black;
      this.lvUsers.FullRowSelect = true;
      this.lvUsers.GridLines = true;
      this.lvUsers.IsSimpleDragSource = true;
      this.lvUsers.Location = new System.Drawing.Point(98, 37);
      this.lvUsers.Name = "lvUsers";
      this.lvUsers.RightToLeftLayout = true;
      this.lvUsers.ShowGroups = false;
      this.lvUsers.Size = new System.Drawing.Size(451, 354);
      this.lvUsers.TabIndex = 48;
      this.lvUsers.UseCompatibleStateImageBehavior = false;
      this.lvUsers.View = System.Windows.Forms.View.Details;
      this.lvUsers.FormatRow += new System.EventHandler<BrightIdeasSoftware.FormatRowEventArgs>(this.lvUsers_FormatRow);
      this.lvUsers.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvUsers_ItemDrag);
      this.lvUsers.SelectedIndexChanged += new System.EventHandler(this.lvUsers_SelectedIndexChanged);
      this.lvUsers.DoubleClick += new System.EventHandler(this.lvUsers_DoubleClick);
      // 
      // olvColumn1
      // 
      this.olvColumn1.AspectName = "ID";
      this.olvColumn1.CellPadding = null;
      this.olvColumn1.Text = "ردیف";
      this.olvColumn1.Width = 38;
      // 
      // olvColumn2
      // 
      this.olvColumn2.AspectName = "FirstName";
      this.olvColumn2.CellPadding = null;
      this.olvColumn2.Text = "نام";
      this.olvColumn2.Width = 75;
      // 
      // olvColumn3
      // 
      this.olvColumn3.AspectName = "LastName";
      this.olvColumn3.CellPadding = null;
      this.olvColumn3.Text = "نام خانوادگی";
      this.olvColumn3.Width = 135;
      // 
      // olvColumn4
      // 
      this.olvColumn4.AspectName = "RegistrationDate";
      this.olvColumn4.AspectToStringFormat = "";
      this.olvColumn4.CellPadding = null;
      this.olvColumn4.Text = "تاریخ عضویت";
      this.olvColumn4.Width = 88;
      // 
      // olvColumn5
      // 
      this.olvColumn5.AspectName = "BirthDate";
      this.olvColumn5.CellPadding = null;
      this.olvColumn5.Text = "تاریخ تولد";
      this.olvColumn5.Width = 86;
      // 
      // mnuWhiteListTwoWeeks
      // 
      this.mnuWhiteListTwoWeeks.Name = "mnuWhiteListTwoWeeks";
      this.mnuWhiteListTwoWeeks.Size = new System.Drawing.Size(160, 22);
      this.mnuWhiteListTwoWeeks.Text = "برای 2 هفته";
      this.mnuWhiteListTwoWeeks.Click += new System.EventHandler(this.mnuWhiteListOneWeek_Click);
      // 
      // ControlPanel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.White;
      this.ClientSize = new System.Drawing.Size(566, 607);
      this.Controls.Add(this.btnRankCompetitions);
      this.Controls.Add(this.btnRankTimeline);
      this.Controls.Add(this.cmbSites);
      this.Controls.Add(this.transparentLabel1);
      this.Controls.Add(this.tbButtons);
      this.Controls.Add(this.txtSearch);
      this.Controls.Add(this.btnReportActivities);
      this.Controls.Add(this.label11);
      this.Controls.Add(this.btnReportFinancial);
      this.Controls.Add(this.tinypicker1);
      this.Controls.Add(this.pnlUserInfo);
      this.Controls.Add(this.lvUsers);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "ControlPanel";
      this.Text = "مدیریت کاربرها";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ControlPanelUser_FormClosing);
      this.Load += new System.EventHandler(this.ControlPanel_Load);
      this.mnuRightClick.ResumeLayout(false);
      this.tbButtons.ResumeLayout(false);
      this.pnlUserInfo.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.lvUsers)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private SpecialForce.TransparentLabel label11;
    private System.Windows.Forms.TextBox txtSearch;
    private ToolBar tbButtons;
    private ToolBar pnlUserInfo;
    private TransparentLabel lblCredit;
    private TransparentLabel lblTeamName;
    private TransparentLabel lblStatus;
    private TransparentLabel lblLastName;
    private TransparentLabel lblFirstName;
    private TransparentLabel label5;
    private TransparentLabel label4;
    private TransparentLabel label3;
    private TransparentLabel label2;
    private TransparentLabel label1;
    private TinyPicker tinypicker1;
    private TransparentLabel lblRegistrationSite;
    private TransparentLabel transparentLabel2;
    private TransparentLabel transparentLabel1;
    private System.Windows.Forms.ComboBox cmbSites;
    private System.Windows.Forms.ContextMenuStrip mnuRightClick;
    private System.Windows.Forms.ToolStripMenuItem mnuChargeAccount;
    private System.Windows.Forms.ToolStripMenuItem mnuRefundAccount;
    private System.Windows.Forms.ToolStripMenuItem mnuBlackList;
    private System.Windows.Forms.ToolStripMenuItem mnuEditUser;
    private System.Windows.Forms.ToolStripMenuItem mnuBlackListOneWeek;
    private System.Windows.Forms.ToolStripMenuItem mnuBlackListOneMonth;
    private System.Windows.Forms.ToolStripMenuItem mnuBlackListThreeMonths;
    private System.Windows.Forms.ToolStripMenuItem mnuBlackListOneYear;
    private System.Windows.Forms.ToolStripMenuItem mnuBlackListForEver;
    private ToolButton btnRefund;
    private ToolButton btnReportFinancial;
    private ToolButton btnCharge;
    private ToolButton btnAdd;
    private ToolButton toolButton6;
    private ToolButton pbUserPhoto;
    private System.Windows.Forms.ToolStripMenuItem mnuWhiteList;
    private System.Windows.Forms.ToolStripMenuItem mnuWhiteListOneWeek;
    private System.Windows.Forms.ToolStripMenuItem mnuWhiteListOneMonth;
    private System.Windows.Forms.ToolStripMenuItem mnuWhiteListOneYear;
    private System.Windows.Forms.ToolStripMenuItem mnuWhiteListForever;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
    private System.Windows.Forms.ToolStripMenuItem mnuRemoveBlackList;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem mnuRemoveWhiteList;
    private System.Windows.Forms.ToolStripMenuItem mnuFire;
    private ToolButton btnReportActivities;
    private System.Windows.Forms.ToolStripMenuItem mnuFinancialReport;
    private System.Windows.Forms.ToolStripMenuItem mnuActivityReport;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
    private ToolButton btnEdit;
    private ToolButton btnUserRankings;
    private ToolButton btnUserReport;
    private ToolButton btnRankTimeline;
    private ToolButton btnRankCompetitions;
    private System.Windows.Forms.ToolStripMenuItem mnuReportLogins;
    private System.Windows.Forms.ToolStripMenuItem mnuReportLoginsUser;
    private System.Windows.Forms.ToolStripMenuItem mnuReportLoginsAll;
    private UserInterface.ListView lvUsers;
    private BrightIdeasSoftware.OLVColumn olvColumn2;
    private BrightIdeasSoftware.OLVColumn olvColumn3;
    private BrightIdeasSoftware.OLVColumn olvColumn4;
    private BrightIdeasSoftware.OLVColumn olvColumn5;
    private BrightIdeasSoftware.OLVColumn olvColumn1;
    private System.Windows.Forms.ToolStripMenuItem mnuBlackListTwoWeeks;
    private System.Windows.Forms.ToolStripMenuItem mnuWhiteListTwoWeeks;
  }
}