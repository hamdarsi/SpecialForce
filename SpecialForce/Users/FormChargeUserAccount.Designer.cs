﻿namespace SpecialForce.Users
{
  partial class FormChargeUserAccount
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormChargeUserAccount));
      this.UserCharges = new System.Windows.Forms.ListView();
      this.columnDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.columnCredit = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.columnType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.label5 = new SpecialForce.TransparentLabel();
      this.btnOK = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.pnlUserInfo = new SpecialForce.ToolBar();
      this.txtCredit = new SpecialForce.Utilities.NumberBox();
      this.label9 = new SpecialForce.TransparentLabel();
      this.label7 = new SpecialForce.TransparentLabel();
      this.lblCurrentCredit = new SpecialForce.TransparentLabel();
      this.lblMembershipDate = new SpecialForce.TransparentLabel();
      this.lblLastName = new SpecialForce.TransparentLabel();
      this.lblFirstName = new SpecialForce.TransparentLabel();
      this.pbUserPhoto = new System.Windows.Forms.PictureBox();
      this.label4 = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.label2 = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.pnlUserInfo.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbUserPhoto)).BeginInit();
      this.SuspendLayout();
      // 
      // UserCharges
      // 
      this.UserCharges.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnDate,
            this.columnCredit,
            this.columnType});
      this.UserCharges.FullRowSelect = true;
      this.UserCharges.GridLines = true;
      this.UserCharges.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
      this.UserCharges.Location = new System.Drawing.Point(12, 190);
      this.UserCharges.Name = "UserCharges";
      this.UserCharges.RightToLeftLayout = true;
      this.UserCharges.Size = new System.Drawing.Size(293, 132);
      this.UserCharges.TabIndex = 1;
      this.UserCharges.TabStop = false;
      this.UserCharges.UseCompatibleStateImageBehavior = false;
      this.UserCharges.View = System.Windows.Forms.View.Details;
      // 
      // columnDate
      // 
      this.columnDate.Text = "تاریخ";
      this.columnDate.Width = 100;
      // 
      // columnCredit
      // 
      this.columnCredit.Text = "مبلغ";
      this.columnCredit.Width = 107;
      // 
      // columnType
      // 
      this.columnType.Text = "نوع";
      // 
      // label5
      // 
      this.label5.Location = new System.Drawing.Point(9, 167);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(110, 15);
      this.label5.TabIndex = 2;
      this.label5.TabStop = false;
      this.label5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label5.Texts")));
      // 
      // btnOK
      // 
      this.btnOK.ForeColor = System.Drawing.Color.Black;
      this.btnOK.Location = new System.Drawing.Point(230, 334);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 4;
      this.btnOK.Text = "تایید";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(149, 334);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 3;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // pnlUserInfo
      // 
      this.pnlUserInfo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlUserInfo.BorderWidth = 0.5F;
      this.pnlUserInfo.Controls.Add(this.txtCredit);
      this.pnlUserInfo.Controls.Add(this.label9);
      this.pnlUserInfo.Controls.Add(this.label7);
      this.pnlUserInfo.Controls.Add(this.lblCurrentCredit);
      this.pnlUserInfo.Controls.Add(this.lblMembershipDate);
      this.pnlUserInfo.Controls.Add(this.lblLastName);
      this.pnlUserInfo.Controls.Add(this.lblFirstName);
      this.pnlUserInfo.Controls.Add(this.pbUserPhoto);
      this.pnlUserInfo.Controls.Add(this.label4);
      this.pnlUserInfo.Controls.Add(this.label3);
      this.pnlUserInfo.Controls.Add(this.label2);
      this.pnlUserInfo.Controls.Add(this.label1);
      this.pnlUserInfo.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlUserInfo.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlUserInfo.Location = new System.Drawing.Point(12, 12);
      this.pnlUserInfo.Name = "pnlUserInfo";
      this.pnlUserInfo.Size = new System.Drawing.Size(290, 138);
      this.pnlUserInfo.TabIndex = 1;
      // 
      // txtCredit
      // 
      this.txtCredit.Location = new System.Drawing.Point(139, 105);
      this.txtCredit.Mode = SpecialForce.Utilities.NumberBox.NumberMode.Currency;
      this.txtCredit.Name = "txtCredit";
      this.txtCredit.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtCredit.Size = new System.Drawing.Size(54, 21);
      this.txtCredit.TabIndex = 2;
      this.txtCredit.Text = "$0.00";
      this.txtCredit.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.btnOK_Click);
      // 
      // label9
      // 
      this.label9.Location = new System.Drawing.Point(105, 108);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(28, 15);
      this.label9.TabIndex = 29;
      this.label9.TabStop = false;
      this.label9.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label9.Texts")));
      // 
      // label7
      // 
      this.label7.Location = new System.Drawing.Point(199, 108);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(83, 15);
      this.label7.TabIndex = 28;
      this.label7.TabStop = false;
      this.label7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label7.Texts")));
      // 
      // lblCurrentCredit
      // 
      this.lblCurrentCredit.FixFromRight = true;
      this.lblCurrentCredit.Location = new System.Drawing.Point(186, 82);
      this.lblCurrentCredit.Name = "lblCurrentCredit";
      this.lblCurrentCredit.Size = new System.Drawing.Size(8, 15);
      this.lblCurrentCredit.TabIndex = 26;
      this.lblCurrentCredit.TabStop = false;
      this.lblCurrentCredit.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblCurrentCredit.Texts")));
      // 
      // lblMembershipDate
      // 
      this.lblMembershipDate.FixFromRight = true;
      this.lblMembershipDate.Location = new System.Drawing.Point(186, 59);
      this.lblMembershipDate.Name = "lblMembershipDate";
      this.lblMembershipDate.Size = new System.Drawing.Size(8, 15);
      this.lblMembershipDate.TabIndex = 25;
      this.lblMembershipDate.TabStop = false;
      this.lblMembershipDate.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblMembershipDate.Texts")));
      // 
      // lblLastName
      // 
      this.lblLastName.FixFromRight = true;
      this.lblLastName.Location = new System.Drawing.Point(186, 36);
      this.lblLastName.Name = "lblLastName";
      this.lblLastName.Size = new System.Drawing.Size(8, 15);
      this.lblLastName.TabIndex = 24;
      this.lblLastName.TabStop = false;
      this.lblLastName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblLastName.Texts")));
      // 
      // lblFirstName
      // 
      this.lblFirstName.FixFromRight = true;
      this.lblFirstName.Location = new System.Drawing.Point(186, 13);
      this.lblFirstName.Name = "lblFirstName";
      this.lblFirstName.Size = new System.Drawing.Size(8, 15);
      this.lblFirstName.TabIndex = 23;
      this.lblFirstName.TabStop = false;
      this.lblFirstName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblFirstName.Texts")));
      // 
      // pbUserPhoto
      // 
      this.pbUserPhoto.Location = new System.Drawing.Point(10, 11);
      this.pbUserPhoto.Name = "pbUserPhoto";
      this.pbUserPhoto.Size = new System.Drawing.Size(87, 116);
      this.pbUserPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pbUserPhoto.TabIndex = 22;
      this.pbUserPhoto.TabStop = false;
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(218, 84);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(61, 15);
      this.label4.TabIndex = 21;
      this.label4.TabStop = false;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(214, 60);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(67, 15);
      this.label3.TabIndex = 20;
      this.label3.TabStop = false;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(214, 36);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(68, 15);
      this.label2.TabIndex = 18;
      this.label2.TabStop = false;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(259, 12);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(21, 15);
      this.label1.TabIndex = 17;
      this.label1.TabStop = false;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // FormChargeUserAccount
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(317, 369);
      this.Controls.Add(this.pnlUserInfo);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOK);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.UserCharges);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormChargeUserAccount";
      this.Text = "شارژ حساب";
      this.Load += new System.EventHandler(this.FormChargeUserAccount_Load);
      this.pnlUserInfo.ResumeLayout(false);
      this.pnlUserInfo.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbUserPhoto)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ListView UserCharges;
    private System.Windows.Forms.ColumnHeader columnDate;
    private System.Windows.Forms.ColumnHeader columnCredit;
    private SpecialForce.TransparentLabel label5;
    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.Button btnCancel;
    private ToolBar pnlUserInfo;
    private TransparentLabel label9;
    private TransparentLabel label7;
    private TransparentLabel lblCurrentCredit;
    private TransparentLabel lblMembershipDate;
    private TransparentLabel lblLastName;
    private TransparentLabel lblFirstName;
    private System.Windows.Forms.PictureBox pbUserPhoto;
    private TransparentLabel label4;
    private TransparentLabel label3;
    private TransparentLabel label2;
    private TransparentLabel label1;
    private Utilities.NumberBox txtCredit;
    private System.Windows.Forms.ColumnHeader columnType;
  }
}