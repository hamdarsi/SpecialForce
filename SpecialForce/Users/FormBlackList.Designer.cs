﻿namespace SpecialForce.Users
{
  partial class FormBlackList
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBlackList));
      this.pnlUserInfo = new SpecialForce.ToolBar();
      this.pbUserPhoto = new SpecialForce.ToolButton();
      this.lblRegistrationSite = new SpecialForce.TransparentLabel();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.lblCredit = new SpecialForce.TransparentLabel();
      this.lblMembershipDate = new SpecialForce.TransparentLabel();
      this.lblBirthDate = new SpecialForce.TransparentLabel();
      this.lblLastName = new SpecialForce.TransparentLabel();
      this.lblFirstName = new SpecialForce.TransparentLabel();
      this.label5 = new SpecialForce.TransparentLabel();
      this.label4 = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.label2 = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.txtReason = new System.Windows.Forms.TextBox();
      this.transparentLabel13 = new SpecialForce.TransparentLabel();
      this.btnOK = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.pnlUserInfo.SuspendLayout();
      this.toolBar1.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlUserInfo
      // 
      this.pnlUserInfo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlUserInfo.BorderWidth = 0.5F;
      this.pnlUserInfo.Controls.Add(this.pbUserPhoto);
      this.pnlUserInfo.Controls.Add(this.lblRegistrationSite);
      this.pnlUserInfo.Controls.Add(this.transparentLabel2);
      this.pnlUserInfo.Controls.Add(this.lblCredit);
      this.pnlUserInfo.Controls.Add(this.lblMembershipDate);
      this.pnlUserInfo.Controls.Add(this.lblBirthDate);
      this.pnlUserInfo.Controls.Add(this.lblLastName);
      this.pnlUserInfo.Controls.Add(this.lblFirstName);
      this.pnlUserInfo.Controls.Add(this.label5);
      this.pnlUserInfo.Controls.Add(this.label4);
      this.pnlUserInfo.Controls.Add(this.label3);
      this.pnlUserInfo.Controls.Add(this.label2);
      this.pnlUserInfo.Controls.Add(this.label1);
      this.pnlUserInfo.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlUserInfo.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlUserInfo.Location = new System.Drawing.Point(12, 12);
      this.pnlUserInfo.Name = "pnlUserInfo";
      this.pnlUserInfo.Size = new System.Drawing.Size(467, 158);
      this.pnlUserInfo.TabIndex = 43;
      // 
      // pbUserPhoto
      // 
      this.pbUserPhoto.Hint = "";
      this.pbUserPhoto.Image = ((System.Drawing.Bitmap)(resources.GetObject("pbUserPhoto.Image")));
      this.pbUserPhoto.Location = new System.Drawing.Point(12, 9);
      this.pbUserPhoto.Name = "pbUserPhoto";
      this.pbUserPhoto.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pbUserPhoto.Size = new System.Drawing.Size(105, 140);
      this.pbUserPhoto.TabIndex = 24;
      this.pbUserPhoto.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("pbUserPhoto.Texts")));
      this.pbUserPhoto.TransparentColor = System.Drawing.Color.White;
      // 
      // lblRegistrationSite
      // 
      this.lblRegistrationSite.FixFromRight = true;
      this.lblRegistrationSite.Location = new System.Drawing.Point(277, 129);
      this.lblRegistrationSite.Name = "lblRegistrationSite";
      this.lblRegistrationSite.Size = new System.Drawing.Size(8, 15);
      this.lblRegistrationSite.TabIndex = 23;
      this.lblRegistrationSite.TabStop = false;
      this.lblRegistrationSite.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblRegistrationSite.Texts")));
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Location = new System.Drawing.Point(365, 129);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(88, 15);
      this.transparentLabel2.TabIndex = 22;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // lblCredit
      // 
      this.lblCredit.FixFromRight = true;
      this.lblCredit.Location = new System.Drawing.Point(277, 106);
      this.lblCredit.Name = "lblCredit";
      this.lblCredit.Size = new System.Drawing.Size(8, 15);
      this.lblCredit.TabIndex = 20;
      this.lblCredit.TabStop = false;
      this.lblCredit.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblCredit.Texts")));
      // 
      // lblMembershipDate
      // 
      this.lblMembershipDate.FixFromRight = true;
      this.lblMembershipDate.Location = new System.Drawing.Point(277, 82);
      this.lblMembershipDate.Name = "lblMembershipDate";
      this.lblMembershipDate.Size = new System.Drawing.Size(8, 15);
      this.lblMembershipDate.TabIndex = 19;
      this.lblMembershipDate.TabStop = false;
      this.lblMembershipDate.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblMembershipDate.Texts")));
      // 
      // lblBirthDate
      // 
      this.lblBirthDate.FixFromRight = true;
      this.lblBirthDate.Location = new System.Drawing.Point(277, 59);
      this.lblBirthDate.Name = "lblBirthDate";
      this.lblBirthDate.Size = new System.Drawing.Size(8, 15);
      this.lblBirthDate.TabIndex = 18;
      this.lblBirthDate.TabStop = false;
      this.lblBirthDate.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBirthDate.Texts")));
      // 
      // lblLastName
      // 
      this.lblLastName.FixFromRight = true;
      this.lblLastName.Location = new System.Drawing.Point(277, 34);
      this.lblLastName.Name = "lblLastName";
      this.lblLastName.Size = new System.Drawing.Size(8, 15);
      this.lblLastName.TabIndex = 17;
      this.lblLastName.TabStop = false;
      this.lblLastName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblLastName.Texts")));
      // 
      // lblFirstName
      // 
      this.lblFirstName.FixFromRight = true;
      this.lblFirstName.Location = new System.Drawing.Point(277, 10);
      this.lblFirstName.Name = "lblFirstName";
      this.lblFirstName.Size = new System.Drawing.Size(8, 15);
      this.lblFirstName.TabIndex = 16;
      this.lblFirstName.TabStop = false;
      this.lblFirstName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblFirstName.Texts")));
      // 
      // label5
      // 
      this.label5.Location = new System.Drawing.Point(394, 106);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(59, 15);
      this.label5.TabIndex = 15;
      this.label5.TabStop = false;
      this.label5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label5.Texts")));
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(387, 82);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(67, 15);
      this.label4.TabIndex = 14;
      this.label4.TabStop = false;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(404, 58);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(50, 15);
      this.label3.TabIndex = 13;
      this.label3.TabStop = false;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(387, 34);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(68, 15);
      this.label2.TabIndex = 12;
      this.label2.TabStop = false;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(432, 10);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(21, 15);
      this.label1.TabIndex = 11;
      this.label1.TabStop = false;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 0.5F;
      this.toolBar1.Controls.Add(this.txtReason);
      this.toolBar1.Controls.Add(this.transparentLabel13);
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(12, 176);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(467, 151);
      this.toolBar1.TabIndex = 1;
      // 
      // txtReason
      // 
      this.txtReason.Location = new System.Drawing.Point(12, 45);
      this.txtReason.Multiline = true;
      this.txtReason.Name = "txtReason";
      this.txtReason.Size = new System.Drawing.Size(441, 91);
      this.txtReason.TabIndex = 2;
      this.txtReason.TextChanged += new System.EventHandler(this.txtReason_TextChanged);
      // 
      // transparentLabel13
      // 
      this.transparentLabel13.Location = new System.Drawing.Point(206, 14);
      this.transparentLabel13.Name = "transparentLabel13";
      this.transparentLabel13.Size = new System.Drawing.Size(259, 15);
      this.transparentLabel13.TabIndex = 11;
      this.transparentLabel13.TabStop = false;
      this.transparentLabel13.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel13.Texts")));
      // 
      // btnOK
      // 
      this.btnOK.Enabled = false;
      this.btnOK.ForeColor = System.Drawing.Color.Black;
      this.btnOK.Location = new System.Drawing.Point(404, 339);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 4;
      this.btnOK.Text = "تایید";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(323, 340);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 3;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // FormBlackList
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(492, 375);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOK);
      this.Controls.Add(this.toolBar1);
      this.Controls.Add(this.pnlUserInfo);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormBlackList";
      this.Text = "لیست سیاه";
      this.Load += new System.EventHandler(this.FormBlackList_Load);
      this.pnlUserInfo.ResumeLayout(false);
      this.toolBar1.ResumeLayout(false);
      this.toolBar1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private ToolBar pnlUserInfo;
    private ToolButton pbUserPhoto;
    private TransparentLabel lblRegistrationSite;
    private TransparentLabel transparentLabel2;
    private TransparentLabel lblCredit;
    private TransparentLabel lblMembershipDate;
    private TransparentLabel lblBirthDate;
    private TransparentLabel lblLastName;
    private TransparentLabel lblFirstName;
    private TransparentLabel label5;
    private TransparentLabel label4;
    private TransparentLabel label3;
    private TransparentLabel label2;
    private TransparentLabel label1;
    private ToolBar toolBar1;
    private System.Windows.Forms.TextBox txtReason;
    private TransparentLabel transparentLabel13;
    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.Button btnCancel;
  }
}