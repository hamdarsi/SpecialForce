﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace SpecialForce.Users
{
  public partial class FormUserReportLogins : SessionAwareForm
  {
    private DB.User mUser;
    public DB.User User
    {
      get { return mUser; }
      set { mUser = value; }
    }

    private PersianDateTime mStart;
    public PersianDateTime Start
    {
      get { return mStart; }
      set { mStart = value; }
    }

    private PersianDateTime mFinish;
    public PersianDateTime Finish
    {
      get { return mFinish; }
      set { mFinish = value; }
    }

    private List<DB.Site> mSites;
    public List<DB.Site> Sites
    {
      get { return mSites; }
      set { mSites = value; }
    }

    public FormUserReportLogins()
    {
      InitializeComponent();
    }

    private void FormUserReportLogins_Shown(object sender, EventArgs e)
    {
      report1.BeginUpdate();
      if (mUser != null)
        report1.Title = "گزارش ورود و خروج برای " + mUser.FullName;
      else
        report1.Title = "گزارش ورودها و خروج ها";

      Reports.Table table = report1.AddTable();
      table.AutoIndexColumn = true;
      table.AddNewColumn(Types.String, "کاربر", 150);
      table.AddNewColumn(Types.DateTime, "ورود", 150).DateTimeMode = DateTimeString.CompactDateTime;
      table.AddNewColumn(Types.String, "خروج", 150);
      table.AddNewColumn(Types.String, "باشگاه", 120);

      System.Collections.Hashtable site_names = new System.Collections.Hashtable();
      foreach (DB.Site site in DB.Site.GetAllSites(true))
        site_names.Add(site.ID, site.FullName);

      foreach(DB.Entrance ent in DB.Entrance.GetAllEntrances(mStart, mFinish, mUser, mSites))
      {
        Reports.Row row = table.AddNewRow();
        row[0] = ent.UserFullName;
        row[1] = ent.Enter;
        row[2] = ent.Exit == null ? "-" : ent.Exit.ToString(DateTimeString.CompactDateTime);
        row[3] = site_names[ent.SiteID] as string;
      }

      // finished
      report1.EndUpdate();
    }
  }
}
