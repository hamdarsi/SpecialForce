﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Users
{
  public partial class FormFire : SessionAwareForm
  {
    private DB.User mUser = null;
    public DB.User User
    {
      get { return mUser; }
      set { mUser = value; }
    }

    public FormFire()
    {
      InitializeComponent();
    }

    private void txtReason_TextChanged(object sender, EventArgs e)
    {
      btnFire.Enabled = txtReason.Text.Length != 0;
    }

    private void btnFire_Click(object sender, EventArgs e)
    {
      if(mUser.Fire(txtReason.Text))
        Session.ShowMessage(mUser.FullName + " اخراج شد");

      DialogResult = System.Windows.Forms.DialogResult.OK;
    }

    private void FormFire_Load(object sender, EventArgs e)
    {
      lblFirstName.Text = mUser.FirstName;
      lblLastName.Text = mUser.LastName;
      lblBirthDate.Text = mUser.BirthDate.ToString(DateTimeString.CompactDate);
      lblMembershipDate.Text = mUser.RegistrationDate.ToString(DateTimeString.CompactDate);
      lblRegistrationSite.Text = DB.Site.GetSiteName(mUser.RegistrationSiteID);
      lblCredit.Text = mUser.Credit.ToCurrencyString() + " تومان";
      pbUserPhoto.Image = mUser.Photo.Resize(pbUserPhoto.Size);
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
