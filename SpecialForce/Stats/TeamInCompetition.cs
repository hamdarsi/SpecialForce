﻿using System;
using System.Collections.Generic;

namespace SpecialForce.Stats
{
  public class TeamInCompetition
  {
    private DB.Team mTeam = null;
    public DB.Team TeamInstance
    {
      get { return mTeam; }
    }

    public int TeamID
    {
      get { return mTeam.ID; }
    }

    private DB.Competition mCompetition = null;
    public DB.Competition CompetitionInstance
    {
      get { return mCompetition; }
    }

    public int CompetitionID
    {
      get { return mCompetition.ID; }
    }

    public string CompetitionName
    {
      get { return mCompetition.Title; }
    }

    public string CompetitionType
    {
      get { return Enums.ToString(mCompetition.Type); }
    }

    public string Start
    {
      get 
      {
        return mCompetition.Start != null ?
          mCompetition.Start.ToString(DateTimeString.CompactDate) :
          "نا مشخص";
      }
    }

    public string End
    {
      get
      {
        return mCompetition.End != null ?
          mCompetition.End.ToString(DateTimeString.CompactDate) :
          "نا مشخص";
      }
    }

    private int mRank = -1;
    public string Rank
    {
      get 
      {
        bool difinite = mCompetition.AllGamesDone;

        if (mRank == -1)
          return "-";

        if(difinite)
          return mRank.ToString();

        return mRank.ToString() + "  (تا این لحظه)";
      }
    }

    public TeamInCompetition(DB.Team team, DB.Competition comp)
    {
      mTeam = team;
      mCompetition = comp;

      // calculate teams rank in competition
      Statistics<Stats.Team> stats = Stats.Generator.Generate(comp, false);
      foreach (Stats.Team st in stats.Data)
        if (st.Instance.ID == team.ID)
          mRank = st.Rank;
    }
  }
}
