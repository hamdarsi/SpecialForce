﻿using System;
using System.Collections.Generic;

namespace SpecialForce.Stats
{
  public class Game
  {
    private DB.Game mGame = null;
    public int ID
    {
      get { return mGame.ID; }
    }

    public DB.Game Instance
    {
      get { return mGame; }
    }

    public PersianDateTime Start
    {
      get { return mGame.Start; }
    }

    public PersianDateTime End
    {
      get { return mGame.Finish; }
    }

    public string Type
    {
      get { return Enums.ToString(mGame.Type); }
    }

    public string Object
    {
      get { return Enums.ToString(mGame.Object); }
    }

    public string BlueTeamName
    {
      get { return mGame.GetBlueTeamName("-"); }
    }

    public string GreenTeamName
    {
      get { return mGame.GetGreenTeamName("-"); }
    }

    private string Result
    {
      get { return mGame.StateString; }
    }

    public string State
    {
      get
      {
        if (mGame.State == GameState.Reserved)
          return "رزرو";

        return mGame.StateString;
      }
    }

    public string ShortState
    {
      get 
      {
        string result = "";
        if (mGame.State == GameState.Free)
          return "آزاد";

        if (mGame.State == GameState.Aborted)
          return "کنسل";

        if (mGame.State == GameState.Reserved)
          return "رزرو";

        // For in progress and finished games:
        int blue, green;
        mGame.GetTeamScores(out blue, out green);

        if (blue == green)
          return blue.ToString() + " - " + green.ToString() + "مساوی";

        result = Math.Max(blue, green).ToString();
        result += " - ";
        result += Math.Min(blue, green).ToString();

        result += " برای ";
        result += blue > green ? mGame.GetBlueTeamName("آبی") : mGame.GetGreenTeamName("سبز");
        return result;
      }
    }

    public Game(DB.Game game)
    {
      mGame = game;
    }
  }
}
