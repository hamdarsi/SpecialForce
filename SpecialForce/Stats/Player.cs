﻿using System;
using System.Collections.Generic;

namespace SpecialForce.Stats
{
  public class Player
  {
    private DB.Round mRound = null;
    private DB.User mPlayer = null;

    public int ID
    {
      get { return mPlayer.ID; }
    }

    public DB.User Instance
    {
      get { return mPlayer; }
    }

    public string KevlarNumber
    {
      get 
      {
        if (!mRound.PlayerKevlars.ContainsKey(mPlayer.ID))
          return "غایب";

        int num = mRound.PlayerKevlars[mPlayer.ID];
        return num <= 10 ? num.ToString() + " آبی" : (num - 10).ToString() + " سبز";
      }
    }

    public string FullName
    {
      get { return mPlayer.FullName; }
    }

    public string TeamName
    {
      get
      {
        DB.Game game = mRound.Game;
        bool is_blue = mRound.Game.BlueUserIDs.Contains(mPlayer.ID);
        return is_blue ? game.GetBlueTeamName("آبی") : game.GetGreenTeamName("سبز");
      }
    }

    public string State
    {
      get 
      {
        if (!mRound.PlayerKevlars.ContainsKey(mPlayer.ID))
          return "غایب";

        List<DB.KillInfo> kills = mRound.Kills;
        foreach(DB.KillInfo kill in kills)
          if (kill.KilledID == mPlayer.ID)
            return "کشته شده توسط " +
                   DB.User.GetUserFullName(kill.KillerID) + " " +
                   DB.Round.TimeToString(kill.TimeInRound);

        return "زنده";
      }
    }

    public int KillCount
    {
      get { return mRound.GetKillCount(mPlayer); }
    }

    public int FriendlyKillCount
    {
      get { return mRound.GetFriendlyKillCount(mPlayer); }
    }

    public Player(DB.Round round, DB.User user)
    {
      mRound = round;
      mPlayer = user;
    }
  }
}
