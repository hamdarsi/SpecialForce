﻿using System;
using System.Collections.Generic;

namespace SpecialForce.Stats
{
  public class TeamInGame
  {
    private DB.Team mTeam = null;
    public DB.Team TeamInstance
    {
      get { return mTeam; }
    }

    public int TeamID
    {
      get { return mTeam.ID; }
    }

    private DB.Game mGame = null;
    public DB.Game GameInstance
    {
      get { return mGame; }
    }

    public int GameID
    {
      get { return mGame.ID; }
    }

    public string Start
    {
      get 
      { 
        return 
          mGame.Start != null ? 
          mGame.Start.ToString(DateTimeString.CompactDateTime) 
          : "نا مشخص"; 
      }
    }

    public string End
    {
      get 
      { 
        return 
          mGame.Finish != null ? 
          mGame.Finish.ToString(DateTimeString.CompactDateTime) 
          : "نا مشخص"; 
      }
    }

    public string Type
    {
      get { return Enums.ToString(mGame.Type); }
    }

    public string Object
    {
      get { return Enums.ToString(mGame.Object); }
    }

    public string Opponent
    {
      get
      {
        if (mTeam.ID == mGame.BlueTeamID)
          return mGame.GetGreenTeamName();
        else
          return mGame.GetBlueTeamName();
      }
    }

    public string Result
    {
      get
      {
        if (mGame.State == GameState.Reserved)
          return "رزرو";

        return mGame.StateString; 
      }
    }

    public int KillCount
    {
      get { return mGame.GetKillCount(mTeam.ID == mGame.BlueTeamID ? TeamType.Blue : TeamType.Green); }
    }

    public int FriendlyKillCount
    {
      get { return mGame.GetFriendlyKillCount(mTeam.ID == mGame.BlueTeamID ? TeamType.Blue : TeamType.Green); }
    }

    public int DeathCount
    {
      get { return mGame.GetDeathCount(mTeam.ID == mGame.BlueTeamID ? TeamType.Blue : TeamType.Green); }
    }

    public int PlantCount
    {
      get { return mGame.GetPlantCount(mTeam.ID == mGame.BlueTeamID ? TeamType.Blue : TeamType.Green); }
    }

    public int DiffuseCount
    {
      get { return mGame.GetDiffuseCount(mTeam.ID == mGame.BlueTeamID ? TeamType.Blue : TeamType.Green); }
    }

    public TeamInGame(DB.Team team, DB.Game game)
    {
      mTeam = team;
      mGame = game;
    }
  }
}
