﻿using System;
using System.Collections.Generic;

namespace SpecialForce.Stats
{
  public class Round
  {
    private DB.Round mRound = null;
    public int ID
    {
      get { return mRound.ID; }
    }

    public DB.Round Instance
    {
      get { return mRound; }
    }

    public int Number
    {
      get { return mRound.Number; }
    }

    public PersianDateTime Start
    {
      get { return mRound.Start; }
    }

    public PersianDateTime End
    {
      get { return mRound.Finish; }
    }

    public string Terrorists
    {
      get
      {
        DB.Game g = mRound.Game;

        if(mRound.TerroristsAreBlue)
          return g.GetBlueTeamName("آبی");
        else
          return g.GetGreenTeamName("سبز");
      }
    }

    public string CounterTerrorists
    {
      get
      {
        DB.Game g = mRound.Game;

        if (mRound.TerroristsAreBlue)
          return g.GetGreenTeamName("سبز");
        else
          return g.GetBlueTeamName("آبی");
      }
    }

    public string RoundState
    {
      get { return mRound.StateString; }
    }

    public string BombState
    {
      get 
      {
        if (mRound.BombState == SpecialForce.BombState.Disabled)
          return "غیر فعال";
        else if (mRound.BombState == SpecialForce.BombState.Enabled)
          return "فعال نشده";
        else if (mRound.BombState == SpecialForce.BombState.Planted)
          return "فعال شده";
        else if (mRound.BombState == SpecialForce.BombState.Diffused)
          return "خنثی شده";
        else
          return "منفجر شده";
      }
    }

    public Round(DB.Round round)
    {
      mRound = round;
    }
  }
}
