﻿using System;
using System.Collections.Generic;

namespace SpecialForce.Stats
{
  public class UserOverall
  {
    private DB.User mUser = null;
    public DB.User UserInstance
    {
      get { return mUser; }
    }

    public int UserID
    {
      get { return mUser.ID; }
    }

    private DB.Competition mCompetition = null;
    public DB.Competition CompetitionInstance
    {
      get { return mCompetition; }
    }

    public int CompetitionID
    {
      get { return mCompetition.ID; }
    }

    private DB.Game mGame = null;
    public DB.Game GameInstance
    {
      get { return mGame; }
    }

    public int GameID
    {
      get { return mGame.ID; }
    }

    public string Title
    {
      get 
      {
        if (mGame != null)
          if (mGame.CompetitionID == -1)
            return "عادی";

        string result =
          mCompetition != null ?
          Enums.ToString(mCompetition.Type) + " " + mCompetition.Title :
          Enums.ToString(mGame.Competition.Type) + " " + mGame.Competition.Title;
        

        if (mGame != null && mGame.BlueTeamID != -1 && mGame.GreenTeamID != -1)
        {
          result += " بین ";
          result += mGame.GetBlueTeamName();
          result += " و ";
          result += mGame.GetGreenTeamName();
        }

        return result;
      }
    }

    public PersianDateTime Date
    {
      get { return mGame != null ? mGame.Start : mCompetition.Start; }
    }

    public string Time
    {
      get { return mGame != null ? mGame.Start.ToString(DateTimeString.TimeNoSecond) : "-"; }
    }

    public string Type
    {
      get { return Enums.ToString(mGame != null ? mGame.Object : mCompetition.Object); }
    }

    public int Frag
    {
      get { return mGame != null ? mGame.GetFrag(mUser) : mCompetition.GetFrag(mUser); }
    }

    public int KillCount
    {
      get { return mGame != null ? mGame.GetKillCount(mUser) : mCompetition.GetKillCount(mUser); }
    }

    public int FriendlyKillCount
    {
      get { return mGame != null ? mGame.GetFriendlyKillCount(mUser) : mCompetition.GetFriendlyKillCount(mUser); }
    }

    public int DeathCount
    {
      get { return mGame != null ? mGame.GetDeathCount(mUser) : mCompetition.GetDeathCount(mUser); }
    }

    public int PlantCount
    {
      get { return mGame != null ? mGame.GetPlantCount(mUser) : mCompetition.GetPlantCount(mUser); }
    }

    public int DiffuseCount
    {
      get { return mGame != null ? mGame.GetDiffuseCount(mUser) : mCompetition.GetDiffuseCount(mUser); }
    }

    public UserOverall(DB.User player, DB.Game game)
    {
      mUser = player;
      mGame = game;
    }

    public UserOverall(DB.User player, DB.Competition comp)
    {
      mUser = player;
      mCompetition = comp;
    }
  }
}
