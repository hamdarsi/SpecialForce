﻿using System;
using System.Collections.Generic;

namespace SpecialForce
{
  public class Statistics<T>
  {
    private Dictionary<string, string> mMetaData = new Dictionary<string,string>();
    public Dictionary<string, string> MetaData
    {
      get { return mMetaData; }
    }

    private List<T> mData = new List<T>();
    public List<T> Data
    {
      get { return mData; }
    }

    public void _AddData(T data)
    {
      mData.Add(data);
    }

    public void _AddMetaData(string key, string value)
    {
      mMetaData.Add(key, value);
    }

    public void PumpMetaData(Reports.Table table)
    {
      foreach (KeyValuePair<string, string> meta in mMetaData)
        table.AddKeyValuePair(meta.Key, meta.Value);
    }
  }
}
