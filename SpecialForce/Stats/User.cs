﻿using System;
using System.Collections.Generic;

namespace SpecialForce.Stats
{
  public class User
  {
    private int mUserID;
    public DB.User Instance
    {
      get { return DB.User.GetUserByID(mUserID); }
    }

    public int ID
    {
      get { return mUserID; }
    }

    private int mRank = 0;
    public int Rank
    {
      get { return mRank; }
    }

    public string FullName
    {
      get { return Instance.FullName; }
    }

    private int mKillCount = 0;
    public int KillCount
    {
      get { return mKillCount; }
    }

    private int mFriendlyKillCount = 0;
    public int FriendlyKillCount
    {
      get { return mFriendlyKillCount; }
    }

    private int mDeathCount = 0;
    public int DeathCount
    {
      get { return mDeathCount; }
    }

    private int mPlantCount = 0;
    public int PlantCount
    {
      get { return mPlantCount; }
    }

    private int mDiffuseCount = 0;
    public int DiffuseCount
    {
      get { return mDiffuseCount; }
    }

    public int Frag
    {
      get 
      {
        return mKillCount * DB.Options.KillFrag -
               mFriendlyKillCount * DB.Options.FriendlyKillFrag +
               mPlantCount * DB.Options.BombPlantFrag +
               mDiffuseCount * DB.Options.BombDiffuseFrag;
      }
    }

    public User(int id)
    {
      mUserID = id;
    }

    public void AddKills(int count)
    {
      mKillCount += count;
    }

    public void AddFriendlyKills(int count)
    {
      mFriendlyKillCount += count;
    }

    public void AddDeaths(int count)
    {
      mDeathCount += count;
    }

    public void AddPlants(int count)
    {
      mPlantCount += count;
    }

    public void AddDiffuses(int count)
    {
      mDiffuseCount += count;
    }

    public void SetRank(int rank)
    {
      mRank = rank;
    }

    public CompareResult CompareTo(User other)
    {
      // Priorities:
      // 1: Frag
      if (Frag > other.Frag)
        return CompareResult.Higher;
      else if (Frag < other.Frag)
        return CompareResult.Lower;

      // 2: Kills
      if (mKillCount > other.mKillCount)
        return CompareResult.Higher;
      else if (mKillCount < other.mKillCount)
        return CompareResult.Lower;

      // 3: Deaths
      if (mDeathCount < other.mDeathCount)
        return CompareResult.Higher;
      else if (mDeathCount > other.mDeathCount)
        return CompareResult.Lower;

      // 4: Plants
      if (mPlantCount > other.mPlantCount)
        return CompareResult.Higher;
      else if (mPlantCount < other.mPlantCount)
        return CompareResult.Lower;

      // 5: Diffuses
      if (mDeathCount > other.mDiffuseCount)
        return CompareResult.Higher;
      else if (mDiffuseCount < other.mDiffuseCount)
        return CompareResult.Lower;

      // 7: Equal
      return CompareResult.Equal;
    }


    public static bool operator > (User u1, User u2)
    {
      return u1.CompareTo(u2) == CompareResult.Higher;
    }

    public static bool operator < (User u1, User u2)
    {
      return u1.CompareTo(u2) == CompareResult.Lower;
    }

    public static bool operator == (User u1, User u2)
    {
      return u1.CompareTo(u2) == CompareResult.Equal;
    }

    public static bool operator !=(User u1, User u2)
    {
      return u1.CompareTo(u2) != CompareResult.Equal;
    }

    public override int GetHashCode()
    {
      return mUserID;
    }

    public override bool Equals(object obj)
    {
      return this == (obj as User);
    }
  }
}
