﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace SpecialForce.Stats
{
  public class Generator
  {
    #region Extraction of Information Relative to Competitions
    static public Statistics<Team> Generate(DB.Competition comp, bool cup_overall, int group = -1)
    {
      // generate list of teams
      List<int> source = group == -1 ? comp.TeamIDs : comp.Groupings[group];
      List<Team> teams = new List<Team>();
      foreach (int team_id in source)
        teams.Add(new Team(DB.Team.GetTeamByID(team_id)));

      // fill in the list
      GenerateCompetitionStats(true, teams, cup_overall, comp, group);

      // return the result
      Statistics<Stats.Team> stats = new Statistics<Team>();
      foreach (Team ts in teams)
        stats._AddData(ts);

      return stats;
    }

    static private Stats.Team CompetitionStatsInternalRoutine(List<Stats.Team> teams, int team_id, DB.Game game)
    {
      Stats.Team result = GetTeamScoresInstance(team_id, teams);
      TeamType side = game.BlueTeamID == team_id ? TeamType.Blue : TeamType.Green;

      if (game != null && (game.State == GameState.InProgress || game.State == GameState.Finished))
      {
        result.AddKills(game.GetKillCount(side));
        result.AddFriendlyKills(game.GetFriendlyKillCount(side));
        result.AddDeaths(game.GetDeathCount(side));
        result.AddPlants(game.GetPlantCount(side));
        result.AddDiffuses(game.GetDiffuseCount(side));
      }

      return result;
    }

    static private void GenerateCompetitionStats(bool rank, List<Stats.Team> team_scores, bool cup_overall, DB.Competition comp, int group)
    {
      if(cup_overall)
      {
        DB.CandidateMatch match = comp.GetCandidateMatchByWinnerCodeName("فینال");
        DB.Game game = DB.GameManager.GetGameByID(match.GameID);

        // Final's teams
        Stats.Team blue_team = CompetitionStatsInternalRoutine(team_scores, game.BlueTeamID, game);
        Stats.Team green_team = CompetitionStatsInternalRoutine(team_scores, game.GreenTeamID, game);

        Stats.Team winner = game.Winner == WinnerType.Blue ? blue_team : green_team;
        Stats.Team loser = game.Winner == WinnerType.Blue ? green_team : blue_team;

        // Winner is assigned 4 wins
        winner.AddWin(); winner.AddWin(); winner.AddWin(); winner.AddWin();
        // Loser three
        loser.AddWin(); loser.AddWin(); loser.AddWin();


        match = comp.GetCandidateMatchByWinnerCodeName("رده بندی");
        game = DB.GameManager.GetGameByID(match.GameID);

        // Final's teams
        blue_team = CompetitionStatsInternalRoutine(team_scores, game.BlueTeamID, game);
        green_team = CompetitionStatsInternalRoutine(team_scores, game.GreenTeamID, game);

        winner = game.Winner == WinnerType.Blue ? blue_team : green_team;
        loser = game.Winner == WinnerType.Blue ? green_team : blue_team;

        // Winner is assigned 2 wins
        winner.AddWin(); winner.AddWin();
        // Loser just 1
        loser.AddWin();
      }
      else
        foreach (DB.CandidateMatch cm in comp.CandidateMatches)
        {
          // Filter matches
          if (group != -1 && (cm.Stage != 0 || cm.Group != group))
            continue;

          // Get team stat instances
          Stats.Team blue = GetTeamScoresInstance(cm.BlueTeam.TeamID, team_scores);
          Stats.Team green = cm.GreenTeam != null ? GetTeamScoresInstance(cm.GreenTeam.TeamID, team_scores) : null;

          // Game count, wins, losses and ties
          DB.Game game = DB.GameManager.GetGameByID(cm.GameID);
          if (cm.Valid && game != null && game.State == GameState.Finished)
          {
            if (game.Winner == WinnerType.Blue)
            {
              blue.AddWin();
              green.AddLoss();
            }
            else if (game.Winner == WinnerType.Green)
            {
              blue.AddLoss();
              green.AddWin();
            }
            else if (game.Winner == WinnerType.Tie)
            {
              blue.AddTie();
              green.AddTie();
            }
          }

          // Kills , Deaths, Plants and Diffuses
          if (game != null && (game.State == GameState.InProgress || game.State == GameState.Finished))
          {
            blue.AddKills(game.GetKillCount(TeamType.Blue));
            blue.AddFriendlyKills(game.GetFriendlyKillCount(TeamType.Blue));
            blue.AddDeaths(game.GetDeathCount(TeamType.Blue));

            green.AddKills(game.GetKillCount(TeamType.Green));
            green.AddFriendlyKills(game.GetFriendlyKillCount(TeamType.Green));
            green.AddDeaths(game.GetDeathCount(TeamType.Green));

            blue.AddPlants(game.GetPlantCount(TeamType.Blue));
            blue.AddDiffuses(game.GetDiffuseCount(TeamType.Blue));

            green.AddPlants(game.GetPlantCount(TeamType.Green));
            green.AddDiffuses(game.GetDiffuseCount(TeamType.Green));
          }
        }

      // Sort the teams
      if (rank)
      {
        for (int i = 0; i < team_scores.Count; ++i)
          for (int j = i + 1; j < team_scores.Count; ++j)
          {
            CompareResult cr = team_scores[j].CompareTo(team_scores[i], comp.CandidateMatches);
            if (cr == CompareResult.Higher)
            {
              Stats.Team temp = team_scores[j];
              team_scores[j] = team_scores[i];
              team_scores[i] = temp;
            }
          }

        // Rank the teams
        int current_rank = 1;
        int delta_rank;
        for (int i = 0; i < team_scores.Count; ++i)
        {
          delta_rank = 1;
          team_scores[i].Rank = current_rank;
          for (int j = i + 1; j < team_scores.Count; ++j)
            if (team_scores[i].CompareTo(team_scores[j], comp.CandidateMatches) == CompareResult.Equal)
            {
              team_scores[j].Rank = current_rank;
              ++i;
              ++delta_rank;
            }
            else
              break;

          current_rank += delta_rank;
        }
      }

      if (cup_overall)
        team_scores.RemoveRange(4, team_scores.Count - 4);
    }
    #endregion Extraction of Information Relative to Competitions


    #region Extraction of Information Relative to Games
    static public Statistics<Game> Generate(List<DB.Game> games)
    {
      SortedSet<int> user_ids = new SortedSet<int>();
      Statistics<Game> result = new Statistics<Game>();

      // statistics for games
      for (int i = 0; i < games.Count; ++i)
      {
        result._AddData(new Stats.Game(games[i]));
        games[i].CopyUserIDsToList(user_ids);
      }

      // statistics for users
      List<User> users = CreateUserStatList(user_ids);
      foreach (User u in users)
        ExtractUserAchievements(u, games);

      Guinness.FromUsers(users).WriteStats(result);
      return result;
    }
    #endregion Extraction of Information Relative to Games


    #region Extraction of Information Relative to Rounds
    static public Statistics<Round> Generate(List<DB.Round> rounds)
    {
      SortedSet<int> user_ids = new SortedSet<int>();
      Statistics<Round> result = new Statistics<Round>();

      // statistics for rounds
      for (int i = 0; i < rounds.Count; ++i)
      {
        result._AddData(new Round(rounds[i]));
        rounds[i].AddUserIDs(user_ids);
      }

      // statistics for users
      List<User> users = CreateUserStatList(user_ids);
      foreach (User u in users)
        ExtractUserAchievements(u, rounds);

      Guinness.FromUsers(users).WriteStats(result);
      return result;
    }

    static public Statistics<Player> Generate(DB.Round round)
    {
      SortedSet<int> user_ids = new SortedSet<int>();
      Statistics<Player> result = new Statistics<Player>();

      round.Game.CopyUserIDsToList(user_ids);
      // statistics for players
      foreach (DB.User u in DB.User.LoadUsers(user_ids.ToList()))
        result._AddData(new Player(round, u));

      // guinness statistics
      List<User> users = CreateUserStatList(user_ids);
      foreach (User u in users)
        ExtractUserAchievements(u, round);

      Guinness.FromUsers(users).WriteStats(result);
      return result;
    }
    #endregion Extraction of Information Relative to Rounds


    #region Extraction of Information Relative to Teams
    static private void ExtractTeamAchievements(Team team, DB.Game game)
    {
      bool is_blue;
      if (game.BlueTeamID == team.ID)
        is_blue = true;
      else if (game.GreenTeamID == team.ID)
        is_blue = false;
      else
        return;

      if (game.State == GameState.Finished)
      {
        if (game.Winner == WinnerType.Blue && is_blue)
          team.AddWin();
        else if (game.Winner == WinnerType.Green && is_blue)
          team.AddLoss();
        else if (game.Winner == WinnerType.Tie)
          team.AddTie();
      }

      // Kills , Deaths, Plants and Diffuses
      TeamType type = is_blue ? TeamType.Blue : TeamType.Green;
      if (game.State == GameState.InProgress || game.State == GameState.Finished)
      {
        team.AddKills(game.GetKillCount(type));
        team.AddFriendlyKills(game.GetFriendlyKillCount(type));
        team.AddDeaths(game.GetDeathCount(type));
        team.AddPlants(game.GetPlantCount(type));
        team.AddDiffuses(game.GetDiffuseCount(type));
      }
    }

    static private void ExtractTeamAchievements(Team team, List<DB.Game> games)
    {
      foreach (DB.Game game in games)
        ExtractTeamAchievements(team, game);
    }

    static public Statistics<TeamInCompetition> Generate(DB.Team team)
    {
      List<DB.Competition> comps = team.ParticipatingCompetitions;
      Statistics<TeamInCompetition> result = new Statistics<TeamInCompetition>();
      foreach (DB.Competition c in comps)
        result._AddData(new TeamInCompetition(team, c));

      return result;
    }

    static public Statistics<TeamInGame> Generate(DB.Team team, DB.Competition comp)
    {
      Statistics<TeamInGame> result = new Statistics<TeamInGame>();
      SortedSet<int> user_ids = new SortedSet<int>();

      // statistics for games
      int kills = 0, fkills = 0, deaths = 0, plants = 0, diffs = 0;

      foreach (DB.Game game in comp.Games)
        if (game.BlueTeamID == team.ID || game.GreenTeamID == team.ID)
        {
          TeamInGame stat = new TeamInGame(team, game);
          kills += stat.KillCount;
          fkills += stat.FriendlyKillCount;
          deaths += stat.DeathCount;
          plants += stat.PlantCount;
          diffs += stat.DiffuseCount;
          result._AddData(stat);
        }

      result._AddMetaData("تعداد کشتن ها", kills.ToString());
      result._AddMetaData("تعداد کشتن خودی", fkills.ToString());
      result._AddMetaData("تعداد مردن ها", deaths.ToString());
      result._AddMetaData("بمب گذاری ها", plants.ToString());
      result._AddMetaData("خنثی کردن ها", diffs.ToString());

      return result;
    }
    #endregion Extraction of Information Relative to Teams


    #region Extraction of Information Relative to Users
    static private void ExtractUserAchievements(User user, DB.Round round)
    {
      DB.User player = user.Instance;
      if (!round.BluePlayers.Contains(player) && !round.GreenPlayers.Contains(player))
        return;

      // Kills , Deaths, Plants and Diffuses
      if (round.State == RoundState.InProgress || round.State == RoundState.Finished)
      {
        user.AddKills(round.GetKillCount(player));
        user.AddFriendlyKills(round.GetFriendlyKillCount(player));
        user.AddDeaths(round.GetDeathCount(player));

        if (round.Planter != null && round.BombState == BombState.Exploded)
          user.AddPlants(round.Planter.ID == player.ID ? 1 : 0);

        if (round.Diffuser != null)
          user.AddDiffuses(round.Diffuser.ID == player.ID ? 1 : 0);
      }
    }

    static private void ExtractUserAchievements(User user, List<DB.Round> rounds)
    {
      foreach (DB.Round round in rounds)
        ExtractUserAchievements(user, round);
    }

    static private void ExtractUserAchievements(User user, DB.Game game)
    {
      if (!game.BlueUserIDs.Contains(user.ID) && !game.GreenUserIDs.Contains(user.ID))
        return;

      // Kills , Deaths, Plants and Diffuses
      DB.User player = user.Instance;
      if (game.State == GameState.InProgress || game.State == GameState.Finished)
      {
        user.AddKills(game.GetKillCount(player));
        user.AddFriendlyKills(game.GetFriendlyKillCount(player));
        user.AddDeaths(game.GetDeathCount(player));
        user.AddPlants(game.GetPlantCount(player));
        user.AddDiffuses(game.GetDiffuseCount(player));
      }
    }

    static private void ExtractUserAchievements(User user, List<DB.Game> games)
    {
      foreach (DB.Game game in games)
        ExtractUserAchievements(user, game);
    }

    static public Statistics<UserOverall> Generate(DB.User player, List<DB.Game> games, List<DB.Competition> comps)
    {
      Statistics<UserOverall> result = new Statistics<UserOverall>();

      if (games != null)
        foreach (DB.Game game in games)
          result._AddData(new UserOverall(player, game));

      if (comps != null)
        foreach (DB.Competition comp in comps)
          result._AddData(new UserOverall(player, comp));

      GenerateStats<UserOverall>(result, result.Data);
      return result;
    }

    static public Statistics<User> RankUsers(List<DB.Game> games)
    {
      // Get all user ids
      SortedSet<int> user_ids = new SortedSet<int>();
      foreach (DB.Game game in games)
        game.CopyUserIDsToList(user_ids);

      // Extract user achievements
      List<User> users = CreateUserStatList(user_ids);
      Statistics<User> result = new Statistics<User>();
      foreach (User u in users)
      {
        ExtractUserAchievements(u, games);
        result._AddData(u);
      }

      // Sort the user list
      if (users.Count > 1)
        Stats.Guinness.QuickSort(users, 0, users.Count - 1);

      // Assign their ranks
      int rank = 0;
      int delta = 1;
      for (int i = users.Count - 1; i >= 0; --i)
      {
        if (i < users.Count - 1 && users[i] == users[i + 1])
          ++delta;
        else
        {
          rank += delta;
          delta = 1;
        }

        users[i].SetRank(rank);
      }

      // Now store them in the result
      return result;
    }
    #endregion Extraction of Information Relative to Users


    #region Auxilary Statistics
    static private Team GetTeamScoresInstance(int id, List<Team> teams)
    {
      foreach (Team ts in teams)
        if (ts.ID == id)
          return ts;

      return null;
    }

    static private User GetUserScoresInstance(int id, List<User> users)
    {
      foreach (User us in users)
        if (us.ID == id)
          return us;

      return null;
    }

    static private List<User> CreateUserStatList(SortedSet<int> ids)
    {
      List<User> result = new List<User>();
      foreach (int id in ids)
        result.Add(new User(id));
      return result;
    }

    static private void GenerateStats<T>(Statistics<T> stats, List<DB.Round> rounds)
    {
      // Get all user ids
      SortedSet<int> user_ids = new SortedSet<int>();
      foreach (DB.Round round in rounds)
        round.AddUserIDs(user_ids);

      // Extract user achievements
      List<User> users = CreateUserStatList(user_ids);
      Statistics<User> result = new Statistics<User>();
      foreach (User u in users)
        ExtractUserAchievements(u, rounds);

      Guinness.FromUsers(users).WriteStats(stats);
    }

    static private void GenerateStats<T>(Statistics<T> meta_data, List<UserOverall> source)
    {
      int frag = 0, kills = 0, fkills = 0, deaths = 0, plants = 0, diffs = 0;

      foreach(UserOverall stat in source)
      {
        frag += stat.Frag;
        kills += stat.KillCount;
        fkills += stat.FriendlyKillCount;
        deaths += stat.DeathCount;
        plants += stat.PlantCount;
        diffs += stat.DiffuseCount;
      }

      meta_data._AddMetaData("اطلاعات جامع این بازی ها", "");
      meta_data._AddMetaData("", "");
      meta_data._AddMetaData("فرگ", frag.ToString());
      meta_data._AddMetaData("تعداد کشتن", kills.ToString());
      meta_data._AddMetaData("تعداد کشتن خودی", fkills.ToString());
      meta_data._AddMetaData("دفعات مردن", deaths.ToString());
      meta_data._AddMetaData("تعداد بمب گذاری", plants.ToString());
      meta_data._AddMetaData("تعداد خنثی سازی", diffs.ToString());
    }
    #endregion Auxilary Statistics
  }
}
