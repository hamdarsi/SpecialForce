﻿using System;
using System.Collections.Generic;

namespace SpecialForce.Stats
{
  public class Team
  {
    #region Properties
    private int mRank = -1;
    public int Rank
    {
      get { return mRank; }
      set { mRank = value; }
    }

    private DB.Team mTeam = null;
    public int ID
    {
      get { return mTeam.ID; }
    }

    public DB.Team Instance
    {
      get { return mTeam; }
    }

    public string TeamName
    {
      get { return mTeam.Name; }
    }

    private int mGameCount = 0;
    public int GameCount
    {
      get { return mGameCount; }
    }

    private int mWinCount = 0;
    public int WinCount
    {
      get { return mWinCount; }
    }

    private int mLossCount = 0;
    public int LossCount
    {
      get { return mLossCount; }
    }

    private int mTieCount = 0;
    public int TieCount
    {
      get { return mTieCount; }
    }

    public int Score
    {
      get { return mWinCount * 2 + mTieCount; }
    }

    private int mKillCount = 0;
    public int KillCount
    {
      get { return mKillCount; }
    }

    private int mFriendlyKillCount = 0;
    public int FriendlyKillCount
    {
      get { return mFriendlyKillCount; }
    }

    private int mDeathCount = 0;
    public int DeathCount
    {
      get { return mDeathCount; }
    }

    public int NetKills
    {
      get { return mKillCount - mDeathCount; }
    }

    private int mPlantCount = 0;
    public int PlantCount
    {
      get { return mPlantCount; }
    }

    private int mDiffuseCount = 0;
    public int DiffuseCount
    {
      get { return mDiffuseCount; }
    }
    #endregion Properties


    #region Constructor
    public Team(DB.Team team)
    {
      mTeam = team;
    }
    #endregion Constructor


    #region Statistics
    public void AddWin()
    {
      ++mGameCount;
      ++mWinCount;
    }

    public void AddLoss()
    {
      ++mGameCount;
      ++mLossCount;
    }

    public void AddTie()
    {
      ++mGameCount;
      ++mTieCount;
    }

    public void AddKills(int count)
    {
      mKillCount += count;
    }

    public void AddFriendlyKills(int count)
    {
      mFriendlyKillCount += count;
    }

    public void AddDeaths(int count)
    {
      mDeathCount += count;
    }

    public void AddPlants(int count)
    {
      mPlantCount += count;
    }

    public void AddDiffuses(int count)
    {
      mDiffuseCount += count;
    }
    #endregion Statistics


    #region Utilities
    private bool MatchContainsTeams(DB.CandidateMatch cm, int id1, int id2)
    {
      if (cm.GameID == -1)
        return false;

      if (cm.BlueTeam.TeamID == -1 || cm.GreenTeam.TeamID == -1)
        return false;

      if (cm.BlueTeam.TeamID != id1 && cm.BlueTeam.TeamID != id2)
        return false;

      if (cm.BlueTeam.TeamID == id1 && cm.GreenTeam.TeamID != id2)
        return false;

      if (cm.BlueTeam.TeamID == id2 && cm.GreenTeam.TeamID != id1)
        return false;

      return true;
    }
    #endregion Utilities


    #region Comparison
    public CompareResult CompareTo(Team ts, List<DB.CandidateMatch> matches)
    {
      // Priorities:
      // 1: Score
      if (Score > ts.Score)
        return CompareResult.Higher;
      else if (Score < ts.Score)
        return CompareResult.Lower;

      // 2: Wins
      if (mWinCount > ts.mWinCount)
        return CompareResult.Higher;
      else if (mWinCount < ts.mWinCount)
        return CompareResult.Lower;

      // 3: Losses
      if (mLossCount < ts.mLossCount)
        return CompareResult.Higher;
      else if (mLossCount > ts.mLossCount)
        return CompareResult.Lower;

      // 4: Frag
      if (NetKills > ts.NetKills)
        return CompareResult.Higher;
      else if (NetKills < ts.NetKills)
        return CompareResult.Lower;

      // 5: Kills
      if (KillCount > ts.KillCount)
        return CompareResult.Higher;
      else if (KillCount < ts.KillCount)
        return CompareResult.Lower;

      // 6: Head to head games
      if(matches != null)
        foreach (DB.CandidateMatch cm in matches)
          if (MatchContainsTeams(cm, mTeam.ID, ts.ID))
          {
            DB.Game game = DB.GameManager.GetGameByID(cm.GameID);
            if (game.WinnerTeam == mTeam.ID)
              return CompareResult.Higher;
            else if (game.WinnerTeam == ts.mTeam.ID)
              return CompareResult.Lower;

            break;
          }

      // 7: Coin toss
      return CompareResult.Equal;
    }
    #endregion Comparison
  }
}
