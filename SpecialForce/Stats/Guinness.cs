﻿using System;
using System.Collections.Generic;

namespace SpecialForce.Stats
{
  public class Guinness
  {
    private enum RecordType
    {
      Kill,
      FriendlyKill,
      Death,
      Plant,
      Diffuse
    }

    public class Record
    {
      public List<User> BestKillers = new List<User>();
      public List<User> BestTeamKillers = new List<User>();
      public List<User> BestEvader = new List<User>();
      public List<User> BestPlanter = new List<User>();
      public List<User> BestDiffuser = new List<User>();
      public List<User> BestPlayers = new List<User>();

      private void WriteMetaData<T>(Statistics<T> stats, string title, List<User> players)
      {
        string players_str = "";
        if (players.Count != 0)
          for (int i = 0; i < players.Count; ++i)
          {
            players_str += players[i].FullName;
            if (i != players.Count - 1)
              players_str += ", ";
          }
        else
          players_str = "-";

        stats._AddMetaData(title, players_str);
      }

      public void WriteStats<T>(Statistics<T> stats)
      {
        // register them
        WriteMetaData(stats, "بهترین بازیکن", BestPlayers);
        WriteMetaData(stats, "بیشترین کشتن", BestKillers);
        WriteMetaData(stats, "بیشترین کشتن خودی", BestTeamKillers);
        WriteMetaData(stats, "کمترین مردن", BestEvader);
        WriteMetaData(stats, "بهترین بمب گذار", BestPlanter);
        WriteMetaData(stats, "بهترین خنثی کننده", BestDiffuser);
      }
    }

    static private void UpdateList(List<User> players, User player, RecordType type)
    {
      bool clear = true;
      bool update = false;

      // For kills: player shall have kills even if list is empty
      if (type == RecordType.Kill)
      {
        if (players.Count == 0 && player.KillCount > 0)
          update = true;
        else if (players.Count != 0)
        {
          if (player.KillCount > players[0].KillCount)
            update = true;
          else if (player.KillCount == players[0].KillCount)
          {
            update = true;
            clear = false;
          }
        }
      }

      // For kills: player shall have kills even if list is empty
      else if (type == RecordType.FriendlyKill)
      {
        if (players.Count == 0 && player.FriendlyKillCount > 0)
          update = true;
        else if (players.Count != 0)
        {
          if (player.FriendlyKillCount > players[0].FriendlyKillCount)
            update = true;
          else if (player.FriendlyKillCount == players[0].FriendlyKillCount)
          {
            update = true;
            clear = false;
          }
        }
      }

      // for deaths, player shall have a kill to be added to evader lists
      else if (type == RecordType.Death)
      {
        if (players.Count == 0 && player.KillCount > 0)
          update = true;
        else if (players.Count != 0)
        {
          if (player.DeathCount < players[0].DeathCount)
          {
            if (player.KillCount > 0)
              update = true;
          }
          else if (player.DeathCount == players[0].DeathCount)
          {
            if (player.KillCount > 0)
            {
              update = true;
              clear = false;
            }
          }
        }
      }

      // for plants, player shall have at least 1 successful plant to be considered
      else if (type == RecordType.Plant)
      {
        if (players.Count == 0 && player.PlantCount > 0)
          update = true;
        else if (players.Count != 0)
        {
          if (player.PlantCount > players[0].PlantCount)
            update = true;
          else if (player.PlantCount == players[0].PlantCount)
          {
            update = true;
            clear = false;
          }
        }
      }

      // same goes for diffuses
      else if (type == RecordType.Diffuse)
      {
        if (players.Count == 0 && player.DiffuseCount > 0)
          update = true;
        else if (players.Count != 0)
        {
          if (player.DiffuseCount > players[0].DiffuseCount)
            update = true;
          else if (player.DiffuseCount == players[0].DiffuseCount)
          {
            update = true;
            clear = false;
          }
        }
      }

      // now update the list if needed
      if (update)
      {
        if (clear)
          players.Clear();

        players.Add(player);
      }
    }

    static private void SwapPlayers(List<User> players, int index1, int index2)
    {
      User temp = players[index1];
      players[index1] = players[index2];
      players[index2] = temp;
    }

    static public void QuickSort(List<User> players, int Left, int Right)
    {
      int LHold = Left;
      int RHold = Right;
      int Pivot = Left;
      ++Left;

      while (Right >= Left)
      {
        if (players[Pivot] < players[Left] && players[Right] < players[Pivot])
          SwapPlayers(players, Left, Right);
        else if (players[Pivot] < players[Left])
          --Right;
        else if (players[Right] < players[Pivot])
          ++Left;
        else
        {
          --Right;
          ++Left;
        }
      }

      SwapPlayers(players, Pivot, Right);
      Pivot = Right;
      if (Pivot > LHold)
        QuickSort(players, LHold, Pivot);
      if (RHold > Pivot + 1)
        QuickSort(players, Pivot + 1, RHold);
    }

    static public Record FromUsers(List<User> users)
    {
      // Create empty list
      Record result = new Record();

      // Now deduce guinness records based on the current achievements
      foreach (User player in users)
      {
        UpdateList(result.BestKillers, player, RecordType.Kill);
        UpdateList(result.BestTeamKillers, player, RecordType.FriendlyKill);
        UpdateList(result.BestEvader, player, RecordType.Death);
        UpdateList(result.BestPlanter, player, RecordType.Plant);
        UpdateList(result.BestDiffuser, player, RecordType.Diffuse);
      }

      // Now sort the list to get the best player
      if (users.Count > 1)
      {
        QuickSort(users, 0, users.Count - 1);
        for (int i = users.Count - 1; i >= 0; --i)
          if (result.BestPlayers.Count != 0)
          {
            if (users[i] == result.BestPlayers[0])
              result.BestPlayers.Add(users[i]);
            else
              break;
          }
          else if (users[i].Frag > 0)
            result.BestPlayers.Add(users[i]);
      }

      return result;
    }
  }
}
