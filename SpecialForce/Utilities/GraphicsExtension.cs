﻿/* GraphicsExtension - [Extended Graphics]
 * Author name:           Arun Reginald Zaheeruddin
 * Current version:       1.0.0.4 (12b)
 * Release documentation: http://www.codeproject.com
 * License information:   Microsoft Public License (Ms-PL) [http://www.opensource.org/licenses/ms-pl.html]
 */

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace SpecialForce
{
  static class GraphicsExtension
  {
    #region Sizes
    static public class Sizes
    {
      static public System.Drawing.Size A4
      {
        get
        {
          return new System.Drawing.Size(598, 842);
        }
      }
    }
    #endregion Sizes


    #region Rounded Rectangle
    private static GraphicsPath GenerateRoundedRectangle(this Graphics graphics, RectangleF rectangle, float radius, RectangleEdgeFilter filter)
		{
			float diameter;
			GraphicsPath path = new GraphicsPath();
			if (radius <= 0.0F || filter == RectangleEdgeFilter.None)
			{
				path.AddRectangle(rectangle);
				path.CloseFigure();
				return path;
			}
			else
			{
				if (radius >= (Math.Min(rectangle.Width, rectangle.Height)) / 2.0)
					return graphics.GenerateCapsule(rectangle);
				diameter = radius * 2.0F;
				SizeF sizeF = new SizeF(diameter, diameter);
				RectangleF arc = new RectangleF(rectangle.Location, sizeF);
				if ((RectangleEdgeFilter.TopLeft & filter) == RectangleEdgeFilter.TopLeft)
					path.AddArc(arc, 180, 90);
				else
				{
					path.AddLine(arc.X, arc.Y + arc.Height, arc.X, arc.Y);
					path.AddLine(arc.X, arc.Y, arc.X + arc.Width, arc.Y);
				}
				arc.X = rectangle.Right - diameter;
				if ((RectangleEdgeFilter.TopRight & filter) == RectangleEdgeFilter.TopRight)
					path.AddArc(arc, 270, 90);
				else
				{
					path.AddLine(arc.X, arc.Y, arc.X + arc.Width, arc.Y);
					path.AddLine(arc.X + arc.Width, arc.Y + arc.Height, arc.X + arc.Width, arc.Y);
				}
				arc.Y = rectangle.Bottom - diameter;
				if ((RectangleEdgeFilter.BottomRight & filter) == RectangleEdgeFilter.BottomRight)
					path.AddArc(arc, 0, 90);
				else
				{
					path.AddLine(arc.X + arc.Width, arc.Y, arc.X + arc.Width, arc.Y + arc.Height);
					path.AddLine(arc.X, arc.Y + arc.Height, arc.X + arc.Width, arc.Y + arc.Height);
				}
				arc.X = rectangle.Left;
				if ((RectangleEdgeFilter.BottomLeft & filter) == RectangleEdgeFilter.BottomLeft)
					path.AddArc(arc, 90, 90);
				else
				{
					path.AddLine(arc.X + arc.Width, arc.Y + arc.Height, arc.X, arc.Y + arc.Height);
					path.AddLine(arc.X, arc.Y + arc.Height, arc.X, arc.Y);
				}
				path.CloseFigure();
			}
			return path;
		}

		private static GraphicsPath GenerateCapsule(this Graphics graphics, RectangleF rectangle)
		{
			float diameter;
			RectangleF arc;
			GraphicsPath path = new GraphicsPath();
			try
			{
				if (rectangle.Width > rectangle.Height)
				{
					diameter = rectangle.Height;
					SizeF sizeF = new SizeF(diameter, diameter);
					arc = new RectangleF(rectangle.Location, sizeF);
					path.AddArc(arc, 90, 180);
					arc.X = rectangle.Right - diameter;
					path.AddArc(arc, 270, 180);
				}
				else if (rectangle.Width < rectangle.Height)
				{
					diameter = rectangle.Width;
					SizeF sizeF = new SizeF(diameter, diameter);
					arc = new RectangleF(rectangle.Location, sizeF);
					path.AddArc(arc, 180, 180);
					arc.Y = rectangle.Bottom - diameter;
					path.AddArc(arc, 0, 180);
				}
				else path.AddEllipse(rectangle);
			}
			catch { path.AddEllipse(rectangle); }
			finally { path.CloseFigure(); }
			return path;
		}

		public static void DrawRoundedRectangle(this Graphics graphics, Pen pen, float x, float y, float width, float height, float radius, RectangleEdgeFilter filter)
		{
			RectangleF rectangle = new RectangleF(x, y, width, height);
			GraphicsPath path = graphics.GenerateRoundedRectangle(rectangle, radius, filter);
			SmoothingMode old = graphics.SmoothingMode;
			graphics.SmoothingMode = SmoothingMode.AntiAlias;
			graphics.DrawPath(pen, path);
			graphics.SmoothingMode = old;
		}

		public static void DrawRoundedRectangle(this Graphics graphics, Pen pen, float x, float y, float width, float height, float radius)
		{
			graphics.DrawRoundedRectangle(
					pen,
					x,
					y,
					width,
					height,
					radius,
					RectangleEdgeFilter.All);
		}

		public static void DrawRoundedRectangle(this Graphics graphics, Pen pen, int x, int y, int width, int height, int radius)
		{
			graphics.DrawRoundedRectangle(
					pen,
					Convert.ToSingle(x),
					Convert.ToSingle(y),
					Convert.ToSingle(width),
					Convert.ToSingle(height),
					Convert.ToSingle(radius));
		}

		public static void DrawRoundedRectangle(this Graphics graphics, Pen pen, Rectangle rectangle, int radius, RectangleEdgeFilter filter)
		{
			graphics.DrawRoundedRectangle(
				pen,
				rectangle.X,
				rectangle.Y,
				rectangle.Width,
				rectangle.Height,
				radius,
				filter);
		}

		public static void DrawRoundedRectangle(this Graphics graphics, Pen pen, Rectangle rectangle, int radius)
		{
			graphics.DrawRoundedRectangle(
				pen,
				rectangle.X,
				rectangle.Y,
				rectangle.Width,
				rectangle.Height,
				radius,
				RectangleEdgeFilter.All);
		}

		public static void DrawRoundedRectangle(this Graphics graphics, Pen pen, RectangleF rectangle, int radius, RectangleEdgeFilter filter)
		{
			graphics.DrawRoundedRectangle(
				pen,
				rectangle.X,
				rectangle.Y,
				rectangle.Width,
				rectangle.Height,
				radius,
				filter);
		}

		public static void DrawRoundedRectangle(this Graphics graphics, Pen pen, RectangleF rectangle, int radius)
		{
			graphics.DrawRoundedRectangle(
				pen,
				rectangle.X,
				rectangle.Y,
				rectangle.Width,
				rectangle.Height,
				radius,
				RectangleEdgeFilter.All);
		}

		public static void FillRoundedRectangle(this Graphics graphics, Brush brush, float x, float y, float width, float height, float radius, RectangleEdgeFilter filter)
		{
			RectangleF rectangle = new RectangleF(x, y, width, height);
			GraphicsPath path = graphics.GenerateRoundedRectangle(rectangle, radius, filter);
			SmoothingMode old = graphics.SmoothingMode;
			graphics.SmoothingMode = SmoothingMode.AntiAlias;
			graphics.FillPath(brush, path);
			graphics.SmoothingMode = old;
		}

		public static void FillRoundedRectangle(this Graphics graphics, Brush brush, float x, float y, float width, float height, float radius)
		{
			graphics.FillRoundedRectangle(
					brush,
					x,
					y,
					width,
					height,
					radius,
					RectangleEdgeFilter.All);
		}

		public static void FillRoundedRectangle(this Graphics graphics, Brush brush, int x, int y, int width, int height, int radius)
		{
			graphics.FillRoundedRectangle(
					brush,
					Convert.ToSingle(x),
					Convert.ToSingle(y),
					Convert.ToSingle(width),
					Convert.ToSingle(height),
					Convert.ToSingle(radius));
		}

		public static void FillRoundedRectangle(this Graphics graphics, Brush brush, Rectangle rectangle, int radius, RectangleEdgeFilter filter)
		{
			graphics.FillRoundedRectangle(
				brush,
				rectangle.X,
				rectangle.Y,
				rectangle.Width,
				rectangle.Height,
				radius,
				filter);
		}

		public static void FillRoundedRectangle(this Graphics graphics, Brush brush, Rectangle rectangle, int radius)
		{
			graphics.FillRoundedRectangle(
				brush,
				rectangle.X,
				rectangle.Y,
				rectangle.Width,
				rectangle.Height,
				radius,
				RectangleEdgeFilter.All);
		}

		public static void FillRoundedRectangle(this Graphics graphics, Brush brush, RectangleF rectangle, int radius, RectangleEdgeFilter filter)
		{
			graphics.FillRoundedRectangle(
				brush,
				rectangle.X,
				rectangle.Y,
				rectangle.Width,
				rectangle.Height,
				radius,
				filter);
		}

		public static void FillRoundedRectangle(this Graphics graphics, Brush brush, RectangleF rectangle, int radius)
		{
			graphics.FillRoundedRectangle(
				brush,
				rectangle.X,
				rectangle.Y,
				rectangle.Width,
				rectangle.Height,
				radius,
				RectangleEdgeFilter.All);
		}
    #endregion Rounded Rectangle


    #region Graphics extensions, Drawing Procedures
    public static void DrawGrayScale(this Graphics graphics, Image img, int x = 0, int y = 0)
    {
      ImageAttributes ia = new ImageAttributes();
      ColorMatrix cm = new ColorMatrix(
        new float[][]
          {
            new float[] {.3f, .3f, .3f, 0, 0},
            new float[] {.59f, .59f, .59f, 0, 0},
            new float[] {.11f, .11f, .11f, 0, 0},
            new float[] {0, 0, 0, 1, 0},
            new float[] {0, 0, 0, 0, 1}
          });

      ia.SetColorMatrix(cm);
      graphics.DrawImage(img,
                         new Rectangle(0, 0, img.Width, img.Height),
                         x,
                         y,
                         img.Width,
                         img.Height,
                         GraphicsUnit.Pixel,
                         ia);
    }

    public static void DrawHighlighted(this Graphics graphics, Image img, float amount = .2f, int x = 0, int y = 0)
    {
      ImageAttributes ia = new ImageAttributes();
      ColorMatrix cm = new ColorMatrix(
        new float[][]
          {
            new float[] {1, 0, 0, 0, 0},
            new float[] {0, 1, 0, 0, 0},
            new float[] {0, 0, 1, 0, 0},
            new float[] {0, 0, 0, 1, 0},
            new float[] {amount, amount, amount, 0, 1}
          });

      ia.SetColorMatrix(cm);
      graphics.DrawImage(img,
                         new Rectangle(0, 0, img.Width, img.Height),
                         x,
                         y,
                         img.Width,
                         img.Height,
                         GraphicsUnit.Pixel,
                         ia);
    }

    public static void DrawGradient(this Graphics graphics, int left, int top, int width, int height, Color start, Color end, Gradients type = Gradients.Vertical, int radius = 10, RectangleEdgeFilter corners = RectangleEdgeFilter.All)
    {
      if (type == Gradients.Vertical)
      {
        Rectangle rect = new Rectangle(left, top, width, height);
        Brush brush = new LinearGradientBrush(new Point(width / 2, top), new Point(width / 2, top + height), start, end);
        graphics.FillRoundedRectangle(brush, rect, radius, corners);
      }
      else if (type == Gradients.VerticalMirror)
      {
        Rectangle rect = new Rectangle(left, top, width, height / 2);
        Brush brush = new LinearGradientBrush(new Point(width / 2, top), new Point(width / 2, top + height / 2), start, end);
        graphics.FillRoundedRectangle(brush, rect, radius, (corners & RectangleEdgeFilter.TopLeft) | (corners & RectangleEdgeFilter.TopRight));

        rect = new Rectangle(left, top + height / 2, width, height / 2);
        brush = new LinearGradientBrush(new Point(width / 2, top + height / 2), new Point(width / 2, top + height), end, start);
        graphics.FillRoundedRectangle(brush, rect, radius, (corners & RectangleEdgeFilter.BottomLeft) | (corners & RectangleEdgeFilter.BottomRight));
      }
    }

    public static void DrawImageUnscaled(this Graphics graphics, int left, int top, Image img)
    {
      graphics.DrawImage(img, new Rectangle(left, top, img.Width, img.Height));
    }
    #endregion Graphics extensions, Drawing Procedures


    #region Save Format Suggestion
    /// <summary>
    /// This method suggests a format to dave the given image. In case
    /// the image is a memory photo it will be suggested to save using
    /// Jpeg image format.
    /// </summary>
    /// <returns>A format which suggests image format to save</returns>
    public static ImageFormat SuggestSaveFormat(this System.Drawing.Image img)
    {
      if (img.RawFormat.Equals(ImageFormat.Jpeg))
        return ImageFormat.Jpeg;
      else if (img.RawFormat.Equals(ImageFormat.Bmp))
        return ImageFormat.Bmp;
      else if (img.RawFormat.Equals(ImageFormat.Png))
        return ImageFormat.Png;
      else if (img.RawFormat.Equals(ImageFormat.Emf))
        return ImageFormat.Emf;
      else if (img.RawFormat.Equals(ImageFormat.Exif))
        return ImageFormat.Exif;
      else if (img.RawFormat.Equals(ImageFormat.Gif))
        return ImageFormat.Gif;
      else if (img.RawFormat.Equals(ImageFormat.Icon))
        return ImageFormat.Icon;
      else if (img.RawFormat.Equals(ImageFormat.MemoryBmp))
        return ImageFormat.Jpeg;
      else if (img.RawFormat.Equals(ImageFormat.Tiff))
        return ImageFormat.Tiff;
      else
        return ImageFormat.Jpeg;
    }
    #endregion Save Format Suggestion


    #region Bitmap's resize extension
    public static Bitmap Resize(this Image img, Size sz)
    {
      return new Bitmap(img, sz);
    }

    public static Bitmap Resize(this Image img, int width, int height)
    {
      return new Bitmap(img, new Size(width, height));
    }
    #endregion Bitmap's resize extension
  }
}
