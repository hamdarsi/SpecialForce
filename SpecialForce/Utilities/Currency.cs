﻿using System;

namespace SpecialForce
{
  public class Currency
  {
    private double mValue;
    public double Value
    {
      get { return mValue; }
    }

    public Currency(double value)
    {
      mValue = value;
    }

    public override string ToString()
    {
      string result = Convert.ToDouble(mValue).ToString();

      int pos = result.IndexOf('.') + 1;
      if (pos == 0)
        pos = result.Length;

      for (pos -= 3; pos > 0; pos -= 3)
        result = result.Insert(pos, ",");

      return result;
    }

    static public Currency FromObject(object field)
    {
      return new Currency(Convert.ToDouble(field));
    }
  }

  static public class CurrencyExtention
  {
    static public Currency ToCurrency(this double value)
    {
      return new Currency(value);
    }

    static public string ToCurrencyString(this double value)
    {
      return new Currency(value).ToString();
    }
  }
}
