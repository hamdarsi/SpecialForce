﻿using System;
using System.Collections.Generic;

namespace SpecialForce
{
  static public class Authentication
  {
    static public string Encrypt(string str)
    {
      return str;
    }

    static public string Decrypt(string str)
    {
      return str;
    }

    static public UserNameState CheckUserName(string str, DB.User user)
    {
      // Is any username given at all?
      if (str.Length == 0)
        return UserNameState.Empty;

      // If it contains invalid characters
      for (int i = 0; i < str.Length; ++i)
        if (str[i] == ' ')
          return UserNameState.InvalidCharacters;

      // Check for duplicate user names
      DB.User check = DB.User.GetUserByUserName(str);
      if (check != null && check.ID != (user == null ? -1 : user.ID))
        return UserNameState.Duplicate;
      
      // User name is acceptable
      return UserNameState.Acceptable;
    }
  }
}
