﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpecialForce
{
  public class TinyMath
  {
    public static int Round(double d)
    {
      return d >= 0.0 ? (int)(d + 0.5) : (int)(d - 0.5);
    }

    private static Random mSeed = new Random();

    public static int Random(int max)
    {
      return mSeed.Next(max);
    }
  }
}
