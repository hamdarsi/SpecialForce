using System;
using System.Runtime.InteropServices;

using System.Drawing;


namespace SpecialForce
{
	/// <summary>
	/// Win32 support code.
	/// (C) 2003 Bob Bradley / ZBobb@hotmail.com
	/// </summary>
	public class Win32
	{
		public const int  WM_MOUSEMOVE      = 0x0200;
		public const int  WM_LBUTTONDOWN    = 0x0201;
		public const int  WM_LBUTTONUP      = 0x0202;
		public const int  WM_RBUTTONDOWN    = 0x0204;
		public const int  WM_LBUTTONDBLCLK  = 0x0203;
		public const int  WM_MOUSELEAVE     = 0x02A3;
		public const int  WM_PAINT          = 0x000F;
		public const int  WM_ERASEBKGND     = 0x0014;
		public const int  WM_PRINT          = 0x0317;
		public const int  WM_HSCROLL        = 0x0114;
		public const int  WM_VSCROLL        = 0x0115;
    public const int  WM_SETREDRAW      = 0xB;
    public const int  WM_KEYDOWN        = 0x0100;
    public const int  WM_SYSKEYDOWN     = 0x0104;
    public const int  WM_NOTIFY         = 0x004E;
    public const int  ECM_FIRST         = 0x1500;
		public const int  EM_GETSEL         = 0x00B0;
		public const int  EM_LINEINDEX      = 0x00BB;
		public const int  EM_LINEFROMCHAR   = 0x00C9;
		public const int  EM_POSFROMCHAR    = 0x00D6;
    public const int  EM_SHOWBALLOONTIP = ECM_FIRST + 3;
    public const int  WM_PRINTCLIENT    = 0x0318;
    public const long PRF_CHECKVISIBLE  = 0x00000001L;
    public const long PRF_NONCLIENT     = 0x00000002L;
    public const long PRF_CLIENT        = 0x00000004L;
    public const long PRF_ERASEBKGND    = 0x00000008L;
    public const long PRF_CHILDREN      = 0x00000010L;
    public const long PRF_OWNED         = 0x00000020L;

    [StructLayout(LayoutKind.Sequential)]
    public struct POINT
    {
      public int X;
      public int Y;
   }

    [DllImport("USER32.DLL", EntryPoint = "PostMessage")]
		static public extern bool PostMessage(IntPtr hwnd, uint msg, IntPtr wParam, IntPtr lParam);

		[DllImport("USER32.DLL", EntryPoint= "SendMessage")]
		static public extern int SendMessage(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam);

    [DllImport("USER32.dll")]
    static public extern int SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);

    [DllImport("USER32.DLL", EntryPoint = "GetCaretBlinkTime")]
		static public extern uint GetCaretBlinkTime();

    [DllImport("user32.dll")]
    static private extern bool GetCursorPos(out POINT lpPoint);

		static public void CaptureWindow(System.Windows.Forms.Control control, ref System.Drawing.Bitmap bitmap)
		{
			//This function captures the contents of a window or control
			Graphics canvas = Graphics.FromImage(bitmap);

			int meint = (int)(PRF_CLIENT | PRF_ERASEBKGND);
			System.IntPtr meptr = new System.IntPtr(meint);

			System.IntPtr hdc = canvas.GetHdc();
			SendMessage(control.Handle, WM_PRINT, hdc, meptr);

			canvas.ReleaseHdc(hdc);
			canvas.Dispose();
		}

    static public Point GetCursorPosition()
    {
      POINT pt;
      GetCursorPos(out pt);
      return new Point(pt.X, pt.Y);
    }

    static public void RunApplication(string command)
    {
      try
      {
        System.Diagnostics.Process.Start(command);
      }
      catch (Exception ex)
      {
        Session.ShowError(ex.Message);
      }
    }
  }
}
