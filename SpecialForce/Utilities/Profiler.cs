﻿using System;
using System.Diagnostics;

namespace SpecialForce
{
  static public class Profiler
  {
    /// <summary>
    /// This method executes the given action while measuring its process time
    /// </summary>
    /// <param name="work">Work to profile its run duration</param>
    /// <returns>Duration for the work to complete in seconds</returns>
    static public double Time(Action work)
    {
      Stopwatch watch = new Stopwatch();
      watch.Start();
      work();
      watch.Stop();
      return watch.ElapsedMilliseconds / 1000.0;
    }

    /// <summary>
    /// This method evaluates the duration for the given action to run and
    /// returns string representation of the time.
    /// </summary>
    /// <param name="work">Work to profile its duration</param>
    /// <returns>String representation of the time to exec the action</returns>
    static public string TimeString(Action work)
    {
      string result = "";
      double time = Time(work);

      int hour = (int)(time / 3600);
      time -= hour * 3600;
      if(hour != 0)
        result = hour.ToString("D2") + ":";

      int min = (int)(time / 60);
      time -= min * 60;
      if(min != 0)
        result += min.ToString("D2") + ":";

      int sec = (int)time;
      time -= sec;
      if (sec != 0)
        result += sec.ToString("D2");
      else
        result += "0";

      result += time.ToString("F3").Substring(1);
      return result;
    }
  }
}
