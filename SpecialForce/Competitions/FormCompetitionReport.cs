﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Competitions
{
  public partial class FormCompetitionReport : SessionAwareForm
  {
    private DB.Competition mCompetition = null;
    public DB.Competition Competition
    {
      get { return mCompetition; }
      set { mCompetition = value; }
    }

    public FormCompetitionReport()
    {
      InitializeComponent();
    }

    private void FormCompetitionReport_Shown(object sender, EventArgs e)
    {
      try
      {
        ShowStandings();

        if (mCompetition.Type == CompetitionType.Cup)
        {
          rgCupViewMode.AddButton(Properties.Resources.Competition_All_Stats, "نمایش آمار در کل مسابقات", false);
          rgCupViewMode.AddButton(Properties.Resources.Competition_Leagues_Only, "نمایش رتبه بندی دور گروهی", true);
          rgCupViewMode.AddButton(Properties.Resources.Competition_Elimination_Only, "نمایش رتبه بندی کل", false);

          TextRenderer tr = new TextRenderer();
          tr.Font = new Font("Arial", 12.0f, FontStyle.Bold);
          tr.Color = Color.Black;
          tr.RightToLeft = false;
          for (int i = 0; i < mCompetition.Groupings.Count; ++i)
          {
            string group = Convert.ToChar(Convert.ToInt32('A') + i).ToString();
            Bitmap bmp = Properties.Resources.Competition_Leagues_Only_Groups;
            Graphics canvas = Graphics.FromImage(bmp);
            tr.Canvas = canvas;
            tr.Text = group;
            tr.Left = (bmp.Width - tr.TextWidth) / 2;
            tr.Top = (bmp.Height - tr.TextHeight) / 2;
            canvas.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            tr.Draw();

            rgGroups.AddButton(bmp, "نمایش گروه " + group, i == 1);
          }

          rgCupViewMode.Left = (report1.Width - rgCupViewMode.Width) / 2;
          rgGroups.Left = (report1.Width - rgGroups.Width) / 2;
          rgCupViewMode.Visible = true;
          rgGroups.Visible = true;
        }
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "(FormCompetitionReport)");
        Session.ShowError("خطایی در هنگام نمایش رتبه بندی ها پیش آمد.");
        DialogResult = DialogResult.Cancel;
        Close();
      }
    }

    private void ShowStandings()
    {
      // Generate report title
      string strTitle = "آمار مسابقات " + mCompetition.Title + " (" + Enums.ToString(mCompetition.Type) + ")";
      if (mCompetition.Type == CompetitionType.Cup)
      {
        if (rgCupViewMode.SelectedIndex == 1)
          strTitle = strTitle + " گروه " + (rgGroups.SelectedIndex + 1).ToString();
        else if(rgCupViewMode.SelectedIndex == 2)
          strTitle = strTitle + " دور حذفی";
      }

      // Wether to show extended view
      bool extended_view = false;
      if (mCompetition.Type == CompetitionType.League)
        extended_view = true;
      else if (mCompetition.Type == CompetitionType.Cup && rgCupViewMode.SelectedIndex == 1)
        extended_view = true;

      // Start drawing the chart
      report1.BeginUpdate();
      report1.Title = strTitle;
      Reports.Table table;

      // A dummy chart
      if (mCompetition.Type == CompetitionType.Cup)
      {
        table = report1.AddTable(false, 100);
      }

      // Report all standings
      table = report1.AddTable();
      if (rgCupViewMode.SelectedIndex == 1)
      {
        table.Width = table.Width - 80;
        table.Left = table.Left + 80;
      }
      table.AddNewColumn(Types.Integer, "رتبه", 50);
      table.AddNewColumn(Types.String, "تیم", 100);

      if(extended_view)
      {
        table.AddNewColumn(Types.Integer, "بازی", 40);
        table.AddNewColumn(Types.Integer, "برد", 40);
        table.AddNewColumn(Types.Integer, "باخت", 40);
        table.AddNewColumn(Types.Integer, "مساوی", 70);
      }

      table.AddNewColumn(Types.Integer, "کشتن", 40);
      table.AddNewColumn(Types.Integer, "مردن", 40);
      table.AddNewColumn(Types.Integer, "تفاضل", 70);

      if(extended_view)
        table.AddNewColumn(Types.Integer, "امتیاز", 40);

      // Fill the table with data
      Statistics<Stats.Team> stats;
      if (mCompetition.Type == CompetitionType.Elimination || (mCompetition.Type == CompetitionType.Cup && rgCupViewMode.SelectedIndex == 2))
        // Only top 4 teams of the whole cup
        stats = Stats.Generator.Generate(mCompetition, true, extended_view ? rgGroups.SelectedIndex : -1);
      else
        // Normal mode.
        stats = Stats.Generator.Generate(mCompetition, false, extended_view ? rgGroups.SelectedIndex : -1);
      foreach (Stats.Team ts in stats.Data)
      {
        int index = 0;
        Reports.Row row = table.AddNewRow(ts.Instance);
        row[index++] = ts.Rank;
        row[index++] = ts.TeamName;

        if (extended_view)
        {
          row[index++] = ts.GameCount;
          row[index++] = ts.WinCount;
          row[index++] = ts.LossCount;
          row[index++] = ts.TieCount;
        }

        row[index++] = ts.KillCount;
        row[index++] = ts.DeathCount;
        row[index++] = ts.NetKills;

        if (extended_view)
          row[index++] = ts.Score;
      }

      stats.PumpMetaData(report1.AddTable(false, 100, 300));

      // finished
      report1.EndUpdate();
      Height = Math.Min(report1.Bottom + TitleBarHeight + 5, 600);

      ScrollControlIntoView(report1);
      Top = (Screen.PrimaryScreen.WorkingArea.Height - Height) / 2;
    }

    private void rgCupViewMode_Click(object sender, EventArgs e)
    {
      rgGroups.Visible = rgCupViewMode.SelectedIndex == 1;
      ShowStandings();
    }

    private void rgGroups_Click(object sender, EventArgs e)
    {
      ShowStandings();
    }
  }
}
