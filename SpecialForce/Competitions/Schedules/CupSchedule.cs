﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Competitions.Charts
{
  [ToolboxItem(false)]
  public partial class CupSchedule : ScheduleCore
  {
    private Font mButtonFont = new Font("Arial", 18.0f, FontStyle.Bold);
    private UserInterface.RadioGroup mViewMode = new UserInterface.RadioGroup();

    public CupSchedule()
    {
      mViewMode.Parent = this;
      mViewMode.Top = 3;
      mViewMode.Click += ViewModeChanged;
      mViewMode.IsHorizontal = false;
    }

    protected override void OnPrepare()
    {
      // Create children for cups
      if (mCompetition.Type == CompetitionType.Cup)
      {
        InitChild(new EliminationSchedule(), -1);

        for (int i = 0; i < mCompetition.Groupings.Count; ++i)
          InitChild(new LeagueSchedule(), i);
      }

      mViewMode.ClearButtons();
      mViewMode.AddButton(Properties.Resources.Competition_Elimination_Only, "نمایش جدول دور حذفی", false);
      TextRenderer tr = new TextRenderer();
      tr.Color = Color.Black;
      tr.RightToLeft = false;
      tr.Font = mButtonFont;
      for (int i = 0; i < mCompetition.Groupings.Count; ++i)
      {
        string group = Convert.ToChar(Convert.ToInt32('A') + i).ToString();
        Bitmap bmp = Properties.Resources.Competition_Leagues_Only;
        Graphics canvas = Graphics.FromImage(bmp);
        tr.Canvas = canvas;
        tr.Text = group;
        tr.Left = (bmp.Width - tr.TextWidth) / 2 + 5;
        tr.Top = (bmp.Height - tr.TextHeight) / 2 + 5;
        canvas.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
        tr.Draw();
        mViewMode.AddButton(bmp, "نمایش جدول گروه " + group, i == 0);
      }
    }

    protected override void OnDrawSchedule()
    {
      for (int i = 0; i < mChildren.Count; ++i)
      {
        mChildren[i].Left = (Width - mChildren[i].Width - 70) / 2;
        mChildren[i].Top = (Height - mChildren[i].Height) / 2;
        mChildren[i].DrawSchedule();
        mChildren[i].Visible = false;
      }

      mViewMode.Left = Width - mViewMode.Width - 2;
      UpdateInterface(1);
    }

    public override void SaveToFile(string filename)
    {
      int h = 0, w = 0;
      for(int i = 0; i < mChildren.Count; ++i)
      {
        w = Math.Max(mChildren[i].Width, w);
        h += mChildren[i].Height;
        h += 30;
      }

      Bitmap bmp = new Bitmap(w, h);
      Graphics canvas = Graphics.FromImage(bmp);
      Application.DoEvents();

      h = 0;
      for (int i = 0; i < mChildren.Count; ++i)
      {
        mChildren[i].PrintMode = true;
        mChildren[i].Prepare();
        mChildren[i].DrawSchedule();

        canvas.DrawImageUnscaled(0, h, mChildren[i].Schedule);
        h += mChildren[i].Height;
        h += 30;

        mChildren[i].PrintMode = false;
        mChildren[i].Prepare();
        mChildren[i].DrawSchedule();
      }

      bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
      canvas.Dispose();
      bmp.Dispose();
    }

    private void UpdateInterface(int index)
    {
      for (int i = 0; i < mChildren.Count; ++i)
        mChildren[i].Visible = i == index;
    }

    private void ViewModeChanged(object sender, EventArgs e)
    {
      UpdateInterface(mViewMode.SelectedIndex);
    }
  }
}
