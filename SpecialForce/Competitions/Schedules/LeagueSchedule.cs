﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace SpecialForce.Competitions.Charts
{
  [ToolboxItem(false)]
  public partial class LeagueSchedule : ScheduleCore
  {
    private List<int> mTeams = null;
    private List<DB.CandidateMatch> mMatches = null;
    private Font mFont = new Font("B Nazanin", 12.0f, FontStyle.Bold);

    private int GetTeamIndex(int team)
    {
      for (int i = 0; i < mTeams.Count; ++i)
        if (mTeams[i] == team)
          return i;

      throw new Exception("(Competitions.Chart.League) team not found");
    }

    protected override void OnDrawSchedule()
    {
      mTeams = mCompetition.Type == CompetitionType.League ?
        mCompetition.TeamIDs :
        mCompetition.Groupings[mGroupNumber];

      mMatches = mCompetition.Type == CompetitionType.League ?
        mCompetition.CandidateMatches :
        mCompetition.GetGroupMatches(mGroupNumber);

      int width = MatchButton.LeagueViewSize.Width;
      int height = MatchButton.LeagueViewSize.Height;
      int current_top = 0;
      int current_left = 0;

      Brush back_brush, fore_brush, hatch_brush;

      if (!mPrintMode)
      {
        BackColor = Color.Transparent;
        ForeColor = Color.White;
        back_brush = Brushes.Transparent;
        fore_brush = Brushes.White;
        hatch_brush = new HatchBrush(HatchStyle.BackwardDiagonal, Color.FromArgb(140, 255, 255, 255), Color.Transparent);
      }
      else
      {
        BackColor = Color.White;
        ForeColor = Color.Black;
        back_brush = Brushes.White;
        fore_brush = Brushes.Black;
        hatch_brush = new HatchBrush(HatchStyle.BackwardDiagonal, Color.Black, Color.White);
      }

      // Paint header colors
      if(BackColor != Color.Transparent)
        Canvas.Clear(BackColor);

      // Draw lines
      Pen p = new Pen(fore_brush);
      for (int i = -1; i <= mTeams.Count; ++i)
      {
        // horizontal line
        Canvas.DrawLine(p, 0, current_top, Width, current_top);

        // vertical line
        Canvas.DrawLine(p, current_left, 0, current_left, Height);

        // update top and left
        current_top += height;
        current_left += width;
      }

      // Draw league name
      TextRenderer rend = new TextRenderer();
      rend.Font = mFont;
      rend.Canvas = Canvas;
      rend.Color = ForeColor;

      rend.Left = 1;
      rend.Top = 10;
      rend.Size = new Size(width, height - 20);
      rend.Text = mCompetition.Title;
      if (mCompetition.Type == CompetitionType.Cup)
        rend.AddText("گروه A" + (mGroupNumber + 1).ToString());
      rend.Draw();

      List<DB.Team> team_insts = DB.Team.GetTeamsByID(mTeams);
      // Draw team names
      for (int i = 0; i < mTeams.Count; ++i)
      {
        rend.Text = team_insts[i].Name;

        rend.Size = new Size(width, height - 20);
        rend.Justify = Alignment.BottomMiddle;
        rend.Vertical = true;
        rend.Left = (i + 1) * width + 30;
        rend.Top = 1;
        rend.Draw();

        rend.Size = new Size(width - 20, height);
        rend.Justify = Alignment.MiddleRight;
        rend.Vertical = false;
        rend.Left = 1;
        rend.Top = (i + 1) * height + 1;
        rend.Draw();
      }

      // Hatch the closed cells
      current_left = 100;
      current_top = 100;
      for (int i = 0; i < mTeams.Count; ++i)
      {
        Rectangle r = new Rectangle(current_left + 1, current_top + 1, width - 1, height - 1);
        Canvas.FillRectangle(hatch_brush, r);

        current_left += width;
        current_top += height;

        if(!mCompetition.RoundRobin)
          for(int j = 0; j <= i; ++j)
          {
            r = new Rectangle(current_left + 1, 100 + j * height + 1, width - 1, height - 1);
            Canvas.FillRectangle(hatch_brush, r);
          }
      }

      // Create buttons based on the new competition
      foreach (DB.CandidateMatch cm in mMatches)
      {
        int blue = GetTeamIndex(cm.BlueTeam.TeamID);
        int green = GetTeamIndex(cm.GreenTeam.TeamID);

        MatchButton mb = CreateButton();
        mb.PrintMode = PrintMode;
        mb.Top = (blue + 1) * height;
        mb.Left = (green + 1) * width;
        mb.Match = cm;
      }
    }
  }
}
