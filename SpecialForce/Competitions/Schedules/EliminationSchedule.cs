﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SpecialForce.Competitions.Charts
{
  [ToolboxItem(false)]
  public partial class EliminationSchedule : ScheduleCore
  {
    private List<int> mTeams = null;
    private List<List<DB.CandidateMatch>> mStages = null;

    protected override void OnDrawSchedule()
    {
      mTeams = mCompetition.TeamIDs;
      mStages = mCompetition.Stages;

      Color HeaderColor = Color.White;
      Color DisabledColor = Color.FromArgb(193, 208, 227);
      Color BorderColor = Color.Black;
      Color TextColor = Color.Black;

      int width = MatchButton.EliminationViewSize.Width;
      int height = MatchButton.EliminationViewSize.Height;
      bool is_cup = mCompetition.Type == CompetitionType.Cup;
      Image cup = Properties.Resources.Competition_Cup;

      if(mPrintMode)
        Canvas.Clear(Color.White);

      // draw matches
      MatchButton mbFinal = null;
      int start_stage = is_cup ? 1 : 0;
      int cup_height = Properties.Resources.Competition_Cup.Height;
      int chart_height = mCompetition.Stages[start_stage].Count / 2 * (height + MatchSpace) - MatchSpace;
      int chart_delta = cup_height + height / 2 - chart_height / 2;

      for (int stage = start_stage; stage < mStages.Count; ++stage)
      {
        for (int i = 0; i < mStages[stage].Count; ++i)
        {
          bool rtl = i >= mStages[stage].Count / 2;
          int btn_left = (stage - start_stage) * (width + MatchSpace);
          if (rtl)
            btn_left = Width - btn_left - width;

          int btn_top;
          MatchButton mb = CreateButton();

          if (stage == start_stage)
          {
            btn_top = rtl ? i - mStages[stage].Count / 2 : i;
            btn_top *= (height + MatchSpace);
            btn_top += chart_delta > 0 ? chart_delta : 0;
          }
          else if (stage != mStages.Count - 1)
          {
            MatchButton btn1 = FindMatchWinningTeam(mStages[stage][i].BlueTeam);
            MatchButton btn2 = FindMatchWinningTeam(mStages[stage][i].GreenTeam);
            btn_top = (btn1.Top + btn2.Top) / 2;
          }
          else if (mStages[stage][i].IsFinalMatch)
          {
            btn_left = (Width - mb.Width) / 2;
            btn_top = chart_delta > 0 ? cup_height : cup_height - chart_delta;
            mbFinal = mb;
          }
          else if (mStages[stage][i].IsLosersFinal)
          {
            btn_left = mbFinal.Left;
            btn_top = mbFinal.Bottom + MatchSpace;
          }
          else
            throw new Exception("(Competitions.Charts.EliminationSchedule) How to evaluate button's top?");

          mb.Left = btn_left;
          mb.Top = btn_top;
          mb.PrintMode = PrintMode;
          mb.Match = mStages[stage][i];
        }
      }

      Point pos = mbFinal.GetHotSpot(MatchButton.HotSpot.CupLocation);
      Canvas.DrawImageUnscaled(pos.X - cup.Width / 2, pos.Y - cup.Height, cup);
      cup.Dispose();
    }

    protected override void OnPostDraw()
    {
      int start_stage = mCompetition.Type == CompetitionType.Cup ? 1 : 0;
      for (int stage = start_stage + 1; stage < mStages.Count; ++stage)
        for (int i = 0; i < mStages[stage].Count; ++i)
        {
          MatchButton mb1 = FindMatchWinningTeam(mStages[stage][i].BlueTeam);
          MatchButton mb2 = FindMatchWinningTeam(mStages[stage][i].GreenTeam);

          if (mb1 == null)
          {
            mb1 = FindMatchLosingTeam(mStages[stage][i].BlueTeam);
            mb2 = FindMatchLosingTeam(mStages[stage][i].GreenTeam);
          }

          MatchButton btnDest = FindMatchWinningTeam(mStages[stage][i].WinnerCodeName);
          ConnectMatches(mb1, btnDest);
          ConnectMatches(mb2, btnDest);
        }
    }

    private void ConnectMatches(MatchButton m1, MatchButton m2)
    {
      Point pd;
      if (m2.Match.IsFinalMatch)
        pd = m2.GetHotSpot(m1.Left < m2.Left ? MatchButton.HotSpot.LeftUpperTeam : MatchButton.HotSpot.RightUpperTeam);
      else if (m2.Match.IsLosersFinal)
        return;
      else if(m1.Left < m2.Left)
        pd = m2.GetHotSpot(m1.Top < m2.Top ? MatchButton.HotSpot.LeftTopCorner : MatchButton.HotSpot.LeftBottomCorner);
      else
        pd = m2.GetHotSpot(m1.Top < m2.Top ? MatchButton.HotSpot.RightTopCorner : MatchButton.HotSpot.RightBottomCorner);

      Point ps = m1.GetHotSpot(MatchButton.HotSpot.GlobalMiddle);
      Pen p = new Pen(mPrintMode ? Brushes.Black : Brushes.White, 2);
      Canvas.DrawLine(p, ps, pd);
    }

    private MatchButton FindMatchWinningTeam(DB.CodeName team)
    {
      foreach (MatchButton mb in mButtons)
        if (mb.Match.WinnerCodeName == team)
          return mb;

      return null;
    }

    private MatchButton FindMatchLosingTeam(DB.CodeName team)
    {
      foreach (MatchButton mb in mButtons)
        if (mb.Match.LoserCodeName == team)
          return mb;

      return null;
    }
  }
}
