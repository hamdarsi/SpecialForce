﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Competitions.Charts
{
  [ToolboxItem(false)]
  public partial class MatchButton : UserControl
  {
    public enum HotSpot
    {
      CupLocation,
      LeftUpperTeam,
      RightUpperTeam,
      GlobalMiddle,
      LeftBottomCorner,
      LeftTopCorner,
      RightBottomCorner,
      RightTopCorner
    }

    #region Internal Data
    internal static Size LeagueViewSize = new Size(100, 100);
    internal static Size EliminationViewSize = new Size(130, 90);

    private Point mClickPoint = new Point(-1, -1);
    private Font mTimeStampFont = new Font("B Nazanin", 9.0f, FontStyle.Bold);
    private Font mElimTeamFont = new Font("B Titr", 11.0f, FontStyle.Bold);
    private Font mLeagueTeamFont = new Font("B Nazanin", 12.0f, FontStyle.Bold);
    #endregion Internal Data


    #region Properties
    private bool mPrintMode = false;
    public bool PrintMode
    {
      get { return mPrintMode; }
      set { mPrintMode = value; }
    }

    private DB.CandidateMatch mMatch = null;
    public DB.CandidateMatch Match
    {
      get { return mMatch; }
      set 
      {
        if (mMatch != null)
          mMatch.OnGameChanged -= MatchUpdated;

        mMatch = value;

        if(mMatch != null)
          mMatch.OnGameChanged += MatchUpdated;
      }
    }

    private int mScoreWidth = 20;
    public int ScoreWidth
    {
      get { return mScoreWidth; }
      set { mScoreWidth = value; }
    }

    private int mForkWidth = 30;
    public int ForkWidth
    {
      get { return mForkWidth; }
      set { mForkWidth = value; }
    }

    public bool RTL
    {
      get { return Left > Parent.Width / 2 - 20; }
    }
    #endregion Properties


    #region Constructor
    public MatchButton()
    {
      Size = EliminationViewSize;
    }
    #endregion Constructor


    #region Paint Overrdies
    protected override CreateParams CreateParams
    {
      get
      {
        CreateParams prm = base.CreateParams;
        prm.ExStyle |= 0x20;
        return prm;
      }
    }

    protected override void OnPaint(PaintEventArgs e)
    {
    }

    protected override void OnPaintBackground(PaintEventArgs e)
    {
    }
    #endregion Paint Overrdies


    #region Mouse Overrides
    protected override void OnMouseDown(MouseEventArgs e)
    {
      base.OnMouseDown(e);

      if (e.Button == MouseButtons.Left)
      {
        mClickPoint.X = e.X;
        mClickPoint.Y = e.Y;
      }
    }

    protected override void OnMouseUp(MouseEventArgs e)
    {
      base.OnMouseUp(e);
      mClickPoint.X = -1;
      mClickPoint.Y = -1;
    }

    protected override void OnMouseMove(MouseEventArgs e)
    {
      base.OnMouseMove(e);
      if (mClickPoint.X == -1)
        return;

      double dx = (e.X - mClickPoint.X);
      double dy = (e.Y - mClickPoint.Y);

      if (dx * dx + dy * dy < 100)
        return;
      else if (mMatch == null)
        return;
      else if (mMatch.GameID != -1)
        return;
      else if (!mMatch.Valid)
        return;
      else if (!Session.CurrentStaff.IsPrivileged())
        return;

      mClickPoint.X = -1;
      mClickPoint.Y = -1;
      Scheduling.ScheduleEntry entry = new Scheduling.ScheduleEntry();
      entry.CandidateMatch = mMatch;
      DoDragDrop(entry, DragDropEffects.Move);
    }
    #endregion Mouse Overrides


    #region Event Handlers
    private void mnuCancelGame_Click(object sender, EventArgs e)
    {
      if (mMatch != null && mMatch.GameID != -1)
        DB.GameManager.DeleteGame(mMatch.GameID);
    }

    private void MatchUpdated(Object sender, EventArgs e)
    {
      if (Parent is ScheduleCore)
        (Parent as ScheduleCore).Invalidate(this);
    }
    #endregion Event Handlers


    #region Area Calculation Utilities
    private void CreateRectangles(int top, out Rectangle whole, out Rectangle score, out Rectangle team_rect)
    {
      int iTotalWidth = Width - 4 - mForkWidth;
      if (mMatch.IsFinalMatch || mMatch.IsLosersFinal)
      {
        whole = new Rectangle(2, top, Width - 4, 30);
        score = new Rectangle(Width - mScoreWidth - 2, top, mScoreWidth, 30);
        team_rect = new Rectangle(2, top, Width - mScoreWidth - 2, 30);
      }
      else if (RTL)
      {
        whole = new Rectangle(mForkWidth + 2, top, iTotalWidth, 30);
        score = new Rectangle(mForkWidth + 2, top, mScoreWidth, 30);
        team_rect = new Rectangle(mForkWidth + mScoreWidth - 5, top, iTotalWidth - mScoreWidth - 2, 30);
      }
      else
      {
        whole = new Rectangle(2, top, iTotalWidth, 30);
        score = new Rectangle(iTotalWidth - mScoreWidth + 2, top, mScoreWidth, 30);
        team_rect = new Rectangle(12, top, iTotalWidth - mScoreWidth - 2, 30);
      }
    }

    public Point GetHotSpot(HotSpot hs)
    {
      if (hs == HotSpot.CupLocation)
        return new Point(Left + (Width - 4 - mScoreWidth) / 2, Top);

      if (hs == HotSpot.LeftUpperTeam)
        return new Point(Left, Top + 15);

      if (hs == HotSpot.RightUpperTeam)
        return new Point(Right - 1, Top + 15);

      if (hs == HotSpot.GlobalMiddle)
        return
          RTL ?
          new Point(Left, Top + Height / 2) :
          new Point(Right - 1, Top + Height / 2);

      if(hs == HotSpot.LeftTopCorner)
        return new Point(Left, Top + 15);

      if(hs == HotSpot.LeftBottomCorner)
        return new Point(Left, Bottom - 15);

      if(hs == HotSpot.RightTopCorner)
        return new Point(Right - 1, Top + 15);

      if(hs == HotSpot.RightBottomCorner)
        return new Point(Right - 1, Bottom - 15);

      throw new Exception("(Competitions.Charts.MatchButton) Unknown hotspot requested: " + hs.ToString());
    }
    #endregion Area Calculation Utilities


    #region Drawing Stuff
    public void DrawButton(Graphics canvas)
    {
      // no match assigned!?
      if (mMatch == null)
        return;

      try
      {
        // check if we are drawing for a league
        bool is_league = false;
        if (mMatch.Competition.Type == CompetitionType.League)
          is_league = true;
        if (mMatch.Competition.Type == CompetitionType.Cup && mMatch.Stage == 0)
          is_league = true;

        // prepare graphic objects
        Size = is_league ? LeagueViewSize : EliminationViewSize;
        DB.Game game = DB.GameManager.GetGameByID(mMatch.GameID);

        // Prepare canvas drawing tranformations
        canvas.TranslateTransform(Left, Top);
        canvas.SetClip(new Rectangle(1, 1, Width - 2, Height - 2));

        // prepare text renderer properties
        TextRenderer rend = new TextRenderer();
        rend.Canvas = canvas;

        if (is_league)
        {
          // adjust colors
          rend.Font = mLeagueTeamFont;
          rend.Left = 0;
          rend.Top = 0;
          rend.Size = Size;
          rend.Color = mPrintMode ? Color.Black : Color.Yellow;
          if (mPrintMode)
            canvas.Clear(Color.White);

          // for league buttons just display date and time of the game
          if (game == null)
            rend.AddText("تنظیم نشده");
          else
          {
            rend.AddText(game.Start.ToString(DateTimeString.TimeNoSecond));
            rend.AddText(game.Start.ToString(DateTimeString.CompactDate));
          }

          rend.Draw();
        }
        else
        {
          if (mPrintMode)
            canvas.Clear(Color.White);

          if (mMatch.GreenTeam != null)
          {
            DrawTeamRect(canvas, 1, mMatch.BlueTeam);
            DrawTeamRect(canvas, Height - 32, mMatch.GreenTeam);
          }
          else
            DrawTeamRect(canvas, (Height - 30) / 2, mMatch.BlueTeam);

          // draw timestamp
          rend.Location = new Point(RTL ? 20 : 0, 0);
          rend.Size = new Size(Width - 20, Height);
          rend.Color = mPrintMode ? Color.FromArgb(135, 85, 78) : Color.FromArgb(153, 217, 234);
          rend.Justify = Alignment.MiddleMiddle;
          if (!mMatch.Valid)
          {
            rend.Top -= 25;
            rend.Left = 0;
            rend.Text = "استراحت";
          }
          else
          {
            string str = "";
            if (mMatch.IsFinalMatch)
              str = "فینال: ";
            else if (mMatch.IsLosersFinal)
              str = "رده بندی: ";

            if (game == null)
              str += "اعلام نشده";
            else
              str += game.Start.ToString(DateTimeString.CompactDateTime);

            rend.Text = str;
          }
          rend.Font = mTimeStampFont;
          rend.Draw();
        }
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "Competition.Charts.MatchButton", "DrawButton");
      }
      finally
      {
        canvas.ResetClip();
        canvas.ResetTransform();
      }
    }

    private void DrawTeamRect(Graphics canvas, int top, DB.CodeName team)
    {
      Brush area_brush = mPrintMode ? Brushes.White : Brushes.LightYellow;
      Brush score_brush = new SolidBrush(Color.FromArgb(241, 174, 41));
      Pen area_pen = new Pen(new SolidBrush(Color.FromArgb(202, 183, 143)), 2);

      // paint all inner area
      Rectangle whole_area, score_area, team_area;
      CreateRectangles(top, out whole_area, out score_area, out team_area);

      canvas.FillRoundedRectangle(area_brush, whole_area, 5);
      if(mMatch.Valid)
        canvas.FillRoundedRectangle(score_brush, score_area, 5, RTL ? RectangleEdgeFilter.BottomLeft | RectangleEdgeFilter.TopLeft : RectangleEdgeFilter.TopRight | RectangleEdgeFilter.BottomRight);
      canvas.DrawRoundedRectangle(area_pen, whole_area, 5);

      // draw team name
      Alignment align = Alignment.MiddleMiddle;
      if (!mMatch.IsFinalMatch && !mMatch.IsLosersFinal)
        align = RTL ? align = Alignment.MiddleRight : Alignment.MiddleLeft;

      TextRenderer rend = new TextRenderer();
      rend.Canvas = canvas;
      rend.Color = mPrintMode ? Color.Black : Color.FromArgb(130, 68, 11);
      rend.Font = mElimTeamFont;
      rend.Location = team_area.Location;
      rend.Size = team_area.Size;
      rend.Text = team.TeamName;
      rend.Justify = align;
      rend.Draw();

      // Draw score
      rend.Justify = Alignment.MiddleMiddle;
      rend.Location = score_area.Location;
      rend.Size = score_area.Size;
      DB.Game game = DB.GameManager.GetGameByID(mMatch.GameID);
      if (game != null && game.State == GameState.Finished)
      {
        int blue_score, green_score;
        game.GetTeamScores(out blue_score, out green_score);
        rend.Text =
          team.TeamID == game.BlueTeamID ?
          blue_score.ToString() :
          green_score.ToString();
        rend.Draw();
      }
      else if(mMatch.Valid)
      {
        rend.Text = "؟";
        rend.Draw();
      }

      // Draw connecting lines
      // Don't draw for center cells
      if (mMatch.IsFinalMatch || mMatch.IsLosersFinal)
        return;

      Pen connection_pen = new Pen(mPrintMode ? Color.Black : Color.White, 2.0f);
      Point start = RTL ? new Point(mForkWidth, top + 15) : new Point(Width - mForkWidth, top + 15);
      Point middle = new Point(RTL ? 1 : Width - 2, start.Y);
      Point end = RTL ? new Point(middle.X, Height / 2) : new Point(middle.X, Height / 2);
      canvas.DrawLine(connection_pen, start, middle);
      canvas.DrawLine(connection_pen, middle, end);
    }
    #endregion Drawing Stuff
  }
}
