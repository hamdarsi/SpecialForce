﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Competitions.Charts
{
  [ToolboxItem(false)]
  public partial class ScheduleCore : ControlBase
  {
    #region Internal Data
    private Bitmap mBackGround;
    #endregion Internal Data


    #region Properties
    protected List<MatchButton> mButtons = new List<MatchButton>();
    protected List<ScheduleCore> mChildren = new List<ScheduleCore>();

    protected Bitmap mSchedule = null;
    public Bitmap Schedule
    {
      get { return mSchedule; }
    }

    private Graphics mCanvas = null;
    protected Graphics Canvas
    {
      get { return mCanvas; }
    }

    protected DB.Competition mCompetition = null;
    public DB.Competition Competition
    {
      get { return mCompetition; }
      set 
      {
        mCompetition = value;
        mGroupNumber = -1;
      }
    }

    protected int mGroupNumber = -1;
    public int GroupNumber
    {
      get { return mGroupNumber; }
      set { mGroupNumber = value; }
    }

    protected bool mPrintMode = false;
    public bool PrintMode
    {
      get { return mPrintMode; }
      set { mPrintMode = value; }
    }

    private int mMatchSpace = 60;
    protected int MatchSpace
    {
      get { return mMatchSpace; }
    }
    #endregion Properties


    #region SubscribableEvents
    public event EventHandler<CompetitionChartClickArgs> OnChartClicked;
    #endregion SubscribableEvents


    #region Overrides
    protected override void OnRecreateView(RecreateArgs e)
    {
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      if (mSchedule != null)
        e.Graphics.DrawImage(mSchedule, e.ClipRectangle, e.ClipRectangle, GraphicsUnit.Pixel);
      else
        e.Graphics.FillRectangle(Brushes.White, ClientRectangle);
    }

    protected override void OnSizeChanged(EventArgs e)
    {
      base.OnSizeChanged(e);

      if (mSchedule != null && Size != mSchedule.Size)
        Size = mSchedule.Size;
    }

    protected override void OnMouseDown(MouseEventArgs e)
    {
      base.OnMouseDown(e);
      FireClickEvent(null, e.Button);
    }
    #endregion Overrides


    #region Size Calculation
    private Size CalculateLeagueSize(int team_count)
    {
      int width = MatchButton.LeagueViewSize.Width;
      int height = MatchButton.LeagueViewSize.Height;
      return new Size((team_count + 1) * width + 1, (team_count + 1) * height + 1);
    }

    private Size CalculateEliminationSize()
    {
      int start_stage = mCompetition.Type == CompetitionType.Cup ? 1 : 0;

      int width = 2 * (mCompetition.Stages.Count - start_stage) - 1;
      width *= (MatchButton.EliminationViewSize.Width + MatchSpace);
      width -= MatchSpace;

      int btn_height = MatchButton.EliminationViewSize.Height;
      int cup_height = Properties.Resources.Competition_Cup.Height;

      // Option 1: Height of the chart
      int chart_height = mCompetition.Stages[start_stage].Count / 2 * (btn_height + MatchSpace) - MatchSpace;

      // Option 2: Cup image's height + Half of the final match button + Half of the chart
      int option2 = cup_height + chart_height / 2 + btn_height / 2;

      // Option 3: Cup image's height + full middle column height
      int option3 = cup_height + (mCompetition.Type == CompetitionType.Cup ? 2 * btn_height + MatchSpace : btn_height);

      // Actuall height is maximum of these three
      int height = Math.Max(Math.Max(chart_height, option2), option3);

      return new Size(width, height);
    }

    protected Size CalculateRequiredSize()
    {
      if (this is LeagueSchedule && mGroupNumber == -1)
        return CalculateLeagueSize(mCompetition.TeamIDs.Count);
      else if (this is LeagueSchedule)
        return CalculateLeagueSize(mCompetition.Groupings[mGroupNumber].Count);

      if (this is EliminationSchedule && mChildren.Count == 0)
        return CalculateEliminationSize();

      Size result = new Size(0, 0);

      for (int i = 0; i < mChildren.Count; ++i)
      {
        if (mChildren[i].Width > result.Width)
          result.Width = mChildren[i].Width;

        if (mChildren[i].Height > result.Height)
          result.Height = mChildren[i].Height;
      }

      result.Width += 70;
      return result;
    }
    #endregion Size Calculation


    #region Utilties for sub classes
    protected MatchButton CreateButton()
    {
      MatchButton btn = new MatchButton();
      btn.Parent = this;
      btn.PrintMode = mPrintMode;
      btn.MouseDown += MatchButtonClicked;
      mButtons.Add(btn);
      btn.BringToFront();
      return btn;
    }

    protected void InitChild(ScheduleCore child, int group)
    {
      mChildren.Add(child);
      child.Parent = this;
      child.Competition = mCompetition;
      child.GroupNumber = group;
      child.OnChartClicked += ChildClicked;
      child.Prepare();
    }

    private void MatchButtonClicked(Object sender, MouseEventArgs e)
    {
      FireClickEvent(sender as Charts.MatchButton, e.Button);
    }

    private void ChildClicked(object sender, CompetitionChartClickArgs e)
    {
      FireClickEvent(e.MatchButton as Charts.MatchButton, e.MouseButton);
    }

    protected void FireClickEvent(Charts.MatchButton btn, MouseButtons mouse_btn)
    {
      CompetitionChartClickArgs prms = new CompetitionChartClickArgs(btn, mouse_btn);
      EventHandler<CompetitionChartClickArgs> ev = OnChartClicked;
      if (ev != null)
        ev(this, prms);
    }
    #endregion Utilties for sub classes


    #region Utilities
    private Bitmap ProvideBackGround(ScheduleCore child)
    {
      Bitmap bmp = new Bitmap(child.Width, child.Height);
      Graphics canvas = Graphics.FromImage(bmp);
      canvas.DrawImage(
        mBackGround,
        new Rectangle(0, 0, child.Width, child.Height),
        new Rectangle(child.Left, child.Top, child.Width, child.Height),
        GraphicsUnit.Pixel);
      canvas.Dispose();
      return bmp;
    }

    public void Invalidate(MatchButton mb)
    {
      mCanvas.DrawImage(
        mBackGround,
        new Rectangle(mb.Left + 1, mb.Top + 1, mb.Width - 1, mb.Height - 1),
        new Rectangle(mb.Left + 1, mb.Top + 1, mb.Width - 1, mb.Height - 1),
        GraphicsUnit.Pixel);
      mb.DrawButton(mCanvas);
      Invalidate(mb.Bounds);
    }
    #endregion Utilities


    #region Schedule Functionality
    public virtual void SaveToFile(string filename)
    {
      if (mCompetition == null)
        return;

      Visible = false;

      mPrintMode = true;
      Prepare();
      DrawSchedule();
      mSchedule.Save(filename);

      mPrintMode = false;
      Prepare();
      DrawSchedule();

      Visible = true;
    }

    public void Prepare()
    {
      // Clear previous junk
      if (mSchedule != null)
      {
        mSchedule.Dispose();
        mCanvas.Dispose();

        foreach (ScheduleCore ctrl in mChildren)
        {
          ctrl.OnChartClicked -= ChildClicked;
          ctrl.Dispose();
        }
        mChildren.Clear();

        foreach (MatchButton mb in mButtons)
          mb.Dispose();
        mButtons.Clear();
      }

      // Ask subclasses for preperations
      OnPrepare();

      // Adjust size and location on parent
      Size = CalculateRequiredSize();
      Left = 0;
      Top = 0;
    }

    public void DrawSchedule()
    {
      // Draw the schedule
      try
      {
        // Create background
        if (!mPrintMode)
        {
          if (mBackGround != null)
            mBackGround.Dispose();

          if (Parent is ScheduleCore)
            mBackGround = (Parent as ScheduleCore).ProvideBackGround(this);
          else
          {
            Bitmap bg = Properties.Resources.BackGround;
            mBackGround = bg.Resize(Width, Height);
            bg.Dispose();
          }
        }

        // Create image and graphics
        mSchedule = mPrintMode ? new Bitmap(Width, Height) : mBackGround.Clone() as Bitmap;
        mCanvas = Graphics.FromImage(mSchedule);
        mCanvas.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
        mCanvas.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

        OnDrawSchedule();

        // In print mode, draw the buttons on the image
        for (int i = 0; i < mButtons.Count; ++i)
          mButtons[i].DrawButton(mCanvas);

        OnPostDraw();
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "ScheduleCore", "DrawSchedule()");
      }
    }
    #endregion Schedule Functionality


    #region Schedule Abstractions
    protected virtual void OnPrepare()
    {
    }

    protected virtual void OnPostDraw()
    {
    }

    protected virtual void OnDrawSchedule()
    {
      throw new NotImplementedException();
    }
    #endregion Schedule Abstractions
  }
}
