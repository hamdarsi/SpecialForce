﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Competitions
{
  public partial class FormViewCompetition : SessionAwareForm
  {
    private Charts.ScheduleCore mView;
    private Charts.MatchButton mCurrentButton;

    private DB.Competition mCompetition;
    public DB.Competition Competition
    {
      get { return mCompetition; }
      set { mCompetition = value; }
    }

    public FormViewCompetition()
    {
      InitializeComponent();
    }

    private void competitionChart1_OnSchedulingRequested(object sender, EventArgs e)
    {
      Scheduling.ControlPanel.Instance.ShowBounded(this);
    }

    private void FormViewCompetition_Shown(object sender, EventArgs e)
    {
      try
      {
        Application.UseWaitCursor = true;
        Application.DoEvents();
        Text = "جدول " + Enums.ToString(mCompetition.Type) + " " + mCompetition.Title;

        if (mCompetition.Type == CompetitionType.Cup)
          mView = new Charts.CupSchedule();
        else if (mCompetition.Type == CompetitionType.Elimination)
          mView = new Charts.EliminationSchedule();
        else
          mView = new Charts.LeagueSchedule();

        mView.Parent = this;
        mView.OnChartClicked += ChartClicked;
        mView.Competition = mCompetition;
        mView.PrintMode = false;
        mView.Prepare();
        int max_height = Screen.PrimaryScreen.WorkingArea.Height - TitleBarHeight - 20;
        Width = Math.Min(mView.Bounds.Width, 1000) + BorderWidth;
        Height = Math.Min(mView.Bounds.Height + TitleBarHeight + 20, max_height);
        mView.Left = 0;
        mView.Top = 0;
        AutoScrollMinSize = new Size(0, 0);
        CenterScreen();
        mView.DrawSchedule();
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "FormViewCompetition", "OnShown");
        Session.ShowError("خطایی در نمایش جدول مسابقه پیش آمد.");
        mView = null;
      }
      finally
      {
        Application.UseWaitCursor = false;
      }
    }

    private void ChartClicked(object sender, CompetitionChartClickArgs e)
    {
      mCurrentButton = e.MatchButton;
      if (e.MouseButton == MouseButtons.Right)
        mnuChart.Show(Cursor.Position, ToolStripDropDownDirection.BelowLeft);
    }

    private void mnuSchedule_Click(object sender, EventArgs e)
    {
      Scheduling.ControlPanel.Instance.ShowBounded(this);
    }

    private void mnuDelete_Click(object sender, EventArgs e)
    {
      DB.GameManager.DeleteGame(mCurrentButton.Match.GameID);
    }

    private void mnuSave_Click(object sender, EventArgs e)
    {
      if(saveFileDialog1.ShowDialog() == DialogResult.OK)
        mView.SaveToFile(saveFileDialog1.FileName);
    }

    private void mnuChart_Opening(object sender, CancelEventArgs e)
    {
      mnuSchedule.Available = Session.CurrentStaff.IsPrivileged();
      mnuDelete.Available = Session.CurrentStaff.IsPrivileged();
      mnuDelete.Available &= (mCurrentButton != null && mCurrentButton.Match != null && mCurrentButton.Match.GameID != -1);
    }
  }
}
