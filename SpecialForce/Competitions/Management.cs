﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Competitions
{
  public partial class Management : SessionAwareForm
  {
    private DB.Competition mCompetition = null;
    public DB.Competition Competition
    {
      get { return mCompetition; }
      set { mCompetition = value; }
    }

    public Management()
    {
      InitializeComponent();

      lvTeams.SmallImageList = new ImageList();
      lvTeams.LargeImageList = new ImageList();
      lvTeams.SmallImageList.ImageSize = new Size(24, 24);
      lvTeams.LargeImageList.ImageSize = new Size(64, 64);

      cmbCompetitionType.Items.Add(Enums.ToString(CompetitionType.Cup));
      cmbCompetitionType.Items.Add(Enums.ToString(CompetitionType.Elimination));
      cmbCompetitionType.Items.Add(Enums.ToString(CompetitionType.League));
      cmbCompetitionType.SelectedIndex = 2;

      cmbGameObject.Items.Add(Enums.ToString(GameObject.DeathMatch));
      cmbGameObject.Items.Add(Enums.ToString(GameObject.Bombing));
      cmbGameObject.SelectedIndex = 1;

      cmbRoundRobin.SelectedIndex = 1;
      cmbFriendlyFire.SelectedIndex = Enums.IndexFromFriendlyFire(true);
    }


    private void Management_Load(object sender, EventArgs e)
    {
      Text = mCompetition == null ? "ایجاد مسابقه" : "ویرایش مسابقه";
      if (mCompetition == null)
        return;

      txtTitle.Text = mCompetition.Title;
      txtRoundCount.Value = mCompetition.RoundCount;
      txtRoundTime.Value = mCompetition.RoundTime / 60;
      txtRestTime.Value = mCompetition.RestTime;
      txtHP.Value = mCompetition.StartHP;
      txtMagazineCount.Value = mCompetition.StartMagazine;
      txtAmmoPerMagazine.Value = mCompetition.AmmoPerMagazine;
      txtBombTimer.Value = mCompetition.BombTime;
      txtPrice.Value = mCompetition.AdmissionFee;
      txtGameTime.Value = mCompetition.GameTime;
      cmbFriendlyFire.SelectedIndex = Enums.IndexFromFriendlyFire(mCompetition.FriendlyFire);
      cmbRoundRobin.SelectedIndex = mCompetition.RoundRobin ? 0 : 1;

      // Competition Type
      for (int i = 0; i < cmbCompetitionType.Items.Count; ++i)
        if (cmbCompetitionType.Items[i].ToString() == Enums.ToString(mCompetition.Type))
        {
          cmbCompetitionType.SelectedIndex = i;
          break;
        }

      // Competition games object
      for (int i = 0; i < cmbGameObject.Items.Count; ++i)
        if (cmbGameObject.Items[i].ToString() == Enums.ToString(mCompetition.Object))
        {
          cmbGameObject.SelectedIndex = i;
          break;
        }

      // Teams
      lvTeams.Items.Clear();
      lvTeams.SmallImageList.Images.Clear();
      lvTeams.LargeImageList.Images.Clear();
      foreach (DB.Team team in mCompetition.GetTeams())
        AddTeam(team);

      // Only administrators can change competition and game settings
      // The competition shall be created first.
      bool admin_mode = Session.CurrentStaff.IsPrivileged();
      grpGameSettings.Enabled = admin_mode;
      grpCompetitionSettings.Enabled = admin_mode;
      txtRestTime.Minimum = Session.ReleaseBuild ? 10 : 1;
      lvTeams.Enabled = true;
      btnTeamAdd.Enabled = true;
      btnTeamRemove.Enabled = true;

      if (!admin_mode)
      {
        Height = grpGameSettings.Bottom + TitleBarHeight + 10;
        lvTeams.Height = grpGameSettings.Bottom - lvTeams.Top;
        Point loc = btnExit.Location;
        btnExit.Location = btnTeamRemove.Location;
        btnTeamRemove.Location = btnTeamAdd.Location;
        btnTeamAdd.Location = loc;
        btnExit.Visible = true;
      }
    }

    private void lvTeams_DragEnter(object sender, DragEventArgs e)
    {
      if (mCompetition != null && mCompetition.State != CompetitionState.Editing)
        return;

     if (e.Data.GetDataPresent(typeof(DB.Team)) == false)
        return;

      e.Effect = DragDropEffects.Move;
    }

    private void AddTeam(DB.Team team)
    {
      Image logo = team.Logo != null ? new Bitmap(team.Logo) : Properties.Resources.Team_Logo;

      ListViewItem it = new ListViewItem();
      it.Tag = team;
      it.Text = team.Name;
      lvTeams.LargeImageList.Images.Add(logo);
      lvTeams.SmallImageList.Images.Add(logo);
      it.ImageIndex = lvTeams.SmallImageList.Images.Count - 1;
      lvTeams.Items.Add(it);
    }

    private void lvTeams_DragDrop(object sender, DragEventArgs e)
    {
      DB.Team team = e.Data.GetData(typeof(DB.Team)) as DB.Team;

      foreach (ListViewItem it in lvTeams.Items)
        if (team.ID == (it.Tag as DB.Team).ID)
        {
          Session.ShowError("این تیم در لیست تیم ها وجود دارد.");
          return;
        }

      string str = "آیا می خواهید تیم " + team.Name + " را به این مسابقه اضافه کنید؟";
      str += Environment.NewLine;
      str += "ورودیه به مبلغ " + mCompetition.AdmissionFee.ToCurrencyString() + " تومان باید دریافت شود.";
      if (Session.Ask(str, "پیام"))
      {
        mCompetition.AddTeam(team);
        AddTeam(team);
      }
    }

    private void mnuTeams_Opening(object sender, CancelEventArgs e)
    {
      if (mCompetition != null && mCompetition.State != CompetitionState.Editing)
        mnuDeleteTeam.Enabled = false;
      else
        mnuDeleteTeam.Enabled = true;
    }

    private void mnuDeleteTeam_Click(object sender, EventArgs e)
    {
      if (lvTeams.SelectedItems.Count == 0 || lvTeams.SelectedItems[0] == null)
      {
        Session.ShowError("تیمی برای حذف کردن انتخاب نشده است.");
        return;
      }

      DB.Team team = lvTeams.SelectedItems[0].Tag as DB.Team;
      double diff = mCompetition.GetTeamRegistration(team).GetDateTimeDifference(PersianDateTime.Now);
      double acc_diff = DB.Options.RefundableCancelDuration * 3600;
      bool refund = diff <= acc_diff;
      string str = "تیم " + team.Name + " از مسابقه حذف شود؟";
      str += Environment.NewLine;
      if (refund)
        str += "مبلغ ورودیه (" + mCompetition.AdmissionFee.ToCurrencyString() + ") باید عودت داده شود.";
      else
      {
        str += "این تیم پس از مدت مقرر شده درخواست انصراف داده است.";
        str += Environment.NewLine;
        str += "مبلغ ورودیه عودت داده نمی شود.";
      }
      
      if (Session.Ask(str))
      {
        lvTeams.Items.Remove(lvTeams.SelectedItems[0]);
        mCompetition.RemoveTeam(team, refund);
      }
    }

    private void mnuLargeIcons_Click(object sender, EventArgs e)
    {
      lvTeams.View = View.LargeIcon;
    }

    private void mnuSmallIcons_Click(object sender, EventArgs e)
    {
      lvTeams.View = View.SmallIcon;
    }

    private void mnuList_Click(object sender, EventArgs e)
    {
      lvTeams.View = View.List;
    }

    private void cmbCompetitionType_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (cmbCompetitionType.SelectedItem.ToString() == Enums.ToString(CompetitionType.League))
      {
        cmbRoundRobin.Enabled = true;
        cmbGameObject.Enabled = true;
        txtRoundCount.Increment = 1;
      }
      else
      {
        cmbRoundRobin.SelectedIndex = 1;
        cmbRoundRobin.Enabled = false;

        cmbGameObject.SelectedIndex = 1;
        cmbGameObject.Enabled = false;

        txtRoundCount.Increment = 2;
        if (txtRoundCount.Value % 2 == 0)
          txtRoundCount.Value = txtRoundCount.Value + 1;
      }

      lblRoundRobin.Enabled = cmbRoundRobin.Enabled;
    }

    private void cmbGameObject_SelectedIndexChanged(object sender, EventArgs e)
    {
      txtBombTimer.Enabled =
         Enums.ParseGameObject(cmbGameObject.SelectedItem.ToString()) == GameObject.Bombing;

      lblBombTimer.Enabled = txtBombTimer.Enabled;
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      // Check for competition title
      if (txtTitle.Text.Length == 0)
      {
        Session.ShowError("نام مسابقه را ذکر کنید.");
        return;
      }

      // Check for competition admission
      if (txtPrice.Value <= 0)
      {
        Session.ShowError("هزینه ورودیه را وارد کنید.");
        return;
      }

      // Check for game lengths
      double len = (double)(txtRoundCount.Value * txtRoundTime.Value * 60 + (txtRoundCount.Value - 1) * txtRestTime.Value);
      if (len > (double)txtGameTime.Value * 60)
      {
        Session.ShowError("مدت زمان اجرای بازی از مدت زمان تعیین شده برای بازی بیشتر است.");
        return;
      }

      List<int> teams = new List<int>();
      foreach (ListViewItem it in lvTeams.Items)
        teams.Add((it.Tag as DB.Team).ID);

      if (mCompetition == null)
        mCompetition = new DB.Competition();

      mCompetition.Title = txtTitle.Text;
      mCompetition.Type = Enums.ParseCompetitionType(cmbCompetitionType.SelectedItem.ToString());
      mCompetition.RoundRobin = cmbRoundRobin.SelectedIndex == 0;
      mCompetition.RoundCount = (int)txtRoundCount.Value;
      mCompetition.RoundTime = (int)txtRoundTime.Value * 60; // to seconds
      mCompetition.RestTime = (int)txtRestTime.Value;
      mCompetition.StartHP = (int)txtHP.Value;
      mCompetition.StartMagazine = (int)txtMagazineCount.Value;
      mCompetition.AmmoPerMagazine = (int)txtAmmoPerMagazine.Value;
      mCompetition.Object = Enums.ParseGameObject(cmbGameObject.SelectedItem.ToString());
      mCompetition.BombTime = (int)txtBombTimer.Value;
      mCompetition.AdmissionFee = txtPrice.Value;
      mCompetition.GameTime = (int)txtGameTime.Value;
      mCompetition.FriendlyFire = Enums.FriendlyFireFromString(cmbFriendlyFire.SelectedItem.ToString());
      mCompetition.Apply();
      DialogResult = DialogResult.OK;
    }

    private void btnTeamAdd_Click(object sender, EventArgs e)
    {
      Teams.ControlPanel.Instance.ShowBounded(this);
    }

    protected override void ApplyPermissions(Post post)
    {
      grpCompetitionSettings.Enabled = post.IsPrivileged();
      grpGameSettings.Enabled = post.IsPrivileged();

      Refresh();
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void txtRoundCount_ValueChanged(object sender, EventArgs e)
    {
      if (txtRoundCount.Value % 2 == 0 && cmbCompetitionType.SelectedItem.ToString() != Enums.ToString(CompetitionType.League))
        txtRoundCount.Value++;
    }

    private void btnExit_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
