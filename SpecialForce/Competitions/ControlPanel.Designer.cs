﻿namespace SpecialForce.Competitions
{
  partial class ControlPanel
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPanel));
      this.lvCompetitions = new System.Windows.Forms.ListView();
      this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.toolBar3 = new SpecialForce.ToolBar();
      this.label6 = new SpecialForce.TransparentLabel();
      this.label2 = new SpecialForce.TransparentLabel();
      this.lblIncome = new SpecialForce.TransparentLabel();
      this.lblCompetitionDuration = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.label4 = new SpecialForce.TransparentLabel();
      this.lblBalance = new SpecialForce.TransparentLabel();
      this.lblExpenses = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.toolBar2 = new SpecialForce.ToolBar();
      this.label10 = new SpecialForce.TransparentLabel();
      this.label9 = new SpecialForce.TransparentLabel();
      this.dtEnd = new SpecialForce.PersianDatePicker();
      this.dtStart = new SpecialForce.PersianDatePicker();
      this.label8 = new SpecialForce.TransparentLabel();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.chkCups = new SpecialForce.TransparentCheckBox();
      this.chkEliminations = new SpecialForce.TransparentCheckBox();
      this.chkLeagues = new SpecialForce.TransparentCheckBox();
      this.label7 = new SpecialForce.TransparentLabel();
      this.tinypicker1 = new SpecialForce.TinyPicker();
      this.tbButtons = new SpecialForce.ToolBar();
      this.btnViewGames = new SpecialForce.ToolButton();
      this.btnRankings = new SpecialForce.ToolButton();
      this.tbLock = new SpecialForce.ToolButton();
      this.btnSchedule = new SpecialForce.ToolButton();
      this.btnStart = new SpecialForce.ToolButton();
      this.btnEdit = new SpecialForce.ToolButton();
      this.btnAdd = new SpecialForce.ToolButton();
      this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.toolBar3.SuspendLayout();
      this.toolBar2.SuspendLayout();
      this.toolBar1.SuspendLayout();
      this.tbButtons.SuspendLayout();
      this.SuspendLayout();
      // 
      // lvCompetitions
      // 
      this.lvCompetitions.BackColor = System.Drawing.SystemColors.Window;
      this.lvCompetitions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
      this.lvCompetitions.FullRowSelect = true;
      this.lvCompetitions.GridLines = true;
      this.lvCompetitions.Location = new System.Drawing.Point(110, 12);
      this.lvCompetitions.Name = "lvCompetitions";
      this.lvCompetitions.RightToLeftLayout = true;
      this.lvCompetitions.Size = new System.Drawing.Size(554, 393);
      this.lvCompetitions.TabIndex = 5;
      this.lvCompetitions.UseCompatibleStateImageBehavior = false;
      this.lvCompetitions.View = System.Windows.Forms.View.Details;
      this.lvCompetitions.SelectedIndexChanged += new System.EventHandler(this.lvCompetitions_SelectedIndexChanged);
      this.lvCompetitions.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvCompetitions_MouseDoubleClick);
      // 
      // columnHeader1
      // 
      this.columnHeader1.Text = "ردیف";
      this.columnHeader1.Width = 37;
      // 
      // columnHeader2
      // 
      this.columnHeader2.Text = "نام مسابقه";
      this.columnHeader2.Width = 97;
      // 
      // columnHeader3
      // 
      this.columnHeader3.Text = "نوع مسابقه";
      this.columnHeader3.Width = 66;
      // 
      // columnHeader4
      // 
      this.columnHeader4.Text = "وضعیت";
      this.columnHeader4.Width = 46;
      // 
      // columnHeader5
      // 
      this.columnHeader5.Text = "تاریخ شروع";
      this.columnHeader5.Width = 83;
      // 
      // columnHeader6
      // 
      this.columnHeader6.Text = "تاریخ پایان";
      this.columnHeader6.Width = 73;
      // 
      // toolBar3
      // 
      this.toolBar3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar3.BorderWidth = 0.5F;
      this.toolBar3.Controls.Add(this.label6);
      this.toolBar3.Controls.Add(this.label2);
      this.toolBar3.Controls.Add(this.lblIncome);
      this.toolBar3.Controls.Add(this.lblCompetitionDuration);
      this.toolBar3.Controls.Add(this.label1);
      this.toolBar3.Controls.Add(this.label4);
      this.toolBar3.Controls.Add(this.lblBalance);
      this.toolBar3.Controls.Add(this.lblExpenses);
      this.toolBar3.Controls.Add(this.label3);
      this.toolBar3.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar3.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar3.Location = new System.Drawing.Point(464, 419);
      this.toolBar3.Name = "toolBar3";
      this.toolBar3.Size = new System.Drawing.Size(200, 116);
      this.toolBar3.TabIndex = 42;
      // 
      // label6
      // 
      this.label6.Location = new System.Drawing.Point(8, 68);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(189, 15);
      this.label6.TabIndex = 27;
      this.label6.TabStop = false;
      this.label6.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label6.Texts")));
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(118, 31);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(70, 15);
      this.label2.TabIndex = 19;
      this.label2.TabStop = false;
      this.label2.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // lblIncome
      // 
      this.lblIncome.Location = new System.Drawing.Point(11, 32);
      this.lblIncome.Name = "lblIncome";
      this.lblIncome.Size = new System.Drawing.Size(19, 15);
      this.lblIncome.TabIndex = 20;
      this.lblIncome.TabStop = false;
      this.lblIncome.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblIncome.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblIncome.Texts")));
      // 
      // lblCompetitionDuration
      // 
      this.lblCompetitionDuration.Location = new System.Drawing.Point(11, 10);
      this.lblCompetitionDuration.Name = "lblCompetitionDuration";
      this.lblCompetitionDuration.Size = new System.Drawing.Size(45, 15);
      this.lblCompetitionDuration.TabIndex = 26;
      this.lblCompetitionDuration.TabStop = false;
      this.lblCompetitionDuration.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblCompetitionDuration.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblCompetitionDuration.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(131, 51);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(55, 15);
      this.label1.TabIndex = 21;
      this.label1.TabStop = false;
      this.label1.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(137, 10);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(50, 15);
      this.label4.TabIndex = 25;
      this.label4.TabStop = false;
      this.label4.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // lblBalance
      // 
      this.lblBalance.Location = new System.Drawing.Point(11, 92);
      this.lblBalance.Name = "lblBalance";
      this.lblBalance.Size = new System.Drawing.Size(19, 15);
      this.lblBalance.TabIndex = 24;
      this.lblBalance.TabStop = false;
      this.lblBalance.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblBalance.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBalance.Texts")));
      // 
      // lblExpenses
      // 
      this.lblExpenses.Location = new System.Drawing.Point(11, 51);
      this.lblExpenses.Name = "lblExpenses";
      this.lblExpenses.Size = new System.Drawing.Size(19, 15);
      this.lblExpenses.TabIndex = 22;
      this.lblExpenses.TabStop = false;
      this.lblExpenses.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblExpenses.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblExpenses.Texts")));
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(124, 92);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(63, 15);
      this.label3.TabIndex = 23;
      this.label3.TabStop = false;
      this.label3.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // toolBar2
      // 
      this.toolBar2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar2.BorderWidth = 0.5F;
      this.toolBar2.Controls.Add(this.label10);
      this.toolBar2.Controls.Add(this.label9);
      this.toolBar2.Controls.Add(this.dtEnd);
      this.toolBar2.Controls.Add(this.dtStart);
      this.toolBar2.Controls.Add(this.label8);
      this.toolBar2.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar2.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar2.Location = new System.Drawing.Point(231, 419);
      this.toolBar2.Name = "toolBar2";
      this.toolBar2.Size = new System.Drawing.Size(219, 116);
      this.toolBar2.TabIndex = 42;
      // 
      // label10
      // 
      this.label10.Location = new System.Drawing.Point(176, 85);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(14, 15);
      this.label10.TabIndex = 9;
      this.label10.TabStop = false;
      this.label10.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label10.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label10.Texts")));
      // 
      // label9
      // 
      this.label9.Location = new System.Drawing.Point(176, 38);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(15, 15);
      this.label9.TabIndex = 8;
      this.label9.TabStop = false;
      this.label9.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label9.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label9.Texts")));
      // 
      // dtEnd
      // 
      this.dtEnd.ActivateEvent = true;
      this.dtEnd.ForeColor = System.Drawing.Color.Black;
      this.dtEnd.Location = new System.Drawing.Point(16, 65);
      this.dtEnd.Name = "dtEnd";
      this.dtEnd.Size = new System.Drawing.Size(132, 43);
      this.dtEnd.TabIndex = 7;
      this.dtEnd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtEnd.Texts")));
      this.dtEnd.OnDateChanged += new System.EventHandler<System.EventArgs>(this.dtStart_OnDateChanged);
      // 
      // dtStart
      // 
      this.dtStart.ActivateEvent = true;
      this.dtStart.ForeColor = System.Drawing.Color.Black;
      this.dtStart.Location = new System.Drawing.Point(16, 17);
      this.dtStart.Name = "dtStart";
      this.dtStart.Size = new System.Drawing.Size(132, 43);
      this.dtStart.TabIndex = 6;
      this.dtStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtStart.Texts")));
      this.dtStart.OnDateChanged += new System.EventHandler<System.EventArgs>(this.dtStart_OnDateChanged);
      // 
      // label8
      // 
      this.label8.Location = new System.Drawing.Point(151, 8);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(55, 15);
      this.label8.TabIndex = 5;
      this.label8.TabStop = false;
      this.label8.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label8.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label8.Texts")));
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 0.5F;
      this.toolBar1.Controls.Add(this.chkCups);
      this.toolBar1.Controls.Add(this.chkEliminations);
      this.toolBar1.Controls.Add(this.chkLeagues);
      this.toolBar1.Controls.Add(this.label7);
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(110, 419);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(107, 116);
      this.toolBar1.TabIndex = 41;
      // 
      // chkCups
      // 
      this.chkCups.Checked = true;
      this.chkCups.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkCups.ForeColor = System.Drawing.Color.White;
      this.chkCups.Location = new System.Drawing.Point(26, 88);
      this.chkCups.Name = "chkCups";
      this.chkCups.Size = new System.Drawing.Size(57, 17);
      this.chkCups.TabIndex = 7;
      this.chkCups.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkCups.Texts")));
      this.chkCups.CheckedChanged += new System.EventHandler(this.chkLeagues_CheckedChanged);
      // 
      // chkEliminations
      // 
      this.chkEliminations.Checked = true;
      this.chkEliminations.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkEliminations.ForeColor = System.Drawing.Color.White;
      this.chkEliminations.Location = new System.Drawing.Point(14, 63);
      this.chkEliminations.Name = "chkEliminations";
      this.chkEliminations.Size = new System.Drawing.Size(69, 17);
      this.chkEliminations.TabIndex = 6;
      this.chkEliminations.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkEliminations.Texts")));
      this.chkEliminations.CheckedChanged += new System.EventHandler(this.chkLeagues_CheckedChanged);
      // 
      // chkLeagues
      // 
      this.chkLeagues.Checked = true;
      this.chkLeagues.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkLeagues.ForeColor = System.Drawing.Color.White;
      this.chkLeagues.Location = new System.Drawing.Point(25, 38);
      this.chkLeagues.Name = "chkLeagues";
      this.chkLeagues.Size = new System.Drawing.Size(58, 17);
      this.chkLeagues.TabIndex = 5;
      this.chkLeagues.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkLeagues.Texts")));
      this.chkLeagues.CheckedChanged += new System.EventHandler(this.chkLeagues_CheckedChanged);
      // 
      // label7
      // 
      this.label7.Location = new System.Drawing.Point(30, 11);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(71, 15);
      this.label7.TabIndex = 4;
      this.label7.TabStop = false;
      this.label7.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label7.Texts")));
      // 
      // tinypicker1
      // 
      this.tinypicker1.Location = new System.Drawing.Point(14, 549);
      this.tinypicker1.Name = "tinypicker1";
      this.tinypicker1.Size = new System.Drawing.Size(652, 38);
      this.tinypicker1.TabIndex = 40;
      this.tinypicker1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tinypicker1.Texts")));
      // 
      // tbButtons
      // 
      this.tbButtons.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbButtons.BorderWidth = 0.5F;
      this.tbButtons.Controls.Add(this.btnViewGames);
      this.tbButtons.Controls.Add(this.btnRankings);
      this.tbButtons.Controls.Add(this.tbLock);
      this.tbButtons.Controls.Add(this.btnSchedule);
      this.tbButtons.Controls.Add(this.btnStart);
      this.tbButtons.Controls.Add(this.btnEdit);
      this.tbButtons.Controls.Add(this.btnAdd);
      this.tbButtons.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbButtons.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbButtons.Location = new System.Drawing.Point(14, 12);
      this.tbButtons.Name = "tbButtons";
      this.tbButtons.Size = new System.Drawing.Size(83, 523);
      this.tbButtons.TabIndex = 39;
      // 
      // btnViewGames
      // 
      this.btnViewGames.Hint = "بازی های مسابقه";
      this.btnViewGames.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnViewGames.Image")));
      this.btnViewGames.Location = new System.Drawing.Point(10, 359);
      this.btnViewGames.Name = "btnViewGames";
      this.btnViewGames.NormalBorderColor = System.Drawing.Color.Gray;
      this.btnViewGames.Size = new System.Drawing.Size(64, 64);
      this.btnViewGames.TabIndex = 51;
      this.btnViewGames.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnViewGames.Texts")));
      this.btnViewGames.TransparentColor = System.Drawing.Color.White;
      this.btnViewGames.Click += new System.EventHandler(this.btnViewGames_Click);
      // 
      // btnRankings
      // 
      this.btnRankings.Hint = "آمار و نتایج مسابقه";
      this.btnRankings.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnRankings.Image")));
      this.btnRankings.Location = new System.Drawing.Point(10, 289);
      this.btnRankings.Name = "btnRankings";
      this.btnRankings.NormalBorderColor = System.Drawing.Color.Gray;
      this.btnRankings.Size = new System.Drawing.Size(64, 64);
      this.btnRankings.TabIndex = 50;
      this.btnRankings.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnRankings.Texts")));
      this.btnRankings.TransparentColor = System.Drawing.Color.White;
      this.btnRankings.Click += new System.EventHandler(this.btnRankings_Click);
      // 
      // tbLock
      // 
      this.tbLock.Hint = "قفل سیستم";
      this.tbLock.Image = ((System.Drawing.Bitmap)(resources.GetObject("tbLock.Image")));
      this.tbLock.Location = new System.Drawing.Point(10, 456);
      this.tbLock.Name = "tbLock";
      this.tbLock.NormalBorderColor = System.Drawing.Color.Gray;
      this.tbLock.Size = new System.Drawing.Size(64, 64);
      this.tbLock.TabIndex = 49;
      this.tbLock.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tbLock.Texts")));
      this.tbLock.TransparentColor = System.Drawing.Color.White;
      this.tbLock.Click += new System.EventHandler(this.tbLock_Click);
      // 
      // btnSchedule
      // 
      this.btnSchedule.Hint = "تاریخ بازی ها";
      this.btnSchedule.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnSchedule.Image")));
      this.btnSchedule.Location = new System.Drawing.Point(10, 217);
      this.btnSchedule.Name = "btnSchedule";
      this.btnSchedule.NormalBorderColor = System.Drawing.Color.Gray;
      this.btnSchedule.Size = new System.Drawing.Size(64, 64);
      this.btnSchedule.TabIndex = 48;
      this.btnSchedule.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnSchedule.Texts")));
      this.btnSchedule.TransparentColor = System.Drawing.Color.White;
      this.btnSchedule.Click += new System.EventHandler(this.btnSchedule_Click);
      // 
      // btnStart
      // 
      this.btnStart.Hint = "تصویب مسابقه";
      this.btnStart.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnStart.Image")));
      this.btnStart.Location = new System.Drawing.Point(10, 147);
      this.btnStart.Name = "btnStart";
      this.btnStart.NormalBorderColor = System.Drawing.Color.Gray;
      this.btnStart.Size = new System.Drawing.Size(64, 64);
      this.btnStart.TabIndex = 47;
      this.btnStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnStart.Texts")));
      this.btnStart.TransparentColor = System.Drawing.Color.White;
      this.btnStart.Click += new System.EventHandler(this.btnApply_Click);
      // 
      // btnEdit
      // 
      this.btnEdit.Hint = "ویرایش مسابقه";
      this.btnEdit.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnEdit.Image")));
      this.btnEdit.Location = new System.Drawing.Point(10, 77);
      this.btnEdit.Name = "btnEdit";
      this.btnEdit.NormalBorderColor = System.Drawing.Color.Gray;
      this.btnEdit.Size = new System.Drawing.Size(64, 64);
      this.btnEdit.TabIndex = 46;
      this.btnEdit.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnEdit.Texts")));
      this.btnEdit.TransparentColor = System.Drawing.Color.White;
      this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
      // 
      // btnAdd
      // 
      this.btnAdd.Hint = "ایجاد مسابقه";
      this.btnAdd.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnAdd.Image")));
      this.btnAdd.Location = new System.Drawing.Point(10, 8);
      this.btnAdd.Name = "btnAdd";
      this.btnAdd.NormalBorderColor = System.Drawing.Color.Gray;
      this.btnAdd.Size = new System.Drawing.Size(64, 64);
      this.btnAdd.TabIndex = 45;
      this.btnAdd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnAdd.Texts")));
      this.btnAdd.TransparentColor = System.Drawing.Color.White;
      this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
      // 
      // columnHeader7
      // 
      this.columnHeader7.Text = "باشگاه ها";
      this.columnHeader7.Width = 116;
      // 
      // ControlPanel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoScroll = true;
      this.BackColor = System.Drawing.Color.Black;
      this.ClientSize = new System.Drawing.Size(679, 592);
      this.Controls.Add(this.toolBar3);
      this.Controls.Add(this.toolBar2);
      this.Controls.Add(this.toolBar1);
      this.Controls.Add(this.tinypicker1);
      this.Controls.Add(this.tbButtons);
      this.Controls.Add(this.lvCompetitions);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "ControlPanel";
      this.Text = "مدیریت مسابقات";
      this.Load += new System.EventHandler(this.ControlPanel_Load);
      this.toolBar3.ResumeLayout(false);
      this.toolBar2.ResumeLayout(false);
      this.toolBar1.ResumeLayout(false);
      this.tbButtons.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ListView lvCompetitions;
    private System.Windows.Forms.ColumnHeader columnHeader1;
    private System.Windows.Forms.ColumnHeader columnHeader2;
    private System.Windows.Forms.ColumnHeader columnHeader3;
    private System.Windows.Forms.ColumnHeader columnHeader4;
    private System.Windows.Forms.ColumnHeader columnHeader5;
    private System.Windows.Forms.ColumnHeader columnHeader6;
    private ToolBar tbButtons;
    private TinyPicker tinypicker1;
    private ToolBar toolBar1;
    private TransparentCheckBox chkCups;
    private TransparentCheckBox chkEliminations;
    private TransparentCheckBox chkLeagues;
    private TransparentLabel label7;
    private ToolBar toolBar2;
    private TransparentLabel label10;
    private TransparentLabel label9;
    private PersianDatePicker dtEnd;
    private PersianDatePicker dtStart;
    private TransparentLabel label8;
    private ToolBar toolBar3;
    private TransparentLabel label6;
    private TransparentLabel label2;
    private TransparentLabel lblIncome;
    private TransparentLabel lblCompetitionDuration;
    private TransparentLabel label1;
    private TransparentLabel label4;
    private TransparentLabel lblBalance;
    private TransparentLabel lblExpenses;
    private TransparentLabel label3;
    private ToolButton btnAdd;
    private ToolButton tbLock;
    private ToolButton btnSchedule;
    private ToolButton btnStart;
    private ToolButton btnEdit;
    private ToolButton btnRankings;
    private ToolButton btnViewGames;
    private System.Windows.Forms.ColumnHeader columnHeader7;

  }
}