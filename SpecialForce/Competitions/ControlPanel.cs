﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SpecialForce.Competitions
{
  public partial class ControlPanel : SessionAwareForm
  {
    private List<DB.Competition> mCompetitions = new List<DB.Competition>();
    private DB.Competition mCurrentCompetition = null;

    static private ControlPanel mInstance = null;
    static public ControlPanel Instance
    {
      get
      {
        if (mInstance == null || mInstance.IsDisposed)
          mInstance = new ControlPanel();

        return mInstance;
      }
    }

    private ControlPanel()
    {
      InitializeComponent();
    }

    private void UpdateList()
    {
      if (Session.DevelopMode)
        return;

      mCompetitions.Clear();
      lvCompetitions.Items.Clear();

      HashSet<CompetitionType> types = new HashSet<CompetitionType>();
      List<DB.Competition> comps = DB.CompetitionManager.AllCompetitions;

      // add types to the set
      if(chkCups.Checked)
        types.Add(CompetitionType.Cup);
      if(chkEliminations.Checked)
        types.Add(CompetitionType.Elimination);
      if(chkLeagues.Checked)
        types.Add(CompetitionType.League);

      int id = 0;
      foreach(DB.Competition c in comps)
      {
        // filter based on type
        if (!types.Contains(c.Type)) 
          continue;

        // filter based on date
        if (c.Start != null && c.Start < dtStart.Date)
          continue;

        ++id;
        mCompetitions.Add(c);
        ListViewItem it = new ListViewItem(id.ToString());
        it.SubItems.Add(c.Title);
        it.SubItems.Add(Enums.ToString(c.Type));
        it.SubItems.Add(Enums.ToString(c.State));
        it.SubItems.Add(c.Start == null ? "تنظیم نشده" : c.Start.ToString(DateTimeString.CompactDate));
        it.SubItems.Add(c.End == null ? "تنظیم نشده" : c.End.ToString(DateTimeString.CompactDate));
        it.SubItems.Add(c.UsedSiteIDs.Count == 0 ? "تنظیم نشده" : String.Join("،", c.UsedSiteIDs.Select(site_id => DB.Site.GetSiteName(site_id))));
        lvCompetitions.Items.Add(it);
      }
    }

    private void btnAdd_Click(object sender, EventArgs e)
    {
      if(ShowModal(new Management(), false) == DialogResult.OK)
        UpdateList();
    }

    private void btnEdit_Click(object sender, EventArgs e)
    {
      if (mCurrentCompetition == null)
        Session.ShowError("هیچ مسابقه ای انتخاب نشده است.");
      else if (mCurrentCompetition.State != CompetitionState.Editing)
        Session.ShowError("مسابقه تایید نهایی شده است.");
      else
      {
        Management frm = new Management();
        frm.Competition = mCurrentCompetition;
        if (ShowModal(frm, false) == DialogResult.OK)
          UpdateList();
      }
    }

    private void btnApply_Click(object sender, EventArgs e)
    {
      int league_min = Session.ReleaseBuild ? 4 : 2;
      int cup_min = Session.ReleaseBuild ? 8 : 4;

      if (mCurrentCompetition == null)
        Session.ShowError("هیچ مسابقه ای انتخاب نشده است.");
      else if (mCurrentCompetition.State != CompetitionState.Editing)
        Session.ShowError("مسابقه تایید نهایی شده است.");
      else if (mCurrentCompetition.TeamIDs.Count < league_min && mCurrentCompetition.Type == CompetitionType.League)
        Session.ShowMessage("تعداد تیم های یک لیگ  کمتر از 4 نمی تواند باشد.");
      else if (mCurrentCompetition.TeamIDs.Count < cup_min && mCurrentCompetition.Type != CompetitionType.League)
        Session.ShowMessage("تعداد تیم های یک مسابقه حذفی و یا کاپ نمی تواند کمتر از 8 باشد.");
      else
      {
        if (mCurrentCompetition.Type == CompetitionType.Cup)
        {
          FormCupGroups frm = new FormCupGroups();
          frm.Competition = mCurrentCompetition;
          if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            return;
        }

        string str = "برنامه برای تایید و ثبت مسابقه آماده است.";
        str += Environment.NewLine;
        str += "ثبت انجام شود؟";
        if (!Session.Ask(str, "پیام"))
          return;

        try
        {
          Application.UseWaitCursor = true;
          Application.DoEvents();

          mCurrentCompetition.Initialize();
          UpdateList();
        }
        catch (Exception ex)
        {
          Logger.LogException(ex, "Competitions.ControlPanel", "Initializing competition");
          MessageBox.Show("خطایی در ثبت مسابقه پیش آمد.");
        }
        finally
        {
          Application.UseWaitCursor = false;
        }
      }
    }

    private void btnSchedule_Click(object sender, EventArgs e)
    {
      if (mCurrentCompetition == null)
        Session.ShowError("هیچ مسابقه ای انتخاب نشده است.");
      else if (mCurrentCompetition.State == CompetitionState.Editing)
        Session.ShowError("مسابقه تایید نهایی نشده است.");
      else
      {
        FormViewCompetition frm = new FormViewCompetition();
        frm.Competition = mCurrentCompetition;
        ShowModal(frm, false);
        UpdateList();
      }
    }

    private void lvCompetitions_SelectedIndexChanged(object sender, EventArgs e)
    {
      int index =
        lvCompetitions.SelectedIndices.Count != 0 ?
        lvCompetitions.SelectedIndices[0] :
        -1;

      mCurrentCompetition = 
        index == -1 ? 
        null : 
        mCompetitions[index];

      if (mCurrentCompetition == null ||
          mCurrentCompetition.Start == null ||
          mCurrentCompetition.End == null ||
          !Session.CurrentStaff.Post.IsPrivileged())
      {
        lblCompetitionDuration.Text = "-";
        lblExpenses.Text = "-";
        lblIncome.Text = "-";
        lblBalance.Text = "-";
      }
      else
      {
        double total_hours = 0;
        foreach (DB.Game g in mCurrentCompetition.Games)
          total_hours += g.Finish.GetTimeDifference(g.Start) / 3600.0;

        int hours = (int)total_hours;
        lblCompetitionDuration.Text = hours.ToString("D2") + ":" + TinyMath.Round((total_hours - hours) * 60).ToString("D2") + " ساعت";

        double income = mCurrentCompetition.TeamIDs.Count * mCurrentCompetition.AdmissionFee;
        lblIncome.Text = income.ToCurrencyString() + " تومان";

        double expense = DB.Invoice.GetCompetitionExpense(mCurrentCompetition);
        lblExpenses.Text = expense.ToCurrencyString() + " تومان";
        lblBalance.Text = (income - expense).ToCurrencyString() + " تومان";
      }
    }

    private void ControlPanel_Load(object sender, EventArgs e)
    {
      dtStart.Date = PersianDateTime.Date(1390, 1, 1);
      dtEnd.Date = PersianDateTime.Date(1400, 1, 1);

      UpdateList();
    }

    private void chkLeagues_CheckedChanged(object sender, EventArgs e)
    {
      UpdateList();
    }

    private void dtStart_OnDateChanged(object sender, EventArgs e)
    {
      UpdateList();
    }

    private void lvCompetitions_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      if (mCurrentCompetition.State == CompetitionState.Editing)
        btnEdit_Click(sender, e);
      else
        btnSchedule_Click(sender, e);
    }

    protected override void ApplyPermissions(Post post)
    {
      btnAdd.Enabled = post.IsPrivileged();
      btnStart.Enabled = post.IsPrivileged();
      btnSchedule.Enabled = post.IsPrivileged();
    }

    private void tbLock_Click(object sender, EventArgs e)
    {
      Session.Lock();
    }

    private void btnRankings_Click(object sender, EventArgs e)
    {
      if (mCurrentCompetition == null)
        Session.ShowError("هیچ مسابقه ای انتخاب نشده است.");
      else if (mCurrentCompetition.State == CompetitionState.Editing)
        Session.ShowError("مسابقه تایید نهایی نشده است.");
      else
      {
        FormCompetitionReport frm = new FormCompetitionReport();
        frm.Competition = mCurrentCompetition;
        ShowModal(frm, false);
      }
    }

    private void btnViewGames_Click(object sender, EventArgs e)
    {
      if (mCurrentCompetition == null)
        Session.ShowError("هیچ مسابقه ای انتخاب نشده است.");
      else if (mCurrentCompetition.State == CompetitionState.Editing)
        Session.ShowError("مسابقه تایید نهایی نشده است.");
      else
      {
        Scheduling.FormGameReport frm = new Scheduling.FormGameReport();
        frm.Games = mCurrentCompetition.Games;
        frm.ShortState = true;
        frm.Title = "گزارش بازی های مسابقه " + mCurrentCompetition.Title + " (" + Enums.ToString(mCurrentCompetition.Type) + ")";
        ShowModal(frm, false);
      }
    }
  }
}
