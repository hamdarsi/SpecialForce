﻿namespace SpecialForce.Competitions
{
  partial class FormCompetitionReport
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCompetitionReport));
      this.report1 = new SpecialForce.Reports.Report();
      this.rgCupViewMode = new SpecialForce.UserInterface.RadioGroup();
      this.rgGroups = new SpecialForce.UserInterface.RadioGroup();
      this.SuspendLayout();
      // 
      // report1
      // 
      this.report1.BackColor = System.Drawing.Color.White;
      this.report1.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.report1.ForeColor = System.Drawing.Color.Black;
      this.report1.Location = new System.Drawing.Point(0, 0);
      this.report1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.report1.MinimumSize = new System.Drawing.Size(0, 400);
      this.report1.Name = "report1";
      this.report1.Size = new System.Drawing.Size(755, 678);
      this.report1.TabIndex = 0;
      this.report1.Title = "label2";
      // 
      // rgCupViewMode
      // 
      this.rgCupViewMode.BackColor = System.Drawing.Color.White;
      this.rgCupViewMode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.rgCupViewMode.Location = new System.Drawing.Point(527, 39);
      this.rgCupViewMode.Name = "rgCupViewMode";
      this.rgCupViewMode.SelectedIndex = -1;
      this.rgCupViewMode.Size = new System.Drawing.Size(64, 64);
      this.rgCupViewMode.TabIndex = 1;
      this.rgCupViewMode.Visible = false;
      this.rgCupViewMode.Click += new System.EventHandler<System.EventArgs>(this.rgCupViewMode_Click);
      // 
      // rgGroups
      // 
      this.rgGroups.BackColor = System.Drawing.Color.White;
      this.rgGroups.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.rgGroups.Location = new System.Drawing.Point(527, 109);
      this.rgGroups.Name = "rgGroups";
      this.rgGroups.SelectedIndex = -1;
      this.rgGroups.Size = new System.Drawing.Size(64, 64);
      this.rgGroups.TabIndex = 2;
      this.rgGroups.Visible = false;
      this.rgGroups.Click += new System.EventHandler<System.EventArgs>(this.rgGroups_Click);
      // 
      // FormCompetitionReport
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoScroll = true;
      this.BackColor = System.Drawing.Color.White;
      this.ClientSize = new System.Drawing.Size(776, 707);
      this.Controls.Add(this.rgGroups);
      this.Controls.Add(this.rgCupViewMode);
      this.Controls.Add(this.report1);
      this.EmptyBackground = true;
      this.ForeColor = System.Drawing.Color.Black;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormCompetitionReport";
      this.Text = "گزارش مسابقه";
      this.Shown += new System.EventHandler(this.FormCompetitionReport_Shown);
      this.ResumeLayout(false);

    }

    #endregion

    private Reports.Report report1;
    private UserInterface.RadioGroup rgCupViewMode;
    private UserInterface.RadioGroup rgGroups;

  }
}