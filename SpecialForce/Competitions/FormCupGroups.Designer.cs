﻿namespace SpecialForce.Competitions
{
  partial class FormCupGroups
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCupGroups));
      this.btnRemoveTeam = new SpecialForce.ToolButton();
      this.btnOK = new SpecialForce.ToolButton();
      this.lblGroups = new SpecialForce.TransparentLabel();
      this.lvTeams = new System.Windows.Forms.ListView();
      this.label1 = new SpecialForce.TransparentLabel();
      this.rgGroupCount = new SpecialForce.UserInterface.RadioGroup();
      this.SuspendLayout();
      // 
      // btnRemoveTeam
      // 
      this.btnRemoveTeam.Hint = "حذف تیم";
      this.btnRemoveTeam.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnRemoveTeam.Image")));
      this.btnRemoveTeam.Location = new System.Drawing.Point(426, 33);
      this.btnRemoveTeam.Name = "btnRemoveTeam";
      this.btnRemoveTeam.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnRemoveTeam.Size = new System.Drawing.Size(64, 64);
      this.btnRemoveTeam.TabIndex = 13;
      this.btnRemoveTeam.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnRemoveTeam.Texts")));
      this.btnRemoveTeam.TransparentColor = System.Drawing.Color.White;
      this.btnRemoveTeam.Click += new System.EventHandler(this.tbRemoveTeam_Click);
      // 
      // btnOK
      // 
      this.btnOK.Hint = "تایید گروه بندی";
      this.btnOK.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnOK.Image")));
      this.btnOK.Location = new System.Drawing.Point(496, 33);
      this.btnOK.Name = "btnOK";
      this.btnOK.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnOK.Size = new System.Drawing.Size(64, 64);
      this.btnOK.TabIndex = 11;
      this.btnOK.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnOK.Texts")));
      this.btnOK.TransparentColor = System.Drawing.Color.White;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // lblGroups
      // 
      this.lblGroups.Location = new System.Drawing.Point(245, 12);
      this.lblGroups.Name = "lblGroups";
      this.lblGroups.Size = new System.Drawing.Size(76, 15);
      this.lblGroups.TabIndex = 11;
      this.lblGroups.TabStop = false;
      this.lblGroups.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblGroups.Texts")));
      // 
      // lvTeams
      // 
      this.lvTeams.AllowDrop = true;
      this.lvTeams.Location = new System.Drawing.Point(12, 33);
      this.lvTeams.Name = "lvTeams";
      this.lvTeams.RightToLeftLayout = true;
      this.lvTeams.Size = new System.Drawing.Size(189, 532);
      this.lvTeams.TabIndex = 7;
      this.lvTeams.UseCompatibleStateImageBehavior = false;
      this.lvTeams.View = System.Windows.Forms.View.List;
      this.lvTeams.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.lvTeams_ItemDrag);
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(12, 12);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(70, 15);
      this.label1.TabIndex = 10;
      this.label1.TabStop = false;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // rgGroupCount
      // 
      this.rgGroupCount.BackColor = System.Drawing.Color.White;
      this.rgGroupCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.rgGroupCount.IsHorizontal = true;
      this.rgGroupCount.Location = new System.Drawing.Point(334, 33);
      this.rgGroupCount.Name = "rgGroupCount";
      this.rgGroupCount.SelectedIndex = -1;
      this.rgGroupCount.Size = new System.Drawing.Size(64, 64);
      this.rgGroupCount.TabIndex = 14;
      this.rgGroupCount.Click += new System.EventHandler<System.EventArgs>(this.rgGroupCount_Click);
      // 
      // FormCupGroups
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(658, 577);
      this.Controls.Add(this.rgGroupCount);
      this.Controls.Add(this.btnRemoveTeam);
      this.Controls.Add(this.btnOK);
      this.Controls.Add(this.lblGroups);
      this.Controls.Add(this.lvTeams);
      this.Controls.Add(this.label1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormCupGroups";
      this.Text = "ساخت یا ویرایش مسابقه";
      this.Load += new System.EventHandler(this.FormCupGroups_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ListView lvTeams;
    private SpecialForce.TransparentLabel label1;
    private SpecialForce.TransparentLabel lblGroups;
    private ToolButton btnOK;
    private ToolButton btnRemoveTeam;
    private UserInterface.RadioGroup rgGroupCount;
  }
}