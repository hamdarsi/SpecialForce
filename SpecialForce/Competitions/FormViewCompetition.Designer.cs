﻿namespace SpecialForce.Competitions
{
  partial class FormViewCompetition
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormViewCompetition));
      this.mnuChart = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mnuSchedule = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuDelete = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuSave = new System.Windows.Forms.ToolStripMenuItem();
      this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
      this.mnuChart.SuspendLayout();
      this.SuspendLayout();
      // 
      // mnuChart
      // 
      this.mnuChart.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSchedule,
            this.mnuDelete,
            this.mnuSave});
      this.mnuChart.Name = "contextMenuStrip1";
      this.mnuChart.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      this.mnuChart.Size = new System.Drawing.Size(201, 92);
      this.mnuChart.Opening += new System.ComponentModel.CancelEventHandler(this.mnuChart_Opening);
      // 
      // mnuSchedule
      // 
      this.mnuSchedule.Name = "mnuSchedule";
      this.mnuSchedule.Size = new System.Drawing.Size(200, 22);
      this.mnuSchedule.Text = "تنظیم تاریخ و زمان بازی ها";
      this.mnuSchedule.Click += new System.EventHandler(this.mnuSchedule_Click);
      // 
      // mnuDelete
      // 
      this.mnuDelete.Name = "mnuDelete";
      this.mnuDelete.Size = new System.Drawing.Size(200, 22);
      this.mnuDelete.Text = "حذف زمان بندی بازی";
      this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
      // 
      // mnuSave
      // 
      this.mnuSave.Name = "mnuSave";
      this.mnuSave.Size = new System.Drawing.Size(200, 22);
      this.mnuSave.Text = "ذخیره جدول در فایل";
      this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
      // 
      // saveFileDialog1
      // 
      this.saveFileDialog1.Filter = "Portable Network Graphics|*.png";
      this.saveFileDialog1.RestoreDirectory = true;
      this.saveFileDialog1.Title = "محل ذخیره را انتخاب کنید";
      // 
      // FormViewCompetition
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoScroll = true;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.ClientSize = new System.Drawing.Size(284, 229);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormViewCompetition";
      this.Text = "زمان بندی بازی ها";
      this.Shown += new System.EventHandler(this.FormViewCompetition_Shown);
      this.mnuChart.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ContextMenuStrip mnuChart;
    private System.Windows.Forms.ToolStripMenuItem mnuSchedule;
    private System.Windows.Forms.ToolStripMenuItem mnuDelete;
    private System.Windows.Forms.ToolStripMenuItem mnuSave;
    private System.Windows.Forms.SaveFileDialog saveFileDialog1;
  }
}