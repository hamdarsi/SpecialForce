﻿namespace SpecialForce.Competitions
{
  partial class Management
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Management));
      this.label2 = new SpecialForce.TransparentLabel();
      this.lvTeams = new System.Windows.Forms.ListView();
      this.mnuTeams = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mnuDeleteTeam = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
      this.mnuLargeIcons = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuSmallIcons = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuList = new System.Windows.Forms.ToolStripMenuItem();
      this.btnOK = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.grpCompetitionSettings = new SpecialForce.ToolBar();
      this.label3 = new SpecialForce.TransparentLabel();
      this.txtTitle = new System.Windows.Forms.TextBox();
      this.label5 = new SpecialForce.TransparentLabel();
      this.txtPrice = new SpecialForce.Utilities.NumberBox();
      this.label6 = new SpecialForce.TransparentLabel();
      this.cmbRoundRobin = new System.Windows.Forms.ComboBox();
      this.lblRoundRobin = new SpecialForce.TransparentLabel();
      this.cmbCompetitionType = new System.Windows.Forms.ComboBox();
      this.grpGameSettings = new SpecialForce.ToolBar();
      this.cmbFriendlyFire = new System.Windows.Forms.ComboBox();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.txtRestTime = new System.Windows.Forms.NumericUpDown();
      this.lblAmmoPerMagazine = new SpecialForce.TransparentLabel();
      this.lblMagazineCount = new SpecialForce.TransparentLabel();
      this.label7 = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.lblBombTimer = new SpecialForce.TransparentLabel();
      this.lblHP = new SpecialForce.TransparentLabel();
      this.label4 = new SpecialForce.TransparentLabel();
      this.lblRoundTime = new SpecialForce.TransparentLabel();
      this.txtGameTime = new System.Windows.Forms.NumericUpDown();
      this.txtBombTimer = new System.Windows.Forms.NumericUpDown();
      this.txtRoundCount = new System.Windows.Forms.NumericUpDown();
      this.txtRoundTime = new System.Windows.Forms.NumericUpDown();
      this.txtMagazineCount = new System.Windows.Forms.NumericUpDown();
      this.txtAmmoPerMagazine = new System.Windows.Forms.NumericUpDown();
      this.txtHP = new System.Windows.Forms.NumericUpDown();
      this.cmbGameObject = new System.Windows.Forms.ComboBox();
      this.btnTeamRemove = new SpecialForce.ToolButton();
      this.btnTeamAdd = new SpecialForce.ToolButton();
      this.btnExit = new SpecialForce.ToolButton();
      this.mnuTeams.SuspendLayout();
      this.grpCompetitionSettings.SuspendLayout();
      this.grpGameSettings.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtRestTime)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtGameTime)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtBombTimer)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtRoundCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtRoundTime)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtMagazineCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtAmmoPerMagazine)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtHP)).BeginInit();
      this.SuspendLayout();
      // 
      // label2
      // 
      this.label2.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label2.Location = new System.Drawing.Point(264, 56);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(68, 24);
      this.label2.TabIndex = 12;
      this.label2.TabStop = false;
      this.label2.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // lvTeams
      // 
      this.lvTeams.AllowDrop = true;
      this.lvTeams.ContextMenuStrip = this.mnuTeams;
      this.lvTeams.Enabled = false;
      this.lvTeams.Location = new System.Drawing.Point(264, 89);
      this.lvTeams.Name = "lvTeams";
      this.lvTeams.RightToLeftLayout = true;
      this.lvTeams.Size = new System.Drawing.Size(350, 390);
      this.lvTeams.TabIndex = 13;
      this.lvTeams.UseCompatibleStateImageBehavior = false;
      this.lvTeams.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvTeams_DragDrop);
      this.lvTeams.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvTeams_DragEnter);
      // 
      // mnuTeams
      // 
      this.mnuTeams.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDeleteTeam,
            this.toolStripMenuItem1,
            this.mnuLargeIcons,
            this.mnuSmallIcons,
            this.mnuList});
      this.mnuTeams.Name = "contextMenuStrip1";
      this.mnuTeams.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      this.mnuTeams.Size = new System.Drawing.Size(190, 98);
      this.mnuTeams.Opening += new System.ComponentModel.CancelEventHandler(this.mnuTeams_Opening);
      // 
      // mnuDeleteTeam
      // 
      this.mnuDeleteTeam.Name = "mnuDeleteTeam";
      this.mnuDeleteTeam.Size = new System.Drawing.Size(189, 22);
      this.mnuDeleteTeam.Text = "حذف تیم های انتخاب شده";
      this.mnuDeleteTeam.Click += new System.EventHandler(this.mnuDeleteTeam_Click);
      // 
      // toolStripMenuItem1
      // 
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.Size = new System.Drawing.Size(186, 6);
      // 
      // mnuLargeIcons
      // 
      this.mnuLargeIcons.Name = "mnuLargeIcons";
      this.mnuLargeIcons.Size = new System.Drawing.Size(189, 22);
      this.mnuLargeIcons.Text = "نمایش بزرگ";
      this.mnuLargeIcons.Click += new System.EventHandler(this.mnuLargeIcons_Click);
      // 
      // mnuSmallIcons
      // 
      this.mnuSmallIcons.Name = "mnuSmallIcons";
      this.mnuSmallIcons.Size = new System.Drawing.Size(189, 22);
      this.mnuSmallIcons.Text = "نمایش کوچک";
      this.mnuSmallIcons.Click += new System.EventHandler(this.mnuSmallIcons_Click);
      // 
      // mnuList
      // 
      this.mnuList.Name = "mnuList";
      this.mnuList.Size = new System.Drawing.Size(189, 22);
      this.mnuList.Text = "نمایش لیست";
      this.mnuList.Click += new System.EventHandler(this.mnuList_Click);
      // 
      // btnOK
      // 
      this.btnOK.ForeColor = System.Drawing.Color.Black;
      this.btnOK.Location = new System.Drawing.Point(177, 456);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 14;
      this.btnOK.Text = "تایید";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(96, 456);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 13;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // grpCompetitionSettings
      // 
      this.grpCompetitionSettings.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.grpCompetitionSettings.BorderWidth = 0.5F;
      this.grpCompetitionSettings.Controls.Add(this.label3);
      this.grpCompetitionSettings.Controls.Add(this.txtTitle);
      this.grpCompetitionSettings.Controls.Add(this.label5);
      this.grpCompetitionSettings.Controls.Add(this.txtPrice);
      this.grpCompetitionSettings.Controls.Add(this.label6);
      this.grpCompetitionSettings.Controls.Add(this.cmbRoundRobin);
      this.grpCompetitionSettings.Controls.Add(this.lblRoundRobin);
      this.grpCompetitionSettings.Controls.Add(this.cmbCompetitionType);
      this.grpCompetitionSettings.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.grpCompetitionSettings.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.grpCompetitionSettings.Location = new System.Drawing.Point(16, 19);
      this.grpCompetitionSettings.Name = "grpCompetitionSettings";
      this.grpCompetitionSettings.Size = new System.Drawing.Size(236, 130);
      this.grpCompetitionSettings.TabIndex = 42;
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(165, 46);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(63, 15);
      this.label3.TabIndex = 34;
      this.label3.TabStop = false;
      this.label3.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // txtTitle
      // 
      this.txtTitle.Location = new System.Drawing.Point(7, 16);
      this.txtTitle.Name = "txtTitle";
      this.txtTitle.Size = new System.Drawing.Size(89, 21);
      this.txtTitle.TabIndex = 0;
      // 
      // label5
      // 
      this.label5.Location = new System.Drawing.Point(203, 19);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(24, 15);
      this.label5.TabIndex = 37;
      this.label5.TabStop = false;
      this.label5.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label5.Texts")));
      // 
      // txtPrice
      // 
      this.txtPrice.Location = new System.Drawing.Point(7, 97);
      this.txtPrice.Mode = SpecialForce.Utilities.NumberBox.NumberMode.Currency;
      this.txtPrice.Name = "txtPrice";
      this.txtPrice.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtPrice.Size = new System.Drawing.Size(89, 21);
      this.txtPrice.TabIndex = 3;
      this.txtPrice.Text = "$0.00";
      // 
      // label6
      // 
      this.label6.Location = new System.Drawing.Point(157, 100);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(72, 15);
      this.label6.TabIndex = 36;
      this.label6.TabStop = false;
      this.label6.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label6.Texts")));
      // 
      // cmbRoundRobin
      // 
      this.cmbRoundRobin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbRoundRobin.FormattingEnabled = true;
      this.cmbRoundRobin.Items.AddRange(new object[] {
            "بله",
            "خیر"});
      this.cmbRoundRobin.Location = new System.Drawing.Point(7, 70);
      this.cmbRoundRobin.Name = "cmbRoundRobin";
      this.cmbRoundRobin.Size = new System.Drawing.Size(89, 21);
      this.cmbRoundRobin.TabIndex = 2;
      // 
      // lblRoundRobin
      // 
      this.lblRoundRobin.Location = new System.Drawing.Point(154, 73);
      this.lblRoundRobin.Name = "lblRoundRobin";
      this.lblRoundRobin.Size = new System.Drawing.Size(75, 15);
      this.lblRoundRobin.TabIndex = 35;
      this.lblRoundRobin.TabStop = false;
      this.lblRoundRobin.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblRoundRobin.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblRoundRobin.Texts")));
      // 
      // cmbCompetitionType
      // 
      this.cmbCompetitionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbCompetitionType.FormattingEnabled = true;
      this.cmbCompetitionType.Location = new System.Drawing.Point(7, 43);
      this.cmbCompetitionType.Name = "cmbCompetitionType";
      this.cmbCompetitionType.Size = new System.Drawing.Size(89, 21);
      this.cmbCompetitionType.TabIndex = 1;
      this.cmbCompetitionType.SelectedIndexChanged += new System.EventHandler(this.cmbCompetitionType_SelectedIndexChanged);
      // 
      // grpGameSettings
      // 
      this.grpGameSettings.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.grpGameSettings.BorderWidth = 0.5F;
      this.grpGameSettings.Controls.Add(this.cmbFriendlyFire);
      this.grpGameSettings.Controls.Add(this.transparentLabel2);
      this.grpGameSettings.Controls.Add(this.transparentLabel1);
      this.grpGameSettings.Controls.Add(this.txtRestTime);
      this.grpGameSettings.Controls.Add(this.lblAmmoPerMagazine);
      this.grpGameSettings.Controls.Add(this.lblMagazineCount);
      this.grpGameSettings.Controls.Add(this.label7);
      this.grpGameSettings.Controls.Add(this.label1);
      this.grpGameSettings.Controls.Add(this.lblBombTimer);
      this.grpGameSettings.Controls.Add(this.lblHP);
      this.grpGameSettings.Controls.Add(this.label4);
      this.grpGameSettings.Controls.Add(this.lblRoundTime);
      this.grpGameSettings.Controls.Add(this.txtGameTime);
      this.grpGameSettings.Controls.Add(this.txtBombTimer);
      this.grpGameSettings.Controls.Add(this.txtRoundCount);
      this.grpGameSettings.Controls.Add(this.txtRoundTime);
      this.grpGameSettings.Controls.Add(this.txtMagazineCount);
      this.grpGameSettings.Controls.Add(this.txtAmmoPerMagazine);
      this.grpGameSettings.Controls.Add(this.txtHP);
      this.grpGameSettings.Controls.Add(this.cmbGameObject);
      this.grpGameSettings.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.grpGameSettings.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.grpGameSettings.Location = new System.Drawing.Point(15, 159);
      this.grpGameSettings.Name = "grpGameSettings";
      this.grpGameSettings.Size = new System.Drawing.Size(236, 279);
      this.grpGameSettings.TabIndex = 43;
      // 
      // cmbFriendlyFire
      // 
      this.cmbFriendlyFire.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbFriendlyFire.FormattingEnabled = true;
      this.cmbFriendlyFire.Items.AddRange(new object[] {
            "بله",
            "خیر"});
      this.cmbFriendlyFire.Location = new System.Drawing.Point(7, 247);
      this.cmbFriendlyFire.Name = "cmbFriendlyFire";
      this.cmbFriendlyFire.Size = new System.Drawing.Size(46, 21);
      this.cmbFriendlyFire.TabIndex = 39;
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Location = new System.Drawing.Point(157, 248);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(64, 15);
      this.transparentLabel2.TabIndex = 38;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Location = new System.Drawing.Point(99, 117);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(129, 15);
      this.transparentLabel1.TabIndex = 53;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // txtRestTime
      // 
      this.txtRestTime.Location = new System.Drawing.Point(7, 116);
      this.txtRestTime.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
      this.txtRestTime.Name = "txtRestTime";
      this.txtRestTime.Size = new System.Drawing.Size(46, 21);
      this.txtRestTime.TabIndex = 8;
      this.txtRestTime.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
      // 
      // lblAmmoPerMagazine
      // 
      this.lblAmmoPerMagazine.Location = new System.Drawing.Point(119, 221);
      this.lblAmmoPerMagazine.Name = "lblAmmoPerMagazine";
      this.lblAmmoPerMagazine.Size = new System.Drawing.Size(106, 15);
      this.lblAmmoPerMagazine.TabIndex = 45;
      this.lblAmmoPerMagazine.TabStop = false;
      this.lblAmmoPerMagazine.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblAmmoPerMagazine.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblAmmoPerMagazine.Texts")));
      // 
      // lblMagazineCount
      // 
      this.lblMagazineCount.Location = new System.Drawing.Point(144, 195);
      this.lblMagazineCount.Name = "lblMagazineCount";
      this.lblMagazineCount.Size = new System.Drawing.Size(81, 15);
      this.lblMagazineCount.TabIndex = 44;
      this.lblMagazineCount.TabStop = false;
      this.lblMagazineCount.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblMagazineCount.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblMagazineCount.Texts")));
      // 
      // label7
      // 
      this.label7.Location = new System.Drawing.Point(172, 39);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(52, 15);
      this.label7.TabIndex = 51;
      this.label7.TabStop = false;
      this.label7.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label7.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(191, 11);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(31, 15);
      this.label1.TabIndex = 50;
      this.label1.TabStop = false;
      this.label1.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // lblBombTimer
      // 
      this.lblBombTimer.Enabled = false;
      this.lblBombTimer.Location = new System.Drawing.Point(141, 143);
      this.lblBombTimer.Name = "lblBombTimer";
      this.lblBombTimer.Size = new System.Drawing.Size(83, 15);
      this.lblBombTimer.TabIndex = 49;
      this.lblBombTimer.TabStop = false;
      this.lblBombTimer.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblBombTimer.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBombTimer.Texts")));
      // 
      // lblHP
      // 
      this.lblHP.Location = new System.Drawing.Point(141, 169);
      this.lblHP.Name = "lblHP";
      this.lblHP.Size = new System.Drawing.Size(82, 15);
      this.lblHP.TabIndex = 48;
      this.lblHP.TabStop = false;
      this.lblHP.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblHP.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblHP.Texts")));
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(162, 65);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(61, 15);
      this.label4.TabIndex = 47;
      this.label4.TabStop = false;
      this.label4.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // lblRoundTime
      // 
      this.lblRoundTime.Location = new System.Drawing.Point(136, 91);
      this.lblRoundTime.Name = "lblRoundTime";
      this.lblRoundTime.Size = new System.Drawing.Size(89, 15);
      this.lblRoundTime.TabIndex = 46;
      this.lblRoundTime.TabStop = false;
      this.lblRoundTime.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblRoundTime.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblRoundTime.Texts")));
      // 
      // txtGameTime
      // 
      this.txtGameTime.Location = new System.Drawing.Point(7, 38);
      this.txtGameTime.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
      this.txtGameTime.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
      this.txtGameTime.Name = "txtGameTime";
      this.txtGameTime.Size = new System.Drawing.Size(46, 21);
      this.txtGameTime.TabIndex = 5;
      this.txtGameTime.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
      // 
      // txtBombTimer
      // 
      this.txtBombTimer.Enabled = false;
      this.txtBombTimer.Location = new System.Drawing.Point(7, 142);
      this.txtBombTimer.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
      this.txtBombTimer.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
      this.txtBombTimer.Name = "txtBombTimer";
      this.txtBombTimer.Size = new System.Drawing.Size(46, 21);
      this.txtBombTimer.TabIndex = 9;
      this.txtBombTimer.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
      // 
      // txtRoundCount
      // 
      this.txtRoundCount.Location = new System.Drawing.Point(7, 64);
      this.txtRoundCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtRoundCount.Name = "txtRoundCount";
      this.txtRoundCount.Size = new System.Drawing.Size(46, 21);
      this.txtRoundCount.TabIndex = 6;
      this.txtRoundCount.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
      this.txtRoundCount.ValueChanged += new System.EventHandler(this.txtRoundCount_ValueChanged);
      // 
      // txtRoundTime
      // 
      this.txtRoundTime.Location = new System.Drawing.Point(7, 90);
      this.txtRoundTime.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
      this.txtRoundTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtRoundTime.Name = "txtRoundTime";
      this.txtRoundTime.Size = new System.Drawing.Size(46, 21);
      this.txtRoundTime.TabIndex = 7;
      this.txtRoundTime.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
      // 
      // txtMagazineCount
      // 
      this.txtMagazineCount.Location = new System.Drawing.Point(7, 194);
      this.txtMagazineCount.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
      this.txtMagazineCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtMagazineCount.Name = "txtMagazineCount";
      this.txtMagazineCount.Size = new System.Drawing.Size(46, 21);
      this.txtMagazineCount.TabIndex = 11;
      this.txtMagazineCount.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
      // 
      // txtAmmoPerMagazine
      // 
      this.txtAmmoPerMagazine.Location = new System.Drawing.Point(7, 220);
      this.txtAmmoPerMagazine.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
      this.txtAmmoPerMagazine.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtAmmoPerMagazine.Name = "txtAmmoPerMagazine";
      this.txtAmmoPerMagazine.Size = new System.Drawing.Size(46, 21);
      this.txtAmmoPerMagazine.TabIndex = 12;
      this.txtAmmoPerMagazine.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
      // 
      // txtHP
      // 
      this.txtHP.Increment = new decimal(new int[] {
            3,
            0,
            0,
            0});
      this.txtHP.Location = new System.Drawing.Point(7, 168);
      this.txtHP.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtHP.Name = "txtHP";
      this.txtHP.Size = new System.Drawing.Size(46, 21);
      this.txtHP.TabIndex = 10;
      this.txtHP.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
      // 
      // cmbGameObject
      // 
      this.cmbGameObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbGameObject.FormattingEnabled = true;
      this.cmbGameObject.Location = new System.Drawing.Point(7, 10);
      this.cmbGameObject.Name = "cmbGameObject";
      this.cmbGameObject.Size = new System.Drawing.Size(89, 21);
      this.cmbGameObject.TabIndex = 4;
      this.cmbGameObject.SelectedIndexChanged += new System.EventHandler(this.cmbGameObject_SelectedIndexChanged);
      // 
      // btnTeamRemove
      // 
      this.btnTeamRemove.Enabled = false;
      this.btnTeamRemove.Hint = "حذف تیم";
      this.btnTeamRemove.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnTeamRemove.Image")));
      this.btnTeamRemove.Location = new System.Drawing.Point(550, 12);
      this.btnTeamRemove.Name = "btnTeamRemove";
      this.btnTeamRemove.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnTeamRemove.Size = new System.Drawing.Size(64, 64);
      this.btnTeamRemove.TabIndex = 15;
      this.btnTeamRemove.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnTeamRemove.Texts")));
      this.btnTeamRemove.TransparentColor = System.Drawing.Color.White;
      this.btnTeamRemove.Click += new System.EventHandler(this.mnuDeleteTeam_Click);
      // 
      // btnTeamAdd
      // 
      this.btnTeamAdd.Enabled = false;
      this.btnTeamAdd.Hint = "اضافه کردن تیم";
      this.btnTeamAdd.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnTeamAdd.Image")));
      this.btnTeamAdd.Location = new System.Drawing.Point(480, 12);
      this.btnTeamAdd.Name = "btnTeamAdd";
      this.btnTeamAdd.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnTeamAdd.Size = new System.Drawing.Size(64, 64);
      this.btnTeamAdd.TabIndex = 16;
      this.btnTeamAdd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnTeamAdd.Texts")));
      this.btnTeamAdd.TransparentColor = System.Drawing.Color.White;
      this.btnTeamAdd.Click += new System.EventHandler(this.btnTeamAdd_Click);
      // 
      // btnExit
      // 
      this.btnExit.Hint = "بازگشت";
      this.btnExit.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnExit.Image")));
      this.btnExit.Location = new System.Drawing.Point(410, 12);
      this.btnExit.Name = "btnExit";
      this.btnExit.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnExit.Size = new System.Drawing.Size(64, 64);
      this.btnExit.TabIndex = 44;
      this.btnExit.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnExit.Texts")));
      this.btnExit.TransparentColor = System.Drawing.Color.White;
      this.btnExit.Visible = false;
      this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
      // 
      // Management
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(629, 491);
      this.Controls.Add(this.btnExit);
      this.Controls.Add(this.grpGameSettings);
      this.Controls.Add(this.btnTeamRemove);
      this.Controls.Add(this.grpCompetitionSettings);
      this.Controls.Add(this.btnTeamAdd);
      this.Controls.Add(this.lvTeams);
      this.Controls.Add(this.btnOK);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.label2);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "Management";
      this.Text = "تنظیمات مسابقه";
      this.Load += new System.EventHandler(this.Management_Load);
      this.mnuTeams.ResumeLayout(false);
      this.grpCompetitionSettings.ResumeLayout(false);
      this.grpCompetitionSettings.PerformLayout();
      this.grpGameSettings.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.txtRestTime)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtGameTime)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtBombTimer)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtRoundCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtRoundTime)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtMagazineCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtAmmoPerMagazine)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtHP)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private SpecialForce.TransparentLabel label2;
    private System.Windows.Forms.ListView lvTeams;
    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.ContextMenuStrip mnuTeams;
    private System.Windows.Forms.ToolStripMenuItem mnuDeleteTeam;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem mnuLargeIcons;
    private System.Windows.Forms.ToolStripMenuItem mnuSmallIcons;
    private System.Windows.Forms.ToolStripMenuItem mnuList;
    private ToolBar grpCompetitionSettings;
    private System.Windows.Forms.TextBox txtTitle;
    private TransparentLabel label5;
    private Utilities.NumberBox txtPrice;
    private TransparentLabel label6;
    private System.Windows.Forms.ComboBox cmbRoundRobin;
    private TransparentLabel lblRoundRobin;
    private System.Windows.Forms.ComboBox cmbCompetitionType;
    private TransparentLabel label3;
    private ToolBar grpGameSettings;
    private System.Windows.Forms.NumericUpDown txtGameTime;
    private TransparentLabel label7;
    private TransparentLabel label1;
    private System.Windows.Forms.ComboBox cmbGameObject;
    private System.Windows.Forms.NumericUpDown txtBombTimer;
    private TransparentLabel lblBombTimer;
    private System.Windows.Forms.NumericUpDown txtRoundCount;
    private System.Windows.Forms.NumericUpDown txtRoundTime;
    private System.Windows.Forms.NumericUpDown txtMagazineCount;
    private System.Windows.Forms.NumericUpDown txtAmmoPerMagazine;
    private System.Windows.Forms.NumericUpDown txtHP;
    private TransparentLabel lblHP;
    private TransparentLabel label4;
    private TransparentLabel lblRoundTime;
    private TransparentLabel lblAmmoPerMagazine;
    private TransparentLabel lblMagazineCount;
    private ToolButton btnTeamRemove;
    private ToolButton btnTeamAdd;
    private TransparentLabel transparentLabel1;
    private System.Windows.Forms.NumericUpDown txtRestTime;
    private System.Windows.Forms.ComboBox cmbFriendlyFire;
    private TransparentLabel transparentLabel2;
    private ToolButton btnExit;

  }
}