﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Competitions
{
  public partial class FormCupGroups : SessionAwareForm
  {
    const int mGroupWidth = 120;
    const int mGroupGap = 10;
    const int mMinTeamsWidth = 450;

    private List<ListView> mGroups = new List<ListView>();
    private List<TransparentLabel> mLabels = new List<TransparentLabel>();
    private ListView mCurrentGroup = null;

    private DB.Competition mCompetition;
    public DB.Competition Competition
    {
      get { return mCompetition; }
      set { mCompetition = value; }
    }

    public FormCupGroups()
    {
      InitializeComponent();

      lvTeams.LargeImageList = new ImageList();
      lvTeams.SmallImageList = new ImageList();
      lvTeams.LargeImageList.ImageSize = new Size(64, 64);
      lvTeams.SmallImageList.ImageSize = new Size(16, 16);
    }

    private void FormCupGroups_Load(object sender, EventArgs e)
    {
      foreach (DB.Team team in mCompetition.GetTeams())
        AddTeam(lvTeams, team);

      AddGroup();
      AddGroup();
      AddGroup();
      AddGroup();
      rgGroupCount.AddButton(Properties.Resources.Competition_2_Groups, "کاپ ، 2 گروه", false);
      rgGroupCount.AddButton(Properties.Resources.Competition_4_Groups, "کاپ ، 4 گروه", true);
      rgGroupCount.AddButton(Properties.Resources.Competition_8_Groups, "کاپ ، 8 گروه", false);
      ResizeForm();
    }

    private void AddTeam(ListView lv, DB.Team team)
    {
      foreach (ListViewItem it_team in lvTeams.Items)
        if (it_team.Tag == team)
          lvTeams.Items.Remove(it_team);

      ListViewItem it = new ListViewItem();
      it.Tag = team;
      it.Text = team.Name;
      lv.LargeImageList.Images.Add(new Bitmap(team.Logo));
      lv.SmallImageList.Images.Add(new Bitmap(team.Logo));
      it.ImageIndex = lv.SmallImageList.Images.Count - 1;
      lv.Items.Add(it);
    }

    private void ResizeForm()
    {
      int min_width = btnOK.Right + 10;
      int group_width = mGroups.Count > 0 ? mGroups[mGroups.Count - 1].Right + 2 * mGroupGap : min_width;
      
      Width = Math.Max(group_width, min_width);
      CenterScreen();
    }

    private void OnTeamDragEnter(Object sender, DragEventArgs e)
    {
      if (e.Data.GetDataPresent(typeof(DB.Team)))
        e.Effect = DragDropEffects.Move;
    }

    private void OnTeamDrop(Object sender, DragEventArgs e)
    {
      AddTeam(sender as ListView, e.Data.GetData(typeof(DB.Team)) as DB.Team);
    }

    private void OnGroupFocused(Object sender, EventArgs e)
    {
      mCurrentGroup = sender as ListView;
      for (int i = 0; i < mGroups.Count; ++i)
        if (mGroups[i] == mCurrentGroup)
          mLabels[i].ForeColor = Color.FromArgb(34,177,76);
        else
          mLabels[i].ForeColor = Color.White;
    }

    private void lvTeams_ItemDrag(object sender, ItemDragEventArgs e)
    {
      DoDragDrop(((ListViewItem)(e.Item)).Tag, DragDropEffects.Move);
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      if (lvTeams.Items.Count != 0)
      {
        Session.ShowError("همه تیم ها باید در گروه ها قرار بگیرند.");
        return;
      }

      // Create list of groups
      List<List<int>> groups = new List<List<int>>();
      for (int group = 0; group < mGroups.Count; ++group)
      {
        List<int> teams_in_group = new List<int>();
        for(int index = 0; index < mGroups[group].Items.Count; ++index)
          teams_in_group.Add(((DB.Team)mGroups[group].Items[index].Tag).ID);
        groups.Add(teams_in_group);
      }

      // Check if a group consists of less than 3 teams
      int minimum = Session.ReleaseBuild ? 4 : 2;
      int maximum = Session.ReleaseBuild ? 5 : 10;
      for(int i = 0; i < groups.Count; ++i)
        if (groups[i].Count < minimum)
        {
          Session.ShowError("گروه " + GetGroupName(i) + " کمتر از " + minimum +" تیم دارد.");
          return;
        }
        else if(groups[i].Count > maximum)
        {
          Session.ShowError("گروه " + GetGroupName(i) + " بیشتر از " + maximum + " تیم دارد.");
          return;
        }

      mCompetition.Groupings = groups;
      DialogResult = System.Windows.Forms.DialogResult.OK;
    }

    private string GetGroupName(int index)
    {
      return ((char)('A' + index)).ToString();
    }

    private void AddGroup()
    {
      int index = mGroups.Count;
      int left = index == 0 ? lvTeams.Right : mGroups[index - 1].Right;
      left += mGroupGap;

      TransparentLabel lbl = new TransparentLabel();
      lbl.Parent = this;
      lbl.AutoSize = true;
      lbl.Font = new Font(lbl.Font.FontFamily, 12.0f, FontStyle.Bold);
      lbl.Left = left + (mGroupWidth - lbl.Width) / 2;
      lbl.Top = btnOK.Bottom + 2 * mGroupGap;
      lbl.Text = GetGroupName(index);
      mLabels.Add(lbl);

      ListView lv = new ListView();
      lv.Parent = this;
      lv.AllowDrop = true;
      lv.View = View.LargeIcon;
      lv.Left = left;
      lv.Top = lbl.Bottom + mGroupGap;
      lv.Width = mGroupWidth;
      lv.Height = lvTeams.Bottom - lv.Top;
      lv.RightToLeftLayout = true;
      lv.Scrollable = true;
      lv.DragEnter += OnTeamDragEnter;
      lv.DragDrop += OnTeamDrop;
      lv.GotFocus += OnGroupFocused;
      lv.LargeImageList = new ImageList();
      lv.SmallImageList = new ImageList();
      lv.LargeImageList.ImageSize = new System.Drawing.Size(64, 64);
      lv.SmallImageList.ImageSize = new System.Drawing.Size(16, 16);
      mGroups.Add(lv);
    }

    private void btnRemoveGroups()
    {
    }

    private void tbRemoveTeam_Click(object sender, EventArgs e)
    {
      if (mCurrentGroup == null)
        Session.ShowError("ابتدا تیم مورد نظر را انتخاب کنید.");
      else if (mCurrentGroup.SelectedIndices.Count == 0)
        Session.ShowError("هیج تیمی انتخاب نشده است.");
      else if(mCurrentGroup.SelectedIndices[0] == -1)
        Session.ShowError("هیج تیمی انتخاب نشده است.");
      else
      {
        AddTeam(lvTeams, mCurrentGroup.SelectedItems[0].Tag as DB.Team);
        mCurrentGroup.Items.RemoveAt(mCurrentGroup.SelectedIndices[0]);
      }
    }

    private void rgGroupCount_Click(object sender, EventArgs e)
    {
      SuspendLayout();

      // Nullify current group
      mCurrentGroup = null;

      // Remove all groups
      for (int i = 0; i < mGroups.Count; ++i)
      {
        // Remove all teams in the group first.
        for (int j = 0; j < mGroups[i].Items.Count; ++j)
          AddTeam(lvTeams, mGroups[i].Items[j].Tag as DB.Team);

        mGroups[i].Parent = null;
        mGroups[i].Dispose();
        mLabels[i].Parent = null;
        mLabels[i].Dispose();
      }

      mGroups.Clear();
      mLabels.Clear();

      // Add needed groups
      int cnt = 2;
      if(rgGroupCount.SelectedIndex == 1)
        cnt = 4;
      else if(rgGroupCount.SelectedIndex == 2)
        cnt = 8;

      for(int i = 0; i < cnt; ++i)
        AddGroup();

      ResizeForm();
      ResumeLayout();
    }
  }
}
