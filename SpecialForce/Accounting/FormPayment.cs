﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Accounting
{
  public partial class FormPayment : SessionAwareForm
  {
    public bool IsBill = false;
    public bool IsRent = false;
    public bool IsCompetition = false;
    private List<DB.Site> mSites;
    private List<DB.Competition> mCompetitions;
    private DB.Invoice mInvoice = null;

    public DB.Invoice Invoice
    {
      get { return mInvoice; }
    }

    public FormPayment()
    {
      InitializeComponent();
    }

    private void FormPayment_Load(object sender, EventArgs e)
    {
      if (IsBill)
      {
        pnlBill.Visible = true;
        Text = "پرداخت قبض";

        cmbBill.Items.Add(Enums.ToString(InvoiceType.BillGas));
        cmbBill.Items.Add(Enums.ToString(InvoiceType.BillPhone));
        cmbBill.Items.Add(Enums.ToString(InvoiceType.BillPower));
        cmbBill.Items.Add(Enums.ToString(InvoiceType.BillSewer));
        cmbBill.Items.Add(Enums.ToString(InvoiceType.BillTax));
        cmbBill.Items.Add(Enums.ToString(InvoiceType.BillWater));
        cmbBill.SelectedIndex = 0;
      }
      else if(IsRent)
      {
        pnlDescription.Visible = true;
        Text = "پرداخت اجاره";
      }
      else if (IsCompetition)
      {
        pnlCompetition.Visible = true;
        pnlDescription.Visible = true;
        pnlDescription.Top = pnlCompetition.Bottom + 5;
        pnlInfo.Height = pnlDescription.Bottom + 5;
        btnCancel.Top = pnlInfo.Bottom + 5;
        btnOK.Top = btnCancel.Top;
        Height = btnCancel.Bottom + 35;
        Text = "هزینه های مسابقات";

        mCompetitions = DB.CompetitionManager.AllCompetitions;
        if(!Session.CurrentStaff.IsGod())
          for (int i = 0; i < mCompetitions.Count; ++i)
            if (!mCompetitions[i].UsedSiteIDs.Contains(Session.ActiveSiteID))
              mCompetitions.RemoveAt(i--);

        for (int i = 0; i < mCompetitions.Count; ++i)
          cmbCompetition.Items.Add(mCompetitions[i].Title + "  (" + Enums.ToString(mCompetitions[i].Type) + ")");

        if (mCompetitions.Count == 0)
        {
          Session.ShowError("هیچ مسابقه ای در سیستم وجود ندارد.");
          Close();
        }
        else
          cmbCompetition.SelectedIndex = mCompetitions.Count - 1;
      }
      else
      {
        pnlDescription.Visible = true;
        Text = "پرداخت هزینه های متفرقه";
      }

      mSites = DB.Site.GetAllSites(false);
      for (int i = 0; i < mSites.Count; ++i)
      {
        cmbEstate.Items.Add(mSites[i]);
        if(mSites[i].ID == Session.ActiveSiteID)
          cmbEstate.SelectedIndex = i;
      }
    }

    protected override void ApplyPermissions(Post post)
    {
      cmbEstate.Enabled = post.IsGod();
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      if (txtAmount.Value <= 0)
      {
        Session.ShowError("مبلغ معتبر نیست.");
        return;
      }

      mInvoice = new DB.Invoice();
      if (IsBill)
      {
        if(cmbBill.SelectedIndex == -1)
        {
          Session.ShowError("نوع قبض را انتخاب کنید.");
          cmbBill.Focus();
          return;
        }

        mInvoice.Type = Enums.ParseInvoiceType(cmbBill.SelectedItem.ToString());
      }
      else if (IsRent)
        mInvoice.Type = InvoiceType.Rent;
      else
      {
        if (txtComment.Text == "")
        {
          Session.ShowError("باید توضیحات مربوط به هزینه را یادداشت کنید.");
          txtComment.Focus();
          txtComment.SelectAll();
          return;
        }

        mInvoice.Type = IsCompetition ? InvoiceType.CompetitionExpense : InvoiceType.UntypedPayment;

        if (IsCompetition)
        {
          DB.Competition comp = mCompetitions[cmbCompetition.SelectedIndex];
          if(comp == null)
          {
            Session.ShowError("مسابقه ای انتخاب نشده است.");
            return;
          }

          mInvoice.CompetitionID = comp.ID;
        }
      }

      mInvoice.Amount = txtAmount.Value;
      mInvoice.SiteID = mSites[cmbEstate.SelectedIndex].ID;
      mInvoice.Comment = txtComment.Text;
      mInvoice.OperatorID = Session.CurrentStaff.ID;
      mInvoice.Apply();

      DialogResult = DialogResult.OK;
    }

    private void txtAmount_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        e.Handled = true;
        btnOK_Click(sender, e);
      }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
