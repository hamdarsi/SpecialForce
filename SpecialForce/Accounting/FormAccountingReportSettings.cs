﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Accounting
{
  public partial class FormAccountingReportSettings : SessionAwareForm
  {
    public FormAccountingReportSettings()
    {
      InitializeComponent();
    }

    private void FormAccountingReport_Load(object sender, EventArgs e)
    {
      cmbReportType.SelectedIndex = cmbReportType.Items.Count - 1;

      foreach(DB.Site site in DB.Site.GetAllSites(true))
      {
        chkSites.Items.Add(site);
        bool en = site.ID == Session.ActiveSiteID;
        chkSites.SetItemChecked(chkSites.Items.Count - 1, en);

        if (en)
          btnReport.Enabled = true;
      }
    }

    protected override void ApplyPermissions(Post post)
    {
      chkSites.Enabled = post.IsGod();
    }

    private void cmbReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
      dtStart.Enabled = true;

      if (cmbReportType.SelectedIndex == 0)
      {
        lblStart.Text = "تاریخ روز:";
        dtStart.ShowDay = true;
        dtStart.ShowMonth = true;
        lblEnd.Visible = false;
        dtEnd.Visible = false;
        dtStart.Date = PersianDateTime.Today;
      }
      else if (cmbReportType.SelectedIndex == 1)
      {
        lblStart.Text = "تاریخ ماه:";
        dtStart.ShowDay = false;
        dtStart.ShowMonth = true;
        dtStart.Date = PersianDateTime.ThisMonth;
        lblEnd.Visible = false;
        dtEnd.Visible = false;
      }
      else if (cmbReportType.SelectedIndex == 2)
      {
        lblStart.Text = "سال:";
        dtStart.ShowDay = false;
        dtStart.ShowMonth = false;
        lblEnd.Visible = false;
        dtEnd.Visible = false;
        dtStart.Date = PersianDateTime.ThisYear;
      }
      else if (cmbReportType.SelectedIndex == 3)
      {
        lblStart.Text = "از:";
        dtStart.ShowDay = true;
        dtStart.ShowMonth = true;
        lblEnd.Visible = true;
        dtEnd.Visible = true;
        dtStart.Date = PersianDateTime.Today;
        dtEnd.Date = dtStart.Date.NextDay;
      }
      else
      {
        dtStart.Enabled = false;
        dtEnd.Visible = false;
        lblEnd.Visible = false;
      }

      Refresh();
    }

    /// <summary>
    /// Perpares the accounting report and passes it to the actual report form.
    /// What is does:
    /// 1 - Checks if any sites have selected.
    /// 2 - Sets the reports inclusion of invoice groups
    /// 3 - Calculcates datetime range for each site
    /// 4 - Creates the report
    /// 5 - Shows the report form
    /// </summary>
    private void btnReport_Click(object sender, EventArgs e)
    {
      // 1 - Checks if any sites have selected.
      if (chkSites.CheckedIndices.Count == 0)
      {
        Session.ShowError("حداقل یک باشگاه باید انتخاب شود.");
        return;
      }

      // 2 - Sets the reports inclusion of invoice groups
      DB.InvoiceReport rep = new DB.InvoiceReport();
      if (chkCupAdmissions.Checked)
        rep.Inclusions.Add(InvoiceGroup.CupAdmission);
      if (chkLeagueAdmissions.Checked)
        rep.Inclusions.Add(InvoiceGroup.LeagueAdmission);
      if (chkEliminationAdmissions.Checked)
        rep.Inclusions.Add(InvoiceGroup.EliminationAdmission);
      if (chkUserCharges.Checked)
        rep.Inclusions.Add(InvoiceGroup.UserCharges);
      if (chkCupExpenses.Checked)
        rep.Inclusions.Add(InvoiceGroup.CupExpenses);
      if (chkLeagueExpenses.Checked)
        rep.Inclusions.Add(InvoiceGroup.LeagueExpenses);
      if (chkElimExpenses.Checked)
        rep.Inclusions.Add(InvoiceGroup.EliminationExpenses);
      if (chkGiftCharges.Checked)
        rep.Inclusions.Add(InvoiceGroup.GiftCharges);
      if (chkRents.Checked)
        rep.Inclusions.Add(InvoiceGroup.Rents);
      if (chkBills.Checked)
        rep.Inclusions.Add(InvoiceGroup.Bills);
      if (chkOtherExpenses.Checked)
        rep.Inclusions.Add(InvoiceGroup.OthersExpenses);
      if (chkRefunds.Checked)
        rep.Inclusions.Add(InvoiceGroup.UserRefunds);

      if (rep.Inclusions.Count == 0)
      {
        Session.ShowError("هیچ مورد گزارشی انتخاب نشده است.");
        return;
      }

      // 3 - Calculcates datetime range for each site
      string str = cmbReportType.SelectedItem.ToString();
      if (cmbReportType.SelectedIndex < 3)
        str = str + ": " + dtStart.Date.ToString(DateTimeString.CompactDate);
      else if (cmbReportType.SelectedIndex == 3)
        str = str + " از " + dtStart.Date.ToString(DateTimeString.CompactDate) + " تا " + dtEnd.Date.ToString(DateTimeString.CompactDate);
      else
        str = "از آخرین تسویه";
      rep.DateString = str;

      foreach (DB.Site site in chkSites.CheckedItems)
      {
        PersianDateTime start = dtStart.Date, end;
        if (cmbReportType.SelectedIndex == 0)
          end = start.NextDay;
        else if (cmbReportType.SelectedIndex == 1)
          end = start.NextMonth;
        else if (cmbReportType.SelectedIndex == 2)
          end = start.NextYear;
        else if (cmbReportType.SelectedIndex == 3)
          end = dtEnd.Date;
        else
        {
          List<DB.SiteClearance> clears = site.Clearances;

          if (clears.Count == 0)
            start = site.StartDate;
          else
          {
            start = clears[clears.Count - 1].TimeStamp;
            start.SetTime(start.Hour, start.Minute, start.Second + 1);
          }
          end = PersianDateTime.Today.NextDay;
        }

        rep.IncludeSite(site, start, end);
      }

      // 4 - Creates the report
      rep.MakeReport();

      // 5 - Shows the report form
      FormAccountingReport frm = new FormAccountingReport();
      frm.mReport = rep;
      ShowModal(frm, true);
      DialogResult = DialogResult.OK;
    }

    private void chkSites_SelectedIndexChanged(object sender, EventArgs e)
    {
      btnReport.Enabled = chkSites.SelectedIndices.Count != 0;
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
