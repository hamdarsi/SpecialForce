﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace SpecialForce.Accounting
{
  public partial class FormClearSite : SessionAwareForm
  {
    private List<DB.Site> mSites = null;
    private double mCash = 0;

    public FormClearSite()
    {
      InitializeComponent();
    }

    private void FormClearSite_Load(object sender, EventArgs e)
    {
      mSites = new List<DB.Site>();
      List<DB.Site> sites = DB.Site.GetAllSites(false);

      for (int i = 0; i < sites.Count; ++i)
      {
        if (Session.CurrentStaff.IsGod() || sites[i].ID == Session.ActiveSiteID)
        {
          lbSites.Items.Add(sites[i]);
          mSites.Add(sites[i]);
        }
      }
    }

    private void txtAmount_OnNumberEntered(object sender, EventArgs e)
    {
      btnOK_Click(sender, e);
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      int index = lbSites.SelectedIndices.Count == 0 ? -1 : lbSites.SelectedIndices[0];
      if (index == -1)
        Session.ShowError("هیچ باشگاهی انتخاب نشده است.");
      else if (mCash <= 0)
        Session.ShowError("در این باشگاه هیچ اعتباری وجود ندارد.");
      else if (txtAmount.Value > mCash)
        Session.ShowError("مقدار وارد شده از موجودی باشگاه بیشتر است.");
      else
      {
        string str = "آیا به اندازه مبلغ وارد کرده از موجودی باشگاه برداشتید؟";
        if (Session.Ask(str))
        {
          DB.SiteClearance.ClearSite(txtAmount.Value, mSites[index]);
          Session.ShowMessage("مقدار وارد شده از موجودی باشگاه برداشته شد.");
          DialogResult = DialogResult.OK;
        }
        else
          Session.ShowMessage("برداشت کنسل شد.");
      }
    }

    private void pnlInfo_Paint(object sender, PaintEventArgs e)
    {
      lvTransactions.Refresh();
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void lbSites_SelectedIndexChanged(object sender, EventArgs e)
    {
      lblCurrentCredit.Text = "";
      int index = lbSites.SelectedIndices.Count == 0 ? -1 : lbSites.SelectedIndices[0];
      txtAmount.Enabled = index != -1;
      if (index == -1)
        return;

      try
      {
        lblCurrentCredit.Text = "در حال محاسبه...";
        Session.WaitCursor = true;

        DB.Site site = mSites[index];
        List<DB.SiteClearance> clears = site.Clearances;

        if (clears.Count != 0)
          lvTransactions.Items.Clear();

        for (int i = clears.Count - 1; i >= 0; --i)
        {
          ListViewItem it = new ListViewItem(clears[i].TimeStamp.ToString(DateTimeString.CompactDate));
          it.SubItems.Add(clears[i].Amount.ToCurrencyString());
          it.SubItems.Add(clears[i].Clearer.FullName);
          lvTransactions.Items.Add(it);
        }

        mCash = DataBase.ExecuteProcedure("GetAvailableCash", "@sid", mSites[index].ID).Rows[0].Field<double>(0);
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "FormClearSite", "SelectedIndexChanged");
        mCash = -1;
      }
      finally
      {
        Session.WaitCursor = false;
        lblCurrentCredit.Text = mCash != -1 ? "موجودی: " + mCash.ToCurrencyString() + " تومان" : "مشکلی پیش آمد";
      }
    }

    private void txtAmount_OnNumberChanged(object sender, EventArgs e)
    {
      btnOK.Enabled = txtAmount.Value > 0;
    }
  }
}
