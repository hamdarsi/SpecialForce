﻿namespace SpecialForce.Accounting
{
  partial class ControlPanel
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPanel));
      this.tinypicker1 = new SpecialForce.TinyPicker();
      this.tbButtons = new SpecialForce.ToolBar();
      this.tbCompetition = new SpecialForce.ToolButton();
      this.tbRent = new SpecialForce.ToolButton();
      this.tbLock = new SpecialForce.ToolButton();
      this.tbClear = new SpecialForce.ToolButton();
      this.tbReport = new SpecialForce.ToolButton();
      this.tbOther = new SpecialForce.ToolButton();
      this.tbBill = new SpecialForce.ToolButton();
      this.lvTodayReport = new SpecialForce.UserInterface.ListView();
      this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn5 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.tbButtons.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lvTodayReport)).BeginInit();
      this.SuspendLayout();
      // 
      // tinypicker1
      // 
      this.tinypicker1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tinypicker1.Location = new System.Drawing.Point(12, 567);
      this.tinypicker1.Name = "tinypicker1";
      this.tinypicker1.Size = new System.Drawing.Size(769, 30);
      this.tinypicker1.TabIndex = 21;
      this.tinypicker1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tinypicker1.Texts")));
      // 
      // tbButtons
      // 
      this.tbButtons.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbButtons.BorderWidth = 0.5F;
      this.tbButtons.Controls.Add(this.tbCompetition);
      this.tbButtons.Controls.Add(this.tbRent);
      this.tbButtons.Controls.Add(this.tbLock);
      this.tbButtons.Controls.Add(this.tbClear);
      this.tbButtons.Controls.Add(this.tbReport);
      this.tbButtons.Controls.Add(this.tbOther);
      this.tbButtons.Controls.Add(this.tbBill);
      this.tbButtons.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbButtons.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbButtons.Location = new System.Drawing.Point(12, 12);
      this.tbButtons.Name = "tbButtons";
      this.tbButtons.Size = new System.Drawing.Size(84, 540);
      this.tbButtons.TabIndex = 38;
      // 
      // tbCompetition
      // 
      this.tbCompetition.Hint = "ثبت پرداخت هزینه های مسابقات";
      this.tbCompetition.Image = ((System.Drawing.Bitmap)(resources.GetObject("tbCompetition.Image")));
      this.tbCompetition.Location = new System.Drawing.Point(9, 143);
      this.tbCompetition.Name = "tbCompetition";
      this.tbCompetition.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.tbCompetition.Size = new System.Drawing.Size(64, 64);
      this.tbCompetition.TabIndex = 50;
      this.tbCompetition.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tbCompetition.Texts")));
      this.tbCompetition.TransparentColor = System.Drawing.Color.White;
      this.tbCompetition.Click += new System.EventHandler(this.tbCompetition_Click);
      // 
      // tbRent
      // 
      this.tbRent.Hint = "ثبت پرداخت اجاره";
      this.tbRent.Image = ((System.Drawing.Bitmap)(resources.GetObject("tbRent.Image")));
      this.tbRent.Location = new System.Drawing.Point(9, 7);
      this.tbRent.Name = "tbRent";
      this.tbRent.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.tbRent.Size = new System.Drawing.Size(64, 64);
      this.tbRent.TabIndex = 49;
      this.tbRent.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tbRent.Texts")));
      this.tbRent.TransparentColor = System.Drawing.Color.White;
      this.tbRent.Click += new System.EventHandler(this.btnPayRent_Click);
      // 
      // tbLock
      // 
      this.tbLock.Hint = "قفل سیستم";
      this.tbLock.Image = ((System.Drawing.Bitmap)(resources.GetObject("tbLock.Image")));
      this.tbLock.Location = new System.Drawing.Point(9, 464);
      this.tbLock.Name = "tbLock";
      this.tbLock.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.tbLock.Size = new System.Drawing.Size(64, 64);
      this.tbLock.TabIndex = 48;
      this.tbLock.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tbLock.Texts")));
      this.tbLock.TransparentColor = System.Drawing.Color.White;
      this.tbLock.Click += new System.EventHandler(this.btnLogout_Click);
      // 
      // tbClear
      // 
      this.tbClear.Hint = "برداشت از موجودی باشگاه ها";
      this.tbClear.Image = ((System.Drawing.Bitmap)(resources.GetObject("tbClear.Image")));
      this.tbClear.ImeMode = System.Windows.Forms.ImeMode.On;
      this.tbClear.Location = new System.Drawing.Point(9, 347);
      this.tbClear.Name = "tbClear";
      this.tbClear.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.tbClear.Size = new System.Drawing.Size(64, 64);
      this.tbClear.TabIndex = 47;
      this.tbClear.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tbClear.Texts")));
      this.tbClear.TransparentColor = System.Drawing.Color.White;
      this.tbClear.Click += new System.EventHandler(this.btnClear_Click);
      // 
      // tbReport
      // 
      this.tbReport.Hint = "گزارش گیری";
      this.tbReport.Image = ((System.Drawing.Bitmap)(resources.GetObject("tbReport.Image")));
      this.tbReport.Location = new System.Drawing.Point(9, 279);
      this.tbReport.Name = "tbReport";
      this.tbReport.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.tbReport.Size = new System.Drawing.Size(64, 64);
      this.tbReport.TabIndex = 46;
      this.tbReport.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tbReport.Texts")));
      this.tbReport.TransparentColor = System.Drawing.Color.White;
      this.tbReport.Click += new System.EventHandler(this.btnReport_Click);
      // 
      // tbOther
      // 
      this.tbOther.Hint = "ثبت پرداخت هزینه های متفرقه";
      this.tbOther.Image = ((System.Drawing.Bitmap)(resources.GetObject("tbOther.Image")));
      this.tbOther.Location = new System.Drawing.Point(9, 211);
      this.tbOther.Name = "tbOther";
      this.tbOther.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.tbOther.Size = new System.Drawing.Size(64, 64);
      this.tbOther.TabIndex = 45;
      this.tbOther.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tbOther.Texts")));
      this.tbOther.TransparentColor = System.Drawing.Color.White;
      this.tbOther.Click += new System.EventHandler(this.btnPayOther_Click);
      // 
      // tbBill
      // 
      this.tbBill.Hint = "ثبت پرداخت قبض";
      this.tbBill.Image = ((System.Drawing.Bitmap)(resources.GetObject("tbBill.Image")));
      this.tbBill.Location = new System.Drawing.Point(9, 75);
      this.tbBill.Name = "tbBill";
      this.tbBill.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.tbBill.Size = new System.Drawing.Size(64, 64);
      this.tbBill.TabIndex = 44;
      this.tbBill.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tbBill.Texts")));
      this.tbBill.TransparentColor = System.Drawing.Color.White;
      this.tbBill.Click += new System.EventHandler(this.btnPayBill_Click);
      // 
      // lvTodayReport
      // 
      this.lvTodayReport.AllColumns.Add(this.olvColumn1);
      this.lvTodayReport.AllColumns.Add(this.olvColumn2);
      this.lvTodayReport.AllColumns.Add(this.olvColumn3);
      this.lvTodayReport.AllColumns.Add(this.olvColumn4);
      this.lvTodayReport.AllColumns.Add(this.olvColumn5);
      this.lvTodayReport.AllowDrop = true;
      this.lvTodayReport.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this.olvColumn3,
            this.olvColumn4,
            this.olvColumn5});
      this.lvTodayReport.EmptyListMsgFont = new System.Drawing.Font("B Nazanin", 12F);
      this.lvTodayReport.ForeColor = System.Drawing.Color.Black;
      this.lvTodayReport.FullRowSelect = true;
      this.lvTodayReport.GridLines = true;
      this.lvTodayReport.IsSimpleDragSource = true;
      this.lvTodayReport.Location = new System.Drawing.Point(102, 12);
      this.lvTodayReport.Name = "lvTodayReport";
      this.lvTodayReport.RightToLeftLayout = true;
      this.lvTodayReport.ShowGroups = false;
      this.lvTodayReport.Size = new System.Drawing.Size(683, 540);
      this.lvTodayReport.TabIndex = 39;
      this.lvTodayReport.UseCompatibleStateImageBehavior = false;
      this.lvTodayReport.View = System.Windows.Forms.View.Details;
      // 
      // olvColumn1
      // 
      this.olvColumn1.CellPadding = null;
      this.olvColumn1.Text = "ردیف";
      this.olvColumn1.Width = 35;
      // 
      // olvColumn2
      // 
      this.olvColumn2.CellPadding = null;
      this.olvColumn2.Text = "توضیحات";
      this.olvColumn2.Width = 352;
      // 
      // olvColumn3
      // 
      this.olvColumn3.CellPadding = null;
      this.olvColumn3.Text = "باشگاه";
      this.olvColumn3.Width = 91;
      // 
      // olvColumn4
      // 
      this.olvColumn4.CellPadding = null;
      this.olvColumn4.Text = "مبلغ";
      this.olvColumn4.Width = 81;
      // 
      // olvColumn5
      // 
      this.olvColumn5.CellPadding = null;
      this.olvColumn5.Text = "تاریخ";
      this.olvColumn5.Width = 94;
      // 
      // ControlPanel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(797, 610);
      this.Controls.Add(this.lvTodayReport);
      this.Controls.Add(this.tinypicker1);
      this.Controls.Add(this.tbButtons);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "ControlPanel";
      this.Text = "کنترل حساب ها";
      this.Load += new System.EventHandler(this.ControlPanelAccounting_Load);
      this.tbButtons.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.lvTodayReport)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private TinyPicker tinypicker1;
    private ToolBar tbButtons;
    private ToolButton tbRent;
    private ToolButton tbLock;
    private ToolButton tbClear;
    private ToolButton tbReport;
    private ToolButton tbOther;
    private ToolButton tbBill;
    private ToolButton tbCompetition;
    private UserInterface.ListView lvTodayReport;
    private BrightIdeasSoftware.OLVColumn olvColumn1;
    private BrightIdeasSoftware.OLVColumn olvColumn2;
    private BrightIdeasSoftware.OLVColumn olvColumn3;
    private BrightIdeasSoftware.OLVColumn olvColumn4;
    private BrightIdeasSoftware.OLVColumn olvColumn5;
  }
}