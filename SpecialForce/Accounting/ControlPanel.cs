﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Accounting
{
  public partial class ControlPanel : SessionAwareForm
  {
    static private ControlPanel mInstance = null;
    static public ControlPanel Instance
    {
      get
      {
        if (mInstance == null || mInstance.IsDisposed)
          mInstance = new ControlPanel();

        return mInstance;
      }
    }

    private ControlPanel()
    {
      InitializeComponent();
    }

    private void RefreshInvoiceReport()
    {
      DB.InvoiceReport rep = new DB.InvoiceReport();
      rep.Inclusions.Add(InvoiceGroup.CupAdmission);
      rep.Inclusions.Add(InvoiceGroup.LeagueAdmission);
      rep.Inclusions.Add(InvoiceGroup.EliminationAdmission);
      rep.Inclusions.Add(InvoiceGroup.UserCharges);
      rep.Inclusions.Add(InvoiceGroup.CupExpenses);
      rep.Inclusions.Add(InvoiceGroup.LeagueExpenses);
      rep.Inclusions.Add(InvoiceGroup.EliminationExpenses);
      rep.Inclusions.Add(InvoiceGroup.UserRefunds);
      rep.Inclusions.Add(InvoiceGroup.GiftCharges);
      rep.Inclusions.Add(InvoiceGroup.Rents);
      rep.Inclusions.Add(InvoiceGroup.Bills);
      rep.Inclusions.Add(InvoiceGroup.OthersExpenses);
      rep.IncludeSite(DB.Site.GetSiteByID(Session.ActiveSiteID), PersianDateTime.Today, PersianDateTime.Tomorrow);
      rep.MakeReport();

      lvTodayReport.SetObjects(rep.Invoices);
    }

    private void btnLogout_Click(object sender, EventArgs e)
    {
      Session.Lock();
    }

    private void btnPayBill_Click(object sender, EventArgs e)
    {
      FormPayment frm = new FormPayment();
      frm.IsBill = true;
      frm.ShowDialog();
      RefreshInvoiceReport();
    }

    private void btnPayRent_Click(object sender, EventArgs e)
    {
      FormPayment frm = new FormPayment();
      frm.IsRent = true;
      frm.ShowDialog();
      RefreshInvoiceReport();
    }

    private void tbCompetition_Click(object sender, EventArgs e)
    {
      FormPayment frm = new FormPayment();
      frm.IsCompetition = true;
      frm.ShowDialog();
      RefreshInvoiceReport();
    }

    private void btnPayOther_Click(object sender, EventArgs e)
    {
      FormPayment frm = new FormPayment();
      frm.ShowDialog();
      RefreshInvoiceReport();
    }

    private void btnReport_Click(object sender, EventArgs e)
    {
      FormAccountingReportSettings frm = new FormAccountingReportSettings();
      frm.ShowDialog();
    }

    private void ControlPanelAccounting_Load(object sender, EventArgs e)
    {
      lvTodayReport.SetEmptyMessage("هیچ حسابی برای امروز ثبت نشده است.");
      lvTodayReport.SetAspect(1, o => { return ((DB.Invoice)o).ToString(); });
      lvTodayReport.SetAspect(2, o => { return ((DB.Invoice)o).SiteName; });
      lvTodayReport.SetAspect(3, o => { return ((DB.Invoice)o).Amount.ToCurrencyString() + " تومان"; });
      lvTodayReport.SetAspect(4, o => { return ((DB.Invoice)o).TimeStamp.ToString(DateTimeString.FullNumericDate); });
      
      RefreshInvoiceReport();
    }

    private void btnClear_Click(object sender, EventArgs e)
    {
      FormClearSite frm = new FormClearSite();
      frm.ShowDialog();
      RefreshInvoiceReport();
    }
  }
}
