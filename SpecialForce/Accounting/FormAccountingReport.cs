﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SpecialForce.Accounting
{
  public partial class FormAccountingReport : SessionAwareForm
  {
    public DB.InvoiceReport mReport;

    public FormAccountingReport()
    {
      InitializeComponent();
    }

    private void FormAccountingReport_Shown(object sender, EventArgs e)
    {
      // Start
      report1.BeginUpdate();

      // Site inclusion information
      List<DB.Site> sites = mReport.Sites;
      Reports.Table table = report1.AddTable(false, 90, 500);
      List<string> inclusions = Enums.ToStringList(mReport.Inclusions, 400, table.Font);
      table.AddKeyValuePair("گزارش از", inclusions[0]);
      for (int i = 1; i < inclusions.Count; ++i)
        table.AddKeyValuePair("", inclusions[i]);
      table.AddKeyValuePair("بازه زمانی", mReport.DateString);

      string str_sites = "";
      for (int i = 0; i < sites.Count; ++i)
        str_sites = Enums.AddToken(str_sites, sites[i].FullName);
      List<string> site_list = TextRenderer.FitStringInLines(str_sites, 400, table.Font);
      table.AddKeyValuePair("باشگاه ها", site_list[0]);
      for (int i = 1; i < site_list.Count; ++i)
        table.AddKeyValuePair("", site_list[i]);

      // Report all the invoices
      table = report1.AddTable();
      table.AutoIndexColumn = true;
      table.AddNewColumn(Types.String, "توضیحات", 350);
      table.AddNewColumn(Types.String, "باشگاه", 100);
      table.AddNewColumn(Types.Currency, "فی", 70);
      table.AddNewColumn(Types.DateTime, "تاریخ", 70);
      table.OnRowDoubleClicked += RowDoubleClicked;

      Reports.Row row;
      foreach (DB.Invoice i in mReport.Invoices)
      {
        if (i.Type == InvoiceType.UserCreditUsage)
          continue;

        row = table.AddNewRow(i);
        row[0] = i.ToString();
        row[1] = i.SiteName;
        row[2] = i.Amount.ToCurrency();
        row[3] = i.TimeStamp;
      }
      
      // Detailed invoice report
      table = report1.AddTable(false, 110, 200);
      table.AddKeyValuePair("شارژ کاربران", mReport.TotalUserCharges.ToCurrencyString());
      table.AddKeyValuePair("استفاده کاربران", mReport.TotalUserUsages.ToCurrencyString());
      table.AddKeyValuePair("درآمد کاپ ها", mReport.TotalCupAdmissions.ToCurrencyString());
      table.AddKeyValuePair("درآمد لیگ ها", mReport.TotalLeagueAdmissions.ToCurrencyString());
      table.AddKeyValuePair("درآمد حذفی ها", mReport.TotalEliminationAdmissions.ToCurrencyString());
      table.AddKeyValuePair("اجاره ها", mReport.TotalRentExpenses.ToCurrencyString());
      table.AddKeyValuePair("قبض ها", mReport.TotalBillExpenses.ToCurrencyString());
      table.AddKeyValuePair("هدیه ها", mReport.TotalGiftCharges.ToCurrencyString());
      table.AddKeyValuePair("سایر هزینه ها", mReport.TotalOtherExpenses.ToCurrencyString());
      table.AddKeyValuePair("هزینه مسابقه ها", mReport.TotalCompetitionExpenses.ToCurrencyString());
      table.AddKeyValuePair("عودت ها", mReport.TotalUserRefunds.ToCurrencyString());
      table.AddKeyValuePair("", "");
      table.AddKeyValuePair("کل درآمد ها", mReport.TotalCharges.ToCurrencyString());
      table.AddKeyValuePair("کل هزینه ها", mReport.TotalExpenses.ToCurrencyString());
      table.AddKeyValuePair("کل عودت ها", mReport.TotalRefunds.ToCurrencyString());
      table.AddKeyValuePair("کل تسویه ها", mReport.TotalClearances.ToCurrencyString());
      table.AddKeyValuePair("موجودی خالص بازه", mReport.Balance.ToCurrencyString());

      // finished
      report1.EndUpdate();
    }

    private void RowDoubleClicked(Object sender, ReportRowArgs args)
    {
      DB.Invoice inv = args.Data as DB.Invoice;

      StringBuilder sb = new StringBuilder();
      sb.AppendLine("نوع: " + Enums.ToString(inv.Type));
      sb.AppendLine("توضیحات: " + inv.Comment);
      Session.ShowMessage(sb.ToString());
    }
  }
}
