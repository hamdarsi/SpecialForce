﻿namespace SpecialForce.Accounting
{
  partial class FormClearSite
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "",
            "خالی"}, -1);
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormClearSite));
      this.lvTransactions = new System.Windows.Forms.ListView();
      this.columnDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.coloumnAmount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.columnClearer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.transparentLabel5 = new SpecialForce.TransparentLabel();
      this.lblCurrentCredit = new SpecialForce.TransparentLabel();
      this.btnOK = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.pnlBanner = new SpecialForce.ToolBar();
      this.lblInfo = new SpecialForce.TransparentLabel();
      this.lblBanner = new SpecialForce.TransparentLabel();
      this.lblFooter = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.lbSites = new System.Windows.Forms.ListBox();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.txtAmount = new SpecialForce.Utilities.NumberBox();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.pnlBanner.SuspendLayout();
      this.SuspendLayout();
      // 
      // lvTransactions
      // 
      this.lvTransactions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnDate,
            this.coloumnAmount,
            this.columnClearer});
      this.lvTransactions.FullRowSelect = true;
      this.lvTransactions.GridLines = true;
      this.lvTransactions.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
      this.lvTransactions.Location = new System.Drawing.Point(188, 110);
      this.lvTransactions.Name = "lvTransactions";
      this.lvTransactions.RightToLeftLayout = true;
      this.lvTransactions.Size = new System.Drawing.Size(340, 160);
      this.lvTransactions.TabIndex = 19;
      this.lvTransactions.UseCompatibleStateImageBehavior = false;
      this.lvTransactions.View = System.Windows.Forms.View.Details;
      // 
      // columnDate
      // 
      this.columnDate.Text = "تاریخ";
      this.columnDate.Width = 86;
      // 
      // coloumnAmount
      // 
      this.coloumnAmount.Text = "مقدار";
      this.coloumnAmount.Width = 92;
      // 
      // columnClearer
      // 
      this.columnClearer.Text = "برداشت کننده";
      this.columnClearer.Width = 136;
      // 
      // transparentLabel5
      // 
      this.transparentLabel5.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel5.Location = new System.Drawing.Point(264, 82);
      this.transparentLabel5.Name = "transparentLabel5";
      this.transparentLabel5.Size = new System.Drawing.Size(99, 24);
      this.transparentLabel5.TabIndex = 18;
      this.transparentLabel5.TabStop = false;
      this.transparentLabel5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel5.Texts")));
      // 
      // lblCurrentCredit
      // 
      this.lblCurrentCredit.FixFromRight = true;
      this.lblCurrentCredit.Location = new System.Drawing.Point(9, 344);
      this.lblCurrentCredit.Name = "lblCurrentCredit";
      this.lblCurrentCredit.Size = new System.Drawing.Size(0, 0);
      this.lblCurrentCredit.TabIndex = 13;
      this.lblCurrentCredit.TabStop = false;
      this.lblCurrentCredit.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblCurrentCredit.Texts")));
      // 
      // btnOK
      // 
      this.btnOK.Enabled = false;
      this.btnOK.ForeColor = System.Drawing.Color.Black;
      this.btnOK.Location = new System.Drawing.Point(372, 341);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 24;
      this.btnOK.Text = "برداشت";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(453, 341);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 25;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // pnlBanner
      // 
      this.pnlBanner.BackColor = System.Drawing.Color.White;
      this.pnlBanner.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlBanner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlBanner.Controls.Add(this.lblInfo);
      this.pnlBanner.Controls.Add(this.lblBanner);
      this.pnlBanner.Location = new System.Drawing.Point(-6, -7);
      this.pnlBanner.Name = "pnlBanner";
      this.pnlBanner.Size = new System.Drawing.Size(553, 80);
      this.pnlBanner.TabIndex = 26;
      // 
      // lblInfo
      // 
      this.lblInfo.AutoSize = true;
      this.lblInfo.FixFromRight = true;
      this.lblInfo.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblInfo.Location = new System.Drawing.Point(188, 39);
      this.lblInfo.Name = "lblInfo";
      this.lblInfo.Size = new System.Drawing.Size(239, 24);
      this.lblInfo.TabIndex = 1;
      this.lblInfo.TabStop = false;
      this.lblInfo.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblInfo.Texts")));
      // 
      // lblBanner
      // 
      this.lblBanner.AutoSize = true;
      this.lblBanner.FixFromRight = true;
      this.lblBanner.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblBanner.Location = new System.Drawing.Point(335, 15);
      this.lblBanner.Name = "lblBanner";
      this.lblBanner.Size = new System.Drawing.Size(101, 26);
      this.lblBanner.TabIndex = 0;
      this.lblBanner.TabStop = false;
      this.lblBanner.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBanner.Texts")));
      // 
      // lblFooter
      // 
      this.lblFooter.AutoSize = true;
      this.lblFooter.Enabled = false;
      this.lblFooter.Location = new System.Drawing.Point(-8, 314);
      this.lblFooter.Name = "lblFooter";
      this.lblFooter.Size = new System.Drawing.Size(544, 15);
      this.lblFooter.TabIndex = 27;
      this.lblFooter.TabStop = false;
      this.lblFooter.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblFooter.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.Location = new System.Drawing.Point(62, 82);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(34, 24);
      this.transparentLabel1.TabIndex = 28;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // lbSites
      // 
      this.lbSites.FormattingEnabled = true;
      this.lbSites.Location = new System.Drawing.Point(12, 110);
      this.lbSites.Name = "lbSites";
      this.lbSites.Size = new System.Drawing.Size(170, 160);
      this.lbSites.TabIndex = 29;
      this.lbSites.SelectedIndexChanged += new System.EventHandler(this.lbSites_SelectedIndexChanged);
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel2.Location = new System.Drawing.Point(7, 280);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(324, 24);
      this.transparentLabel2.TabIndex = 30;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // txtAmount
      // 
      this.txtAmount.Enabled = false;
      this.txtAmount.Location = new System.Drawing.Point(406, 282);
      this.txtAmount.Mode = SpecialForce.Utilities.NumberBox.NumberMode.Currency;
      this.txtAmount.Name = "txtAmount";
      this.txtAmount.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtAmount.Size = new System.Drawing.Size(85, 21);
      this.txtAmount.TabIndex = 31;
      this.txtAmount.Text = "$0.00";
      this.txtAmount.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtAmount_OnNumberEntered);
      this.txtAmount.OnNumberChanged += new System.EventHandler<System.EventArgs>(this.txtAmount_OnNumberChanged);
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel3.Location = new System.Drawing.Point(497, 281);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(31, 24);
      this.transparentLabel3.TabIndex = 32;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // FormClearSite
      // 
      this.AcceptButton = this.btnOK;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(541, 376);
      this.Controls.Add(this.transparentLabel3);
      this.Controls.Add(this.txtAmount);
      this.Controls.Add(this.transparentLabel2);
      this.Controls.Add(this.lbSites);
      this.Controls.Add(this.transparentLabel1);
      this.Controls.Add(this.lvTransactions);
      this.Controls.Add(this.transparentLabel5);
      this.Controls.Add(this.lblFooter);
      this.Controls.Add(this.pnlBanner);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.lblCurrentCredit);
      this.Controls.Add(this.btnOK);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormClearSite";
      this.Text = "برداشت";
      this.Load += new System.EventHandler(this.FormClearSite_Load);
      this.pnlBanner.ResumeLayout(false);
      this.pnlBanner.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private TransparentLabel lblCurrentCredit;
    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.ListView lvTransactions;
    private System.Windows.Forms.ColumnHeader columnDate;
    private System.Windows.Forms.ColumnHeader coloumnAmount;
    private System.Windows.Forms.ColumnHeader columnClearer;
    private TransparentLabel transparentLabel5;
    private ToolBar pnlBanner;
    private TransparentLabel lblInfo;
    private TransparentLabel lblBanner;
    private TransparentLabel lblFooter;
    private TransparentLabel transparentLabel1;
    private System.Windows.Forms.ListBox lbSites;
    private TransparentLabel transparentLabel2;
    private Utilities.NumberBox txtAmount;
    private TransparentLabel transparentLabel3;

  }
}