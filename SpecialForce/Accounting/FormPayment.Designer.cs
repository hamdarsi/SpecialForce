﻿namespace SpecialForce.Accounting
{
  partial class FormPayment
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPayment));
      this.pnlInfo = new SpecialForce.ToolBar();
      this.txtAmount = new SpecialForce.Utilities.NumberBox();
      this.cmbEstate = new System.Windows.Forms.ComboBox();
      this.label1 = new SpecialForce.TransparentLabel();
      this.label2 = new SpecialForce.TransparentLabel();
      this.label4 = new SpecialForce.TransparentLabel();
      this.pnlBill = new SpecialForce.ToolBar();
      this.cmbBill = new System.Windows.Forms.ComboBox();
      this.lblPayType = new SpecialForce.TransparentLabel();
      this.pnlCompetition = new SpecialForce.ToolBar();
      this.cmbCompetition = new System.Windows.Forms.ComboBox();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.pnlDescription = new SpecialForce.ToolBar();
      this.txtComment = new System.Windows.Forms.TextBox();
      this.label3 = new SpecialForce.TransparentLabel();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnOK = new System.Windows.Forms.Button();
      this.pnlInfo.SuspendLayout();
      this.pnlBill.SuspendLayout();
      this.pnlCompetition.SuspendLayout();
      this.pnlDescription.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlInfo
      // 
      this.pnlInfo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlInfo.BorderWidth = 0.5F;
      this.pnlInfo.Controls.Add(this.txtAmount);
      this.pnlInfo.Controls.Add(this.cmbEstate);
      this.pnlInfo.Controls.Add(this.label1);
      this.pnlInfo.Controls.Add(this.label2);
      this.pnlInfo.Controls.Add(this.label4);
      this.pnlInfo.Controls.Add(this.pnlBill);
      this.pnlInfo.Controls.Add(this.pnlCompetition);
      this.pnlInfo.Controls.Add(this.pnlDescription);
      this.pnlInfo.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlInfo.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlInfo.Location = new System.Drawing.Point(10, 12);
      this.pnlInfo.Name = "pnlInfo";
      this.pnlInfo.Size = new System.Drawing.Size(370, 151);
      this.pnlInfo.TabIndex = 23;
      // 
      // txtAmount
      // 
      this.txtAmount.Location = new System.Drawing.Point(168, 36);
      this.txtAmount.Mode = SpecialForce.Utilities.NumberBox.NumberMode.Currency;
      this.txtAmount.Name = "txtAmount";
      this.txtAmount.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtAmount.Size = new System.Drawing.Size(121, 21);
      this.txtAmount.TabIndex = 20;
      this.txtAmount.Text = "$0.00";
      // 
      // cmbEstate
      // 
      this.cmbEstate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbEstate.FormattingEnabled = true;
      this.cmbEstate.Location = new System.Drawing.Point(168, 10);
      this.cmbEstate.Name = "cmbEstate";
      this.cmbEstate.Size = new System.Drawing.Size(121, 21);
      this.cmbEstate.TabIndex = 12;
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(329, 13);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(28, 15);
      this.label1.TabIndex = 15;
      this.label1.TabStop = false;
      this.label1.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(330, 38);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(28, 15);
      this.label2.TabIndex = 14;
      this.label2.TabStop = false;
      this.label2.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(134, 38);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(28, 15);
      this.label4.TabIndex = 16;
      this.label4.TabStop = false;
      this.label4.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // pnlBill
      // 
      this.pnlBill.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(112)))), ((int)(((byte)(171)))));
      this.pnlBill.Controls.Add(this.cmbBill);
      this.pnlBill.Controls.Add(this.lblPayType);
      this.pnlBill.CornerRadius = 10;
      this.pnlBill.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlBill.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlBill.Location = new System.Drawing.Point(168, 63);
      this.pnlBill.Name = "pnlBill";
      this.pnlBill.ShowBorder = false;
      this.pnlBill.Size = new System.Drawing.Size(190, 31);
      this.pnlBill.TabIndex = 19;
      this.pnlBill.Visible = false;
      // 
      // cmbBill
      // 
      this.cmbBill.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbBill.FormattingEnabled = true;
      this.cmbBill.Location = new System.Drawing.Point(1, 0);
      this.cmbBill.Name = "cmbBill";
      this.cmbBill.Size = new System.Drawing.Size(121, 21);
      this.cmbBill.TabIndex = 4;
      // 
      // lblPayType
      // 
      this.lblPayType.Location = new System.Drawing.Point(143, 2);
      this.lblPayType.Name = "lblPayType";
      this.lblPayType.Size = new System.Drawing.Size(48, 15);
      this.lblPayType.TabIndex = 3;
      this.lblPayType.TabStop = false;
      this.lblPayType.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblPayType.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblPayType.Texts")));
      // 
      // pnlCompetition
      // 
      this.pnlCompetition.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(112)))), ((int)(((byte)(171)))));
      this.pnlCompetition.Controls.Add(this.cmbCompetition);
      this.pnlCompetition.Controls.Add(this.transparentLabel1);
      this.pnlCompetition.CornerRadius = 10;
      this.pnlCompetition.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlCompetition.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlCompetition.Location = new System.Drawing.Point(168, 63);
      this.pnlCompetition.Name = "pnlCompetition";
      this.pnlCompetition.ShowBorder = false;
      this.pnlCompetition.Size = new System.Drawing.Size(190, 23);
      this.pnlCompetition.TabIndex = 20;
      this.pnlCompetition.Visible = false;
      // 
      // cmbCompetition
      // 
      this.cmbCompetition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbCompetition.FormattingEnabled = true;
      this.cmbCompetition.Location = new System.Drawing.Point(1, 0);
      this.cmbCompetition.Name = "cmbCompetition";
      this.cmbCompetition.Size = new System.Drawing.Size(121, 21);
      this.cmbCompetition.TabIndex = 4;
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Location = new System.Drawing.Point(148, 1);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(44, 15);
      this.transparentLabel1.TabIndex = 3;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // pnlDescription
      // 
      this.pnlDescription.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(112)))), ((int)(((byte)(171)))));
      this.pnlDescription.Controls.Add(this.txtComment);
      this.pnlDescription.Controls.Add(this.label3);
      this.pnlDescription.CornerRadius = 10;
      this.pnlDescription.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlDescription.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlDescription.Location = new System.Drawing.Point(11, 63);
      this.pnlDescription.Name = "pnlDescription";
      this.pnlDescription.ShowBorder = false;
      this.pnlDescription.Size = new System.Drawing.Size(346, 77);
      this.pnlDescription.TabIndex = 18;
      this.pnlDescription.Visible = false;
      // 
      // txtComment
      // 
      this.txtComment.Location = new System.Drawing.Point(3, 1);
      this.txtComment.Multiline = true;
      this.txtComment.Name = "txtComment";
      this.txtComment.Size = new System.Drawing.Size(276, 72);
      this.txtComment.TabIndex = 12;
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(301, 1);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(49, 15);
      this.label3.TabIndex = 11;
      this.label3.TabStop = false;
      this.label3.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(224, 169);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 24;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnOK
      // 
      this.btnOK.ForeColor = System.Drawing.Color.Black;
      this.btnOK.Location = new System.Drawing.Point(305, 169);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 25;
      this.btnOK.Text = "تایید";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // FormPayment
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(389, 198);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOK);
      this.Controls.Add(this.pnlInfo);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormPayment";
      this.Text = "پرداخت";
      this.Load += new System.EventHandler(this.FormPayment_Load);
      this.pnlInfo.ResumeLayout(false);
      this.pnlInfo.PerformLayout();
      this.pnlBill.ResumeLayout(false);
      this.pnlCompetition.ResumeLayout(false);
      this.pnlDescription.ResumeLayout(false);
      this.pnlDescription.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private ToolBar pnlInfo;
    private ToolBar pnlBill;
    private System.Windows.Forms.ComboBox cmbBill;
    private TransparentLabel lblPayType;
    private ToolBar pnlDescription;
    private System.Windows.Forms.TextBox txtComment;
    private TransparentLabel label3;
    private System.Windows.Forms.ComboBox cmbEstate;
    private TransparentLabel label1;
    private TransparentLabel label2;
    private TransparentLabel label4;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnOK;
    private Utilities.NumberBox txtAmount;
    private ToolBar pnlCompetition;
    private System.Windows.Forms.ComboBox cmbCompetition;
    private TransparentLabel transparentLabel1;
  }
}