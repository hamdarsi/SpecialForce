﻿namespace SpecialForce.Accounting
{
  partial class FormAccountingReportSettings
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAccountingReportSettings));
      this.btnReport = new System.Windows.Forms.Button();
      this.pnlInfo = new SpecialForce.ToolBar();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.chkCupAdmissions = new SpecialForce.TransparentCheckBox();
      this.chkEliminationAdmissions = new SpecialForce.TransparentCheckBox();
      this.chkLeagueAdmissions = new SpecialForce.TransparentCheckBox();
      this.chkUserCharges = new SpecialForce.TransparentCheckBox();
      this.toolBar1 = new SpecialForce.ToolBar();
      this.chkSites = new System.Windows.Forms.CheckedListBox();
      this.label7 = new SpecialForce.TransparentLabel();
      this.toolBar2 = new SpecialForce.ToolBar();
      this.dtEnd = new SpecialForce.PersianDatePicker();
      this.dtStart = new SpecialForce.PersianDatePicker();
      this.lblEnd = new SpecialForce.TransparentLabel();
      this.lblStart = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.cmbReportType = new System.Windows.Forms.ComboBox();
      this.btnCancel = new System.Windows.Forms.Button();
      this.toolBar3 = new SpecialForce.ToolBar();
      this.chkRefunds = new SpecialForce.TransparentCheckBox();
      this.chkOtherExpenses = new SpecialForce.TransparentCheckBox();
      this.chkRents = new SpecialForce.TransparentCheckBox();
      this.chkBills = new SpecialForce.TransparentCheckBox();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.chkCupExpenses = new SpecialForce.TransparentCheckBox();
      this.chkElimExpenses = new SpecialForce.TransparentCheckBox();
      this.chkLeagueExpenses = new SpecialForce.TransparentCheckBox();
      this.chkGiftCharges = new SpecialForce.TransparentCheckBox();
      this.pnlInfo.SuspendLayout();
      this.toolBar1.SuspendLayout();
      this.toolBar2.SuspendLayout();
      this.toolBar3.SuspendLayout();
      this.SuspendLayout();
      // 
      // btnReport
      // 
      this.btnReport.Enabled = false;
      this.btnReport.ForeColor = System.Drawing.Color.Black;
      this.btnReport.Location = new System.Drawing.Point(374, 415);
      this.btnReport.Name = "btnReport";
      this.btnReport.Size = new System.Drawing.Size(75, 23);
      this.btnReport.TabIndex = 19;
      this.btnReport.Text = "گزارش گیری";
      this.btnReport.UseVisualStyleBackColor = true;
      this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
      // 
      // pnlInfo
      // 
      this.pnlInfo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlInfo.BorderWidth = 0.5F;
      this.pnlInfo.Controls.Add(this.transparentLabel1);
      this.pnlInfo.Controls.Add(this.chkCupAdmissions);
      this.pnlInfo.Controls.Add(this.chkEliminationAdmissions);
      this.pnlInfo.Controls.Add(this.chkLeagueAdmissions);
      this.pnlInfo.Controls.Add(this.chkUserCharges);
      this.pnlInfo.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlInfo.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlInfo.Location = new System.Drawing.Point(12, 116);
      this.pnlInfo.Name = "pnlInfo";
      this.pnlInfo.Size = new System.Drawing.Size(436, 61);
      this.pnlInfo.TabIndex = 46;
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Location = new System.Drawing.Point(256, 8);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(173, 15);
      this.transparentLabel1.TabIndex = 57;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // chkCupAdmissions
      // 
      this.chkCupAdmissions.Checked = true;
      this.chkCupAdmissions.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkCupAdmissions.ForeColor = System.Drawing.Color.White;
      this.chkCupAdmissions.Location = new System.Drawing.Point(351, 29);
      this.chkCupAdmissions.Name = "chkCupAdmissions";
      this.chkCupAdmissions.Size = new System.Drawing.Size(76, 18);
      this.chkCupAdmissions.TabIndex = 56;
      this.chkCupAdmissions.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkCupAdmissions.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkCupAdmissions.Texts")));
      // 
      // chkEliminationAdmissions
      // 
      this.chkEliminationAdmissions.Checked = true;
      this.chkEliminationAdmissions.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkEliminationAdmissions.ForeColor = System.Drawing.Color.White;
      this.chkEliminationAdmissions.Location = new System.Drawing.Point(236, 29);
      this.chkEliminationAdmissions.Name = "chkEliminationAdmissions";
      this.chkEliminationAdmissions.Size = new System.Drawing.Size(87, 18);
      this.chkEliminationAdmissions.TabIndex = 55;
      this.chkEliminationAdmissions.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkEliminationAdmissions.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkEliminationAdmissions.Texts")));
      // 
      // chkLeagueAdmissions
      // 
      this.chkLeagueAdmissions.Checked = true;
      this.chkLeagueAdmissions.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkLeagueAdmissions.ForeColor = System.Drawing.Color.White;
      this.chkLeagueAdmissions.Location = new System.Drawing.Point(133, 29);
      this.chkLeagueAdmissions.Name = "chkLeagueAdmissions";
      this.chkLeagueAdmissions.Size = new System.Drawing.Size(75, 18);
      this.chkLeagueAdmissions.TabIndex = 54;
      this.chkLeagueAdmissions.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkLeagueAdmissions.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkLeagueAdmissions.Texts")));
      // 
      // chkUserCharges
      // 
      this.chkUserCharges.Checked = true;
      this.chkUserCharges.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkUserCharges.ForeColor = System.Drawing.Color.White;
      this.chkUserCharges.Location = new System.Drawing.Point(23, 29);
      this.chkUserCharges.Name = "chkUserCharges";
      this.chkUserCharges.Size = new System.Drawing.Size(82, 18);
      this.chkUserCharges.TabIndex = 53;
      this.chkUserCharges.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkUserCharges.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkUserCharges.Texts")));
      // 
      // toolBar1
      // 
      this.toolBar1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar1.BorderWidth = 0.5F;
      this.toolBar1.Controls.Add(this.chkSites);
      this.toolBar1.Controls.Add(this.label7);
      this.toolBar1.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar1.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar1.Location = new System.Drawing.Point(12, 287);
      this.toolBar1.Name = "toolBar1";
      this.toolBar1.Size = new System.Drawing.Size(436, 119);
      this.toolBar1.TabIndex = 47;
      // 
      // chkSites
      // 
      this.chkSites.BackColor = System.Drawing.SystemColors.Window;
      this.chkSites.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.chkSites.CheckOnClick = true;
      this.chkSites.ForeColor = System.Drawing.SystemColors.WindowText;
      this.chkSites.FormattingEnabled = true;
      this.chkSites.Location = new System.Drawing.Point(13, 24);
      this.chkSites.Name = "chkSites";
      this.chkSites.Size = new System.Drawing.Size(386, 80);
      this.chkSites.TabIndex = 38;
      this.chkSites.SelectedIndexChanged += new System.EventHandler(this.chkSites_SelectedIndexChanged);
      // 
      // label7
      // 
      this.label7.Location = new System.Drawing.Point(243, 3);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(181, 15);
      this.label7.TabIndex = 37;
      this.label7.TabStop = false;
      this.label7.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label7.Texts")));
      // 
      // toolBar2
      // 
      this.toolBar2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar2.BorderWidth = 0.5F;
      this.toolBar2.Controls.Add(this.dtEnd);
      this.toolBar2.Controls.Add(this.dtStart);
      this.toolBar2.Controls.Add(this.lblEnd);
      this.toolBar2.Controls.Add(this.lblStart);
      this.toolBar2.Controls.Add(this.label1);
      this.toolBar2.Controls.Add(this.cmbReportType);
      this.toolBar2.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar2.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar2.Location = new System.Drawing.Point(12, 12);
      this.toolBar2.Name = "toolBar2";
      this.toolBar2.Size = new System.Drawing.Size(436, 91);
      this.toolBar2.TabIndex = 48;
      // 
      // dtEnd
      // 
      this.dtEnd.Location = new System.Drawing.Point(13, 35);
      this.dtEnd.Name = "dtEnd";
      this.dtEnd.Size = new System.Drawing.Size(132, 43);
      this.dtEnd.TabIndex = 32;
      this.dtEnd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtEnd.Texts")));
      this.dtEnd.Visible = false;
      // 
      // dtStart
      // 
      this.dtStart.Location = new System.Drawing.Point(226, 35);
      this.dtStart.Name = "dtStart";
      this.dtStart.Size = new System.Drawing.Size(132, 43);
      this.dtStart.TabIndex = 29;
      this.dtStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("dtStart.Texts")));
      // 
      // lblEnd
      // 
      this.lblEnd.Location = new System.Drawing.Point(158, 51);
      this.lblEnd.Name = "lblEnd";
      this.lblEnd.Size = new System.Drawing.Size(14, 15);
      this.lblEnd.TabIndex = 28;
      this.lblEnd.TabStop = false;
      this.lblEnd.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblEnd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblEnd.Texts")));
      this.lblEnd.Visible = false;
      // 
      // lblStart
      // 
      this.lblStart.Location = new System.Drawing.Point(378, 51);
      this.lblStart.Name = "lblStart";
      this.lblStart.Size = new System.Drawing.Size(46, 15);
      this.lblStart.TabIndex = 27;
      this.lblStart.TabStop = false;
      this.lblStart.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblStart.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(367, 11);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(57, 15);
      this.label1.TabIndex = 4;
      this.label1.TabStop = false;
      this.label1.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // cmbReportType
      // 
      this.cmbReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbReportType.FormattingEnabled = true;
      this.cmbReportType.Items.AddRange(new object[] {
            "روزانه",
            "ماهانه",
            "سالیانه",
            "دلخواه",
            "از آخرین برداشت"});
      this.cmbReportType.Location = new System.Drawing.Point(224, 8);
      this.cmbReportType.Name = "cmbReportType";
      this.cmbReportType.Size = new System.Drawing.Size(135, 21);
      this.cmbReportType.TabIndex = 3;
      this.cmbReportType.SelectedIndexChanged += new System.EventHandler(this.cmbReportType_SelectedIndexChanged);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(293, 415);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 49;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // toolBar3
      // 
      this.toolBar3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.toolBar3.BorderWidth = 0.5F;
      this.toolBar3.Controls.Add(this.chkRefunds);
      this.toolBar3.Controls.Add(this.chkOtherExpenses);
      this.toolBar3.Controls.Add(this.chkRents);
      this.toolBar3.Controls.Add(this.chkBills);
      this.toolBar3.Controls.Add(this.transparentLabel2);
      this.toolBar3.Controls.Add(this.chkCupExpenses);
      this.toolBar3.Controls.Add(this.chkElimExpenses);
      this.toolBar3.Controls.Add(this.chkLeagueExpenses);
      this.toolBar3.Controls.Add(this.chkGiftCharges);
      this.toolBar3.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.toolBar3.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.toolBar3.Location = new System.Drawing.Point(12, 189);
      this.toolBar3.Name = "toolBar3";
      this.toolBar3.Size = new System.Drawing.Size(436, 86);
      this.toolBar3.TabIndex = 58;
      // 
      // chkRefunds
      // 
      this.chkRefunds.Checked = true;
      this.chkRefunds.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkRefunds.ForeColor = System.Drawing.Color.White;
      this.chkRefunds.Location = new System.Drawing.Point(43, 53);
      this.chkRefunds.Name = "chkRefunds";
      this.chkRefunds.Size = new System.Drawing.Size(62, 18);
      this.chkRefunds.TabIndex = 61;
      this.chkRefunds.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkRefunds.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkRefunds.Texts")));
      // 
      // chkOtherExpenses
      // 
      this.chkOtherExpenses.Checked = true;
      this.chkOtherExpenses.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkOtherExpenses.ForeColor = System.Drawing.Color.White;
      this.chkOtherExpenses.Location = new System.Drawing.Point(146, 53);
      this.chkOtherExpenses.Name = "chkOtherExpenses";
      this.chkOtherExpenses.Size = new System.Drawing.Size(62, 18);
      this.chkOtherExpenses.TabIndex = 60;
      this.chkOtherExpenses.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkOtherExpenses.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkOtherExpenses.Texts")));
      // 
      // chkRents
      // 
      this.chkRents.Checked = true;
      this.chkRents.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkRents.ForeColor = System.Drawing.Color.White;
      this.chkRents.Location = new System.Drawing.Point(372, 53);
      this.chkRents.Name = "chkRents";
      this.chkRents.Size = new System.Drawing.Size(52, 18);
      this.chkRents.TabIndex = 59;
      this.chkRents.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkRents.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkRents.Texts")));
      // 
      // chkBills
      // 
      this.chkBills.Checked = true;
      this.chkBills.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkBills.ForeColor = System.Drawing.Color.White;
      this.chkBills.Location = new System.Drawing.Point(259, 53);
      this.chkBills.Name = "chkBills";
      this.chkBills.Size = new System.Drawing.Size(63, 18);
      this.chkBills.TabIndex = 58;
      this.chkBills.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkBills.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkBills.Texts")));
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Location = new System.Drawing.Point(256, 8);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(175, 15);
      this.transparentLabel2.TabIndex = 57;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // chkCupExpenses
      // 
      this.chkCupExpenses.Checked = true;
      this.chkCupExpenses.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkCupExpenses.ForeColor = System.Drawing.Color.White;
      this.chkCupExpenses.Location = new System.Drawing.Point(375, 29);
      this.chkCupExpenses.Name = "chkCupExpenses";
      this.chkCupExpenses.Size = new System.Drawing.Size(49, 18);
      this.chkCupExpenses.TabIndex = 56;
      this.chkCupExpenses.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkCupExpenses.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkCupExpenses.Texts")));
      // 
      // chkElimExpenses
      // 
      this.chkElimExpenses.Checked = true;
      this.chkElimExpenses.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkElimExpenses.ForeColor = System.Drawing.Color.White;
      this.chkElimExpenses.Location = new System.Drawing.Point(261, 29);
      this.chkElimExpenses.Name = "chkElimExpenses";
      this.chkElimExpenses.Size = new System.Drawing.Size(61, 18);
      this.chkElimExpenses.TabIndex = 55;
      this.chkElimExpenses.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkElimExpenses.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkElimExpenses.Texts")));
      // 
      // chkLeagueExpenses
      // 
      this.chkLeagueExpenses.Checked = true;
      this.chkLeagueExpenses.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkLeagueExpenses.ForeColor = System.Drawing.Color.White;
      this.chkLeagueExpenses.Location = new System.Drawing.Point(158, 29);
      this.chkLeagueExpenses.Name = "chkLeagueExpenses";
      this.chkLeagueExpenses.Size = new System.Drawing.Size(50, 18);
      this.chkLeagueExpenses.TabIndex = 54;
      this.chkLeagueExpenses.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkLeagueExpenses.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkLeagueExpenses.Texts")));
      // 
      // chkGiftCharges
      // 
      this.chkGiftCharges.Checked = true;
      this.chkGiftCharges.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkGiftCharges.ForeColor = System.Drawing.Color.White;
      this.chkGiftCharges.Location = new System.Drawing.Point(32, 29);
      this.chkGiftCharges.Name = "chkGiftCharges";
      this.chkGiftCharges.Size = new System.Drawing.Size(73, 18);
      this.chkGiftCharges.TabIndex = 53;
      this.chkGiftCharges.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.chkGiftCharges.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("chkGiftCharges.Texts")));
      // 
      // FormAccountingReportSettings
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(461, 449);
      this.Controls.Add(this.toolBar3);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.toolBar2);
      this.Controls.Add(this.toolBar1);
      this.Controls.Add(this.pnlInfo);
      this.Controls.Add(this.btnReport);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormAccountingReportSettings";
      this.Text = "گزارش های مالی";
      this.Load += new System.EventHandler(this.FormAccountingReport_Load);
      this.pnlInfo.ResumeLayout(false);
      this.toolBar1.ResumeLayout(false);
      this.toolBar2.ResumeLayout(false);
      this.toolBar3.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnReport;
    private ToolBar pnlInfo;
    private TransparentLabel transparentLabel1;
    private TransparentCheckBox chkCupAdmissions;
    private TransparentCheckBox chkEliminationAdmissions;
    private TransparentCheckBox chkLeagueAdmissions;
    private TransparentCheckBox chkUserCharges;
    private ToolBar toolBar1;
    private System.Windows.Forms.CheckedListBox chkSites;
    private TransparentLabel label7;
    private ToolBar toolBar2;
    private PersianDatePicker dtEnd;
    private TransparentLabel lblEnd;
    private TransparentLabel lblStart;
    private TransparentLabel label1;
    private System.Windows.Forms.ComboBox cmbReportType;
    private PersianDatePicker dtStart;
    private System.Windows.Forms.Button btnCancel;
    private ToolBar toolBar3;
    private TransparentCheckBox chkOtherExpenses;
    private TransparentCheckBox chkRents;
    private TransparentCheckBox chkBills;
    private TransparentLabel transparentLabel2;
    private TransparentCheckBox chkCupExpenses;
    private TransparentCheckBox chkElimExpenses;
    private TransparentCheckBox chkLeagueExpenses;
    private TransparentCheckBox chkGiftCharges;
    private TransparentCheckBox chkRefunds;
  }
}