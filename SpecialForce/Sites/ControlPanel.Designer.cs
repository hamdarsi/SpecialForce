﻿namespace SpecialForce.Sites
{
  partial class ControlPanel
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPanel));
      this.pnlInfo = new SpecialForce.ToolBar();
      this.lblTelephone = new SpecialForce.TransparentLabel();
      this.lblStatus = new SpecialForce.TransparentLabel();
      this.lblUseDate = new SpecialForce.TransparentLabel();
      this.lblSiteShortName = new SpecialForce.TransparentLabel();
      this.lblSiteName = new SpecialForce.TransparentLabel();
      this.label5 = new SpecialForce.TransparentLabel();
      this.label4 = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.label2 = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.tinypicker1 = new SpecialForce.TinyPicker();
      this.tbButtons = new SpecialForce.ToolBar();
      this.toolButton5 = new SpecialForce.ToolButton();
      this.btnSiteCancel = new SpecialForce.ToolButton();
      this.btnSiteEdit = new SpecialForce.ToolButton();
      this.btnSiteAdd = new SpecialForce.ToolButton();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.txtSearch = new System.Windows.Forms.TextBox();
      this.lvSites = new SpecialForce.UserInterface.ListView();
      this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.pnlInfo.SuspendLayout();
      this.tbButtons.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lvSites)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlInfo
      // 
      this.pnlInfo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlInfo.BorderWidth = 0.5F;
      this.pnlInfo.Controls.Add(this.lblTelephone);
      this.pnlInfo.Controls.Add(this.lblStatus);
      this.pnlInfo.Controls.Add(this.lblUseDate);
      this.pnlInfo.Controls.Add(this.lblSiteShortName);
      this.pnlInfo.Controls.Add(this.lblSiteName);
      this.pnlInfo.Controls.Add(this.label5);
      this.pnlInfo.Controls.Add(this.label4);
      this.pnlInfo.Controls.Add(this.label3);
      this.pnlInfo.Controls.Add(this.label2);
      this.pnlInfo.Controls.Add(this.label1);
      this.pnlInfo.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlInfo.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlInfo.Location = new System.Drawing.Point(105, 253);
      this.pnlInfo.Name = "pnlInfo";
      this.pnlInfo.Size = new System.Drawing.Size(315, 117);
      this.pnlInfo.TabIndex = 9;
      // 
      // lblTelephone
      // 
      this.lblTelephone.FixFromRight = true;
      this.lblTelephone.ForeColor = System.Drawing.Color.White;
      this.lblTelephone.Location = new System.Drawing.Point(148, 93);
      this.lblTelephone.Name = "lblTelephone";
      this.lblTelephone.Size = new System.Drawing.Size(8, 15);
      this.lblTelephone.TabIndex = 29;
      this.lblTelephone.TabStop = false;
      this.lblTelephone.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblTelephone.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblTelephone.Texts")));
      // 
      // lblStatus
      // 
      this.lblStatus.FixFromRight = true;
      this.lblStatus.ForeColor = System.Drawing.Color.White;
      this.lblStatus.Location = new System.Drawing.Point(148, 72);
      this.lblStatus.Name = "lblStatus";
      this.lblStatus.Size = new System.Drawing.Size(8, 15);
      this.lblStatus.TabIndex = 28;
      this.lblStatus.TabStop = false;
      this.lblStatus.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblStatus.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblStatus.Texts")));
      // 
      // lblUseDate
      // 
      this.lblUseDate.FixFromRight = true;
      this.lblUseDate.ForeColor = System.Drawing.Color.White;
      this.lblUseDate.Location = new System.Drawing.Point(148, 51);
      this.lblUseDate.Name = "lblUseDate";
      this.lblUseDate.Size = new System.Drawing.Size(8, 15);
      this.lblUseDate.TabIndex = 27;
      this.lblUseDate.TabStop = false;
      this.lblUseDate.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblUseDate.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblUseDate.Texts")));
      // 
      // lblSiteShortName
      // 
      this.lblSiteShortName.FixFromRight = true;
      this.lblSiteShortName.ForeColor = System.Drawing.Color.White;
      this.lblSiteShortName.Location = new System.Drawing.Point(148, 30);
      this.lblSiteShortName.Name = "lblSiteShortName";
      this.lblSiteShortName.Size = new System.Drawing.Size(8, 15);
      this.lblSiteShortName.TabIndex = 26;
      this.lblSiteShortName.TabStop = false;
      this.lblSiteShortName.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblSiteShortName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblSiteShortName.Texts")));
      // 
      // lblSiteName
      // 
      this.lblSiteName.FixFromRight = true;
      this.lblSiteName.ForeColor = System.Drawing.Color.White;
      this.lblSiteName.Location = new System.Drawing.Point(148, 9);
      this.lblSiteName.Name = "lblSiteName";
      this.lblSiteName.Size = new System.Drawing.Size(8, 15);
      this.lblSiteName.TabIndex = 25;
      this.lblSiteName.TabStop = false;
      this.lblSiteName.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblSiteName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblSiteName.Texts")));
      // 
      // label5
      // 
      this.label5.ForeColor = System.Drawing.Color.White;
      this.label5.Location = new System.Drawing.Point(239, 93);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(63, 15);
      this.label5.TabIndex = 24;
      this.label5.TabStop = false;
      this.label5.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label5.Texts")));
      // 
      // label4
      // 
      this.label4.ForeColor = System.Drawing.Color.White;
      this.label4.Location = new System.Drawing.Point(255, 30);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(47, 15);
      this.label4.TabIndex = 23;
      this.label4.TabStop = false;
      this.label4.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // label3
      // 
      this.label3.ForeColor = System.Drawing.Color.White;
      this.label3.Location = new System.Drawing.Point(261, 72);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(41, 15);
      this.label3.TabIndex = 22;
      this.label3.TabStop = false;
      this.label3.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // label2
      // 
      this.label2.ForeColor = System.Drawing.Color.White;
      this.label2.Location = new System.Drawing.Point(216, 51);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(86, 15);
      this.label2.TabIndex = 21;
      this.label2.TabStop = false;
      this.label2.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // label1
      // 
      this.label1.ForeColor = System.Drawing.Color.White;
      this.label1.Location = new System.Drawing.Point(245, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(57, 15);
      this.label1.TabIndex = 20;
      this.label1.TabStop = false;
      this.label1.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // tinypicker1
      // 
      this.tinypicker1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tinypicker1.Location = new System.Drawing.Point(12, 379);
      this.tinypicker1.Name = "tinypicker1";
      this.tinypicker1.Size = new System.Drawing.Size(406, 30);
      this.tinypicker1.TabIndex = 20;
      this.tinypicker1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tinypicker1.Texts")));
      // 
      // tbButtons
      // 
      this.tbButtons.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbButtons.BorderWidth = 0.5F;
      this.tbButtons.Controls.Add(this.toolButton5);
      this.tbButtons.Controls.Add(this.btnSiteCancel);
      this.tbButtons.Controls.Add(this.btnSiteEdit);
      this.tbButtons.Controls.Add(this.btnSiteAdd);
      this.tbButtons.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbButtons.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbButtons.Location = new System.Drawing.Point(13, 12);
      this.tbButtons.Name = "tbButtons";
      this.tbButtons.Size = new System.Drawing.Size(81, 358);
      this.tbButtons.TabIndex = 25;
      // 
      // toolButton5
      // 
      this.toolButton5.Hint = "قفل سیستم";
      this.toolButton5.Image = ((System.Drawing.Bitmap)(resources.GetObject("toolButton5.Image")));
      this.toolButton5.Location = new System.Drawing.Point(8, 285);
      this.toolButton5.Name = "toolButton5";
      this.toolButton5.NormalBorderColor = System.Drawing.Color.Gray;
      this.toolButton5.Size = new System.Drawing.Size(64, 64);
      this.toolButton5.TabIndex = 31;
      this.toolButton5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("toolButton5.Texts")));
      this.toolButton5.TransparentColor = System.Drawing.Color.White;
      this.toolButton5.Click += new System.EventHandler(this.btnLogout_Click);
      // 
      // btnSiteCancel
      // 
      this.btnSiteCancel.Hint = "تعطیل کردن باشگاه";
      this.btnSiteCancel.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnSiteCancel.Image")));
      this.btnSiteCancel.Location = new System.Drawing.Point(8, 145);
      this.btnSiteCancel.Name = "btnSiteCancel";
      this.btnSiteCancel.NormalBorderColor = System.Drawing.Color.Gray;
      this.btnSiteCancel.Size = new System.Drawing.Size(64, 64);
      this.btnSiteCancel.TabIndex = 29;
      this.btnSiteCancel.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnSiteCancel.Texts")));
      this.btnSiteCancel.TransparentColor = System.Drawing.Color.White;
      this.btnSiteCancel.Click += new System.EventHandler(this.btnSiteDisable_Click);
      // 
      // btnSiteEdit
      // 
      this.btnSiteEdit.Hint = "ویرایش باشگاه";
      this.btnSiteEdit.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnSiteEdit.Image")));
      this.btnSiteEdit.Location = new System.Drawing.Point(8, 75);
      this.btnSiteEdit.Name = "btnSiteEdit";
      this.btnSiteEdit.NormalBorderColor = System.Drawing.Color.Gray;
      this.btnSiteEdit.Size = new System.Drawing.Size(64, 64);
      this.btnSiteEdit.TabIndex = 28;
      this.btnSiteEdit.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnSiteEdit.Texts")));
      this.btnSiteEdit.TransparentColor = System.Drawing.Color.White;
      this.btnSiteEdit.Click += new System.EventHandler(this.btnSiteEdit_Click);
      // 
      // btnSiteAdd
      // 
      this.btnSiteAdd.Hint = "افزودن باشگاه";
      this.btnSiteAdd.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnSiteAdd.Image")));
      this.btnSiteAdd.Location = new System.Drawing.Point(8, 5);
      this.btnSiteAdd.Name = "btnSiteAdd";
      this.btnSiteAdd.NormalBorderColor = System.Drawing.Color.Gray;
      this.btnSiteAdd.Size = new System.Drawing.Size(64, 64);
      this.btnSiteAdd.TabIndex = 27;
      this.btnSiteAdd.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnSiteAdd.Texts")));
      this.btnSiteAdd.TransparentColor = System.Drawing.Color.White;
      this.btnSiteAdd.Click += new System.EventHandler(this.btnSiteAdd_Click);
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Location = new System.Drawing.Point(105, 15);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(44, 15);
      this.transparentLabel1.TabIndex = 26;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // txtSearch
      // 
      this.txtSearch.Location = new System.Drawing.Point(162, 12);
      this.txtSearch.Name = "txtSearch";
      this.txtSearch.Size = new System.Drawing.Size(258, 21);
      this.txtSearch.TabIndex = 27;
      this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
      // 
      // lvSites
      // 
      this.lvSites.AllColumns.Add(this.olvColumn1);
      this.lvSites.AllColumns.Add(this.olvColumn2);
      this.lvSites.AllColumns.Add(this.olvColumn3);
      this.lvSites.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this.olvColumn3});
      this.lvSites.FullRowSelect = true;
      this.lvSites.GridLines = true;
      this.lvSites.Location = new System.Drawing.Point(105, 39);
      this.lvSites.Name = "lvSites";
      this.lvSites.RightToLeftLayout = true;
      this.lvSites.ShowGroups = false;
      this.lvSites.Size = new System.Drawing.Size(313, 208);
      this.lvSites.TabIndex = 28;
      this.lvSites.UseCompatibleStateImageBehavior = false;
      this.lvSites.View = System.Windows.Forms.View.Details;
      this.lvSites.FormatRow += new System.EventHandler<BrightIdeasSoftware.FormatRowEventArgs>(this.lvSites_FormatRow);
      this.lvSites.SelectedIndexChanged += new System.EventHandler(this.lvSites_SelectedIndexChanged);
      // 
      // olvColumn1
      // 
      this.olvColumn1.AspectName = "ID";
      this.olvColumn1.CellPadding = null;
      this.olvColumn1.Text = "ردیف";
      this.olvColumn1.Width = 36;
      // 
      // olvColumn2
      // 
      this.olvColumn2.AspectName = "ShortName";
      this.olvColumn2.CellPadding = null;
      this.olvColumn2.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.olvColumn2.Text = "نام";
      this.olvColumn2.Width = 72;
      // 
      // olvColumn3
      // 
      this.olvColumn3.AspectName = "Address";
      this.olvColumn3.CellPadding = null;
      this.olvColumn3.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.olvColumn3.Text = "آدرس";
      this.olvColumn3.Width = 166;
      // 
      // ControlPanel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(434, 421);
      this.Controls.Add(this.lvSites);
      this.Controls.Add(this.txtSearch);
      this.Controls.Add(this.transparentLabel1);
      this.Controls.Add(this.tinypicker1);
      this.Controls.Add(this.pnlInfo);
      this.Controls.Add(this.tbButtons);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "ControlPanel";
      this.Text = "مدیریت باشگاه ها";
      this.Load += new System.EventHandler(this.ControlPanelSites_Load);
      this.pnlInfo.ResumeLayout(false);
      this.tbButtons.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.lvSites)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private ToolBar pnlInfo;
    private TinyPicker tinypicker1;
    private ToolBar tbButtons;
    private TransparentLabel lblTelephone;
    private TransparentLabel lblStatus;
    private TransparentLabel lblUseDate;
    private TransparentLabel lblSiteShortName;
    private TransparentLabel lblSiteName;
    private TransparentLabel label5;
    private TransparentLabel label4;
    private TransparentLabel label3;
    private TransparentLabel label2;
    private TransparentLabel label1;
    private ToolButton toolButton5;
    private ToolButton btnSiteCancel;
    private ToolButton btnSiteEdit;
    private ToolButton btnSiteAdd;
    private TransparentLabel transparentLabel1;
    private System.Windows.Forms.TextBox txtSearch;
    private UserInterface.ListView lvSites;
    private BrightIdeasSoftware.OLVColumn olvColumn1;
    private BrightIdeasSoftware.OLVColumn olvColumn2;
    private BrightIdeasSoftware.OLVColumn olvColumn3;
  }
}