﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Sites
{
  public partial class ControlPanel : SessionAwareForm
  {
    static private ControlPanel mInstance = null;
    static public ControlPanel Instance
    {
      get
      {
        if (mInstance == null || mInstance.IsDisposed)
          mInstance = new ControlPanel();

        return mInstance;
      }
    }

    private ControlPanel()
    {
      InitializeComponent();
    }

    protected override void ApplyPermissions(Post post)
    {
      btnSiteAdd.Enabled = post.IsGod();
      btnSiteCancel.Enabled = post.IsGod();
    }

    private void RefreshSiteList()
    {
      lvSites.SetObjects(DB.Site.GetAllSites(true, txtSearch.Text));
    }

    private void UpdateInfoPane()
    {
      lblSiteName.Text = "-";
      lblSiteShortName.Text = "-";
      lblTelephone.Text = "-";
      lblUseDate.Text = "-";
      lblStatus.Text = "-";

      DB.Site site = lvSites.SelectedObject as DB.Site;
      if (site != null)
      {
        lblSiteName.Text = site.FullName;
        lblSiteShortName.Text = site.ShortName;
        lblTelephone.Text = site.Telephone;
        lblUseDate.Text = site.StartDate.ToString(DateTimeString.Date);
        lblStatus.Text = site.IsInUse ? "در حال استفاده" : "تعطیل شده در " + site.EndDate.ToString(DateTimeString.Date);
      }
    }

    private void ControlPanelSites_Load(object sender, EventArgs e)
    {
      RefreshSiteList();
      UpdateInfoPane();
    }

    private void btnSiteAdd_Click(object sender, EventArgs e)
    {
      if (new FormSiteAddEdit().ShowDialog() == DialogResult.OK)
        RefreshSiteList();
    }

    private void btnSiteEdit_Click(object sender, EventArgs e)
    {
      DB.Site site = lvSites.SelectedObject as DB.Site;

      if (site == null)
        Session.ShowError("هیچ باشگاهی انتخاب نشده است.");
      else if (site.IsInUse == false)
        Session.ShowError("باشگاه " + site.ShortName + " تعطیل شده است.");
      else if (!Session.CurrentStaff.IsGod() && site.ID != Session.ActiveSiteID)
        Session.ShowError("شما فقط می توانید باشگاه خود را ویرایش کنید.");
      else
      {
        FormSiteAddEdit frm = new FormSiteAddEdit();
        frm.mSite = site;
        if (frm.ShowDialog() == DialogResult.OK)
        {
          RefreshSiteList();
          UpdateInfoPane();
        }
      }
    }

    private void sites_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      btnSiteEdit_Click(sender, e);
    }

    private void btnLogout_Click(object sender, EventArgs e)
    {
      Session.Lock();
    }

    private void btnSiteDisable_Click(object sender, EventArgs e)
    {
      DB.Site site = lvSites.SelectedObject as DB.Site;

      if (site == null)
        Session.ShowError("هیچ باشگاهی انتخاب نشده است.");
      else if (!site.IsInUse)
        Session.ShowError("این باشگاه تعطیل شده است.");
      else
      {
        string str = "آیا واقعا میخواهید باشگاه " + site.FullName + " را تعطیل کنید؟";

        if (Session.Ask(str))
        {
          if (!site.StopUsage())
            Session.ShowError("این باشگاه را نمی توان تعطیل کرد.");
          else
          {
            RefreshSiteList();
            Session.ShowMessage("باشگاه تعطیل شد.");
          }
        }
      }
    }

    private void txtSearch_TextChanged(object sender, EventArgs e)
    {
      RefreshSiteList();
    }

    private void lvSites_FormatRow(object sender, BrightIdeasSoftware.FormatRowEventArgs e)
    {
      DB.Site site = e.Item.RowObject as DB.Site;
      if (site != null && !site.IsInUse)
      {
        //e.Item.SubItems[0].Text = "asd";
        e.Item.BackColor = Color.FromArgb(255, 149, 152); // light red
      }
    }

    private void lvSites_SelectedIndexChanged(object sender, EventArgs e)
    {
      UpdateInfoPane();
    }
  }
}
