﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.Sites
{
  public partial class FormSiteAddEdit : SessionAwareForm
  {
    public DB.Site mSite = null;

    public FormSiteAddEdit()
    {
      InitializeComponent();
    }

    private void FormSiteAdd_Load(object sender, EventArgs e)
    {
      if (mSite != null)
      {
        txtName.Text = mSite.FullName;
        txtShortName.Text = mSite.ShortName;
        txtTelephone.Text = mSite.Telephone;
        txtAddress.Text = mSite.Address;
        Text = "ویرایش باشگاه";
      }
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      if (txtName.Text == "")
        Session.ShowError("نام باشگاه وارد نشده است.");
      else if(txtShortName.Text == "")
        Session.ShowError("نام کوچک باشگاه وارد نشده است.");
      else if(txtTelephone.Text == "")
        Session.ShowError("شماره تلفن باشگاه وارد نشده است.");
      else if (txtAddress.Text == "")
        Session.ShowError("آدرس باشگاه وارد نشده است.");
      else
      {
        if (mSite == null)
          mSite = new DB.Site();

        mSite.FullName = txtName.Text;
        mSite.ShortName = txtShortName.Text;
        mSite.Telephone = txtTelephone.Text;
        mSite.Address = txtAddress.Text;

        mSite.Apply();

        DialogResult = System.Windows.Forms.DialogResult.OK;
      }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
