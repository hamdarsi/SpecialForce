﻿namespace SpecialForce.Sites
{
  partial class FormSiteAddEdit
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSiteAddEdit));
      this.btnOK = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.txtAddress = new System.Windows.Forms.TextBox();
      this.txtTelephone = new System.Windows.Forms.TextBox();
      this.txtShortName = new System.Windows.Forms.TextBox();
      this.txtName = new System.Windows.Forms.TextBox();
      this.tbButtons = new SpecialForce.ToolBar();
      this.label4 = new SpecialForce.TransparentLabel();
      this.label3 = new SpecialForce.TransparentLabel();
      this.label2 = new SpecialForce.TransparentLabel();
      this.label1 = new SpecialForce.TransparentLabel();
      this.tbButtons.SuspendLayout();
      this.SuspendLayout();
      // 
      // btnOK
      // 
      this.btnOK.ForeColor = System.Drawing.Color.Black;
      this.btnOK.Location = new System.Drawing.Point(300, 229);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 5;
      this.btnOK.Text = "تایید";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(219, 229);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 4;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // txtAddress
      // 
      this.txtAddress.Location = new System.Drawing.Point(99, 127);
      this.txtAddress.Multiline = true;
      this.txtAddress.Name = "txtAddress";
      this.txtAddress.Size = new System.Drawing.Size(241, 57);
      this.txtAddress.TabIndex = 3;
      // 
      // txtTelephone
      // 
      this.txtTelephone.Location = new System.Drawing.Point(99, 100);
      this.txtTelephone.Name = "txtTelephone";
      this.txtTelephone.Size = new System.Drawing.Size(73, 21);
      this.txtTelephone.TabIndex = 2;
      // 
      // txtShortName
      // 
      this.txtShortName.Location = new System.Drawing.Point(99, 73);
      this.txtShortName.Name = "txtShortName";
      this.txtShortName.Size = new System.Drawing.Size(73, 21);
      this.txtShortName.TabIndex = 1;
      // 
      // txtName
      // 
      this.txtName.Location = new System.Drawing.Point(99, 46);
      this.txtName.Name = "txtName";
      this.txtName.Size = new System.Drawing.Size(157, 21);
      this.txtName.TabIndex = 0;
      // 
      // tbButtons
      // 
      this.tbButtons.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbButtons.BorderWidth = 0.5F;
      this.tbButtons.Controls.Add(this.label4);
      this.tbButtons.Controls.Add(this.label3);
      this.tbButtons.Controls.Add(this.label2);
      this.tbButtons.Controls.Add(this.label1);
      this.tbButtons.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbButtons.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbButtons.Location = new System.Drawing.Point(13, 12);
      this.tbButtons.Name = "tbButtons";
      this.tbButtons.Size = new System.Drawing.Size(362, 204);
      this.tbButtons.TabIndex = 26;
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(309, 91);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(29, 15);
      this.label4.TabIndex = 15;
      this.label4.TabStop = false;
      this.label4.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label4.Texts")));
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(304, 118);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(34, 15);
      this.label3.TabIndex = 14;
      this.label3.TabStop = false;
      this.label3.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label3.Texts")));
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(291, 64);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(47, 15);
      this.label2.TabIndex = 13;
      this.label2.TabStop = false;
      this.label2.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label2.Texts")));
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(317, 37);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(21, 15);
      this.label1.TabIndex = 12;
      this.label1.TabStop = false;
      this.label1.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label1.Texts")));
      // 
      // FormSiteAddEdit
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(387, 264);
      this.Controls.Add(this.txtAddress);
      this.Controls.Add(this.txtTelephone);
      this.Controls.Add(this.txtShortName);
      this.Controls.Add(this.txtName);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOK);
      this.Controls.Add(this.tbButtons);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormSiteAddEdit";
      this.Text = "باشگاه جدید";
      this.Load += new System.EventHandler(this.FormSiteAdd_Load);
      this.tbButtons.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.TextBox txtAddress;
    private System.Windows.Forms.TextBox txtTelephone;
    private System.Windows.Forms.TextBox txtShortName;
    private System.Windows.Forms.TextBox txtName;
    private ToolBar tbButtons;
    private TransparentLabel label4;
    private TransparentLabel label3;
    private TransparentLabel label2;
    private TransparentLabel label1;
  }
}