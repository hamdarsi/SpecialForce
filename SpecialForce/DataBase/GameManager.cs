﻿using System;
using System.Data;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SpecialForce.DB
{
  public class GameManager
  {
    #region Internal data
    static private bool mInformRoundEnding = true;
    static Timer mDaemonTimer = new Timer();
    static Timer mRoundTimer = new Timer();
    static private Hashtable mGames = new Hashtable();
    static private DateTime mLastDuplicateSessions = DateTime.MinValue;
    #endregion Internal data


    #region Properties
    static Game mCurrentGame = null;
    static public Game CurrentGame
    {
      get { return mCurrentGame; }
    }
    #endregion Properties


    #region Events
    static public event EventHandler<GameDeleteArgs> OnGameDelete;
    static public event EventHandler<EventArgs> OnScheduledGameUpdated;
    #endregion Events


    #region Constructor
    static GameManager()
    {
      mDaemonTimer.Tick += ScheduleTimerTick;
      mRoundTimer.Tick += RoundTimeUpdated;
      mRoundTimer.Interval = 500;
    }
    #endregion Constructor


    #region Game Cache Management
    static public bool GameExistsInTimeLine(PersianDateTime start, PersianDateTime end, int site_id, int ex_id)
    {
      List<Game> games = GetGames(start, end, site_id);
      foreach (Game g in games)
        if(g.ID != ex_id)
          return true;

      return false;
    }

    static public bool DeleteGame(int id)
    {
      // check if the game exists
      Game g = GetGameByID(id);
      if (g == null)
        return false;

      // fire delete event
      EventHandler<GameDeleteArgs> ev = OnGameDelete;
      if(ev != null)
        ev(null, new GameDeleteArgs(g));

      // delete the game
      g._Delete();
      mGames.Remove(id);
      return true;
    }

    /// <summary>
    /// This method returns game information for the given game ID. If the game has not yet been loaded, it will try to load it.
    /// </summary>
    /// <param name="id">Game ID to load</param>
    /// <returns>Game information instance</returns>
    static public Game GetGameByID(int id)
    {
      if (id == -1)
        return null;

      if (mGames.ContainsKey(id))
        return mGames[id] as Game;

      Logger.Log("(DB.GameManager) Cache does not contain a game with ID " + id.ToString() + ", attempting to check out");
      DataTable query = DataBase.QueryResult("SELECT * FROM Games WHERE GameID=@1", id);
      if (query.Rows.Count != 1)
      {
        Logger.Log("(DB.GameManager) Given game does not exist or has a data problem: " + id.ToString());
        return null;
      }

      Game temp = Game.LoadFromData(query.Rows[0]);
      if (temp == null)
        throw new Exception("(DB.GameManager) Problem loading game information for id " + id.ToString());
      mGames[temp.ID] = temp;
      return temp;
    }

    /// <summary>
    /// This method returns all the games registered in the given site id.
    /// </summary>
    /// <param name="site_id">Site ID to get all its game. -1 means all sites.</param>
    /// <returns></returns>
    static public List<Game> GetAllGames(int site_id = -1)
    {
      return
        site_id == -1 ?
        GenerateGameList("") :
        GenerateGameList("WHERE SiteID=@1", site_id);
    }

    /// <summary>
    /// This method queries database against all games starting from the given datetime and updates internal cache.
    /// </summary>
    /// <param name="start">Time range start</param>
    /// <param name="site_id">Which site's games should be cached. -1 means all sites.</param>
    static public List<Game> GetGamesStartingAfter(PersianDateTime start, int site_id)
    {
      string q = "WHERE Start>=@1";
      return
        site_id == -1 ?
        GenerateGameList(q, start.DBFormat) :
        GenerateGameList(q + " AND SiteID=@2", start, site_id);
    }

    /// <summary>
    /// This method queries database against all games end prior to the given datetime and updates internal cache.
    /// </summary>
    /// <param name="end">Time range end</param>
    /// <param name="site_id">Which site's games should be cached. -1 means all sites.</param>
    static public List<Game> GetGamesEndingBefore(PersianDateTime end, int site_id)
    {
      string q = "WHERE Finish<=@1";
      return
        site_id == -1 ?
        GenerateGameList(q, end.DBFormat) :
        GenerateGameList(q + " AND SiteID=@2)", end, site_id);
    }

    /// <summary>
    /// This method queries database against all games within the given datetime period 
    /// and updates internal cache. The query returns all games which have time intersection 
    /// with the given range. The games will not necessirily be entirely in the given timeline.
    /// <remarks>This method gets site ID not its instance. This is done to minimize database imapct</remarks>
    /// </summary>
    /// <param name="start">Time range start</param>
    /// <param name="end">Time range end</param>
    /// <param name="site_id">Which site's games should be cached. -1 means all sites.</param>
    static public List<Game> GetGames(PersianDateTime start, PersianDateTime end, int site_id)
    {
      string q = "WHERE ((Start>=@1 AND Start<=@2) OR (Finish>=@3 AND Finish<=@4) OR (Start<@5 AND Finish>@6))";

      return 
        site_id == -1 ?
        GenerateGameList(q, start.DBFormat, end.DBFormat, start.DBFormat, end.DBFormat, start.DBFormat, end.DBFormat) :
        GenerateGameList(q + " AND (SiteID=@7)", start.DBFormat, end.DBFormat, start.DBFormat, end.DBFormat, start.DBFormat, end.DBFormat, site_id);
    }

    static public List<int> GetGameIDs(
      PersianDateTime start,
      PersianDateTime end,
      List<Site> sites,
      bool inc_dummy,
      bool inc_normal,
      bool inc_cup,
      bool inc_league,
      bool inc_elim)
    {
      string match_str = "";
      string site_str = "";
      List<int> result = new List<int>();

      string time = Profiler.TimeString(() =>
      {
        List<string> match_includes = new List<string>();
        if (inc_dummy)
          match_includes.Add(Enums.ToString(CompetitionType.Dummy));
        if (inc_normal)
          match_includes.Add(Enums.ToString(CompetitionType.FreeMatch));
        if (inc_cup)
          match_includes.Add(Enums.ToString(CompetitionType.Cup));
        if (inc_league)
          match_includes.Add(Enums.ToString(CompetitionType.League));
        if (inc_elim)
          match_includes.Add(Enums.ToString(CompetitionType.Elimination));

        match_str = string.Join(",", match_includes.Select(match => "'" + match + "'").ToArray());
        site_str = string.Join(",", sites.Select(site => site.ID).ToArray());

        string sql = "SELECT GameID FROM Games WHERE GameType IN (" + match_str + ") AND SiteID IN (" + site_str + ")";
        sql += " AND Start>=@1 AND Finish<=@2";

        foreach (DataRow row in DataBase.QueryResult(sql, start.DBFormat, end.DBFormat).Rows)
          result.Add(Convert.ToInt32(row[0]));
      });

      Logger.Log("(DB.GameManager) Generated " + result.Count.ToString() + " game ids in " + time);
      Logger.Log("(DB.GameManager)   Included sites     : " + site_str);
      Logger.Log("(DB.GameManager)   Included game types: " + match_str);
      Logger.Log("(DB.GameManager)   Start              : " + start.ToString(DateTimeString.CompactDateTime));
      Logger.Log("(DB.GameManager)   End                : " + end.ToString(DateTimeString.CompactDateTime));

      return result;
    }

    /// <summary>
    /// This method generates a list of all games on the given conditions.
    /// <remarks>Get site specifications in instance not in int. This is to
    /// ease coding on parts including this method.</remarks>
    /// </summary>
    /// <param name="start">Time period start</param>
    /// <param name="end">Time period end</param>
    /// <param name="sites">List of sites to include</param>
    /// <param name="inc_dummy">Whether include dummy games</param>
    /// <param name="inc_normal">Whether include regular matches</param>
    /// <param name="inc_cup">Whether include cup competitions</param>
    /// <param name="inc_league">Whether include league competitions</param>
    /// <param name="inc_elim">Whether include elimination competitions</param>
    /// <param name="inc_free">Whether include test matches</param>
    /// <returns>List of games generated on the given conditions</returns>
    static public List<Game> GetGames(
      PersianDateTime start,
      PersianDateTime end,
      List<Site> sites,
      bool inc_dummy,
      bool inc_normal,
      bool inc_cup,
      bool inc_league,
      bool inc_elim)
    {
      List<Game> result = new List<Game>();
      foreach (Site st in sites)
        result.AddRange(GetGames(start, end, st.ID));

      // now filter based on game type
      for (int i = 0; i < result.Count; ++i)
      {
        if (result[i].Type == CompetitionType.Dummy && !inc_dummy)
          result.RemoveAt(i--);
        else if (result[i].Type == CompetitionType.FreeMatch && !inc_normal)
          result.RemoveAt(i--);
        else if (result[i].Type == CompetitionType.Cup && !inc_cup)
          result.RemoveAt(i--);
        else if (result[i].Type == CompetitionType.League && !inc_league)
          result.RemoveAt(i--);
        else if (result[i].Type == CompetitionType.Elimination && !inc_elim)
          result.RemoveAt(i--);
      }
      return result;
    }

    /// <summary>
    /// This method is used to cache the given game data as game instances
    /// </summary>
    static private int CacheGamesInternal(DataTable data)
    {
      int count = 0;
      Game temp;
      foreach (DataRow row in data.Rows)
        if ((temp = Game.LoadFromData(row)) != null)
        {
          mGames[temp.ID] = temp;
          ++count;
        }
        else
          Logger.Log("(DB.GameManager) Problem loading game with ID: " + row["GameID"]);

      return count;
    }

    /// <summary>
    /// This method updates the cache internally with the given 
    /// </summary>
    /// <param name="where_clause"></param>
    /// <param name="values"></param>
    /// <returns></returns>
    static private List<Game> GenerateGameList(string where_clause, params object[] values)
    {
      int cached_count = 0;
      List<Game> result = new List<Game>();
      List<int> ids = new List<int>();
      List<int> ids_to_load = new List<int>();

      string time = Profiler.TimeString(() =>
      {
        // Step 1: Get all game IDs
        int id;
        foreach (DataRow row in DataBase.QueryResult("SELECT GameID From Games " + where_clause, values).Rows)
        {
          id = Convert.ToInt32(row[0]);
          ids.Add(id);
          if (!mGames.ContainsKey(id))
            ids_to_load.Add(id);
        }

        // Step 2: Cache all non cached instances
        if (ids_to_load.Count > 0)
        {
          // First generated select query
          string s = "SELECT * FROM Games WHERE GameID IN (" + string.Join(",", ids_to_load) + ")";
          cached_count = CacheGamesInternal(DataBase.QueryResult(s));
        }
      });

      if (ids_to_load.Count != 0)
        Logger.Log("(DB.GameManager) Generated report for " + ids.Count.ToString() + " games, cached " + cached_count.ToString() + " in " + time);

      // Step 3: Build output result
      for (int i = 0; i < ids.Count; ++i)
        result.Add(mGames[ids[i]] as Game);

      return result;
    }

    /// <summary>
    /// This method updates game cache for all games in datbase
    /// </summary>
    static public void UpdateCache()
    {
      int count = 0;
      string time = Profiler.TimeString(() =>
      {
        count = CacheGamesInternal(DataBase.QueryResult("SELECT * FROM Games"));
      });

      Logger.Log("(DB.GameManager) Cached " + count.ToString() + " games in " + time);
    }

    /// <summary>
    /// Internal method. Used to register a new game in SiteManager cache.
    /// </summary>
    /// <param name="game">Newly created game instance</param>
    static public void _InsertInCache(Game game)
    {
      mGames.Add(game.ID, game);
    }
    #endregion Game Cache Management


    #region Daemon management
    static public void StartDaemon(bool verbose)
    {
      if (mDaemonTimer.Enabled)
        return;

      if (Session.ReleaseBuild && !ControlTower.CommandQueue.IsOpen)
      {
        if(!Properties.Settings.Default.KioskMode)
          Session.ShowMessage("ارتباط بی سیم با لباس ها برقرار نیست. بازی ها برگزار نمی شوند.");

        Logger.Log("(DB.GameManager) Wireless module not started. Will not run schedule.");
        return;
      }

      UpdateScheduleCheckInterval();
      mDaemonTimer.Start();

      if (verbose && !Properties.Settings.Default.KioskMode)
        Session.ShowMessage("ارتباط بی سیم با لباس ها وصل شد. بازی ها برگزار می شوند.");
    }

    static public void EndDaemon(bool force)
    {
      if (mDaemonTimer.Enabled)
      {
        mDaemonTimer.Stop();

        if (force)
        {
          if(!Properties.Settings.Default.KioskMode)
            Session.ShowMessage("ارتباط بی سیم با لباس ها قطع شد. بازی ها برگزار نمی شوند.");

          Logger.Log("(DB.GameManager) Wireless module not started. Will not run schedule.");
        }
      }
    }

    static public void UpdateScheduleCheckInterval()
    {
      mDaemonTimer.Interval = Properties.Settings.Default.ScheduleCheckInterval * 1000;
      if (mDaemonTimer.Enabled)
      {
        mDaemonTimer.Stop();
        mDaemonTimer.Start();
      }
    }
    #endregion Daemon management


    #region Round Management
    static public void StartRoundTimer()
    {
      mRoundTimer.Enabled = true;
    }

    /// <summary>
    /// Stops the timer which is used to check round state.
    /// This method is the endpoint of a round, it also
    /// flushes the logger cache.
    /// </summary>
    static public void StopRoundTimer()
    {
      mRoundTimer.Enabled = false;
      Logger.Flush();
    }

    /// <summary>
    /// Checks for timeouts on round and bomb. In case any tout occured,
    /// round timer will be disabled and corresponding action will be taken.
    /// </summary>
    static private void RoundTimeUpdated(Object sender, EventArgs e)
    {
      try
      {
        DB.Round round = mCurrentGame.CurrentRound;
        if (round.BombState == BombState.Planted && round.RemainingBombTime <= 0)
        {
          StopRoundTimer();
          ControlTower.Tower.ActiveBomb.ManualStateUpdate(BombState.Exploded, true);
        }
        else if (round.State == RoundState.InProgress && round.RemainingTime <= 0)
        {
          StopRoundTimer();
          CheckCurrentRound();
        }
        else if (round.RemainingTime < 10 && mInformRoundEnding)
        {
          mInformRoundEnding = false;
          Session.PlaySound(Properties.Resources.Round_Notification);
        }
        else if (round.RemainingBombTime < 10 && mInformRoundEnding)
        {
          mInformRoundEnding = false;
          Session.PlaySound(Properties.Resources.Round_Notification);
        }
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "DB.GameManager", "RoundTimeUpdated");
        StopRoundTimer();
      }
    }

    /// <summary>
    /// FormGameStart has initialized starting parameters and
    /// everything is ready to go. Start the round starting all
    /// needed management instances.
    /// Also initialize round notification information.
    /// </summary>
    static public void CurrentRoundReadyStartIt()
    {
      try
      {
        for (int i = 0; i < 20; ++i)
        {
          ControlTower.Kevlar kev = ControlTower.Tower.GetDeviceInstance(Kevlars.KevlarIDFromNumber(i + 1)) as ControlTower.Kevlar;
          if (kev != null)
          {
            String s = "Kevar #" + i.ToString() + ": ";
            if (kev.Player == null)
              s += " none";
            else
              s += "#" + kev.Player.ID.ToString() + " (" + kev.Player.ToString() + ")";
            Logger.Log(s);
          }
        }

        mInformRoundEnding = true;
        ControlTower.Tower.ConnectPipeline();
        ControlTower.ControlPanel.MonitorGame();
        ControlTower.FormDeviceInteractions.Instance.ApplyInteractions();
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "DB.GameManager", "CurrentRoundReadyStartIt");
      }
    }
    
    /// <summary>
    /// This method is called to abort the current running round.
    /// It's called by ControlPanel's mnuAbortRound.
    /// </summary>
    /// <param name="reason">Reason of round abortion</param>
    static public void AbortCurrentRound(string reason)
    {
      Session.Synchronize(new MethodInvoker(delegate { AbortCurrentRoundInternal(reason); }));
    }

    /// <summary>
    /// Internal implementation to abort current game.
    /// </summary>
    /// <param name="reason">Reason of round abortion</param>
    static private void AbortCurrentRoundInternal(string reason)
    {
      try
      {
        Logger.Log("(DB.GameManager) Current round aborted. Reason: " + reason);

        // This method is called only when a round was present
        if (mCurrentGame.CurrentRound.State == RoundState.InProgress ||
            mCurrentGame.CurrentRound.State == RoundState.Pending)
          mCurrentGame.CurrentRound.RoundAborted();

        StopSessionControl();
        Session.ShowMessage(reason);
        // Now that the round's state has been clarified, start the
        // new round if necessary, with the previous round's players.
        // In case the session's time has finished, abort the game.
        if (mCurrentGame.CheckGameMustContinue())
          CurrentGameUpdated();
        else
          AbortCurrentGame("اجرای بازی هم قطع شد.");
      }
      catch (Exception e)
      {
        Logger.LogException(e, "DB.GameManager", "Exception when aborting round");
      }
    }

    public static void CheckCurrentRound()
    {
      if (mCurrentGame == null)
        return;

      Round round = mCurrentGame.CurrentRound;
      if (round == null)
        return;

      // if the round has a winner, register the winner
      WinnerType winner = DB.Round.CheckRoundWinner(round);
      if (winner == WinnerType.NoWinner)
        return;

      // Save all action
      round.RoundFinished(winner);

      // Turn off all kevlars and bombs and timers
      StopSessionControl();

      // now that the round's state has been
      // clarified, start the new round if necessary
      // with the previous round's players
      if (mCurrentGame.CheckGameMustContinue())
        CurrentGameUpdated();
      else
        CurrentGameFinishedPostUpdates();
    }
    #endregion Round Management


    #region Game Management
    /// <summary>
    /// Is used to stop the current running session control.
    /// Its called by:
    ///   - AbortCurrentRound()
    ///   - AbortCurrentGame()
    ///   - CheckCurrentRound() (when round's finished)
    /// </summary>
    static private void StopSessionControl()
    {
      // Stop the round's timer
      mRoundTimer.Stop();

      // Turn off all kevlars and bombs and timers
      ControlTower.Tower.TurnOffAll();

      // Close control panel in control tower
      ControlTower.ControlPanel.StopMonitoring();

      // Stop all running interactions
      ControlTower.FormDeviceInteractions.Instance.StopInteractions();
    }

    /// <summary>
    /// Current game is finished. Stop the game and send match stats
    /// to all Kevlars. In case the game is of a competition, it also
    /// updates the competition. This is called only by CheckCurrentRound() 
    /// </summary>
    static private void CurrentGameFinishedPostUpdates()
    {
      mCurrentGame.GameFinished();
      if (mCurrentGame.CompetitionID != -1)
        mCurrentGame.Competition.ResultProvided(mCurrentGame);

      // Broadcast winner information
      int blue, green;
      mCurrentGame.GetTeamScores(out blue, out green);
      string line1 = "SPForce.ir   B" + blue.ToString("D2");
      string line2 = "";
      if (mCurrentGame.Winner == WinnerType.Blue)
        line2 = "  Blue won!  G" + green.ToString("D2");
      else if (mCurrentGame.Winner == WinnerType.Green)
        line2 = "  Green won! G" + green.ToString("D2");
      else if (mCurrentGame.Winner == WinnerType.Tie)
        line2 = "  Tied!      G" + green.ToString("D2");

      ControlTower.Tower.Broadcast(line1, line2);
    }

    /// <summary>
    /// This method is used to abort the current game and remove
    /// all controls. It is used by FormGameStart's btnAbort and
    /// ControlPanel's mnuAbortGame.
    /// </summary>
    static public void AbortCurrentGame(string reason)
    {
      Session.Synchronize(new MethodInvoker(delegate { AbortCurrentGameInternal(reason); }));
    }

    /// <summary>
    /// Internal implementation to abort the current game.
    /// </summary>
    /// <param name="reason">Reason of game abortion</param>
    static private void AbortCurrentGameInternal(string reason)
    {
      try
      {
        Logger.Log("(DB.GameManager) Game aborted. Reason: " + reason);
        DB.GameManager.CurrentGame.GameAborted();
        StopSessionControl();
        Session.ShowMessage(reason);
      }
      catch (Exception e)
      {
        Logger.LogException(e, "DB.GameManager", "Exception when aborting game");
      }
    }
    #endregion Game Management


    #region Schedule Management
    static private void CurrentGameUpdated()
    {
      // Log the game change event
      if (mCurrentGame != null)
        Logger.Log("(DB.GameManager) Broadcasted start event for Game #" + mCurrentGame.ID.ToString());
      else
        Logger.Log("(DB.GameManager) Broadcasted stop event");

      // Broadcast the schedule update event
      EventHandler<EventArgs> ev = OnScheduledGameUpdated;
      if (ev != null)
        ev(mCurrentGame, new EventArgs());

      if (mCurrentGame == null)
      {
        // Stop monitoring the game
        ControlTower.ControlPanel.StopMonitoring();

        // Detach all players from the kevlars
        ControlTower.Tower.DetachPlayers();
      }
      else
      {
        // Now start game control if a new game should be started
        // Prepare the game if its just started
        if (mCurrentGame.State == GameState.Reserved)
        {
          // Detach all players from kevlars. In case previous attempt was unsuccessful
          ControlTower.Tower.DetachPlayers();

          // Prepare the game to start
          mCurrentGame.PrepareGame();

          // Restore all previous round information in case game was cancelled
          if(mCurrentGame.AllRounds.Count != 0)
            mCurrentGame.RestoreKevlarInformation();
        }

        // Create a new round for the game
        mCurrentGame.CreateNewRound();
        Logger.Log("(DB.GameManager) Started new round: [ID #" +
          mCurrentGame.CurrentRound.ID.ToString() + ", Round #" + mCurrentGame.CurrentRound.Number.ToString() + "]");

        // Start round setup. I love this part:
        // Start FormGameStart after this has finished!
        Session.Synchronize(new Action(ControlTower.FormGameStart.StartSetup));
      }
    }

    static private DB.Game GetGameScheduledForNow()
    {
      DateTime now = DateTime.Now;
      List<Game> games = GenerateGameList("WHERE Start<=@1 AND @2<=Finish AND SiteID=@3 AND State=@4",
                                             now, now, Session.ActiveSiteID, Enums.ToString(GameState.Reserved));

      if (games.Count > 1)
      {
        // log only once in 5 minutes
        if ((now - mLastDuplicateSessions).TotalMinutes >= 5)
        {
          mLastDuplicateSessions = now;
          Logger.Log("(DB.GameManager) More than 1 instance of games were found for: " + PersianDateTime.Now.ToString(DateTimeString.CompactDateTime));
          foreach (Game game in games)
            Logger.Log("(DB.GameManager)   #" + game.ID.ToString());
          Logger.Log("(DB.GameManager) Truncating the list.");
        }

        // truncate the list, maintaining only the smallest game id
        games.RemoveRange(0, games.Count - 2);
      }

      // Return the instance of game
      return games.Count == 0 ? null : games[0];
    }

    static private void ScheduleTimerTick(Object sender, EventArgs e)
    {
      // Enclose all in a big try catch!
      try
      {
        // First of all, evaluate the state of current game being played
        // If its finished in any how, then close it and inform everything
        // that there is currently no game being played
        if (mCurrentGame != null)
          if (mCurrentGame.State == GameState.Finished || mCurrentGame.State == GameState.Aborted)
          {
            Logger.Log("(DB.GameManager) Current game closed. ID: #" + mCurrentGame.ID.ToString());
            mCurrentGame = null;
            CurrentGameUpdated();
          }

        // Check if a game should be in progress
        DB.Game new_game = GetGameScheduledForNow();
        if (new_game == null)
          return;

        // Check if the new_game is another one
        if (mCurrentGame != null && new_game.ID == mCurrentGame.ID)
          return;

        // There is still another game being played?
        // then what the fuck is happening?
        if (mCurrentGame != null)
        {
          Logger.Log("(DB.GameManager) Another game is being played and its not closed!");
          Logger.Log("(DB.GameManager)   - Current game: " + mCurrentGame.ID.ToString());
          Logger.Log("(DB.GameManager)   - New game:     " + new_game.ID.ToString());
          throw new Exception("(DB.GameManager) Another game is being played and its not closed.");
        }
        else if (new_game.State == GameState.Reserved)
        {
          // Now that the new game is ready to run, run it.
          mCurrentGame = new_game;
          Logger.Log("(DB.GameManager) New game started. ID: #" + mCurrentGame.ID.ToString());
          CurrentGameUpdated();
        }
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "DB.GameManager", "Error updating schedule");
      }
    }
    #endregion Schedule Management
  }
}
