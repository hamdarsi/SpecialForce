﻿using System;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class Message
  {
    #region Properties
    private int mMessageID = -1;
    public int ID
    {
      get { return mMessageID; }
    }

    private string mTitle = "";
    public string Title
    {
      get { return mTitle; }
      set { mTitle = value; }
    }

    private string mLine1 = "";
    public string Line1
    {
      get { return mLine1; }
      set { mLine1 = Command.CorrectMessage(value); }
    }

    private string mLine2 = "";
    public string Line2
    {
      get { return mLine2; }
      set { mLine2 = Command.CorrectMessage(value); }
    }

    private int mOperatorID = -1;
    public int OperatorID
    {
      get { return mOperatorID; }
    }

    private PersianDateTime mCreatedOn = null;
    public PersianDateTime CreatedOn
    {
      get { return mCreatedOn == null ? null : new PersianDateTime(mCreatedOn); }
    }

    private PersianDateTime mDeletedOn = null;
    public PersianDateTime DeletedOn
    {
      get { return mDeletedOn == null ? null : new PersianDateTime(mDeletedOn); }
    }

    public bool Deleted
    {
      get { return mDeletedOn != null; }
    }
    #endregion Properties


    #region Constructor
    public Message()
    {
    }
    #endregion Constructor


    #region Utilities
    public override string ToString()
    {
      return mTitle;
    }

    static private Message LoadFromData(DataRow data)
    {
      Message result = new Message();
      result.mMessageID = Convert.ToInt32(data["MessageID"]);
      result.mTitle = data.Field<string>("Title");
      result.mLine1 = data.Field<string>("Line1");
      result.mLine2 = data.Field<string>("Line2");
      result.mOperatorID = data.Field<int>("OperatorID");
      result.mCreatedOn = PersianDateTime.FromObject(data["CreatedOn"]);
      result.mDeletedOn = PersianDateTime.FromObject(data["DeletedOn"]);
      return result;
    }

    static public List<Message> GetAllMessages(bool deleted_also = true)
    {
      List<Message> result = new List<Message>();
      string sql = "SELECT * FROM Messages";
      if (!deleted_also)
        sql += " WHERE DeletedOn IS NULL";

      foreach (DataRow row in DataBase.QueryResult(sql).Rows)
        result.Add(LoadFromData(row));

      return result;
    }
    #endregion Utilities


    #region DataBase Interactions
    public void Delete()
    {
      mDeletedOn = PersianDateTime.Now;
      string sql = "UPDATE Messages SET DeletedOn=@1 WHERE MessageID=@2";
      DataBase.ExecuteSQL(sql, mDeletedOn.DBFormat, mMessageID);
    }

    public void Apply()
    {
      if (mMessageID == -1)
      {
        string sql = "INSERT INTO Messages (Title, Line1, Line2, OperatorID, CreatedOn) VALUES (@1, @2, @3, @4, @5)";
        DataBase.ExecuteSQL(sql, mTitle, mLine1, mLine2, Session.CurrentStaff.ID, PersianDateTime.Now.DBFormat);
        mMessageID = DataBase.InsertID;
      }
      else
      {
        string sql = "UPDATE Messages SET Title=@1, Line1=@2, Line2=@3, OperatorID=@4, CreatedOn=@5 WHERE MessageID=@6";
        DataBase.ExecuteSQL(sql, mTitle, mLine1, mLine2, Session.CurrentStaff.ID, PersianDateTime.Now.DBFormat, mMessageID);
      }
    }
    #endregion DataBase Interactions
  }
}
