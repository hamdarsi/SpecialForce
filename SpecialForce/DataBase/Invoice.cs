﻿using System;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class Invoice
  {
    #region Properties
    private int mInvoiceID = -1;
    public int ID
    {
      get { return mInvoiceID; }
    }

    private int mSiteID = -1;
    public int SiteID
    {
      get { return mSiteID; }
      set { mSiteID = value; }
    }

    private InvoiceType mType = InvoiceType.UntypedPayment;
    public InvoiceType Type
    {
      get { return mType; }
      set { mType = value; }
    }

    public bool IsBill
    {
      get
      {
        return
          mType == InvoiceType.BillGas ||
          mType == InvoiceType.BillPhone ||
          mType == InvoiceType.BillPower ||
          mType == InvoiceType.BillSewer ||
          mType == InvoiceType.BillTax ||
          mType == InvoiceType.BillWater;
      }
    }

    private double mAmount = 0.0;
    public double Amount
    {
      get { return mAmount; }
      set { mAmount = value; }
    }

    private string mComment = "";
    public string Comment
    {
      get { return mComment; }
      set { mComment = value; }
    }

    private PersianDateTime mTimeStamp = null;
    public PersianDateTime TimeStamp
    {
      get { return mTimeStamp == null ? mTimeStamp : new PersianDateTime(mTimeStamp); }
      set { mTimeStamp = value; }
    }

    private int mOperatorID = -1;
    public int OperatorID
    {
      get { return mOperatorID; }
      set { mOperatorID = value; }
    }

    public string OperatorName
    {
      get { return User.GetUserFullName(mOperatorID);}
    }

    private int mCompetitionID = -1;
    public int CompetitionID
    {
      get { return mCompetitionID; }
      set { mCompetitionID = value; }
    }

    private int mTeamID = -1;
    public int TeamID
    {
      get { return mTeamID; }
      set { mTeamID = value; }
    }

    private int mUserID = -1;
    public int UserID
    {
      get { return mUserID; }
      set { mUserID = value; }
    }

    private int mGameID = -1;
    public int GameID
    {
      get { return mGameID; }
      set { mGameID = value; }
    }

    private string mTeamName;
    public string TeamName
    {
      get { return mTeamName; }
    }

    private string mCompetitionTitle;
    public string CompetitionTitle
    {
      get { return mCompetitionTitle == null ? "" : mCompetitionTitle; }
    }

    private string mSiteName;
    public string SiteName
    {
      get { return mSiteName == null ? "" : mSiteName; }
    }

    private string mUserFullName;
    public string UserFullName
    {
      get { return mUserFullName == null ? "" : mUserFullName; }
    }

    private string mOperatorFullName;
    public string OperatorFullName
    {
      get { return mOperatorFullName == null ? "" : mOperatorFullName; }
    }
    #endregion Properties


    #region Constructor
    public Invoice()
    {
    }
    #endregion Constructor


    #region Utilities
    static public double GetCompetitionExpense(Competition c)
    {
      try
      {
        string sql = "SELECT Amount FROM Accounting WHERE CompetitionID=@1 AND Type=@2";
        return DataBase.QueryValue(sql, 0.0, c.ID, Enums.ToString(InvoiceType.CompetitionExpense));
      }
      catch(Exception ex)
      {
        Logger.LogException(ex, "DB.Invoice", "GetCompetitionExpense");
        return 0;
      }
    }

    public override string ToString()
    {
      string strOp = mOperatorID != -1 ? " توسط " + mOperatorFullName : "";
      if (mType == InvoiceType.UserCreditCharge)
        return "شارژ: " + UserFullName + strOp;
      else if (mType == InvoiceType.UserCreditRefund)
        return "عودت: " + UserFullName + strOp;
      else if (mType == InvoiceType.UserGiftCharge)
        return "شارژ هدیه برای " + UserFullName;
      else if (mType == InvoiceType.UntypedPayment)
        return mComment;
      else if (mType == InvoiceType.CompetitionAdmission)
        return "ورودیه مسابقه " + CompetitionTitle + " برای تیم " + TeamName;
      else if (mType == InvoiceType.CompetitionExpense)
        return "هزینه مسابقه " + CompetitionTitle + ": " + mComment;
      else
        return 
          mComment.Length > 0 ?
          Enums.ToString(mType) + ": " + mComment :
          Enums.ToString(mType);
    }
    #endregion Utilities


    #region DataBase Interactions
    static public Invoice LoadFromData(DataRow data)
    {
      try
      {
        Invoice result = new Invoice();
        result.mSiteID = data.Field<int>("SiteID");
        result.mAmount = data.Field<double>("Amount");
        result.mComment = data.Field<string>("Comment");
        result.mType = Enums.ParseInvoiceType(data.Field<string>("Type"));
        result.mTimeStamp = PersianDateTime.FromObject(data["TimeStamp"]);
        result.mOperatorID = data.Field<int>("OperatorID");
        result.mCompetitionID = data.Field<int>("CompetitionID");
        result.mTeamID = data.Field<int>("TeamID");
        result.mUserID = data.Field<int>("UserID");
        result.mGameID = data.Field<int>("GameID");

        result.mTeamName = data.Table.Columns.Contains("TeamName") ? data.Field<string>("TeamName") : null;
        result.mCompetitionTitle = data.Table.Columns.Contains("CompetitionTitle") ? data.Field<string>("CompetitionTitle") : null;
        result.mSiteName = data.Table.Columns.Contains("SiteName") ? data.Field<string>("SiteName") : null;
        result.mUserFullName = data.Table.Columns.Contains("UserFullName") ? data.Field<string>("UserFullName") : null;
        result.mOperatorFullName = data.Table.Columns.Contains("OperatorFullName") ? data.Field<string>("OperatorFullName") : null;

        return result;
      }
      catch(Exception ex)
      {
        Logger.LogException(ex, "DB.Invoice", "GetInvoice()");
        return null;
      }
    }

    public void Apply()
    {
      if (mInvoiceID != -1)
        throw new Exception("(DB.Invoice) Invoice already applied");

      mTimeStamp = PersianDateTime.Now;
      // no updates for any invoice
      // just insertions
      string sql = "INSERT INTO Accounting (Amount, TimeStamp, Comment, SiteID, Type, OperatorID, CompetitionID, TeamID, UserID, GameID) ";
      sql += "VALUES (@1, @2, @3, @4, @5, @6, @7, @8, @9, @10)";
      if (DataBase.ExecuteSQL(sql, mAmount, mTimeStamp.Value, mComment, mSiteID, Enums.ToString(mType), mOperatorID, mCompetitionID, mTeamID, mUserID, mGameID) != 1)
        throw new Exception("(DB.Invoice) probable error when inserting into accounting");
    }
    #endregion DataBase Interactions
  }
}
