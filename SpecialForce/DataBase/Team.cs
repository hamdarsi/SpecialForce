﻿using System;
using System.Data;
using System.Drawing;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class Team
  {
    #region Properties
    private int mTeamID = -1;
    public int ID
    {
      get { return mTeamID; }
    }

    private string mName = "";
    public string Name
    {
      get { return mName; }
      set { mName = value; }
    }

    private byte[] mLogoStream = null;
    private Image mLogo = null;
    public Image Logo
    {
      get
      {
        if (mLogoStream == null)
          return null;

        if (mLogo == null)
          mLogo = DataBase.ImageFromBlob(mLogoStream);

        return mLogo.Clone() as Image;
      }
      set
      {
        mLogo = value;
        mLogoStream = DataBase.ImageToBlob(mLogo);
      }
    }

    private PersianDateTime mRegistrationDate = null;
    public PersianDateTime RegistrationDate
    {
      get { return mRegistrationDate == null ? null : new PersianDateTime(mRegistrationDate); }
      set { mRegistrationDate = value; }
    }

    private int mRegistrationSiteID = -1;
    private DB.Site mRegistrationSite;
    public DB.Site RegistrationSite
    {
      get 
      {
        if(mRegistrationSite == null)
          mRegistrationSite = DB.Site.GetSiteByID(mRegistrationSiteID);

        return mRegistrationSite;
      }
    }

    /// <summary>
    /// Returns list of all competitions the team has participated
    /// </summary>
    public List<DB.Competition> ParticipatingCompetitions
    {
      get
      {
        List<DB.Competition> result = new List<Competition>();
        string sql = "SELECT CompetitionID FROM CompetitionTeams WHERE TeamID=@1";
        foreach (DataRow row in DataBase.QueryResult(sql, mTeamID).Rows)
          result.Add(DB.CompetitionManager.GetCompetition(row.Field<int>(0)));
        return result;
      }
    }

    /// <summary>
    /// Controlled functionality to create a new instance of a team.
    /// </summary>
    #endregion Properties


    #region Constructor
    private Team()
    {
    }
    #endregion Constructor


    #region Membership interface
    /// <summary>
    /// Returns number of active members in the team
    /// </summary>
    /// <param name="team_id">Team ID to get its member count</param>
    /// <returns>Active number of members in the team</returns>
    public int GetMemberCount()
    {
      return GetMemberCount(mTeamID);
    }

    /// <summary>
    /// Returns number of active members in the team
    /// </summary>
    /// <param name="team_id">Team ID to get its member count</param>
    /// <returns>Active number of members in the team</returns>
    static public int GetMemberCount(int team_id)
    {
      if (team_id == -1)
        return 0;

      string sql = "SELECT COUNT(*) FROM TeamMembership WHERE TeamID=@1 AND MembershipEnd IS NULL";
      return DataBase.QueryValue(sql, 0, team_id);
    }

    /// <summary>
    /// This method registers the given user to the given team
    /// </summary>
    /// <param name="user">User ID to register</param>
    /// <param name="team">Destination Team ID</param>
    static private void AddMember(int user, int team)
    {
      string sql = "INSERT INTO TeamMembership (TeamID, UserID, MembershipStart) VALUES (@1, @2, @3)";
      DataBase.ExecuteSQL(sql, team, user, DateTime.Now);
      User.SetCurrentTeam(user, team);
    }

    /// <summary>
    /// This method removes the given member from any team he is on.
    /// </summary>
    /// <param name="id">User ID to remove from team</param>
    static private void RemoveMember(int id, int exclude_team = -1)
    {
      int result;
      string sql = "UPDATE TeamMembership SET MembershipEnd=@1 WHERE UserID=@2 AND MembershipEnd IS NULL";
      if (exclude_team == -1)
        result = DataBase.ExecuteSQL(sql, DateTime.Now, id);
      else
        result = DataBase.ExecuteSQL(sql + " AND TeamID<>@3", DateTime.Now, id, exclude_team);

      if (result != 1)
        Logger.Log("(DB.Team) Cannot remove membership for user #" + id.ToString());

      // Also update users table
      User.SetCurrentTeam(id, -1);
    }

    /// <summary>
    /// This method assigns the given user ids to the team.
    /// </summary>
    /// <param name="new_members">List of all the teams members, not only new ones.</param>
    public void AssignMembers(List<int> new_members)
    {
      // First remove members which are not going to remain in the team
      DateTime now = DateTime.Now;
      List<int> cur_members = ListMemberIDs();
      foreach (int member in cur_members)
        if (!new_members.Contains(member))
          RemoveMember(member);

      // Now remove new members if they are on another team
      foreach (int member in new_members)
        RemoveMember(member, mTeamID);

      // Add new members
      foreach (int member in new_members)
        if (!cur_members.Contains(member))
          AddMember(member, mTeamID);
    }

    /// <summary>
    /// Lists all registered user IDs for the team.
    /// </summary>
    public List<int> ListMemberIDs()
    {
      return ListMemberIDs(mTeamID);
    }

    /// <summary>
    /// Lists all registered user IDs for the given team.
    /// </summary>
    /// <param name="team_id">Team ID to list its members</param>
    static public List<int> ListMemberIDs(int team_id)
    {
      List<int> result = new List<int>();
      if (team_id == -1)
        return result;

      string sql = "SELECT UserID FROM TeamMembership WHERE TeamID=@1 AND MembershipEnd IS NULL";
      foreach (DataRow row in DataBase.QueryResult(sql, team_id).Rows)
        result.Add(row.Field<int>(0));
      return result;
    }

    /// <summary>
    /// Queries and lists all members.
    /// </summary>
    /// <returns>All active team members</returns>
    public List<User> ListMembers()
    {
      return ListMembers(mTeamID);
    }

    /// <summary>
    /// Queries and lists all members for the given team.
    /// </summary>
    /// <param name="team_id">The team ID to check for its members</param>
    /// <returns>All active team members</returns>
    static public List<User> ListMembers(int team_id)
    {
      if (team_id == -1)
        return new List<User>();

      string sql = @"SELECT Users.* FROM TeamMembership INNER JOIN Users
                       ON TeamMembership.UserID=Users.UserID
                       WHERE TeamMembership.MembershipEnd IS NULL AND TeamMembership.TeamID=@1";
      return User.LoadFromQuery(DataBase.QueryResult(sql, team_id));
    }
    #endregion Membership interface


    #region Team enacapsulation
    /// <summary>
    /// Checks whether the team has enough members to be acknowledged.
    /// </summary>
    /// <returns>True if team is able to be in any competitions, false otherwise</returns>
    public bool IsActive()
    {
      string sql = "SELECT COUNT(TeamMembershipID) FROM TeamMembership WHERE TeamID=@1 AND MembershipEnd IS NULL";
      return DataBase.QueryValue(sql, 0, mTeamID) >= DB.Options.TeamMinimumMembers;
    }

    /// <summary>
    /// This is used to create a new team.
    /// </summary>
    /// <param name="name">Name of the team</param>
    /// <param name="logo">Team logo</param>
    /// <returns>The newly created team</returns>
    static public Team CreateNewTeam(string name, Image logo)
    {
      Team result = new Team();
      result.mName = name;
      result.mLogoStream = DataBase.ImageToBlob(logo);
      result.Logo = logo;
      result.Apply();
      return result;
    }

    /// <summary>
    /// Loads all teams in the given query
    /// </summary>
    static public List<Team> LoadFromQuery(DataTable query)
    {
      Team temp;
      List<Team> result = new List<Team>();

      foreach (DataRow row in query.Rows)
        if ((temp = LoadFromData(row)) != null)
          result.Add(temp);

      return result;
    }

    /// <summary>
    /// Loads the requested team from the given ID
    /// </summary>
    /// <param name="id">TeamID</param>
    /// <returns>Team instance</returns>
    static public Team GetTeamByID(int id)
    {
      List<DB.Team> result = LoadFromQuery(DataBase.QueryResult("SELECT * FROM Teams WHERE TeamID=@1", id));
      if (result.Count != 1)
        Logger.Log("(DB.TeamManager) Given team does not exist or has a data problem: " + id.ToString());

      return result.Count == 1 ? result[0] : null;
    }

    /// <summary>
    /// This method lists registered teams.
    /// </summary>
    /// <param name="site_id">If specified, result will be filtered to 
    /// teams registering in the given site.</param>
    /// <param name="namepart">Part of the teams name</param>
    /// <returns>List of registered teams</returns>
    static public List<Team> GetTeamsBySite(int site_id, string namepart)
    {
      List<Team> result = null;

      string time = Profiler.TimeString(() =>
      {
        string sql = "SELECT * FROM TEAMS";
        string namesql = " WHERE Name LIKE '%" + namepart + "%'";

        if (site_id == -1)
        {
          if (namepart.Length != 0)
            sql += " WHERE Name LIKE '%" + namepart + "%'";

          result = LoadFromQuery(DataBase.QueryResult(sql));
        }
        else
        {
          sql += " WHERE RegistrationSiteID=@1";
          if (namepart.Length != 0)
            sql += " AND Name LIKE '%" + namepart + "%'";

          result = LoadFromQuery(DataBase.QueryResult(sql, site_id));
        }
      });

      Logger.Log("(DB.Team) Loaded " + result.Count.ToString() + " teams in " + time);
      return result;
    }

    /// <summary>
    /// Querties DB against all the given team ids and loads their data
    /// </summary>
    /// <param name="ids">Team IDs to search for in the DB</param>
    /// <returns>List of all loaded teams</returns>
    static public List<Team> GetTeamsByID(List<int> ids)
    {
      if (ids.Count == 0)
        return new List<Team>();

      string sql = "SELECT * FROM Teams WHERE TeamID IN (" + string.Join(",", ids) + ")";
      return LoadFromQuery(DataBase.QueryResult(sql));
    }

    /// <summary>
    /// Returns name of the requested team by its ID
    /// </summary>
    /// <param name="team_id">Team ID to get its name</param>
    /// <param name="name_if_not_found">Default value for team name</param>
    /// <returns>Name of the requested team</returns>
    static public string GetTeamName(int team_id, string name_if_not_found = "تیم وجود ندارد")
    {
      return DataBase.QueryValue("SELECT Name FROM Teams WHERE TeamID=@1", name_if_not_found, team_id);
    }

    /// <summary>
    /// Checks whether any team exists with the given team name, not considering the default team ID
    /// </summary>
    /// <param name="strName">Name of the team</param>
    /// <param name="own_id">Default ID. If a team with requested name is 
    /// matched against this ID, it will not be considered</param>
    static public bool TeamExistsByName(string strName, int own_id)
    {
      bool exists = false;
      string sql = "SELECT TeamID FROM Teams WHERE Name=@1";
      foreach (DataRow data in DataBase.QueryResult(sql, strName).Rows)
        if (Convert.ToInt32(data[0]) == own_id)
          continue;
        else
        {
          exists = true;
          break;
        }

      return exists;
    }

    public override string ToString()
    {
      return mName;
    }
    #endregion Team encapsulation


    #region DataBase Interactions
    /// <summary>
    /// Loads a team from the given data
    /// </summary>
    /// <param name="data">Data containing team information</param>
    /// <returns>Team instance loaded from the given data</returns>
    static public Team LoadFromData(DataRow data)
    {
      try
      {
        Team result = new Team();
        result.mTeamID = Convert.ToInt32(data["TeamID"]);
        result.mName = data.Field<string>("Name");
        result.mLogoStream = data.Field<byte[]>("Logo");
        result.mRegistrationDate = PersianDateTime.FromObject(data["RegistrationDate"]);
        result.mRegistrationSiteID = data.Field<int>("RegistrationSiteID");
        return result;
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "DB.Team", "LoadFromData");
        return null;
      }
    }

    public void Apply()
    {
      string sql;

      if (mTeamID == -1) // shall insert
      {
        mRegistrationDate = PersianDateTime.Now;
        mRegistrationSiteID = Session.ActiveSiteID;

        sql = "INSERT INTO Teams (Name, Logo, RegistrationDate, RegistrationSiteID) VALUES (@1, @2, @3, @4)";
        if (DataBase.ExecuteSQL(sql, mName, mLogoStream, mRegistrationDate.DBFormat, mRegistrationSiteID) != 1)
          throw new Exception("(DB.Team) probable fault when inserting new row");

        mTeamID = DataBase.InsertID;
      }
      else
      {
        sql = "UPDATE Teams SET Name=@1, Logo=@4 WHERE TeamID=@5";

        if (DataBase.ExecuteSQL(sql, mName, mLogoStream, mTeamID) != 1)
          throw new Exception("(DB.Team) probable fault when updating a row");
      }
    }
    #endregion DataBase Interactions
  }
}
