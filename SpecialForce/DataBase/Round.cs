﻿using System;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public partial class Round
  {
    #region Properties
    #region Round Information
    private int mRoundID = -1;
    public int ID
    {
      get { return mRoundID; }
    }

    private int mGameID = -1;
    public int GameID
    {
      get { return mGameID; }
    }

    private DB.Game mGameInstance;
    public Game Game
    {
      get 
      {
        if (mGameID == -1)
          return null;
        else if (mGameInstance == null || mGameInstance.ID != mGameID)
          mGameInstance = DB.GameManager.GetGameByID(mGameID);

        return mGameInstance; 
      }
    }

    private PersianDateTime mStart = null;
    public PersianDateTime Start
    {
      get { return mStart == null ? null : new PersianDateTime(mStart); }
    }

    private PersianDateTime mFinish = null;
    public PersianDateTime Finish
    {
      get { return mFinish == null ? null : new PersianDateTime(mFinish); }
    }

    private bool mTerroristsAreBlue = true;
    public bool TerroristsAreBlue
    {
      get { return mTerroristsAreBlue; }
      set
      {
        if (mState != RoundState.Pending)
          throw new Exception("(DB.Round) terrorist team can not change when round is not pending");

        mTerroristsAreBlue = value;
        string sql = "Update Rounds SET TerroristsAreBlue=@1 WHERE RoundID=@2";
        DataBase.ExecuteSQL(sql, mTerroristsAreBlue, mRoundID);
      }
    }

    private int mNumber = -1;
    public int Number
    {
      get { return mNumber; }
      set { mNumber = value; }
    }
    #endregion Round Information

    #region Round State Information
    private RoundState mState = RoundState.Pending;
    public RoundState State
    {
      get { return mState; }
    }

    /// <summary>
    /// A round is eligible when its in progress or finished.
    /// </summary>
    public bool Eligible
    {
      get { return mState == RoundState.InProgress || mState == RoundState.Finished; }
    }

    public string StateString
    {
      get
      {
        if (mState == RoundState.Pending)
          return "در انتظار شروع";

        if (mState == RoundState.Aborted)
          return "کنسل شده";

        if (mState == RoundState.InProgress)
          return "در حال اجرا";

        if (mWinner == WinnerType.Tie)
          return "مساوی";
        else if (mWinner == WinnerType.Blue)
          return Game.GetBlueTeamName("آبی") + " برد.";
        else if (mWinner == WinnerType.Green)
          return Game.GetGreenTeamName("سبز") + " برد.";
        else
          return "WTF";
      }
    }

    private WinnerType mWinner = WinnerType.NotPlayed;
    public WinnerType Winner
    {
      get { return mWinner; }
    }
    #endregion Round State Information

    #region Round Time Information
    public int CurrentTime
    {
      get
      {
        if (mStart == null)
          return 0;

        PersianDateTime dt = PersianDateTime.Now;
        return TinyMath.Round(dt.GetTimeDifference(mStart));
      }
    }

    public int RemainingTime
    {
      get
      {
        if (mState == RoundState.Aborted)
          return Game.RoundTime - TinyMath.Round(mFinish.GetTimeDifference(mStart));
        else if (mState == RoundState.Pending)
          return Game.RoundTime;
        else if (mState == RoundState.Finished)
        {
          if (mBombState == SpecialForce.BombState.Diffused)
            return Game.RoundTime - mPlantTime;
          else if (mBombState == SpecialForce.BombState.Exploded)
            return Game.RoundTime - mPlantTime;
          else
            return 0;
        }
        else if (mState == RoundState.InProgress)
        {
          if (mBombState == SpecialForce.BombState.Planted)
            return Game.RoundTime - mPlantTime;
          else
            return Game.RoundTime - CurrentTime;
        }
        else
          throw new Exception("(DB.Round) Round state is unknown! " + Enums.ToString(mState));
      }
    }
    #endregion Round Time Information

    #region Plant Information
    private int mPlanterID = -1; // UserID
    private User mCachedPlanter = null;
    public User Planter
    {
      get 
      {
        if (mPlanterID == -1)
          return null;

        if (mCachedPlanter == null)
          mCachedPlanter = User.GetUserByID(mPlanterID);

        return mCachedPlanter; 
      }
    }

    public TeamType PlanterTeam
    {
      get
      {
        foreach (User u in BluePlayers)
          if (u.ID == mPlanterID)
            return TeamType.Blue;

        foreach (User u in GreenPlayers)
          if (u.ID == mPlanterID)
            return TeamType.Green;

        throw new Exception("(DB.Round) planter is null, can not get team");
      }
    }

    private int mPlantTime = -1;
    public int PlantTime
    {
      get { return mPlantTime; }
    }
    #endregion Plant Information

    #region Diffuse Information
    private int mDiffuserID = -1; // UserID
    private User mCachedDiffuser = null;
    public User Diffuser
    {
      get 
      {
        if (mDiffuserID == -1)
          return null;

        if (mCachedDiffuser == null)
          mCachedDiffuser = User.GetUserByID(mDiffuserID);

        return mCachedDiffuser; 
      }
    }

    public TeamType DiffuserTeam
    {
      get
      {
        foreach (User u in BluePlayers)
          if (u.ID == mDiffuserID)
            return TeamType.Blue;

        foreach (User u in GreenPlayers)
          if (u.ID == mDiffuserID)
            return TeamType.Green;

        throw new Exception("(DB.Round) diffuser is null, can not get team");
      }
    }

    private int mDiffuseTime = -1;
    public int DiffuseTime
    {
      get { return mDiffuseTime; }
    }
    #endregion Diffuse Information

    #region Bomb State Information
    private BombState mBombState = BombState.Disabled;
    public BombState BombState
    {
      get { return mBombState; }
    }

    private int mPlantedBomb = -1;
    /// <summary>
    /// Index of the planted bomb in the tower
    /// </summary>
    public int PlantedBomb
    {
      get { return mPlantedBomb; }
    }

    public string BombStateString
    {
      get
      {
        if (mBombState == BombState.Disabled)
          return "غیر فعال";

        if (mBombState == BombState.Enabled)
          return "آماده فعال سازی";

        string result = "توسط " + Planter.FullName + " گذاشته شد";
        if (mBombState == BombState.Planted)
        { }
        if (mBombState == BombState.Diffused)
        {
          result += "و توسط ";
          result += Diffuser.FullName;
          result += "خنثی شد";
        }
        else
          result += "منفجر شد";

        return result;
      }
    }

    public int RemainingBombTime
    {
      get 
      {
        if (mBombState == SpecialForce.BombState.Diffused)
          return Game.BombTime - (mDiffuseTime - mPlantTime);

        if (mBombState == SpecialForce.BombState.Exploded)
          return 0;

        if (mBombState != SpecialForce.BombState.Planted)
          return Game.BombTime;

        // Bomb is planted:
        if(mState ==  RoundState.InProgress)
          return Game.BombTime - (CurrentTime - mPlantTime);

        return Game.BombTime -(TinyMath.Round(mFinish.GetTimeDifference(mStart)) - mPlantTime);
      }
    }
    #endregion Bomb State Information

    #region Player Information
    private List<User> mBluePlayers = null;
    public List<User> BluePlayers
    {
      get 
      {
        if (mBluePlayers == null)
          LoadPlayersInternal();

        return mBluePlayers; 
      }
    }

    private List<User> mGreenPlayers = null;
    public List<User> GreenPlayers
    {
      get 
      {
        if (mGreenPlayers == null)
          LoadPlayersInternal();

        return mGreenPlayers; 
      }
    }

    /// <summary>
    /// Dictionary, Maps player id to his kevlar number
    /// </summary>
    private Dictionary<int, int> mPlayerKevlars = null;
    public Dictionary<int, int> PlayerKevlars
    {
      get 
      {
        if (mPlayerKevlars == null)
          LoadPlayersInternal();

        return mPlayerKevlars;
      }
    }
    #endregion Player Information

    #region Kill Information
    private List<KillInfo> mKills = null;
    public List<KillInfo> Kills
    {
      get 
      {
        if (mKills == null)
          mKills = KillInfo.GetKillsOfRound(mGameID, mRoundID);

        return mKills; 
      }
    }
    #endregion Kill Information
    #endregion Properties


    #region Constructors
    public Round(int game_id, int round_nr, bool terrors_are_blue)
    {
      // Initialize the round
      mGameID = game_id;
      mNumber = round_nr;
      mTerroristsAreBlue = terrors_are_blue;
      mState = RoundState.Pending;
      Apply();

      // Now initialize the players in the round till further update by FormGameStart
      Game g = Game;
      if (mNumber == 1)
        AssignPlayersAndKevlars(g.BlueUsers, g.GreenUsers);
      else
      {
        Round r = g.Rounds[g.Rounds.Count - 1];
        AssignPlayersAndKevlars(r.BluePlayers, r.GreenPlayers);
      }
    }

    private Round()
    {
    }
    #endregion Constructors


    #region Statistics
    public bool PlayerIsForTeam(int uid, TeamType tt)
    {
      List<User> players = tt == TeamType.Blue ? BluePlayers : GreenPlayers;
      foreach (User user in players)
        if (user.ID == uid)
          return true;

      return false;
    }

    public int GetKillCount(TeamType tt)
    {
      int cnt = 0;
      foreach (KillInfo inf in Kills)
        if (PlayerIsForTeam(inf.KillerID, tt))
          if (inf.Legal)
            ++cnt;

      return cnt;
    }

    public int GetFriendlyKillCount(TeamType tt)
    {
      int cnt = 0;
      foreach (KillInfo inf in Kills)
        if (PlayerIsForTeam(inf.KillerID, tt))
          if (!inf.Legal)
            ++cnt;

      return cnt;
    }

    public int GetKillCount(User player)
    {
      int cnt = 0;
      foreach (KillInfo inf in Kills)
        if (inf.KillerID == player.ID)
          if (inf.Legal)
            ++cnt;

      return cnt;
    }

    public int GetFriendlyKillCount(User player)
    {
      int cnt = 0;
      foreach (KillInfo inf in Kills)
        if (inf.KillerID == player.ID)
          if (!inf.Legal)
            ++cnt;

      return cnt;
    }

    public int GetDeathCount(TeamType tt)
    {
      int cnt = 0;
      foreach (KillInfo inf in Kills)
        if (PlayerIsForTeam(inf.KilledID, tt))
          ++cnt;

      return cnt;
    }

    public int GetDeathCount(User player)
    {
      int cnt = 0;
      foreach (KillInfo inf in Kills)
        if (inf.KilledID == player.ID)
          ++cnt;

      return cnt;
    }

    public int GetAliveCount(TeamType tt)
    {
      int cnt = 0;
      List<User> players = tt == TeamType.Blue ? BluePlayers : GreenPlayers;
      foreach (DB.User player in players)
        if (player.AssignedKevlar.State == KevlarState.Working)
          ++cnt;

      return cnt;
    }
    #endregion Statistics


    #region Round Interactions
    public void RegisterNewKill(User killer, User killed)
    {
      bool legal = killer.AssignedKevlar.Team != killed.AssignedKevlar.Team;
      Kills.Add(new KillInfo(mGameID, mRoundID, killer.ID, killed.ID, CurrentTime, legal));
    }

    public void BombUpdated()
    {
      ControlTower.Bomb bomb = ControlTower.Tower.ActiveBomb;
      mBombState = bomb.State;

      if (mBombState == BombState.Planted)
      {
        mPlantedBomb = Kevlars.IndexFromID(bomb.ID);
        mPlantTime = CurrentTime;
        mPlanterID = bomb.PlanterID != DeviceID.Unknown ? bomb.PlanterUser.ID : -1;
        if(!PlayerKevlars.ContainsKey(mPlanterID))
          mPlanterID = -1;
      }
      else if (mBombState == BombState.Diffused)
      {
        mDiffuseTime = CurrentTime;
        mDiffuserID = bomb.DiffuserID != DeviceID.Unknown ? bomb.DiffuserUser.ID : -1;
        if (!PlayerKevlars.ContainsKey(mPlanterID))
          mDiffuserID = -1;
      }
    }

    public void AssignPlayersAndKevlars(List<User> blue_team, List<User> green_team)
    {
      // Clear previous data
      BluePlayers.Clear();
      GreenPlayers.Clear();
      PlayerKevlars.Clear();

      // Set players in the round and register their kevlar number
      foreach (User u in blue_team)
        if (u.AssignedKevlar != null)
        {
          BluePlayers.Add(u);
          PlayerKevlars[u.ID] = u.AssignedKevlar.Number;
        }

      foreach (User u in green_team)
        if(u.AssignedKevlar != null)
        {
          GreenPlayers.Add(u);
          PlayerKevlars[u.ID] = u.AssignedKevlar.Number;
        }

      // Remove all previous data
      string sql = "DELETE FROM PlayersInRounds WHERE RoundID=@1";
      DataBase.ExecuteSQL(sql, mRoundID);

      // Update players and their kevlar numbers in the database
      sql = "INSERT INTO PlayersInRounds (RoundID, UserID, PlayerIsForBlueTeam, KevlarNumber) VALUES (@1, @2, @3, @4)";
      foreach (User u in BluePlayers)
        DataBase.ExecuteSQL(sql, mRoundID, u.ID, true, u.AssignedKevlar != null ? u.AssignedKevlar.Number : -1);
      foreach (User u in GreenPlayers)
        DataBase.ExecuteSQL(sql, mRoundID, u.ID, false, u.AssignedKevlar != null ? u.AssignedKevlar.Number : -1);
    }

    public void RoundStarted()
    {
      if (mState != RoundState.Pending)
        throw new Exception("(DB.Round) round is already started");

      // Now update the round state
      mBombState = 
        Game.Object == GameObject.Bombing ? 
        SpecialForce.BombState.Enabled : 
        SpecialForce.BombState.Disabled;
      mState = RoundState.InProgress;
      mStart = PersianDateTime.Now;
      Apply();

      // Start round timer on game manager
      DB.GameManager.StartRoundTimer();
    }

    public void RoundFinished(WinnerType winner)
    {
      mState = RoundState.Finished;
      mFinish = PersianDateTime.Now;
      mWinner = winner;
      Apply();
    }

    public void RoundAborted()
    {
      mState = RoundState.Aborted;
      if (mStart == null)
        mStart = PersianDateTime.Now;
      mFinish = PersianDateTime.Now;
      Apply();
    }

    public void DeleteRound()
    {
      string sql = "DELETE FROM Rounds WHERE RoundID=@1";
      if (DataBase.ExecuteSQL(sql, mRoundID) != 1)
        throw new Exception("(DB.Round) could not delete round");

      // First delete previous players in round. This
      //   will happen when a round was not successfully
      //   started in its previous try.
      sql = "DELETE FROM PlayersInRounds WHERE RoundID=@1";
      DataBase.ExecuteSQL(sql, mRoundID);

      // Reset game and round ids
      mGameID = -1;
      mRoundID = -1;
    }
    #endregion Round Interactions


    #region Check Round Winner
    static public WinnerType CheckRoundWinner(DB.Round round)
    {
      // Can not have a winner if round is not in progress
      if (round.mState != RoundState.InProgress)
        return WinnerType.NoWinner;

      // Exlude dummy games
      if (round.Game.Type == CompetitionType.Dummy)
      {
        if (round.GreenPlayers.Count == 0)
          return WinnerType.NoWinner;

        if (round.BluePlayers.Count == 0)
          return WinnerType.NoWinner;
      }

      // Check for winner based on the game's object
      if (round.Game.Object == GameObject.DeathMatch)
        return CheckRegularMatchWinner(round);
      else if (round.Game.Object == GameObject.Bombing)
        return CheckBombingMatchWinner(round);
      else
        throw new Exception("(DB.Round) Round type not recognized to check for winner" + Enums.ToString(round.Game.Object));
    }

    /// <summary>
    /// Checks for winner of a regular match. Order of decision:
    /// > Check for alive counts:
    ///   - Blue count = 0 ? Winner = green
    ///   - Green count = 0 ? Winner = blue
    ///  
    /// > Time still ongoing? no winner
    /// 
    /// > Who has higher kills?
    ///   - Blue kills > Green kills ? Blue
    ///   - Blue kills > Green kills ? Blue
    ///   
    /// > Who has lower friendly kills?
    ///   - Blue friendly kills > Green friendly kills ? Green
    ///   - Green friendly kills > Blue friendly kills ? Blue
    ///   
    /// > A tie.
    /// </summary>
    static private WinnerType CheckRegularMatchWinner(DB.Round round)
    {
      #region Check For Alive Counts
      if (round.GetAliveCount(TeamType.Blue) == 0)
        return WinnerType.Green;

      if (round.GetAliveCount(TeamType.Green) == 0)
        return WinnerType.Blue;
      #endregion Check For Alive Counts


      #region Time Still Ongoing
      if (round.RemainingTime > 0)
        return WinnerType.NoWinner;
      #endregion Time Still Ongoing


      #region Who Has Higher Kills?
      // > Who has higher kills?
      if (round.GetKillCount(TeamType.Blue) > round.GetKillCount(TeamType.Green))
        return WinnerType.Blue;
      else if (round.GetKillCount(TeamType.Green) > round.GetKillCount(TeamType.Blue))
        return WinnerType.Green;
      #endregion Who Has Higher Kills?


      #region Who Has Lower Friendly Kills?
      if (round.GetFriendlyKillCount(TeamType.Blue) > round.GetFriendlyKillCount(TeamType.Green))
        return WinnerType.Green;
      
      if (round.GetFriendlyKillCount(TeamType.Green) > round.GetFriendlyKillCount(TeamType.Blue))
        return WinnerType.Blue;
      #endregion Who Has Lower Friendly Kills?

      // > A Tie
      return WinnerType.Tie;
    }

    /// <summary>
    /// Checks for winner of a bombing match. Order of decision:
    /// > Bomb Exploded?
    ///   - Terrorist won
    /// 
    /// > Bomb Diffused?
    ///   - Counter Terrorist won
    ///   
    /// > Bomb Planted?
    ///   - Counter Terrorists are all dead? Terrorists won
    ///   
    /// > Bomb Not Planted?
    ///    > Check For Alive Counts
    ///      - Blue count = 0 ? Winner = green
    ///      - Green count = 0 ? Winner = blue
    ///      
    ///    > Round Time Finished?
    ///      - Counter Terrorist won
    ///  
    /// </summary>
    static private WinnerType CheckBombingMatchWinner(DB.Round round)
    {
      #region Bomb Exploded?
      if (round.BombState == BombState.Exploded)
        return round.mTerroristsAreBlue ? WinnerType.Blue : WinnerType.Green;

      if (round.RemainingBombTime == 0)
        return round.mTerroristsAreBlue ? WinnerType.Blue : WinnerType.Green;
      #endregion Bomb Exploded?


      #region Bomb Diffused?
      if (round.mBombState == BombState.Diffused)
        return round.mTerroristsAreBlue ? WinnerType.Green : WinnerType.Blue;
      #endregion Bomb Diffused?


      #region Bomb Planted?
      // Counter Terrorists are all dead? Terrorists won
      if (round.mBombState == BombState.Planted)
      {
        TeamType ct = round.mTerroristsAreBlue ? TeamType.Green : TeamType.Blue;
        if (round.GetAliveCount(ct) == 0)
          return ct == TeamType.Blue ? WinnerType.Green : WinnerType.Blue;

        // Otherwise resume round.
        return WinnerType.NoWinner;
      }
      #endregion Bomb Planted?


      #region Bomb Not Planted?
      // Check For Alive Counts
      if (round.GetAliveCount(TeamType.Blue) == 0)
        return WinnerType.Green;
      if (round.GetAliveCount(TeamType.Green) == 0)
        return WinnerType.Blue;

      // Round Time Finished?
      if (round.RemainingTime == 0)
        return round.mTerroristsAreBlue ? WinnerType.Green : WinnerType.Blue;

      return WinnerType.NoWinner;
      #endregion Bomb Not Planted?
    }
    #endregion Check Round Winner


    #region Utilities
    public void AddUserIDs(SortedSet<int> ids)
    {
      for (int i = 0; i < BluePlayers.Count; ++i)
        if (!ids.Contains(BluePlayers[i].ID))
          ids.Add(BluePlayers[i].ID);

      for (int i = 0; i < GreenPlayers.Count; ++i)
        if (!ids.Contains(GreenPlayers[i].ID))
          ids.Add(GreenPlayers[i].ID);
    }

    /// <summary>
    /// Finds out the user in the given kevlar id.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public DB.User GetUserWithDeviceID(DeviceID id)
    {
      int num = Kevlars.NumberFromID(id);
      Dictionary<int, int>.Enumerator it = PlayerKevlars.GetEnumerator();
      while(it.MoveNext())
        if(it.Current.Value == num)
          return DB.User.GetUserByID(it.Current.Key);

      return null;
    }

    static public string TimeToString(int seconds)
    {
      return PersianDateTime.Time(0, 0, seconds).ToString(DateTimeString.TimeNoHour);
    }
    #endregion Utilities


    #region DataBase Interactions
    private void LoadPlayersInternal()
    {
      // read players in round
      List<int> blue_ids = new List<int>();
      List<int> green_ids = new List<int>();
      mPlayerKevlars = new Dictionary<int, int>();

      string sql = "SELECT * FROM PlayersInRounds WHERE RoundID=@1";
      foreach(DataRow row in DataBase.QueryResult(sql, mRoundID).Rows)
      {
        int uid = row.Field<int>("UserID");
        if (row.Field<bool>("PlayerIsForBlueTeam"))
          blue_ids.Add(uid);
        else
          green_ids.Add(uid);

        // also add to player <--> kevlar dictionary
        mPlayerKevlars[uid] = row.Field<int>("KevlarNumber");
      }

      mBluePlayers = User.LoadUsers(blue_ids);
      mGreenPlayers = User.LoadUsers(green_ids);
    }

    static public List<Round> LoadGameRounds(int game_id)
    {
      List<Round> result = new List<Round>();
      string sql = "SELECT * FROM Rounds WHERE GameID=@1";
      foreach (DataRow row in DataBase.QueryResult(sql, game_id).Rows)
        result.Add(LoadFromData(row));
      return result;
    }

    static public Round LoadFromData(DataRow data)
    {
      Round result = new Round();
      result.mRoundID = Convert.ToInt32(data["RoundID"]);
      result.mGameID = data.Field<int>("GameID");
      result.mNumber = data.Field<int>("RoundNumber");
      result.mStart = PersianDateTime.FromObject(data["Start"]);
      result.mFinish = PersianDateTime.FromObject(data["Finish"]);
      result.mState = Enums.ParseRoundState(data.Field<string>("State"));
      result.mWinner = (WinnerType)data.Field<int>("Winner");
      result.mTerroristsAreBlue = data.Field<bool>("TerroristsAreBlue");
      result.mDiffuserID = data.Field<int>("Diffuser");
      result.mDiffuseTime = data.Field<int>("DiffuseTime");
      result.mPlanterID = data.Field<int>("Planter");
      result.mPlantTime = data.Field<int>("PlantTime");
      result.mPlantedBomb = data.Field<int>("PlantedBomb");
      result.mBombState = Enums.BombStateFromString(data.Field<string>("BombState"));
      return result;
    }

    private void Apply()
    {
      string sql;
      if (mRoundID == -1)
      {
        sql = "INSERT INTO Rounds(GameID, RoundNumber, State, TerroristsAreBlue) ";
        sql += "VALUES (@1, @2, @3, @4)";

        if (DataBase.ExecuteSQL(sql, mGameID, mNumber, Enums.ToString(mState), mTerroristsAreBlue) != 1)
          throw new Exception("(DB.Round) could not insert new round");

        mRoundID = DataBase.InsertID;
      }
      else
      {
        sql = "UPDATE Rounds SET Start=@1, Finish=@2, State=@3, Winner=@4, Diffuser=@5, DiffuseTime=@6, Planter=@7, PlantTime=@8, PlantedBomb=@9, BombState=@10, TerroristsAreBlue=@11 WHERE RoundID=@12";
        if (DataBase.ExecuteSQL(sql, PersianDateTime.ToObject(mStart), PersianDateTime.ToObject(mFinish), Enums.ToString(mState), mWinner, mDiffuserID, mDiffuseTime, mPlanterID, mPlantTime, mPlantedBomb, Enums.ToString(mBombState), mTerroristsAreBlue, mRoundID) != 1)
          throw new Exception("(DB.Round) could not update round information");
      }
    }
    #endregion DataBase Interactions
  }
}
