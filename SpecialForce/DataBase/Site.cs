﻿using System;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class Site
  {
    #region Properties
    private int mSiteID = -1;
    public int ID
    {
      get { return mSiteID; }
    }

    private string mFullName = "";
    public string FullName
    {
      get { return mFullName; }
      set { mFullName = value; }
    }

    private string mShortName = "";
    public string ShortName
    {
      get { return mShortName; }
      set { mShortName = value; }
    }

    private string mAddress = "";
    public string Address
    {
      get { return mAddress; }
      set { mAddress = value; }
    }

    private string mTelephone = "";
    public string Telephone
    {
      get { return mTelephone; }
      set { mTelephone = value; }
    }

    private PersianDateTime mStartDate = null;
    public PersianDateTime StartDate
    {
      get { return mStartDate == null ? null : new PersianDateTime(mStartDate); }
    }

    private PersianDateTime mEndDate = null;
    public PersianDateTime EndDate
    {
      get { return mEndDate == null ? null : new PersianDateTime(mEndDate); }
    }

    public bool IsInUse
    {
      get { return mEndDate == null; }
    }

    public int NrOfRegisteredUsers
    {
      get { return DataBase.EvaluateFunction("NrOfRegisteredUsers", "@site_id", mSiteID); }
    }

    public List<SiteClearance> Clearances
    {
      get { return DB.SiteClearance.GetSiteClearances(this); }
    }

    /// <summary>
    /// This method checks whether a site with the given ID is closed or still operating.
    /// </summary>
    /// <param name="id">Site ID to check</param>
    /// <returns>True if the site is closed, false otherwise</returns>
    static public bool IsClosed(int id)
    {
      return
        DataBase.QueryValue("SELECT Count(SiteID) FROM Sites WHERE SiteID=@1 AND UsageEnd IS NOT NULL", 0, id) == 0;
    }
    #endregion Properties


    #region Constructor
    public Site()
    {
    }
    #endregion Constructor


    #region Site Utilities
    public bool StopUsage()
    {
      if (mEndDate != null)
        return false;

      if (DataBase.QueryValue("SELECT COUNT(SiteID) FROM Sites WHERE UsageEnd IS NULL", 0) <= 1)
      {
        Logger.Log("(DB.Site) Can not delete last active site.");
        return false;
      }

      mEndDate = PersianDateTime.Now;
      string sql = "UPDATE Sites SET UsageEnd=@1 WHERE SiteID=@2";
      if (DataBase.ExecuteSQL(sql, mEndDate.Value, mSiteID) != 1)
        throw new Exception("(DB.Site) could not stop usage of the site");
      return true;
    }

    public override string ToString()
    {
      return mShortName;
    }
    #endregion Site Utilities


    #region Usage Interface
    /// <summary>
    /// This method returns site information for the given site ID. If the site has not yet been loaded, it will try to load it.
    /// </summary>
    /// <param name="id">Site ID to load</param>
    /// <returns>Site information instance</returns>
    static public Site GetSiteByID(int id)
    {
      DataTable query = DataBase.QueryResult("SELECT * FROM Sites WHERE SiteID=@1", id);
      if (query.Rows.Count != 1)
      {
        Logger.Log("(DB.SiteManager) Given site does not exist or has a data problem: " + id.ToString());
        return null;
      }

      Site temp = Site.LoadFromData(query.Rows[0]);
      if (temp == null)
        throw new Exception("(DB.SiteManager) Problem loading site information for id " + id.ToString());
      return temp;
    }

    /// <summary>
    /// This method returns the name of site with the given ID
    /// </summary>
    /// <param name="id">ID of the wanted site name</param>
    /// <returns>Name of the site</returns>
    static public string GetSiteName(int id)
    {
      return DataBase.QueryValue("SELECT ShortName FROM Sites WHERE SiteID=@1", "", id);
    }

    /// <summary>
    /// Returns all sites in the database.
    /// </summary>
    /// <param name="include_closed">Whether include already close sites or not</param>
    static public List<Site> GetAllSites(bool include_closed, string search = "")
    {
      List<Site> result = new List<Site>();
      string sql = "SELECT * FROM Sites";
      if (!include_closed)
      {
        sql += " WHERE UsageEnd IS NULL";
        if(search.Length != 0)
          sql += " AND (ShortName LIKE '%" + search + "%'" + " OR FullName LIKE '%" + search + "%')";
      }
      else if (search.Length != 0)
        sql += " AND (ShortName LIKE '%" + search + "%'" + " OR FullName LIKE '%" + search + "%')";

      string time = Profiler.TimeString(() =>
      {
        foreach (DataRow row in DataBase.QueryResult(sql).Rows)
          result.Add(LoadFromData(row));
      });

      Logger.Log("(DB.SiteManager) Loaded " + result.Count.ToString() + " sites in " + time);
      return result;
    }
    #endregion Usage Interface


    #region DataBase Interactions
    static private Site LoadFromData(DataRow data)
    {
      try
      {
        Site site = new Site();
        site.mSiteID = Convert.ToInt32(data["SiteID"]);
        site.mFullName = data.Field<string>("FullName");
        site.mShortName = data.Field<string>("ShortName");
        site.mAddress = data.Field<string>("Address");
        site.mTelephone = data.Field<string>("Telephone");
        site.mStartDate = PersianDateTime.FromObject(data["UsageStart"]);
        site.mEndDate = PersianDateTime.FromObject(data["UsageEnd"]);
        return site;
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "DB.Site");
        return null;
      }
    }

    public void Apply()
    {
      string sql;
      if (mSiteID == -1) // shall insert
      {
        mStartDate = PersianDateTime.Now;

        sql = "INSERT INTO Sites (FullName, ShortName, Address, Telephone, UsageStart) VALUES ";
        sql += "(@1, @2, @3, @4, @5)";
        if (DataBase.ExecuteSQL(sql, mFullName, mShortName, mAddress, mTelephone, mStartDate.Value) != 1)
          throw new Exception("(DB.Site) probable fault when inserting new row");

        mSiteID = DataBase.InsertID;
      }
      else
      {
        sql = "UPDATE Sites SET FullName=@1, ShortName=@2, Address=@3, Telephone=@4 ";
        sql += "WHERE SiteID=@5";

        if (DataBase.ExecuteSQL(sql, mFullName, mShortName, mAddress, mTelephone, mSiteID) != 1)
          throw new Exception("(DB.Site) probable fault when updating a row");
      }
    }
    #endregion DataBase Interactions
  }
}
