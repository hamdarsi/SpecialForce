﻿using System;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class Note
  {
    #region Properties
    private int mNoteID = -1;
    public int ID
    {
      get { return mNoteID; }
    }

    private string mNote = "";
    public string Memo
    {
      get { return mNote; }
      set { mNote = value; }
    }

    private int mUserID = -1;
    public int UserID
    {
      get { return mUserID; }
    }

    private PersianDateTime mLastChange = null;
    public PersianDateTime LastChange
    {
      get { return mLastChange == null ? null : new PersianDateTime(mLastChange); }
    }

    private bool mDeleted = false;
    public bool Deleted
    {
      get { return mDeleted; }
      set { mDeleted = value; }
    }
    #endregion Properties


    #region Constructor
    public Note(int uid = -1)
    {
      mUserID = uid;
    }
    #endregion Constructor


    #region Note Related Procedure
    public void Delete()
    {
      if (mNoteID == -1)
        throw new Exception("(DB.Note) can not delete a note which is not in DB");

      if(mDeleted)
        throw new Exception("(DB.Note) can not delete a previously deleted note");

      string sql = "UPDATE Notes SET Deleted=@1 WHERE NoteID=@2";
      if (DataBase.ExecuteSQL(sql, true, mNoteID) != 1)
        throw new Exception("(DB.Note) could not delete the note");
    }

    static public List<Note> GetValidUserNotes(int uid)
    {
      List<Note> notes = new List<Note>();

      string sql = "SELECT * FROM Notes WHERE UserID=@1 AND Deleted=@2";
      foreach (DataRow row in DataBase.QueryResult(sql, uid, false).Rows)
        notes.Add(LoadFromData(row));

      return notes;
    }

    static public List<Note> GetAllUserNotes(int uid)
    {
      List<Note> notes = new List<Note>();

      string sql = "SELECT * FROM Notes WHERE UserID=@1";
      foreach (DataRow row in DataBase.QueryResult(sql, uid).Rows)
        notes.Add(LoadFromData(row));

      return notes;
    }
    #endregion Note Related Procedure


    #region DataBase Interactions
    static private Note LoadFromData(DataRow data)
    {
      Note result = new Note();
      result.mNoteID = Convert.ToInt32(data["NoteID"]);
      result.mNote = data.Field<string>("Note");
      result.mUserID = data.Field<int>("UserID");
      result.mLastChange = PersianDateTime.FromObject(data["LastChange"]);
      result.mDeleted = data.Field<bool>("Deleted");
      return result;
    }

    public void Apply()
    {
      if (mNoteID == -1)
      {
        mLastChange = PersianDateTime.Now;
        string sql = "INSERT INTO Notes (Note, UserID, LastChange) VALUES (@1, @2, @3)";
        if (DataBase.ExecuteSQL(sql, mNote, mUserID, PersianDateTime.ToObject(mLastChange)) != 1)
          throw new Exception("(DB.Note) could not insert note");

        mNoteID = DataBase.InsertID;
      }
      else
      {
        string sql = "UPDATE Notes SET Note=@1, LastChange=@2 WHERE NoteID=@3";
        if (DataBase.ExecuteSQL(sql, mNote, PersianDateTime.ToObject(mLastChange), mNoteID) != 1)
          throw new Exception("(DB.Note) could not update note");
      }
    }
    #endregion DataBase Interactions
  }
}
