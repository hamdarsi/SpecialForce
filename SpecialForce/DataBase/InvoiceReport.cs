﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class InvoiceReport
  {
    #region Internal Types and Data
    private struct SiteReport
    {
      public PersianDateTime Start;
      public PersianDateTime End;
      public Site Site;
      public List<Invoice> Invoices;
    }

    private List<SiteReport> mSites = new List<SiteReport>();
    #endregion Internal Types and Data


    #region Properties
    private SortedSet<InvoiceGroup> mInclusions = new SortedSet<InvoiceGroup>();
    public SortedSet<InvoiceGroup> Inclusions
    {
      get { return mInclusions; }
    }

    private double mTotalUserCharges = 0;
    public double TotalUserCharges
    {
      get { return mTotalUserCharges; }
    }

    private double mTotalUserRefunds = 0;
    public double TotalUserRefunds
    {
      get { return mTotalUserRefunds; }
    }

    private double mTotalUserUsages = 0;
    public double TotalUserUsages
    {
      get { return mTotalUserUsages; }
    }

    private double mTotalGiftCharges = 0;
    public double TotalGiftCharges
    {
      get { return mTotalGiftCharges; }
    }

    private double mTotalCupAdmissions = 0;
    public double TotalCupAdmissions
    {
      get { return mTotalCupAdmissions; }
    }

    private double mTotalCupExpenses = 0;
    public double TotalCupExpenses
    {
      get { return mTotalCupExpenses; }
    }

    private double mTotalLeagueAdmissions = 0;
    public double TotalLeagueAdmissions
    {
      get { return mTotalLeagueAdmissions; }
    }

    private double mTotalLeagueExpenses = 0;
    public double TotalLeagueExpenses
    {
      get { return mTotalLeagueExpenses; }
    }

    private double mTotalEliminationAdmissions = 0;
    public double TotalEliminationAdmissions
    {
      get { return mTotalEliminationAdmissions; }
    }

    private double mTotalEliminationExpenses = 0;
    public double TotalEliminationExpenses
    {
      get { return mTotalEliminationExpenses; }
    }

    private double mTotalRentExpenses = 0;
    public double TotalRentExpenses
    {
      get { return mTotalRentExpenses; }
    }

    private double mTotalBillExpenses = 0;
    public double TotalBillExpenses
    {
      get { return mTotalBillExpenses; }
    }

    private double mTotalOtherExpenses = 0;
    public double TotalOtherExpenses
    {
      get { return mTotalOtherExpenses; }
    }

    private double mTotalClearances = 0;
    public double TotalClearances
    {
      get { return mTotalClearances; }
    }

    public double TotalCompetitionExpenses
    {
      get { return mTotalLeagueExpenses +
                   mTotalCupExpenses +
                   mTotalEliminationExpenses; }
    }

    public double TotalCompetitionAdmissions
    {
      get { return mTotalLeagueAdmissions +
                   mTotalCupAdmissions +
                   mTotalEliminationAdmissions; }
    }

    public double TotalExpenses
    {
      get { return mTotalRentExpenses +
                   mTotalBillExpenses +
                   mTotalOtherExpenses +
                   TotalCompetitionExpenses; }
    }

    public double TotalRefunds
    {
      get { return mTotalUserRefunds; }
    }

    public double TotalCharges
    {
      get { return mTotalUserCharges + TotalCompetitionAdmissions; }
    }

    public double Balance
    {
      get { return TotalCharges - (TotalExpenses + TotalUserRefunds + mTotalClearances); }
    }

    public List<Invoice> Invoices
    {
      get
      {
        List<Invoice> result = new List<Invoice>();
        foreach (SiteReport sr in mSites)
          result.AddRange(sr.Invoices);

        return result;
      }
    }

    public List<Site> Sites
    {
      get
      {
        List<Site> result = new List<Site>();
        foreach (SiteReport sr in mSites)
          result.Add(sr.Site);
        return result;
      }
    }

    private string mDateString = "";
    public string DateString
    {
      get { return mDateString; }
      set { mDateString = value; }
    }
    #endregion Properties


    #region Site Inclusion Utilities
    /// <summary>
    /// Checks whether the given site is used in the report or not.
    /// </summary>
    /// <param name="id">The Site ID to check existing</param>
    /// <returns>True if the given site id is in the report, false otherwise</returns>
    private bool IncludesSite(int id)
    {
      foreach (SiteReport sr in mSites)
        if (sr.Site.ID == id)
          return true;

      return false;
    }

    /// <summary>
    /// Inculdes the given time period for the report for all registered sites.
    /// </summary>
    /// <param name="start">Starting point for the report</param>
    /// <param name="end">Ending point for the report</param>
    public void IncludeAllSites(PersianDateTime start, PersianDateTime end)
    {
      foreach (Site site in Site.GetAllSites(true))
        IncludeSite(site, start, end);
    }

    /// <summary>
    /// This method includes the given site invoices within the given time period. Site 
    /// will be given as an instance not ID, this will speed up query by caching the 
    /// site data for a small time.
    /// </summary>
    /// <param name="st">The site to include in the report</param>
    /// <param name="start">Starting point for the report</param>
    /// <param name="end">Ending point for the report</param>
    public void IncludeSite(Site st, PersianDateTime start, PersianDateTime end)
    {
      if (IncludesSite(st.ID))
        return;

      SiteReport sr = new SiteReport();
      sr.Invoices = new List<Invoice>();
      sr.Site = st;
      sr.Start = start;
      sr.End = end;
      mSites.Add(sr);
    }
    #endregion Site Inclusion Utilities


    #region Report Generation
    private bool AddInvoiceToReport(Invoice inv)
    {
      if (inv.Type == InvoiceType.UserCreditCharge)
      {
        if (mInclusions.Contains(InvoiceGroup.UserCharges))
          mTotalUserCharges += inv.Amount;
        else
          return false;
      }

      else if (inv.Type == InvoiceType.UserCreditRefund)
      {
        if (mInclusions.Contains(InvoiceGroup.UserRefunds))
          mTotalUserRefunds += inv.Amount;
        else
          return false;
      }

      else if (inv.Type == InvoiceType.UserCreditUsage)
      {
        if (mInclusions.Contains(InvoiceGroup.UserCharges))
          mTotalUserUsages += inv.Amount;
        else
          return false;
      }

      else if (inv.Type == InvoiceType.UserGiftCharge)
      {
        if (mInclusions.Contains(InvoiceGroup.GiftCharges))
          mTotalGiftCharges += inv.Amount;
        else
          return false;
      }

      else if (inv.Type == InvoiceType.Rent)
      {
        if (mInclusions.Contains(InvoiceGroup.Rents))
          mTotalRentExpenses += inv.Amount;
        else
          return false;
      }

      else if (inv.Type == InvoiceType.UntypedPayment)
      {
        if (mInclusions.Contains(InvoiceGroup.OthersExpenses))
          mTotalOtherExpenses += inv.Amount;
        else
          return false;
      }

      else if (inv.Type == InvoiceType.CompetitionExpense)
      {
        Competition c = CompetitionManager.GetCompetition(inv.CompetitionID);
        if (c.Type == CompetitionType.Cup && mInclusions.Contains(InvoiceGroup.CupExpenses))
          mTotalCupExpenses += inv.Amount;
        else if (c.Type == CompetitionType.Elimination && mInclusions.Contains(InvoiceGroup.EliminationExpenses))
          mTotalEliminationExpenses += inv.Amount;
        else if (c.Type == CompetitionType.League && mInclusions.Contains(InvoiceGroup.LeagueExpenses))
          mTotalLeagueExpenses += inv.Amount;
        else
          return false;
      }

      else if (inv.Type == InvoiceType.CompetitionAdmission)
      {
        Competition c = CompetitionManager.GetCompetition(inv.CompetitionID);
        if (c.Type == CompetitionType.Cup && mInclusions.Contains(InvoiceGroup.CupAdmission))
          mTotalCupAdmissions += inv.Amount;
        else if (c.Type == CompetitionType.Elimination && mInclusions.Contains(InvoiceGroup.EliminationAdmission))
          mTotalEliminationAdmissions += inv.Amount;
        else if (c.Type == CompetitionType.League && mInclusions.Contains(InvoiceGroup.LeagueAdmission))
          mTotalLeagueAdmissions += inv.Amount;
        else
          return false;
      }

      else if (inv.IsBill)
      {
        if (mInclusions.Contains(InvoiceGroup.Bills))
          mTotalBillExpenses += inv.Amount;
        else
          return false;
      }

      else if (inv.Type == InvoiceType.Clear)
        mTotalClearances += inv.Amount;

      else
        throw new Exception("unhandled invoice type: " + inv.ToString());

      return true;
    }

    public void MakeReport()
    {
      int count = 0;
      string time = Profiler.TimeString(() =>
      {
        // Get free style match charges => user credit charges
        foreach (SiteReport sr in mSites)
        {
          // Now get all invoices for the specified date range
          string sql = @"SELECT 
                          invoice_detail.*, 
                          CONCAT(Users.FirstName, ' ', Users.LastName) as OperatorFullName 

                        FROM
                          (SELECT
                             Accounting.*,
                             Teams.Name as TeamName,
                             Competitions.Title as CompetitionTitle,
                             Sites.ShortName as SiteName,
                             CONCAT(Users.FirstName, ' ', Users.LastName) as UserFullName

                           FROM  Accounting
                             LEFT JOIN Teams ON Accounting.TeamID=Teams.TeamID
                             LEFT JOIN Competitions ON Competitions.CompetitionID=Accounting.CompetitionID
                             LEFT JOIN Sites ON Sites.SiteID=Accounting.SiteID
                             LEFT JOIN Users ON Accounting.UserID=Users.UserID

                           WHERE
                             `TimeStamp`>=@1 AND `TimeStamp`<=@2 AND Accounting.SiteID=@3
                          ) invoice_detail

                          LEFT JOIN Users ON invoice_detail.OperatorID=Users.UserID
                          
                        ORDER BY invoice_detail.TimeStamp DESC";

          foreach (DataRow row in DataBase.QueryResult(sql, sr.Start.Value, sr.End.Value, sr.Site.ID).Rows)
            try
            {
              Invoice inv = Invoice.LoadFromData(row);
              if (AddInvoiceToReport(inv))
              {
                sr.Invoices.Add(inv);
                ++count;
              }
            }
            catch (Exception ex)
            {
              Logger.LogException(ex, "DB.InvoiceReport", "MakeReport()");
            }
        }
      });

      Logger.Log("(DB.InvoiceReport) Generated invoice report from " + count.ToString() + " invoices in " + time);
    }
    #endregion Report Generation
  }
}
