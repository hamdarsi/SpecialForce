﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  static public class Options
  {
    #region Properties
    static private double mGiftCreditNecessaryCharge = 50000.0;
    /// <summary>
    /// Necessary charge to give gift then. When player uses
    /// more credit than this parameter, he will be rewarded
    /// a gift amount. Unit = 1 Toman
    /// </summary>
    static public double GiftCreditNecessaryCharge
    {
      get { return mGiftCreditNecessaryCharge; }
      set { mGiftCreditNecessaryCharge = value; }
    }

    static private double mGiftCreditAmount = 5000.0;
    /// <summary>
    /// The amount of gift credit. Unit = 1 Toman
    /// </summary>
    static public double GiftCreditAmount
    {
      get { return mGiftCreditAmount; }
      set { mGiftCreditAmount = value; }
    }

    static private int mBombPlantFrag = 3;
    /// <summary>
    /// Awarded frag when a player plants the bomb and bomb explodes.
    /// Unit = 1 Frag
    /// </summary>
    static public int BombPlantFrag
    {
      get { return mBombPlantFrag; }
      set { mBombPlantFrag = value; }
    }

    static private int mBombDiffuseFrag = 3;
    /// <summary>
    /// Awarded frag when a player diffuses a bomb.
    /// Unit = 1 Frag
    /// </summary>
    static public int BombDiffuseFrag
    {
      get { return mBombDiffuseFrag; }
      set { mBombDiffuseFrag = value; }
    }

    static private int mKillFrag = 1;
    /// <summary>
    /// Awarded frag when a player kills a player from the enemy.
    /// Unit = 1 Frag
    /// </summary>
    static public int KillFrag
    {
      get { return mKillFrag; }
      set { mKillFrag = value; }
    }

    /// <summary>
    /// Penalty frag for the times when a teammate kills another teammate.
    /// Unit = 1 Frag
    /// </summary>
    static private int mFriendlyKillFrag = 1;
    static public int FriendlyKillFrag
    {
      get { return mFriendlyKillFrag; }
      set { mFriendlyKillFrag = value; }
    }

    static private int mMaximumTeamNameLength = 15;
    /// <summary>
    /// Maximum length of a team name. Unit = 1 Character
    /// </summary>
    static public int MaximumTeamNameLength
    {
      get { return mMaximumTeamNameLength; }
      set { mMaximumTeamNameLength = value; }
    }

    static private int mTeamMinimumMembers = 3;
    /// <summary>
    /// Minimum number of team members. Unit = 1 Member
    /// </summary>
    static public int TeamMinimumMembers
    {
      get { return mTeamMinimumMembers; }
      set { mTeamMinimumMembers = value; }
    }

    static private int mRefundableCancelDuration = 72;
    /// <summary>
    /// The period in hours which authorizes refund upon cancelling games.
    /// </summary>
    static public int RefundableCancelDuration
    {
      get { return mRefundableCancelDuration; }
      set { mRefundableCancelDuration = value; }
    }
    #endregion Properties


    #region Constructor
    static Options()
    {
      Logger.Log("(DB.Options) Checking for options:");
      if (DataBase.QueryValue("SELECT COUNT(OptionID) FROM Options", 0) == 0)
      {
        Logger.Log("(DB.Options) Absent. Attempting insertion:");

        string sql = "INSERT INTO Options (GiftCreditNecessaryCharge, GiftCreditAmount, BombPlantFrag, BombDiffuseFrag, KillFrag, FriendlyKillFrag, MaximumTeamNameLength, TeamMinimumMembers, RefundableCancelDuration) VALUES (@1, @2, @3, @4, @5, @6, @7, @8, @9)";
        if (DataBase.ExecuteSQL(sql, mGiftCreditNecessaryCharge, mGiftCreditAmount, mBombPlantFrag, mBombDiffuseFrag, mKillFrag, mFriendlyKillFrag, mMaximumTeamNameLength, mTeamMinimumMembers, mRefundableCancelDuration) == 1)
          Logger.Log("(DB.Options) Done.");
        else
          Logger.Log("(DB.Options) Failed.");
      }
      else
        Logger.Log("(DB.Options) Present.");

      CheckOut();
    }
    #endregion Constructor


    #region DataBase Interactions
    static private void CheckOut()
    {
      try
      {
        DataRow data = DataBase.QueryResult("SELECT * FROM Options").Rows[0];
        mGiftCreditNecessaryCharge = data.Field<double>("GiftCreditNecessaryCharge");
        mGiftCreditAmount = data.Field<double>("GiftCreditAmount");
        mBombPlantFrag = data.Field<int>("BombPlantFrag");
        mBombDiffuseFrag = data.Field<int>("BombDiffuseFrag");
        mKillFrag = data.Field<int>("KillFrag");
        mFriendlyKillFrag = data.Field<int>("FriendlyKillFrag");
        mMaximumTeamNameLength = data.Field<int>("MaximumTeamNameLength");
        mTeamMinimumMembers = data.Field<int>("TeamMinimumMembers");
        mRefundableCancelDuration = data.Field<int>("RefundableCancelDuration");
      }
      catch(Exception ex)
      {
        Logger.LogException(ex, "(DB.Options)");
      }
    }

    static public void Apply()
    {
      string sql = "UPDATE Options SET ";
      sql += "GiftCreditNecessaryCharge=@1, ";
      sql += "GiftCreditAmount=@2, ";
      sql += "BombPlantFrag=@3, ";
      sql += "BombDiffuseFrag=@4, ";
      sql += "KillFrag=@5, ";
      sql += "FriendlyKillFrag=@6, ";
      sql += "MaximumTeamNameLength=@7, ";
      sql += "TeamMinimumMembers=@8, ";
      sql += "RefundableCancelDuration=@9";

      int result = DataBase.ExecuteSQL(sql,
                                       mGiftCreditNecessaryCharge,
                                       mGiftCreditAmount,
                                       mBombPlantFrag,
                                       mBombDiffuseFrag,
                                       mKillFrag,
                                       mFriendlyKillFrag,
                                       mMaximumTeamNameLength,
                                       mTeamMinimumMembers,
                                       mRefundableCancelDuration);

      if (result == 0)
        Logger.Log("(DB.Options) Apply failed.");
      else
        Logger.Log("(DB.Options) Apply done.");
    }
    #endregion DataBase Interactions
  }
}
