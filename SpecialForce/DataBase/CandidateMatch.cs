﻿using System;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class CandidateMatch
  {
    #region Properties
    private int mCandidateMatchID = -1;
    public int ID
    {
      get { return mCandidateMatchID; }
    }
      
    private Competition mCompetition = null;
    public Competition Competition
    {
      get { return mCompetition; }
      set { mCompetition = value; }
    }

    private int mGameID = -1;
    public int GameID
    {
      get { return mGameID; }
    }

    private CodeName mBlueTeam = null;
    public CodeName BlueTeam
    {
      get { return mBlueTeam; }
      set { mBlueTeam = value; }
    }

    private CodeName mGreenTeam = null;
    public CodeName GreenTeam
    {
      get { return mGreenTeam; }
      set { mGreenTeam = value; }
    }

    private CodeName mWinnerCodeName = null;
    public CodeName WinnerCodeName
    {
      get { return mWinnerCodeName; }
      set { mWinnerCodeName = value; }
    }

    private CodeName mLoserCodeName = null;
    public CodeName LoserCodeName
    {
      get { return mLoserCodeName; }
      set { mLoserCodeName = value; }
    }

    private int mStage = -1;
    public int Stage
    {
      get { return mStage; }
      set { mStage = value; }
    }

    private bool mValid = false;
    public bool Valid
    {
      get { return mValid; }
      set { mValid = value; }
    }

    private int mGroup = -1;
    public int Group
    {
      get { return mGroup; }
      set { mGroup = value; }
    }

    public bool IsFinalMatch
    {
      get 
      {
        if (mWinnerCodeName == null)
          return false;

        // if the competition is cup then final
        // match's winner name has been set
        else if (mCompetition.Type == CompetitionType.Cup && mWinnerCodeName.Code == "فینال")
          return true;

        // if the competition is elimination
        // then the last stage has only one match
        // and thats the final match
        else if (mCompetition.Type == CompetitionType.Elimination && mStage == mCompetition.Stages.Count - 1)
          return true;

        return false;
      }
    }

    public bool IsLosersFinal
    {
      get { return mWinnerCodeName != null && mWinnerCodeName.Code == "رده بندی"; }
    }
    #endregion Properties


    #region Events
    public event EventHandler<EventArgs> OnGameChanged;
    #endregion Events


    #region Constructor
    public CandidateMatch(Competition c)
    {
      mCompetition = c;
    }
    #endregion Constructor


    #region Game Related Interactions
    private void FireGameUpdateEvent()
    {
      EventHandler<EventArgs> ev = OnGameChanged;
      if (ev != null)
        ev(this, null);

      mCompetition._FireRefreshEvent();
    }

    public void GameCanceled()
    {
      mGameID = -1;
      Apply();

      FireGameUpdateEvent();
    }

    public bool AssignGame(Game game)
    {
      // Will not remove the game this way.
      if (game == null)
        return false;

      foreach (DB.CandidateMatch cm in mCompetition.CandidateMatches)
      {
        if (cm.GameID == -1)
          continue;

        if (cm.Stage < mStage && game.Start < DB.GameManager.GetGameByID(cm.GameID).Start)
        {
          Session.ShowError("این مسابقه باید پس از مسابقات دور " + (cm.Stage + 1) + "قرار بگیرد.");
          return false;
        }

        if (cm.Stage > mStage && game.Start > DB.GameManager.GetGameByID(cm.GameID).Start)
        {
          Session.ShowError("این مسابقه باید قبل از مسابقات دور " + (cm.Stage + 1) + "قرار بگیرد.");
          return false;
        }
      }

      // set game id
      mGameID = game.ID;
      Apply();

      // Set the game's candidate match
      game.AssignCandidateMatch(this);

      FireGameUpdateEvent();
      return true;
    }
    #endregion Game Related Interactions


    #region Utilities
    public bool ContainsCodeName(CodeName cn)
    {
      if (mBlueTeam != null && mBlueTeam.ID == cn.ID)
        return true;

      if (mGreenTeam != null && mGreenTeam.ID == cn.ID)
        return true;

      return false;
    }
    #endregion Utilities


    #region DataBase Interactions
    static private CandidateMatch ParseData(Competition parent, DataRow data)
    {
      try
      {
        CandidateMatch result = new CandidateMatch(parent);
        result.mCandidateMatchID = Convert.ToInt32(data["CandidateMatchID"]);
        result.mGameID = data.Field<int>("GameID");
        result.mBlueTeam = parent.GetCodeName(data.Field<string>("BlueTeamCodeName"));
        result.mGreenTeam = parent.GetCodeName(data.Field<string>("GreenTeamCodeName"));
        result.mWinnerCodeName = parent.GetCodeName(data.Field<string>("WinnerCodeName"));
        result.mLoserCodeName = parent.GetCodeName(data.Field<string>("LoserCodeName"));
        result.mStage = data.Field<int>("Stage");
        result.mValid = data.Field<bool>("Valid");
        result.mGroup = data.Field<int>("GroupIndex");
        return result;
      }
      catch(Exception ex)
      {
        Logger.LogException(ex, "DB.CandidateMatch", "ParseData");
        return null;
      }
    }

    static public List<CandidateMatch> LoadCompetitionCandidateMatches(Competition parent)
    {
      CandidateMatch match;
      List<CandidateMatch> result = new List<CandidateMatch>();
      string sql = "SELECT * FROM CandidateMatches WHERE CompetitionID=@1 ORDER BY CandidateMatchID";
      foreach(DataRow row in DataBase.QueryResult(sql, parent.ID).Rows)
        if((match = ParseData(parent, row)) != null)
          result.Add(match);
      return result;
    }

    public void Apply()
    {
      string sql;
      string blue = mBlueTeam != null ? mBlueTeam.Code : "";
      string green = mGreenTeam != null ? mGreenTeam.Code : "";
      string winner = mWinnerCodeName != null ? mWinnerCodeName.Code : "";
      string loser = mLoserCodeName != null ? mLoserCodeName.Code : "";

      if (mCandidateMatchID == -1)
      {
        sql = "INSERT INTO CandidateMatches (CompetitionID, GameID, BlueTeamCodeName, ";
        sql += "GreenTeamCodeName, WinnerCodeName, LoserCodeName, Stage, Valid, ";
        sql += "GroupIndex) VALUES (@1, @2, @3, @4, @5, @6, @7, @8, @9)";
        if (DataBase.ExecuteSQL(sql, mCompetition.ID, mGameID, blue, green, winner,
                                loser, mStage, mValid, mGroup) != 1)
          throw new Exception("(DB.CandidateMatch) could not insert candidate match");
        mCandidateMatchID = DataBase.InsertID;
      }
      else
      {
        sql = "UPDATE CandidateMatches SET CompetitionID=@1, GameID=@2, ";
        sql += "BlueTeamCodeName=@3, GreenTeamCodeName=@4, WinnerCodeName=@5, ";
        sql += "LoserCodeName=@6, Stage=@7, Valid=@8, GroupIndex=@9 ";
        sql += "WHERE CandidateMatchID=@10";
        if (DataBase.ExecuteSQL(sql, mCompetition.ID, mGameID, blue, green, winner,
                                loser, mStage, mValid, mGroup, mCandidateMatchID) != 1)
          throw new Exception("(DB.CandidateMatch) could not update match information");
      }
    }

    public override string ToString()
    {
      string result = "";
      Game game = GameManager.GetGameByID(mGameID);
      string blue = game.GetBlueTeamName("");
      string green = game.GetGreenTeamName("");
      if (blue == "" && mBlueTeam != null)
        blue = mBlueTeam.Code;
      if (green == "" && mGreenTeam != null)
        green = mGreenTeam.Code;

      if (mCompetition.Type == CompetitionType.Elimination || mCompetition.Type == CompetitionType.Cup)
      {
        if (!mValid)
          result = "استراحت برای" + "\n" + blue;
        else
          result = blue + "\n" + green;
      }

      return result;
    }
    #endregion DataBase Interactions
  }
}
