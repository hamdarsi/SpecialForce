﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public partial class Game
  {
    #region Properties
    #region Game Execution Information
    private int mGameID = -1;
    public int ID
    {
      get { return mGameID; }
    }

    private CompetitionType mGameType = CompetitionType.Dummy;
    public CompetitionType Type
    {
      get { return mGameType; }
      set { mGameType = value; }
    }

    private GameObject mGameObject = GameObject.NoObject;
    public GameObject Object
    {
      get { return mGameObject; }
      set { mGameObject = value; }
    }

    private PersianDateTime mReservation = null;
    public PersianDateTime Reservation
    {
      get { return mReservation; }
    }

    private PersianDateTime mStart = null;
    public PersianDateTime Start
    {
      get { return new PersianDateTime(mStart); }
      set { mStart = value; }
    }

    private PersianDateTime mFinish = null;
    public PersianDateTime Finish
    {
      get { return new PersianDateTime(mFinish); }
      set { mFinish = value; }
    }

    private double mAdmissionFee = 0.0;
    public double AdmissionFee
    {
      get { return mAdmissionFee; }
      set
      {
        if (mState != GameState.Free)
          throw new Exception("(DB.Game) Can not adjust admission fee when game is not free");

        mGameType = CompetitionType.FreeMatch;
        mAdmissionFee = value;
        Apply();
      }
    }

    private int mSiteID = -1;
    public int SiteID
    {
      get { return mSiteID; }
      set { mSiteID = value; }
    }
    #endregion Game Execution Information


    #region GameState
    private GameState mState = GameState.Free;
    public GameState State
    {
      get { return mState; }
      set { mState = value; }
    }

    public string StateString
    {
      get
      {
        string result = "";
        if (mState == GameState.Free)
          return "آزاد";

        if (mState == GameState.Aborted)
          return "کنسل";

        if (mState == GameState.Reserved)
        {
          result = "رزرو";
          if (mGreenTeamID != -1 && mBlueTeamID != -1)
          {
            result += ": بین ";
            result += GetBlueTeamName();
            result += " و ";
            result += GetGreenTeamName();
          }
          return result;
        }

        // For in progress and finished games:
        int blue, green;
        GetTeamScores(out blue, out green);

        if (mState == GameState.InProgress)
          result = "نتیجه زنده: ";

        if (blue == green)
          return result + blue.ToString() + " - " + green.ToString() + "مساوی";

        result += Math.Max(blue, green).ToString();
        result += " - ";
        result += Math.Min(blue, green).ToString();

        result += " برای ";
        result += blue > green ? GetBlueTeamName("آبی") : GetGreenTeamName("سبز");
        return result;
      }
    }
    #endregion GameState


    #region Teams and Users
    private int mBlueTeamID = -1;
    public int BlueTeamID
    {
      get { return mBlueTeamID; }
    }

    private List<int> mBlueUserIDs = null;
    public List<int> BlueUserIDs
    {
      get 
      {
        if (mBlueUserIDs == null)
          LoadPlayersInternal();

        return mBlueUserIDs; 
      }
    }

    public List<User> BlueUsers
    {
      get { return User.LoadUsers(BlueUserIDs); }
    }

    private int mGreenTeamID = -1;
    public int GreenTeamID
    {
      get { return mGreenTeamID; }
    }

    private List<int> mGreenUserIDs = null;
    public List<int> GreenUserIDs
    {
      get
      {
        if (mGreenUserIDs == null)
          LoadPlayersInternal();

        return mGreenUserIDs;
      }
    }

    public List<User> GreenUsers
    {
      get { return User.LoadUsers(mGreenUserIDs); }
    }

    private List<User> AllUsers
    {
      get
      {
        List<User> result = new List<User>();
        result.AddRange(BlueUsers);
        result.AddRange(GreenUsers);
        return result;
      }
    }

    private List<User> mPresentUsers = new List<User>();
    public List<User> PresentUsers
    {
      get { return mPresentUsers; }
    }
    #endregion Teams and Users


    #region Game Settings: HP, Mag, Ammo, Rest, FriendlyFire, BombTime
    private int mRestTime = -1; // In seconds
    public int RestTime
    {
      get { return mRestTime; }
      set { mRestTime = value; }
    }

    private bool mRestNeeded = false;
    public bool RestNeeded
    {
      get { return mRestNeeded; }
    }

    private int mStartHP = -1;
    public int StartHP
    {
      get { return mStartHP; }
      set { mStartHP = value; }
    }

    private int mStartMagazine = -1;
    public int StartMagazine
    {
      get { return mStartMagazine; }
      set { mStartMagazine = value; }
    }

    private int mAmmoPerMagazine = -1;
    public int AmmoPerMagazine
    {
      get { return mAmmoPerMagazine; }
      set { mAmmoPerMagazine = value; }
    }

    private bool mFriendlyFire = true;
    public bool FriendlyFire
    {
      get { return mFriendlyFire; }
      set { mFriendlyFire = value; }
    }

    private int mBombTime = -1;
    public int BombTime
    {
      get { return mBombTime; }
      set { mBombTime = value; }
    }
    #endregion Game Settings: HP, Mag, Ammo, Rest, FriendlyFire, BombTime


    #region Competition Related Information
    private int mCompetitionID = -1;
    public int CompetitionID
    {
      get { return mCompetitionID; }
    }

    public Competition Competition
    {
      get { return DB.CompetitionManager.GetCompetition(mCompetitionID); }
    }

    public CandidateMatch CandidateMatch
    {
      get { return mCompetitionID == -1 ? null : Competition.GetCandidateMatch(this); }
    }
    #endregion Competition Related Information


    #region Round Related Information
    private int mRoundCount = -1;
    public int RoundCount
    {
      get { return mRoundCount; }
      set { mRoundCount = value; }
    }

    private int mRoundTime = -1;
    public int RoundTime
    {
      get { return mRoundTime; }
      set { mRoundTime = value; }
    }

    private List<Round> mRounds = null;
    public List<Round> Rounds
    {
      get 
      {
        if (mRounds == null)
          LoadRoundsInternal();

        return mRounds.Where(round => round.Eligible).ToList();
      }
    }

    public List<Round> AllRounds
    {
      get
      {
        if (mRounds == null)
          LoadRoundsInternal();

        return mRounds;
      }
    }

    public Round CurrentRound
    {
      get
      {
        if (mState != GameState.InProgress)
          return null;

        List<Round> rounds = AllRounds;
        return rounds.Count != 0 ? rounds[rounds.Count - 1] : null;
      }
    }

    public int PlayedRounds
    {
      get { return AllRounds.Where(round => round.Eligible).Count(); }
    }

    #endregion Round Related Information


    #region Winners and Losers
    private WinnerType mWinner = WinnerType.NotPlayed;
    public WinnerType Winner
    {
      get { return mWinner; }
    }

    public int WinnerTeam
    {
      get
      {
        if (mWinner == WinnerType.Blue)
          return mBlueTeamID;
        else if (mWinner == WinnerType.Green)
          return mGreenTeamID;
        else
          return -1;
      }
    }

    public int LoserTeam
    {
      get
      {
        if (mWinner == WinnerType.Blue)
          return mGreenTeamID;
        else if (mWinner == WinnerType.Green)
          return mBlueTeamID;
        else
          return -1;
      }
    }
    #endregion Winners and Losers


    #region Notes
    private List<DB.ScheduleNote> mNotes = null;
    public List<DB.ScheduleNote> Notes
    {
      get 
      {
        if (mNotes == null)
          mNotes = ScheduleNote.GetGameNotes(mGameID);

        return mNotes; 
      }
    }
    #endregion Notes
    #endregion Properties


    #region Events
    public event EventHandler<EventArgs> OnAttributeUpdate;
    #endregion Events


    #region Constructors
    private Game()
    {
    }

    public Game(PersianDateTime start, PersianDateTime end, int site_id)
    {
      mStart = start;
      mFinish = end;
      mSiteID = site_id;
      Apply();

      GameManager._InsertInCache(this);
    }
    #endregion Constructors


    #region Statistics
    /// <summary>
    /// This method returns frag of the given player in the whole game.
    /// Only eligible rounds are taken into account.
    /// </summary>
    public int GetFrag(User player)
    {
      return
        GetKillCount(player) * DB.Options.KillFrag -
        GetFriendlyKillCount(player) * DB.Options.FriendlyKillFrag +
        GetPlantCount(player) * DB.Options.BombPlantFrag +
        GetDiffuseCount(player) * DB.Options.BombDiffuseFrag;
    }

    /// <summary>
    /// This method returns kill count for the player in the whole game.
    /// Only eligible rounds are taken into account.
    /// </summary>
    public int GetKillCount(User player)
    {
      int count = 0;
      try
      { 
        Rounds.ForEach(round => count += round.GetKillCount(player));
      }
      catch
      { }

      return count;
    }

    /// <summary>
    /// This method returns friendly kill count for the player in the whole game.
    /// Only eligible rounds are taken into account.
    /// </summary>
    public int GetFriendlyKillCount(User player)
    {
      int count = 0;
      try
      {
        Rounds.ForEach(round => count += round.GetFriendlyKillCount(player));
      }
      catch
      { }

      return count;
    }

    /// <summary>
    /// This method returns kill count for the given team in the whole game.
    /// Only eligible rounds are taken into account.
    /// </summary>
    public int GetKillCount(TeamType tt)
    {
      int cnt = 0;
      try
      {
        Rounds.ForEach(round => cnt += round.GetKillCount(tt));
      }
      catch
      { }

      return cnt;
    }

    /// <summary>
    /// This method returns friendly kill count for the given team in the whole game.
    /// Only eligible rounds are taken into account.
    /// </summary>
    public int GetFriendlyKillCount(TeamType tt)
    {
      int cnt = 0;
      try
      {
        Rounds.ForEach(round => cnt += round.GetFriendlyKillCount(tt));
      }
      catch
      { }

      return cnt;
    }

    /// <summary>
    /// This method returns death count for the given player in the whole game.
    /// Only eligible rounds are taken into account.
    /// </summary>
    public int GetDeathCount(User player)
    {
      int cnt = 0;
      try
      {
        Rounds.ForEach(round => cnt += round.GetDeathCount(player));
      }
      catch
      { }

      return cnt;
    }

    /// <summary>
    /// This method returns death count for the given team in the whole game.
    /// Only eligible rounds are taken into account.
    /// </summary>
    public int GetDeathCount(TeamType tt)
    {
      int count = 0;
      try
      {
        Rounds.ForEach(round => count += round.GetDeathCount(tt));
      }
      catch
      { }

      return count;
    }

    /// <summary>
    /// This method returns plant count for the given player in the whole game.
    /// Only eligible rounds are taken into account.
    /// </summary>
    public int GetPlantCount(User player)
    {
      try
      {
        return Rounds.Where(round => (round.BombState == BombState.Exploded && round.Planter.ID == player.ID)).Count();
      }
      catch
      {
        return 0;
      }
    }

    /// <summary>
    /// This method returns plant count for the given team in the whole game.
    /// Only eligible rounds are taken into account.
    /// </summary>
    public int GetPlantCount(TeamType tt)
    {
      try
      {
        return Rounds.Where(round => (round.BombState == BombState.Exploded && round.PlanterTeam == tt)).Count();
      }
      catch
      {
        return 0;
      }
    }

    /// <summary>
    /// This method returns diffuse count for the given player in the whole game.
    /// Only eligible rounds are taken into account.
    /// </summary>
    public int GetDiffuseCount(User player)
    {
      try
      {
        return Rounds.Where(round => (round.BombState == BombState.Diffused && round.Diffuser.ID == player.ID)).Count();
      }
      catch
      {
        return 0;
      }
    }

    /// <summary>
    /// This method returns diffuse count for the given team in the whole game.
    /// Only eligible rounds are taken into account.
    /// </summary>
    public int GetDiffuseCount(TeamType tt)
    {
      try
      {
        return Rounds.Where(round => (round.BombState == BombState.Diffused && round.DiffuserTeam == tt)).Count();
      }
      catch
      {
        return 0;
      }
    }

    /// <summary>
    /// This method returns score of the match based on the played rounds.
    /// Only eligible rounds are taken into account.
    /// </summary>
    public void GetTeamScores(out int blue, out int green)
    {
      blue = green = 0;
      foreach (Round r in Rounds)
        if (r.Winner == WinnerType.Blue)
          ++blue;
        else if (r.Winner == WinnerType.Green)
          ++green;
    }
    #endregion Statistics


    #region Game Notes
    public void AddNote(string note, PersianDateTime due)
    {
      // This also saves the note in DB
      ScheduleNote nt = ScheduleNote.NewNote(mGameID, note, due);

      if (nt != null)
      {
        Notes.Add(nt);
        FireAttributeUpdateEvent();
      }
    }

    public void DeleteNote(int id)
    {
      foreach (ScheduleNote note in Notes)
        if (note.ID == id)
        {
          note._Delete();
          FireAttributeUpdateEvent();
        }
    }

    public bool HasValidNotes()
    {
      if (Session.CurrentStaff.IsPrivileged())
        return Notes.Count != 0;

      foreach (ScheduleNote note in Notes)
        if (note.Deleted)
          continue;
        else if (note.Expired)
          continue;
        else
          return true;
      return false;
    }
    #endregion Game Notes


    #region HashMap Implementations
    public override bool Equals(object obj)
    {
      Game g = obj as Game;
      return g == null ? false : g.mGameID == mGameID;
    }

    public override int GetHashCode()
    {
      return mGameID;
    }
    #endregion HashMap Implementations


    #region Utilities
    private void FireAttributeUpdateEvent()
    {
      EventHandler<EventArgs> ev = OnAttributeUpdate;
      if (ev != null)
        ev(this, new EventArgs());
    }

    public void CopyUserIDsToList(SortedSet<int> ids)
    {
      // for blue team
      foreach (int id in BlueUserIDs)
        if (!ids.Contains(id))
          ids.Add(id);

      // for green team
      foreach (int id in GreenUserIDs)
        if (!ids.Contains(id))
          ids.Add(id);
    }

    public void _Delete()
    {
      if (mState != GameState.Free && mCompetitionID == -1)
        return;

      // Delete notes belonging to the game
      Notes.ForEach(note => note._Delete());
      mNotes = null;

      // Delete rounds belonging to the game
      AllRounds.ForEach(round => round.DeleteRound());
      mRounds = null;

      // Delete associated users
      DataBase.ExecuteSQL("DELETE FROM PlayersInGames WHERE GameID=@1", mGameID);
      mPresentUsers.Clear();
      mBlueUserIDs = null;
      mGreenUserIDs = null;

      // Delete the game entry
      DataBase.ExecuteSQL("DELETE FROM Games WHERE GameID=@1", mGameID);
      mGameID = -1;
    }

    /// <summary>
    /// Returns the blue team's name.
    /// </summary>
    /// <param name="default_value">If no team is registered this value will be returned</param>
    public string GetBlueTeamName(string default_value = "آبی")
    {
      return DB.Team.GetTeamName(mBlueTeamID, default_value);
    }

    /// <summary>
    /// Returns the green team's name.
    /// </summary>
    /// <param name="default_value">If no team is registered this value will be returned</param>
    public string GetGreenTeamName(string default_value = "سبز")
    {
      return DB.Team.GetTeamName(mGreenTeamID, default_value);
    }
    #endregion Utilities


    #region Reservation
    private void AssignPlayers(List<int> blue_users, List<int> green_users)
    {
      // Now that the game has been reserved, update players in the game
      mBlueUserIDs = blue_users;
      mGreenUserIDs = green_users;

      // In case game's reservation has been cancelled
      string sql = "DELETE FROM PlayersInGames WHERE GameID=@1";
      DataBase.ExecuteSQL(sql, mGameID);

      // Save the players in the game to database
      sql = "INSERT INTO PlayersInGames (GameID, UserID, PlayerIsForBlueTeam) VALUES (@1, @2, @3)";
      foreach (int user_id in BlueUserIDs)
        if (DataBase.ExecuteSQL(sql, mGameID, user_id, true) != 1)
          throw new Exception("(DB.Game) could not insert blue team user information on a game");
      foreach (int user_id in GreenUserIDs)
        if (DataBase.ExecuteSQL(sql, mGameID, user_id, false) != 1)
          throw new Exception("(DB.Game) could not insert green team user information on a game");
    }

    public void AssignCandidateMatch(CandidateMatch cm)
    {
      if (mCompetitionID != -1 && mCompetitionID != cm.Competition.ID)
        throw new Exception("(DB.Game) can not reserve a competition game for anotherone");

      if (mState != GameState.Free && mState != GameState.Reserved)
        throw new Exception("(DB.Game) can not reserve a competition game when game in play or finished");

      // Update settings
      mState = GameState.Reserved;
      mCompetitionID = cm.Competition.ID;
      mGameType = cm.Competition.Type;
      mAdmissionFee = cm.Competition.AdmissionFee;
      mGameObject = cm.Competition.Object;
      mRoundCount = cm.Competition.RoundCount;
      mRoundTime = cm.Competition.RoundTime;
      mRestTime = cm.Competition.RestTime;
      mBombTime = cm.Competition.BombTime;
      mStartHP = cm.Competition.StartHP;
      mStartMagazine = cm.Competition.StartMagazine;
      mAmmoPerMagazine = cm.Competition.AmmoPerMagazine;
      mFriendlyFire = cm.Competition.FriendlyFire;
      mBlueTeamID = cm.BlueTeam != null ? cm.BlueTeam.TeamID : -1;
      mGreenTeamID = cm.GreenTeam != null ? cm.GreenTeam.TeamID : -1;
      Apply();

      // Assign players if any
      if (cm.BlueTeam == null || cm.GreenTeam == null)
        return;

      AssignPlayers(DB.Team.ListMemberIDs(cm.BlueTeam.TeamID), DB.Team.ListMemberIDs(cm.GreenTeam.TeamID));
      SetReservation(PersianDateTime.Now);
    }

    private void SetReservation(PersianDateTime dt)
    {
      mReservation = dt;
      string sql = "UPDATE Games SET Reservation=@1 WHERE GameID=@2";
      DataBase.ExecuteSQL(sql, PersianDateTime.ToObject(mReservation), mGameID);
    }

    public void ReserveRegularMatch(List<DB.User> blue_users, List<DB.User> green_users)
    {
      if (mState != GameState.Free)
        throw new Exception("(DB.Game) Game is not free. It can not be reserved");

      if(mCompetitionID != -1)
        throw new Exception("(DB.Game) Game is of a competition. It can not be reserved");

      // deduct game's admission fee from all players
      foreach (DB.User player in blue_users)
        player.ReserveAdmission(this);

      foreach (DB.User player in green_users)
        player.ReserveAdmission(this);

      // Assign players to the game
      AssignPlayers(blue_users.Select(user => user.ID).ToList(), green_users.Select(user => user.ID).ToList());

      // Update game state
      mState = GameState.Reserved;
      Apply();

      SetReservation(PersianDateTime.Now);
    }

    public void CancelRegularMatch(bool refund)
    {
      if (mState != GameState.Reserved)
        throw new Exception("(DB.Game) game is not reserved");

      if (mCompetitionID != -1)
        throw new Exception("(DB.Game) can not cancel a competition match");

      // Remove duducted admission fees from users
      if (mGameType == CompetitionType.FreeMatch && refund)
      {
        DB.User.LoadUsers(BlueUserIDs).ForEach(user => user.RefundReservation(this));
        DB.User.LoadUsers(GreenUserIDs).ForEach(user => user.RefundReservation(this));
      }

      // Clear game state
      mState = GameState.Free;

      // Remove all players from the game
      mBlueUserIDs.Clear();
      mGreenUserIDs.Clear();

      // Also remove from the database
      string sql = "DELETE FROM PlayersInGames WHERE GameID=@1";
      DataBase.ExecuteSQL(sql, mGameID);

      Apply();
      SetReservation(null);
    }
    #endregion Reservation


    #region Match Related Procedures
    /// <summary>
    /// Prepares the game for start.
    /// </summary>
    public void PrepareGame()
    {
      mState = GameState.InProgress;
      Apply();
    }

    public void RestoreKevlarInformation()
    {
      Round r = AllRounds[AllRounds.Count - 1];

      foreach (KeyValuePair<int, int> pair in r.PlayerKevlars)
      {
        User player = DB.User.GetUserByID(pair.Key);

        DeviceID kev_id = Kevlars.KevlarIDFromNumber(pair.Value);
        int kev_index = Kevlars.IndexFromID(kev_id);
        ControlTower.Kevlar kev = ControlTower.Tower.Kevlars[kev_index];

        kev.Player = player;
      }
    }

    /// <summary>
    /// Abort the game. In case current round is running, it will be aborted.
    /// No need to check other rounds. Since they are surely not eligible.
    /// </summary>
    public void GameAborted()
    {
      if(mState != GameState.InProgress)
        throw new Exception("(DB.Game) game is not in progress/reserved");

      // If a round is running, abort it
      Round r = CurrentRound;
      if (r != null)
        if (r.State == RoundState.InProgress || r.State == RoundState.Pending)
          r.RoundAborted();

      // Update the game's state
      mState = GameState.Aborted;
      Apply();
    }

    /// <summary>
    /// This method is called when a game is finished. Will find out the winner.
    /// </summary>
    public void GameFinished()
    {
      if (mState != GameState.InProgress)
        throw new Exception("(DB.Game) game is not in progress");

      // determine the winner
      int blue_score = 0;
      foreach (Round r in Rounds)
        if (r.Winner == WinnerType.Blue)
          ++blue_score;
        else if(r.Winner == WinnerType.Green)
          --blue_score;

      if (blue_score > 0)       // green is winner
        mWinner = WinnerType.Blue;
      else if (blue_score == 0) // tie
        mWinner = WinnerType.Tie;
      else                      // blue is winner
        mWinner = WinnerType.Green;

      // Adjust game end to the last round's ending time.
      mState = GameState.Finished;
      if(mRounds.Count > 0)
        mFinish = mRounds[mRounds.Count - 1].Finish;

      Apply();
    }

    /// <summary>
    /// Used for keeping track of players in the game. Will not 
    /// allow users not from the game to enter arena.
    /// </summary>
    /// <param name="player">Player who wants to enter the arena</param>
    /// <returns>True if enter is accepted, false otherwise</returns>
    public bool PlayerEntered(User player)
    {
      if (player == null)
        return false;

      if (!GreenUserIDs.Contains(player.ID) && !BlueUserIDs.Contains(player.ID))
        return false;

      if (mPresentUsers.Contains(player))
        return false;

      mPresentUsers.Add(player);
      player.DeductAdmission(this);
      return true;
    }

    /// <summary>
    /// Checks whether the current game shall continue running.
    /// </summary>
    /// <returns>True if game is not finished yet, false otherwise</returns>
    public bool CheckGameMustContinue()
    {
      // game is finished if game duration is finished
      if (PersianDateTime.Now >= mFinish)
        return false;

      // game is finished if specified number of rounds are played
      if (PlayedRounds == mRoundCount)
        return false;

      return true;
    }

    /// <summary>
    /// Creates a fresh round for the game. Assigns basic
    /// round information and selected terrorist team.
    /// </summary>
    public void CreateNewRound()
    {
      Round r = CurrentRound;
      if (r != null)
      {
        if(r.State != RoundState.Finished && r.State != RoundState.Aborted)
          throw new Exception("(DB.Game) previous round is still in progess");

        mRestNeeded = r.State == RoundState.Finished;
      }

      // Assign terrorist team
      int count = PlayedRounds;
      bool terrors_are_blue = count < mRoundCount / 2;
      if (mRoundCount % 2 == 1 && count == mRoundCount - 1)
        terrors_are_blue = new Random().NextDouble() < 0.5;

      // Create the round. Use mRounds instead of Rounds
      mRounds.Add(new Round(mGameID, count + 1, terrors_are_blue));
    }
    #endregion Match Related Procedures


    #region DataBase Interactions
    private void LoadPlayersInternal()
    {
      mBlueUserIDs = new List<int>();
      mGreenUserIDs = new List<int>();

      if(mGameID != -1)
      {
        string sql = "SELECT UserID, PlayerIsForBlueTeam FROM PlayersInGames WHERE GameID=@1";
        foreach (DataRow row in DataBase.QueryResult(sql, mGameID).Rows)
          if (row.Field<bool>("PlayerIsForBlueTeam"))
            mBlueUserIDs.Add(row.Field<int>("UserID"));
          else
            mGreenUserIDs.Add(row.Field<int>("UserID"));
      }
    }

    private void LoadRoundsInternal()
    {
      // Load the round
      mRounds = mGameID == -1 ? new List<Round>() : Round.LoadGameRounds(mGameID);

      // Auto clean up on load.
      foreach (Round round in mRounds)
        if (round.State == RoundState.InProgress || round.State == RoundState.Pending)
          round.RoundAborted();
    }

    static public Game LoadFromData(DataRow data)
    {
      Game result = new Game();
      result.mGameID = Convert.ToInt32(data["GameID"]);
      result.mReservation = PersianDateTime.FromObject(data["Reservation"]);
      result.mStart = PersianDateTime.FromObject(data["Start"]);
      result.mFinish = PersianDateTime.FromObject(data["Finish"]);
      result.mState = Enums.ParseGameState(data.Field<string>("State"));
      result.mAdmissionFee = data.Field<double>("AdmissionFee");
      result.mGameType = Enums.ParseCompetitionType(data.Field<string>("GameType"));
      result.mGameObject = Enums.ParseGameObject(data.Field<string>("GameObject"));
      result.mBlueTeamID = data.Field<int>("BlueTeam");
      result.mGreenTeamID = data.Field<int>("GreenTeam");
      result.mRoundCount = data.Field<int>("RoundCount");
      result.mRoundTime = data.Field<int>("RoundTime");
      result.mRestTime = data.Field<int>("RestTime");
      result.mStartHP = data.Field<int>("StartHP");
      result.mStartMagazine = data.Field<int>("StartMagazine");
      result.mAmmoPerMagazine = data.Field<int>("AmmoPerMagazine");
      result.mFriendlyFire = data.Field<bool>("FriendlyFire");
      result.mWinner = (WinnerType)data.Field<int>("Winner");
      result.mBombTime = data.Field<int>("BombTime");
      result.mSiteID = data.Field<int>("SiteID");
      result.mCompetitionID = data.Field<int>("CompetitionID");
      result.FireAttributeUpdateEvent();
      return result;
    }

    /// <summary>
    /// Saves game information into DataBase.
    /// </summary>
    public void Apply()
    {
      string sql;

      if (mGameID == -1)
      {
        sql = "INSERT INTO Games (Start, Finish, SiteID, State, AdmissionFee, GameType, GameObject, BlueTeam, GreenTeam, RoundCount, RestTime, RoundTime, StartHP, StartMagazine, AmmoPerMagazine, FriendlyFire, BombTime, CompetitionID) VALUES ";
        sql += "(@1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15, @16, @17, @18)";

        if (mSiteID == -1)
          throw new Exception("(DB.Game) SiteID not set");

        if (DataBase.ExecuteSQL(sql, mStart.DBFormat, mFinish.DBFormat, mSiteID, Enums.ToString(mState), mAdmissionFee, Enums.ToString(mGameType), Enums.ToString(mGameObject), mBlueTeamID, mGreenTeamID, mRoundCount, mRoundTime, mRestTime, mStartHP, mStartMagazine, mAmmoPerMagazine, mFriendlyFire, mBombTime, mCompetitionID) != 1)
          throw new Exception("(DB.Game) probable fault when inserting new game");

        mGameID = DataBase.InsertID;
      }
      else
      {
        sql = "UPDATE Games SET Start=@1, Finish=@2, State=@3, AdmissionFee=@4, GameType=@5, GameObject=@6, BlueTeam=@7, GreenTeam=@8, RoundCount=@9, RoundTime=@10, RestTime=@11, StartHP=@12, StartMagazine=@13, AmmoPerMagazine=@14, FriendlyFire=@15, Winner=@16, BombTime=@17, CompetitionID=@18 WHERE GameID=@19";
        if (DataBase.ExecuteSQL(sql, mStart.DBFormat, mFinish.DBFormat, Enums.ToString(mState), mAdmissionFee, Enums.ToString(mGameType), Enums.ToString(mGameObject), mBlueTeamID, mGreenTeamID, mRoundCount, mRoundTime, mRestTime, mStartHP, mStartMagazine, mAmmoPerMagazine, mFriendlyFire, (int)mWinner, mBombTime, mCompetitionID, mGameID) != 1)
          throw new Exception("(DB.Game) probable fault when updating a row");
      }

      FireAttributeUpdateEvent();
    }
    #endregion DataBase Interactions
  }
}
