﻿using System;
using System.Data;
using System.Drawing;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class User
  {
    #region Properties
    private int mUserID = -1;
    public int ID
    {
      get { return mUserID; }
    }

    private string mFirstName = "";
    public string FirstName
    {
      get { return mFirstName; }
      set { mFirstName = value; }
    }

    private string mLastName = "";
    public string LastName
    {
      get { return mLastName; }
      set { mLastName = value; }
    }

    public string FullName
    {
      get { return mFirstName + " " + mLastName; }
    }

    private bool mGender = true;
    public bool Gender
    {
      get { return mGender; }
      set { mGender = value; }
    }

    private Post mPost = Post.User;
    public Post Post
    {
      get { return mPost; }
      set { mPost = value; }
    }

    private string mAddress = "";
    public string Address
    {
      get { return mAddress; }
      set { mAddress = value; }
    }

    private string mMobilePrefix = "";
    public string MobilePrefix
    {
      get { return mMobilePrefix; }
      set { mMobilePrefix = value; }
    }

    private string mMobileNumber = "";
    public string MobileNumber
    {
      get { return mMobileNumber; }
      set { mMobileNumber = value; }
    }

    private string mTelephone = "";
    public string Telephone
    {
      get { return mTelephone; }
      set { mTelephone = value; }
    }

    private PersianDateTime mBirthDate = null;
    public PersianDateTime BirthDate
    {
      get { return mBirthDate == null ? null : new PersianDateTime(mBirthDate); }
      set { mBirthDate = value; }
    }

    private byte[] mPhotoStream = null;
    private Image mPhoto = null;
    public Image Photo
    {
      get 
      {
        if (mPhotoStream == null)
          return null;
        
        if (mPhoto == null)
          mPhoto = DataBase.ImageFromBlob(mPhotoStream);

        return mPhoto.Clone() as Image;
      }
    }

    private byte[] mAuthentication = null;
    public byte[] Authentication
    {
      get { return mAuthentication.Clone() as byte[]; }
    }

    private byte[] mFingerPrintImageStream = null;
    private Image mFingerPrintImage = null;
    public Image FingerPrintImage
    {
      get
      {
        if (mFingerPrintImageStream == null)
          return null;

        if (mFingerPrintImage == null)
          mFingerPrintImage = DataBase.ImageFromBlob(mFingerPrintImageStream);

        return mFingerPrintImage.Clone() as Image;
      }
    }

    private int mTeamID = -1;
    public int TeamID
    {
      get { return mTeamID; }
    }

    private string mUserName = "";
    public string UserName
    {
      get { return mUserName; }
      set { mUserName = value; }
    }

    private string mPassword = "";
    public string Password
    {
      get { return mPassword; }
      set { mPassword = value; }
    }

    private string mEmail = "";
    public string Email
    {
      get { return mEmail; }
      set { mEmail = value; }
    }

    private PersianDateTime mRegistrationDate = null;
    public PersianDateTime RegistrationDate
    {
      get { return mRegistrationDate == null ? null : new PersianDateTime(mRegistrationDate); }
    }

    private int mRegistrationSiteID = -1;
    public int RegistrationSiteID
    {
      get { return mRegistrationSiteID; }
      set { mRegistrationSiteID = value; }
    }

    private PersianDateTime mFireDate = null;
    public PersianDateTime FireDate
    {
      get { return mFireDate == null ? null : new PersianDateTime(mFireDate); }
    }

    private string mFireReason = "";
    public string FireReason
    {
      get { return mFireReason; }
    }

    public bool Fired
    {
      get { return mFireDate != null; }
    }

    private double mChargedCredit = 0;
    public double ChargedCredit
    {
      get { return mChargedCredit; }
    }

    private double mRefundedCredit = 0;
    public double RefundedCredit
    {
      get { return mRefundedCredit; }
    }

    private double mReservedCredit = 0;
    public double ReservedCredit
    {
      get { return mReservedCredit; }
    }

    private double mUsedCredit = 0;
    public double UsedCredit
    {
      get { return mUsedCredit; }
    }

    private double mLastFreeCreditFlag = 0;

    private double mGiftedCredit = 0;
    public double GiftedCredit
    {
      get { return mGiftedCredit; }
    }

    public double Credit
    {
      get 
      {
        return 
          mChargedCredit + 
          mGiftedCredit - 
          mRefundedCredit - 
          mReservedCredit - 
          mUsedCredit; 
      }
    }

    public ControlTower.Kevlar AssignedKevlar
    {
      get { return ControlTower.Tower.GetActiveKevlarForUserID(mUserID); }
    }

    static public DB.User New
    {
      get { return new User(); }
    }
    #endregion Properties


    #region Constructor
    private User()
    {
    }
    #endregion Constructor


    #region User Accounting
    /// <summary>
    /// Charges a given amount for the user
    /// </summary>
    /// <param name="credit">Amount to add to user credit</param>
    public void Charge(double credit)
    {
      // No need to do anything!
      if (credit <= 0)
        return;

      // Insert the transaction
      ApplyTransaction(credit, InvoiceType.UserCreditCharge);

      // Register current credit values
      mChargedCredit += credit;
      string sql = "UPDATE Users SET ChargedCredit=@1 WHERE UserID=@2";
      if (DataBase.ExecuteSQL(sql, mChargedCredit, mUserID) != 1)
        throw new Exception("(DB.User) probable fault when charging user credit");
    }

    /// <summary>
    /// Refunds a given amount from user credit.
    /// </summary>
    /// <param name="credit">Credit to deduct</param>
    public bool Refund(double credit)
    {
      try
      {
        // Record the refund information
        ApplyTransaction(credit, InvoiceType.UserCreditRefund);

        // Update user's refunded credit on Users table
        mRefundedCredit += credit;
        string sql = "UPDATE Users SET RefundedCredit=@1 WHERE UserID=@2";
        if (DataBase.ExecuteSQL(sql, mRefundedCredit, mUserID) != 1)
          throw new Exception("(DB.User) could not update user credit after refund");

        return true;
      }
      catch (Exception e)
      {
        Logger.LogException(e, "DB.User", "Refund");
        Session.ShowError("مشکلی پیش آمد و هزینه عودت داده نشد.");
        return false;
      }
    }

    public void ReserveAdmission(Game game)
    {
      try
      {
        // Record the admission fee
        if (GetCurrentSpecialTreat().IsValidWhiteList())
          return;

        // Update user's deducted credit on Users table
        mReservedCredit += game.AdmissionFee;
        string sql = "UPDATE Users SET ReservedCredit=@1 WHERE UserID=@2";
        if (DataBase.ExecuteSQL(sql, mReservedCredit, mUserID) != 1)
          throw new Exception("(DB.User) Could not reserve admission");

        return;
      }
      catch (Exception e)
      {
        Logger.LogException(e, "DB.User", "DeductAdmission");
        Session.ShowError("مشکلی پیش آمد و ورودیه ثبت نشد.");
        return;
      }
    }

    public void RefundReservation(Game game)
    {
      try
      {
        if (GetCurrentSpecialTreat().IsValidWhiteList())
          return;

        mReservedCredit -= game.AdmissionFee;
        string sql = "UPDATE Users SET ReservedCredit=@1 WHERE UserID=@2";
        if (DataBase.ExecuteSQL(sql, mUsedCredit, mUserID) != 1)
          throw new Exception("(DB.User) could not update user credit after deduction");
      }
      catch (Exception e)
      {
        Logger.LogException(e, "DB.User", "RefundAdmission");
        Session.ShowError("مشکلی پیش آمد و ورودیه عودت داده نشد.");
      }
    }

    public void DeductAdmission(Game game)
    {
      if (GetCurrentSpecialTreat().IsValidWhiteList() || game.Type != CompetitionType.FreeMatch)
        return;

      double credit = game.AdmissionFee;

      // Now add gifted credit if set
      if (DB.Options.GiftCreditAmount > 0 && DB.Options.GiftCreditNecessaryCharge > 0)
      {
        double needed = Options.GiftCreditNecessaryCharge;
        double gift = Options.GiftCreditAmount;
        while (mUsedCredit + credit - mLastFreeCreditFlag >= needed)
        {
          mLastFreeCreditFlag += needed;
          mGiftedCredit += gift;
          // Insert the transaction
          ApplyTransaction(credit, InvoiceType.UserGiftCharge);
        }
      }
      else
        mLastFreeCreditFlag = mChargedCredit + credit;

      string sql = "UPDATE Users SET LastFreeCreditFlag=@1, GiftedCredit=@2 WHERE UserID=@3";
      if (DataBase.ExecuteSQL(sql, mLastFreeCreditFlag, mGiftedCredit, mUserID) != 1)
        throw new Exception("(DB.User) Could not reserve admission");
    }

    /// <summary>
    /// Performs a query on accounting to get all user invoices. Returns what found.
    /// </summary>
    /// <returns>List of all registered user invoices.</returns>
    public List<Invoice> GetRecentInvoiceHistory()
    {
      List<Invoice> result = new List<Invoice>();

      string time = Profiler.TimeString(() =>
      {
        string sql = "SELECT * FROM Accounting WHERE UserID=@1 ORDER BY InvoiceID DESC LIMIT 10";
        foreach (DataRow row in DataBase.QueryResult(sql, mUserID).Rows)
          result.Add(Invoice.LoadFromData(row));
      });
      Logger.Log("(DB.User) Generated recent user invoice report for user #" + mUserID.ToString() + " in " + time);

      return result;
    }
    #endregion User Accounting


    #region Utilities
    /// <summary>
    /// Sets the user's team and updates database
    /// </summary>
    /// <param name="team_id">Team ID</param>
    /// <param name="user_id">The user ID to set its team</param>
    static public void SetCurrentTeam(int user_id, int team_id)
    {
      string sql = "UPDATE Users SET TeamID=@1 WHERE UserID=@2";
      DataBase.ExecuteSQL(sql, team_id, user_id);
    }

    public bool Fire(string reason)
    {
      mFireDate = PersianDateTime.Now;
      mFireReason = reason;
      string sql = "UPDATE Users SET FireDate=@1, FireReason=@2 WHERE UserID=@3";
      return DataBase.ExecuteSQL(sql, PersianDateTime.ToObject(mFireDate), mFireReason, mUserID) == 1;
    }

    public bool SetPhoto(Image photo)
    {
      byte[] temp = DataBase.ImageToBlob(photo);
      int size = temp.Length * sizeof(byte);
      if (size > 200 * 1024)
        return false;

      mPhoto = photo;
      mPhotoStream = temp;
      return true;
    }

    public bool SetFingerPrint(byte[] data, Image img)
    {
      int size = DataBase.ImageToBlob(img).Length * sizeof(byte);
      if (size > 200 * 1024)
        return false;

      mFingerPrintImageStream = DataBase.ImageToBlob(img);
      mFingerPrintImage = img;
      mAuthentication = data;
      return true;
    }

    /// <summary>
    /// Finds out the user's team name.
    /// </summary>
    /// <param name="default_value">If the user is in no team, this value will be returned</param>
    public string GetTeamName(string default_value)
    {
      string name = DB.Team.GetTeamName(mTeamID);
      return name.Length > 0 ? name : default_value;
    }

    public bool IsPrivileged()
    {
      return mPost.IsPrivileged();
    }

    public bool IsGod()
    {
      return mPost.IsGod();
    }

    public bool IsAdmin()
    {
      return mPost.IsAdmin();
    }

    public bool IsOperator()
    {
      return mPost.IsOperator();
    }

    public bool IsLowlyUser()
    {
      return mPost.IsUser();
    }

    public override bool Equals(object obj)
    {
      if(obj == null)
        return false;

      User other = obj as DB.User;
      if (other != null)
        return other.mUserID == mUserID;

      return false;
    }

    public override string ToString()
    {
      return FullName;
    }
    #endregion Utilities


    #region Special Treats (BlackList, ...)
    /// <summary>
    /// This method queries the database against all special treats 
    /// and returns the most recent one for this user.
    /// </summary>
    /// <returns>User's most recent treat. In case no treat is assigned to 
    /// the user null won't be returned; an empty treat will be returned</returns>
    public UserSpecialTreat GetCurrentSpecialTreat()
    {
      DB.UserSpecialTreat treat = UserSpecialTreat.GetLastTreatForUserInThisSite(mUserID);
      return treat != null ? treat : DB.UserSpecialTreat.EmptyTreat;
    }

    /// <summary>
    /// This method queries all of user's special treats.
    /// </summary>
    /// <returns>All registered user special treats</returns>
    public List<UserSpecialTreat> GetAllSpecialTreats()
    {
      return UserSpecialTreat.GetAllTreatsForUser(mUserID);
    }

    /// <summary>
    /// Creates a new treat with the given data and stores it in the user instance.
    /// Also implicitely stores in UserManager.
    /// </summary>
    /// <param name="treat">Treat type</param>
    /// <param name="due">Treat expire date</param>
    /// <param name="reason">String message for the treat</param>
    public void AssignSpecialTreat(TreatType treat, PersianDateTime due, string reason)
    {
      UserSpecialTreat.Register(due, mUserID, treat, reason, Session.ActiveSiteID);
    }

    public void CancelCurrentSpecialTreat()
    {
      GetCurrentSpecialTreat().Cancel();
    }
    #endregion Special Treats (BlackList, ...)


    #region User encapsulation
    /// <summary>
    /// Returns current live nr. of registered administrators on the database.
    /// Does not require user cache.
    /// </summary>
    static public int GetNrOfGods()
    {
      int result = 0;
      string time = Profiler.TimeString(() =>
      {
        try
        {
          string sql = "SELECT Count(UserID) FROM Users WHERE Post=@1 AND FireDate IS NULL";
          result = DataBase.QueryValue(sql, 0, Enums.ToString(Post.God));
        }
        catch (Exception ex)
        {
          Logger.LogException(ex, "DB.UserManager", "NrOfAdministrators");
        }
      });

      Logger.Log("(DB.User) Found " + result.ToString() + " admins in " + time);
      return result;
    }

    /// <summary>
    /// This method searches the users for one with the given information
    /// </summary>
    /// <param name="username">User's username</param>
    /// <param name="password">User's matching password</param>
    /// <returns>UserID if a user with the given ID was found, -1 otherwise</returns>
    static public int FindUserWithCredentials(string username, string password)
    {
      return DataBase.QueryValue("SELECT UserID FROM Users WHERE Username=@1 AND Password=@2", -1, username, password);
    }

    /// <summary>
    /// This method gets the given user's full name
    /// </summary>
    /// <param name="id">User ID to get full name</param>
    /// <returns>Given user's full name</returns>
    static public string GetUserFullName(int id)
    {
      string sql = "SELECT FirstName, LastName FROM Users WHERE UserID=@1";
      DataTable query = DataBase.QueryResult(sql, id);
      if (query.Rows.Count == 0)
        return "?";

      return query.Rows[0].Field<string>(0) + " " + query.Rows[0].Field<string>(1);
    }

    /// <summary>
    /// Loads the given user information
    /// </summary>
    /// <param name="id">User ID to load</param>
    /// <returns>Load user instance</returns>
    static public User GetUserByID(int id)
    {
      DataTable query = DataBase.QueryResult("SELECT * FROM Users WHERE UserID=@1", id);
      if (query.Rows.Count != 1)
      {
        Logger.Log("(DB.UserManager) Given user does not exist or has a data problem: " + id.ToString());
        return null;
      }

      User result = new User();
      return result.LoadFromData(query.Rows[0]) ? result : null;
    }

    /// <summary>
    /// This method searches for a user having the given username.
    /// </summary>
    /// <param name="username">Username to search for in the list of users</param>
    /// <returns>A user having the given username. If no user was found, null will be returned.</returns>
    static public User GetUserByUserName(string username)
    {
      string sql = "SELECT * From Users WHERE UserName=@1";
      List<DB.User> users = LoadFromQuery(DataBase.QueryResult(sql, username));

      if(users.Count > 1)
        Logger.Log("(DB.User) GetUserByUserName: duplicate users with username '" + username + "'");
      return users.Count == 1 ? users[0] : null;
    }

    /// <summary>
    /// Loads list of users from the given query data. If something
    /// goes wrong for a user, only that one will not be present in the list.
    /// </summary>
    /// <param name="query">Query data containing user information</param>
    /// <returns>List of loaded users</returns>
    static public List<User> LoadFromQuery(DataTable query)
    {
      List<User> result = new List<User>();

      foreach (DataRow data in query.Rows)
      {
        User temp = new User();
        if (temp.LoadFromData(data))
          result.Add(temp);
      }

      return result;
    }

    /// <summary>
    /// Loads the given users information
    /// </summary>
    /// <param name="ids">User IDs to load</param>
    /// <returns>Load user instances</returns>
    static public List<User> LoadUsers(List<int> ids)
    {
      if(ids.Count == 0)
        return new List<User>();

      string sql = "SELECT * FROM Users WHERE UserID IN (" + string.Join(",", ids) + ")";
      DataTable query = DataBase.QueryResult(sql);
      if (query.Rows.Count != ids.Count)
      {
        Logger.Log("(DB.UserManager) Given users are not completely valid");
        return new List<User>();
      }
      else
        return LoadFromQuery(query);
    }

    /// <summary>
    /// Loads limited number of users as given.
    /// </summary>
    /// <param name="limit">Maximum nunber of user to load</param>
    /// <param name="site_id">Specifies which site's users shall be returned. -1 means all sites.</param>
    /// <param name="search_for">If desired limits results to users containing this value in their names.</param>
    /// <returns>List of loaded users</returns>
    static public List<User> LoadSomeUsers(int limit, int site_id, string search_for = "")
    {
      List<User> result = null;
      string time = Profiler.TimeString(() =>
      {
        try
        {
          string name_query = String.Format("(CONCAT(FirstName, ' ' , LastName) LIKE '%{0}%' OR CONCAT(MobilePrefix, MobileNumber) LIKE '%{1}%')", search_for, search_for);
          string postsql = "ORDER BY RegistrationDate DESC";
          string sql1 = "SELECT * FROM Users " + postsql + " LIMIT @1";
          string sql2 = "SELECT * FROM Users WHERE RegistrationSite=@1 " + postsql + " LIMIT @2";
          string sql3 = "SELECT * FROM Users WHERE " + name_query + " " + postsql + " LIMIT @1";
          string sql4 = "SELECT * FROM Users WHERE RegistrationSite=@1 AND " + name_query + " " + postsql + " LIMIT @2";

          if(site_id == -1)
          {
            if(search_for.Length == 0)
              result = LoadFromQuery(DataBase.QueryResult(sql1, limit));
            else
              result = LoadFromQuery(DataBase.QueryResult(sql3, limit));
          }
          else
          {
            if (search_for.Length == 0)
              result = LoadFromQuery(DataBase.QueryResult(sql2, site_id, limit));
            else
              result = LoadFromQuery(DataBase.QueryResult(sql4, site_id, limit));
          }
        }
        catch (Exception ex)
        {
          Logger.LogException(ex, "DB.UserManager", "LoadSomeUsers");
          result = new List<User>();
        }
      });

      Logger.Log("(DB.User) Loaded some users. " + limit.ToString() + " users in " + time);
      return result;
    }

    /// <summary>
    /// Loads all users in the database
    /// </summary>
    /// <returns>Loaded users</returns>
    static public List<User> LoadAllUsers()
    {
      List<User> result = null;
      string time = Profiler.TimeString(() =>
      {
        try
        {
          string sql = "SELECT * FROM Users";
          result = LoadFromQuery(DataBase.QueryResult(sql));
        }
        catch (Exception ex)
        {
          Logger.LogException(ex, "DB.UserManager", "LoadAllUsers");
          result = new List<User>();
        }
      });

      Logger.Log("(DB.User) Loaded " + result.Count.ToString() + " users in " + time);
      return result;
    }

    static public DataTable LoadAllFingerPrints()
    {
      return DataBase.QueryResult("SELECT FingerPrintData, UserID FROM Users");
    }
    #endregion User encapsulation


    #region DataBase Interactions
    /// <summary>
    /// Reloads user data
    /// </summary>
    public void ReloadData()
    {
      if(!LoadFromData(DataBase.QueryResult("SELECT * FROM Users WHERE UserID=@1", mUserID).Rows[0]))
        throw new Exception("(DB.User) Could not reloaded user data with user id: " + mUserID.ToString());
    }

    /// <summary>
    /// Tries to load user information from the given query data
    /// </summary>
    /// <param name="data">Data containing user information</param>
    /// <returns>True on successful load, false otherwise</returns>
    public bool LoadFromData(DataRow data)
    {
      try
      {
        mUserID = Convert.ToInt32(data["UserID"]);
        mFirstName = data.Field<string>("FirstName");
        mLastName = data.Field<string>("LastName");
        mGender = data.Field<bool>("Gender");
        mPost = Enums.ParsePost(data.Field<string>("Post"));
        mAddress = data.Field<string>("Address");
        mMobilePrefix = data.Field<string>("MobilePrefix");
        mMobileNumber = data.Field<string>("MobileNumber");
        mTelephone = data.Field<string>("Telephone");
        mBirthDate = PersianDateTime.FromObject(data["BirthDate"]);
        mPhotoStream = (byte[])data["Photo"];
        mAuthentication = (byte[])data["FingerPrintData"];
        mFingerPrintImageStream = (byte[])data["FingerPrintImage"];
        mTeamID = data.Field<int>("TeamID");
        mUserName = data.Field<string>("UserName");
        mPassword = data.Field<string>("Password");
        mEmail = data.Field<string>("Email");
        mRegistrationDate = PersianDateTime.FromObject(data["RegistrationDate"]);
        mRegistrationSiteID = data.Field<int>("RegistrationSite");
        mFireDate = PersianDateTime.FromObject(data["FireDate"]);
        mFireReason = data.Field<string>("FireReason");
        mChargedCredit = data.Field<double>("ChargedCredit");
        mRefundedCredit = data.Field<double>("RefundedCredit");
        mReservedCredit = data.Field<double>("ReservedCredit");
        mUsedCredit = data.Field<double>("UsedCredit");
        mLastFreeCreditFlag = data.Field<double>("LastFreeCreditFlag");
        mGiftedCredit = data.Field<double>("GiftedCredit");

        return true;
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "DB.User", "LoadFromData");
        return false;
      }
    }

    private void ApplyTransaction(double credit, InvoiceType type, int game_id = -1)
    {
      Invoice inv = new Invoice();
      inv.Amount = credit;
      inv.OperatorID = Session.CurrentStaff.ID;
      inv.SiteID = Session.ActiveSiteID;
      inv.Type = type;
      inv.UserID = mUserID;
      inv.GameID = game_id;
      inv.Apply();
    }

    public void Apply()
    {
      string sql;

      if (mUserID == -1) // should insert the record
      {
        mRegistrationDate = PersianDateTime.Now;
        mRegistrationSiteID = Session.ActiveSiteID;
        if(mRegistrationSiteID == -1)
          mRegistrationSiteID = Properties.Settings.Default.SiteID;
        if (mRegistrationSiteID == -1)
          throw new Exception("(DB.User) site unknown");

        sql = "INSERT INTO Users (RegistrationDate, RegistrationSite) VALUES (@1, @2)";
        if(DataBase.ExecuteSQL(sql, mRegistrationDate.DBFormat, mRegistrationSiteID) != 1)
          throw new Exception("(DB.User) Error inserting user information");

        mUserID = DataBase.InsertID;
      }

      sql = "UPDATE Users SET FirstName=@1, LastName=@2, Gender=@3, Post=@4, Address=@5, MobilePrefix=@6, MobileNumber=@7, Telephone=@8, BirthDate=@9, UserName=@10, Password=@11, Email=@12 WHERE UserID=@13";
      if(DataBase.ExecuteSQL(sql, mFirstName, mLastName, mGender, Enums.ToString(mPost), mAddress, mMobilePrefix, mMobileNumber, mTelephone, mBirthDate.DBFormat, mUserName, mPassword, mEmail, mUserID) != 1)
        throw new Exception("(DB.User) error updating user: " + ToString());

      sql = "UPDATE Users SET Photo=@1, FingerPrintImage=@2, FingerPrintData=@3 WHERE UserID=@4";
      if(DataBase.ExecuteSQL(sql, mPhotoStream, mFingerPrintImageStream, mAuthentication, mUserID) != 1)
        throw new Exception("(DB.User) could not update user authentication/photo for " + ToString());
    }
    #endregion DataBase Interactions
  }
}
