﻿using System;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class CodeName
  {
    #region Properties
    private int mCompetitionCodeNameID = -1;
    public int ID
    {
      get { return mCompetitionCodeNameID; }
    }

    private Competition mCompetition = null;
    public Competition Competition
    {
      get { return mCompetition; }
      set { mCompetition = value; }
    }

    private string mCode = "";
    public string Code
    {
      get { return mCode; }
    }

    private int mTeamID = -1;
    public int TeamID
    {
      get { return mTeamID; }
      set { mTeamID = value; }
    }

    public string TeamName
    {
      get { return mTeamID == -1 ? mCode : DB.Team.GetTeamName(mTeamID); }
    }

    private int mStage = -1;
    public int Stage
    {
      get { return mStage; }
      set { mStage = value; }
    }
    #endregion Properties


    #region Constructor
    public CodeName(Competition c)
    {
      mCompetition = c;
    }
    #endregion Constructor


    #region CodeName Utilities
    public void SetCodeName(int stage, int index, int group = 0)
    {
      mStage = stage;
      mCode = CreateCodeName(stage, index, group);
    }

    public void SetCodeNameForGroupLeaders(int index, bool first_team)
    {
      mStage = 1;
      mCode = (first_team ? "اول" : "دوم") + " A" + (index + 1).ToString();
    }

    public void SetCodeNameForFinalStage(bool final)
    {
      mCode = final ? "فینال" : "رده بندی";
    }

    static public string CreateGroupName(int group)
    {
      return " گروه" + "A" + (group + 1).ToString();
    }

    static public string CreateCodeName(int stage, int index, int group = 0)
    {
      string stage_str = ((char)(stage + 'A')).ToString();
      string group_str = group == 0 ? "" : (group + 1).ToString();
      string index_str = "_" + (index + 1).ToString();

      return stage_str + group_str + index_str;
    }
    #endregion CodeName Utilities


    #region DataBase Interactions
    /// <summary>
    /// This method loads all code names for the given competition
    /// </summary>
    /// <param name="comp">Competition instance</param>
    static public List<CodeName> LoadCompetitionsCodeNames(Competition comp)
    {
      List<CodeName> result = new List<CodeName>();

      try
      {
        string sql = "SELECT * FROM CompetitionCodeNames WHERE CompetitionID=@1";
        foreach (DataRow row in DataBase.QueryResult(sql, comp.ID).Rows)
          result.Add(LoadFromData(comp, row));
      }
      catch(Exception ex)
      {
        Logger.LogException(ex, "CodeName", "LoadCompetitionCodeNames");
      }

      return result;
    }

    static private CodeName LoadFromData(Competition comp, DataRow data)
    {
      try
      {
        CodeName result = new CodeName(comp);
        result.mCompetitionCodeNameID = Convert.ToInt32(data["CompetitionCodeNameID"]);
        result.mCode = data.Field<string>("CodeName");
        result.mTeamID = data.Field<int>("TeamID");
        result.mStage = data.Field<int>("Stage");
        return result;
      }
      catch(Exception ex)
      {
        Logger.LogException(ex, "CodeName", "LoadFromData");
        return null;
      }
    }

    public void Apply()
    {
      string sql;

      if (mCompetitionCodeNameID == -1)
      {
        sql = "INSERT INTO CompetitionCodeNames (CompetitionID, CodeName, TeamID, Stage) ";
        sql += "VALUES (@1, @2, @3, @4)";
        if (DataBase.ExecuteSQL(sql, mCompetition.ID, mCode, mTeamID, mStage) != 1)
          throw new Exception("(DB.CodeName) could not insert code name");

        mCompetitionCodeNameID = DataBase.InsertID;
      }
      else
      {
        sql = "UPDATE CompetitionCodeNames SET TeamID=@1 WHERE CompetitionCodeNameID=@2";
        if (DataBase.ExecuteSQL(sql, mTeamID, mCompetitionCodeNameID) != 1)
          throw new Exception("(DB.CodeName) could not update code name");
      }
    }
    #endregion DataBase Interactions
  }
}
