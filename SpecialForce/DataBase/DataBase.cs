﻿using System;
using System.IO;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

using MySql.Data.MySqlClient;

namespace SpecialForce
{
  public class DataBase
  {
    #region Internal Types and Data
    public enum OpenResult
    {
      HostUnreachable,
      AuthenticationFailure,
      NoAdminFound,
      NoPassword,
      CreationNeeded,
      ExcessiveGames,
      Succeeded
    }

    static private MySqlConnection mDataBase = null;
    #endregion Internal Types and Data


    #region Properties
    static private long mInsertID = -1;
    static public int InsertID
    {
      get { return (int)mInsertID; }
    }

    private static string mErrorString = "";
    static public string ErrorString
    {
      get { return mErrorString; }
    }

    static public bool IsOpen
    {
      get { return mDataBase != null; }
    }

    static public string Name
    {
      get { return "sp_specialforce"; }
    }

    static private int ExcessiveGameCount
    {
      get { return 7000; }
    }
    #endregion Properties


    #region Openning and Closing
    /// <summary>
    /// This method opens a connection to the appliction database
    /// In case a problem occured during opening process, database
    /// error string is set to the problem message
    /// </summary>
    /// <returns>True if a connection to database could be established, false otherwise</returns>
    public static OpenResult Open()
    {
      if (mDataBase != null)
      {
        mErrorString = "(DataBase) DB already openned";
        return OpenResult.Succeeded;
      }

      try
      {
        // Connection settings
        string host = Properties.Settings.Default.DataBaseIP;
        string password = Properties.Settings.Default.DataBasePassword;

        if (password == "")
          return OpenResult.NoPassword;

        string sql = "user=sp_spf;host=" + host;
        sql += ";password=" + password;
        sql += ";database=" + Name;
        sql += ";Charset=utf8;";

        // Database initialization
        mDataBase = new MySqlConnection(sql);
        mDataBase.Open();

        // Check if there is any administrator user in the database
        if(DB.User.GetNrOfGods() == 0)
          return OpenResult.NoAdminFound;

        // Now check for excessive games ;)
        sql = "SELECT GameID FROM Games ORDER BY GameID DESC LIMIT 1";
        if(QueryValue(sql, 0) >= ExcessiveGameCount)
          return OpenResult.ExcessiveGames;

        return OpenResult.Succeeded;
      }
      catch (Exception e)
      {
        OpenResult result;
        mErrorString = e.Message;
        Logger.Log("(DataBase) Open returned error:");
        Logger.Log("(DataBase) " + e.Message);
        if (mErrorString.ToLower().EndsWith("does not support ssl connections."))
          result = OpenResult.HostUnreachable;
        else if (mErrorString.ToLower().Contains("unable to connect"))
          result = OpenResult.HostUnreachable;
        else if (mErrorString.ToLower().Contains("unknown database"))
          result = OpenResult.CreationNeeded;
        else
          result = OpenResult.AuthenticationFailure;

        mDataBase = null;
        return result;
      }
    }

    /// <summary>
    /// This method closes an already openned connection to database
    /// </summary>
    public static void Close()
    {
      if (mDataBase != null)
      {
        mDataBase.Close();
        mDataBase = null;
      }
    }
    #endregion Openning and Closing


    #region Internal Utilities
    /// <summary>
    /// This method is used to retreive parameter names in a given sql command/query
    /// For Example: SELECT * From Users WHERE UID=@uid and Account > @MinimumAccount
    /// In this example extracted parameter names are:
    /// "@uid", "@MinimumAccount"
    /// </summary>
    /// <param name="cmd">The command to extract parameter names from</param>
    /// <returns>A list of all extracted parameter names</returns>
    private static List<string> GetParameterNames(string cmd)
    {
      List<string> result = new List<string>();
      string token = "";

      for(int i = 0; i < cmd.Length; ++i)
        if (cmd[i] == '@')
        {
          do
          {
            token += cmd[i];
            ++i;
          } while (i < cmd.Length && cmd[i] != ' ' && cmd[i] != ',' && cmd[i] != ')');

          bool found = false;
          foreach (string s in result)
            if (s == token)
            {
              found = true;
              break;
            }

          if(!found)
            result.Add(token);

          token = "";
        }

      return result;
    }
    #endregion Internal Utilities


    #region Dealing With Images In DataBases
    /// <summary>
    /// This method extracts an image from a given BLOB object stored in a database
    /// </summary>
    /// <param name="blob">Stream of bytes that represent an image</param>
    /// <returns>Extracted image from the given BLOB data</returns>
    static public Image ImageFromBlob(object blob)
    {
      using (MemoryStream ms = new MemoryStream((byte[])blob))
        return Image.FromStream(ms);
    }

    /// <summary>
    /// This method converts a given image to binary storable data type. 
    /// Tries to minimize image size while converting to blob data.
    /// </summary>
    /// <param name="img">Image to convert to binary data</param>
    /// <returns>Binary data storable in a database</returns>
    static public byte [] ImageToBlob(Image img)
    {
      using (MemoryStream ms = new MemoryStream())
      {
        if (img != null)
          new Bitmap(img).Save(ms, img.SuggestSaveFormat());
        return ms.ToArray();
      }
    }
    #endregion Dealing With Images In DataBases


    #region New SQL Interface
    /// <summary>
    /// This method is used to retreive parameter names in a given sql command/query
    /// For Example: SELECT * From Users WHERE UID=@uid and Account > @MinimumAccount
    /// In this example extracted parameter names are:
    /// "@uid", "@MinimumAccount"
    /// </summary>
    /// <param name="cmd">The command to extract parameter names from</param>
    /// <returns>A list of all extracted parameter names</returns>
    private static MySqlCommand BuildSQLStatement(string template, params object[] p)
    {
      List<string> param_names = new List<string>();
      string token = "";

      // Extract parameter names
      for (int i = 0; i < template.Length; ++i)
        if (template[i] == '@')
        {
          do
          {
            token += template[i];
            ++i;
          } while (i < template.Length && char.IsLetterOrDigit(template[i]));

          foreach (string s in param_names)
            if (s == token)
              throw new Exception("(DataBase) Parameter name is not unique: " + token);

          param_names.Add(token);
          token = "";
        }

      // Sanity check for parameter count
      if (param_names.Count != p.Length)
        throw new Exception("(DataBase) Parameter name and value size mismatch." + template + ":" + p.ToString());

      // Now finally build the statement
      MySqlCommand result = new MySqlCommand(template, mDataBase);
      for (int i = 0; i < p.Length; ++i)
        result.Parameters.AddWithValue(param_names[i], p[i]);
      return result;
    }

    /// <summary>
    /// This method executes the given SQL query statement and returns its results
    /// </summary>
    /// <param name="sql">The SQL statement to execute</param>
    /// <param name="p">All the parameters required to execute the statment</param>
    /// <returns>DataTable containing the query result</returns>
    static public DataTable QueryResult(string sql, params object[] p)
    {
      MySqlDataAdapter adapter = new MySqlDataAdapter(BuildSQLStatement(sql, p));
      DataTable result = new DataTable();
      adapter.Fill(result);
      return result;
    }

    /// <summary>
    /// This method executes the given query and assumes that query output should only be a single value.
    /// </summary>
    /// <param name="sql">SQL query to execute</param>
    /// <param name="default_value">In case the query does not return any rows of data, this value will be returned</param>
    /// <param name="p">All parameters to the query statement</param>
    /// <returns>The output of executing the query</returns>
    static public Variant QueryValue(string sql, object default_value, params object[] p)
    {
      DataTable result = QueryResult(sql, p);

      if (result.Rows.Count > 1)
        throw new Exception("(DataBase) Query value: QueryResult() returned unacceptable count of rows");

      if (result.Columns.Count != 1)
        throw new Exception("(DataBase) Query value: QueryResult() returned unacceptable count of columns");

      return
        result.Rows.Count == 0 ?
        new Variant(default_value) :
        new Variant(result.Rows[0][0]);
    }

    /// <summary>
    /// This method executes a given SQL command
    /// </summary>
    /// <param name="sql">The SQL command to be executed: "CREATE TABLE @tablename"</param>
    /// <param name="p">The values for the parameters in the SQL command</param>
    /// <returns>Number of affected rows</returns>
    public static int ExecuteSQL(string sql, params object[] p)
    {
      MySqlCommand cmd = BuildSQLStatement(sql, p);
      int result = cmd.ExecuteNonQuery();
      mInsertID = cmd.LastInsertedId;
      return result;
    }

    /// <summary>
    /// This method executes a stored procedure and returns its results.
    /// </summary>
    /// <param name="proc_name">Name of the stored procedure</param>
    /// <param name="param_names">Name of the parameters for the stored procedure each starting with @ and seperated by space</param>
    /// <param name="p">Values for the parameters given to the stored procedure</param>
    /// <returns>Result from executing the stored procedure</returns>
    public static DataTable ExecuteProcedure(string proc_name, string param_names, params object[] p)
    {
      // Build a command for the stored procedure
      MySqlCommand cmd = BuildSQLStatement(param_names, p);
      cmd.CommandText = proc_name;
      cmd.CommandType = System.Data.CommandType.StoredProcedure;

      // And execute it
      DataTable result = new DataTable();
      new MySqlDataAdapter(cmd).Fill(result);
      return result;
    }

    /// <summary>
    /// This method executes a stored function and returns its result.
    /// </summary>
    /// <param name="func_name">Name of the stored function</param>
    /// <param name="param_names">Name of the parameters for the stored function each starting with @ and seperated by space</param>
    /// <param name="p">Values for the parameters given to the stored function</param>
    /// <returns>Output of the stored function</returns>
    public static Variant EvaluateFunction(string func_name, string param_names, params object[] p)
    {
      // Build a command for the stored function
      MySqlCommand cmd = BuildSQLStatement(param_names, p);
      cmd.CommandText = func_name;
      cmd.CommandType = System.Data.CommandType.StoredProcedure;

      // And execute it
      MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
      DataTable result = new DataTable();
      adapter.Fill(result);

      if (result.Rows.Count != 1)
        throw new Exception("(DataBase) evaluate function: result contains unacceptable count of rows");

      if (result.Columns.Count != 1)
        throw new Exception("(DataBase) evaluate function: result contains unacceptable count of columns");

      return new Variant(result.Rows[0][0]);
    }

    /// <summary>
    /// This method searches a table's column for a given value
    /// </summary>
    /// <param name="table">Table name to search in</param>
    /// <param name="field">Column name to search</param>
    /// <param name="value">Part or whole of the text to find</param>
    /// <param name="select_what">List of columns to select in the matching rows</param>
    /// <returns>Rows of data containing the search text</returns>
    public static DataTable SearchForValue(string table, string field, string value, string select_what = "*")
    {
      string sql = "SELECT " + select_what + " FROM " + table + " WHERE ";
      sql += field + " LIKE '%" + value + "%'";
      return QueryResult(sql);
    }
    #endregion New SQL Interface
  }
}
