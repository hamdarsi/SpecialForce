﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public partial class Competition
  {
    #region Properties
    private int mCompetitionID;
    public int ID
    {
      get { return mCompetitionID; }
    }

    private string mTitle;
    public string Title
    {
      get { return mTitle; }
      set
      {
        if (mState == CompetitionState.Editing)
          mTitle = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    private CompetitionType mCompetitionType;
    public CompetitionType Type
    {
      get { return mCompetitionType; }
      set
      {
        if (mState == CompetitionState.Editing)
          mCompetitionType = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    private bool mRoundRobin;
    public bool RoundRobin
    {
      get { return mRoundRobin; }
      set
      {
        if (mState == CompetitionState.Editing)
          mRoundRobin = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    private int mRoundCount;
    public int RoundCount
    {
      get { return mRoundCount; }
      set
      {
        if (mState == CompetitionState.Editing)
          mRoundCount = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    private int mRoundTime;
    public int RoundTime
    {
      get { return mRoundTime; }
      set
      {
        if (mState == CompetitionState.Editing)
          mRoundTime = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    private int mRestTime;
    public int RestTime
    {
      get { return mRestTime; }
      set
      {
        if (mState == CompetitionState.Editing)
          mRestTime = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    private int mStartHP;
    public int StartHP
    {
      get { return mStartHP; }
      set
      {
        if (mState == CompetitionState.Editing)
          mStartHP = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    private int mStartMagazine;
    public int StartMagazine
    {
      get { return mStartMagazine; }
      set
      {
        if (mState == CompetitionState.Editing)
          mStartMagazine = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    private int mAmmoPerMagazine;
    public int AmmoPerMagazine
    {
      get { return mAmmoPerMagazine; }
      set
      {
        if (mState == CompetitionState.Editing)
          mAmmoPerMagazine = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    private bool mFriendlyFire;
    public bool FriendlyFire
    {
      get { return mFriendlyFire; }
      set
      {
        if (mState == CompetitionState.Editing)
          mFriendlyFire = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    private GameObject mObject;
    public GameObject Object
    {
      get { return mObject; }
      set
      {
        if (mState == CompetitionState.Editing)
          mObject = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    private int mBombTime;
    public int BombTime
    {
      get { return mBombTime; }
      set
      {
        if (mState == CompetitionState.Editing)
          mBombTime = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    private double mAdmissionFee;
    public double AdmissionFee
    {
      get { return mAdmissionFee; }
      set
      {
        if (mState == CompetitionState.Editing)
          mAdmissionFee = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    // In minutes
    private int mGameTime;
    public int GameTime
    {
      get { return mGameTime; }
      set
      {
        if (mState == CompetitionState.Editing)
          mGameTime = value;
        else
          throw new Exception("(DB.Competition) Competition already initialized");
      }
    }

    private CompetitionState mState;
    public CompetitionState State
    {
      get { return mState; }
    }

    public PersianDateTime Start
    {
      get 
      {
        string sql = "SELECT Min(Start) FROM Games WHERE CompetitionID=@1";
        return DataBase.QueryValue(sql, null, mCompetitionID);
      }
    }

    public PersianDateTime End
    {
      get
      {
        if (!AllGamesInitialized)
          return null;

        string sql = "SELECT MAX(Finish) FROM Games WHERE CompetitionID=@1";
        return DataBase.QueryValue(sql, null, mCompetitionID);
      }
    }

    private List<CodeName> mCodeNameCache = null;
    private List<CodeName> mCodeNames
    {
      get
      {
        if(mCodeNameCache == null)
          mCodeNameCache = CodeName.LoadCompetitionsCodeNames(this);

        return mCodeNameCache;
      }
    }
    public List<CodeName> CodeNames
    {
      get 
      {
        List<CodeName> result = new List<CodeName>();
        result.AddRange(mCodeNames);
        return result;
      }
    }

    private List<List<CandidateMatch>> mStageCache = null;
    private List<List<CandidateMatch>> mStages
    {
      get
      {
        if(mStageCache == null)
        {
          mStageCache = new List<List<CandidateMatch>>();
          foreach (CandidateMatch cm in mCandidateMatches)
          {
            while (mStageCache.Count <= cm.Stage)
              mStageCache.Add(new List<CandidateMatch>());

            mStages[cm.Stage].Add(cm);
          }
        }

        return mStageCache;
      }
    }
    public List<List<CandidateMatch>> Stages
    {
      get
      {
        List<List<CandidateMatch>> result = new List<List<CandidateMatch>>();
        for (int i = 0; i < mStages.Count; ++i)
        {
          result.Add(new List<CandidateMatch>());
          for (int j = 0; j < mStages[i].Count; ++j)
            result[i].Add(mStages[i][j]);
        }
        return result;
      }
    }

    private List<CandidateMatch> mCandidateMatchCache = null;
    private List<CandidateMatch> mCandidateMatches
    {
      get
      {
        if(mCandidateMatchCache == null)
          mCandidateMatchCache = CandidateMatch.LoadCompetitionCandidateMatches(this);

        return mCandidateMatchCache;
      }
    }
    public List<CandidateMatch> CandidateMatches
    {
      get 
      {
        List<CandidateMatch> result = new List<CandidateMatch>();
        result.AddRange(mCandidateMatches);
        return result; 
      }
    }

    private List<int> mTeamIDCache = null;
    private List<int> mTeamIDs
    {
      get
      {
        if (mTeamIDCache == null)
        {
          mTeamIDCache = new List<int>();
          foreach (DataRow data in DataBase.QueryResult("SELECT TeamID FROM CompetitionTeams WHERE CompetitionID=@1", mCompetitionID).Rows)
            mTeamIDCache.Add(data.Field<int>(0));
        }

        return mTeamIDCache;
      }
    }
    public List<int> TeamIDs
    {
      get 
      {
        List<int> result = new List<int>();
        result.AddRange(mTeamIDs);
        return result; 
      }
    }

    private List<List<int>> mGroupCache = null;
    /// <summary>
    /// Groups of the cup. Each group is a list of team ids
    /// </summary>
    private List<List<int>> mGroupings
    {
      get
      {
        if(mGroupCache == null)
        {
          mGroupCache = new List<List<int>>();
          string sql = "SELECT * FROM CompetitionGroupings WHERE CompetitionID=@1";
          foreach (DataRow row in DataBase.QueryResult(sql, mCompetitionID).Rows)
          {
            int g_index = row.Field<int>("GroupIndex");
            int t_index = row.Field<int>("TeamIndex");
            int t_id = row.Field<int>("TeamID");

            while (mGroupCache.Count <= g_index)
              mGroupCache.Add(new List<int>());
            while (mGroupCache[g_index].Count <= t_index)
              mGroupCache[g_index].Add(-1);
            mGroupCache[g_index][t_index] = t_id;
          }
        }

        return mGroupCache;
      }
    }
    public List<List<int>> Groupings
    {
      get 
      {
        List<List<int>> result = new List<List<int>>();
        for (int i = 0; i < mGroupings.Count; ++i)
        {
          result.Add(new List<int>());
          for (int j = 0; j < mGroupings[i].Count; ++j)
            result[i].Add(mGroupings[i][j]);
        }
        return result;
      }

      set
      {
        if (mState == CompetitionState.Editing)
        {
          mGroupCache = value;

          string sql = "DELETE FROM CompetitionGroupings WHERE CompetitionID=@1";
          DataBase.ExecuteSQL(sql, mCompetitionID);

          sql = "INSERT INTO CompetitionGroupings ";
          sql += "(CompetitionID, GroupIndex, TeamIndex, TeamID) ";
          sql += "VALUES (@1, @2, @3, @4)";

          for (int i = 0; i < mGroupings.Count; ++i)
            for (int j = 0; j < mGroupings[i].Count; ++j)
              if (DataBase.ExecuteSQL(sql, mCompetitionID, i, j, mGroupings[i][j]) != 1)
                throw new Exception("(DB.Competition) Could not insert team grouping information");
        }
        else
          throw new Exception("(DB.Competition) competition already initialized");
      }
    }

    private List<Game> mGameCache = null;
    private List<Game> mGames
    {
      get
      {
        if(mGameCache == null)
          RegenerateGameList();

        return mGameCache;
      }
    }
    public List<Game> Games
    {
      get 
      {
        List<Game> result = new List<Game>();
        result.AddRange(mGames);
        return result;
      }
    }

    public bool AllGamesInitialized
    {
      get
      {
        string sql = @"SELECT COUNT(*) FROM CandidateMatches WHERE Valid=1 AND GameID=-1 AND CompetitionID=@1
                       UNION ALL
                       SELECT COUNT(*) FROM CandidateMatches WHERE CompetitionID=@2";
        DataTable tbl = DataBase.QueryResult(sql, mCompetitionID, mCompetitionID);
        return Convert.ToInt32(tbl.Rows[0][0]) == 0 && Convert.ToInt32(tbl.Rows[1][0]) != 0;
      }
    }

    public bool AllGamesDone
    {
      get
      {
        foreach (CandidateMatch cm in mCandidateMatches)
          if (!cm.Valid)
            continue;
          else if (cm.GameID == -1)
            return false;
          else if (DB.GameManager.GetGameByID(cm.GameID).State != GameState.Finished)
            return false;

        return true;
      }
    }

    private List<int> mUsedSiteIDsCache = null;

    private List<int> mUsedSiteIDs
    {
      get 
      {
        if (mUsedSiteIDsCache == null)
        {
          mUsedSiteIDsCache = new List<int>();
          RegenerateGameList();
        }

        return mUsedSiteIDsCache;
      }
    }

    public List<int> UsedSiteIDs
    {
      get { return mUsedSiteIDs; }
    }
    #endregion Properties


    #region Events
    public event EventHandler<EventArgs> OnRefresh;
    #endregion Events


    #region Constructor
    public Competition()
    {
      Clear();

      GameManager.OnGameDelete += OnGameDeleted;
    }
    #endregion Constructor


    #region Statistics
    public int GetFrag(DB.User player)
    {
      int frag = 0;
      foreach (DB.Game game in mGames)
        frag += game.GetFrag(player);

      return frag;
    }

    public int GetKillCount(DB.User player)
    {
      int cnt = 0;
      foreach (DB.Game game in mGames)
        cnt += game.GetKillCount(player);

      return cnt;
    }

    public int GetFriendlyKillCount(DB.User player)
    {
      int cnt = 0;
      foreach (DB.Game game in mGames)
        cnt += game.GetFriendlyKillCount(player);

      return cnt;
    }

    public int GetDeathCount(DB.User player)
    {
      int cnt = 0;
      foreach (DB.Game game in mGames)
        cnt += game.GetDeathCount(player);

      return cnt;
    }

    public int GetPlantCount(DB.User player)
    {
      int cnt = 0;
      foreach (DB.Game game in mGames)
        cnt += game.GetPlantCount(player);

      return cnt;
    }

    public int GetDiffuseCount(DB.User player)
    {
      int cnt = 0;
      foreach (DB.Game game in mGames)
        cnt += game.GetDiffuseCount(player);

      return cnt;
    }
    #endregion Statistics


    #region Utilities
    private void OnGameDeleted(Object sender, GameDeleteArgs e)
    {
      foreach (Game g in mGames)
        if (g.ID == e.Game.ID)
        {
          mGames.Remove(g);
          break;
        }

      foreach (CandidateMatch cm in mCandidateMatches)
        if (cm.GameID == e.Game.ID)
        {
          // This line fires CandidateMatch's FireEvent and also Competition's FireEvent
          cm.GameCanceled();
          break;
        }
    }

    public void _FireRefreshEvent()
    {
      RegenerateGameList();

      EventHandler<EventArgs> ev = OnRefresh;
      if (ev != null)
        ev(this, new EventArgs());
    }

    /// <summary>
    /// Finds out the date and time of registration for the given team.
    /// </summary>
    public PersianDateTime GetTeamRegistration(Team team)
    {
      string sql = "SELECT Registration FROM CompetitionTeams WHERE CompetitionID=@1 AND TeamID=@2";
      PersianDateTime result = DataBase.QueryValue(sql, null, mCompetitionID, team.ID);

      if (result == null)
        throw new Exception("(DB.Competition) Could not read team registration information");
      return result;
    }

    public int GetTeamIndex(int team_id)
    {
      for (int i = 0; i < mTeamIDs.Count; ++i)
        if (mTeamIDs[i] == team_id)
          return i;

      return -1;
    }

    public CodeName GetCodeName(string codename)
    {
      foreach (CodeName cn in mCodeNames)
        if (cn.Code == codename)
          return cn;

      return null;
    }

    public CodeName GetCodeName(int id)
    {
      foreach (CodeName cn in mCodeNames)
        if (cn.TeamID == id)
          return cn;

      return null;
    }

    public CandidateMatch GetCandidateMatchByWinnerCodeName(string codename)
    {
      foreach(CandidateMatch cm in mCandidateMatches)
        if (cm.WinnerCodeName == null)
          continue;
        else if (cm.WinnerCodeName.Code == null)
          continue;
        else if (cm.WinnerCodeName.Code == codename)
          return cm;

      return null;
    }

    public CandidateMatch GetCandidateMatch(CodeName blue, CodeName green)
    {
      foreach (CandidateMatch cm in mCandidateMatches)
        if (cm.ContainsCodeName(blue) && cm.ContainsCodeName(green))
          return cm;

      return null;
    }

    private List<CandidateMatch> GetCandidateMatches(CodeName team)
    {
      List<CandidateMatch> result = new List<CandidateMatch>();
      foreach (CandidateMatch cm in mCandidateMatches)
        if (cm.ContainsCodeName(team))
          result.Add(cm);

      return result;
    }

    public CandidateMatch GetCandidateMatch(Game game)
    {
      foreach(CandidateMatch cm in mCandidateMatches)
        if(cm.GameID == game.ID)
          return cm;

      return null;
    }

    static public int GetEliminationTotalCount(int real_count)
    {
      if (real_count <= 2)
        return 2;
      else if (real_count <= 4)
        return 4;
      else if (real_count <= 8)
        return 8;
      else if (real_count <= 16)
        return 16;
      else if (real_count <= 32)
        return 32;
      else
        return 64;
    }

    public List<CandidateMatch> GetGroupMatches(int group_index)
    {
      List<CandidateMatch> result = new List<CandidateMatch>();
      for (int i = 0; i < mCandidateMatches.Count; ++i)
        if (mCandidateMatches[i].Valid && mCandidateMatches[i].Stage == 0 && mCandidateMatches[i].Group == group_index)
          result.Add(mCandidateMatches[i]);
      return result;
    }

    /// <summary>
    /// Loads information for all the registered teams.
    /// </summary>
    /// <returns>List of load team instances</returns>
    public List<Team> GetTeams()
    {
      return DB.Team.GetTeamsByID(mTeamIDs);
    }

    public override string ToString()
    {
      return mTitle;
    }
    #endregion Utilities
    

    #region HashMap Performance Optimizations
    public override bool Equals(object obj)
    {
      Competition c = obj as Competition;
      return c == null ? false : c.mCompetitionID == mCompetitionID;
    }

    public override int GetHashCode()
    {
      return mCompetitionID;
    }
    #endregion HashMap Performance Optimizations


    #region Initialization Utilities
    private void AddCandidateMatch(CodeName blue, CodeName green,
                                   int stage, CodeName result = null,
                                   bool valid = true, int group = -1)
    {
      // Add to mCandidateMatches
      CandidateMatch cm = new CandidateMatch(this);
      cm.BlueTeam = blue;
      cm.GreenTeam = green;
      cm.WinnerCodeName = result;
      cm.Stage = stage;
      cm.Valid = valid;
      cm.Group = group;
      cm.Apply();
      mCandidateMatches.Add(cm);

      // Add to mStages
      while (mStages.Count <= cm.Stage)
        mStages.Add(new List<CandidateMatch>());

      mStages[cm.Stage].Add(cm);
    }

    private CodeName AddCodeName(int stage, int index, int team = -1, int group = 0)
    {
      CodeName cn = new CodeName(this);
      cn.SetCodeName(stage, index, group);
      cn.TeamID = team;
      cn.Apply();
      mCodeNames.Add(cn);

      return cn;
    }

    private CodeName AddCodeNameForGroupLeaders(int index, bool first_team)
    {
      CodeName cn = new CodeName(this);
      cn.SetCodeNameForGroupLeaders(index, first_team);
      cn.Apply();
      mCodeNames.Add(cn);

      return cn;
    }

    private CodeName AddCodeNameForFinalStage(bool final)
    {
      CodeName cn = new CodeName(this);
      cn.SetCodeNameForFinalStage(final);
      cn.Stage = mStages.Count - 1;
      cn.Apply();
      mCodeNames.Add(cn);

      return cn;
    }
    #endregion Initialization Utitlities


    #region Initialization
    public void Initialize()
    {
      mState = CompetitionState.Fixated;
      Apply();

      // Assign each team a code name and register it
      for (int i = 0; i < mTeamIDs.Count; ++i)
        AssignTeamToCodeName(mTeamIDs[i], AddCodeName(0, i));

      // Now initialize all matches
      mStages.Clear();
      if (mCompetitionType == CompetitionType.League)
        InitializeLeague();
      else if (mCompetitionType == CompetitionType.Elimination)
        InitializeElimination();
      else if (mCompetitionType == CompetitionType.Cup)
        InitializeCup();
    }

    private void InitializeLeague()
    {
      // intialize matches
      for (int x = 0; x < mTeamIDs.Count; ++x)
        for (int y = x + 1; y < mTeamIDs.Count; ++y)
        {
          CodeName blue = GetCodeName(mTeamIDs[y]);
          CodeName green = GetCodeName(mTeamIDs[x]);

          AddCandidateMatch(blue, green, 0, null);
          if (mRoundRobin)
            AddCandidateMatch(green, blue, 0, null);
        }
    }

    private void InitializeElimination()
    {
      int stage = 0;

      //randomize team indices for elimination competitions
      if (mCompetitionType == CompetitionType.Elimination)
      {
        List<int> initial_teams = new List<int>();
        List<int> new_teams = new List<int>();
        Random rand = new Random();

        initial_teams.AddRange(mTeamIDs);
        while(initial_teams.Count != 0)
        {
          int index = TinyMath.Round(rand.NextDouble() * (initial_teams.Count - 1));
          new_teams.Add(initial_teams[index]);
          initial_teams.RemoveAt(index);
        }

        mTeamIDs.Clear();
        mTeamIDs.AddRange(new_teams);
      }

      // create the schedule
      do
      {
        int total_count = GetEliminationTotalCount(stage == 0 ? mTeamIDs.Count : mStages[stage - 1].Count);
        int half_count = total_count / 2;

        for (int i = 0; i < half_count; ++i)
        {
          CodeName result = AddCodeName(stage + 1, i);
          CodeName blue_code;
          CodeName green_code;

          if (stage == 0)
          {
            blue_code = GetCodeName(mTeamIDs[i]);
            green_code = i + half_count < mTeamIDs.Count ? GetCodeName(mTeamIDs[i + half_count]) : null;
          }
          else
          {
            blue_code = mStages[stage - 1][i * 2].WinnerCodeName;
            green_code = mStages[stage - 1][i * 2 + 1].WinnerCodeName;
          }

          if (green_code != null)
            AddCandidateMatch(blue_code, green_code, stage, result, true);
          else
            AddCandidateMatch(blue_code, null, stage, result, false);
        }

      } while (mStages[stage++].Count != 1);
    }

    private void InitializeCup()
    {
      // Intialize group matches
      CodeName blue_code, green_code;
      for (int group = 0; group < mGroupings.Count; ++group)
        for (int x = 0; x < mGroupings[group].Count; ++x)
          for(int y = x + 1; y < mGroupings[group].Count; ++y)
          {
            blue_code = GetCodeName(mGroupings[group][y]);
            green_code = GetCodeName(mGroupings[group][x]);

            AddCandidateMatch(blue_code, green_code, 0, null, true, group);
            if (mRoundRobin)
              AddCandidateMatch(green_code, blue_code, 0, null, true, group);
          }

      // Initialize eliminations matches
      // First stage: cross matches. Matches between first team
      // of one group and second team of next group. Note that
      // a cup is always run in one of 2, 4 and 8 group modes.
      // So there is no resting turn for any of teams.
      int cnt = 0;
      for (int i = 0; i < mGroupings.Count; i += 2)
      {
        blue_code = AddCodeNameForGroupLeaders(i, true);
        green_code = AddCodeNameForGroupLeaders(i + 1, false);
        AddCandidateMatch(blue_code, green_code, 1, AddCodeName(2, cnt++));
      }
      for (int i = 0; i < mGroupings.Count; i += 2)
      {
        blue_code = AddCodeNameForGroupLeaders(i + 1, true);
        green_code = AddCodeNameForGroupLeaders(i, false);
        AddCandidateMatch(blue_code, green_code, 1, AddCodeName(2, cnt++));
      }

      // Second stage. Winners vs winners
      int stage = 1;
      while(mStages[stage++].Count != 2)
      {
        for (int i = 0; i < mStages[stage - 1].Count; i += 2)
        {
          blue_code = mStages[stage - 1][i].WinnerCodeName;
          green_code = mStages[stage - 1][i + 1].WinnerCodeName;
          AddCandidateMatch(blue_code, green_code, stage, AddCodeName(stage + 1, i / 2));
        }
      }

      // third stage: final match and classification
      CodeName winner1 = mStages[stage - 1][0].WinnerCodeName;
      CodeName winner2 = mStages[stage - 1][1].WinnerCodeName;
      AddCandidateMatch(winner1, winner2, stage, AddCodeNameForFinalStage(true));

      CodeName loser1 = AddCodeName(stage, 2);
      CodeName loser2 = AddCodeName(stage, 3);
      mStages[stage - 1][0].LoserCodeName = loser1;
      mStages[stage - 1][1].LoserCodeName = loser2;
      mStages[stage - 1][0].Apply();
      mStages[stage - 1][1].Apply();
      AddCandidateMatch(loser1, loser2, stage, AddCodeNameForFinalStage(false));
    }
    #endregion Initialization


    #region Competition Updates
    public void AssignTeamToCodeName(int team_id, CodeName codename)
    {
      // The code name's team ID
      codename.TeamID = team_id;
      codename.Apply();

      // Reassign all matches to their games to update the game.
      GetCandidateMatches(codename).ForEach(match => match.AssignGame(GameManager.GetGameByID(match.GameID)));
   }

    public void ResultProvided(Game game)
    {
      try
      {
        CandidateMatch cm = GetCandidateMatch(game);
        if (cm == null)
          throw new Exception("Game #" + game.ID + " has no candidate match");

        if (mCompetitionType == CompetitionType.Cup && cm.Stage == 0)
        {
          int done_cnt = 0, total_cnt = 0;
          for (int i = 0; i < mCandidateMatches.Count; ++i)
            if (mCandidateMatches[i].Stage == 0 && mCandidateMatches[i].Group == cm.Group)
            {
              ++total_cnt;
              if (mCandidateMatches[i].GameID != -1)
              {
                Game g = DB.GameManager.GetGameByID(mCandidateMatches[i].GameID);
                if (g.State == GameState.Finished)
                  ++done_cnt;
              }
            }

          if (done_cnt != total_cnt)
            return;

          string log = "Group games finished for Competition#" + mCompetitionID;
          log += ", Group #" + cm.Group;
          Logger.Log("(DB.Competition) " + log);

          Statistics<Stats.Team> rankings = Stats.Generator.Generate(this, false, cm.Group);
          CodeName first = GetCodeName("اول A" + (cm.Group + 1).ToString());
          CodeName second = GetCodeName("دوم A" + (cm.Group + 1).ToString());
          foreach (Stats.Team st in rankings.Data)
            if (st.Rank == 1)
              AssignTeamToCodeName(st.ID, first);
            else if (st.Rank == 2)
              AssignTeamToCodeName(st.ID, second);
        }
        else
        {
          // Update winner code name
          if (cm.WinnerCodeName != null)
            AssignTeamToCodeName(game.WinnerTeam, cm.WinnerCodeName);

          // Also update the loser code name
          if (cm.LoserCodeName != null)
            AssignTeamToCodeName(game.LoserTeam, cm.LoserCodeName);
        }

        // Check if all games were done.
        for(int i = 0; i < mCandidateMatches.Count; ++i)
          if (!mCandidateMatches[i].Valid)
            continue;
          else if (mCandidateMatches[i].GameID == -1)
            return;
          else if (DB.GameManager.GetGameByID(mCandidateMatches[i].GameID).State != GameState.Finished)
            return;

        // All matches are finished?
        mState = CompetitionState.Finished;
        Apply();
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "DB.Competition", "ResultProvided");
      }
    }
    #endregion Competition Updates


    #region DataBase Interactions
    public void AddTeam(DB.Team team)
    {
      mTeamIDs.Add(team.ID);
      string sql = "INSERT INTO CompetitionTeams (CompetitionID, TeamID, Registration) VALUES (@1, @2, @3)";
      DataBase.ExecuteSQL(sql, mCompetitionID, team.ID, PersianDateTime.Now.DBFormat);

      // And deduct admission fee.
      Invoice inv = new Invoice();
      inv.Amount = mAdmissionFee;
      inv.CompetitionID = mCompetitionID;
      inv.Type = InvoiceType.CompetitionAdmission;
      inv.TeamID = team.ID;
      inv.SiteID = Session.ActiveSiteID;
      inv.OperatorID = Session.CurrentStaff.ID;
      inv.TimeStamp = PersianDateTime.Now;
      inv.Apply();
    }

    public void RemoveTeam(DB.Team team, bool refund)
    {
      mTeamIDs.Remove(team.ID);

      string sql = "DELETE FROM CompetitionTeams WHERE CompetitionID=@1 AND TeamID=@2";
      if (DataBase.ExecuteSQL(sql, mCompetitionID, team.ID) != 1)
        throw new Exception("(DB.Competition) Could not delete the given team");

      if (refund)
      {
        sql = "DELETE FROM Accounting WHERE Type=@1 AND CompetitionID=@2 AND TeamID=@3";
        DataBase.ExecuteSQL(sql, Enums.ToString(InvoiceType.CompetitionAdmission), mCompetitionID, team.ID);
      }
    }

    public bool _Delete()
    {
      if (mState != CompetitionState.Editing)
        return false;

      foreach (Game g in mGames)
        GameManager.DeleteGame(g.ID);

      string sql = "DELETE FROM CandidateMatches WHERE CompetitionID=@1";
      DataBase.ExecuteSQL(sql, mCompetitionID);

      sql = "DELETE FROM CompetitionCodeNames WHERE CompetitionID=@1";
      DataBase.ExecuteSQL(sql, mCompetitionID);

      sql = "DELETE FROM CompetitionTeams WHERE CompetitionID=@1";
      DataBase.ExecuteSQL(sql, mCompetitionID);

      sql = "DELETE FROM Competitions WHERE CompetitionID=@1";
      DataBase.ExecuteSQL(sql, mCompetitionID);

      Clear();
      return true;
    }

    private void Clear()
    {
      mCompetitionID = -1;
      mTitle = "بدون عنوان";
      mCompetitionType = CompetitionType.Dummy;
      mRoundRobin = false;
      mRoundCount = -1;
      mRoundTime = -1;
      mStartHP = -1;
      mStartMagazine = -1;
      mAmmoPerMagazine = -1;
      mFriendlyFire = true;
      mObject = GameObject.NoObject;
      mBombTime = -1;
      mAdmissionFee = 0.0;
      mGameTime = 0;
      mState = CompetitionState.Editing;

      mTeamIDCache = null;
      mGroupCache = null;
      mCandidateMatchCache = null;
      mCodeNameCache = null;
      mStageCache = null;
      mGameCache = null;
    }

    /// <summary>
    /// Loads the game list for the competition.
    /// Also updates competitions time line.
    /// Also updates sites which the competition is being played.
    /// </summary>
    private void RegenerateGameList()
    {
      // Load games
      SortedSet<int> site_ids = new SortedSet<int>();
      mGameCache = new List<Game>();
      foreach (DB.CandidateMatch cm in mCandidateMatches)
        if (cm.GameID != -1)
        {
          DB.Game game = GameManager.GetGameByID(cm.GameID);
          mGameCache.Add(game);
          site_ids.Add(game.SiteID);
        }

      // Load used sites.
      mUsedSiteIDs.Clear();
      mUsedSiteIDs.AddRange(site_ids.ToList());
    }

    /// <summary>
    /// Checks out the competition and its information.
    /// To Do List:
    /// 1 - Read competition properties
    /// 2 - Read teams participating in the competition
    /// 3 - Read all CodeNames in the competition
    /// 4 - Read all CandidateMatches
    /// 5 - Read all Games in the competition
    /// 6 - Generate all stages based on candidate matches
    /// </summary>
    /// <param name="id">CompetitionID for the competition</param>
    /// <returns>True on successful read, false otherwise</returns>
    static private Competition LoadFromData(DataRow data)
    {
      try
      {
        Competition result = new Competition();
        result.mCompetitionID = Convert.ToInt32(data["CompetitionID"]);
        result.mTitle = data.Field<string>("Title");
        result.mCompetitionType = Enums.ParseCompetitionType(data.Field<string>("CompetitionType"));
        result.mRoundRobin = data.Field<bool>("RoundRobin");
        result.mRoundCount = data.Field<int>("RoundCount");
        result.mRoundTime = data.Field<int>("RoundTime");
        result.mRestTime = data.Field<int>("RestTime");
        result.mStartHP = data.Field<int>("StartHP");
        result.mStartMagazine = data.Field<int>("StartMagazine");
        result.mAmmoPerMagazine = data.Field<int>("AmmoPerMagazine");
        result.mFriendlyFire = data.Field<bool>("FriendlyFire");
        result.mObject = Enums.ParseGameObject(data.Field<string>("GameObject"));
        result.mBombTime = data.Field<int>("BombTime");
        result.mAdmissionFee = data.Field<double>("Admission");
        result.mGameTime = data.Field<int>("GameTime");
        result.mState = Enums.ParseCompetitionState(data.Field<string>("Status"));
        return result;
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "DB.Competition", "CheckOut");
        return null;
      }
    }

    static public Competition GetCompetitionByID(int id)
    {
      string sql = "SELECT * FROM Competitions WHERE CompetitionID=@1";
      return LoadFromData(DataBase.QueryResult(sql, id).Rows[0]);
    }

    static public List<Competition> LoadAllCompetitions()
    {
      Competition comp;
      List<Competition> result = new List<Competition>();

      foreach(DataRow row in DataBase.QueryResult("SELECT * FROM Competitions").Rows)
        if((comp = LoadFromData(row)) != null)
          result.Add(comp);

      return result;
    }

    static public List<int> GetGamesForCompetition(int id)
    {
      List<int> result = new List<int>();
      string sql = "SELECT GameID FROM Games WHERE CompetitionID=@1";
      foreach(DataRow row in DataBase.QueryResult(sql, id).Rows)
        result.Add(Convert.ToInt32(row[0]));
      return result;
    }

    public void Apply()
    {
      string sql;
      if (mCompetitionID == -1)
      {
        sql = "INSERT INTO Competitions (Title, CompetitionType, RoundRobin, RoundCount, RoundTime, RestTime, StartHP, StartMagazine, AmmoPerMagazine, FriendlyFire, GameObject, BombTime, Admission, GameTime, Status) VALUES ";
        sql += "(@1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14, @15)";
        if (DataBase.ExecuteSQL(sql, mTitle, Enums.ToString(mCompetitionType), mRoundRobin, mRoundCount, mRoundTime, mRestTime, mStartHP, mStartMagazine, mAmmoPerMagazine, mFriendlyFire, Enums.ToString(mObject), mBombTime, mAdmissionFee, mGameTime, Enums.ToString(mState)) != 1)
          throw new Exception("(DB.Competition) could not insert new comeptition");

        mCompetitionID = DataBase.InsertID;

        // now add to CompetitionManager
        CompetitionManager._CacheCompetition(this);
      }
      else
      {
        sql = "UPDATE Competitions SET Title=@1, CompetitionType=@2, RoundRobin=@3, RoundCount=@4, RoundTime=@5, RestTime=@6, StartHP=@7, StartMagazine=@8, AmmoPerMagazine=@9, FriendlyFire=@10, GameObject=@11, BombTime=@12, Admission=@13, GameTime=@14, Status=@15 WHERE CompetitionID=@16";
        if (DataBase.ExecuteSQL(sql, mTitle, Enums.ToString(mCompetitionType), mRoundRobin, mRoundCount, mRoundTime, mRestTime, mStartHP, mStartMagazine, mAmmoPerMagazine, mFriendlyFire, Enums.ToString(mObject), mBombTime, mAdmissionFee, mGameTime, Enums.ToString(mState), mCompetitionID) != 1)
          throw new Exception("(DB.Competition) could not update the comeptition");
      }
    }
    #endregion DataBase Interactions
  }
}
