﻿using System;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class UserSpecialTreat
  {
    #region Properties
    private int mTreatID = -1;
    public int ID
    {
      get { return mTreatID; }
    }

    private PersianDateTime mAssignDate = null;
    public PersianDateTime AssignDate
    {
      get { return mAssignDate == null ? null : new PersianDateTime(mAssignDate); }
    }

    private PersianDateTime mDueDate = null;
    public PersianDateTime DueDate
    {
      get { return mDueDate == null ? null : new PersianDateTime(mDueDate); }
    }

    private int mUserID = -1;
    public int UserID
    {
      get { return mUserID; }
    }

    private int mAssignerID = -1;
    public int AssignerID
    {
      get { return mAssignerID; }
    }

    private TreatType mTreat = TreatType.None;
    /// <summary>
    /// Returns current treat type.
    /// <remarks>CAUTION: This will not return empty treat type if 
    /// the treat is cancelled. Use IsWhiteList and IsBlackList instead</remarks>
    /// </summary>
    public TreatType Treat
    {
      get { return mTreat; }
    }

    private string mReason = "";
    public string Reason
    {
      get { return mReason; }
    }

    private int mCancellerID = -1;
    public int CancellerID
    {
      get { return mCancellerID; }
    }

    private PersianDateTime mCancelDate = null;
    public PersianDateTime CancelDate
    {
      get { return mCancelDate == null ? null : new PersianDateTime(mCancelDate); }
    }

    public bool Cancelled
    {
      get { return mCancelDate != null; }
    }

    private int mSiteID = -1;
    public int SiteID
    {
      get { return mSiteID; }
    }

    /// <summary>
    /// Empty treat. Shows no treat at all
    /// </summary>
    static public UserSpecialTreat EmptyTreat
    {
      get { return new UserSpecialTreat(); }
    }
    #endregion Properties


    #region Constructor
    private UserSpecialTreat()
    {
    }
    #endregion Constructor


    #region Treat encapsulation
    /// <summary>
    /// Nothing fancy. Executes the given query and loads all returned data.
    /// </summary>
    /// <param name="query">Query to execute</param>
    /// <param name="values">Query parameters</param>
    /// <returns>Loaded data from query result</returns>
    static private List<UserSpecialTreat> LoadQueryData(DataTable query)
    {
      List<UserSpecialTreat> result = new List<UserSpecialTreat>();
      foreach (DataRow data in query.Rows)
      {
        UserSpecialTreat treat = new UserSpecialTreat();
        treat.mTreatID = Convert.ToInt32(data["TreatID"]);
        treat.mAssignDate = PersianDateTime.FromObject(data["AssignDate"]);
        treat.mDueDate = PersianDateTime.FromObject(data["DueDate"]);
        treat.mUserID = data.Field<int>("UserID");
        treat.mAssignerID = data.Field<int>("AssignerID");
        treat.mReason = data.Field<string>("Reason");
        treat.mTreat = Enums.ParseTreatType(data.Field<string>("Treat"));
        treat.mCancellerID = data.Field<int>("CancellerID");
        treat.mCancelDate = PersianDateTime.FromObject(data["CancelDate"]);
        treat.mSiteID = data.Field<int>("SiteID");
        result.Add(treat);
      }
      
      return result;
    }

    /// <summary>
    /// This method searches the database for last given special treat to the given user.
    /// </summary>
    /// <param name="uid">User ID to get last treat</param>
    /// <returns>Last treat for the given UserID. If no treat found, null will be returned</returns>
    static public UserSpecialTreat GetLastTreatForUserInThisSite(int uid)
    {
      string sql = "SELECT * FROM UserSpecialTreats WHERE UserID=@1 AND SiteID=@2 ORDER BY AssignDate DESC LIMIT 1";
      List<UserSpecialTreat> treats = LoadQueryData(DataBase.QueryResult(sql, uid, Session.ActiveSiteID));
      return treats.Count == 1 ? treats[0] : null;
    }

    /// <summary>
    /// This method queries all given special treats to the selected user.
    /// </summary>
    /// <param name="uid">User ID to get all given treats</param>
    /// <returns>All registered treat for the given UserID</returns>
    static public List<UserSpecialTreat> GetAllTreatsForUser(int uid)
    {
      string sql = "SELECT * FROM UserSpecialTreats WHERE UserID=@1";
      return LoadQueryData(DataBase.QueryResult(sql, uid));
    }

    /// <summary>
    /// This method queries all given special treats.
    /// </summary>
    /// <returns>All registered treats</returns>
    static public List<UserSpecialTreat> GetAllTreats()
    {
      List<UserSpecialTreat> result = null;
      string time = Profiler.TimeString(() =>
      {
        string sql = "SELECT * FROM UserSpecialTreats";
        result = LoadQueryData(DataBase.QueryResult(sql));
      });

      Logger.Log("(DB.UserSpecialTreat) All treats loaded in " + time);
      return result;
    }

    /// <summary>
    /// This method queries all given special treats within the given time period.
    /// <param name="start">Start of the search period</param>
    /// <param name="end">End of the search period</param>
    /// </summary>
    /// <returns>All registered treats</returns>
    static public List<UserSpecialTreat> GetAllTreats(PersianDateTime start, PersianDateTime end)
    {
      List<UserSpecialTreat> result = null;
      string time = Profiler.TimeString( () =>
      {
        string sql = "SELECT * FROM UserSpecialTreats WHERE AssignedDate>=@1 AND AssignedDate<=@2";
        result = LoadQueryData(DataBase.QueryResult(sql, start.DBFormat, end.DBFormat));
      });

      Logger.Log("(DB.UserSpecialTreat) All treats loaded in " + time);
      return result;
    }

    /// <summary>
    /// This method queries all active special treats.
    /// </summary>
    /// <returns>All registered and active treats</returns>
    static public List<UserSpecialTreat> GetAllActiveTreats()
    {
      List<UserSpecialTreat> result = null;
      string time = Profiler.TimeString( () =>
      {
        /*
         * For future reference: I first wrote this stored procedure, It was some how sluggish.
         * It took 250ms in high end system for 21 rows. Most of its time was used in creating and
         * destroying its temporary table. Then I came up with this query. It took 5ms for the same data.
         
          @"CREATE DEFINER=`root`@`localhost` PROCEDURE `ListActiveTreats`()
          BEGIN
            DROP TABLE IF EXISTS TreatsTemp;
            CREATE TEMPORARY TABLE `TreatsTemp` (
              `TreatID` int(10) unsigned,
              `AssignDate` datetime NOT NULL,
              `DueDate` datetime NOT NULL,
              `UserID` int(11) NOT NULL,
              `AssignerID` int(11) NOT NULL,
              `Reason` varchar(1024) COLLATE utf8_persian_ci NOT NULL,
              `Treat` varchar(45) COLLATE utf8_persian_ci NOT NULL,
              `CancellerID` int(11) NOT NULL DEFAULT '-1',
              `CancelDate` datetime DEFAULT NULL,
	          PRIMARY KEY (`UserID`)
            );

            INSERT INTO TreatsTemp (SELECT * FROM UserSpecialTreats ORDER BY AssignDate ASC) 
                        on duplicate key update
                        TreatsTemp.TreatID=UserSpecialTreats.TreatID,
                        TreatsTemp.AssignDate=UserSpecialTreats.AssignDate,
                        TreatsTemp.DueDate=UserSpecialTreats.DueDate,
                        TreatsTemp.AssignerID=UserSpecialTreats.AssignerID,
                        TreatsTemp.Reason=UserSpecialTreats.Reason,
                        TreatsTemp.Treat=UserSpecialTreats.Treat,
                        TreatsTemp.CancellerID=UserSpecialTreats.CancellerID,
                        TreatsTemp.CancelDate=UserSpecialTreats.CancelDate;


            SELECT * FROM TreatsTemp;
          END"
         */
        string sql =  @"SELECT temp1.AssignDate, 
                                temp1.DueDate,
                                temp1.AssignerID,
                                temp1.Reason,
                                temp1.Treat,
                                temp1.CancellerID,
                                temp1.CancelDate,
                                temp1.TreatID,
                                temp1.UserID,
                                temp1.SiteID

                        FROM UserSpecialTreats temp1 
                        INNER JOIN (SELECT MAX(TreatID) as TreatID, UserID from UserSpecialTreats GROUP BY UserID) temp2
                          ON temp1.TreatID=temp2.TreatID and temp1.UserID=temp2.UserID";

        result = LoadQueryData(DataBase.QueryResult(sql));
      });

      Logger.Log("(DB.UserSpecialTreat) Active treats loaded in " + time);
      return result;
    }
    #endregion Treat encapsulation


    #region Utilities
    public bool IsValidBlackList()
    {
      if (mTreat != TreatType.BlackList)
        return false;

      if (mCancelDate != null)
        return false;

      if (mSiteID != Session.ActiveSiteID)
        return false;

      return mDueDate > PersianDateTime.Now;
    }

    public bool IsValidWhiteList()
    {
      if (mTreat != TreatType.WhiteList)
        return false;

      if (mCancelDate != null)
        return false;

      if (mSiteID != Session.ActiveSiteID)
        return false;

      return mDueDate > PersianDateTime.Now;
    }
    #endregion Utilities


    #region DataBase Interactions
    /// <summary>
    /// Registers the treat as a new interaction
    /// </summary>
    /// <param name="due">Due date for the treat</param>
    /// <param name="user">User ID</param>
    /// <param name="type">Type of the treat</param>
    /// <param name="reason">A string message used for later reviews</param>
    /// <param name="site_id">The specified site ID for this treat</param>
    /// <returns>The newly special treat created instance</returns>
    static public UserSpecialTreat Register(PersianDateTime due, int user, TreatType type, string reason, int site_id)
    {
      UserSpecialTreat result = new UserSpecialTreat();
      result.mAssignDate = PersianDateTime.Now;
      result.mDueDate = due;
      result.mUserID = user;
      result.mAssignerID = Session.CurrentStaff.ID;
      result.mTreat = type;
      result.mReason = reason;
      result.mSiteID = Session.ActiveSiteID;

      string sql = "INSERT INTO UserSpecialTreats (AssignDate, DueDate, UserID, AssignerID, Reason, Treat, SiteID) VALUES (@1, @2, @3, @4, @5, @6, @7)";
      if (DataBase.ExecuteSQL(sql, PersianDateTime.ToObject(result.mAssignDate), PersianDateTime.ToObject(result.mDueDate), result.mUserID, result.mAssignerID, result.mReason, Enums.ToString(result.mTreat), site_id) != 1)
        throw new Exception("(DB.UserSpecialTreat) error inserting special treat");

      result.mTreatID = DataBase.InsertID;
      return result;
    }

    /// <summary>
    /// Invalidates the special treat.
    /// </summary>
    public void Cancel()
    {
      mCancelDate = PersianDateTime.Now;
      mCancellerID = Session.CurrentStaff.ID;

      string sql = "UPDATE UserSpecialTreats SET CancellerID=@1, CancelDate=@2 WHERE TreatID=@3";
      if(DataBase.ExecuteSQL(sql, mCancellerID, PersianDateTime.ToObject(mCancelDate), mTreatID) != 1)
        throw new Exception("(DB.UserSpecialTreat) error canceling treat");
    }
    #endregion DataBase Interactions
  }
}
