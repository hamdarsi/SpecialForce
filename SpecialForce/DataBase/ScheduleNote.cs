﻿using System;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class ScheduleNote
  {
    #region Properties
    private int mNoteID = -1;
    public int ID
    {
      get { return mNoteID; }
    }

    private int mUserID = -1;
    public int UserID
    {
      get { return mUserID; }
    }

    private int mGameID = -1;
    public int GameID
    {
      get { return mGameID; }
    }

    private string mNote = "";
    public string Note
    {
      get { return mNote; }
    }

    private PersianDateTime mTimeStamp = null;
    public PersianDateTime TimeStamp
    {
      get { return mTimeStamp == null ? null : new PersianDateTime(mTimeStamp); }
    }

    private PersianDateTime mDue = null;
    public PersianDateTime Due
    {
      get { return mDue == null ? null : new PersianDateTime(mDue); }
    }

    private bool mDeleted = false;
    public bool Deleted
    {
      get { return mDeleted; }
    }

    public bool Expired
    {
      get { return mDue == null ? false : mDue < PersianDateTime.Now; }
    }
    #endregion Properties


    #region Constructor
    private ScheduleNote()
    {
    }
    #endregion Constructor


    #region DataBase Interactions
    public bool _Delete()
    {
      string sql = "UPDATE ScheduleNotes SET Deleted=@1 WHERE NoteID=@2";
      if (DataBase.ExecuteSQL(sql, true, mNoteID) != 1)
        return false;

      mDeleted = true;
      return true;
    }

    static public ScheduleNote NewNote(int game_id, string note, PersianDateTime due)
    {
      PersianDateTime now = PersianDateTime.Now;
      string sql = "INSERT INTO ScheduleNotes (UserID, GameID, Note, TimeStamp, Due) VALUES (@1, @2, @3, @4, @5)";
      if (DataBase.ExecuteSQL(sql, Session.CurrentStaff.ID, game_id, note, now.DBFormat, PersianDateTime.ToObject(due)) != 1)
        return null;

      ScheduleNote nt = new ScheduleNote();
      nt.mNoteID = DataBase.InsertID;
      nt.mGameID = game_id;
      nt.mNote = note;
      nt.mUserID = Session.CurrentStaff.ID;
      nt.mTimeStamp = now;
      nt.mDue = due;
      nt.mDeleted = false;
      return nt;
    }

    static public List<ScheduleNote> GetGameNotes(int game_id)
    {
      List<ScheduleNote> result = new List<ScheduleNote>();
      string sql = "SELECT * FROM ScheduleNotes WHERE GameID=@1";
      foreach(DataRow data in DataBase.QueryResult(sql, game_id).Rows)
      {
        ScheduleNote nt = new ScheduleNote();
        nt.mNoteID = Convert.ToInt32(data["NoteID"]);
        nt.mUserID = data.Field<int>("UserID");
        nt.mGameID = data.Field<int>("GameID");
        nt.mNote = data.Field<string>("Note");
        nt.mTimeStamp = PersianDateTime.FromObject(data["TimeStamp"]);
        nt.mDue = PersianDateTime.FromObject(data["Due"]);
        nt.mDeleted = data.Field<bool>("Deleted");
        result.Add(nt);
      }
      return result;
    }
    #endregion DataBase Interactions
  }
}
