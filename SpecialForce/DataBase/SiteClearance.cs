﻿using System;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class SiteClearance
  {
    #region Properties
    private int mClearID = -1;
    public int ClearID
    {
      get { return mClearID; }
    }

    private PersianDateTime mTimeStamp = null;
    public PersianDateTime TimeStamp
    {
      get { return mTimeStamp == null ? null : new PersianDateTime(mTimeStamp); }
      set { mTimeStamp = value; }
    }

    private double mAmount = 0.0;
    public double Amount
    {
      get { return mAmount; }
    }

    private int mClearerID = -1;
    public User Clearer
    {
      get { return User.GetUserByID(mClearerID); }
    }

    private string mComment = "";
    public string Comment
    {
      get { return mComment; }
    }

    private int mSiteID = -1;
    public Site Site
    {
      get { return Site.GetSiteByID(mSiteID); }
    }
    #endregion Properties


    #region DataBase Interactions
    static private SiteClearance LoadFromData(DataRow data)
    {
      try
      {
        SiteClearance result = new SiteClearance();
        result.mClearID = Convert.ToInt32(data["InvoiceID"]);
        result.mTimeStamp = PersianDateTime.FromObject(data["TimeStamp"]);
        result.mAmount = data.Field<double>("Amount");
        result.mClearerID = data.Field<int>("OperatorID");
        result.mComment = data.Field<string>("Comment");
        return result;
      }
      catch(Exception ex)
      {
        Logger.LogException(ex, "DB.SiteClearance", "LoadFromData");
        return null;
      }
    }

    static public void ClearSite(double amount, DB.Site site)
    {
      SiteClearance result = new SiteClearance();
      result.mAmount = amount;
      result.mComment = "";
      result.mTimeStamp = PersianDateTime.Now;
      result.mSiteID = site.ID;
      result.mClearerID = Session.CurrentStaff.ID;

      string sql = "INSERT INTO Accounting (Amount, Comment, TimeStamp, Type, SiteID, OperatorID)";
      sql += "VALUES (@1, @2, @3, @4, @5, @6)";
      if (DataBase.ExecuteSQL(sql, result.Amount, result.Comment, PersianDateTime.ToObject(result.TimeStamp), Enums.ToString(InvoiceType.Clear), result.Site.ID, result.Clearer.ID) != 1)
        throw new Exception("(DB.SiteClearance) could not clear site");

      site.Clearances.Add(result);
    }

    static public List<SiteClearance> GetSiteClearances(Site site)
    {
      List<SiteClearance> result = new List<SiteClearance>();

      string time = Profiler.TimeString(() =>
      {
        string sql = "SELECT * FROM Accounting WHERE Type=@1 AND SiteID=@2";
        foreach (DataRow row in DataBase.QueryResult(sql, Enums.ToString(InvoiceType.Clear), site.ID).Rows)
          result.Add(LoadFromData(row));
      });

      Logger.Log("(DB.SiteClearance) Generated " + result.Count.ToString() + " site clearances in " + time);
      return result;
    }
    #endregion DataBase Interactions
  }
}
