﻿using System;
using System.Data;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class UserStats
  {
    #region Properties
    private int mUserID;
    public int UserID
    {
      get { return mUserID; }
    }

    private string mFirstName;
    public string FirstName
    {
      get { return mFirstName; }
    }

    private string mLastName;
    public string LastName
    {
      get { return mLastName; }
    }

    public string FullName
    {
      get { return mFirstName + " " + mLastName; }
    }

    private int mTeamID;
    public int TeamID
    {
      get { return mTeamID; }
    }

    private string mTeamName;
    public string TeamName
    {
      get { return mTeamName; }
    }

    private int? mEnemyKills;
    public int EnemyKills
    {
      get
      {
        if (!mEnemyKills.HasValue)
          throw new Exception("(DB.UserStats) EnemyKills not initialized in query");

        return mEnemyKills.Value;
      }
    }

    private int? mFriendlyKills;
    public int FriendlyKills
    {
      get
      {
        if (!mFriendlyKills.HasValue)
          throw new Exception("(DB.UserStats) FriendlyKills not initialized in query");

        return mFriendlyKills.Value;
      }
    }

    public int TotalKills
    {
      get { return EnemyKills + FriendlyKills; }
    }

    private int? mDeathsFromEnemy;
    public int DeathsFromEnemy
    {
      get
      {
        if (!mDeathsFromEnemy.HasValue)
          throw new Exception("(DB.UserStats) DeathsFromEnemy not initialized in query");

        return mDeathsFromEnemy.Value;
      }
    }

    private int? mDeathsFromTeam;
    public int DeathsFromTeam
    {
      get
      {
        if (!mDeathsFromTeam.HasValue)
          throw new Exception("(DB.UserStats) DeathsFromTeam not initialized in query");

        return mDeathsFromTeam.Value;
      }
    }

    public int TotalDeaths
    {
      get { return DeathsFromEnemy + DeathsFromTeam; }
    }

    private int? mSuccessfulPlants;
    public int SuccessfulPlants
    {
      get
      {
        if (!mSuccessfulPlants.HasValue)
          throw new Exception("(DB.UserStats) SuccessfulPlants not initialized in query");

        return mSuccessfulPlants.Value;
      }
    }

    private int? mUnsuccessfulPlants;
    public int UnsuccessfulPlants
    {
      get
      {
        if (!mUnsuccessfulPlants.HasValue)
          throw new Exception("(DB.UserStats) UnsuccessfulPlants not initialized in query");

        return mUnsuccessfulPlants.Value;
      }
    }

    public int TotalPlants
    {
      get { return SuccessfulPlants + UnsuccessfulPlants; }
    }

    private int? mDiffuses;
    public int Diffuses
    {
      get
      {
        if (!mDiffuses.HasValue)
          throw new Exception("(DB.UserStats) Diffuses not initialized in query");

        return mDiffuses.Value;
      }
    }

    public int TotalFrag
    {
      get 
      {
        return
          EnemyKills * DB.Options.KillFrag
          - FriendlyKills * DB.Options.FriendlyKillFrag
          + SuccessfulPlants * DB.Options.BombPlantFrag
          + Diffuses * DB.Options.BombDiffuseFrag;
      }
    }

    private int mRank;
    public int Rank
    {
      get { return mRank; }
    }
    #endregion Properties


    #region Constructor
    private UserStats(bool ek, bool tk, bool de, bool dt, bool sp, bool up, bool df)
    {
      mUserID = -1;
      mFirstName = null;
      mLastName = null;
      mTeamID = -1;
      mTeamName = null;

      if(ek)
        mEnemyKills = 0;

      if(tk)
        mFriendlyKills = 0;

      if(de)
        mDeathsFromEnemy = 0;

      if(dt)
        mDeathsFromTeam = 0;

      if(sp)
        mSuccessfulPlants = 0;

      if(up)
        mUnsuccessfulPlants = 0;

      if(df)
        mDiffuses = 0;
    }
    #endregion Constructor


    #region Sub stats generators
    static private DataTable GenerateEnemyKills(List<int> games_ids)
    {
      string ids_str = string.Join(",", games_ids);
      string sql = @"SELECT
                       qplayers.UserID AS UserID,
                       COUNT(qKills.KillerID) AS EnemyKills

                     FROM
                       (SELECT
                           PlayersInRounds.UserID AS UserID,
                           PlayersInRounds.RoundID AS RoundID
                         FROM PlayersInRounds
	                     WHERE PlayersInRounds.RoundID IN 
                           (SELECT RoundID FROM Rounds WHERE Rounds.GameID IN (" + ids_str + @") AND Rounds.State='تمام شده')
                       ) qplayers
                     LEFT JOIN
                       (SELECT Kills.RoundID, Kills.KillerID FROM Kills WHERE Kills.Legal=1) qKills
                     ON 
                       qKills.RoundID=qplayers.RoundID AND
                       qKills.KillerID=qplayers.UserID

                     GROUP BY qplayers.UserID";

      return DataBase.QueryResult(sql);
    }

    static private DataTable GenerateFriendlyKills(List<int> games_ids)
    {
      string ids_str = string.Join(",", games_ids);
      string sql = @"SELECT
                       qplayers.UserID AS UserID,
                       COUNT(qKills.KillerID) AS FriendlyKills

                     FROM
                       (SELECT
                           PlayersInRounds.UserID AS UserID,
                           PlayersInRounds.RoundID AS RoundID
                         FROM PlayersInRounds
	                     WHERE PlayersInRounds.RoundID IN 
                           (SELECT RoundID FROM Rounds WHERE Rounds.GameID IN (" + ids_str + @") AND Rounds.State='تمام شده')
                       ) qplayers
                     LEFT JOIN
                       (SELECT Kills.RoundID, Kills.KillerID FROM Kills WHERE Kills.Legal=0) qKills
                     ON 
                       qKills.RoundID=qplayers.RoundID AND
                       qKills.KillerID=qplayers.UserID

                     GROUP BY qplayers.UserID";

      return DataBase.QueryResult(sql);
    }

    static private DataTable GenerateDeathsFromEnemy(List<int> games_ids)
    {
      string ids_str = string.Join(",", games_ids);
      string sql = @"SELECT
                      qplayers.UserID AS UserID,
                      COUNT(qKills.KilledID) AS DeathsFromEnemy

                     FROM
                       (SELECT
                           PlayersInRounds.UserID AS UserID,
                           PlayersInRounds.RoundID AS RoundID
                         FROM PlayersInRounds
	                     WHERE PlayersInRounds.RoundID IN 
                           (SELECT RoundID FROM Rounds WHERE Rounds.GameID IN (" + ids_str + @") AND Rounds.State='تمام شده')
                       ) qplayers
                     LEFT JOIN
                       (SELECT Kills.RoundID, Kills.KilledID FROM Kills WHERE Kills.Legal=1) qKills
                     ON 
                       qKills.RoundID=qplayers.RoundID AND
                       qKills.KilledID=qplayers.UserID

                     GROUP BY qplayers.UserID";

      return DataBase.QueryResult(sql);
    }

    static private DataTable GenerateDeathsFromTeam(List<int> games_ids)
    {
      string ids_str = string.Join(",", games_ids);
      string sql = @"SELECT
                      qplayers.UserID AS UserID,
                      COUNT(qKills.KilledID) AS DeathsFromTeam

                     FROM
                       (SELECT
                           PlayersInRounds.UserID AS UserID,
                           PlayersInRounds.RoundID AS RoundID
                         FROM PlayersInRounds
	                     WHERE PlayersInRounds.RoundID IN 
                           (SELECT RoundID FROM Rounds WHERE Rounds.GameID IN (" + ids_str + @") AND Rounds.State='تمام شده')
                       ) qplayers
                     LEFT JOIN
                       (SELECT Kills.RoundID, Kills.KilledID FROM Kills WHERE Kills.Legal=0) qKills
                     ON 
                       qKills.RoundID=qplayers.RoundID AND
                       qKills.KilledID=qplayers.UserID

                     GROUP BY qplayers.UserID";

      return DataBase.QueryResult(sql);
    }

    static private DataTable GenerateSuccessfulPlants(List<int> games_ids)
    {
      string ids_str = string.Join(",", games_ids);
      string sql = @"SELECT
                       qplayers.UserID AS UserID,
                       COUNT(Rounds.RoundID) AS SuccessfulPlants

                     FROM
                       (SELECT
                           PlayersInRounds.UserID AS UserID,
                           PlayersInRounds.RoundID AS RoundID
                         FROM PlayersInRounds
	                     WHERE PlayersInRounds.RoundID IN 
                           (SELECT RoundID FROM Rounds WHERE Rounds.GameID IN (" + ids_str + @") AND Rounds.State='تمام شده' AND Rounds.BombState='Exploded')
                       ) qplayers
                     LEFT JOIN
                       Rounds ON 
                         Rounds.RoundID=qplayers.RoundID AND
                         Rounds.Planter=qplayers.UserID

                     GROUP BY qplayers.UserID";

      return DataBase.QueryResult(sql);
    }

    static private DataTable GenerateUnsuccessfulPlants(List<int> games_ids)
    {
      string ids_str = string.Join(",", games_ids);
      string sql = @"SELECT
                       qplayers.UserID AS UserID,
                       COUNT(Rounds.RoundID) AS UnsuccessfulPlants

                     FROM
                       (SELECT
                           PlayersInRounds.UserID AS UserID,
                           PlayersInRounds.RoundID AS RoundID
                         FROM PlayersInRounds
	                     WHERE PlayersInRounds.RoundID IN 
                           (SELECT RoundID FROM Rounds WHERE Rounds.GameID IN (" + ids_str + @") AND Rounds.State='تمام شده' AND Rounds.BombState='Diffused')
                       ) qplayers
                     LEFT JOIN
                       Rounds ON 
                         Rounds.RoundID=qplayers.RoundID AND
                         Rounds.Planter=qplayers.UserID

                     GROUP BY qplayers.UserID";

      return DataBase.QueryResult(sql);
    }

    static private DataTable GenerateDiffuses(List<int> games_ids)
    {
      string ids_str = string.Join(",", games_ids);
      string sql = @"SELECT
                       qplayers.UserID AS UserID,
                       COUNT(Rounds.RoundID) AS Diffuses

                     FROM
                       (SELECT
                           PlayersInRounds.UserID AS UserID,
                           PlayersInRounds.RoundID AS RoundID
                         FROM PlayersInRounds
	                     WHERE PlayersInRounds.RoundID IN 
                           (SELECT RoundID FROM Rounds WHERE Rounds.GameID IN (" + ids_str + @") AND Rounds.State='تمام شده' AND Rounds.BombState='Diffused')
                       ) qplayers
                     LEFT JOIN
                       Rounds ON 
                         Rounds.RoundID=qplayers.RoundID AND
                         Rounds.Diffuser=qplayers.UserID

                     GROUP BY qplayers.UserID";

      return DataBase.QueryResult(sql);
    }


    static private DataTable GenerateUserList(List<int> games_ids)
    {
      string ids_str = string.Join(",", games_ids);
      string sql = @"SELECT 
                       qUserInfo.*,
                       Teams.Name AS TeamName

                     FROM
                       (SELECT
                          qplayers.UserID AS UserID,
                          all_users.FirstName AS FirstName,
                          all_users.LastName AS LastName,
                          all_users.TeamID AS TeamID

                        FROM
                          (SELECT
                             PlayersInRounds.UserID AS UserID
                           FROM
                             PlayersInRounds
	                       WHERE PlayersInRounds.RoundID IN 
                             (SELECT RoundID FROM Rounds WHERE Rounds.GameID IN (" + ids_str + @") AND Rounds.State='تمام شده')
                           GROUP BY
                             PlayersInRounds.UserID
                          ) qplayers

                        LEFT JOIN
                          (SELECT UserID, FirstName, LastName, TeamID FROM Users) all_users ON qplayers.UserID=all_users.UserID
                       ) qUserInfo

                     LEFT JOIN Teams
                     ON qUserInfo.TeamID=Teams.TeamID";

      return DataBase.QueryResult(sql);
    }
    #endregion Sub stats generators


    #region Main statistics generators
    static public List<UserStats> GenerateStats(
      List<int> game_ids, 
      bool inc_enemy_kills, 
      bool inc_friendly_kills, 
      bool inc_deaths_from_enemy, 
      bool inc_deaths_from_team,
      bool inc_successful_plants,
      bool inc_unsuccessful_plants,
      bool inc_diffuses)
    {
      if(game_ids.Count == 0)
        return new List<UserStats>();

      // Load basic information for all users
      Hashtable map = new Hashtable();
      List<UserStats> result;

      string time = Profiler.TimeString(() =>
      {
        Logger.Log("(DB.UserStats) Started generating statistics.");
        Logger.Log("(DB.UserStats)   Includes enemy kills        : " + inc_enemy_kills.ToString());
        Logger.Log("(DB.UserStats)   Includes friendly kills     : " + inc_friendly_kills.ToString());
        Logger.Log("(DB.UserStats)   Includes deaths by enemy    : " + inc_deaths_from_enemy.ToString());
        Logger.Log("(DB.UserStats)   Includes deaths by team     : " + inc_deaths_from_team.ToString());
        Logger.Log("(DB.UserStats)   Includes successful plants  : " + inc_successful_plants.ToString());
        Logger.Log("(DB.UserStats)   Includes unsuccessful plants: " + inc_unsuccessful_plants.ToString());
        Logger.Log("(DB.UserStats)   Includes diffuses           : " + inc_diffuses.ToString());

        // Load all user infromation for the given set of games
        foreach (DataRow row in GenerateUserList(game_ids).Rows)
        {
          UserStats inf = new UserStats(inc_enemy_kills, inc_friendly_kills, inc_deaths_from_enemy, inc_deaths_from_team, inc_successful_plants, inc_unsuccessful_plants, inc_diffuses);
          inf.mUserID = row.Field<int>("UserID");
          inf.mFirstName = row.Field<string>("FirstName");
          inf.mLastName = row.Field<string>("LastName");
          inf.mTeamID = row.Field<int>("TeamID");
          inf.mTeamName = row.Field<string>("TeamName");
          if (inf.mTeamName == null)
            inf.mTeamName = "";

          map.Add(row.Field<int>("UserID"), inf);
        }

        // Generate their statistics
        if (inc_enemy_kills)
          foreach (DataRow data in GenerateEnemyKills(game_ids).Rows)
            (map[Convert.ToInt32(data[0])] as UserStats).mEnemyKills = Convert.ToInt32(data[1]);

        if (inc_friendly_kills)
          foreach (DataRow data in GenerateFriendlyKills(game_ids).Rows)
            (map[Convert.ToInt32(data[0])] as UserStats).mFriendlyKills = Convert.ToInt32(data[1]);

        if (inc_deaths_from_enemy)
          foreach (DataRow data in GenerateDeathsFromEnemy(game_ids).Rows)
            (map[Convert.ToInt32(data[0])] as UserStats).mDeathsFromEnemy = Convert.ToInt32(data[1]);

        if (inc_deaths_from_team)
          foreach (DataRow data in GenerateDeathsFromTeam(game_ids).Rows)
            (map[Convert.ToInt32(data[0])] as UserStats).mDeathsFromTeam = Convert.ToInt32(data[1]);
        
        if (inc_successful_plants)
          foreach (DataRow data in GenerateSuccessfulPlants(game_ids).Rows)
            (map[Convert.ToInt32(data[0])] as UserStats).mSuccessfulPlants = Convert.ToInt32(data[1]);
        
        if (inc_unsuccessful_plants)
          foreach (DataRow data in GenerateUnsuccessfulPlants(game_ids).Rows)
            (map[Convert.ToInt32(data[0])] as UserStats).mUnsuccessfulPlants = Convert.ToInt32(data[1]);
        
        if (inc_diffuses)
          foreach (DataRow data in GenerateDiffuses(game_ids).Rows)
            (map[Convert.ToInt32(data[0])] as UserStats).mDiffuses = Convert.ToInt32(data[1]);

        // Sort the user list
        result = map.Values.Cast<UserStats>().ToList();
        if (result.Count > 1)
          QuickSort(result, 0, result.Count - 1);

        // Assign their ranks
        int rank = 0;
        int delta = 1;
        for (int i = result.Count - 1; i >= 0; --i)
        {
          if (i < result.Count - 1 && result[i] == result[i + 1])
            ++delta;
          else
          {
            rank += delta;
            delta = 1;
          }

          result[i].mRank = rank;
        }
      });

      string log = 
        "(DB.UserStats) Generated statistics of " +
        map.Count.ToString() + " users for " + 
        game_ids.Count.ToString() + " games done in " + time;
      Logger.Log(log);
      return map.Values.Cast<UserStats>().ToList();
    }

    static private void SwapPlayers(List<UserStats> stats, int index1, int index2)
    {
      UserStats temp = stats[index1];
      stats[index1] = stats[index2];
      stats[index2] = temp;
    }

    static private void QuickSort(List<UserStats> players, int Left, int Right)
    {
      int LHold = Left;
      int RHold = Right;
      int Pivot = Left;
      ++Left;

      while (Right >= Left)
      {
        if (players[Pivot] < players[Left] && players[Right] < players[Pivot])
          SwapPlayers(players, Left, Right);
        else if (players[Pivot] < players[Left])
          --Right;
        else if (players[Right] < players[Pivot])
          ++Left;
        else
        {
          --Right;
          ++Left;
        }
      }

      SwapPlayers(players, Pivot, Right);
      Pivot = Right;
      if (Pivot > LHold)
        QuickSort(players, LHold, Pivot);
      if (RHold > Pivot + 1)
        QuickSort(players, Pivot + 1, RHold);
    }
    #endregion Main statistics generators


    #region UserStats comparison
    public CompareResult CompareTo(UserStats other)
    {
      // Priorities:
      // 1: Frag
      if (TotalFrag > other.TotalFrag)
        return CompareResult.Higher;
      else if (TotalFrag < other.TotalFrag)
        return CompareResult.Lower;

      // 2: Kills
      if (EnemyKills > other.EnemyKills)
        return CompareResult.Higher;
      else if (EnemyKills < other.EnemyKills)
        return CompareResult.Lower;

      // 3: Deaths
      if (TotalDeaths < other.TotalDeaths)
        return CompareResult.Higher;
      else if (TotalDeaths > other.TotalDeaths)
        return CompareResult.Lower;

      // 4: Plants
      if (SuccessfulPlants > other.SuccessfulPlants)
        return CompareResult.Higher;
      else if (SuccessfulPlants < other.SuccessfulPlants)
        return CompareResult.Lower;

      // 5: Diffuses
      if (Diffuses > other.Diffuses)
        return CompareResult.Higher;
      else if (Diffuses < other.Diffuses)
        return CompareResult.Lower;

      // 7: Equal
      return CompareResult.Equal;
    }

    public static bool operator >(UserStats u1, UserStats u2)
    {
      return u1.CompareTo(u2) == CompareResult.Higher;
    }

    public static bool operator <(UserStats u1, UserStats u2)
    {
      return u1.CompareTo(u2) == CompareResult.Lower;
    }

    public static bool operator ==(UserStats u1, UserStats u2)
    {
      return u1.CompareTo(u2) == CompareResult.Equal;
    }

    public static bool operator !=(UserStats u1, UserStats u2)
    {
      return u1.CompareTo(u2) != CompareResult.Equal;
    }

    public override int GetHashCode()
    {
      return mUserID;
    }

    public override bool Equals(object obj)
    {
      return this == (obj as UserStats);
    }
    #endregion UserStats comparison
  }
}
