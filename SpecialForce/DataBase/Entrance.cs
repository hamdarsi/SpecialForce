﻿using System;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class Entrance
  {
    #region Propeties
    private int mEntranceID = -1;
    public int ID
    {
      get { return mEntranceID; }
    }

    private int mUserID;
    public int UserID
    {
      get { return mUserID; }
    }

    private string mUserFullName;
    public string UserFullName
    {
      get { return mUserFullName; }
    }

    private PersianDateTime mEnter;
    public PersianDateTime Enter
    {
      get { return mEnter; }
    }

    private PersianDateTime mExit;
    public PersianDateTime Exit
    {
      get { return mExit; }
    }

    private int mSiteID;
    public int SiteID
    {
      get { return mSiteID; }
    }
    #endregion Propeties


    #region Entrance logs
    static public Entrance Login(User user)
    {
      Entrance result = new Entrance();
      result.mUserID = user.ID;
      result.mUserFullName = user.FullName;
      result.mEnter = PersianDateTime.Now;
      result.mExit = null;
      result.mSiteID = Session.ActiveSiteID;

      string sql = "INSERT INTO Entrances (UserID, Enter, SiteID) VALUES (@1, @2, @3)";
      if (DataBase.ExecuteSQL(sql, result.mUserID, result.mEnter.DBFormat, result.mSiteID) != 1)
        throw new Exception("(Session) could not record login information");

      result.mEntranceID = DataBase.InsertID;
      return result;
    }

    public void Logout()
    {
      if(mEntranceID == -1)
        throw new Exception("(Session) No entrance IDs set.");

      mExit = PersianDateTime.Now;
      string sql = "UPDATE Entrances SET Dismiss=@1 WHERE EntranceID=@2";
      DataBase.ExecuteSQL(sql, mExit.DBFormat, mEntranceID);
    }
    #endregion Entrance logs


    #region Reports
    static private Entrance ParseRow(DataRow data)
    {
      Entrance result = new Entrance();
      result.mUserID = data.Field<int>("UserID");
      result.mUserFullName = data.Field<string>("FirstName") + " " + data.Field<string>("LastName");
      result.mEnter = PersianDateTime.FromObject(data["Enter"]);
      result.mExit = PersianDateTime.FromObject(data["Dismiss"]);
      result.mSiteID = data.Field<int>("SiteID");
      return result;
    }

    static public List<Entrance> GetAllEntrances(PersianDateTime start, PersianDateTime finish)
    {
      List<Entrance> result = new List<Entrance>();

      string time = Profiler.TimeString(() =>
      {
        string sql = @"SELECT EntranceID, Enter, Dismiss, SiteID, Entrances.UserID, Users.FirstName, Users.LastName 
                       FROM Entrances INNER JOIN Users ON 
                       Entrances.UserID=Users.UserID
                       WHERE Entrances.Enter>=@1 AND Entrances.Enter<=@2";
        foreach (DataRow data in DataBase.QueryResult(sql, start.DBFormat, finish.DBFormat).Rows)
          result.Add(ParseRow(data));
      });

      Logger.Log("(DB.Entrance) Generated entrance report from " + result.Count.ToString() + " records in " + time);
      return result;
    }

    static public List<Entrance> GetAllEntrances(PersianDateTime start, PersianDateTime finish, DB.User user)
    {
      List<Entrance> result = new List<Entrance>();

      string time = Profiler.TimeString(() =>
      {
        string sql = @"SELECT EntranceID, Enter, Dismiss, SiteID, Entrances.UserID, Users.FirstName, Users.LastName 
                       FROM Entrances INNER JOIN Users ON 
                       Entrances.UserID=Users.UserID
                       WHERE Entrances.Enter>=@1 AND Entrances.Enter<=@2 And Entrances.UserID=@3";

        foreach (DataRow data in DataBase.QueryResult(sql, start.DBFormat, finish.DBFormat, user.ID).Rows)
          result.Add(ParseRow(data));
      });

      Logger.Log("(DB.Entrance) Generated entrance report from " + result.Count.ToString() + " records in " + time);
      return result;
    }

    static public List<Entrance> GetAllEntrances(PersianDateTime start, PersianDateTime finish, DB.User user, List<DB.Site> sites)
    {
      List<Entrance> result =
        user == null ?
        GetAllEntrances(start, finish) :
        GetAllEntrances(start, finish, user);

      HashSet<int> site_ids = new HashSet<int>();
      foreach(DB.Site st in sites)
        site_ids.Add(st.ID);

      for (int i = 0; i < result.Count; ++i)
        if (!site_ids.Contains(result[i].SiteID))
          result.RemoveAt(i--);

      return result;
    }
    #endregion Reports
  }
}
