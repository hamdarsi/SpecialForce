﻿using System;
using System.Linq;
using System.Data;
using System.Collections;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class CompetitionManager
  {
    #region Internal Data
    static private Hashtable mCompetitions = new Hashtable();
    #endregion Internal Data


    #region Properties
    static public List<DB.Competition> AllCompetitions
    {
      get
      {
        UpdateCache();
        return mCompetitions.Values.Cast<Competition>().ToList();
      }
    }
    #endregion Properties


    #region Events
    static public event EventHandler<EventArgs> OnCompetitionsUpdated;
    #endregion Events


    #region Constructor
    public CompetitionManager()
    {
    }
    #endregion Constructor


    #region Utilities
    static private Competition CacheCompetition(int id)
    {
      if (id == -1)
        return null;

      if (mCompetitions.ContainsKey(id))
        return mCompetitions[id] as Competition;

      Competition c = null;
      try
      {
        c = Competition.GetCompetitionByID(id);
      }
      catch
      {
        return null;
      }

      mCompetitions.Add(c.ID, c);
      FireUpdateEvent();
      return c;
    }

    static private void FireUpdateEvent()
    {
      EventHandler<EventArgs> ev = OnCompetitionsUpdated;
      if (ev != null)
        ev(null, new EventArgs());
    }
    #endregion Utilities


    #region Internal Affairs
    static public void _CacheCompetition(Competition instance)
    {
      mCompetitions.Add(instance.ID, instance);
    }

    static public bool _RemoveCache(int id)
    {
      if (!mCompetitions.Contains(id))
        return false;

      (mCompetitions[id] as Competition)._Delete();
      mCompetitions.Remove(id);
      FireUpdateEvent();
      return true;
    }
    #endregion Internal Affairs


    #region Public Interface
    static private void UpdateCache()
    {
      List<int> ids = new List<int>();
      foreach(DataRow row in DataBase.QueryResult("SELECT CompetitionID FROM Competitions").Rows)
        CacheCompetition(Convert.ToInt32(row[0]));
    }

    static public Competition GetCompetition(int id)
    {
      return CacheCompetition(id);
    }
    #endregion Public Interface
  }
}
