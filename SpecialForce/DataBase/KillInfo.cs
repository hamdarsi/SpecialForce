﻿using System;
using System.Data;
using System.Collections.Generic;

namespace SpecialForce.DB
{
  public class KillInfo
  {
    #region Properties
    private int mKillID = -1;
    public int ID
    {
      get { return mKillID; }
    }

    private int mGameID = -1;
    public int GameID
    {
      get { return mGameID; }
    }

    private int mRoundID = -1;
    public int RoundID
    {
      get { return mRoundID; }
    }

    private int mKiller = -1;
    public int KillerID
    {
      get { return mKiller; }
    }

    private int mKilled = -1;
    public int KilledID
    {
      get { return mKilled; }
    }

    private int mTimeInRound = -1;
    public int TimeInRound
    {
      get { return mTimeInRound; }
    }

    private bool mLegal = false;
    public bool Legal
    {
      get { return mLegal; }
    }
    #endregion Properties


    #region Constructors
    public KillInfo(int game_id, int round_id, int killer, int killed, int time, bool legal)
    {
      mGameID = game_id;
      mRoundID = round_id;
      mKiller = killer;
      mKilled = killed;
      mLegal = legal;
      mTimeInRound = time;

      string sql = "INSERT INTO Kills (GameID, RoundID, KillerID, KilledID, TimeInRound, Legal) ";
      sql += "VALUES (@1, @2, @3, @4, @5, @6)";

      if (DataBase.ExecuteSQL(sql, mGameID, mRoundID, mKiller, mKilled, mTimeInRound, Legal) != 1)
        throw new Exception("(DB.KillInfo) could not insert kill info into Kills table");
    }

    private KillInfo()
    {
    }
    #endregion Constructors


    #region Internal Utilities
    static private KillInfo ParseRow(DataRow data)
    {
      KillInfo inf = new KillInfo();
      inf.mKillID = Convert.ToInt32(data[0]);
      inf.mGameID = data.Field<int>(1);
      inf.mRoundID = data.Field<int>(2);
      inf.mKiller = data.Field<int>(3);
      inf.mKilled = data.Field<int>(4);
      inf.mTimeInRound = data.Field<int>(5);
      inf.mLegal = data.Field<bool>(6);
      return inf;
    }
    #endregion Internal Utilities


    #region Kill Information Utilities
    static public List<KillInfo> GetKillsOfRound(int game_id, int round_id)
    {
      List<KillInfo> result = new List<KillInfo>();
      string sql = "SELECT * FROM Kills WHERE GameID=@1 AND RoundID=@2";
      foreach(DataRow row in DataBase.QueryResult(sql, game_id, round_id).Rows)
        result.Add(ParseRow(row));
      return result;
    }

    static public List<KillInfo> GetKillsOfGame(int game_id)
    {
      List<KillInfo> result = new List<KillInfo>();
      string sql = "SELECT * FROM Kills WHERE GameID=@1";
      foreach (DataRow row in DataBase.QueryResult(sql, game_id).Rows)
        result.Add(ParseRow(row));
      return result;
    }
    #endregion Kill Information Utilities
  }
}
