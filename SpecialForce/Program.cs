﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;

namespace SpecialForce
{
  static class Program
  {
    #region Data and Properties
    private enum InitResult
    {
      AnotherInstanceOpen,
      InstanceRegistrationFailure,
      DataBaseUnreachable,
      DataBaseAuthenticationFailure,
      DataBaseInitializationPending,
      FirstTimeInDataBase,
      NoPassword,
      FingerPrintNotConfigured,
      SiteNotSet,
      Succeeded
    }

    static private Mutex mMutex = null;
    static private string mMutexName = "ALERPA program for counter strike gaming";

    static private ExecutionMode mExecutionMode = SpecialForce.ExecutionMode.None;
    static public ExecutionMode ExecutionMode
    {
      get { return mExecutionMode; }
    }
    #endregion Data and Properties


    #region Initialization
    static InitResult InitializeApplication()
    {
      // List of things to do:
      // =========================================
      //   1 - Open Logger
      //   2 - Check if another instance is up
      //   3 - Database
      //   4 - Finger print
      //   5 - Session
      //   6 - Control Tower
      //   7 - Clean up if needed. Before application start


      // 1 - Open Logger
      Logger.Open();


      // 2 - Check if another instance is up
      // Attempt to open existing mutex
      Logger.Log("(Program) Starting application");
      Logger.Log("(Program) Checking for another running instance:");
      try
      {
        mMutex = Mutex.OpenExisting(mMutexName);

        // It openned? then there is another instance up and running
        // Should not release, since no call to WaitOne() is done
        // Disposing is done in application clean up by CLR I think
        mMutex = null;
        Session.ShowMessage("برنامه در حال اجراست.");
        Logger.Log("(Program) Another instance is currently running. Mutex openned with no exception.");
        return InitResult.AnotherInstanceOpen;
      }
      catch (WaitHandleCannotBeOpenedException)
      {
        // Could not open existing mutex?
        // This program is the first to run.
        Logger.Log("(Program) No such instance found. Attempting creation:");

        bool created;
        mMutex = new Mutex(false, mMutexName, out created);
        if (!created)
        {
          // Could not acquire the mutex?
          mMutex = null;
          Session.ShowError("اتفاق نادری رخ داده است. لطفا در صورت مشاهده دوباره این پیام با شرکت آلرپا تماس بگیرید.");
          Logger.Log("(Program) Failed");
          return InitResult.InstanceRegistrationFailure;
        }

        mMutex.WaitOne();
        Logger.Log("(Program) Done.");
      }
      catch
      {
        // Something weird has happenned
        // Should not release, since no call to WaitOne() is done
        // Disposing is done in application clean up by CLR I think
        mMutex = null;
        Session.ShowError("اتفاق نادری رخ داده است. لطفا در صورت مشاهده دوباره این پیام با شرکت آلرپا تماس بگیرید.");
        Logger.Log("(Program) Something weird happened");
        return InitResult.InstanceRegistrationFailure;
      }

      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);


      // 3 - Database
      Logger.Log("(Program) Attempting initializtion of DataBase system:");
      DataBase.OpenResult res = DataBase.Open();
      if (res == DataBase.OpenResult.NoPassword)
        return InitResult.NoPassword;
      else if (res == DataBase.OpenResult.HostUnreachable)
      {
        Session.ShowError("برنامه نمی تواند به پایگاه داده متصل شود.");
        return InitResult.DataBaseUnreachable;
      }
      else if (res == DataBase.OpenResult.AuthenticationFailure)
      {
        Session.ShowError("رمز عبور پایگاه داده اشتباه وارد شده است.");
        return InitResult.DataBaseAuthenticationFailure;
      }
      else if (res == DataBase.OpenResult.CreationNeeded)
      {
        Logger.Log("(Program) No database existing. Creation pending.");
        return InitResult.DataBaseInitializationPending;
      }
      else if (res == DataBase.OpenResult.NoAdminFound)
      {
        Logger.Log("(Program) Database seems empty. No admins found.");
        return InitResult.FirstTimeInDataBase;
      }
      else if (res == DataBase.OpenResult.ExcessiveGames)
      {
        Logger.Log("(Program) What the fuck happenned?");
        return InitResult.AnotherInstanceOpen;
      }
      Logger.Log("(Program) Done.");


      // 4 - Finger print
      Logger.Log("(Program) Attempting initialization of FingerPrint system:");
      FingerPrint.OpenResult fp_res = FingerPrint.OpenSystem();
      if (fp_res == FingerPrint.OpenResult.NotConfigured)
        return InitResult.FingerPrintNotConfigured;
      Logger.Log("(Program) Done.");


      // 5 - Session
      Logger.Log("(Program) Attempting to start Session system:");
      Session.OpenResult result = Session.Start();
      if (result == Session.OpenResult.SiteNotSet)
        return InitResult.SiteNotSet;
      Logger.Log("(Program) Done.");

      // 6 - Control Tower
      Logger.Log("(Program) Attempting initialization of ControlTower:");
      ControlTower.Tower.Start();
      Logger.Log("(Program) Done.");


      // 7 - Clean up if needed. Before application start
      Logger.Log("(Program) Initialization done.");
      PrepareApplication();
      return InitResult.Succeeded;
    }
    #endregion Initialization


    #region Finalization
    static void FinalizeApplication()
    {
      Logger.Log("(Program) Application terminated, cleaning up:");

      ControlTower.Tower.Stop();
      Logger.Log("(Program) ControlTower: done.");

      DB.GameManager.EndDaemon(false);
      Logger.Log("(Program) DB.GameManager: done.");

      Session.End();
      Logger.Log("(Program) Session: done.");

      FingerPrint.CloseSystem();
      Logger.Log("(Program) FingerPrint: done.");

      DataBase.Close();
      Logger.Log("(Program) DataBase: done.");

      if (mMutex != null)
      {
        Logger.Log("(Program) Application instance: done.");
        mMutex.ReleaseMutex();
      }

      Logger.Close();
    }
    #endregion Finalization


    #region Preparation
    static void PrepareApplication()
    {
      // Generic preperation
      // ====================================

      // 1: For dummy games which are timed to be executed now, reset them to be run again
      PersianDateTime now = PersianDateTime.Now;
      string sql = "UPDATE Games SET State=@1 WHERE GameType=@2 AND Start<=@3 AND Finish>=@4 AND State<>@5";
      DataBase.ExecuteSQL(sql, Enums.ToString(GameState.Reserved), Enums.ToString(CompetitionType.Dummy), now.DBFormat, now.DBFormat, Enums.ToString(GameState.Free));

      // Debugging preparation
      // ====================================

      if (!Session.ReleaseBuild)
      {
        List<DB.User> candidates = DB.User.LoadSomeUsers(1, -1, "همدرسی");
        foreach (DB.User candidate in candidates)
          if (candidate.FirstName != "مهدی")
            continue;
          else if (candidate.BirthDate != PersianDateTime.Date(1366, 10, 4))
            continue;
          else if (!candidate.Gender)
            continue;
          else
          {
            Session.Login(candidate.ID);
            break;
          }

        if(!Session.Open)
          Session.Login(1);
      }
    }
    #endregion Preparation


    #region Main
    [STAThread]
    static void Main()
    {
      try
      {
        Form frm = null;
        InitResult result = InitializeApplication();

        // If a wizard needs to be shown:
        if (result == InitResult.DataBaseAuthenticationFailure)
          frm = new Wizards.WizardManager();
        else if (result == InitResult.FirstTimeInDataBase)
          frm = new Wizards.WizardManager();
        else if (result == InitResult.DataBaseUnreachable)
          frm = new Wizards.WizardManager();
        else if (result == InitResult.DataBaseInitializationPending)
          frm = new Wizards.WizardManager();
        else if (result == InitResult.NoPassword)
          frm = new Wizards.WizardManager();
        else if (result == InitResult.SiteNotSet)
          frm = new Wizards.WizardManager();
        else if (result == InitResult.FingerPrintNotConfigured)
          frm = new Wizards.WizardManager();
        else if (result == InitResult.Succeeded)
        {
          if (Properties.Settings.Default.KioskMode)
            frm = Desktop.Kiosk.Instance;
          else
            frm = Desktop.Desktop.Instance;
        }
        
        // Run if no problems found
        if (frm != null)
        {
          if (frm is Wizards.WizardForm)
          {
            Logger.Log("(Program) Running application in wizard mode");
            mExecutionMode = SpecialForce.ExecutionMode.Wizard;
          }
          else if (frm is Desktop.Kiosk)
          {
            Logger.Log("(Program) Running in kiosk mode");
            mExecutionMode = SpecialForce.ExecutionMode.Kiosk;
          }
          else
          {
            Logger.Log("(Program) Running in client mode");
            mExecutionMode = SpecialForce.ExecutionMode.Client;
          }

          Application.Run(frm);
        }
      }
      catch (Exception e)
      {
        Logger.LogException(e, "Program", "Fatal exception");
        if (Session.ReleaseBuild)
        {
          string s1 = "برنامه با مشکل مواجه شده است و نیاز به اجرای دوباره دارد.";
          string s2 = "در صورت تکرار مشکل با شرکت الرپا تماس بگیرید.";
          Session.ShowError(s1 + Environment.NewLine + s2);
        }
        else
          Session.ShowError(e.Message + Environment.NewLine + e.StackTrace);

        throw;
      }

      FinalizeApplication();
    }
  }
  #endregion Main
}
