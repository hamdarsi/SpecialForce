﻿namespace SpecialForce.ControlTower
{
  partial class FormDeviceInteractions
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDeviceInteractions));
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.interactions = new SpecialForce.ControlTower.DeviceInteractionsUI();
      this.lblTitle = new SpecialForce.TransparentLabel();
      this.SuspendLayout();
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.Location = new System.Drawing.Point(12, 2);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(70, 26);
      this.transparentLabel1.TabIndex = 0;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.FixFromRight = true;
      this.transparentLabel3.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel3.Location = new System.Drawing.Point(12, 273);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(0, 0);
      this.transparentLabel3.TabIndex = 5;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // interactions
      // 
      this.interactions.Font = new System.Drawing.Font("B Nazanin", 12F);
      this.interactions.ForeColor = System.Drawing.Color.White;
      this.interactions.Location = new System.Drawing.Point(12, 37);
      this.interactions.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
      this.interactions.Name = "interactions";
      this.interactions.Size = new System.Drawing.Size(331, 200);
      this.interactions.TabIndex = 6;
      this.interactions.TabStop = false;
      this.interactions.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("interactions.Texts")));
      // 
      // lblTitle
      // 
      this.lblTitle.FixFromRight = true;
      this.lblTitle.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblTitle.Location = new System.Drawing.Point(88, 2);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(73, 26);
      this.lblTitle.TabIndex = 7;
      this.lblTitle.TabStop = false;
      this.lblTitle.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblTitle.Texts")));
      // 
      // FormDeviceInteractions
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(355, 272);
      this.ControlBox = false;
      this.Controls.Add(this.lblTitle);
      this.Controls.Add(this.interactions);
      this.Controls.Add(this.transparentLabel3);
      this.Controls.Add(this.transparentLabel1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.MaximumSize = new System.Drawing.Size(371, 503);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(371, 288);
      this.Name = "FormDeviceInteractions";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormDeviceGroupUI_FormClosing);
      this.Load += new System.EventHandler(this.FormDeviceGroupUI_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private TransparentLabel transparentLabel1;
    private TransparentLabel transparentLabel3;
    private DeviceInteractionsUI interactions;
    private TransparentLabel lblTitle;
  }
}