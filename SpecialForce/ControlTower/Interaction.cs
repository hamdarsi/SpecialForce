﻿using System;
using System.Collections.Generic;

namespace SpecialForce.ControlTower
{
  public class Interaction
  {
    private string mTryString;
    private string mDoneString;
    private string mNoNeedString;

    FormDeviceInteractions mParent = null;

    private Interactions mType;
    public Interactions Type
    {
      get { return mType; }
    }

    private DeviceBase mDevice = null;
    public DeviceID DeviceID
    {
      get { return mDevice == null ? DeviceID.Unknown : mDevice.ID; }
    }

    private bool mValid = true;
    public bool Valid
    {
      get { return mValid; }
    }

    public Interaction(FormDeviceInteractions frm, Interactions type, DeviceID target, bool valid)
    {
      mParent = frm;
      mType = type;
      mValid = valid;
      if (target != DeviceID.AllDevices)
        mDevice = Tower.GetDeviceInstance(target);

      // Now set teh state strings
      if (mType == Interactions.ConfigureKevlar)
      {
        mTryString = "در حال تنظیم";
        mDoneString = "تنظیم شد.";
        mNoNeedString = "در بازی نیست.";
      }
      else if (mType == Interactions.ConfigureBomb)
      {
        mTryString = "در حال تنظیم بمب";
        mDoneString = "بمب تنظیم شد.";
        mNoNeedString = "نیازی نیست";
      }
      else if (mType == Interactions.TurnOn)
      {
        mTryString = "";
        mDoneString = "";
        mNoNeedString = "";
      }
      else if (mType == Interactions.CheckOn)
      {
        mTryString = "در حال چک کردن";
        mDoneString = "روشن";
        mNoNeedString = "ها ها!";
      }
      else
      {
        mTryString = "در حال چک کردن";
        mDoneString = "خاموش";
        mNoNeedString = "در بازی است";
      }
    }

    /// <summary>
    /// Constraints the given value to be in the given range.
    /// </summary>
    private int ApplyRange(int value, int min, int max)
    {
      value = Math.Max(value, min);
      value = Math.Min(value, max);
      return value;
    }

    public bool Execute()
    {
      // If the device is intended to be untouched, do not do anything
      if (!mValid)
      {
        mParent.ShowInteraction(mNoNeedString, mDevice, DeviceStateUI.NotAffected);
        return true;
      }

      // If device needs configuration, configure it
      Kevlar kevlar = mDevice as Kevlar;
      Bomb bomb = mDevice as Bomb;
      DB.Game game = DB.GameManager.CurrentGame;
      if (mType == Interactions.ConfigureKevlar)
      {
        // Assign device properties
        int blue, green;
        game.GetTeamScores(out blue, out green);

        kevlar.StartHP = ApplyRange(game.StartHP, 1, 99);
        kevlar.StartMagazine = ApplyRange(game.StartMagazine, 0, 99);
        kevlar.AmmoPerMagazine = ApplyRange(game.AmmoPerMagazine, 0, 999);
        kevlar.Frag = ApplyRange(game.GetFrag(kevlar.Player), -9, 99);
        kevlar.DeathCount = ApplyRange(game.GetDeathCount(kevlar.Player), 0, 99);
        kevlar.RoundTime = ApplyRange(game.RoundTime, 10, 999);
        kevlar.BombTime = ApplyRange(game.BombTime, 30, 999);
        kevlar.FriendlyFire = game.FriendlyFire;
        kevlar.BlueScore = ApplyRange(blue, 0, 99);
        kevlar.GreenScore = ApplyRange(green, 0, 99);
      }
      else if (mType == Interactions.ConfigureBomb)
        foreach(Bomb b in Tower.Bombs)
        {
          b.TerroristTeam = game.CurrentRound.TerroristsAreBlue ? TeamType.Blue : TeamType.Green;
          b.BombTimer = ApplyRange(game.BombTime, 10, 999);
        }

      // Actual execution
      int tries;
      string error_string = "";
      for (tries = 1; tries <= 3; ++tries)
      {
        mParent.ShowInteraction(mTryString, mDevice, DeviceStateUI.Commiting, tries);
        if (mType == Interactions.ConfigureKevlar)
        {
          DeviceLocation loc = DeviceLocation.Center;
          if (kevlar != null)
            loc =
              kevlar.Team == TeamType.Blue ?
              DeviceLocation.BlueBase :
              DeviceLocation.GreenBase;
          if (mDevice.Configure(loc))
            break;

          Tower.TurnOff(mDevice.ID, true);
          error_string = "تنظیم نمی شود.";
        }
        else if (mType == Interactions.ConfigureBomb)
        {
          bool ok = true;
          foreach(Bomb b in Tower.Bombs)
            if (!b.Configure())
            {
              Tower.TurnOff(b.ID, true);
              ok = false;
              break;
            }

          if(ok)
          {
            Command cmd = new Command(Commands.ConfigureParams, DeviceID.AllDevices);
            cmd.AddParameter(game.BombTime, 3);

            CommandQueue.SleeplessWait(1.0);
            CommandQueue.SendCommand(cmd);
            CommandQueue.SleeplessWait(0.5);
            CommandQueue.SendCommand(cmd);
            CommandQueue.SleeplessWait(0.5);
            CommandQueue.SendCommand(cmd);
            break;
          }

          error_string = "بمب تنظیم نمی شود.";
        }
        else if (mType == Interactions.TurnOn)
        {
          Tower.TurnOnAll();
          break;
        }
        else if (mType == Interactions.CheckOn)
        {
          DeviceLocation loc = DeviceLocation.Center;
          if (kevlar != null)
            loc = 
              kevlar.Team == TeamType.Blue ? 
              DeviceLocation.BlueBase : 
              DeviceLocation.GreenBase;
          mDevice.Sync(loc);

          if (kevlar != null)
          {
            if (kevlar.State != KevlarState.Working)
              error_string = "روشن نشد.";
            else if (kevlar.HP != kevlar.StartHP)
              error_string = "روشن شد ولی سلامتی تنظیم نیست.";

            if(!Properties.Settings.Default.KevlarFullCheck)
              break;

            // Not full check => check all parameters
            if (kevlar.Magazine != kevlar.StartMagazine)
              error_string = "روشن شد ولی خشاب تنظیم نیست.";
            else if (kevlar.Ammo != kevlar.AmmoPerMagazine)
              error_string = "روشن شد ولی تیر تنظیم نیست.";
            else if (Math.Abs(kevlar.RemainingTime - kevlar.RoundTime) > 59)
              error_string = "روشن شد ولی زمان راند تنظیم نیست.";
            else if (kevlar.RemainingBombTime != kevlar.BombTime)
              error_string = "روشن شد ولی زمان بمب تنظیم نیست.";
            else
              break;
          }
        }
        else if (mType == Interactions.CheckOff)
        {
          if (kevlar != null || bomb != null)
          {
            Command cmd = null;
            cmd = new Command(Commands.State, mDevice.ID);
            cmd.AddParameter((int)KevlarState.Disabled, 2);
            DeviceLocation loc = DeviceLocation.Center;
            if (kevlar != null)
              loc =
                kevlar.Team == TeamType.Blue ?
                DeviceLocation.BlueBase :
                DeviceLocation.GreenBase;
            cmd.PreferedLocation = loc;
            CommandQueue.SendCommand(cmd);
            break;
          }
        }
      }

      if (tries == 4)
      {
        mParent.AbortInteractions(error_string, mDevice);
        return false;
      }
      else
      {
        mParent.ShowInteraction(mDoneString, mDevice, DeviceStateUI.Finished);
        return true;
      }
    }

    public override string ToString()
    {
      string result = "None";
      if (mType == Interactions.ConfigureKevlar)
        result = "ConfigKev";
      else if (mType == Interactions.ConfigureBomb)
        result = "ConfigBomb";
      else if (mType == Interactions.TurnOn)
        result = "TurnOn";
      else if (mType == Interactions.CheckOn)
        result = "CheckOn";
      else if (mType == Interactions.CheckOff)
        result = "CheckOff";

      result += "|";
      result += mDevice != null ? Kevlars.ToString(mDevice.ID) : "All";
      return mValid ? result : result + " [disabled]";
    }
  }
}
