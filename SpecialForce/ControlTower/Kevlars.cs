﻿using System;
using System.Collections.Generic;

namespace SpecialForce
{
  public static class Kevlars
  {
    #region Counts
    public static int CountForTeams
    {
      get { return 10; }
    }

    public static int TotalCount
    {
      get { return 20; }
    }

    public static int BombCount
    {
      get { return 2; }
    }
    #endregion Counts


    #region ID <--> Number
    public static int NumberFromID(DeviceID id)
    {
      if (id >= DeviceID.KevlarBlue1 && id <= DeviceID.KevlarGreen10)
        return (int)id;
      else if (id <= DeviceID.Bomb1 && id >= DeviceID.Bomb2)
        return Math.Abs((int)DeviceID.Bomb1);
      else
        return -1;
    }

    public static DeviceID KevlarIDFromNumber(int number)
    {
      return (DeviceID)number;
    }
    #endregion ID <--> Number


    #region ID <--> Index
    public static int IndexFromID(DeviceID id)
    {
      if (id >= DeviceID.KevlarBlue1 && id <= DeviceID.KevlarGreen10)
        return (int)id - 1;
      else if (id >= DeviceID.Bomb2 && id <= DeviceID.Bomb1)
        return Math.Abs((int)id) - 1;
      else
        return -1;
    }

    public static DeviceID KevlarIDFromIndex(int index)
    {
      return (DeviceID)(index + 1);
    }
    #endregion ID <--> Index


    #region ID <--> User
    public static DB.User UserFromID(DeviceID id)
    {
      ControlTower.Kevlar kevlar = ControlTower.Tower.GetDeviceInstance(id) as ControlTower.Kevlar;
      return kevlar != null ? kevlar.Player : null;
    }
    #endregion ID <--> User


    #region Is xxx ?
    static public bool IsKevlar(DeviceID id)
    {
      return id >= DeviceID.KevlarBlue1 && id <= DeviceID.KevlarGreen10;
    }

    static public bool IsBomb(DeviceID id)
    {
      return id == DeviceID.Bomb1 || id == DeviceID.Bomb2;
    }
    #endregion Is xxx ?


    #region ID -> String
    static public string ToString(DeviceID id)
    {
      if (id >= DeviceID.KevlarBlue1 && id <= DeviceID.KevlarBlue10)
        return "لباس " + (id - DeviceID.KevlarBlue1 + 1).ToString() + " آبی";
      else if (id >= DeviceID.KevlarGreen1 && id <= DeviceID.KevlarGreen10)
        return "لباس " + (id - DeviceID.KevlarGreen1 + 1).ToString() + " سبز";
      else if (id == DeviceID.Bomb1)
        return "بمب 1";
      else if (id == DeviceID.Bomb2)
        return "بمب 2";
      else
        throw new Exception("(Kevlars) Unknown id to convert to string: " + id.ToString());
    }
    #endregion ID -> String
  }
}
