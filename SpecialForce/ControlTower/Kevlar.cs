﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SpecialForce.ControlTower
{
  public partial class Kevlar : DeviceBase
  {
    #region Number and Team
    public int Number
    {
      get { return (int)mDeviceID; }
      set
      {
        if (value < 1 || value > Kevlars.TotalCount)
          throw new Exception("(Kevlar) Number out of range: " + value.ToString());

        mDeviceID = (DeviceID)value;
      }
    }

    public int TeamNumber
    {
      get
      {
        if (mDeviceID >= DeviceID.KevlarBlue1 && mDeviceID < DeviceID.KevlarGreen1)
          return Number;
        else
          return Number - Kevlars.CountForTeams;
      }
    }

    public TeamType Team
    {
      get
      {
        return
          (int)mDeviceID <= Kevlars.CountForTeams ?
          TeamType.Blue :
          TeamType.Green;
      }
    }
    #endregion Number and Team


    #region States
    private KevlarState mState = KevlarState.Disabled;
    public KevlarState State
    {
      get { return mState; }
    }

    private bool mResetInProgress = false;
    #endregion States


    #region Kevlar properties and match information
    /// <summary>
    /// Default count of hit points for the kevlar.
    /// </summary>
    private int mStartHP = 3;
    public int StartHP
    {
      get { return mStartHP; }
      set { mStartHP = value; }
    }

    /// <summary>
    /// Current remaining hit points for the kevlar.
    /// </summary>
    private int mHP = 3;
    public int HP
    {
      get { return mHP; }
      set { mHP = value; }
    }

    /// <summary>
    /// Default count of magazines for the kevlar
    /// </summary>
    private int mStartMagazine = 5;
    public int StartMagazine
    {
      get { return mStartMagazine; }
      set { mStartMagazine = value; }
    }

    /// <summary>
    /// Current count of remaining magazines for the kevlar.
    /// </summary>
    private int mMagazine = 5;
    public int Magazine
    {
      get { return mMagazine; }
    }

    /// <summary>
    /// Maximum ammo that a magazine can handle.
    /// </summary>
    private int mAmmoPerMagazine = 30;
    public int AmmoPerMagazine
    {
      get { return mAmmoPerMagazine; }
      set { mAmmoPerMagazine = value; }
    }

    /// <summary>
    /// Current ammunition on the kevlars. This is not the total
    /// of bullets each kevlar has remaining, but the current 
    /// count of bullet in the current loaded magazine.
    /// </summary>
    private int mAmmo = 30;
    public int Ammo
    {
      get { return mAmmo; }
    }

    /// <summary>
    /// This parameter tells kevlar whether they should accept shots
    /// from friendly kevlars or not. If set to true, then their 
    /// shots are friendly, they will not shoot themselves.
    /// </summary>
    private bool mFriendlyFire = true;
    public bool FriendlyFire
    {
      get { return mFriendlyFire; }
      set { mFriendlyFire = value; }
    }

    /// <summary>
    /// DefaultRoundTime parameter in kevlars. Expressed in seconds.
    /// </summary>
    private int mRoundTime = 480;
    public int RoundTime
    {
      get { return mRoundTime; }
      set { mRoundTime = value; }
    }

    /// <summary>
    /// Remaining time on the kevlars. Expressed in seconds
    /// </summary>
    private int mRemainingTime = 0;
    public int RemainingTime
    {
      get { return mRemainingTime; }
    }

    /// <summary>
    /// The duration which the bomb takes to explode 
    /// after it has been planted. Expressed in seconds.
    /// </summary>
    private int mBombTime = 90; // in seconds
    public int BombTime
    {
      get { return mBombTime; }
      set { mBombTime = value; }
    }

    /// <summary>
    /// This parameter shows the remaining bomb time on
    /// the kevlar once the bomb has been activated.
    /// </summary>
    private int mRemainingBombTime = 0;
    public int RemainingBombTime
    {
      get { return mRemainingBombTime; }
    }

    /// <summary>
    /// The user in the kevlar.
    /// </summary>
    public DB.User Player
    {
      get { return Tower.GetActiveUserInstanceForKevlar(mDeviceID); }
      set
      {
        // Update ControlTower.Tower
        ControlTower.Tower.UpdatePlayerInKevlar(mDeviceID, value);

        // Broadcast player assigned event
        EventHandler<EventArgs> ev = OnPlayerAssigned;
        if (ev != null)
          ev(this, new EventArgs());
      }
    }
    #endregion Kevlar properties and match information


    #region Scores and Stats
    private int mFrag = 0;
    public int Frag
    {
      get { return mFrag; }
      set { mFrag = value; }
    }

    private int mDeathCount = 0;
    public int DeathCount
    {
      get { return mDeathCount; }
      set { mDeathCount = value; }
    }

    private int mBlueScore = 0;
    public int BlueScore
    {
      get { return mBlueScore; }
      set { mBlueScore = value; }
    }

    private int mGreenScore = 0;
    public int GreenScore
    {
      get { return mGreenScore; }
      set { mGreenScore = value; }
    }
    #endregion Scores and Stats


    #region Events
    public event EventHandler<EventArgs> OnPlayerAssigned;
    #endregion Events


    #region Constructor
    public Kevlar(int number)
    {
      Number = number;
    }
    #endregion Constructor


    #region Message Processing
    protected override bool ProcessMessageInternal(Commands cmd, List<int> prms)
    {
      if (cmd == Commands.State)
      { }
      else if (cmd == Commands.ConfigurePlayer)
      {
        mConfigured = true;
        mHP = mStartHP;
        mMagazine = mStartMagazine;
        mAmmo = mAmmoPerMagazine;
      }
      else if (cmd == Commands.RequestGameState)
      {
        DB.Game game = DB.GameManager.CurrentGame;
        DB.Round round = game != null ? game.CurrentRound : null;

        // Quite a list of conditions!
        if(round != null &&
           round.State == RoundState.InProgress && 
           Tower.GetActiveUserIDForKevlar(mDeviceID) != -1 && 
           round.GetDeathCount(Tower.GetActiveUserInstanceForKevlar(mDeviceID)) == 0 && 
           !mResetInProgress)
        {
          try
          {
            mResetInProgress = true;
            // Log the reset event
            string strLog = "در بازی " + game.ID.ToString();
            strLog += " در راند " + round.Number.ToString();
            strLog += "(" + round.ID.ToString() + ") ";
            strLog += ToString() + " ریست شد.";
            Logger.Log(strLog);

            int hp = mHP, mag = mMagazine, ammo = mAmmo;
            List<Command> cmds = new List<Command>();
            cmds.Add(CreateConfigureCommand());
            cmds.Add(DeviceBase.CreateTurnOnCommand(mDeviceID));
            cmds.Add(CreateGameStateCommand(round));
            cmds.Add(CreateCheatCommand(hp, mag, ammo));
            CommandQueue.ExecuteBatchCommands(cmds);

            if (mState != KevlarState.Working)
              Logger.Log("ولی روشن نشد.");
            else if (mHP != hp || mMagazine != mag || mAmmo != ammo)
              Logger.Log("ولی تنظیمات سلامتی، خشاب و تیر انجام نشد.");

            // Inform tower of the device reset.
            Tower.SignalDeviceReset(this);
          }
          catch (Exception ex)
          {
            Logger.LogException(ex, "Kevlar", "Processing reset request");
          }
          finally
          {
            mResetInProgress = false;
          }
        }
      }
      else if (cmd == Commands.GameState)
      { }
      else if (cmd == Commands.CheatDie)
      { }
      else if (cmd == Commands.SendUpdate)
      {
        // Update based on the given parameters
        KevlarState new_state = (KevlarState)prms[0];
        mHP = prms[1];
        mMagazine = prms[2];
        mAmmo = prms[3];

        if (new_state != mState)
        {
          KevlarState prev_state = mState;
          mState = new_state;

          if(new_state == KevlarState.Working)
          {
            // A required hack. This is done because lots of times the message sent by kevlars had noisy endings.
            // And kevlar's remaining time and remaining bomb time was garbled beyond recognition.
            mRemainingTime = mRoundTime;
            mRemainingBombTime = mBombTime;
          }
          else if (new_state == KevlarState.Dead && prev_state == KevlarState.Working)
          {
            // Send shut up to the kevlar to stop screaming!
            Command stopcmd = new Command(Commands.StopUpdate, mDeviceID);
            CommandQueue.SendCommand(stopcmd);

            // Signal the tower that a player is dead.
            DeviceID killer = (DeviceID)prms[4];
            Tower.SignalPlayerDied(killer, mDeviceID);
          }
        }
      }
      else if (cmd == Commands.SendMessage)
      { }
      else if (cmd == Commands.CheatPlayer)
      { }
      else if (cmd == Commands.Frag)
      { }
      else
        return false;

      return true;
    }
    #endregion Message Processing


    #region Commands
    protected override Command CreateConfigureCommand()
    {
      Command cmd = new Command(Commands.ConfigurePlayer, mDeviceID);
      cmd.AddParameter(mStartHP, 2);
      cmd.AddParameter(mStartMagazine, 2);
      cmd.AddParameter(mAmmoPerMagazine, 3);
      cmd.AddParameter(mFrag, 2);
      cmd.AddParameter(mDeathCount, 2);
      cmd.AddParameter(mRoundTime, 3);
      cmd.AddParameter(mBombTime, 3);
      cmd.AddParameter(mFriendlyFire ? 1 : 0, 1);
      cmd.AddParameter(mBlueScore, 2);
      cmd.AddParameter(mGreenScore, 2);
      cmd.AddParameter(Properties.Settings.Default.TimeToReturnToBase, 2);
      return cmd;
    }

    private Command CreateCheatCommand(int hp, int mag, int ammo)
    {
      Command cmd = new Command(Commands.CheatPlayer, mDeviceID);
      cmd.AddParameter(Math.Min(hp, mStartHP), 2);
      cmd.AddParameter(mag, 2);
      cmd.AddParameter(ammo, 3);
      return cmd;
    }

    /// <summary>
    /// Forcefully sets hp, magazine and ammo of the kevlar to the given values.
    /// </summary>
    public bool Cheat(int hp, int mag, int ammo)
    {
      if(mState != KevlarState.Working)
        return false;

      Command cmd = CreateCheatCommand(hp, mag, ammo);
      ExecuteCommand(cmd);
      CommandQueue.EnsureQueueIsSent();
      if (cmd.State == CommandStatus.Processed)
      {
        mHP = hp;
        mMagazine = mag;
        mAmmo = ammo;
      }

      return cmd.State == CommandStatus.Processed;
    }

    public void FakeDeath(int opp_number)
    {
      DeviceID opp_id = Team == TeamType.Blue ? DeviceID.KevlarGreen1 + (opp_number - 1) : DeviceID.KevlarBlue1 + (opp_number - 1);
      opp_number = Kevlars.NumberFromID(opp_id);
      Command cmd = new Command(Commands.CheatDie, mDeviceID);
      cmd.AddParameter(opp_number, 2);
      ExecuteCommand(cmd);
      CommandQueue.EnsureQueueIsSent();
    }

    public void IncreaseFrag(int count)
    {
      mFrag += count;
      Command cmd = new Command(Commands.Frag, mDeviceID);
      cmd.AddParameter(mFrag, 2);
      ExecuteCommand(cmd);
      CommandQueue.EnsureQueueIsSent();
    }

    public void SendMessage(string line1, string line2 = "")
    {
      Command cmd = new Command(Commands.SendMessage, mDeviceID);
      cmd.AppendMessage(line1, line2);
      CommandQueue.SendCommand(cmd);
    }

    private Command CreateGameStateCommand(DB.Round round)
    {
      Command rep = new Command(Commands.GameState, mDeviceID);
      rep.AddParameter(round.RemainingTime, 3);
      rep.AddParameter(round.RemainingBombTime, 3);
      return rep;
    }
    #endregion Commands


    #region Utilities
    public void ResetScore()
    {
      mDeathCount = 0;
      mFrag = 0;
    }

    public void ManualStateUpdate(KevlarState ks)
    {
      mState = ks;
      Tower.SignalDeviceUpdated(mDeviceID);
    }

    public void IncreaseDeaths()
    {
      ++mDeathCount;
    }
    #endregion Utilities
  }
}
