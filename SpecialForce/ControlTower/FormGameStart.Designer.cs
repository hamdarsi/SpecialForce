﻿namespace SpecialForce.ControlTower
{
  partial class FormGameStart
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGameStart));
      this.btnStart = new SpecialForce.ToolButton();
      this.tbBlueUsers = new SpecialForce.ToolBar();
      this.btnBomb = new SpecialForce.ToolButton();
      this.lblBlueTeam = new SpecialForce.TransparentLabel();
      this.tbGreenUsers = new SpecialForce.ToolBar();
      this.lblGreenTeam = new SpecialForce.TransparentLabel();
      this.pnlIcons = new SpecialForce.ToolBar();
      this.btnAbort = new SpecialForce.ToolButton();
      this.mnuKevlarIcon = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mnuRemoveKevlar = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuSendMessage = new System.Windows.Forms.ToolStripMenuItem();
      this.tmRest = new System.Windows.Forms.Timer(this.components);
      this.tbGameSettings = new SpecialForce.ToolBar();
      this.cmbFriendlyFire = new System.Windows.Forms.ComboBox();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.txtRestTime = new System.Windows.Forms.NumericUpDown();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.transparentLabel6 = new SpecialForce.TransparentLabel();
      this.cmbGameObject = new System.Windows.Forms.ComboBox();
      this.txtBombTime = new System.Windows.Forms.NumericUpDown();
      this.lblBombTime = new SpecialForce.TransparentLabel();
      this.txtRoundCount = new System.Windows.Forms.NumericUpDown();
      this.txtRoundTime = new System.Windows.Forms.NumericUpDown();
      this.txtStartMagazine = new System.Windows.Forms.NumericUpDown();
      this.txtAmmoPerMagazine = new System.Windows.Forms.NumericUpDown();
      this.txtStartHP = new System.Windows.Forms.NumericUpDown();
      this.transparentLabel8 = new SpecialForce.TransparentLabel();
      this.transparentLabel9 = new SpecialForce.TransparentLabel();
      this.transparentLabel10 = new SpecialForce.TransparentLabel();
      this.transparentLabel11 = new SpecialForce.TransparentLabel();
      this.transparentLabel12 = new SpecialForce.TransparentLabel();
      this.btnSendMessage = new SpecialForce.ToolButton();
      this.tbBlueUsers.SuspendLayout();
      this.tbGreenUsers.SuspendLayout();
      this.mnuKevlarIcon.SuspendLayout();
      this.tbGameSettings.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtRestTime)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtBombTime)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtRoundCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtRoundTime)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtStartMagazine)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtAmmoPerMagazine)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtStartHP)).BeginInit();
      this.SuspendLayout();
      // 
      // btnStart
      // 
      this.btnStart.Enabled = false;
      this.btnStart.Hint = "شروع مسابقه";
      this.btnStart.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnStart.Image")));
      this.btnStart.Location = new System.Drawing.Point(207, 13);
      this.btnStart.Name = "btnStart";
      this.btnStart.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnStart.Size = new System.Drawing.Size(64, 64);
      this.btnStart.TabIndex = 36;
      this.btnStart.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnStart.Texts")));
      this.btnStart.TransparentColor = System.Drawing.Color.White;
      this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
      // 
      // tbBlueUsers
      // 
      this.tbBlueUsers.AllowDrop = true;
      this.tbBlueUsers.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbBlueUsers.BorderWidth = 0.5F;
      this.tbBlueUsers.Controls.Add(this.btnBomb);
      this.tbBlueUsers.Controls.Add(this.lblBlueTeam);
      this.tbBlueUsers.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbBlueUsers.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbBlueUsers.Location = new System.Drawing.Point(278, 13);
      this.tbBlueUsers.Name = "tbBlueUsers";
      this.tbBlueUsers.Size = new System.Drawing.Size(684, 158);
      this.tbBlueUsers.TabIndex = 44;
      this.tbBlueUsers.DragDrop += new System.Windows.Forms.DragEventHandler(this.tbBlueUsers_DragDrop);
      this.tbBlueUsers.DragEnter += new System.Windows.Forms.DragEventHandler(this.tbBlueUsers_DragEnter);
      // 
      // btnBomb
      // 
      this.btnBomb.Hint = "برای تغییر تیم تروریست مکان بمب را عوض کنید";
      this.btnBomb.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnBomb.Image")));
      this.btnBomb.Location = new System.Drawing.Point(617, 3);
      this.btnBomb.Name = "btnBomb";
      this.btnBomb.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnBomb.ShowBorder = false;
      this.btnBomb.Size = new System.Drawing.Size(64, 64);
      this.btnBomb.TabIndex = 46;
      this.btnBomb.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnBomb.Texts")));
      this.btnBomb.TransparentColor = System.Drawing.Color.Transparent;
      this.btnBomb.OnDragStart += new System.EventHandler<SpecialForce.DragObjectRequestArgs>(this.btnBomb_OnDragStart);
      // 
      // lblBlueTeam
      // 
      this.lblBlueTeam.FixFromRight = true;
      this.lblBlueTeam.Font = new System.Drawing.Font("B Nazanin", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblBlueTeam.Location = new System.Drawing.Point(509, 3);
      this.lblBlueTeam.Name = "lblBlueTeam";
      this.lblBlueTeam.Size = new System.Drawing.Size(82, 31);
      this.lblBlueTeam.TabIndex = 45;
      this.lblBlueTeam.TabStop = false;
      this.lblBlueTeam.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBlueTeam.Texts")));
      // 
      // tbGreenUsers
      // 
      this.tbGreenUsers.AllowDrop = true;
      this.tbGreenUsers.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbGreenUsers.BorderWidth = 0.5F;
      this.tbGreenUsers.Controls.Add(this.lblGreenTeam);
      this.tbGreenUsers.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbGreenUsers.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbGreenUsers.Location = new System.Drawing.Point(278, 177);
      this.tbGreenUsers.Name = "tbGreenUsers";
      this.tbGreenUsers.Size = new System.Drawing.Size(684, 158);
      this.tbGreenUsers.TabIndex = 45;
      this.tbGreenUsers.DragDrop += new System.Windows.Forms.DragEventHandler(this.tbBlueUsers_DragDrop);
      this.tbGreenUsers.DragEnter += new System.Windows.Forms.DragEventHandler(this.tbBlueUsers_DragEnter);
      // 
      // lblGreenTeam
      // 
      this.lblGreenTeam.FixFromRight = true;
      this.lblGreenTeam.Font = new System.Drawing.Font("B Nazanin", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblGreenTeam.Location = new System.Drawing.Point(509, 3);
      this.lblGreenTeam.Name = "lblGreenTeam";
      this.lblGreenTeam.Size = new System.Drawing.Size(85, 31);
      this.lblGreenTeam.TabIndex = 45;
      this.lblGreenTeam.TabStop = false;
      this.lblGreenTeam.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblGreenTeam.Texts")));
      // 
      // pnlIcons
      // 
      this.pnlIcons.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlIcons.BorderWidth = 0.5F;
      this.pnlIcons.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlIcons.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlIcons.Location = new System.Drawing.Point(12, 341);
      this.pnlIcons.Name = "pnlIcons";
      this.pnlIcons.Size = new System.Drawing.Size(950, 275);
      this.pnlIcons.TabIndex = 46;
      // 
      // btnAbort
      // 
      this.btnAbort.Hint = "جلوگیری از اجرای مسابقه";
      this.btnAbort.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnAbort.Image")));
      this.btnAbort.Location = new System.Drawing.Point(137, 13);
      this.btnAbort.Name = "btnAbort";
      this.btnAbort.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnAbort.Size = new System.Drawing.Size(64, 64);
      this.btnAbort.TabIndex = 37;
      this.btnAbort.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnAbort.Texts")));
      this.btnAbort.TransparentColor = System.Drawing.Color.White;
      this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
      // 
      // mnuKevlarIcon
      // 
      this.mnuKevlarIcon.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuRemoveKevlar,
            this.mnuSendMessage});
      this.mnuKevlarIcon.Name = "mnuKevlarIcon";
      this.mnuKevlarIcon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      this.mnuKevlarIcon.Size = new System.Drawing.Size(171, 48);
      // 
      // mnuRemoveKevlar
      // 
      this.mnuRemoveKevlar.Name = "mnuRemoveKevlar";
      this.mnuRemoveKevlar.Size = new System.Drawing.Size(170, 22);
      this.mnuRemoveKevlar.Text = "گرفتن لباس از بازیکن";
      this.mnuRemoveKevlar.Click += new System.EventHandler(this.mnuRemoveKevlar_Click);
      // 
      // mnuSendMessage
      // 
      this.mnuSendMessage.Name = "mnuSendMessage";
      this.mnuSendMessage.Size = new System.Drawing.Size(170, 22);
      this.mnuSendMessage.Text = "فرستادن پیام";
      this.mnuSendMessage.Click += new System.EventHandler(this.mnuSendMessage_Click);
      // 
      // tmRest
      // 
      this.tmRest.Enabled = true;
      this.tmRest.Interval = 1000;
      this.tmRest.Tick += new System.EventHandler(this.tmRest_Tick);
      // 
      // tbGameSettings
      // 
      this.tbGameSettings.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbGameSettings.BorderWidth = 0.5F;
      this.tbGameSettings.Controls.Add(this.cmbFriendlyFire);
      this.tbGameSettings.Controls.Add(this.transparentLabel2);
      this.tbGameSettings.Controls.Add(this.txtRestTime);
      this.tbGameSettings.Controls.Add(this.transparentLabel1);
      this.tbGameSettings.Controls.Add(this.transparentLabel6);
      this.tbGameSettings.Controls.Add(this.cmbGameObject);
      this.tbGameSettings.Controls.Add(this.txtBombTime);
      this.tbGameSettings.Controls.Add(this.lblBombTime);
      this.tbGameSettings.Controls.Add(this.txtRoundCount);
      this.tbGameSettings.Controls.Add(this.txtRoundTime);
      this.tbGameSettings.Controls.Add(this.txtStartMagazine);
      this.tbGameSettings.Controls.Add(this.txtAmmoPerMagazine);
      this.tbGameSettings.Controls.Add(this.txtStartHP);
      this.tbGameSettings.Controls.Add(this.transparentLabel8);
      this.tbGameSettings.Controls.Add(this.transparentLabel9);
      this.tbGameSettings.Controls.Add(this.transparentLabel10);
      this.tbGameSettings.Controls.Add(this.transparentLabel11);
      this.tbGameSettings.Controls.Add(this.transparentLabel12);
      this.tbGameSettings.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbGameSettings.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbGameSettings.Location = new System.Drawing.Point(12, 83);
      this.tbGameSettings.Name = "tbGameSettings";
      this.tbGameSettings.Size = new System.Drawing.Size(259, 252);
      this.tbGameSettings.TabIndex = 47;
      // 
      // cmbFriendlyFire
      // 
      this.cmbFriendlyFire.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbFriendlyFire.FormattingEnabled = true;
      this.cmbFriendlyFire.Items.AddRange(new object[] {
            "بله",
            "خیر"});
      this.cmbFriendlyFire.Location = new System.Drawing.Point(14, 216);
      this.cmbFriendlyFire.Name = "cmbFriendlyFire";
      this.cmbFriendlyFire.Size = new System.Drawing.Size(56, 21);
      this.cmbFriendlyFire.TabIndex = 39;
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Location = new System.Drawing.Point(179, 220);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(64, 15);
      this.transparentLabel2.TabIndex = 38;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // txtRestTime
      // 
      this.txtRestTime.Location = new System.Drawing.Point(14, 191);
      this.txtRestTime.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
      this.txtRestTime.Name = "txtRestTime";
      this.txtRestTime.Size = new System.Drawing.Size(56, 21);
      this.txtRestTime.TabIndex = 37;
      this.txtRestTime.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Location = new System.Drawing.Point(167, 195);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(75, 15);
      this.transparentLabel1.TabIndex = 36;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // transparentLabel6
      // 
      this.transparentLabel6.Location = new System.Drawing.Point(194, 20);
      this.transparentLabel6.Name = "transparentLabel6";
      this.transparentLabel6.Size = new System.Drawing.Size(47, 15);
      this.transparentLabel6.TabIndex = 35;
      this.transparentLabel6.TabStop = false;
      this.transparentLabel6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel6.Texts")));
      // 
      // cmbGameObject
      // 
      this.cmbGameObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbGameObject.FormattingEnabled = true;
      this.cmbGameObject.Location = new System.Drawing.Point(14, 16);
      this.cmbGameObject.Name = "cmbGameObject";
      this.cmbGameObject.Size = new System.Drawing.Size(103, 21);
      this.cmbGameObject.TabIndex = 34;
      this.cmbGameObject.SelectedIndexChanged += new System.EventHandler(this.cmbGameObject_SelectedIndexChanged);
      // 
      // txtBombTime
      // 
      this.txtBombTime.Enabled = false;
      this.txtBombTime.Location = new System.Drawing.Point(14, 166);
      this.txtBombTime.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
      this.txtBombTime.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
      this.txtBombTime.Name = "txtBombTime";
      this.txtBombTime.Size = new System.Drawing.Size(56, 21);
      this.txtBombTime.TabIndex = 33;
      this.txtBombTime.Value = new decimal(new int[] {
            180,
            0,
            0,
            0});
      // 
      // lblBombTime
      // 
      this.lblBombTime.Enabled = false;
      this.lblBombTime.Location = new System.Drawing.Point(159, 170);
      this.lblBombTime.Name = "lblBombTime";
      this.lblBombTime.Size = new System.Drawing.Size(83, 15);
      this.lblBombTime.TabIndex = 32;
      this.lblBombTime.TabStop = false;
      this.lblBombTime.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBombTime.Texts")));
      // 
      // txtRoundCount
      // 
      this.txtRoundCount.Location = new System.Drawing.Point(14, 41);
      this.txtRoundCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtRoundCount.Name = "txtRoundCount";
      this.txtRoundCount.Size = new System.Drawing.Size(56, 21);
      this.txtRoundCount.TabIndex = 31;
      this.txtRoundCount.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
      // 
      // txtRoundTime
      // 
      this.txtRoundTime.Location = new System.Drawing.Point(14, 66);
      this.txtRoundTime.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
      this.txtRoundTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtRoundTime.Name = "txtRoundTime";
      this.txtRoundTime.Size = new System.Drawing.Size(56, 21);
      this.txtRoundTime.TabIndex = 30;
      this.txtRoundTime.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
      // 
      // txtStartMagazine
      // 
      this.txtStartMagazine.Location = new System.Drawing.Point(14, 116);
      this.txtStartMagazine.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
      this.txtStartMagazine.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtStartMagazine.Name = "txtStartMagazine";
      this.txtStartMagazine.Size = new System.Drawing.Size(56, 21);
      this.txtStartMagazine.TabIndex = 29;
      this.txtStartMagazine.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
      // 
      // txtAmmoPerMagazine
      // 
      this.txtAmmoPerMagazine.Location = new System.Drawing.Point(14, 141);
      this.txtAmmoPerMagazine.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
      this.txtAmmoPerMagazine.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtAmmoPerMagazine.Name = "txtAmmoPerMagazine";
      this.txtAmmoPerMagazine.Size = new System.Drawing.Size(56, 21);
      this.txtAmmoPerMagazine.TabIndex = 28;
      this.txtAmmoPerMagazine.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
      // 
      // txtStartHP
      // 
      this.txtStartHP.Increment = new decimal(new int[] {
            3,
            0,
            0,
            0});
      this.txtStartHP.Location = new System.Drawing.Point(14, 91);
      this.txtStartHP.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.txtStartHP.Name = "txtStartHP";
      this.txtStartHP.Size = new System.Drawing.Size(56, 21);
      this.txtStartHP.TabIndex = 27;
      this.txtStartHP.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
      // 
      // transparentLabel8
      // 
      this.transparentLabel8.Location = new System.Drawing.Point(161, 95);
      this.transparentLabel8.Name = "transparentLabel8";
      this.transparentLabel8.Size = new System.Drawing.Size(82, 15);
      this.transparentLabel8.TabIndex = 26;
      this.transparentLabel8.TabStop = false;
      this.transparentLabel8.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel8.Texts")));
      // 
      // transparentLabel9
      // 
      this.transparentLabel9.Location = new System.Drawing.Point(181, 45);
      this.transparentLabel9.Name = "transparentLabel9";
      this.transparentLabel9.Size = new System.Drawing.Size(61, 15);
      this.transparentLabel9.TabIndex = 25;
      this.transparentLabel9.TabStop = false;
      this.transparentLabel9.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel9.Texts")));
      // 
      // transparentLabel10
      // 
      this.transparentLabel10.Location = new System.Drawing.Point(154, 70);
      this.transparentLabel10.Name = "transparentLabel10";
      this.transparentLabel10.Size = new System.Drawing.Size(89, 15);
      this.transparentLabel10.TabIndex = 24;
      this.transparentLabel10.TabStop = false;
      this.transparentLabel10.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel10.Texts")));
      // 
      // transparentLabel11
      // 
      this.transparentLabel11.Location = new System.Drawing.Point(137, 145);
      this.transparentLabel11.Name = "transparentLabel11";
      this.transparentLabel11.Size = new System.Drawing.Size(106, 15);
      this.transparentLabel11.TabIndex = 23;
      this.transparentLabel11.TabStop = false;
      this.transparentLabel11.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel11.Texts")));
      // 
      // transparentLabel12
      // 
      this.transparentLabel12.Location = new System.Drawing.Point(162, 120);
      this.transparentLabel12.Name = "transparentLabel12";
      this.transparentLabel12.Size = new System.Drawing.Size(81, 15);
      this.transparentLabel12.TabIndex = 22;
      this.transparentLabel12.TabStop = false;
      this.transparentLabel12.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel12.Texts")));
      // 
      // btnSendMessage
      // 
      this.btnSendMessage.Hint = "فرستادن پیام به همه لباس ها";
      this.btnSendMessage.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnSendMessage.Image")));
      this.btnSendMessage.Location = new System.Drawing.Point(13, 13);
      this.btnSendMessage.Name = "btnSendMessage";
      this.btnSendMessage.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnSendMessage.Size = new System.Drawing.Size(64, 64);
      this.btnSendMessage.TabIndex = 48;
      this.btnSendMessage.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnSendMessage.Texts")));
      this.btnSendMessage.TransparentColor = System.Drawing.Color.White;
      this.btnSendMessage.Click += new System.EventHandler(this.btnSendMessage_Click);
      // 
      // FormGameStart
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(978, 627);
      this.Controls.Add(this.pnlIcons);
      this.Controls.Add(this.tbGameSettings);
      this.Controls.Add(this.tbGreenUsers);
      this.Controls.Add(this.btnSendMessage);
      this.Controls.Add(this.tbBlueUsers);
      this.Controls.Add(this.btnAbort);
      this.Controls.Add(this.btnStart);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormGameStart";
      this.Text = "تنظیمات شروع بازی";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormGameStart_FormClosing);
      this.Load += new System.EventHandler(this.FormGameStart_Load);
      this.tbBlueUsers.ResumeLayout(false);
      this.tbGreenUsers.ResumeLayout(false);
      this.mnuKevlarIcon.ResumeLayout(false);
      this.tbGameSettings.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.txtRestTime)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtBombTime)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtRoundCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtRoundTime)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtStartMagazine)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtAmmoPerMagazine)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.txtStartHP)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private ToolBar tbBlueUsers;
    private TransparentLabel lblBlueTeam;
    private ToolBar tbGreenUsers;
    private TransparentLabel lblGreenTeam;
    private ToolBar pnlIcons;
    private ToolButton btnStart;
    private ToolButton btnAbort;
    private ToolButton btnBomb;
    private System.Windows.Forms.ContextMenuStrip mnuKevlarIcon;
    private System.Windows.Forms.ToolStripMenuItem mnuRemoveKevlar;
    private System.Windows.Forms.Timer tmRest;
    private ToolBar tbGameSettings;
    private System.Windows.Forms.ComboBox cmbFriendlyFire;
    private TransparentLabel transparentLabel2;
    private System.Windows.Forms.NumericUpDown txtRestTime;
    private TransparentLabel transparentLabel1;
    private TransparentLabel transparentLabel6;
    private System.Windows.Forms.ComboBox cmbGameObject;
    private System.Windows.Forms.NumericUpDown txtBombTime;
    private TransparentLabel lblBombTime;
    private System.Windows.Forms.NumericUpDown txtRoundCount;
    private System.Windows.Forms.NumericUpDown txtRoundTime;
    private System.Windows.Forms.NumericUpDown txtStartMagazine;
    private System.Windows.Forms.NumericUpDown txtAmmoPerMagazine;
    private System.Windows.Forms.NumericUpDown txtStartHP;
    private TransparentLabel transparentLabel8;
    private TransparentLabel transparentLabel9;
    private TransparentLabel transparentLabel10;
    private TransparentLabel transparentLabel11;
    private TransparentLabel transparentLabel12;
    private ToolButton btnSendMessage;
    private System.Windows.Forms.ToolStripMenuItem mnuSendMessage;
  }
}