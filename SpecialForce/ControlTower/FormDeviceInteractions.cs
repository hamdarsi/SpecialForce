﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SpecialForce.ControlTower
{
  public partial class FormDeviceInteractions : SessionAwareForm
  {
    private List<Interaction> mActions = new List<Interaction>();

    // Singleton instance
    static private FormDeviceInteractions mInstance = null;
    static public FormDeviceInteractions Instance
    {
      get
      {
        if (mInstance == null)
          mInstance = new FormDeviceInteractions();

        return mInstance;
      }
    }

    private FormDeviceInteractions()
    {
      InitializeComponent();
    }

    static private void Destruct()
    {
      if (mInstance != null && mInstance.IsDisposed == false)
      {
        mInstance.Close();
        mInstance.Dispose();
        mInstance = null;
      }
    }

    private void FormDeviceGroupUI_Load(object sender, EventArgs e)
    {
      if (DB.GameManager.CurrentGame == null)
        Logger.Log("(FormDeviceGroupUI) Shown for nothing");
    }

    private void FormDeviceGroupUI_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (e.CloseReason == CloseReason.UserClosing)
      {
        e.Cancel = true;
        ControlBox = false;
        Hide();
      }
    }

    public void AbortInteractions(string strError, DeviceBase dev)
    {
      if (IsDisposed)
        return;

      ControlBox = true;
      ShowInteraction(strError, dev, DeviceStateUI.Error);
      string msg = dev != null ? dev.ToString() : "";
      msg = msg + ' ' + strError;
      DB.GameManager.AbortCurrentRound(msg);
    }

    public void ShowInteraction(string msg, DeviceBase dev, DeviceStateUI state, int tries = 0)
    {
      if (IsDisposed)
        return;

      if(state == DeviceStateUI.Commiting && tries > 1)
        msg = msg + (tries == 2 ? " - بار دوم" : " - بار سوم");

      interactions.ShowInteraction(dev != null ? dev.ID : DeviceID.AllDevices, msg, state);
    }

    public void ApplyInteractions()
    {
      mActions.Clear();
      DB.Game game = DB.GameManager.CurrentGame;
      DB.Round round = game.CurrentRound;
      List<DeviceID> ids = Tower.GetAllDeviceIDs();

      // Players
      // Configure list
      for (int i = 0; i < ids.Count; ++i)
        if (Kevlars.IsKevlar(ids[i]))
        {
          bool valid = round.PlayerKevlars.ContainsValue(Kevlars.NumberFromID(ids[i]));
          mActions.Add(new Interaction(this, Interactions.ConfigureKevlar, ids[i], valid));
        }

      // Turn on
      mActions.Add(new Interaction(this, Interactions.TurnOn, DeviceID.AllDevices, true));

      // Check list
      for (int i = 0; i < ids.Count; ++i)
        if (Kevlars.IsKevlar(ids[i]))
        {
          bool on = round.PlayerKevlars.ContainsValue(Kevlars.NumberFromID(ids[i]));
          mActions.Add(new Interaction(this, on ? Interactions.CheckOn : Interactions.CheckOff, ids[i], true));
        }

      // Bombs
      if (game.Object == GameObject.Bombing)
        mActions.Add(new Interaction(this, Interactions.ConfigureBomb, DeviceID.AllDevices, true));
      else
        for (int i = 0; i < ids.Count; ++i)
          if (Kevlars.IsBomb(ids[i]))
            mActions.Add(new Interaction(this, Interactions.CheckOff, ids[i], true));

      // Show the window
      Show();
      Left = Screen.PrimaryScreen.WorkingArea.Left + 10;
      Top = Screen.PrimaryScreen.WorkingArea.Bottom - Height - 10;

      // Start execution if application is running in release configuration or a radio transmitter is working
      interactions.ResetInteractions();
      if(!Session.ReleaseBuild && !ControlTower.CommandQueue.IsOpen)
        mActions.Clear();
      PostExecuteRequest();
    }

    public void StopInteractions()
    {
      mActions.Clear();
      Hide();
    }

    private void PostExecuteRequest()
    {
      Session.Synchronize(new Action(Application.DoEvents));
      Session.Synchronize(new Action(ExecuteInteraction));
    }

    private void ExecuteInteraction()
    {
      if (IsDisposed)
        return;

      // Finished?
      if (mActions.Count == 0)
      {
        Hide();
        return;
      }

      // Update title based on current action
      if (mActions[0].Type == Interactions.ConfigureKevlar)
        lblTitle.Text = "در حال تنظیم دستگاه ها";
      else if (mActions[0].Type == Interactions.TurnOn)
        lblTitle.Text = "در حال شروع راند";
      else
        lblTitle.Text = "در حال چک دستگاه ها";

      if (mActions[0].Execute())
        PostExecuteRequest();

      // Reset the states if execution stage is changed.
      if (mActions.Count > 1 && mActions[1].Type != mActions[0].Type)
      {
        bool reset = true;
        if(mActions[0].Type == Interactions.CheckOn && mActions[1].Type == Interactions.CheckOff)
          reset = false;
        else if(mActions[0].Type == Interactions.CheckOff && mActions[1].Type == Interactions.CheckOn)
          reset = false;

        if(reset)
          interactions.ResetInteractions();
      }

      // Remove the processed actions if action list is not already cleared
      if(mActions.Count > 0)
        mActions.RemoveAt(0);
    }
  }
}
