﻿using System;
using System.IO.Ports;
using System.Collections.Generic;

namespace SpecialForce
{
  public class Command
  {
    #region Properties
    private PersianDateTime mCreationTime;
    public PersianDateTime CreationTime
    {
      get { return mCreationTime; }
    }

    private PersianDateTime mLastTryTime = null;
    public PersianDateTime LastTryTime
    {
      get { return mLastTryTime; }
    }

    private Commands mCommand;
    public Commands Type
    {
      get { return mCommand; }
    }

    private string mCommandString;
    public string CommandString
    {
      get { return mCommandString; }
    }

    private CommandStatus mState = CommandStatus.Unknown;
    public CommandStatus State
    {
      get { return mState; }
      set { mState = value; }
    }

    private DeviceID mRecipient;
    public DeviceID Recipient
    {
      get { return mRecipient; }
    }

    private DeviceLocation mPreferedLocation = DeviceLocation.Center;
    public DeviceLocation PreferedLocation
    {
      get { return mPreferedLocation; }
      set { mPreferedLocation = value; }
    }

    private int mTryCount = 0;
    public int TryCount
    {
      get { return mTryCount; }
    }
    #endregion Properties


    #region Constructor
    public Command(Commands cmd, DeviceID recipient)
    {
      mCommand = cmd;
      mRecipient = recipient;
      mCreationTime = PersianDateTime.Now;

      // prefix with recipient's name
      if (mRecipient == DeviceID.Bomb1)
        mCommandString = "B1|";
      else if (mRecipient == DeviceID.Bomb2)
        mCommandString = "B2|";
      else if (mRecipient == DeviceID.AllDevices)
        mCommandString = "All";
      else
        mCommandString = ((int)mRecipient).ToString("D2") + "|";

      mCommandString += ToString(cmd);
    }
    #endregion Constructor


    #region Command Utilities
    public void AddParameter(int param, int precision)
    {
      mCommandString += '|';

      // In case the parameter is negative, it needs a '-' character
      if (param < 0)
        --precision;

      mCommandString += param.ToString("D" + precision.ToString());
    }

    static public string CorrectMessage(string str)
    {
      if (str.Length > 16)
        return str.Remove(16);

      string strEmpty = "";
      for (int i = str.Length; i < 16; ++i)
        strEmpty += ' ';

      return str + strEmpty;
    }

    public void AppendMessage(string line1, string line2)
    {
      mCommandString += '|';
      mCommandString += CorrectMessage(line1);
      mCommandString += CorrectMessage(line2);
    }

    public void Send(SerialPort port)
    {
      ++mTryCount;
      mLastTryTime = PersianDateTime.Now;
      mState = CommandStatus.Sent;
      port.WriteLine(mCommandString);
    }
    #endregion Command Utilities


    #region String Conversions
    public static string ToString(Commands cmd)
    {
      switch (cmd)
      {
        case Commands.Ping: return "Ping";

        case Commands.State: return "State";
        case Commands.ConfigurePlayer: return "Config";
        case Commands.ConfigureParams: return "Params";
        case Commands.GameState: return "GameState";
        case Commands.SendUpdate: return "Update";
        case Commands.StopUpdate: return "Stop";
        case Commands.SendMessage: return "Msg";
        case Commands.Frag: return "Frag";

        case Commands.ConfigureBomb: return "BConf";
        case Commands.BombUpdate: return "BCheck";
        case Commands.BombPlantBroadcast: return "BombPlanted";
        case Commands.PlaySound: return "Play";

        case Commands.CheatPlayer: return "Cheat";
        case Commands.CheatDie: return "Die";
        case Commands.CheatBomb: return "Hack";

        default:
          return "";
      }
    }

    public static Commands FromString(string str)
    {
      if (str == "Pong")
        return Commands.Ping;

      else if (str.StartsWith("StateSet"))
        return Commands.State;

      else if (str == "ReqGameState")
        return Commands.RequestGameState;

      else if (str == "GameStateDone")
        return Commands.GameState;

      else if (str == "ConfigDone")
        return Commands.ConfigurePlayer;

      else if (str == "ParamsDone")
        return Commands.ConfigureParams;

      else if (str.StartsWith("Updated"))
        return Commands.SendUpdate;

      else if (str == "MsgDone")
        return Commands.SendMessage;

      else if (str == "FragDone")
        return Commands.Frag;

      else if (str == "BConfDone")
        return Commands.ConfigureBomb;

      else if (str.StartsWith("BombInfo"))
        return Commands.BombUpdate;

      else if (str == "BombRoger")
        return Commands.BombPlantBroadcast;

      else if (str == "PlayDone")
        return Commands.PlaySound;

      else if (str == "CheatDone")
        return Commands.CheatPlayer;

      else if (str == "FakedDeath")
        return Commands.CheatDie;

      else if (str == "HackDone")
        return Commands.CheatBomb;

      else
        return Commands.Unknown;
    }

    public static string ToString(TeamType tt)
    {
      return tt == TeamType.Blue ? "آبی" : "سبز";
    }
    #endregion String Conversions
  }
}
