﻿namespace SpecialForce.ControlTower
{
  partial class FormSendMessage
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSendMessage));
      this.lblDevice = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.tbAdmin = new SpecialForce.ToolBar();
      this.transparentLabel4 = new SpecialForce.TransparentLabel();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.lblMessageLine2 = new SpecialForce.TransparentLabel();
      this.transparentLabel9 = new SpecialForce.TransparentLabel();
      this.transparentLabel10 = new SpecialForce.TransparentLabel();
      this.txtMessageLine2 = new System.Windows.Forms.TextBox();
      this.lblMessageLine1 = new SpecialForce.TransparentLabel();
      this.transparentLabel6 = new SpecialForce.TransparentLabel();
      this.transparentLabel5 = new SpecialForce.TransparentLabel();
      this.txtMessageLine1 = new System.Windows.Forms.TextBox();
      this.btnOK = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.lbMessages = new System.Windows.Forms.ListBox();
      this.transparentLabel15 = new SpecialForce.TransparentLabel();
      this.tbAdmin.SuspendLayout();
      this.SuspendLayout();
      // 
      // lblDevice
      // 
      this.lblDevice.FixFromRight = true;
      this.lblDevice.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblDevice.Location = new System.Drawing.Point(224, 12);
      this.lblDevice.Name = "lblDevice";
      this.lblDevice.Size = new System.Drawing.Size(56, 26);
      this.lblDevice.TabIndex = 0;
      this.lblDevice.TabStop = false;
      this.lblDevice.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblDevice.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.FixFromRight = true;
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.Location = new System.Drawing.Point(177, 12);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(41, 26);
      this.transparentLabel1.TabIndex = 1;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // tbAdmin
      // 
      this.tbAdmin.AllowDrop = true;
      this.tbAdmin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.tbAdmin.BorderWidth = 0.5F;
      this.tbAdmin.Controls.Add(this.transparentLabel4);
      this.tbAdmin.Controls.Add(this.transparentLabel3);
      this.tbAdmin.Controls.Add(this.lblMessageLine2);
      this.tbAdmin.Controls.Add(this.transparentLabel9);
      this.tbAdmin.Controls.Add(this.transparentLabel10);
      this.tbAdmin.Controls.Add(this.txtMessageLine2);
      this.tbAdmin.Controls.Add(this.lblMessageLine1);
      this.tbAdmin.Controls.Add(this.transparentLabel6);
      this.tbAdmin.Controls.Add(this.transparentLabel5);
      this.tbAdmin.Controls.Add(this.txtMessageLine1);
      this.tbAdmin.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.tbAdmin.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbAdmin.Location = new System.Drawing.Point(181, 44);
      this.tbAdmin.Name = "tbAdmin";
      this.tbAdmin.Size = new System.Drawing.Size(373, 175);
      this.tbAdmin.TabIndex = 46;
      // 
      // transparentLabel4
      // 
      this.transparentLabel4.FixFromRight = true;
      this.transparentLabel4.Font = new System.Drawing.Font("DejaVu Sans Mono", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel4.Location = new System.Drawing.Point(3, 143);
      this.transparentLabel4.Name = "transparentLabel4";
      this.transparentLabel4.Size = new System.Drawing.Size(352, 14);
      this.transparentLabel4.TabIndex = 55;
      this.transparentLabel4.TabStop = false;
      this.transparentLabel4.TextAlign = SpecialForce.Alignment.MiddleLeft;
      this.transparentLabel4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel4.Texts")));
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.FixFromRight = true;
      this.transparentLabel3.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel3.Location = new System.Drawing.Point(44, 121);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(331, 21);
      this.transparentLabel3.TabIndex = 54;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // lblMessageLine2
      // 
      this.lblMessageLine2.FixFromRight = true;
      this.lblMessageLine2.Font = new System.Drawing.Font("DejaVu Sans Mono", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblMessageLine2.Location = new System.Drawing.Point(124, 87);
      this.lblMessageLine2.Name = "lblMessageLine2";
      this.lblMessageLine2.RightToLeft = false;
      this.lblMessageLine2.Size = new System.Drawing.Size(113, 14);
      this.lblMessageLine2.TabIndex = 53;
      this.lblMessageLine2.TabStop = false;
      this.lblMessageLine2.TextAlign = SpecialForce.Alignment.MiddleLeft;
      this.lblMessageLine2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblMessageLine2.Texts")));
      // 
      // transparentLabel9
      // 
      this.transparentLabel9.FixFromRight = true;
      this.transparentLabel9.Font = new System.Drawing.Font("B Nazanin", 9.75F);
      this.transparentLabel9.Location = new System.Drawing.Point(243, 83);
      this.transparentLabel9.Name = "transparentLabel9";
      this.transparentLabel9.Size = new System.Drawing.Size(110, 21);
      this.transparentLabel9.TabIndex = 52;
      this.transparentLabel9.TabStop = false;
      this.transparentLabel9.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel9.Texts")));
      // 
      // transparentLabel10
      // 
      this.transparentLabel10.FixFromRight = true;
      this.transparentLabel10.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel10.Location = new System.Drawing.Point(325, 61);
      this.transparentLabel10.Name = "transparentLabel10";
      this.transparentLabel10.Size = new System.Drawing.Size(39, 26);
      this.transparentLabel10.TabIndex = 51;
      this.transparentLabel10.TabStop = false;
      this.transparentLabel10.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel10.Texts")));
      // 
      // txtMessageLine2
      // 
      this.txtMessageLine2.Location = new System.Drawing.Point(219, 62);
      this.txtMessageLine2.Name = "txtMessageLine2";
      this.txtMessageLine2.Size = new System.Drawing.Size(100, 21);
      this.txtMessageLine2.TabIndex = 50;
      this.txtMessageLine2.TextChanged += new System.EventHandler(this.txtMessageLine2_TextChanged);
      // 
      // lblMessageLine1
      // 
      this.lblMessageLine1.FixFromRight = true;
      this.lblMessageLine1.Font = new System.Drawing.Font("DejaVu Sans Mono", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblMessageLine1.Location = new System.Drawing.Point(199, 35);
      this.lblMessageLine1.Name = "lblMessageLine1";
      this.lblMessageLine1.RightToLeft = false;
      this.lblMessageLine1.Size = new System.Drawing.Size(38, 14);
      this.lblMessageLine1.TabIndex = 49;
      this.lblMessageLine1.TabStop = false;
      this.lblMessageLine1.TextAlign = SpecialForce.Alignment.MiddleLeft;
      this.lblMessageLine1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblMessageLine1.Texts")));
      // 
      // transparentLabel6
      // 
      this.transparentLabel6.FixFromRight = true;
      this.transparentLabel6.Font = new System.Drawing.Font("B Nazanin", 9.75F);
      this.transparentLabel6.Location = new System.Drawing.Point(243, 31);
      this.transparentLabel6.Name = "transparentLabel6";
      this.transparentLabel6.Size = new System.Drawing.Size(110, 21);
      this.transparentLabel6.TabIndex = 48;
      this.transparentLabel6.TabStop = false;
      this.transparentLabel6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel6.Texts")));
      // 
      // transparentLabel5
      // 
      this.transparentLabel5.FixFromRight = true;
      this.transparentLabel5.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel5.Location = new System.Drawing.Point(325, 9);
      this.transparentLabel5.Name = "transparentLabel5";
      this.transparentLabel5.Size = new System.Drawing.Size(39, 26);
      this.transparentLabel5.TabIndex = 47;
      this.transparentLabel5.TabStop = false;
      this.transparentLabel5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel5.Texts")));
      // 
      // txtMessageLine1
      // 
      this.txtMessageLine1.Location = new System.Drawing.Point(219, 10);
      this.txtMessageLine1.Name = "txtMessageLine1";
      this.txtMessageLine1.Size = new System.Drawing.Size(100, 21);
      this.txtMessageLine1.TabIndex = 0;
      this.txtMessageLine1.TextChanged += new System.EventHandler(this.txtMessageLine1_TextChanged);
      // 
      // btnOK
      // 
      this.btnOK.ForeColor = System.Drawing.Color.Black;
      this.btnOK.Location = new System.Drawing.Point(479, 230);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 47;
      this.btnOK.Text = "ارسال";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.ForeColor = System.Drawing.Color.Black;
      this.btnCancel.Location = new System.Drawing.Point(398, 230);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 48;
      this.btnCancel.Text = "انصراف";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // lbMessages
      // 
      this.lbMessages.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lbMessages.FormattingEnabled = true;
      this.lbMessages.ItemHeight = 23;
      this.lbMessages.Location = new System.Drawing.Point(12, 46);
      this.lbMessages.Name = "lbMessages";
      this.lbMessages.Size = new System.Drawing.Size(154, 165);
      this.lbMessages.TabIndex = 48;
      this.lbMessages.SelectedIndexChanged += new System.EventHandler(this.lbMessages_SelectedIndexChanged);
      // 
      // transparentLabel15
      // 
      this.transparentLabel15.FixFromRight = true;
      this.transparentLabel15.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel15.Location = new System.Drawing.Point(12, 12);
      this.transparentLabel15.Name = "transparentLabel15";
      this.transparentLabel15.Size = new System.Drawing.Size(39, 26);
      this.transparentLabel15.TabIndex = 47;
      this.transparentLabel15.TabStop = false;
      this.transparentLabel15.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel15.Texts")));
      // 
      // FormSendMessage
      // 
      this.AcceptButton = this.btnOK;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(569, 268);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOK);
      this.Controls.Add(this.tbAdmin);
      this.Controls.Add(this.transparentLabel15);
      this.Controls.Add(this.transparentLabel1);
      this.Controls.Add(this.lbMessages);
      this.Controls.Add(this.lblDevice);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "FormSendMessage";
      this.ShowInTaskbar = false;
      this.Text = "ارسال پیام";
      this.TopMost = true;
      this.Load += new System.EventHandler(this.FormSendMessage_Load);
      this.tbAdmin.ResumeLayout(false);
      this.tbAdmin.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private TransparentLabel lblDevice;
    private TransparentLabel transparentLabel1;
    private ToolBar tbAdmin;
    private TransparentLabel lblMessageLine2;
    private TransparentLabel transparentLabel9;
    private TransparentLabel transparentLabel10;
    private System.Windows.Forms.TextBox txtMessageLine2;
    private TransparentLabel lblMessageLine1;
    private TransparentLabel transparentLabel6;
    private TransparentLabel transparentLabel5;
    private System.Windows.Forms.TextBox txtMessageLine1;
    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.Button btnCancel;
    private TransparentLabel transparentLabel4;
    private TransparentLabel transparentLabel3;
    private System.Windows.Forms.ListBox lbMessages;
    private TransparentLabel transparentLabel15;
  }
}