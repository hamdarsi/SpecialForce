﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.ControlTower
{
  public partial class KevlarIcon : ToolButton
  {
    private const int PreferredWidth = 90;
    private const int PreferredHeight = 120;

    private Kevlar mKevlar = null;
    public Kevlar Kevlar
    {
      get { return mKevlar; }
      set 
      {
        if (mKevlar != null)
          mKevlar.OnPlayerAssigned -= UpdateIcon;

        mKevlar = value;

        if (mKevlar != null) 
          mKevlar.OnPlayerAssigned += UpdateIcon;

        UpdateIcon();
      }
    }

    public DB.User Player
    {
      get { return mKevlar != null ? mKevlar.Player : null; }
    }

    private bool mBlueKevlar;
    public bool BlueKevlar
    {
      get 
      {
        if (mKevlar != null)
          return mKevlar.Team == TeamType.Blue;
        else
          return mBlueKevlar;
      }
      set { mBlueKevlar = value; }
    }

    public KevlarIcon()
    {
      InitializeComponent();
    }

    protected override void OnHandleCreated(EventArgs e)
    {
      base.OnHandleCreated(e);
      Tower.OnDeviceUpdated += DeviceUpdated;
    }

    protected override void OnHandleDestroyed(EventArgs e)
    {
      base.OnHandleDestroyed(e);

      Tower.OnDeviceUpdated -= DeviceUpdated;
      if (mKevlar != null)
        mKevlar.OnPlayerAssigned -= UpdateIcon;
    }

    private Brush SelectBrush(int value, int max)
    {
      double percent = (float)value / max;
      if (percent > 0.67)
        return Brushes.Green;
      else if (percent > 0.34)
        return Brushes.Yellow;
      else
        return Brushes.Red;
    }

    private void UpdateIcon(Object sender = null, EventArgs e = null)
    {
      if (Session.DevelopMode)
        return;

      string strHint = "";
      if (mKevlar != null)
      {
        int num = mKevlar.Number > Kevlars.CountForTeams ? mKevlar.Number - Kevlars.CountForTeams : mKevlar.Number;
        strHint = "لباس شماره " + num.ToString() + "\r\n";
      }

      Bitmap bmp;
      Font number_font = new Font("Arial", 26.0f, FontStyle.Bold);
      if (mKevlar == null || mKevlar.Player == null)
      {
        if (BlueKevlar)
          bmp = Properties.Resources.ControlTower_No_Player_For_Blue;
        else
          bmp = Properties.Resources.ControlTower_No_Player_For_Green;

        // Draw kevlar number on the icon
        if (mKevlar != null)
        {
          Graphics c = Graphics.FromImage(bmp);
          c.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
          c.DrawString(mKevlar.TeamNumber.ToString(), number_font, Brushes.Yellow, 0, 0);
        }

        Image = bmp;
        Hint = strHint + "بدون بازیکن";
        return;
      }

      int w = PreferredWidth;
      int h = PreferredHeight;
      bmp = new Bitmap(w, h);
      Graphics canvas = Graphics.FromImage(bmp);
      canvas.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;

      if (mKevlar.State == KevlarState.Dead)
      {
        canvas.DrawGrayScale(mKevlar.Player.Photo.Resize(w, h));
        canvas.DrawImageUnscaled(0, 0, Properties.Resources.ControlTower_Cross);
      }
      else if (mKevlar.State == KevlarState.Disabled)
        canvas.DrawGrayScale(mKevlar.Player.Photo.Resize(w, h));
      else
      {
        Font font = new Font("Arial", 12.0f, FontStyle.Bold);
        canvas.DrawImageUnscaled(0, 0, mKevlar.Player.Photo.Resize(w, h));
        Image img = Properties.Resources.ControlTower_Player_Is_Playing;
        canvas.DrawImage(img, w - img.Width, 0);
      }

      canvas.DrawString(mKevlar.TeamNumber.ToString(), number_font, Brushes.Yellow, 0, 0);
      Image = bmp;

      Hint = strHint + mKevlar.Player.FullName;
    }

    private void DeviceUpdated(Object sender, DeviceEventArgs e)
    {
      if (mKevlar == null)
        return;
      else if (mKevlar.Player == null)
        return;
      else if (mKevlar.ID != e.ID)
        return;

      UpdateIcon();
    }
  }
}
