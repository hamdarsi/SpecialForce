﻿namespace SpecialForce.ControlTower
{
  partial class ControlPanel
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPanel));
      this.tinypicker1 = new SpecialForce.TinyPicker();
      this.label9 = new SpecialForce.TransparentLabel();
      this.transparentLabel1 = new SpecialForce.TransparentLabel();
      this.pnlRoundInfo = new SpecialForce.ToolBar();
      this.lblRoundNumber = new SpecialForce.TransparentLabel();
      this.transparentLabel16 = new SpecialForce.TransparentLabel();
      this.lblRemainingBombTime = new SpecialForce.TransparentLabel();
      this.transparentLabel15 = new SpecialForce.TransparentLabel();
      this.lblBombState = new SpecialForce.TransparentLabel();
      this.transparentLabel2 = new SpecialForce.TransparentLabel();
      this.btnNextRound = new SpecialForce.ToolButton();
      this.btnPrevRound = new SpecialForce.ToolButton();
      this.lblBlueTeam = new SpecialForce.TransparentLabel();
      this.lblGreenTeam = new SpecialForce.TransparentLabel();
      this.lblRoundState = new SpecialForce.TransparentLabel();
      this.label17 = new SpecialForce.TransparentLabel();
      this.lblBombDiffuser = new SpecialForce.TransparentLabel();
      this.label13 = new SpecialForce.TransparentLabel();
      this.lblGameType = new SpecialForce.TransparentLabel();
      this.label10 = new SpecialForce.TransparentLabel();
      this.lblGameObject = new SpecialForce.TransparentLabel();
      this.transparentLabel3 = new SpecialForce.TransparentLabel();
      this.lblRoundIndex = new SpecialForce.TransparentLabel();
      this.lblGameResult = new SpecialForce.TransparentLabel();
      this.label12 = new SpecialForce.TransparentLabel();
      this.lblRemainingTime = new SpecialForce.TransparentLabel();
      this.label8 = new SpecialForce.TransparentLabel();
      this.lblBombPlanter = new SpecialForce.TransparentLabel();
      this.transparentLabel4 = new SpecialForce.TransparentLabel();
      this.transparentLabel5 = new SpecialForce.TransparentLabel();
      this.transparentLabel6 = new SpecialForce.TransparentLabel();
      this.transparentLabel7 = new SpecialForce.TransparentLabel();
      this.btnKevlarPane = new SpecialForce.ToolButton();
      this.pnlKevlarInfo = new SpecialForce.ToolBar();
      this.lblLastTimeOutCommand = new SpecialForce.TransparentLabel();
      this.transparentLabel13 = new SpecialForce.TransparentLabel();
      this.lblTimeOutCommandCount = new SpecialForce.TransparentLabel();
      this.transparentLabel12 = new SpecialForce.TransparentLabel();
      this.lblLastErrorMessage = new SpecialForce.TransparentLabel();
      this.lblMessageErrorCount = new SpecialForce.TransparentLabel();
      this.lblLastMessage = new SpecialForce.TransparentLabel();
      this.lblLastCommand = new SpecialForce.TransparentLabel();
      this.lblStatus = new SpecialForce.TransparentLabel();
      this.lblPing = new SpecialForce.TransparentLabel();
      this.label25 = new SpecialForce.TransparentLabel();
      this.label24 = new SpecialForce.TransparentLabel();
      this.label23 = new SpecialForce.TransparentLabel();
      this.label22 = new SpecialForce.TransparentLabel();
      this.label20 = new SpecialForce.TransparentLabel();
      this.label21 = new SpecialForce.TransparentLabel();
      this.tbUserPhoto = new SpecialForce.ToolButton();
      this.lblAmmo = new SpecialForce.TransparentLabel();
      this.lblMagazine = new SpecialForce.TransparentLabel();
      this.lblHP = new SpecialForce.TransparentLabel();
      this.transparentLabel8 = new SpecialForce.TransparentLabel();
      this.transparentLabel9 = new SpecialForce.TransparentLabel();
      this.transparentLabel10 = new SpecialForce.TransparentLabel();
      this.lblTeamName = new SpecialForce.TransparentLabel();
      this.transparentLabel11 = new SpecialForce.TransparentLabel();
      this.lblDeathCount = new SpecialForce.TransparentLabel();
      this.lblKillCount = new SpecialForce.TransparentLabel();
      this.lblFrag = new SpecialForce.TransparentLabel();
      this.lblNumber = new SpecialForce.TransparentLabel();
      this.lblUserName = new SpecialForce.TransparentLabel();
      this.label35 = new SpecialForce.TransparentLabel();
      this.label34 = new SpecialForce.TransparentLabel();
      this.label32 = new SpecialForce.TransparentLabel();
      this.lblTeam = new SpecialForce.TransparentLabel();
      this.label30 = new SpecialForce.TransparentLabel();
      this.label28 = new SpecialForce.TransparentLabel();
      this.label26 = new SpecialForce.TransparentLabel();
      this.btnRoundPane = new SpecialForce.ToolButton();
      this.kevlarIcon1 = new SpecialForce.ControlTower.KevlarIcon();
      this.kevlarIcon2 = new SpecialForce.ControlTower.KevlarIcon();
      this.kevlarIcon3 = new SpecialForce.ControlTower.KevlarIcon();
      this.kevlarIcon4 = new SpecialForce.ControlTower.KevlarIcon();
      this.kevlarIcon5 = new SpecialForce.ControlTower.KevlarIcon();
      this.kevlarIcon6 = new SpecialForce.ControlTower.KevlarIcon();
      this.kevlarIcon7 = new SpecialForce.ControlTower.KevlarIcon();
      this.kevlarIcon8 = new SpecialForce.ControlTower.KevlarIcon();
      this.kevlarIcon9 = new SpecialForce.ControlTower.KevlarIcon();
      this.kevlarIcon10 = new SpecialForce.ControlTower.KevlarIcon();
      this.kevlarIcon11 = new SpecialForce.ControlTower.KevlarIcon();
      this.kevlarIcon12 = new SpecialForce.ControlTower.KevlarIcon();
      this.kevlarIcon13 = new SpecialForce.ControlTower.KevlarIcon();
      this.kevlarIcon14 = new SpecialForce.ControlTower.KevlarIcon();
      this.pnlCheat = new System.Windows.Forms.Panel();
      this.btnCloseCheat = new SpecialForce.ToolButton();
      this.txtCheatAmmo = new SpecialForce.Utilities.NumberBox();
      this.txtCheatMag = new SpecialForce.Utilities.NumberBox();
      this.txtCheatHP = new SpecialForce.Utilities.NumberBox();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.tmRoundTime = new System.Windows.Forms.Timer(this.components);
      this.mnuGameActions = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mnuAbortRound = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuAbortGame = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuSendMessage = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuPingAll = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuUpdate = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuCheat = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuFakeKill = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuBombPlant = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuBombDiffuse = new System.Windows.Forms.ToolStripMenuItem();
      this.mnuBombExplode = new System.Windows.Forms.ToolStripMenuItem();
      this.pnlPlayerSelection = new System.Windows.Forms.Panel();
      this.btnCloseCheatKill = new SpecialForce.ToolButton();
      this.cmbPlayer = new System.Windows.Forms.ComboBox();
      this.label6 = new System.Windows.Forms.Label();
      this.pnlRoundInfo.SuspendLayout();
      this.pnlKevlarInfo.SuspendLayout();
      this.pnlCheat.SuspendLayout();
      this.mnuGameActions.SuspendLayout();
      this.pnlPlayerSelection.SuspendLayout();
      this.SuspendLayout();
      // 
      // tinypicker1
      // 
      this.tinypicker1.Location = new System.Drawing.Point(12, 436);
      this.tinypicker1.Name = "tinypicker1";
      this.tinypicker1.Size = new System.Drawing.Size(598, 30);
      this.tinypicker1.TabIndex = 2;
      this.tinypicker1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tinypicker1.Texts")));
      // 
      // label9
      // 
      this.label9.Font = new System.Drawing.Font("B Nazanin", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label9.ForeColor = System.Drawing.Color.White;
      this.label9.Location = new System.Drawing.Point(353, 61);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(60, 34);
      this.label9.TabIndex = 42;
      this.label9.TabStop = false;
      this.label9.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label9.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label9.Texts")));
      // 
      // transparentLabel1
      // 
      this.transparentLabel1.Font = new System.Drawing.Font("B Nazanin", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel1.ForeColor = System.Drawing.Color.White;
      this.transparentLabel1.Location = new System.Drawing.Point(216, 61);
      this.transparentLabel1.Name = "transparentLabel1";
      this.transparentLabel1.Size = new System.Drawing.Size(57, 34);
      this.transparentLabel1.TabIndex = 53;
      this.transparentLabel1.TabStop = false;
      this.transparentLabel1.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel1.Texts")));
      // 
      // pnlRoundInfo
      // 
      this.pnlRoundInfo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlRoundInfo.BorderWidth = 0.5F;
      this.pnlRoundInfo.Controls.Add(this.lblRoundNumber);
      this.pnlRoundInfo.Controls.Add(this.transparentLabel16);
      this.pnlRoundInfo.Controls.Add(this.lblRemainingBombTime);
      this.pnlRoundInfo.Controls.Add(this.transparentLabel15);
      this.pnlRoundInfo.Controls.Add(this.lblBombState);
      this.pnlRoundInfo.Controls.Add(this.transparentLabel2);
      this.pnlRoundInfo.Controls.Add(this.btnNextRound);
      this.pnlRoundInfo.Controls.Add(this.btnPrevRound);
      this.pnlRoundInfo.Controls.Add(this.lblBlueTeam);
      this.pnlRoundInfo.Controls.Add(this.lblGreenTeam);
      this.pnlRoundInfo.Controls.Add(this.lblRoundState);
      this.pnlRoundInfo.Controls.Add(this.label17);
      this.pnlRoundInfo.Controls.Add(this.lblBombDiffuser);
      this.pnlRoundInfo.Controls.Add(this.label13);
      this.pnlRoundInfo.Controls.Add(this.lblGameType);
      this.pnlRoundInfo.Controls.Add(this.label10);
      this.pnlRoundInfo.Controls.Add(this.lblGameObject);
      this.pnlRoundInfo.Controls.Add(this.transparentLabel3);
      this.pnlRoundInfo.Controls.Add(this.lblRoundIndex);
      this.pnlRoundInfo.Controls.Add(this.lblGameResult);
      this.pnlRoundInfo.Controls.Add(this.label12);
      this.pnlRoundInfo.Controls.Add(this.lblRemainingTime);
      this.pnlRoundInfo.Controls.Add(this.label8);
      this.pnlRoundInfo.Controls.Add(this.lblBombPlanter);
      this.pnlRoundInfo.Controls.Add(this.transparentLabel4);
      this.pnlRoundInfo.Controls.Add(this.transparentLabel5);
      this.pnlRoundInfo.Controls.Add(this.transparentLabel6);
      this.pnlRoundInfo.Controls.Add(this.transparentLabel7);
      this.pnlRoundInfo.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlRoundInfo.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlRoundInfo.Location = new System.Drawing.Point(652, 23);
      this.pnlRoundInfo.Name = "pnlRoundInfo";
      this.pnlRoundInfo.Size = new System.Drawing.Size(308, 400);
      this.pnlRoundInfo.TabIndex = 58;
      // 
      // lblRoundNumber
      // 
      this.lblRoundNumber.FixFromRight = true;
      this.lblRoundNumber.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblRoundNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblRoundNumber.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.lblRoundNumber.Location = new System.Drawing.Point(135, 129);
      this.lblRoundNumber.Name = "lblRoundNumber";
      this.lblRoundNumber.Size = new System.Drawing.Size(12, 26);
      this.lblRoundNumber.TabIndex = 78;
      this.lblRoundNumber.TabStop = false;
      this.lblRoundNumber.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblRoundNumber.Texts")));
      // 
      // transparentLabel16
      // 
      this.transparentLabel16.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel16.Location = new System.Drawing.Point(230, 129);
      this.transparentLabel16.Name = "transparentLabel16";
      this.transparentLabel16.Size = new System.Drawing.Size(60, 26);
      this.transparentLabel16.TabIndex = 77;
      this.transparentLabel16.TabStop = false;
      this.transparentLabel16.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel16.Texts")));
      // 
      // lblRemainingBombTime
      // 
      this.lblRemainingBombTime.FixFromRight = true;
      this.lblRemainingBombTime.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblRemainingBombTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblRemainingBombTime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.lblRemainingBombTime.Location = new System.Drawing.Point(135, 241);
      this.lblRemainingBombTime.Name = "lblRemainingBombTime";
      this.lblRemainingBombTime.Size = new System.Drawing.Size(12, 26);
      this.lblRemainingBombTime.TabIndex = 76;
      this.lblRemainingBombTime.TabStop = false;
      this.lblRemainingBombTime.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblRemainingBombTime.Texts")));
      // 
      // transparentLabel15
      // 
      this.transparentLabel15.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel15.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.transparentLabel15.Location = new System.Drawing.Point(186, 241);
      this.transparentLabel15.Name = "transparentLabel15";
      this.transparentLabel15.Size = new System.Drawing.Size(104, 26);
      this.transparentLabel15.TabIndex = 75;
      this.transparentLabel15.TabStop = false;
      this.transparentLabel15.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel15.Texts")));
      // 
      // lblBombState
      // 
      this.lblBombState.FixFromRight = true;
      this.lblBombState.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblBombState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblBombState.Location = new System.Drawing.Point(135, 185);
      this.lblBombState.Name = "lblBombState";
      this.lblBombState.Size = new System.Drawing.Size(12, 26);
      this.lblBombState.TabIndex = 74;
      this.lblBombState.TabStop = false;
      this.lblBombState.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBombState.Texts")));
      // 
      // transparentLabel2
      // 
      this.transparentLabel2.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.transparentLabel2.Location = new System.Drawing.Point(218, 185);
      this.transparentLabel2.Name = "transparentLabel2";
      this.transparentLabel2.Size = new System.Drawing.Size(72, 26);
      this.transparentLabel2.TabIndex = 73;
      this.transparentLabel2.TabStop = false;
      this.transparentLabel2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel2.Texts")));
      // 
      // btnNextRound
      // 
      this.btnNextRound.Enabled = false;
      this.btnNextRound.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnNextRound.Hint = "اجرای بعدی";
      this.btnNextRound.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnNextRound.Image")));
      this.btnNextRound.Location = new System.Drawing.Point(194, 8);
      this.btnNextRound.Name = "btnNextRound";
      this.btnNextRound.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnNextRound.ShowBorder = false;
      this.btnNextRound.Size = new System.Drawing.Size(24, 24);
      this.btnNextRound.TabIndex = 72;
      this.btnNextRound.TabStop = false;
      this.btnNextRound.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnNextRound.Texts")));
      this.btnNextRound.TransparentColor = System.Drawing.Color.White;
      this.btnNextRound.Click += new System.EventHandler(this.btnNextRound_Click);
      // 
      // btnPrevRound
      // 
      this.btnPrevRound.Enabled = false;
      this.btnPrevRound.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.btnPrevRound.Hint = "اجرای قبلی";
      this.btnPrevRound.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnPrevRound.Image")));
      this.btnPrevRound.Location = new System.Drawing.Point(81, 8);
      this.btnPrevRound.Name = "btnPrevRound";
      this.btnPrevRound.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnPrevRound.ShowBorder = false;
      this.btnPrevRound.Size = new System.Drawing.Size(24, 24);
      this.btnPrevRound.TabIndex = 71;
      this.btnPrevRound.TabStop = false;
      this.btnPrevRound.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnPrevRound.Texts")));
      this.btnPrevRound.TransparentColor = System.Drawing.Color.White;
      this.btnPrevRound.Click += new System.EventHandler(this.btnPreviousRound_Click);
      // 
      // lblBlueTeam
      // 
      this.lblBlueTeam.FixFromRight = true;
      this.lblBlueTeam.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblBlueTeam.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblBlueTeam.Location = new System.Drawing.Point(135, 297);
      this.lblBlueTeam.Name = "lblBlueTeam";
      this.lblBlueTeam.Size = new System.Drawing.Size(12, 26);
      this.lblBlueTeam.TabIndex = 70;
      this.lblBlueTeam.TabStop = false;
      this.lblBlueTeam.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBlueTeam.Texts")));
      // 
      // lblGreenTeam
      // 
      this.lblGreenTeam.FixFromRight = true;
      this.lblGreenTeam.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblGreenTeam.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblGreenTeam.Location = new System.Drawing.Point(135, 269);
      this.lblGreenTeam.Name = "lblGreenTeam";
      this.lblGreenTeam.Size = new System.Drawing.Size(12, 26);
      this.lblGreenTeam.TabIndex = 69;
      this.lblGreenTeam.TabStop = false;
      this.lblGreenTeam.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblGreenTeam.Texts")));
      // 
      // lblRoundState
      // 
      this.lblRoundState.FixFromRight = true;
      this.lblRoundState.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblRoundState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblRoundState.Location = new System.Drawing.Point(135, 157);
      this.lblRoundState.Name = "lblRoundState";
      this.lblRoundState.Size = new System.Drawing.Size(12, 26);
      this.lblRoundState.TabIndex = 68;
      this.lblRoundState.TabStop = false;
      this.lblRoundState.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblRoundState.Texts")));
      // 
      // label17
      // 
      this.label17.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label17.Location = new System.Drawing.Point(222, 157);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(68, 26);
      this.label17.TabIndex = 67;
      this.label17.TabStop = false;
      this.label17.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label17.Texts")));
      // 
      // lblBombDiffuser
      // 
      this.lblBombDiffuser.FixFromRight = true;
      this.lblBombDiffuser.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblBombDiffuser.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblBombDiffuser.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.lblBombDiffuser.Location = new System.Drawing.Point(135, 353);
      this.lblBombDiffuser.Name = "lblBombDiffuser";
      this.lblBombDiffuser.Size = new System.Drawing.Size(12, 26);
      this.lblBombDiffuser.TabIndex = 66;
      this.lblBombDiffuser.TabStop = false;
      this.lblBombDiffuser.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBombDiffuser.Texts")));
      // 
      // label13
      // 
      this.label13.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label13.Location = new System.Drawing.Point(220, 353);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(70, 26);
      this.label13.TabIndex = 65;
      this.label13.TabStop = false;
      this.label13.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label13.Texts")));
      // 
      // lblGameType
      // 
      this.lblGameType.FixFromRight = true;
      this.lblGameType.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblGameType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblGameType.Location = new System.Drawing.Point(135, 45);
      this.lblGameType.Name = "lblGameType";
      this.lblGameType.Size = new System.Drawing.Size(12, 26);
      this.lblGameType.TabIndex = 64;
      this.lblGameType.TabStop = false;
      this.lblGameType.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblGameType.Texts")));
      // 
      // label10
      // 
      this.label10.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label10.Location = new System.Drawing.Point(239, 45);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(51, 26);
      this.label10.TabIndex = 63;
      this.label10.TabStop = false;
      this.label10.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label10.Texts")));
      // 
      // lblGameObject
      // 
      this.lblGameObject.FixFromRight = true;
      this.lblGameObject.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblGameObject.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblGameObject.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.lblGameObject.Location = new System.Drawing.Point(135, 73);
      this.lblGameObject.Name = "lblGameObject";
      this.lblGameObject.Size = new System.Drawing.Size(12, 26);
      this.lblGameObject.TabIndex = 60;
      this.lblGameObject.TabStop = false;
      this.lblGameObject.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblGameObject.Texts")));
      // 
      // transparentLabel3
      // 
      this.transparentLabel3.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.transparentLabel3.Location = new System.Drawing.Point(230, 73);
      this.transparentLabel3.Name = "transparentLabel3";
      this.transparentLabel3.Size = new System.Drawing.Size(60, 26);
      this.transparentLabel3.TabIndex = 59;
      this.transparentLabel3.TabStop = false;
      this.transparentLabel3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel3.Texts")));
      // 
      // lblRoundIndex
      // 
      this.lblRoundIndex.FixFromRight = true;
      this.lblRoundIndex.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblRoundIndex.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblRoundIndex.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.lblRoundIndex.Location = new System.Drawing.Point(126, 8);
      this.lblRoundIndex.Name = "lblRoundIndex";
      this.lblRoundIndex.Size = new System.Drawing.Size(21, 26);
      this.lblRoundIndex.TabIndex = 58;
      this.lblRoundIndex.TabStop = false;
      this.lblRoundIndex.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblRoundIndex.Texts")));
      // 
      // lblGameResult
      // 
      this.lblGameResult.FixFromRight = true;
      this.lblGameResult.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblGameResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblGameResult.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.lblGameResult.Location = new System.Drawing.Point(135, 101);
      this.lblGameResult.Name = "lblGameResult";
      this.lblGameResult.Size = new System.Drawing.Size(12, 26);
      this.lblGameResult.TabIndex = 57;
      this.lblGameResult.TabStop = false;
      this.lblGameResult.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblGameResult.Texts")));
      // 
      // label12
      // 
      this.label12.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.label12.Location = new System.Drawing.Point(203, 101);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(87, 26);
      this.label12.TabIndex = 56;
      this.label12.TabStop = false;
      this.label12.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label12.Texts")));
      // 
      // lblRemainingTime
      // 
      this.lblRemainingTime.FixFromRight = true;
      this.lblRemainingTime.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblRemainingTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblRemainingTime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.lblRemainingTime.Location = new System.Drawing.Point(135, 213);
      this.lblRemainingTime.Name = "lblRemainingTime";
      this.lblRemainingTime.Size = new System.Drawing.Size(12, 26);
      this.lblRemainingTime.TabIndex = 55;
      this.lblRemainingTime.TabStop = false;
      this.lblRemainingTime.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblRemainingTime.Texts")));
      // 
      // label8
      // 
      this.label8.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.label8.Location = new System.Drawing.Point(190, 213);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(100, 26);
      this.label8.TabIndex = 54;
      this.label8.TabStop = false;
      this.label8.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label8.Texts")));
      // 
      // lblBombPlanter
      // 
      this.lblBombPlanter.FixFromRight = true;
      this.lblBombPlanter.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblBombPlanter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblBombPlanter.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.lblBombPlanter.Location = new System.Drawing.Point(135, 325);
      this.lblBombPlanter.Name = "lblBombPlanter";
      this.lblBombPlanter.Size = new System.Drawing.Size(12, 26);
      this.lblBombPlanter.TabIndex = 53;
      this.lblBombPlanter.TabStop = false;
      this.lblBombPlanter.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblBombPlanter.Texts")));
      // 
      // transparentLabel4
      // 
      this.transparentLabel4.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.transparentLabel4.Location = new System.Drawing.Point(234, 325);
      this.transparentLabel4.Name = "transparentLabel4";
      this.transparentLabel4.Size = new System.Drawing.Size(56, 26);
      this.transparentLabel4.TabIndex = 52;
      this.transparentLabel4.TabStop = false;
      this.transparentLabel4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel4.Texts")));
      // 
      // transparentLabel5
      // 
      this.transparentLabel5.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.transparentLabel5.Location = new System.Drawing.Point(151, 8);
      this.transparentLabel5.Name = "transparentLabel5";
      this.transparentLabel5.Size = new System.Drawing.Size(27, 26);
      this.transparentLabel5.TabIndex = 51;
      this.transparentLabel5.TabStop = false;
      this.transparentLabel5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel5.Texts")));
      // 
      // transparentLabel6
      // 
      this.transparentLabel6.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.transparentLabel6.Location = new System.Drawing.Point(244, 297);
      this.transparentLabel6.Name = "transparentLabel6";
      this.transparentLabel6.Size = new System.Drawing.Size(46, 26);
      this.transparentLabel6.TabIndex = 50;
      this.transparentLabel6.TabStop = false;
      this.transparentLabel6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel6.Texts")));
      // 
      // transparentLabel7
      // 
      this.transparentLabel7.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.transparentLabel7.Location = new System.Drawing.Point(241, 269);
      this.transparentLabel7.Name = "transparentLabel7";
      this.transparentLabel7.Size = new System.Drawing.Size(49, 26);
      this.transparentLabel7.TabIndex = 49;
      this.transparentLabel7.TabStop = false;
      this.transparentLabel7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel7.Texts")));
      // 
      // btnKevlarPane
      // 
      this.btnKevlarPane.Hint = "نمایش اطلاعات بازیکن";
      this.btnKevlarPane.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnKevlarPane.Image")));
      this.btnKevlarPane.Location = new System.Drawing.Point(966, 192);
      this.btnKevlarPane.Name = "btnKevlarPane";
      this.btnKevlarPane.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnKevlarPane.ShowBorder = false;
      this.btnKevlarPane.Size = new System.Drawing.Size(30, 55);
      this.btnKevlarPane.TabIndex = 74;
      this.btnKevlarPane.TabStop = false;
      this.btnKevlarPane.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnKevlarPane.Texts")));
      this.btnKevlarPane.TransparentColor = System.Drawing.Color.White;
      this.btnKevlarPane.Click += new System.EventHandler(this.btnKevlarPane_Click);
      // 
      // pnlKevlarInfo
      // 
      this.pnlKevlarInfo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlKevlarInfo.BorderWidth = 0.5F;
      this.pnlKevlarInfo.Controls.Add(this.lblLastTimeOutCommand);
      this.pnlKevlarInfo.Controls.Add(this.transparentLabel13);
      this.pnlKevlarInfo.Controls.Add(this.lblTimeOutCommandCount);
      this.pnlKevlarInfo.Controls.Add(this.transparentLabel12);
      this.pnlKevlarInfo.Controls.Add(this.lblLastErrorMessage);
      this.pnlKevlarInfo.Controls.Add(this.lblMessageErrorCount);
      this.pnlKevlarInfo.Controls.Add(this.lblLastMessage);
      this.pnlKevlarInfo.Controls.Add(this.lblLastCommand);
      this.pnlKevlarInfo.Controls.Add(this.lblStatus);
      this.pnlKevlarInfo.Controls.Add(this.lblPing);
      this.pnlKevlarInfo.Controls.Add(this.label25);
      this.pnlKevlarInfo.Controls.Add(this.label24);
      this.pnlKevlarInfo.Controls.Add(this.label23);
      this.pnlKevlarInfo.Controls.Add(this.label22);
      this.pnlKevlarInfo.Controls.Add(this.label20);
      this.pnlKevlarInfo.Controls.Add(this.label21);
      this.pnlKevlarInfo.Controls.Add(this.tbUserPhoto);
      this.pnlKevlarInfo.Controls.Add(this.lblAmmo);
      this.pnlKevlarInfo.Controls.Add(this.lblMagazine);
      this.pnlKevlarInfo.Controls.Add(this.lblHP);
      this.pnlKevlarInfo.Controls.Add(this.transparentLabel8);
      this.pnlKevlarInfo.Controls.Add(this.transparentLabel9);
      this.pnlKevlarInfo.Controls.Add(this.transparentLabel10);
      this.pnlKevlarInfo.Controls.Add(this.lblTeamName);
      this.pnlKevlarInfo.Controls.Add(this.transparentLabel11);
      this.pnlKevlarInfo.Controls.Add(this.lblDeathCount);
      this.pnlKevlarInfo.Controls.Add(this.lblKillCount);
      this.pnlKevlarInfo.Controls.Add(this.lblFrag);
      this.pnlKevlarInfo.Controls.Add(this.lblNumber);
      this.pnlKevlarInfo.Controls.Add(this.lblUserName);
      this.pnlKevlarInfo.Controls.Add(this.label35);
      this.pnlKevlarInfo.Controls.Add(this.label34);
      this.pnlKevlarInfo.Controls.Add(this.label32);
      this.pnlKevlarInfo.Controls.Add(this.lblTeam);
      this.pnlKevlarInfo.Controls.Add(this.label30);
      this.pnlKevlarInfo.Controls.Add(this.label28);
      this.pnlKevlarInfo.Controls.Add(this.label26);
      this.pnlKevlarInfo.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(70)))), ((int)(((byte)(107)))));
      this.pnlKevlarInfo.GradientStartColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlKevlarInfo.Location = new System.Drawing.Point(1002, 23);
      this.pnlKevlarInfo.Name = "pnlKevlarInfo";
      this.pnlKevlarInfo.Size = new System.Drawing.Size(308, 401);
      this.pnlKevlarInfo.TabIndex = 73;
      // 
      // lblLastTimeOutCommand
      // 
      this.lblLastTimeOutCommand.Font = new System.Drawing.Font("DejaVu Sans Mono", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblLastTimeOutCommand.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblLastTimeOutCommand.Location = new System.Drawing.Point(14, 366);
      this.lblLastTimeOutCommand.Name = "lblLastTimeOutCommand";
      this.lblLastTimeOutCommand.Size = new System.Drawing.Size(11, 15);
      this.lblLastTimeOutCommand.TabIndex = 112;
      this.lblLastTimeOutCommand.TabStop = false;
      this.lblLastTimeOutCommand.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblLastTimeOutCommand.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblLastTimeOutCommand.Texts")));
      // 
      // transparentLabel13
      // 
      this.transparentLabel13.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.transparentLabel13.Location = new System.Drawing.Point(190, 360);
      this.transparentLabel13.Name = "transparentLabel13";
      this.transparentLabel13.Size = new System.Drawing.Size(108, 26);
      this.transparentLabel13.TabIndex = 111;
      this.transparentLabel13.TabStop = false;
      this.transparentLabel13.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel13.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel13.Texts")));
      // 
      // lblTimeOutCommandCount
      // 
      this.lblTimeOutCommandCount.Font = new System.Drawing.Font("DejaVu Sans Mono", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblTimeOutCommandCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblTimeOutCommandCount.Location = new System.Drawing.Point(13, 346);
      this.lblTimeOutCommandCount.Name = "lblTimeOutCommandCount";
      this.lblTimeOutCommandCount.Size = new System.Drawing.Size(11, 15);
      this.lblTimeOutCommandCount.TabIndex = 110;
      this.lblTimeOutCommandCount.TabStop = false;
      this.lblTimeOutCommandCount.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblTimeOutCommandCount.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblTimeOutCommandCount.Texts")));
      // 
      // transparentLabel12
      // 
      this.transparentLabel12.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.transparentLabel12.Location = new System.Drawing.Point(188, 338);
      this.transparentLabel12.Name = "transparentLabel12";
      this.transparentLabel12.Size = new System.Drawing.Size(110, 26);
      this.transparentLabel12.TabIndex = 109;
      this.transparentLabel12.TabStop = false;
      this.transparentLabel12.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel12.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel12.Texts")));
      // 
      // lblLastErrorMessage
      // 
      this.lblLastErrorMessage.Font = new System.Drawing.Font("DejaVu Sans Mono", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblLastErrorMessage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblLastErrorMessage.Location = new System.Drawing.Point(13, 326);
      this.lblLastErrorMessage.Name = "lblLastErrorMessage";
      this.lblLastErrorMessage.Size = new System.Drawing.Size(11, 15);
      this.lblLastErrorMessage.TabIndex = 108;
      this.lblLastErrorMessage.TabStop = false;
      this.lblLastErrorMessage.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblLastErrorMessage.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblLastErrorMessage.Texts")));
      // 
      // lblMessageErrorCount
      // 
      this.lblMessageErrorCount.Font = new System.Drawing.Font("DejaVu Sans Mono", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblMessageErrorCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblMessageErrorCount.Location = new System.Drawing.Point(13, 306);
      this.lblMessageErrorCount.Name = "lblMessageErrorCount";
      this.lblMessageErrorCount.Size = new System.Drawing.Size(11, 15);
      this.lblMessageErrorCount.TabIndex = 107;
      this.lblMessageErrorCount.TabStop = false;
      this.lblMessageErrorCount.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblMessageErrorCount.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblMessageErrorCount.Texts")));
      // 
      // lblLastMessage
      // 
      this.lblLastMessage.Font = new System.Drawing.Font("DejaVu Sans Mono", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblLastMessage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblLastMessage.Location = new System.Drawing.Point(13, 286);
      this.lblLastMessage.Name = "lblLastMessage";
      this.lblLastMessage.Size = new System.Drawing.Size(11, 15);
      this.lblLastMessage.TabIndex = 106;
      this.lblLastMessage.TabStop = false;
      this.lblLastMessage.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblLastMessage.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblLastMessage.Texts")));
      // 
      // lblLastCommand
      // 
      this.lblLastCommand.Font = new System.Drawing.Font("DejaVu Sans Mono", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblLastCommand.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblLastCommand.Location = new System.Drawing.Point(13, 266);
      this.lblLastCommand.Name = "lblLastCommand";
      this.lblLastCommand.Size = new System.Drawing.Size(11, 15);
      this.lblLastCommand.TabIndex = 105;
      this.lblLastCommand.TabStop = false;
      this.lblLastCommand.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblLastCommand.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblLastCommand.Texts")));
      // 
      // lblStatus
      // 
      this.lblStatus.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblStatus.Location = new System.Drawing.Point(13, 238);
      this.lblStatus.Name = "lblStatus";
      this.lblStatus.Size = new System.Drawing.Size(12, 26);
      this.lblStatus.TabIndex = 104;
      this.lblStatus.TabStop = false;
      this.lblStatus.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblStatus.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblStatus.Texts")));
      // 
      // lblPing
      // 
      this.lblPing.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblPing.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblPing.Location = new System.Drawing.Point(137, 218);
      this.lblPing.Name = "lblPing";
      this.lblPing.Size = new System.Drawing.Size(12, 26);
      this.lblPing.TabIndex = 102;
      this.lblPing.TabStop = false;
      this.lblPing.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblPing.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblPing.Texts")));
      // 
      // label25
      // 
      this.label25.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label25.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.label25.Location = new System.Drawing.Point(214, 318);
      this.label25.Name = "label25";
      this.label25.Size = new System.Drawing.Size(84, 26);
      this.label25.TabIndex = 101;
      this.label25.TabStop = false;
      this.label25.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label25.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label25.Texts")));
      // 
      // label24
      // 
      this.label24.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label24.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.label24.Location = new System.Drawing.Point(212, 298);
      this.label24.Name = "label24";
      this.label24.Size = new System.Drawing.Size(86, 26);
      this.label24.TabIndex = 100;
      this.label24.TabStop = false;
      this.label24.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label24.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label24.Texts")));
      // 
      // label23
      // 
      this.label23.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label23.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.label23.Location = new System.Drawing.Point(225, 278);
      this.label23.Name = "label23";
      this.label23.Size = new System.Drawing.Size(73, 26);
      this.label23.TabIndex = 99;
      this.label23.TabStop = false;
      this.label23.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label23.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label23.Texts")));
      // 
      // label22
      // 
      this.label22.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label22.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.label22.Location = new System.Drawing.Point(222, 258);
      this.label22.Name = "label22";
      this.label22.Size = new System.Drawing.Size(76, 26);
      this.label22.TabIndex = 98;
      this.label22.TabStop = false;
      this.label22.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label22.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label22.Texts")));
      // 
      // label20
      // 
      this.label20.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label20.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.label20.Location = new System.Drawing.Point(252, 238);
      this.label20.Name = "label20";
      this.label20.Size = new System.Drawing.Size(46, 26);
      this.label20.TabIndex = 97;
      this.label20.TabStop = false;
      this.label20.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label20.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label20.Texts")));
      // 
      // label21
      // 
      this.label21.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label21.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.label21.Location = new System.Drawing.Point(248, 218);
      this.label21.Name = "label21";
      this.label21.Size = new System.Drawing.Size(50, 26);
      this.label21.TabIndex = 95;
      this.label21.TabStop = false;
      this.label21.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label21.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label21.Texts")));
      // 
      // tbUserPhoto
      // 
      this.tbUserPhoto.Hint = "";
      this.tbUserPhoto.Image = ((System.Drawing.Bitmap)(resources.GetObject("tbUserPhoto.Image")));
      this.tbUserPhoto.Location = new System.Drawing.Point(13, 18);
      this.tbUserPhoto.Name = "tbUserPhoto";
      this.tbUserPhoto.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.tbUserPhoto.Size = new System.Drawing.Size(105, 140);
      this.tbUserPhoto.TabIndex = 94;
      this.tbUserPhoto.TabStop = false;
      this.tbUserPhoto.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("tbUserPhoto.Texts")));
      this.tbUserPhoto.TransparentColor = System.Drawing.Color.White;
      // 
      // lblAmmo
      // 
      this.lblAmmo.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblAmmo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblAmmo.Location = new System.Drawing.Point(137, 198);
      this.lblAmmo.Name = "lblAmmo";
      this.lblAmmo.Size = new System.Drawing.Size(12, 26);
      this.lblAmmo.TabIndex = 93;
      this.lblAmmo.TabStop = false;
      this.lblAmmo.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblAmmo.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblAmmo.Texts")));
      // 
      // lblMagazine
      // 
      this.lblMagazine.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblMagazine.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblMagazine.Location = new System.Drawing.Point(137, 178);
      this.lblMagazine.Name = "lblMagazine";
      this.lblMagazine.Size = new System.Drawing.Size(12, 26);
      this.lblMagazine.TabIndex = 92;
      this.lblMagazine.TabStop = false;
      this.lblMagazine.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblMagazine.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblMagazine.Texts")));
      // 
      // lblHP
      // 
      this.lblHP.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblHP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblHP.Location = new System.Drawing.Point(137, 158);
      this.lblHP.Name = "lblHP";
      this.lblHP.Size = new System.Drawing.Size(12, 26);
      this.lblHP.TabIndex = 91;
      this.lblHP.TabStop = false;
      this.lblHP.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblHP.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblHP.Texts")));
      // 
      // transparentLabel8
      // 
      this.transparentLabel8.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel8.Location = new System.Drawing.Point(257, 178);
      this.transparentLabel8.Name = "transparentLabel8";
      this.transparentLabel8.Size = new System.Drawing.Size(41, 26);
      this.transparentLabel8.TabIndex = 90;
      this.transparentLabel8.TabStop = false;
      this.transparentLabel8.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel8.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel8.Texts")));
      // 
      // transparentLabel9
      // 
      this.transparentLabel9.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel9.Location = new System.Drawing.Point(275, 198);
      this.transparentLabel9.Name = "transparentLabel9";
      this.transparentLabel9.Size = new System.Drawing.Size(23, 26);
      this.transparentLabel9.TabIndex = 89;
      this.transparentLabel9.TabStop = false;
      this.transparentLabel9.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel9.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel9.Texts")));
      // 
      // transparentLabel10
      // 
      this.transparentLabel10.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel10.Location = new System.Drawing.Point(252, 158);
      this.transparentLabel10.Name = "transparentLabel10";
      this.transparentLabel10.Size = new System.Drawing.Size(46, 26);
      this.transparentLabel10.TabIndex = 88;
      this.transparentLabel10.TabStop = false;
      this.transparentLabel10.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel10.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel10.Texts")));
      // 
      // lblTeamName
      // 
      this.lblTeamName.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblTeamName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblTeamName.Location = new System.Drawing.Point(137, 58);
      this.lblTeamName.Name = "lblTeamName";
      this.lblTeamName.Size = new System.Drawing.Size(12, 26);
      this.lblTeamName.TabIndex = 87;
      this.lblTeamName.TabStop = false;
      this.lblTeamName.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblTeamName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblTeamName.Texts")));
      // 
      // transparentLabel11
      // 
      this.transparentLabel11.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.transparentLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.transparentLabel11.Location = new System.Drawing.Point(256, 58);
      this.transparentLabel11.Name = "transparentLabel11";
      this.transparentLabel11.Size = new System.Drawing.Size(42, 26);
      this.transparentLabel11.TabIndex = 86;
      this.transparentLabel11.TabStop = false;
      this.transparentLabel11.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.transparentLabel11.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("transparentLabel11.Texts")));
      // 
      // lblDeathCount
      // 
      this.lblDeathCount.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblDeathCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblDeathCount.Location = new System.Drawing.Point(137, 138);
      this.lblDeathCount.Name = "lblDeathCount";
      this.lblDeathCount.Size = new System.Drawing.Size(12, 26);
      this.lblDeathCount.TabIndex = 85;
      this.lblDeathCount.TabStop = false;
      this.lblDeathCount.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblDeathCount.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblDeathCount.Texts")));
      // 
      // lblKillCount
      // 
      this.lblKillCount.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblKillCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblKillCount.Location = new System.Drawing.Point(137, 118);
      this.lblKillCount.Name = "lblKillCount";
      this.lblKillCount.Size = new System.Drawing.Size(12, 26);
      this.lblKillCount.TabIndex = 84;
      this.lblKillCount.TabStop = false;
      this.lblKillCount.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblKillCount.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblKillCount.Texts")));
      // 
      // lblFrag
      // 
      this.lblFrag.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblFrag.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblFrag.Location = new System.Drawing.Point(137, 98);
      this.lblFrag.Name = "lblFrag";
      this.lblFrag.Size = new System.Drawing.Size(12, 26);
      this.lblFrag.TabIndex = 83;
      this.lblFrag.TabStop = false;
      this.lblFrag.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblFrag.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblFrag.Texts")));
      // 
      // lblNumber
      // 
      this.lblNumber.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblNumber.Location = new System.Drawing.Point(137, 38);
      this.lblNumber.Name = "lblNumber";
      this.lblNumber.Size = new System.Drawing.Size(12, 26);
      this.lblNumber.TabIndex = 82;
      this.lblNumber.TabStop = false;
      this.lblNumber.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblNumber.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblNumber.Texts")));
      // 
      // lblUserName
      // 
      this.lblUserName.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblUserName.Location = new System.Drawing.Point(137, 78);
      this.lblUserName.Name = "lblUserName";
      this.lblUserName.Size = new System.Drawing.Size(12, 26);
      this.lblUserName.TabIndex = 75;
      this.lblUserName.TabStop = false;
      this.lblUserName.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblUserName.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblUserName.Texts")));
      // 
      // label35
      // 
      this.label35.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label35.Location = new System.Drawing.Point(263, 138);
      this.label35.Name = "label35";
      this.label35.Size = new System.Drawing.Size(35, 26);
      this.label35.TabIndex = 81;
      this.label35.TabStop = false;
      this.label35.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label35.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label35.Texts")));
      // 
      // label34
      // 
      this.label34.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label34.Location = new System.Drawing.Point(259, 118);
      this.label34.Name = "label34";
      this.label34.Size = new System.Drawing.Size(39, 26);
      this.label34.TabIndex = 80;
      this.label34.TabStop = false;
      this.label34.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label34.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label34.Texts")));
      // 
      // label32
      // 
      this.label32.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label32.Location = new System.Drawing.Point(267, 98);
      this.label32.Name = "label32";
      this.label32.Size = new System.Drawing.Size(31, 26);
      this.label32.TabIndex = 79;
      this.label32.TabStop = false;
      this.label32.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label32.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label32.Texts")));
      // 
      // lblTeam
      // 
      this.lblTeam.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.lblTeam.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.lblTeam.Location = new System.Drawing.Point(137, 18);
      this.lblTeam.Name = "lblTeam";
      this.lblTeam.Size = new System.Drawing.Size(12, 26);
      this.lblTeam.TabIndex = 77;
      this.lblTeam.TabStop = false;
      this.lblTeam.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.lblTeam.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("lblTeam.Texts")));
      // 
      // label30
      // 
      this.label30.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label30.Location = new System.Drawing.Point(260, 38);
      this.label30.Name = "label30";
      this.label30.Size = new System.Drawing.Size(38, 26);
      this.label30.TabIndex = 78;
      this.label30.TabStop = false;
      this.label30.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label30.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label30.Texts")));
      // 
      // label28
      // 
      this.label28.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label28.Location = new System.Drawing.Point(273, 18);
      this.label28.Name = "label28";
      this.label28.Size = new System.Drawing.Size(25, 26);
      this.label28.TabIndex = 76;
      this.label28.TabStop = false;
      this.label28.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label28.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label28.Texts")));
      // 
      // label26
      // 
      this.label26.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(237)))), ((int)(((byte)(0)))));
      this.label26.Location = new System.Drawing.Point(266, 78);
      this.label26.Name = "label26";
      this.label26.Size = new System.Drawing.Size(32, 26);
      this.label26.TabIndex = 74;
      this.label26.TabStop = false;
      this.label26.TextAlign = SpecialForce.Alignment.MiddleMiddle;
      this.label26.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("label26.Texts")));
      // 
      // btnRoundPane
      // 
      this.btnRoundPane.Hint = "نمایش اطلاعات راند";
      this.btnRoundPane.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnRoundPane.Image")));
      this.btnRoundPane.Location = new System.Drawing.Point(616, 192);
      this.btnRoundPane.Name = "btnRoundPane";
      this.btnRoundPane.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnRoundPane.ShowBorder = false;
      this.btnRoundPane.Size = new System.Drawing.Size(30, 55);
      this.btnRoundPane.TabIndex = 52;
      this.btnRoundPane.TabStop = false;
      this.btnRoundPane.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnRoundPane.Texts")));
      this.btnRoundPane.TransparentColor = System.Drawing.Color.White;
      this.btnRoundPane.Click += new System.EventHandler(this.btnRoundPane_Click);
      // 
      // kevlarIcon1
      // 
      this.kevlarIcon1.AllowDrop = true;
      this.kevlarIcon1.BlueKevlar = true;
      this.kevlarIcon1.Hint = "";
      this.kevlarIcon1.Image = null;
      this.kevlarIcon1.Kevlar = null;
      this.kevlarIcon1.Location = new System.Drawing.Point(12, 23);
      this.kevlarIcon1.Name = "kevlarIcon1";
      this.kevlarIcon1.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon1.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon1.TabIndex = 75;
      this.kevlarIcon1.TabStop = false;
      this.kevlarIcon1.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon1.Texts")));
      this.kevlarIcon1.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon1.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // kevlarIcon2
      // 
      this.kevlarIcon2.AllowDrop = true;
      this.kevlarIcon2.BlueKevlar = true;
      this.kevlarIcon2.Hint = "";
      this.kevlarIcon2.Image = null;
      this.kevlarIcon2.Kevlar = null;
      this.kevlarIcon2.Location = new System.Drawing.Point(12, 163);
      this.kevlarIcon2.Name = "kevlarIcon2";
      this.kevlarIcon2.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon2.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon2.TabIndex = 76;
      this.kevlarIcon2.TabStop = false;
      this.kevlarIcon2.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon2.Texts")));
      this.kevlarIcon2.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon2.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // kevlarIcon3
      // 
      this.kevlarIcon3.AllowDrop = true;
      this.kevlarIcon3.BlueKevlar = true;
      this.kevlarIcon3.Hint = "";
      this.kevlarIcon3.Image = null;
      this.kevlarIcon3.Kevlar = null;
      this.kevlarIcon3.Location = new System.Drawing.Point(12, 303);
      this.kevlarIcon3.Name = "kevlarIcon3";
      this.kevlarIcon3.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon3.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon3.TabIndex = 77;
      this.kevlarIcon3.TabStop = false;
      this.kevlarIcon3.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon3.Texts")));
      this.kevlarIcon3.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon3.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // kevlarIcon4
      // 
      this.kevlarIcon4.AllowDrop = true;
      this.kevlarIcon4.BlueKevlar = true;
      this.kevlarIcon4.Hint = "";
      this.kevlarIcon4.Image = null;
      this.kevlarIcon4.Kevlar = null;
      this.kevlarIcon4.Location = new System.Drawing.Point(108, 23);
      this.kevlarIcon4.Name = "kevlarIcon4";
      this.kevlarIcon4.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon4.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon4.TabIndex = 80;
      this.kevlarIcon4.TabStop = false;
      this.kevlarIcon4.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon4.Texts")));
      this.kevlarIcon4.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon4.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // kevlarIcon5
      // 
      this.kevlarIcon5.AllowDrop = true;
      this.kevlarIcon5.BlueKevlar = true;
      this.kevlarIcon5.Hint = "";
      this.kevlarIcon5.Image = null;
      this.kevlarIcon5.Kevlar = null;
      this.kevlarIcon5.Location = new System.Drawing.Point(108, 163);
      this.kevlarIcon5.Name = "kevlarIcon5";
      this.kevlarIcon5.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon5.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon5.TabIndex = 79;
      this.kevlarIcon5.TabStop = false;
      this.kevlarIcon5.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon5.Texts")));
      this.kevlarIcon5.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon5.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // kevlarIcon6
      // 
      this.kevlarIcon6.AllowDrop = true;
      this.kevlarIcon6.BlueKevlar = true;
      this.kevlarIcon6.Hint = "";
      this.kevlarIcon6.Image = null;
      this.kevlarIcon6.Kevlar = null;
      this.kevlarIcon6.Location = new System.Drawing.Point(108, 303);
      this.kevlarIcon6.Name = "kevlarIcon6";
      this.kevlarIcon6.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon6.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon6.TabIndex = 78;
      this.kevlarIcon6.TabStop = false;
      this.kevlarIcon6.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon6.Texts")));
      this.kevlarIcon6.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon6.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // kevlarIcon7
      // 
      this.kevlarIcon7.AllowDrop = true;
      this.kevlarIcon7.BlueKevlar = true;
      this.kevlarIcon7.Hint = "";
      this.kevlarIcon7.Image = null;
      this.kevlarIcon7.Kevlar = null;
      this.kevlarIcon7.Location = new System.Drawing.Point(204, 303);
      this.kevlarIcon7.Name = "kevlarIcon7";
      this.kevlarIcon7.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon7.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon7.TabIndex = 83;
      this.kevlarIcon7.TabStop = false;
      this.kevlarIcon7.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon7.Texts")));
      this.kevlarIcon7.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon7.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // kevlarIcon8
      // 
      this.kevlarIcon8.AllowDrop = true;
      this.kevlarIcon8.BlueKevlar = false;
      this.kevlarIcon8.Hint = "";
      this.kevlarIcon8.Image = null;
      this.kevlarIcon8.Kevlar = null;
      this.kevlarIcon8.Location = new System.Drawing.Point(520, 23);
      this.kevlarIcon8.Name = "kevlarIcon8";
      this.kevlarIcon8.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon8.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon8.TabIndex = 82;
      this.kevlarIcon8.TabStop = false;
      this.kevlarIcon8.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon8.Texts")));
      this.kevlarIcon8.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon8.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // kevlarIcon9
      // 
      this.kevlarIcon9.AllowDrop = true;
      this.kevlarIcon9.BlueKevlar = false;
      this.kevlarIcon9.Hint = "";
      this.kevlarIcon9.Image = null;
      this.kevlarIcon9.Kevlar = null;
      this.kevlarIcon9.Location = new System.Drawing.Point(520, 163);
      this.kevlarIcon9.Name = "kevlarIcon9";
      this.kevlarIcon9.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon9.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon9.TabIndex = 81;
      this.kevlarIcon9.TabStop = false;
      this.kevlarIcon9.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon9.Texts")));
      this.kevlarIcon9.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon9.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // kevlarIcon10
      // 
      this.kevlarIcon10.AllowDrop = true;
      this.kevlarIcon10.BlueKevlar = false;
      this.kevlarIcon10.Hint = "";
      this.kevlarIcon10.Image = null;
      this.kevlarIcon10.Kevlar = null;
      this.kevlarIcon10.Location = new System.Drawing.Point(520, 303);
      this.kevlarIcon10.Name = "kevlarIcon10";
      this.kevlarIcon10.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon10.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon10.TabIndex = 86;
      this.kevlarIcon10.TabStop = false;
      this.kevlarIcon10.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon10.Texts")));
      this.kevlarIcon10.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon10.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // kevlarIcon11
      // 
      this.kevlarIcon11.AllowDrop = true;
      this.kevlarIcon11.BlueKevlar = false;
      this.kevlarIcon11.Hint = "";
      this.kevlarIcon11.Image = null;
      this.kevlarIcon11.Kevlar = null;
      this.kevlarIcon11.Location = new System.Drawing.Point(424, 23);
      this.kevlarIcon11.Name = "kevlarIcon11";
      this.kevlarIcon11.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon11.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon11.TabIndex = 85;
      this.kevlarIcon11.TabStop = false;
      this.kevlarIcon11.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon11.Texts")));
      this.kevlarIcon11.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon11.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // kevlarIcon12
      // 
      this.kevlarIcon12.AllowDrop = true;
      this.kevlarIcon12.BlueKevlar = false;
      this.kevlarIcon12.Hint = "";
      this.kevlarIcon12.Image = null;
      this.kevlarIcon12.Kevlar = null;
      this.kevlarIcon12.Location = new System.Drawing.Point(424, 163);
      this.kevlarIcon12.Name = "kevlarIcon12";
      this.kevlarIcon12.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon12.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon12.TabIndex = 84;
      this.kevlarIcon12.TabStop = false;
      this.kevlarIcon12.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon12.Texts")));
      this.kevlarIcon12.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon12.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // kevlarIcon13
      // 
      this.kevlarIcon13.AllowDrop = true;
      this.kevlarIcon13.BlueKevlar = false;
      this.kevlarIcon13.Hint = "";
      this.kevlarIcon13.Image = null;
      this.kevlarIcon13.Kevlar = null;
      this.kevlarIcon13.Location = new System.Drawing.Point(424, 303);
      this.kevlarIcon13.Name = "kevlarIcon13";
      this.kevlarIcon13.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon13.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon13.TabIndex = 87;
      this.kevlarIcon13.TabStop = false;
      this.kevlarIcon13.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon13.Texts")));
      this.kevlarIcon13.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon13.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // kevlarIcon14
      // 
      this.kevlarIcon14.AllowDrop = true;
      this.kevlarIcon14.BlueKevlar = false;
      this.kevlarIcon14.Hint = "";
      this.kevlarIcon14.Image = null;
      this.kevlarIcon14.Kevlar = null;
      this.kevlarIcon14.Location = new System.Drawing.Point(328, 303);
      this.kevlarIcon14.Name = "kevlarIcon14";
      this.kevlarIcon14.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.kevlarIcon14.Size = new System.Drawing.Size(90, 120);
      this.kevlarIcon14.TabIndex = 88;
      this.kevlarIcon14.TabStop = false;
      this.kevlarIcon14.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("kevlarIcon14.Texts")));
      this.kevlarIcon14.TransparentColor = System.Drawing.Color.White;
      this.kevlarIcon14.OnClick += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.KevlarIconClicked);
      // 
      // pnlCheat
      // 
      this.pnlCheat.BackColor = System.Drawing.Color.White;
      this.pnlCheat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlCheat.Controls.Add(this.btnCloseCheat);
      this.pnlCheat.Controls.Add(this.txtCheatAmmo);
      this.pnlCheat.Controls.Add(this.txtCheatMag);
      this.pnlCheat.Controls.Add(this.txtCheatHP);
      this.pnlCheat.Controls.Add(this.label3);
      this.pnlCheat.Controls.Add(this.label2);
      this.pnlCheat.Controls.Add(this.label1);
      this.pnlCheat.ForeColor = System.Drawing.Color.Black;
      this.pnlCheat.Location = new System.Drawing.Point(259, 163);
      this.pnlCheat.Name = "pnlCheat";
      this.pnlCheat.Size = new System.Drawing.Size(117, 99);
      this.pnlCheat.TabIndex = 0;
      this.pnlCheat.Visible = false;
      // 
      // btnCloseCheat
      // 
      this.btnCloseCheat.Hint = "";
      this.btnCloseCheat.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnCloseCheat.Image")));
      this.btnCloseCheat.Location = new System.Drawing.Point(2, 2);
      this.btnCloseCheat.Name = "btnCloseCheat";
      this.btnCloseCheat.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnCloseCheat.Size = new System.Drawing.Size(16, 16);
      this.btnCloseCheat.TabIndex = 10;
      this.btnCloseCheat.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnCloseCheat.Texts")));
      this.btnCloseCheat.TransparentColor = System.Drawing.Color.Transparent;
      this.btnCloseCheat.Click += new System.EventHandler(this.btnCloseCheat_Click);
      // 
      // txtCheatAmmo
      // 
      this.txtCheatAmmo.Location = new System.Drawing.Point(17, 73);
      this.txtCheatAmmo.Name = "txtCheatAmmo";
      this.txtCheatAmmo.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtCheatAmmo.Size = new System.Drawing.Size(30, 21);
      this.txtCheatAmmo.TabIndex = 9;
      this.txtCheatAmmo.Text = "0";
      this.txtCheatAmmo.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtCheatAmmo_OnNumberEntered);
      this.txtCheatAmmo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCheat_KeyDown);
      // 
      // txtCheatMag
      // 
      this.txtCheatMag.Location = new System.Drawing.Point(17, 48);
      this.txtCheatMag.Name = "txtCheatMag";
      this.txtCheatMag.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtCheatMag.Size = new System.Drawing.Size(30, 21);
      this.txtCheatMag.TabIndex = 8;
      this.txtCheatMag.Text = "0";
      this.txtCheatMag.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtCheatMag_OnNumberEntered);
      this.txtCheatMag.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCheat_KeyDown);
      // 
      // txtCheatHP
      // 
      this.txtCheatHP.Location = new System.Drawing.Point(17, 23);
      this.txtCheatHP.Name = "txtCheatHP";
      this.txtCheatHP.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtCheatHP.Size = new System.Drawing.Size(30, 21);
      this.txtCheatHP.TabIndex = 7;
      this.txtCheatHP.Text = "0";
      this.txtCheatHP.OnNumberEntered += new System.EventHandler<System.EventArgs>(this.txtCheatHP_OnNumberEntered);
      this.txtCheatHP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCheat_KeyDown);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(58, 51);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(42, 13);
      this.label3.TabIndex = 2;
      this.label3.Text = "خشاب:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(77, 76);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(23, 13);
      this.label2.TabIndex = 1;
      this.label2.Text = "تیر:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(49, 26);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(51, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "سلامتی:";
      // 
      // tmRoundTime
      // 
      this.tmRoundTime.Enabled = true;
      this.tmRoundTime.Interval = 1000;
      this.tmRoundTime.Tick += new System.EventHandler(this.tmRoundTime_Tick);
      // 
      // mnuGameActions
      // 
      this.mnuGameActions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAbortRound,
            this.mnuAbortGame,
            this.mnuSendMessage,
            this.mnuPingAll,
            this.mnuUpdate,
            this.mnuCheat,
            this.mnuFakeKill,
            this.mnuBombPlant,
            this.mnuBombDiffuse,
            this.mnuBombExplode});
      this.mnuGameActions.Name = "contextMenuStrip1";
      this.mnuGameActions.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
      this.mnuGameActions.Size = new System.Drawing.Size(190, 224);
      this.mnuGameActions.Opening += new System.ComponentModel.CancelEventHandler(this.mnuGameActions_Opening);
      // 
      // mnuAbortRound
      // 
      this.mnuAbortRound.Name = "mnuAbortRound";
      this.mnuAbortRound.Size = new System.Drawing.Size(189, 22);
      this.mnuAbortRound.Text = "جلوگیری از اجرای راند";
      this.mnuAbortRound.Click += new System.EventHandler(this.mnuAbortRound_Click);
      // 
      // mnuAbortGame
      // 
      this.mnuAbortGame.Name = "mnuAbortGame";
      this.mnuAbortGame.Size = new System.Drawing.Size(189, 22);
      this.mnuAbortGame.Text = "جلو گیری از اجرای بازی";
      this.mnuAbortGame.Click += new System.EventHandler(this.mnuAbortGame_Click);
      // 
      // mnuSendMessage
      // 
      this.mnuSendMessage.Name = "mnuSendMessage";
      this.mnuSendMessage.Size = new System.Drawing.Size(189, 22);
      this.mnuSendMessage.Text = "ارسال پیام";
      this.mnuSendMessage.Click += new System.EventHandler(this.mnuSendMessage_Click);
      // 
      // mnuPingAll
      // 
      this.mnuPingAll.Name = "mnuPingAll";
      this.mnuPingAll.Size = new System.Drawing.Size(189, 22);
      this.mnuPingAll.Text = "پینگ همه";
      this.mnuPingAll.Click += new System.EventHandler(this.mnuPingAll_Click);
      // 
      // mnuUpdate
      // 
      this.mnuUpdate.Name = "mnuUpdate";
      this.mnuUpdate.Size = new System.Drawing.Size(189, 22);
      this.mnuUpdate.Text = "به روز رسانی اطلاعات";
      this.mnuUpdate.Click += new System.EventHandler(this.mnuUpdate_Click);
      // 
      // mnuCheat
      // 
      this.mnuCheat.Name = "mnuCheat";
      this.mnuCheat.Size = new System.Drawing.Size(189, 22);
      this.mnuCheat.Text = "تقلب";
      this.mnuCheat.Click += new System.EventHandler(this.mnuCheat_Click);
      // 
      // mnuFakeKill
      // 
      this.mnuFakeKill.Name = "mnuFakeKill";
      this.mnuFakeKill.Size = new System.Drawing.Size(189, 22);
      this.mnuFakeKill.Text = "کشتن این بازیکن";
      this.mnuFakeKill.Click += new System.EventHandler(this.mnuFakeKill_Click);
      // 
      // mnuBombPlant
      // 
      this.mnuBombPlant.Name = "mnuBombPlant";
      this.mnuBombPlant.Size = new System.Drawing.Size(189, 22);
      this.mnuBombPlant.Text = "کاشتن بمب";
      this.mnuBombPlant.Click += new System.EventHandler(this.mnuBombPlant_Click);
      // 
      // mnuBombDiffuse
      // 
      this.mnuBombDiffuse.Name = "mnuBombDiffuse";
      this.mnuBombDiffuse.Size = new System.Drawing.Size(189, 22);
      this.mnuBombDiffuse.Text = "خنثی کردن بمب";
      this.mnuBombDiffuse.Click += new System.EventHandler(this.mnuBombDiffuse_Click);
      // 
      // mnuBombExplode
      // 
      this.mnuBombExplode.Name = "mnuBombExplode";
      this.mnuBombExplode.Size = new System.Drawing.Size(189, 22);
      this.mnuBombExplode.Text = "ترکاندن بمب";
      this.mnuBombExplode.Click += new System.EventHandler(this.mnuBombExplode_Click);
      // 
      // pnlPlayerSelection
      // 
      this.pnlPlayerSelection.BackColor = System.Drawing.Color.White;
      this.pnlPlayerSelection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlPlayerSelection.Controls.Add(this.btnCloseCheatKill);
      this.pnlPlayerSelection.Controls.Add(this.cmbPlayer);
      this.pnlPlayerSelection.Controls.Add(this.label6);
      this.pnlPlayerSelection.ForeColor = System.Drawing.Color.Black;
      this.pnlPlayerSelection.Location = new System.Drawing.Point(259, 275);
      this.pnlPlayerSelection.Name = "pnlPlayerSelection";
      this.pnlPlayerSelection.Size = new System.Drawing.Size(117, 52);
      this.pnlPlayerSelection.TabIndex = 10;
      this.pnlPlayerSelection.Visible = false;
      // 
      // btnCloseCheatKill
      // 
      this.btnCloseCheatKill.Hint = "";
      this.btnCloseCheatKill.Image = ((System.Drawing.Bitmap)(resources.GetObject("btnCloseCheatKill.Image")));
      this.btnCloseCheatKill.Location = new System.Drawing.Point(2, 1);
      this.btnCloseCheatKill.Name = "btnCloseCheatKill";
      this.btnCloseCheatKill.NormalBorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.btnCloseCheatKill.Size = new System.Drawing.Size(16, 16);
      this.btnCloseCheatKill.TabIndex = 2;
      this.btnCloseCheatKill.Texts = ((System.Collections.Generic.List<string>)(resources.GetObject("btnCloseCheatKill.Texts")));
      this.btnCloseCheatKill.TransparentColor = System.Drawing.Color.Transparent;
      this.btnCloseCheatKill.Click += new System.EventHandler(this.btnCloseCheatKill_Click);
      // 
      // cmbPlayer
      // 
      this.cmbPlayer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbPlayer.FormattingEnabled = true;
      this.cmbPlayer.Location = new System.Drawing.Point(21, 15);
      this.cmbPlayer.Name = "cmbPlayer";
      this.cmbPlayer.Size = new System.Drawing.Size(38, 21);
      this.cmbPlayer.TabIndex = 1;
      this.cmbPlayer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbKiller_KeyDown);
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(66, 18);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(38, 13);
      this.label6.TabIndex = 0;
      this.label6.Text = "توسط:";
      // 
      // ControlPanel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1320, 478);
      this.ContextMenuStrip = this.mnuGameActions;
      this.Controls.Add(this.pnlCheat);
      this.Controls.Add(this.pnlPlayerSelection);
      this.Controls.Add(this.kevlarIcon14);
      this.Controls.Add(this.kevlarIcon13);
      this.Controls.Add(this.kevlarIcon10);
      this.Controls.Add(this.kevlarIcon11);
      this.Controls.Add(this.kevlarIcon12);
      this.Controls.Add(this.kevlarIcon7);
      this.Controls.Add(this.kevlarIcon8);
      this.Controls.Add(this.kevlarIcon9);
      this.Controls.Add(this.kevlarIcon4);
      this.Controls.Add(this.kevlarIcon5);
      this.Controls.Add(this.kevlarIcon6);
      this.Controls.Add(this.kevlarIcon3);
      this.Controls.Add(this.kevlarIcon2);
      this.Controls.Add(this.kevlarIcon1);
      this.Controls.Add(this.pnlKevlarInfo);
      this.Controls.Add(this.btnKevlarPane);
      this.Controls.Add(this.pnlRoundInfo);
      this.Controls.Add(this.transparentLabel1);
      this.Controls.Add(this.label9);
      this.Controls.Add(this.btnRoundPane);
      this.Controls.Add(this.tinypicker1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Location = new System.Drawing.Point(0, 0);
      this.Name = "ControlPanel";
      this.Text = "کنترل بازی";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ControlPanel_FormClosing);
      this.Load += new System.EventHandler(this.ControlPanel_Load);
      this.pnlRoundInfo.ResumeLayout(false);
      this.pnlKevlarInfo.ResumeLayout(false);
      this.pnlCheat.ResumeLayout(false);
      this.pnlCheat.PerformLayout();
      this.mnuGameActions.ResumeLayout(false);
      this.pnlPlayerSelection.ResumeLayout(false);
      this.pnlPlayerSelection.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private TinyPicker tinypicker1;
    private SpecialForce.TransparentLabel label9;
    private ToolButton btnRoundPane;
    private TransparentLabel transparentLabel1;
    private ToolBar pnlRoundInfo;
    private ToolButton btnPrevRound;
    private TransparentLabel lblBlueTeam;
    private TransparentLabel lblGreenTeam;
    private TransparentLabel lblRoundState;
    private TransparentLabel label17;
    private TransparentLabel lblGameType;
    private TransparentLabel label10;
    private TransparentLabel lblGameObject;
    private TransparentLabel transparentLabel3;
    private TransparentLabel lblRoundIndex;
    private TransparentLabel lblGameResult;
    private TransparentLabel label12;
    private TransparentLabel lblRemainingTime;
    private TransparentLabel label8;
    private TransparentLabel transparentLabel5;
    private TransparentLabel transparentLabel6;
    private TransparentLabel transparentLabel7;
    private ToolButton btnNextRound;
    private ToolBar pnlKevlarInfo;
    private TransparentLabel lblAmmo;
    private TransparentLabel lblMagazine;
    private TransparentLabel lblHP;
    private TransparentLabel transparentLabel8;
    private TransparentLabel transparentLabel9;
    private TransparentLabel transparentLabel10;
    private TransparentLabel lblTeamName;
    private TransparentLabel transparentLabel11;
    private TransparentLabel lblDeathCount;
    private TransparentLabel lblKillCount;
    private TransparentLabel lblFrag;
    private TransparentLabel lblNumber;
    private TransparentLabel lblUserName;
    private TransparentLabel label35;
    private TransparentLabel label34;
    private TransparentLabel label32;
    private TransparentLabel lblTeam;
    private TransparentLabel label30;
    private TransparentLabel label28;
    private TransparentLabel label26;
    private ToolButton tbUserPhoto;
    private TransparentLabel lblLastTimeOutCommand;
    private TransparentLabel transparentLabel13;
    private TransparentLabel lblTimeOutCommandCount;
    private TransparentLabel transparentLabel12;
    private TransparentLabel lblLastErrorMessage;
    private TransparentLabel lblMessageErrorCount;
    private TransparentLabel lblLastMessage;
    private TransparentLabel lblLastCommand;
    private TransparentLabel lblStatus;
    private TransparentLabel lblPing;
    private TransparentLabel label25;
    private TransparentLabel label24;
    private TransparentLabel label23;
    private TransparentLabel label22;
    private TransparentLabel label20;
    private TransparentLabel label21;
    private ToolButton btnKevlarPane;
    private KevlarIcon kevlarIcon1;
    private KevlarIcon kevlarIcon2;
    private KevlarIcon kevlarIcon3;
    private KevlarIcon kevlarIcon4;
    private KevlarIcon kevlarIcon5;
    private KevlarIcon kevlarIcon6;
    private KevlarIcon kevlarIcon7;
    private KevlarIcon kevlarIcon8;
    private KevlarIcon kevlarIcon9;
    private KevlarIcon kevlarIcon10;
    private KevlarIcon kevlarIcon11;
    private KevlarIcon kevlarIcon12;
    private KevlarIcon kevlarIcon13;
    private KevlarIcon kevlarIcon14;
    private System.Windows.Forms.Panel pnlCheat;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Timer tmRoundTime;
    private System.Windows.Forms.ContextMenuStrip mnuGameActions;
    private System.Windows.Forms.ToolStripMenuItem mnuAbortRound;
    private System.Windows.Forms.ToolStripMenuItem mnuPingAll;
    private TransparentLabel lblBombState;
    private TransparentLabel transparentLabel2;
    private System.Windows.Forms.ToolStripMenuItem mnuCheat;
    private Utilities.NumberBox txtCheatAmmo;
    private Utilities.NumberBox txtCheatMag;
    private Utilities.NumberBox txtCheatHP;
    private System.Windows.Forms.ToolStripMenuItem mnuFakeKill;
    private System.Windows.Forms.Panel pnlPlayerSelection;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.ComboBox cmbPlayer;
    private ToolButton btnCloseCheatKill;
    private ToolButton btnCloseCheat;
    private System.Windows.Forms.ToolStripMenuItem mnuUpdate;
    private System.Windows.Forms.ToolStripMenuItem mnuSendMessage;
    private System.Windows.Forms.ToolStripMenuItem mnuAbortGame;
    private System.Windows.Forms.ToolStripMenuItem mnuBombPlant;
    private System.Windows.Forms.ToolStripMenuItem mnuBombDiffuse;
    private System.Windows.Forms.ToolStripMenuItem mnuBombExplode;
    private TransparentLabel lblBombDiffuser;
    private TransparentLabel label13;
    private TransparentLabel lblBombPlanter;
    private TransparentLabel transparentLabel4;
    private TransparentLabel lblRemainingBombTime;
    private TransparentLabel transparentLabel15;
    private TransparentLabel lblRoundNumber;
    private TransparentLabel transparentLabel16;
  }
}