﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SpecialForce.ControlTower
{
  public partial class FormSendMessage : SessionAwareForm
  {
    private Kevlar mReceiver = null;
    private DeviceID mReceiverID = DeviceID.Unknown;
    public DeviceID ReceiverID
    {
      get { return mReceiverID; }
      set 
      {
        mReceiverID = value;

        if (mReceiverID != DeviceID.AllDevices)
          mReceiver = Tower.GetDeviceInstance(mReceiverID) as Kevlar;
      }
    }

    public FormSendMessage()
    {
      InitializeComponent();
    }

    private void FormSendMessage_Load(object sender, EventArgs e)
    {
      lblDevice.Text = mReceiver == null ? "همه لباس ها" : mReceiver.Player.FullName;

      txtMessageLine1.RightToLeft = System.Windows.Forms.RightToLeft.No;
      txtMessageLine2.RightToLeft = System.Windows.Forms.RightToLeft.No;
      txtMessageLine1_TextChanged(null, null);
      txtMessageLine2_TextChanged(null, null);

      foreach (DB.Message msg in DB.Message.GetAllMessages())
        if(Session.CurrentStaff.IsPrivileged() || !msg.Deleted)
          lbMessages.Items.Add(msg);

      tbAdmin.Enabled = Session.CurrentStaff.IsPrivileged();
    }

    private void txtMessageLine1_TextChanged(object sender, EventArgs e)
    {
      lblMessageLine1.Text = '"' + Command.CorrectMessage(txtMessageLine1.Text) + '"';
    }

    private void txtMessageLine2_TextChanged(object sender, EventArgs e)
    {
      lblMessageLine2.Text = '"' + Command.CorrectMessage(txtMessageLine2.Text) + '"';
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      if (txtMessageLine1.Focused)
      {
        txtMessageLine2.Focus();
        return;
      }

      if (txtMessageLine1.Text.Trim().Length == 0 && txtMessageLine2.Text.Trim().Length == 0)
      {
        Session.ShowError("پیامی برای فرستادن انتخاب نشده است.");
        return;
      }

      if (mReceiver == null)
        Tower.Broadcast(txtMessageLine1.Text, txtMessageLine2.Text);
      else
        mReceiver.SendMessage(txtMessageLine1.Text, txtMessageLine2.Text);

      Close();
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void lbMessages_SelectedIndexChanged(object sender, EventArgs e)
    {
      DB.Message msg = lbMessages.SelectedItem as DB.Message;

      if(msg != null)
      {
        txtMessageLine1.Text = msg.Line1;
        txtMessageLine2.Text = msg.Line2;

        if (Session.CurrentStaff.Post.IsPrivileged())
        {
          txtMessageLine1.Focus();
          txtMessageLine1.SelectAll();
        }
      }
    }
  }
}
