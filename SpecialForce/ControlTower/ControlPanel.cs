﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.ControlTower
{
  public partial class ControlPanel : SessionAwareForm
  {
    private List<KevlarIcon> mKevlars = new List<KevlarIcon>();

    private int mRoundIndex = -1;
    private Kevlar mCurrentKevlar = null;
    private bool mRoundPaneOpen = false;
    private bool mKevlarPaneOpen = false;
    private FormSendMessage mMsgForm = null;

    private int OptimumWidth
    {
      get
      {
        int w = 625 + BorderWidth + 24;

        if(mRoundPaneOpen)
          w = w + pnlRoundInfo.Width + 40;

        if (mKevlarPaneOpen)
          w = w + pnlKevlarInfo.Width + 17;

        return w;
      }
    }

    static private ControlPanel mInstance = null;

    private ControlPanel()
    {
      InitializeComponent();
    }

    private void ControlPanel_Load(object sender, EventArgs e)
    {
      Width = OptimumWidth;
      tinypicker1.Size = new Size(0, 0);

      mKevlars.Add(kevlarIcon1);
      mKevlars.Add(kevlarIcon2);
      mKevlars.Add(kevlarIcon3);
      mKevlars.Add(kevlarIcon4);
      mKevlars.Add(kevlarIcon5);
      mKevlars.Add(kevlarIcon6);
      mKevlars.Add(kevlarIcon7);
      mKevlars.Add(kevlarIcon8);
      mKevlars.Add(kevlarIcon9);
      mKevlars.Add(kevlarIcon10);
      mKevlars.Add(kevlarIcon11);
      mKevlars.Add(kevlarIcon12);
      mKevlars.Add(kevlarIcon13);
      mKevlars.Add(kevlarIcon14);

      // In FormGameStart all players have been asigned to Tower.Kevlars
      // So no need to do anything here
      // add for teams
      int blue_index = 0, green_index = 7;
      for (int i = 0; i < Kevlars.CountForTeams; ++i)
      {
        if (Tower.Kevlars[i].Player != null)
          mKevlars[blue_index++].Kevlar = Tower.Kevlars[i];

        if (Tower.Kevlars[i + Kevlars.CountForTeams].Player != null)
          mKevlars[green_index++].Kevlar = Tower.Kevlars[i + Kevlars.CountForTeams];
      }

      // Assign event handlers
      Tower.OnDeviceUpdated += DeviceUpdated;
      Tower.OnPlayerDied += PlayerDied;
      Session.OnSessionLocked += OnSessionLocked;
      Session.OnSessionChanged += OnSessionLogin;

      // Update round pane based on current round information
      UpdateRoundInfo();
    }

    private void btnRoundPane_Click(object sender, EventArgs e)
    {
      mRoundPaneOpen = !mRoundPaneOpen;
      btnRoundPane.Image = mRoundPaneOpen ?
        Properties.Resources.ControlTower_Pane_Close :
        Properties.Resources.ControlTower_Pane_Open;

      Width = OptimumWidth;
      tinypicker1.Size = new Size(0, 0);
      CenterScreen();
      Refresh();
    }

    private void btnKevlarPane_Click(object sender, EventArgs e)
    {
      mKevlarPaneOpen = !mKevlarPaneOpen;
      btnRoundPane.Enabled = !mKevlarPaneOpen;
      btnKevlarPane.Image = mKevlarPaneOpen ?
        Properties.Resources.ControlTower_Pane_Close :
        Properties.Resources.ControlTower_Pane_Open;

      Width = OptimumWidth;
      tinypicker1.Size = new Size(0, 0);
      CenterScreen();
      Refresh();
    }

    private void KevlarIconClicked(object sender, EventArgs e)
    {
      KevlarIcon x = sender as KevlarIcon;

      if (x.Player != null)
      {
        mCurrentKevlar = x.Player.AssignedKevlar;
        UpdateKevlarPane();

        if (!mRoundPaneOpen)
          btnRoundPane_Click(sender, e);

        if (!mKevlarPaneOpen)
          btnKevlarPane_Click(sender, e);
      }
    }

    private void txtCheatHP_ValueChanged(object sender, EventArgs e)
    {
      mCurrentKevlar.Cheat((int)txtCheatMag.Value, (int)txtCheatAmmo.Value, (int)txtCheatHP.Value);
    }

    private void CheatRequested(object sender, EventArgs e)
    {
      KevlarIcon ki = sender as KevlarIcon;
      mCurrentKevlar = ki.Kevlar;
      pnlCheat.Visible = true;
      pnlCheat.Left = ki.Left + ki.Width;
      pnlCheat.Top = ki.Top;

      txtCheatHP.Value = mCurrentKevlar.HP;
      txtCheatMag.Value = mCurrentKevlar.Magazine;
      txtCheatAmmo.Value = mCurrentKevlar.Ammo;

      txtCheatHP.Focus();
    }

    private void txtCheatMag_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Escape)
        pnlCheat.Visible = false;
    }

    private void DeviceUpdated(Object sender, DeviceEventArgs e)
    {
      if (e.IsKevlar && mCurrentKevlar != null && mCurrentKevlar.ID == e.ID)
        UpdateKevlarPane();
      else if (e.IsBomb)
        UpdateRoundInfo();
    }

    private void PlayerDied(Object sender, PlayerDiedArgs e)
    {
      UpdateRoundInfo();
    }

    private void btnPreviousRound_Click(object sender, EventArgs e)
    {
      if (mRoundIndex > 0)
      {
        --mRoundIndex;
        UpdateRoundInfo();
      }
    }

    private void btnNextRound_Click(object sender, EventArgs e)
    {
      if(mRoundIndex < DB.GameManager.CurrentGame.AllRounds.Count - 1)
      {
        ++mRoundIndex;
        UpdateRoundInfo();
      }
    }

    private void mnuAbortRound_Click(object sender, EventArgs e)
    {
      string str = "آیا واقعا میخواهید از اجرای راند جلو گیری کنید؟";
      if (!Session.Ask(str))
        return;

      // Funs of using c#
      // Execute the method after this one has finished
      Session.Synchronize(new MethodInvoker(delegate { DB.GameManager.AbortCurrentRound("اجرای راند توسط اپراتور متوقف شد."); }));
    }

    private void mnuAbortGame_Click(object sender, EventArgs e)
    {
      if (Session.Ask("آیا واقعا میخواهید از اجرای بازی جلو گیری کنید؟"))
        DB.GameManager.AbortCurrentGame("اجرای بازی توسط اپراتور متوقف شد.");
    }

    private void mnuPingAll_Click(object sender, EventArgs e)
    {
      bool show_error = false;
      string strErrors = "";
      foreach (KevlarIcon ki in mKevlars)
        if (ki.Kevlar != null && ki.Kevlar.Player != null)
          if (!ki.Kevlar.Ping())
          {
            string num = ki.Kevlar.TeamNumber.ToString();
            string team = Command.ToString(ki.Kevlar.Team);
            strErrors = strErrors + ki.Kevlar.ToString() + Environment.NewLine;
            show_error = true;
          }

      if (show_error)
        Session.ShowError("لباس های زیر پینگ نمیشوند\n\n" + strErrors);
      else
        Session.ShowMessage("همه لباس ها پینگ میشوند");
    }

    private void mnuGameActions_Opening(object sender, System.ComponentModel.CancelEventArgs e)
    {
      // Can not do anything if no game is running
      DB.Game game = DB.GameManager.CurrentGame;
      if(game == null)
      {
        e.Cancel = true;
        return;
      }

      // Can not do anything if no round is running
      DB.Round round = game.CurrentRound;
      if(round == null)
      {
        e.Cancel = true;
        return;
      }

      // Evaluate primal information
      Point p = PointToClient(Cursor.Position);
      KevlarIcon ki = GetChildAtPoint(p) as KevlarIcon;
      mCurrentKevlar = ki == null ? null : ki.Kevlar;

      // Begin handling menu items
      mnuAbortRound.Available = round.State == RoundState.InProgress;
      mnuAbortGame.Available = true;
      mnuSendMessage.Available = true;
      mnuPingAll.Available = true;
      mnuUpdate.Available = mCurrentKevlar != null;

      // Disable all cheat menus
      mnuCheat.Available = false;
      mnuFakeKill.Available = false;
      mnuBombPlant.Available = false;
      mnuBombDiffuse.Available = false;
      mnuBombExplode.Available = false;

      // Cheat menus depend on the kevlar to have a player
      // And the current staff to be administrator
      if(!Session.CurrentStaff.Post.IsPrivileged())
        return;

      mnuBombExplode.Available = round.BombState == BombState.Planted;
      if (mCurrentKevlar == null)
        return;

      if(mCurrentKevlar.State == KevlarState.Working)
      {
        mnuCheat.Available = true;
        mnuFakeKill.Available = 
          mCurrentKevlar.Team == TeamType.Blue ?
          round.GetKillCount(TeamType.Blue) < round.GreenPlayers.Count:  // If any green is alive
          round.GetKillCount(TeamType.Green) < round.BluePlayers.Count;  // If any blue is alive
      }

      if(game.Object == GameObject.Bombing)
      {
        TeamType ct = round.TerroristsAreBlue ? TeamType.Green : TeamType.Blue;
        bool is_ct = mCurrentKevlar.Team == ct;

        mnuBombPlant.Available = (round.BombState == BombState.Enabled) && (!is_ct);
        mnuBombDiffuse.Available = (round.BombState == BombState.Planted) && (is_ct);
      }
    }

    private void mnuCheat_Click(object sender, EventArgs e)
    {
      txtCheatHP.Value = mCurrentKevlar.HP;
      txtCheatMag.Value = mCurrentKevlar.Magazine;
      txtCheatAmmo.Value = mCurrentKevlar.Ammo;
      pnlCheat.Visible = true;
      txtCheatHP.Focus();
    }

    private void txtCheatHP_OnNumberEntered(object sender, EventArgs e)
    {
      txtCheatMag.Focus();
    }

    private void txtCheatMag_OnNumberEntered(object sender, EventArgs e)
    {
      txtCheatAmmo.Focus();
    }

    private void txtCheatAmmo_OnNumberEntered(object sender, EventArgs e)
    {
      int hp = txtCheatHP.IntegerValue;
      int mag = txtCheatMag.IntegerValue;
      int ammo = txtCheatAmmo.IntegerValue;

      if (hp < 1 || hp > 99)
        Session.ShowError("سلامتی وارد شده معتبر نیست");
      else if(mag < 1 || mag > 99)
        Session.ShowError("خشاب وارد شده معتبر نیست.");
      else if (ammo < 1 || ammo > 999)
        Session.ShowError("تیر وارد شده معتبر نیست.");
      else
      {
        // Hide the cheat dialog so that multiple cheats can not be entered
        pnlCheat.Visible = false;
        Application.DoEvents();

        // Cheat!
        mCurrentKevlar.Cheat(txtCheatHP.IntegerValue, txtCheatMag.IntegerValue, txtCheatAmmo.IntegerValue);
      }
    }

    private void txtCheat_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Escape)
        pnlCheat.Visible = false;
    }

    private void mnuFakeKill_Click(object sender, EventArgs e)
    {
      DB.Round round = DB.GameManager.CurrentGame.CurrentRound;
      List<DB.User> opps = 
        mCurrentKevlar.Team == TeamType.Blue ?
        round.GreenPlayers :
        round.BluePlayers;

      cmbPlayer.Items.Clear();
      for (int i = 0; i < opps.Count; ++i)
        if (opps[i].AssignedKevlar.State == KevlarState.Working)
          cmbPlayer.Items.Add(opps[i].AssignedKevlar.TeamNumber);
      cmbPlayer.SelectedIndex = 0;

      pnlPlayerSelection.Left = pnlCheat.Left;
      pnlPlayerSelection.Top = pnlCheat.Top;
      pnlPlayerSelection.Visible = true;
      cmbPlayer.Focus();
    }

    private void cmbKiller_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Escape)
        pnlPlayerSelection.Visible = false;
      else if (e.KeyCode == Keys.Enter)
      {
        // Hide the kill dialog so that multiple kills can not be possible
        pnlPlayerSelection.Visible = false;
        Application.DoEvents();

        // Now fake the kill
        int num = Convert.ToInt32(cmbPlayer.SelectedItem.ToString());
        mCurrentKevlar.FakeDeath(num);
      }
    }

    public void ShowGame()
    {
      Show();
      BringToFront();
      CenterScreen();
    }

    private void btnCloseCheatKill_Click(object sender, EventArgs e)
    {
      pnlPlayerSelection.Visible = false;
    }

    private void btnCloseCheat_Click(object sender, EventArgs e)
    {
      pnlCheat.Visible = false;
    }

    private void mnuUpdate_Click(object sender, EventArgs e)
    {
      mCurrentKevlar.Sync();
    }

    private void ControlPanel_FormClosing(object sender, FormClosingEventArgs e)
    {
      Session.OnSessionLocked -= OnSessionLocked;
      Session.OnSessionChanged -= OnSessionLogin;
      Tower.OnDeviceUpdated -= DeviceUpdated;
      Tower.OnPlayerDied -= PlayerDied;
    }

    static public void MonitorGame()
    {
      if (mInstance == null || mInstance.IsDisposed)
      {
        mInstance = new ControlPanel();
        mInstance.Show();
      }

      mInstance.UpdateRoundInfo(DB.GameManager.CurrentGame.AllRounds.Count - 1); 
      mInstance.BringToFront();
      mInstance.Focus();
    }

    static public void StopMonitoring()
    {
      if (mInstance != null && !mInstance.IsDisposed)
      {
        mInstance.Close();
        mInstance.Dispose();
        mInstance = null;
      }
    }

    private void mnuSendMessage_Click(object sender, EventArgs e)
    {
      if (mMsgForm != null && !mMsgForm.IsDisposed)
      {
        mMsgForm.Close();
        mMsgForm.Dispose();
      }

      mMsgForm = new FormSendMessage();
      mMsgForm.ReceiverID =
        mCurrentKevlar != null && mCurrentKevlar.Player != null ?
        mCurrentKevlar.ID : 
        DeviceID.AllDevices;
      mMsgForm.Show();
    }

    private void OnSessionLocked(object sender, EventArgs e)
    {
      if (!IsDisposed)
        Enabled = false;
    }

    private void OnSessionLogin(object sender, EventArgs e)
    {
      if (!IsDisposed && !Session.CurrentStaff.Post.IsUser())
        Enabled = true;
    }

    private void mnuBombPlant_Click(object sender, EventArgs e)
    {
      Tower.Bombs[0].CheatState(BombState.Planted, mCurrentKevlar.ID);
    }

    private void mnuBombDiffuse_Click(object sender, EventArgs e)
    {
      Tower.Bombs[0].CheatState(BombState.Diffused, mCurrentKevlar.ID);
    }

    private void mnuBombExplode_Click(object sender, EventArgs e)
    {
      ControlTower.Tower.ActiveBomb.ManualStateUpdate(BombState.Exploded, true);
    }

    public void UpdateRoundInfo(int round_nr = -1)
    {
      if (round_nr != -1)
        mRoundIndex = round_nr;

      lblGameType.Text = "-";
      lblGameObject.Text = "-";
      lblGameResult.Text = "-";
      lblGreenTeam.Text = "-";
      lblBlueTeam.Text = "-";
      lblBombPlanter.Text = "-";
      lblBombDiffuser.Text = "-";
      lblRoundNumber.Text = "-";

      DB.Game game = DB.GameManager.CurrentGame;
      if (game == null)
        return;

      if (mRoundIndex == -1)
        mRoundIndex = game.AllRounds.Count - 1;

      btnNextRound.Enabled = mRoundIndex != game.AllRounds.Count - 1;
      btnPrevRound.Enabled = mRoundIndex != 0;

      DB.Round round = game.AllRounds[mRoundIndex];
      lblRoundIndex.Text = (mRoundIndex + 1).ToString() + "/" + game.AllRounds.Count.ToString();
      lblGameType.Text = Enums.ToString(game.Type);
      lblGameObject.Text = Enums.ToString(game.Object);
      lblRoundNumber.Text = round.State == RoundState.Aborted ? "-" : round.Number.ToString();
      lblBlueTeam.Text = game.GetBlueTeamName("بازی رسمی نیست");
      lblGreenTeam.Text = game.GetGreenTeamName("بازی رسمی نیست");
      lblBombPlanter.Text = round.Planter != null ? round.Planter.FullName : "-";
      lblBombDiffuser.Text = round.Diffuser != null ? round.Diffuser.FullName : "-";

      if (game.State == GameState.Free || game.State == GameState.Reserved)
        lblGameResult.Text = "-";
      else if (game.State == GameState.Aborted)
        lblGameResult.Text = "کنسل";
      else
      {
        int bs, gs;
        game.GetTeamScores(out bs, out gs);
        string str = Math.Max(bs, gs) + " - " + Math.Min(bs, gs);
        str += " ";
        if (bs == gs)
          str += "مساوی";
        else if (bs > gs)
          str += game.GetBlueTeamName("آبی");
        else
          str += game.GetGreenTeamName("سبز");

        lblGameResult.Text = str;
      }

      if (round.BombState == BombState.Disabled)
        lblBombState.Text = "غیر فعال";
      else if (round.BombState == BombState.Enabled)
        lblBombState.Text = "فعال نشده";
      else if (round.BombState == BombState.Planted)
        lblBombState.Text = "فعال";
      else if (round.BombState == BombState.Diffused)
        lblBombState.Text = "خنثی شده";
      else
        lblBombState.Text = "منفجر شده";

      // show remaining time
      tmRoundTime_Tick(this, null);
    }

    private void UpdateKevlarPane()
    {
      tbUserPhoto.Image = Properties.Resources.User_Photo_Place_Holder;
      lblTeam.Text = "-";
      lblNumber.Text = "-";
      lblTeamName.Text = "-";
      lblUserName.Text = "-";
      lblFrag.Text = "-";
      lblKillCount.Text = "-";
      lblDeathCount.Text = "-";
      lblHP.Text = "-";
      lblMagazine.Text = "-";
      lblAmmo.Text = "-";
      lblPing.Text = "-";
      lblStatus.Text = "-";
      lblLastCommand.Text = "-";
      lblLastMessage.Text = "-";
      lblMessageErrorCount.Text = "-";
      lblLastErrorMessage.Text = "-";
      lblTimeOutCommandCount.Text = "-";
      lblLastTimeOutCommand.Text = "-";

      if (mCurrentKevlar == null)
        return;

      DB.Game game = DB.GameManager.CurrentGame;
      if (game == null)
        return;

      int num = mCurrentKevlar.Number;
      tbUserPhoto.Image = mCurrentKevlar.Player.Photo.Resize(tbUserPhoto.Size);
      lblTeam.Text = Command.ToString(mCurrentKevlar.Team);
      lblNumber.Text = num > Kevlars.CountForTeams ? (num - Kevlars.CountForTeams).ToString() : num.ToString();
      lblTeamName.Text = mCurrentKevlar.Player.GetTeamName("-");
      lblUserName.Text = mCurrentKevlar.Player.FullName;
      lblFrag.Text = mCurrentKevlar.Frag.ToString();
      lblKillCount.Text = game.GetKillCount(mCurrentKevlar.Player).ToString();
      lblDeathCount.Text = game.GetDeathCount(mCurrentKevlar.Player).ToString();
      lblHP.Text = mCurrentKevlar.HP.ToString();
      lblMagazine.Text = mCurrentKevlar.Magazine.ToString();
      lblAmmo.Text = mCurrentKevlar.Ammo.ToString();
      lblPing.Text = mCurrentKevlar.PingCount.ToString() + " از " + mCurrentKevlar.PongCount.ToString();
      lblLastCommand.Text = mCurrentKevlar.LastCommand;
      lblLastMessage.Text = mCurrentKevlar.LastMessage;
      lblMessageErrorCount.Text = mCurrentKevlar.IllegalMessages.ToString();
      lblLastErrorMessage.Text = mCurrentKevlar.LastIllegalMessage;
      lblTimeOutCommandCount.Text = mCurrentKevlar.TimedOutMessages.ToString();
      lblLastTimeOutCommand.Text = mCurrentKevlar.LastTimedOutMessage;

      string strState = Enums.ToString(mCurrentKevlar.State);
      try
      {
        if(mCurrentKevlar.State == KevlarState.Dead)
        {
          DB.Round r = game.CurrentRound;
          if(r != null)
          {
            for(int i = 0; i < r.Kills.Count; ++i)
              if (r.Kills[i].KilledID == mCurrentKevlar.Player.ID)
              {
                strState += " توسط " + DB.User.GetUserFullName(r.Kills[i].KillerID);
                break;
              }
          }
        }
      }
      finally
      {
        lblStatus.Text = strState;
      }
    }

    private void tmRoundTime_Tick(object sender, EventArgs e)
    {
      DB.Game game = DB.GameManager.CurrentGame;
      if(game == null)
        return;

      if (mRoundIndex < 0 || mRoundIndex >= game.AllRounds.Count)
        return;

      string state = "-";
      DB.Round r = game.AllRounds[mRoundIndex];
      if (r.State == RoundState.Pending)
        state = "در انتظار شروع";
      else if (r.State == RoundState.Aborted)
        state = "کنسل شده";
      else if (r.State == RoundState.InProgress)
        state = "در حال اجرا";
      else if (r.Winner == WinnerType.Tie)
        state = "مساوی";
      else if (r.Winner == WinnerType.Blue)
        state = "آبی برد.";
      else if (r.Winner == WinnerType.Green)
        state = "سبز برد.";

      lblRemainingTime.Text = DB.Round.TimeToString(Math.Max(r.RemainingTime, 0));
      if (game.Object == GameObject.Bombing)
        lblRemainingBombTime.Text = DB.Round.TimeToString(Math.Max(r.RemainingBombTime, 0));

      if (state != lblRoundState.Text)
        lblRoundState.Text = state;
    }
  }
}
