﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SpecialForce.ControlTower
{
  public enum DeviceStateUI
  {
    Empty,
    Commiting,
    Error,
    Finished,
    NotAffected
  }

  public partial class DeviceInteractionsUI : ControlBase
  {
    #region Internal Properties
    private class Device
    {
      public string State;
      public DeviceStateUI Mode;

      public Device(string st, DeviceStateUI ui)
      {
        State = st;
        Mode = ui;
      }
    }

    private Bitmap mBackImage = null;
    private Graphics mCanvas = null;
    private TextRenderer mRenderer;
    private Size mDefiniteSize = Size.Empty;
    private List<Device> mStates = new List<Device>();
    #endregion Internal Properties


    #region Properties
    private int mIconX = 280;
    [DefaultValue(280)]
    public int IconX
    {
      get { return mIconX; }
      set { mIconX = value; }
    }

    private int mSeperatorX = 210;
    [DefaultValue(210)]
    private int SeperatorX
    {
      get { return mSeperatorX; }
      set { mSeperatorX = value; }
    }

    private int mGridWidth = 310;
    [DefaultValue(310)]
    public int GridWidth
    {
      get { return mGridWidth; }
      set { mGridWidth = value; }
    }

    private int mTitleHeight = 26;
    [DefaultValue(26)]
    public int TitleHeight
    {
      get { return mTitleHeight; }
      set { mTitleHeight = value; }
    }

    private int mRowHeight = 26;
    [DefaultValue(26)]
    public int RowHeight
    {
      get { return mRowHeight; }
      set { mRowHeight = value; }
    }

    private Color mGridColor = Color.FromArgb(150, 150, 150);
    [DefaultValue(typeof(Color), "0xFF969696")]
    public Color GridColor
    {
      get { return mGridColor; }
      set { mGridColor = value; }
    }

    private Color mSelectedColor = Color.LightBlue;
    [DefaultValue(typeof(Color), "0xFFADD8E6")]
    public Color SelectedColor
    {
      get { return mSelectedColor; }
      set { mSelectedColor = value; }
    }

    private Color mErrorColor = Color.Red;
    [DefaultValue(typeof(Color), "0xFFFF0000")]
    public Color ErrorColor
    {
      get { return mErrorColor; }
      set { mErrorColor = value; }
    }

    private Color mFinishedColor = Color.Green;
    [DefaultValue(typeof(Color), "0xFF008000")]
    public Color FinishedColor
    {
      get { return mFinishedColor; }
      set { mFinishedColor = value; }
    }

    private Color mUntouchedColor = Color.LightGreen;
    [DefaultValue(typeof(Color), "0xFF90EE90")]
    public Color UntouchedColor
    {
      get { return mUntouchedColor; }
      set { mUntouchedColor = value; }
    }
    #endregion Properties


    #region Constructor
    public DeviceInteractionsUI()
    {
      TabStop = false;

      for (int i = 0; i < Kevlars.TotalCount + Kevlars.BombCount; ++i)
        mStates.Add(new Device("انتظار", DeviceStateUI.Empty));

      // Create background image
      mBackImage = new Bitmap(mGridWidth, mTitleHeight + mStates.Count * mRowHeight + 1);
      mCanvas = Graphics.FromImage(mBackImage);

      // Create text renderer
      mRenderer = new TextRenderer();
      mRenderer.Canvas = mCanvas;
      mRenderer.RightToLeft = true;
      mRenderer.Justify = Alignment.MiddleRight;
      mRenderer.Font = new Font("B Nazanin", 12);

      // Set definite size
      mDefiniteSize = new Size(mBackImage.Width + 21, 200);
      Size = mDefiniteSize;

      InitializeComponent();
    }
    #endregion Constructor


    #region Overrides
    protected override BehaviorParams CreateBehavior
    {
      get
      {
        BehaviorParams prms = base.CreateBehavior;
        prms.UsesAntiAlias();
        return prms;
      }
    }

    protected override Size QueryPrefferedSize()
    {
      return mDefiniteSize != Size.Empty ? mDefiniteSize : new Size(0, 0);
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);

      scrVertical.Left = Width - scrVertical.Width;
      scrVertical.Top = 0;
      scrVertical.Height = Height;
      scrVertical.Maximum = mBackImage.Height - mTitleHeight;
      DrawInteractions();
    }

    protected override void OnMouseEnter(EventArgs e)
    {
      base.OnMouseEnter(e);
      Focus();
    }

    private void scrVertical_Scroll(object sender, ScrollEventArgs e)
    {
      RequestRepaint();
    }

    protected override void OnMouseWheel(MouseEventArgs e)
    {
      base.OnMouseWheel(e);

      int v = scrVertical.Value - e.Delta / 5;
      v = Math.Min(v, scrVertical.Maximum);
      v = Math.Max(v, scrVertical.Minimum);
      scrVertical.Value = v;
      RequestRepaint();
    }
    #endregion Overrides


    #region Interface
    public void ShowInteraction(DeviceID id, string state, DeviceStateUI mode)
    {
      // Get the interaction's index
      int index;
      if (id <= DeviceID.Bomb1 && id >= DeviceID.Bomb2)
        index = Kevlars.TotalCount + (DeviceID.Bomb1 - id);
      else if (id >= DeviceID.KevlarBlue1 && id <= DeviceID.KevlarGreen10)
        index = (int)id - 1;
      else
        index = -1;

      if (index == -1)
        return;

      // Update current interaction
      mStates[index].State = state;
      mStates[index].Mode = mode;
      scrVertical.Value = TinyMath.Round((double)index / mStates.Count * scrVertical.Maximum);

      // Draw all interactions
      DrawInteractions(index);
    }

    public void ResetInteractions()
    {
      for (int i = 0; i < mStates.Count; ++i)
      {
        mStates[i].State = "انتظار";
        mStates[i].Mode = DeviceStateUI.Empty;
      }

      scrVertical.Value = scrVertical.Minimum;
      DrawInteractions();
    }

    public DeviceID DeviceFromIndex(int index)
    {
      if (index < Kevlars.TotalCount)
        return DeviceID.KevlarBlue1 + index;
      else if (index == 20)
        return DeviceID.Bomb1;
      else if (index == 21)
        return DeviceID.Bomb2;
      else
        throw new Exception("(DeviceInteractionUI) unknown index: " + index.ToString());
    }
    #endregion Interface


    #region Graphical Functionality
    protected override void OnRecreateView(RecreateArgs e)
    {
      // A simple hack to always display the title the easy way... ?!
      int top = TinyMath.Round((double)scrVertical.Value * (scrVertical.Maximum + mRowHeight - Height + 6) / scrVertical.Maximum);
      Rectangle source = new Rectangle(0, top + mTitleHeight, mBackImage.Width, ClientRectangle.Height - mTitleHeight);
      Rectangle dest = new Rectangle(0, mTitleHeight, mBackImage.Width, ClientRectangle.Height - mTitleHeight);
      e.Canvas.DrawImage(mBackImage, dest, source, GraphicsUnit.Pixel);

      source = new Rectangle(0, 0, mBackImage.Width, mTitleHeight);
      dest = new Rectangle(0, 0, mBackImage.Width, mTitleHeight);
      e.Canvas.DrawImage(mBackImage, dest, source, GraphicsUnit.Pixel);
    }

    private void DrawInteraction(int index)
    {
      mRenderer.Color = ForeColor;

      // Draw background
      int top = mTitleHeight + index * mRowHeight;

      if (mStates[index].Mode == DeviceStateUI.Empty)
      { }
      else if(mStates[index].Mode == DeviceStateUI.Commiting)
        mCanvas.DrawGradient(0, top, mGridWidth - 1, mRowHeight, mSelectedColor, ControlPaint.Dark(mSelectedColor, 0.05f), Gradients.VerticalMirror, 2);
      else if(mStates[index].Mode == DeviceStateUI.Error)
        mCanvas.DrawGradient(0, top, mGridWidth - 1, mRowHeight, mErrorColor, ControlPaint.Dark(mErrorColor, 0.05f), Gradients.VerticalMirror, 2);
      else if(mStates[index].Mode == DeviceStateUI.Finished)
        mCanvas.DrawGradient(0, top, mGridWidth - 1, mRowHeight, mFinishedColor, ControlPaint.Dark(mFinishedColor, 0.05f), Gradients.VerticalMirror, 2);
      else if (mStates[index].Mode == DeviceStateUI.NotAffected)
        mCanvas.DrawGradient(0, top, mGridWidth - 1, mRowHeight, mUntouchedColor, ControlPaint.Dark(mUntouchedColor, 0.05f), Gradients.VerticalMirror, 2);

      // Draw marker on top of background for current commiting row
      if (mStates[index].Mode == DeviceStateUI.Commiting)
      {
        Bitmap img = Properties.Resources.ControlTower_Kevlar_Marker;
        int spc = mGridWidth - mIconX;
        int marker_left = mIconX + (spc - img.Width) / 2;
        int marker_top = top + (mRowHeight - img.Height) / 2;
        mCanvas.DrawImageUnscaled(marker_left, marker_top, img);
      }

      // Update renderer properties
      mRenderer.Left = 0;
      mRenderer.Size = new Size(mSeperatorX - 15, mRowHeight);
      mRenderer.Top = mTitleHeight + index * mRowHeight;
      mRenderer.Text = mStates[index].State;
      mRenderer.Draw();

      // Draw device's name
      int j = index + 1;
      if (index < Kevlars.CountForTeams)
        mRenderer.Text = "آبی " + j.ToString();
      else if (index < Kevlars.TotalCount)
        mRenderer.Text = "سبز " + (j - Kevlars.CountForTeams).ToString();
      else
        mRenderer.Text = "بمب " + (j - Kevlars.TotalCount).ToString();

      // Draw device's state
      mRenderer.Left = mSeperatorX;
      mRenderer.Size = new Size(mIconX - mSeperatorX - 15, mRowHeight);
      mRenderer.Draw();
    }

    private void DrawInteractions(int selected_index = -1)
    {
      // Draw graphics
      int count = Kevlars.TotalCount + Kevlars.BombCount;

      // Clear the image
      mCanvas.Clear(Color.Transparent);

      // Draw border
      mCanvas.DrawRoundedRectangle(new Pen(ForeColor), 0, 0, mGridWidth - 1, mBackImage.Height - 1, 6);
      
      // Draw title bar
      mCanvas.DrawGradient(0, 0, mGridWidth - 1, mTitleHeight, SystemColors.GradientActiveCaption, SystemColors.GradientInactiveCaption, Gradients.Vertical, 6, RectangleEdgeFilter.TopLeft | RectangleEdgeFilter.TopRight);

      // Draw all interactions
      for (int i = 0; i < count; ++i)
        DrawInteraction(i);

      // Draw grid
      Pen line = new Pen(mGridColor);

      // Vertical lines
      mCanvas.DrawLine(line, mSeperatorX, mTitleHeight, mSeperatorX, mBackImage.Height);
      mCanvas.DrawLine(line, mIconX, mTitleHeight, mIconX, mBackImage.Height);

      // Horizontal lines
      for (int i = 1; i < count; ++i)
      {
        int top = mTitleHeight + i * mRowHeight;
        mCanvas.DrawLine(line, 1, top, Width - 2, top);
      }

      // Draw title bar column names
      mRenderer.Left = mSeperatorX;
      mRenderer.Color = Color.DarkBlue;
      mRenderer.Size = new System.Drawing.Size(mIconX - mSeperatorX - 5, mTitleHeight);
      mRenderer.Top = 0;
      mRenderer.Text = "دستگاه";
      mRenderer.Draw();

      mRenderer.Left = 0;
      mRenderer.Size = new Size(mSeperatorX - 5, mRowHeight);
      mRenderer.Text = "وضعیت";
      mRenderer.Draw();

      // Refresh parent
      Refresh();
    }
    #endregion Graphical Functionality
  }
}
