﻿namespace SpecialForce.ControlTower
{
  partial class DeviceInteractionsUI
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.scrVertical = new System.Windows.Forms.VScrollBar();
      this.SuspendLayout();
      // 
      // scrVertical
      // 
      this.scrVertical.Location = new System.Drawing.Point(133, 0);
      this.scrVertical.Maximum = 10;
      this.scrVertical.Name = "scrVertical";
      this.scrVertical.Size = new System.Drawing.Size(17, 150);
      this.scrVertical.TabIndex = 0;
      this.scrVertical.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrVertical_Scroll);
      // 
      // DeviceInteractionsUI
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.scrVertical);
      this.Name = "DeviceInteractionsUI";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.VScrollBar scrVertical;

  }
}
