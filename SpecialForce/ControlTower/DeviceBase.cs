﻿using System;
using System.Collections.Generic;

namespace SpecialForce.ControlTower
{
  public abstract class DeviceBase
  {
    #region Ping Pong!
    protected int mPingCount = 0;
    public int PingCount
    {
      get { return mPingCount; }
    }

    protected PersianDateTime mLastPingTime = null;
    public PersianDateTime LastPing
    {
      get { return mLastPingTime; }
    }

    protected int mPongCount = 0;
    public int PongCount
    {
      get { return mPongCount; }
    }

    protected PersianDateTime mLastPongTime = null;
    public PersianDateTime LastPong
    {
      get { return mLastPongTime; }
    }

    public double PingRatio
    {
      get { return (double)mPongCount / mPingCount; }
    }
    #endregion Ping Pong!


    #region Device Information
    protected DeviceID mDeviceID = DeviceID.Unknown;
    public DeviceID ID
    {
      get { return mDeviceID; }
      set { mDeviceID = value; }
    }

    protected bool mConfigured = false;
    #endregion Device Information


    #region Commands and Messages
    // This switch just disables massaging pipe, not the whole kevlar
    protected bool mResponsive = false;
    public bool Responsive
    {
      get { return mResponsive; }
      set { mResponsive = value; }
    }

    protected string mLastCommand = "-";
    public string LastCommand
    {
      get { return mLastCommand; }
    }

    protected string mLastMessage = "-";
    public string LastMessage
    {
      get { return mLastMessage; }
    }

    protected int mIllegalMessages = 0;
    public int IllegalMessages
    {
      get { return mIllegalMessages; }
    }

    protected string mLastIllegalMessage = "-";
    public string LastIllegalMessage
    {
      get { return mLastIllegalMessage; }
    }

    protected int mTimedOutMessages = 0;
    public int TimedOutMessages
    {
      get { return mTimedOutMessages; }
    }

    protected string mLastTimedOutMessage = "-";
    public string LastTimedOutMessage
    {
      get { return mLastTimedOutMessage; }
    }
    #endregion Commands and Messages


    #region Utilities
    protected void ClearPingStats()
    {
      mPingCount = 0;
      mPongCount = 0;
      mLastPingTime = null;
      mLastPongTime = null;
    }

    protected void ClearCommunicationCache()
    {
      mLastCommand = "-";
      mLastMessage = "-";
      mIllegalMessages = 0;
      mLastIllegalMessage = "-";
    }

    public bool Ping()
    {
      int cnt = mPongCount;
      Command cmd = new Command(Commands.Ping, mDeviceID);
      mLastPingTime = PersianDateTime.Now;
      ++mPingCount;

      ExecuteCommand(cmd);
      CommandQueue.EnsureQueueIsSent();
      return mPongCount > cnt;
    }

    protected void Pong()
    {
      mLastPongTime = PersianDateTime.Now;
      ++mPongCount;
    }

    public override string ToString()
    {
      return Kevlars.ToString(mDeviceID);
    }
    #endregion Utilities


    #region Commands and Messages
    public void _RecordTimedOutMessage(string msg)
    {
      ++mTimedOutMessages;
      mLastTimedOutMessage = msg;
    }

    public bool Configure(DeviceLocation loc = DeviceLocation.Center)
    {
      mConfigured = false;
      Command cmd = CreateConfigureCommand();
      cmd.PreferedLocation = loc;
      ExecuteCommand(cmd);
      CommandQueue.EnsureQueueIsSent();
      return mConfigured;
    }

    public void Sync(DeviceLocation loc = DeviceLocation.Center)
    {
      Command cmd = new Command(Commands.SendUpdate, mDeviceID);
      cmd.PreferedLocation = loc;
      ExecuteCommand(cmd);
      CommandQueue.EnsureQueueIsSent();
    }

    protected void ExecuteCommand(Command cmd)
    {
      mLastCommand = cmd.CommandString;
      CommandQueue.PostCommand(cmd);
    }

    private List<int> ExtractParameters(string msg)
    {
      List<int> values = new List<int>();

      bool cont = true;
      while(cont)
        try
        {
          int index = msg.LastIndexOf('|') + 1;
          if (index == 0)
            cont = false;
          else
          {
            values.Add(Convert.ToInt32(msg.Substring(index, msg.Length - index)));
            msg = msg.Substring(0, index - 1);
          }
        }
        catch
        {
          cont = false;
        }

      List<int> result = new List<int>();
      for (int i = values.Count - 1; i >= 0; --i)
        result.Add(values[i]);
      return result;
    }

    public void ProcessMessage(string strMessage)
    {
      Commands cmd = Command.FromString(strMessage);
      List<int> prms = ExtractParameters(strMessage);

      // In case the pipeline is shut off just remove
      // the associated command and exit
      if (!mResponsive)
      {
        CommandQueue.Remove(cmd, mDeviceID);
        return;
      }

      // Process the command
      bool processed = true;
      if (cmd == Commands.Ping)
        Pong();
      else if(!ProcessMessageInternal(cmd, prms))
        processed = false;

      if (!processed)
      {
        ++mIllegalMessages;
        mLastIllegalMessage = strMessage;
      }
      else
        mLastMessage = strMessage;

      // Clean up and signal update.
      CommandQueue.Remove(cmd, mDeviceID);
      Tower.SignalDeviceUpdated(mDeviceID);
    }
    #endregion Commands and Messages


    #region Statis Commands
    /// <summary>
    /// This method is used to create a turn on command for a given device.
    /// </summary>
    public static Command CreateTurnOnCommand(DeviceID device)
    {
      Command cmd = new Command(Commands.State, device);
      cmd.AddParameter((int)KevlarState.Working, 2);
      return cmd;
    }

    /// <summary>
    /// This method creates a turn off command for a given device.
    /// </summary>
    public static Command CreateTurnOffCommand(DeviceID device)
    {
      Command cmd = new Command(Commands.State, device);
      cmd.AddParameter((int)KevlarState.Disabled, 2);
      return cmd;
    }
    #endregion


    #region Abstractions
    abstract protected Command CreateConfigureCommand();
    abstract protected bool ProcessMessageInternal(Commands type, List<int> prms);
    #endregion Abstractions
  }
}
