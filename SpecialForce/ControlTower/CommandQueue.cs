﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Threading;

namespace SpecialForce.ControlTower
{
  public class CommandQueue
  {
    #region Private types
    private struct Packet
    {
      public string Data;
      public PersianDateTime Time;
    };
    #endregion Private types


    #region Instances and Data
    // devices
    static private List<Wireless> mDevices = new List<Wireless>();

    // receive management
    static private List<string> mMessageQueue = new List<String>();
    static List<Packet> mRecorededMessages = new List<Packet>();

    // sender daemon management
    static private List<List<Command>> mQueues = new List<List<Command>>();
    static private BackgroundWorker mSender = new BackgroundWorker();
    static private bool mApplicationExitCall = false;
    static private System.Windows.Forms.Timer mWaitTimer = new System.Windows.Forms.Timer();
    static private PersianDateTime mLastSendTimeStamp = PersianDateTime.Now;
    static private string mLastCommandText = "";

    // locks
    static private object mSenderLock = new object();
    static private object mSpeedLock = new object();
    static private object mRecvLock = new object();
    #endregion Instances and Data


    #region Properties
    /// <summary>
    /// Tells whether the CommandQueue is open and functioning or not
    /// </summary>
    static public bool IsOpen
    {
      get 
      {
        Wireless wl = GetWirelessPort(DeviceLocation.Center);
        return  wl != null && wl.Active;
      }
    }

    /// <summary>
    /// Controls minimum time in seconds between sending of commands
    /// </summary>
    static private double MinimumSendInterval = 0.3;

    /// <summary>
    /// Controls duplicate packets time based threshold. 
    /// Exact packets within the given threshold are dropped.
    /// </summary>
    static private double DuplicateThreshold = 0.05;

    /// <summary>
    /// Current command queue, in full detail.
    /// </summary>
    static public string Queue
    {
      get
      {
        StringBuilder sb = new StringBuilder();

        for(int q = mQueues.Count - 1; q >= 0; --q)
        {
          sb.AppendLine("Queue #" + q.ToString());

          for (int i = 0; i < mQueues[q].Count; ++i)
            sb.AppendLine("  " + mQueues[q][i].CommandString);
        }

        return sb.ToString();
      }
    }
    #endregion Properties


    #region Constructor
    static CommandQueue()
    {
      mWaitTimer.Interval = 25;
      mWaitTimer.Tick += WaitTimerTicked;
      mSender.DoWork += StartSenderDaemon;
      mSender.RunWorkerAsync();
    }
    #endregion Constructor


    #region Initialization and Finalization
    static private WirelessState Quelch(Wireless wlc, Wireless wlb, Wireless wlg, WirelessState result, bool fallback_fix)
    {
      wlc.Close();
      wlb.Close();
      wlg.Close();

      string old_center = Properties.Settings.Default.WirelessPortCenter;
      string old_blue = Properties.Settings.Default.WirelessPortBlue;
      string old_green = Properties.Settings.Default.WirelessPortGreen;

      if(fallback_fix)
        AssignPorts(old_center, old_blue, old_green, false);

      return result;
    }

    /// <summary>
    /// Called to assign the given wireless ports to application queue.
    /// </summary>
    /// <param name="center">COM port name for the center wireless device</param>
    /// <param name="blue">COM port name for the blue wireless device</param>
    /// <param name="green">COM port name for the green wireless device</param>
    /// <returns>Wireless state information after trying to open the ports</returns>
    static public WirelessState AssignPorts(string center, string blue, string green, bool fallback_fix = true)
    {
      // Close first
      Close(false);

      // Start ports
      Wireless wlc = new Wireless(center, DeviceLocation.Center);
      Wireless wlb = new Wireless(blue, DeviceLocation.BlueBase);
      Wireless wlg = new Wireless(green, DeviceLocation.GreenBase);

      // Check devices
      if (!wlc.Active)
        return Quelch(wlc, wlb, wlg, WirelessState.CenterNotWorking, fallback_fix);
      else if (blue != "" && wlb.Active == false)
        return Quelch(wlc, wlb, wlg, WirelessState.BlueNotWorking, fallback_fix);
      else if (green != "" && wlg.Active == false)
        return Quelch(wlc, wlb, wlg, WirelessState.GreenNotWorking, fallback_fix);

      // Add devices
      mDevices.Add(wlc);
      if (wlb.Active)
        mDevices.Add(wlb);
      if(wlg.Active)
        mDevices.Add(wlg);

      return WirelessState.Open;
    }

    /// <summary>
    /// Called in initialization to open the wireless ports.
    /// In case something goes wrong, initialization will be stopped.
    /// </summary>
    static public void Open()
    {
      if (Properties.Settings.Default.KioskMode)
        return;

      string center = Properties.Settings.Default.WirelessPortCenter;
      string blue = Properties.Settings.Default.WirelessPortBlue;
      string green = Properties.Settings.Default.WirelessPortGreen;

      WirelessState wls = AssignPorts(center, blue, green);
      if (wls == WirelessState.CenterNotWorking && Session.ReleaseBuild)
      {
        Session.ShowError("دستگاه وایرلس مرکزی فعال نشد.");
        Properties.Settings.Default.WirelessPortCenter = "";
      }
      else if (wls == WirelessState.BlueNotWorking)
      {
        Session.ShowError("دستگاه وایرلس پایگاه تیم آبی فعال نشد.");
        Properties.Settings.Default.WirelessPortBlue = "";
      }
      else if (wls == WirelessState.GreenNotWorking)
      {
        Session.ShowError("دستگاه وایرلس پایگاه تیم سبز فعال نشد.");
        Properties.Settings.Default.WirelessPortGreen = "";
      }
    }

    /// <summary>
    /// Cleans up open wireless ports and instances.
    /// </summary>
    /// <param name="application_exiting">When application is exiting
    /// no error message will be shown.</param>
    static public void Close(bool application_exiting)
    {
      if (!IsOpen)
        return;

      mApplicationExitCall = application_exiting;

      foreach (Wireless wl in mDevices)
        wl.Close();
      mDevices.Clear();
    }
    #endregion Initialization and Finalization


    #region Receiving Module
    /// <summary>
    /// This method checks whether the given message is a duplicate
    /// of previous received messages or not
    /// </summary>
    /// <param name="strMsg"></param>
    /// <returns></returns>
    static private bool CheckMessageIsDuplicate(string strMsg)
    {
      bool result = false;
      PersianDateTime now = PersianDateTime.Now;

      if (strMsg == mLastCommandText)
        return true;

      for (int i = 0; i < mRecorededMessages.Count; ++i)
        if (mRecorededMessages[i].Time.GetTimeDifference(now) > DuplicateThreshold)
          mRecorededMessages.RemoveAt(i--);
        else if (mRecorededMessages[i].Data == strMsg)
          result = true;

      return result;
    }

    /// <summary>
    /// This method is called by wireless devices to inform CommandQueue
    /// of a received message. It will check the message for duplicates 
    /// and in case of message authenticity it will be passed to Tower 
    /// to be processed.
    /// </summary>
    /// <param name="strMsg">The received message</param>
    static public void MessageReceived(string strMsg)
    {
      lock (mRecvLock)
      {
        // Do not add if it is already there
        // when received with another device
        if (CheckMessageIsDuplicate(strMsg))
          return;

        // So add the message to recorded messages
        Packet p = new Packet();
        p.Time = PersianDateTime.Now;
        p.Data = strMsg;
        mRecorededMessages.Add(p);

        // Add the message to notification queue
        mMessageQueue.Add(strMsg);
      }

      // Log it
      string str = PersianDateTime.Now.ToString(DateTimeString.DetailedTime);
      Logger.Log("(CommandQueue) [" + str + "]" + " Recv \"" + strMsg + "\"");

      // report message is received
      Session.Synchronize(new Action(NotifyMessageReceived));
    }

    static public void DeviceClosed(Wireless wl)
    {
      // Nothing to do when application is closing.
      if (mApplicationExitCall || !IsOpen)
        return;

      // Nothing to do when the port is not being used
      if (GetWirelessPort(wl.PortName) == null)
        return;

      Logger.Log("(CommandQueue) Device closed: " + wl.PortName + ", Reason: " + wl.ErrorString);

      if(wl.Location == DeviceLocation.Center)
        Properties.Settings.Default.WirelessPortCenter = "";
      else if(wl.Location == DeviceLocation.BlueBase)
        Properties.Settings.Default.WirelessPortBlue = "";
      else if(wl.Location == DeviceLocation.GreenBase)
        Properties.Settings.Default.WirelessPortGreen = "";

      // Stop game manager from running games when a device is not configured
      if(!Session.WizardMode && !mApplicationExitCall)
        DB.GameManager.EndDaemon(true);
    }

    static private void NotifyMessageReceived()
    {
      while (mMessageQueue.Count != 0)
      {
        // Cache the message
        string strMessage = mMessageQueue[0];

        // Remove the cache
        mMessageQueue.RemoveAt(0);

        // Notify the tower about the new message
        Tower._MessageReceived(strMessage);
      }
    }
    #endregion Receiving Module


    #region Sending Module
    static private void StartSenderDaemon(object sender, DoWorkEventArgs e)
    {
      // Set the sender thread
      if (Thread.CurrentThread.Name == null)
        Thread.CurrentThread.Name = "Wireless Sender Daemon";

      // Send queue till application end
      while (!mApplicationExitCall)
        lock (mSenderLock)
        {
          // The interval in milliseconds
          int ms = TinyMath.Round(Properties.Settings.Default.CommandSendInterval * 1000);

          // Wait for duration between commands to send commands
          // Or till a new command arrives
          Monitor.Wait(mSenderLock, ms);

          // Send the head queue command if there is any
          SendQueueHead();
        }
    }

    static private void SignalSenderDaemon()
    {
      lock (mSenderLock)
        Monitor.Pulse(mSenderLock);
    }

    /// <summary>
    /// This method posts the given command to the send queue. Its 
    /// then sent by the timer tick event at its regular interval.
    /// The command is added to the default queue '0'. The queue
    /// will be created if needed.
    /// </summary>
    static public void PostCommand(Command cmd)
    {
      // Do not send when the port is not open.
      if (!IsOpen)
        return;

      // Put the command in send queue
      cmd.State = CommandStatus.InQueue;
      if (mQueues.Count == 0)
        mQueues.Add(new List<Command>());
      mQueues[0].Add(cmd);

      // And signal sender to actually send it
      SignalSenderDaemon();
    }

    /// <summary>
    /// This method starts a batch operation and returns 
    /// a numerical handle to the created job.
    /// </summary>
    static public void ExecuteBatchCommands(List<Command> cmds)
    {
      // Do not send when the port is not open.
      if (!IsOpen)
        return;

      // Put the commands in batch queue
      mQueues.Add(cmds);
      int index = mQueues.Count - 1;

      // And signal sender to actually send it
      SignalSenderDaemon();

      // Wait till all the batch is sent
      EnsureQueueIsSent(index);
    }

    /// <summary>
    ///  This method directly sends the given command with minimum latency. In case
    ///  the previous commands was sent no more than 220ms ago, it will wait for the
    ///  duration to be passed. Taken in account for hardware compatibility.
    /// </summary>
    /// <param name="cmd">The command to be directly sent</param>
    static public void SendCommand(Command cmd)
    {
      // Do not send when a port is not available
      Wireless wl = GetWirelessPort(cmd.PreferedLocation);
      if (wl == null)
        return;

      try
      {
        // Wait a little if sending too fast.
        lock (mSpeedLock)
        {
          double diff = mLastSendTimeStamp.GetTimeDifference(PersianDateTime.Now);
          diff = MinimumSendInterval - diff; // Force minimum time between sending of commands
          if (diff > 0.0)
            System.Threading.Thread.Sleep(TinyMath.Round(diff * 1000));
        }

        // Send the command
        mLastCommandText = cmd.CommandString;
        cmd.Send(wl.PortInstance);
        mLastSendTimeStamp = PersianDateTime.Now;
        Tower._CommandSent(cmd);

        // Log the sent command
        string strTime = cmd.LastTryTime.ToString(DateTimeString.DetailedTime);
        Logger.Log("(CommandQueue) [" + strTime + "]" + " Sent \"" + cmd.CommandString + "\"");
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "CommandQueue", "SendCommand");
        Properties.Settings.Default.WirelessPortCenter = "";
        Properties.Settings.Default.WirelessPortBlue = "";
        Properties.Settings.Default.WirelessPortGreen = "";
        Close(false);
      }
    }

    /// <summary>
    /// This method sends the given string directly without any delay.
    /// For debug purposes
    /// </summary>
    /// <param name="str">The string to be sent</param>
    static public void SendString(string str)
    {
      if (!IsOpen)
        return;

      str += (char)13;
      string strTime = PersianDateTime.Now.ToString(DateTimeString.DetailedTime);
      Logger.Log("(CommandQueue) [" + strTime + "]" + " Sent \"" + str + "\"");
      mDevices[0].PortInstance.Write(str);
    }
    #endregion Sending Module


    #region Queue Management
    /// <summary>
    /// Regulates sending of commands. Commands in send queue 
    /// are only sent in the timer's tick interval.
    /// </summary>
    static private void SendQueueHead()
    {
      try
      {
        Command cmd = null;
        PersianDateTime now = PersianDateTime.Now;

        // select the oldest command in queue to be sent
        // but according to queue order
        for (int q = mQueues.Count - 1; q >= 0; --q)
        {
          for (int i = 0; i < mQueues[q].Count; ++i)
            if (mQueues[q][i].State == CommandStatus.Sent &&
               mQueues[q][i].TryCount >= Properties.Settings.Default.CommandMaxTries)
            {
              // update command status
              mQueues[q][i].State = CommandStatus.TimedOut;

              // log the timed out message
              string strTime = PersianDateTime.Now.ToString(DateTimeString.DetailedTime);
              Logger.Log("(CommandQueue) [" + strTime + "]" + " TOut \"" + mQueues[q][i].CommandString + "\"");

              // inform Tower of time out
              Tower._CommandTimedOut(mQueues[q][i]);

              // remove the command
              if (q > 0)
                RemoveFromQueues(q, i--);
              else
                RemoveFromQueues(q);
            }
            else
            {
              cmd = mQueues[q][i];
              break;
            }

          if (cmd != null)
            break;
        }

        // A command is found? send it
        if (cmd != null)
          SendCommand(cmd);
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "CommandQueue", "TimerTicked");
        Logger.Log("(CommandQueue) Queue: ");
        Logger.Log(Queue);
      }
    }

    /// <summary>
    /// This method is responsible to remove the given command.
    /// Used by DeviceBase after it has been processed.
    /// </summary>
    /// <param name="type">Type of the command</param>
    /// <param name="name">Its recipient</param>
    static public void Remove(Commands type, DeviceID name)
    {
      for(int q = mQueues.Count - 1; q >= 0 && q < mQueues.Count; --q)
        for (int i = 0; i < mQueues[q].Count; ++i)
          if (mQueues[q][i].Type == type && mQueues[q][i].Recipient == name)
          {
            mQueues[q][i].State = CommandStatus.Processed;
            RemoveFromQueues(q, i);
            return;
          }
    }

    /// <summary>
    /// This method removes the given command from the given queue.
    /// If the queue becomes empty, it will be removed.
    /// </summary>
    /// <param name="qindex">Index of the queue that the command is in</param>
    /// <param name="cindex">Index of the command that the queue is in.
    /// In case this is not set, whole queue will be removed</param>
    static private void RemoveFromQueues(int qindex, int cindex = -1)
    {
      // Remove the command first
      if (cindex == -1)
        mQueues[qindex].Clear();
      else
        mQueues[qindex].RemoveAt(cindex);

      // Removed the queue if it has become empty
      // Only in case that there is no other queue
      // on top of the queue that contianed the message
      if (mQueues[qindex].Count == 0)
      {
        bool remove_queue = true;
        for (int q = qindex + 1; q < mQueues.Count; --q)
          if (mQueues[q].Count != 0)
          {
            remove_queue = false;
            break;
          }

        if(remove_queue)
          mQueues.RemoveAt(qindex);
      }
    }
    #endregion Queue Management


    #region Utilities
    static private void WaitTimerTicked(Object sender, EventArgs e)
    {
    }

    /// <summary>
    /// This method waits for the given approximate time.
    /// Application will be responsive when waiting.
    /// </summary>
    /// <param name="seconds">Time to wait, expressed in seconds</param>
    static public void SleeplessWait(double seconds)
    {
      DateTime start = DateTime.Now;
      DateTime now;

      mWaitTimer.Start();
      do
      {
        now = DateTime.Now;
        Application.DoEvents();
      } while((now - start).TotalSeconds < seconds);
      mWaitTimer.Stop();
    }

    /// <summary>
    /// This method makes sure that all of the commands for the given queue
    /// is sent. If no queue is specified, application queue instance is 
    /// considered to be the queue.
    /// </summary>
    static public void EnsureQueueIsSent(int qid = 0)
    {
      while (mQueues.Count > qid && !Desktop.Desktop.Instance.IsDisposed)
        Application.DoEvents();
    }

    /// <summary>
    /// Updates send timer interval based on current application settings.
    /// </summary>
    static public void UpdateTimerInterval()
    {
      SignalSenderDaemon();
    }

    /// <summary>
    /// Tries to return a working serial port instance for the given 
    /// location. Will return center wireless port if the requested 
    /// port is not found, that is if the center exists.
    /// </summary>
    static private Wireless GetWirelessPort(DeviceLocation loc)
    {
      for (int i = 0; i < mDevices.Count; ++i)
        if (mDevices[i].Location == loc)
          return mDevices[i];

      if (loc != DeviceLocation.Center)
        return GetWirelessPort(DeviceLocation.Center);

      return null;
    }

    /// <summary>
    /// This method returns the wireless instance for the given port
    /// name if there is such configured and running device.
    /// </summary>
    static public Wireless GetWirelessPort(string port_name)
    {
      for (int i = 0; i < mDevices.Count; ++i)
        if (mDevices[i].PortName == port_name)
          return mDevices[i];

      return null;
    }
    #endregion Utilities
  }
}
