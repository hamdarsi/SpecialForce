﻿using System;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace SpecialForce.ControlTower
{
  public partial class FormGameStart : SessionAwareForm
  {
    private int mCountDown = -1;
    private bool mCloseAuthorized = false;
    private Kevlar mCurrentKevlar = null;
    private CaptureThread mThread = null;
    private FormSendMessage mMsgForm = null;
    private SearchThread mFingerSearchThread = null;
    private List<KevlarIcon> mKevlars = new List<KevlarIcon>();
    private List<PictureBox> mPictures = new List<PictureBox>();
    private Dictionary<PictureBox, ToolTip> mHints = new Dictionary<PictureBox, ToolTip>();

    static private FormGameStart mInstance = null;

    private FormGameStart()
    {
      InitializeComponent();
    }

    static public void StartSetup()
    {
      if (mInstance == null || mInstance.IsDisposed)
      {
        mInstance = new FormGameStart();
        mInstance.Show();
      }

      mInstance.BringToFront();
      mInstance.Focus();
    }

    private void AddPictureBox(Control parent, int left, DB.User user)
    {
      PictureBox pb = new PictureBox();
      pb.Parent = parent;
      pb.Size = new Size(90, 120);
      pb.Location = new Point(left, 35);
      pb.MouseDown += PictureBoxMouseDown;
      pb.Tag = user;
      pb.SizeMode = PictureBoxSizeMode.CenterImage;
      pb.Image = Properties.Resources.ControlTower_Cross;
      pb.MouseEnter += ActivateTooltip;
      pb.MouseLeave += DeActivateTooltip;
      mPictures.Add(pb);

      mHints[pb] = null;
      if (user != null)
      {
        mHints[pb] = new ToolTip();
        mHints[pb].SetToolTip(pb, user.FullName);
      }
    }

    private void ActivateTooltip(object sender, EventArgs e)
    {
      PictureBox pb = sender as PictureBox;
      if (pb != null && mHints[pb] != null)
        mHints[pb].Active = true;
    }

    private void DeActivateTooltip(object sender, EventArgs e)
    {
      PictureBox pb = sender as PictureBox;
      if (pb != null && mHints[pb] != null)
        mHints[pb].Active = false;
    }

    private void ShowUserPhoto(DB.User user)
    {
      foreach(PictureBox pb in mPictures)
        if(pb.Tag == user)
        {
          Bitmap bmp = user.Photo.Resize(pb.Size);
          Graphics canvas = Graphics.FromImage(bmp);

          if(DB.GameManager.CurrentGame.PresentUsers.Contains(user))
            canvas.DrawImageUnscaled(0, 0, Properties.Resources.ControlTower_Player_Present);
          else
            canvas.DrawImageUnscaled(0, 0, Properties.Resources.ControlTower_Player_Not_Present);

          pb.Image = bmp;
        }
    }

    private void AddKevlar(int left, int top, Kevlar kev)
    {
      KevlarIcon ki = new KevlarIcon();
      ki.Parent = pnlIcons;
      ki.Left = left;
      ki.Top = top;
      ki.Kevlar = kev;
      ki.DragEnter += KevlarIcon_DragEnter;
      ki.DragDrop += KevlarIcon_DragDrop;
      ki.MouseDown += KevlarIcon_MouseDown;
      mKevlars.Add(ki);
    }

    private void CreateTeamPanel(ToolBar parent, List<DB.User> players)
    {
      int l = 585;
      DB.Game game = DB.GameManager.CurrentGame;
      for (int i = 0; i < 7; ++i)
      {
        // Create their photo frames
        DB.User player = i < players.Count ? players[i] : null;
        AddPictureBox(parent, l, player);

        l -= 96;

        if (player != null)
        {
          // If no fingerprint present then all players can play
          if (!Properties.Settings.Default.FingerPrintModuleEnabled)
            game.PlayerEntered(player);

          // Show player's photo
          ShowUserPhoto(player);
        }
      }
    }

    protected override void ApplyPermissions(Post post)
    {
      SystemLockStateChanged(null, null);
      tbGameSettings.Enabled = Session.CurrentStaff != null && Session.CurrentStaff.IsPrivileged();
    }

    private void SystemLockStateChanged(Object sender, EventArgs e)
    {
      Enabled = Session.CurrentStaff != null && !Session.CurrentStaff.IsLowlyUser();
      txtBombTime.Visible = false;
      txtBombTime.Visible = true;
    }

    private void FormGameStart_Load(object sender, EventArgs e)
    {
      // List of things to do:
      // ==================================================
      //   1 - Get the current game and round from DB.GameManager
      //   2 - Create panels for blue and green team players
      //   3 - Create panels for all kevlars
      //   4 - Put the bomb on the correct team panel
      //   5 - Load game settings
      //   6 - Register event handlers for game, finger prints and session
      //   7 - Rest!
      //   8 - Check for lock state

      try
      {
        // 1 - Get the current game and round from DB.GameManager
        DB.Game game = DB.GameManager.CurrentGame;
        DB.Round round = game.CurrentRound;

        // 2 - Create panels for players
        SuspendLayout();
        CreateTeamPanel(tbBlueUsers, game.BlueUsers);
        CreateTeamPanel(tbGreenUsers, game.GreenUsers);

        // 3 - Create panels for kevlars
        for (int i = 0; i < Kevlars.CountForTeams; ++i)
          AddKevlar(852 - i * 94, 10, Tower.Kevlars[i]);

        for (int i = Kevlars.CountForTeams; i < Kevlars.TotalCount; ++i)
          AddKevlar(852 - (i - Kevlars.CountForTeams) * 94, 140, Tower.Kevlars[i]);

        // 4 - Put the bomb on the correct team panel
        tbBlueUsers_DragDrop(round.TerroristsAreBlue ? tbBlueUsers : tbGreenUsers, null);
        ResumeLayout();

        // 5 - Load game settings
        if(!Session.ReleaseBuild)
          txtBombTime.Minimum = 10;
        cmbGameObject.Items.Add(Enums.ToString(GameObject.Bombing));
        cmbGameObject.Items.Add(Enums.ToString(GameObject.DeathMatch));
        cmbGameObject.SelectedIndex = game.Object == GameObject.DeathMatch ? 1 : 0;
        txtRoundCount.Value = game.RoundCount;
        txtRoundTime.Value = game.RoundTime / 60; // convert to minutes
        txtStartHP.Value = game.StartHP;
        txtStartMagazine.Value = game.StartMagazine;
        txtAmmoPerMagazine.Value = game.AmmoPerMagazine;
        txtBombTime.Value = game.BombTime;
        txtRestTime.Value = game.RestTime;
        cmbFriendlyFire.SelectedIndex = Enums.IndexFromFriendlyFire(game.FriendlyFire);
        btnBomb.Parent = round.TerroristsAreBlue ? tbBlueUsers : tbGreenUsers;
        btnBomb.Visible = game.Object == GameObject.Bombing;
        lblBlueTeam.Text = "اعضا تیم آبی: " + DB.Team.GetTeamName(game.BlueTeamID, "");
        lblGreenTeam.Text = "اعضا تیم سبز: " + DB.Team.GetTeamName(game.GreenTeamID, "");

        // 6 - Register event handlers for finger prints
        mThread = FingerPrint.OpenDeviceAsThread(0);
        mThread.OnCapture += FingerPrintCaptured;
        Session.OnSessionLocked += SystemLockStateChanged;

        // 7 - Rest!
        mCountDown = game.RestNeeded ? game.RestTime : 1;

        // 8 - Check for lock state
        SystemLockStateChanged(null, null);
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "FormGameStart", "OnLoad");
        DB.GameManager.AbortCurrentGame("مشکلی در اجرای راند پیش آمد." + Environment.NewLine + " بازی متوقف شد.");
      }
    }

    public void FingerPrintCaptured(object sender, CaptureEventArgs e)
    {
      mFingerSearchThread = FingerPrint.SearchWithThread(e.Data);
      mFingerSearchThread.OnSearchFinish += FingerPrintSearchFinish;

      mFingerSearchThread.Start();
    }

    public void FingerPrintSearchFinish(Object sender, SearchFinishEventArgs args)
    {
      if (args.Succeeded)
      {
        DB.User player = args.User;
        DB.GameManager.CurrentGame.PlayerEntered(player);
        ShowUserPhoto(player);
      }
      else
        Session.ShowError("کاربری با اثر انگشت وارد شده یافت نشد.");
    }

    private void PictureBoxMouseDown(Object sender, MouseEventArgs e)
    {
      DB.User user = (sender as Control).Tag as DB.User;
      if (user == null)
        return;
      else if(DB.GameManager.CurrentGame.PresentUsers.Contains(user))
        DoDragDrop(user, DragDropEffects.Move);
    }

    private void KevlarIcon_DragEnter(object sender, DragEventArgs e)
    {
      if (e.Data.GetDataPresent(typeof(DB.User)))
        e.Effect = DragDropEffects.Move;
    }

    private void KevlarIcon_DragDrop(object sender, DragEventArgs e)
    {
      DB.User user = e.Data.GetData(typeof(DB.User)) as DB.User;
      KevlarIcon kevlar = sender as KevlarIcon;

      DB.Game game = DB.GameManager.CurrentGame;
      TeamType user_team = game.BlueUserIDs.Contains(user.ID) ? TeamType.Blue : TeamType.Green;
      if(kevlar.Kevlar.Team != user_team)
      {
        string team_str = Command.ToString(user_team);
        Session.ShowError(user.FullName + " عضو تیم " + team_str + " است و باید لباس " + team_str + " رنگ را به او داد.");
        return;
      }

      foreach (KevlarIcon ki in mKevlars)
        if (ki.Player == user)
          ki.Kevlar.Player = null;

      kevlar.Kevlar.Player = user;
    }

    private void cmbGameObject_SelectedIndexChanged(object sender, EventArgs e)
    {
      lblBombTime.Enabled = Enums.ParseGameObject(cmbGameObject.SelectedItem.ToString()) == GameObject.Bombing;
      txtBombTime.Enabled = lblBombTime.Enabled;
      btnBomb.Visible = lblBombTime.Enabled;

      Refresh();
    }

    private bool AskForPlayer(string name)
    {
      string text = "به " + name + " هیچ لباسی داده نشده است.";
      text += "آیا میخواهید ادامه دهید؟";
      return Session.Ask(text);
    }

    private bool AskClosingPermission()
    {
      if(!Session.Ask("آیا واقعا میخواهید از اجرای بازی جلو گیری کنید؟"))
        return false;

      mCloseAuthorized = true;
      DB.GameManager.AbortCurrentGame("اجرای بازی توسط اپراتور متوقف شد.");
      return true;
    }

    private void btnAbort_Click(object sender, EventArgs e)
    {
      if (AskClosingPermission())
        Close();
    }

    private void FormGameStart_FormClosing(object sender, FormClosingEventArgs e)
    {
      try
      {
        if (!mCloseAuthorized)
          AskClosingPermission();

        if (mCloseAuthorized)
        {
          mThread.OnCapture -= FingerPrintCaptured;
          Session.OnSessionLocked -= SystemLockStateChanged;
        }
        else
          e.Cancel = true;
      }
      catch (Exception ex)
      {
        try
        {
          Logger.Log("(FormGameStart) OnFormClosing. IsDisposed: " + IsDisposed);
        }
        catch
        { 
          Logger.Log("(FormGameStart) OnFormClosing: iSDisposed usage exception"); 
        }

        Logger.LogException(ex, "FormGameStart", "OnFormClosing");
      }
    }

    private void btnBomb_OnDragStart(object sender, DragObjectRequestArgs e)
    {
      e.Object = btnBomb;
    }

    private void tbBlueUsers_DragEnter(object sender, DragEventArgs e)
    {
      ToolButton btn = e.Data.GetData(typeof(ToolButton)) as ToolButton;
      if (btn != null && btn == btnBomb)
        e.Effect = DragDropEffects.Move;
    }

    private void tbBlueUsers_DragDrop(object sender, DragEventArgs e)
    {
      btnBomb.Parent = sender as ToolBar;
      btnBomb.BringToFront();
      string hint = sender == tbBlueUsers ? "آبی" : "سبز";
      hint = "در این راند تیم " + hint + " تروریست است.\n\r";
      hint += "برای تغییر تیم تروریست مکان بمب را عوض کنید";
      btnBomb.Hint = hint;

      Refresh();
    }

    /// <summary>
    /// Prepares the round to be started.
    /// List of things to do:
    /// ==================================================
    ///   1 - Get the current game and round
    ///   2 - Create a list of players for each team
    ///   3 - Check if a player has not been assigned a kevlar
    ///   4 - Update game and round settings
    ///   5 - Pass the control to DB.GameManager.CurrentRoundReadyStartIt()
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnStart_Click(object sender, EventArgs e)
    {
      try
      {
        // 1 - Get the current game and round
        DB.Game game = DB.GameManager.CurrentGame;
        DB.Round round = game.CurrentRound;

        // 2 - Create a list of players for each team
        List<DB.User> blue_team = new List<DB.User>();
        List<DB.User> green_team = new List<DB.User>();
        foreach (KevlarIcon k in mKevlars)
          if (k.Player != null)
            if (game.BlueUserIDs.Contains(k.Player.ID))
              blue_team.Add(k.Player);
            else
              green_team.Add(k.Player);

        // 3 - Check if a player has not been assigned a kevlar
        if (game.Type != CompetitionType.Dummy)
        {
          foreach (int player_id in game.BlueUserIDs)
            if (blue_team.Count(player => player.ID == player_id) == 0 && !AskForPlayer(DB.User.GetUserFullName(player_id)))
              return;

          foreach (int player_id in game.GreenUserIDs)
            if (green_team.Count(player => player.ID == player_id) == 0 && !AskForPlayer(DB.User.GetUserFullName(player_id)))
              return;
        }

        // 4 - Update game and round settings
        game.Object = cmbGameObject.SelectedIndex == 0 ? GameObject.Bombing : GameObject.DeathMatch;
        game.RoundCount = (int)txtRoundCount.Value;
        game.RoundTime = (int)txtRoundTime.Value * 60; // convert to seconds
        game.StartHP = (int)txtStartHP.Value;
        game.StartMagazine = (int)txtStartMagazine.Value;
        game.AmmoPerMagazine = (int)txtAmmoPerMagazine.Value;
        game.BombTime = (int)txtBombTime.Value;
        game.RestTime = (int)txtRestTime.Value;
        game.FriendlyFire = Enums.FriendlyFireFromString(cmbFriendlyFire.SelectedItem.ToString());
        game.Apply();

        round.AssignPlayersAndKevlars(blue_team, green_team);
        round.TerroristsAreBlue = btnBomb.Parent == tbBlueUsers; // This also applies.

        // 5 - Pass the control to DB.GameManager.CurrentRoundReadyStartIt()
        Session.Synchronize(new Action(DB.GameManager.CurrentRoundReadyStartIt));
        mCloseAuthorized = true;
        Close();
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "FormGameStart", "btnStartClicked");
        Session.ShowError("در هنگام اجرای راند خطایی رخ داد.");
      }
    }

    private void mnuRemoveKevlar_Click(object sender, EventArgs e)
    {
      mCurrentKevlar.Player = null;
    }

    private void KevlarIcon_MouseDown(object sender, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Right)
        return;

      mCurrentKevlar = (sender as KevlarIcon).Kevlar;
      if(mCurrentKevlar.Player != null)
        mnuKevlarIcon.Show(Cursor.Position, ToolStripDropDownDirection.BelowLeft);
    }

    private void tmRest_Tick(object sender, EventArgs e)
    {
      DB.Game game = DB.GameManager.CurrentGame;
      DB.Round round = game != null ? game.CurrentRound : null;

      if (round == null)
        return;

      string num_string = round.Number.ToString();
      if (mCountDown <= 0)
      {
        // Enable starting the game
        tmRest.Stop();
        btnStart.Enabled = true;

        // update status on form
        Text = "در انتظار شروع راند " + num_string;
        Tower.Broadcast("Please wait for", "game to start.");
        return;
      }

      if (mCountDown % 10 == 0 && mCountDown != 0)
        Tower.Broadcast(DB.Round.TimeToString(mCountDown) + " to start...");

      Text = mCountDown--.ToString() + " ثانیه استراحت تا شروع راند " + num_string;
    }

    static public void CloseSetup()
    {
      if (mInstance != null && !mInstance.IsDisposed)
      {
        mInstance.Close();
        mInstance.Dispose();
        mInstance = null;
      }
    }

    private void OpenMessaging(DeviceID receiver)
    {
      if (mMsgForm != null && !mMsgForm.IsDisposed)
      {
        mMsgForm.Close();
        mMsgForm.Dispose();
      }

      mMsgForm = new FormSendMessage();
      mMsgForm.ReceiverID = receiver;
      mMsgForm.Show();
    }

    private void btnSendMessage_Click(object sender, EventArgs e)
    {
      OpenMessaging(DeviceID.AllDevices);
    }

    private void mnuSendMessage_Click(object sender, EventArgs e)
    {
      OpenMessaging(mCurrentKevlar.ID);
    }
  }
}
