﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SpecialForce.ControlTower
{
  public class Bomb : DeviceBase
  {
    #region Bomb state relative properties
    private BombState mState;
    public BombState State
    {
      get { return mState; }
    }

    public bool Exploded
    {
      get { return mState == BombState.Exploded; }
    }

    public bool Planted
    {
      get { return mState == BombState.Planted; }
    }

    public bool Diffused
    {
      get { return mState == BombState.Diffused; }
    }

    public bool Ready
    {
      get { return mState != BombState.Disabled; }
    }
    #endregion Bomb state relative properties


    #region Information of interaction with the bomb
    private bool mIsResetInProgress = false;

    private DeviceID mPlanter = DeviceID.Unknown;
    public DeviceID PlanterID
    {
      get { return mPlanter; }
    }

    public Kevlar PlanterKevlar
    {
      get { return Tower.GetDeviceInstance(mPlanter) as Kevlar; }
    }

    public DB.User PlanterUser
    {
      get { return Kevlars.UserFromID(mPlanter); }
    }

    public int PlanterIndex
    {
      get { return Kevlars.IndexFromID(mPlanter); }
    }

    public int PlanterNumber
    {
      get { return Kevlars.NumberFromID(mPlanter); }
    }

    private DeviceID mDiffuser = DeviceID.Unknown;
    public DeviceID DiffuserID
    {
      get { return mDiffuser; }
    }

    public Kevlar DiffuserKevlar
    {
      get { return Tower.GetDeviceInstance(mDiffuser) as Kevlar; }
    }

    public DB.User DiffuserUser
    {
      get { return Kevlars.UserFromID(mDiffuser); }
    }

    public int DiffuserIndex
    {
      get { return Kevlars.IndexFromID(mDiffuser); }
    }

    public int Number
    {
      get { return Kevlars.NumberFromID(mDeviceID); }
    }

    public int DiffuserNumber
    {
      get { return Kevlars.NumberFromID(mDiffuser); }
    }

    private TeamType mTerroristTeam;
    public TeamType TerroristTeam
    {
      get { return mTerroristTeam; }
      set { mTerroristTeam = value; }
    }

    private int mBombTimer;
    public int BombTimer
    {
      get { return mBombTimer; }
      set { mBombTimer = value; }
    }
    #endregion Information of interaction with the bomb


    #region Constructor
    public Bomb(DeviceID id)
    {
      Clear();

      ID = id;
    }
    #endregion Constructor


    #region Utilities
    private void Clear()
    {
      ClearPingStats();
      ClearCommunicationCache();

      mBombTimer = 60;
      mTerroristTeam = TeamType.Blue;
      mPlanter = DeviceID.Unknown;
      mDiffuser = DeviceID.Unknown;
      mState = BombState.Disabled;
    }

    public void PlaySoundOnDevice(int number)
    {
      Command cmd = new Command(Commands.PlaySound, mDeviceID);
      cmd.AddParameter(number, 2);
      CommandQueue.SendCommand(cmd);
    }
    #endregion Utilities


    #region Functionality
    protected override Command CreateConfigureCommand()
    {
      Command cmd = new Command(Commands.ConfigureBomb, mDeviceID);
      cmd.AddParameter(mTerroristTeam == TeamType.Blue ? 0 : 1, 1);
      cmd.AddParameter(mBombTimer, 3);
      return cmd;
    }

    protected override bool ProcessMessageInternal(Commands cmd, List<int> prms)
    {
      bool signal = true;

      if (cmd == Commands.ConfigureBomb)
      {
        mConfigured = true;
        signal = false;
      }
      else if (cmd == Commands.RequestGameState)
      {
        mIsResetInProgress = true;
        DB.Game game = DB.GameManager.CurrentGame;
        DB.Round round = game != null ? game.CurrentRound : null;
        int index = -1;
        for (int i = 0; i < Tower.Bombs.Count; ++i)
          if (Tower.Bombs[i].Number == Number)
          {
            index = i;
            break;
          }

        if (round != null && round.State == RoundState.InProgress)
          if (round.BombState != BombState.Planted || round.PlantedBomb == index)
          {
            // Log the reset event
            string strLog = "در بازی " + game.ID.ToString();
            strLog += " در راند " + round.Number.ToString();
            strLog += "(" + round.ID.ToString() + ") ";
            strLog += ToString() + " ریست شد.";
            Logger.Log(strLog);

            Configure();
            SendGameState(round);
          }

        // Inform tower of the device reset.
        Tower.SignalDeviceReset(this);
        mIsResetInProgress = false;
        signal = false;
      }
      else if (cmd == Commands.CheatBomb)
        signal = false;
      else if (cmd == Commands.SendUpdate)
      {
        BombState new_state = (BombState)prms[0];
        DeviceID actr = Kevlars.KevlarIDFromNumber(prms[1]);


        if (new_state == mState || mIsResetInProgress)
          signal = false;
        else
        {
          if (new_state == BombState.Planted && actr != DeviceID.AllDevices)
            mPlanter = actr;
          else if (new_state == BombState.Diffused && actr != DeviceID.AllDevices)
            mDiffuser = actr;
          else
            signal = false;

          mState = new_state;
        }
      }
      else
        return false;

      if(signal)
        Tower.SignalBombIsTouched();

      return true;
    }
    #endregion Functionality


    #region Remote Control
    public void CheatState(BombState state, DeviceID player)
    {
      Command cmd = new Command(Commands.CheatBomb, mDeviceID);
      cmd.AddParameter((int)state, 1);
      cmd.AddParameter((int)player, 2);

      ExecuteCommand(cmd);
      CommandQueue.EnsureQueueIsSent();

      if (cmd.State == CommandStatus.Processed)
      {
        mState = state;
        if (mState == BombState.Planted)
        {
          mPlanter = player;
          Tower.SignalBombIsTouched();
        }
        else if (mState == BombState.Diffused)
        {
          mDiffuser = player;
          Tower.SignalBombIsTouched();
        }
      }
    }

    public void ManualStateUpdate(BombState bs, bool critical_update = false)
    {
      mState = bs;
      Tower.SignalDeviceUpdated(mDeviceID);

      if (critical_update)
        Tower.SignalBombIsTouched();
    }

    public void SendGameState(DB.Round round)
    {
      Command cmd = new Command(Commands.GameState, mDeviceID);
      cmd.AddParameter((int)mState, 1);
      cmd.AddParameter(round.RemainingBombTime, 3);
      ExecuteCommand(cmd);
    }
    #endregion Remote Control
  }
}
