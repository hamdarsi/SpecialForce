﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

namespace SpecialForce.ControlTower
{
  public static class Tower
  {
    #region Data
    /// <summary>
    /// Maps UserIDs to their active kevlar DeviceIDs
    /// </summary>
    static private Dictionary<DeviceID, int> mKevlarsAndUsers = new Dictionary<DeviceID, int>();
    #endregion Data


    #region Properties
    static private double HammerTime
    {
      get { return 0.5; }
    }

    static private List<Kevlar> mKevlars = new List<Kevlar>();
    static public List<Kevlar> Kevlars
    {
      get { return mKevlars; }
    }

    static private List<Bomb> mBombs = new List<Bomb>();
    static public List<Bomb> Bombs
    {
      get { return mBombs; }
    }

    static public List<DeviceBase> AllDevices
    {
      get
      {
        List<DeviceBase> result = new List<DeviceBase>();
        result.AddRange(mKevlars);
        result.AddRange(mBombs);
        return result;
      }
    }

    static public Bomb ActiveBomb
    {
      get { return mBombs[0].State >= BombState.Planted ? mBombs[0] : mBombs[1]; }
    }
    #endregion Properties


    #region Events
    static public event EventHandler<DeviceEventArgs> OnDeviceUpdated;
    static public event EventHandler<PlayerDiedArgs> OnPlayerDied;
    static public event EventHandler<MessageReceivedArgs> OnMessageReceived;
    static public event EventHandler<MessageReceivedArgs> OnMessageError;
    static public event EventHandler<MessageReceivedArgs> OnInternalMessageReceived;
    static public event EventHandler<CommandArgs> OnCommandTimedOut;
    static public event EventHandler<CommandArgs> OnCommandSent;
    static public event EventHandler<DeviceEventArgs> OnDeviceReset;
    #endregion Events


    #region Singleton Management
    static public void Start()
    {
      // Command Queue
      CommandQueue.Open();

      // Kevlars
      for (int i = 0; i < SpecialForce.Kevlars.TotalCount; ++i)
        mKevlars.Add(new Kevlar(i + 1));

      // Bombs
      mBombs.Add(new Bomb(DeviceID.Bomb1));
      mBombs.Add(new Bomb(DeviceID.Bomb2));
    }

    static public void Stop()
    {
      CommandQueue.Close(true);
    }
    #endregion Singleton Management


    #region Execution
    /// <summary>
    /// This method is used to turn on all the devices simultaneously 
    /// and start the round. Sends the turn on command three times in
    /// a row. Before sending them, It will give a brief window to 
    /// analyze pending kevlar messages.
    /// </summary>
    static public void TurnOnAll()
    {
      // The command of the gods to turn on all devices
      Command cmd = new Command(Commands.State, DeviceID.AllDevices);
      cmd.AddParameter((int)KevlarState.Working, 2);
      int ht = TinyMath.Round(HammerTime * 1000);

      // What?! You want to turn on everything? wait for their previous messages
      // Then send the damn command
      CommandQueue.SleeplessWait(HammerTime);
      CommandQueue.SendCommand(cmd);

      // Making sure of it
      System.Threading.Thread.Sleep(ht);
      CommandQueue.SendCommand(cmd);

      // One last try
      System.Threading.Thread.Sleep(ht);
      CommandQueue.SendCommand(cmd);

      // start current round too
      DB.GameManager.CurrentGame.CurrentRound.RoundStarted();

      // Now wait for all kevlars to send their remaining commands -- if any --
      CommandQueue.SleeplessWait(HammerTime);
    }

    /// <summary>
    /// This method is used to simultaneously turn off all devices and disconnect
    /// the communication pipeline. Sends the turn off command three times in a
    /// row. There will be a brief windows before the first signal and after the 
    /// last signal to process pending device messages.
    /// </summary>
    static public void TurnOffAll()
    {
      // Stop the round timer
      DB.GameManager.StopRoundTimer();

      // Create turn off command
      Command cmd = new Command(Commands.State, DeviceID.AllDevices);
      cmd.AddParameter((int)KevlarState.Disabled, 2);
      int ht = TinyMath.Round(HammerTime * 1000);


      // Provide a brief window and signal turn off the first time
      CommandQueue.SleeplessWait(HammerTime);
      CommandQueue.SendCommand(cmd);

      // Broadcast turn off signal again
      System.Threading.Thread.Sleep(ht);
      CommandQueue.SendCommand(cmd);

      // One last time
      System.Threading.Thread.Sleep(ht);
      CommandQueue.SendCommand(cmd);

      // Provide a brief window after turning off devices
      CommandQueue.SleeplessWait(HammerTime);

      // Disconnect the pipeline
      DisconnectPipeline();
    }

    /// <summary>
    /// This method turns on the given device id.
    /// </summary>
    /// <param name="id">The device ID to turn on</param>
    /// <returns>True if successfully turned on the requested device, false otherwise</returns>
    static public bool TurnOn(DeviceID id)
    {
      Command cmd = DeviceBase.CreateTurnOnCommand(id);
      CommandQueue.PostCommand(cmd);
      CommandQueue.EnsureQueueIsSent();
      return cmd.State == CommandStatus.Processed;
    }

    /// <summary>
    /// This method turns off the given device.
    /// </summary>
    /// <param name="id">The ID of the device to turn off</param>
    /// <param name="quickie">If passed true, one turn off command will be
    /// sent promptly and nothing else will be done. But in case false is passed,
    /// the turn off command is posted and waited for its process. Upon execution
    /// the device will be forcefully set state.</param>
    static public void TurnOff(DeviceID id, bool quickie)
    {
      // Send turn off message
      Command cmd = DeviceBase.CreateTurnOffCommand(id);
      if (quickie)
        CommandQueue.SendCommand(cmd);
      else
      {
        CommandQueue.PostCommand(cmd);

        // force kevlar disable
        DeviceBase dev = GetDeviceInstance(id);
        if (dev is Kevlar)
          (dev as Kevlar).ManualStateUpdate(KevlarState.Disabled);
        else if (dev is Bomb)
          (dev as Bomb).ManualStateUpdate(BombState.Disabled);
      }
    }

    /// <summary>
    /// Broadcasts the given message lines to all devices.
    /// </summary>
    static public void Broadcast(string line1, string line2 = "")
    {
      Command cmd = new Command(Commands.SendMessage, DeviceID.AllDevices);
      cmd.AppendMessage(line1, line2);
      CommandQueue.SendCommand(cmd);
    }

    /// <summary>
    /// This method detaches all players from their kevlars and reset kevlar scores.
    /// </summary>
    static public void DetachPlayers()
    {
      foreach (Kevlar k in mKevlars)
      {
        // Reset frag and death count on kevlars
        k.ResetScore();

        // Detach player from kevlar
        k.Player = null;
      }
    }
    #endregion Execution


    #region Communication Pipeline
    static public void ConnectPipeline()
    {
      foreach (Kevlar k in mKevlars)
        k.Responsive = true;

      foreach (Bomb b in mBombs)
        b.Responsive = true;
    }

    static public void DisconnectPipeline()
    {
      foreach (Kevlar k in mKevlars)
        k.Responsive = false;

      foreach (Bomb b in mBombs)
        b.Responsive = false;
    }
    #endregion Communication Pipeline


    #region Instance Management
    static public DeviceID DeviceIDFromString(string str)
    {
      if (str == "B1")
        return DeviceID.Bomb1;
      else if (str == "B2")
        return DeviceID.Bomb2;
      else if (str == "All")
        return DeviceID.AllDevices;

      try
      {
        return (DeviceID)(Convert.ToInt32(str));
      }
      catch
      {
        return DeviceID.Unknown;
      }
    }

    static public List<DeviceID> GetAllDeviceIDs()
    {
      List<DeviceID> result = new List<DeviceID>();
      result.Add(DeviceID.KevlarBlue1);
      result.Add(DeviceID.KevlarBlue2);
      result.Add(DeviceID.KevlarBlue3);
      result.Add(DeviceID.KevlarBlue4);
      result.Add(DeviceID.KevlarBlue5);
      result.Add(DeviceID.KevlarBlue6);
      result.Add(DeviceID.KevlarBlue7);
      result.Add(DeviceID.KevlarBlue8);
      result.Add(DeviceID.KevlarBlue9);
      result.Add(DeviceID.KevlarBlue10);
      result.Add(DeviceID.KevlarGreen1);
      result.Add(DeviceID.KevlarGreen2);
      result.Add(DeviceID.KevlarGreen3);
      result.Add(DeviceID.KevlarGreen4);
      result.Add(DeviceID.KevlarGreen5);
      result.Add(DeviceID.KevlarGreen6);
      result.Add(DeviceID.KevlarGreen7);
      result.Add(DeviceID.KevlarGreen8);
      result.Add(DeviceID.KevlarGreen9);
      result.Add(DeviceID.KevlarGreen10);
      result.Add(DeviceID.Bomb1);
      return result;
    }

    static public DeviceBase GetDeviceInstance(DeviceID did)
    {
      switch (did)
      {
        case DeviceID.KevlarBlue1: return mKevlars[0];
        case DeviceID.KevlarBlue2: return mKevlars[1];
        case DeviceID.KevlarBlue3: return mKevlars[2];
        case DeviceID.KevlarBlue4: return mKevlars[3];
        case DeviceID.KevlarBlue5: return mKevlars[4];
        case DeviceID.KevlarBlue6: return mKevlars[5];
        case DeviceID.KevlarBlue7: return mKevlars[6];
        case DeviceID.KevlarBlue8: return mKevlars[7];
        case DeviceID.KevlarBlue9: return mKevlars[8];
        case DeviceID.KevlarBlue10: return mKevlars[9];
        case DeviceID.KevlarGreen1: return mKevlars[10];
        case DeviceID.KevlarGreen2: return mKevlars[11];
        case DeviceID.KevlarGreen3: return mKevlars[12];
        case DeviceID.KevlarGreen4: return mKevlars[13];
        case DeviceID.KevlarGreen5: return mKevlars[14];
        case DeviceID.KevlarGreen6: return mKevlars[15];
        case DeviceID.KevlarGreen7: return mKevlars[16];
        case DeviceID.KevlarGreen8: return mKevlars[17];
        case DeviceID.KevlarGreen9: return mKevlars[18];
        case DeviceID.KevlarGreen10: return mKevlars[19];
        case DeviceID.Bomb1: return mBombs[0];
        case DeviceID.Bomb2: return mBombs[1];

        default:
          throw new Exception("(ControlTower.Tower) no such device: " + did.ToString());
      }
    }

    static public int GetActiveUserIDForKevlar(DeviceID kevlar)
    {
      return mKevlarsAndUsers.ContainsKey(kevlar) ? mKevlarsAndUsers[kevlar] : -1;
    }

    static public DB.User GetActiveUserInstanceForKevlar(DeviceID kevlar)
    {
      int id = GetActiveUserIDForKevlar(kevlar);
      return id == -1 ? null : DB.User.GetUserByID(id);
    }

    static public Kevlar GetActiveKevlarForUserID(int userid)
    {
      DeviceID id = mKevlarsAndUsers.FirstOrDefault(x => { return x.Value == userid; }).Key;
      return id == default(DeviceID) ? null : GetDeviceInstance(id) as Kevlar;
    }

    static internal void UpdatePlayerInKevlar(DeviceID kevlar, DB.User player)
    {
      // In case the player was in another kevlar
      if(player != null)
      {
        // Remove it from the other kevlar
        Kevlar prev = GetActiveKevlarForUserID(player.ID);
        if(prev != null)
          prev.Player = null;
      }

      mKevlarsAndUsers[kevlar] = player == null ? -1 : player.ID;
    }
    #endregion Instance Management


    #region Live Update Management
    /// <summary>
    /// This method is called by a device which is informed of its hardware update.
    /// </summary>
    static public void SignalDeviceUpdated(DeviceID id)
    {
      try
      {
        // inform all recipients of the updated state of the device
        EventHandler<DeviceEventArgs> ev = OnDeviceUpdated;
        if (ev != null)
          ev(null, new DeviceEventArgs(GetDeviceInstance(id)));
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "Tower", "SignalDeviceUpdated");
      }
    }

    /// <summary>
    /// This method is called by devices when they find out
    /// their respective hardware is reset -or- turned on.
    /// </summary>
    static public void SignalDeviceReset(DeviceBase device)
    {
      try
      {
        // Inform the operator of device reset if debugging mode is enabled.
        if(!Session.ReleaseBuild)
          Session.ShowWarning(device.ToString() + " ریست شد.");

        // Broadcast the event
        EventHandler<DeviceEventArgs> ev = OnDeviceReset;
        if (ev != null)
          ev(null, new DeviceEventArgs(device));
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "Tower", "SignalDeviceReset");
      }
    }

    /// <summary>
    /// This method is called whenever a device is
    /// informed its player has been shot to death.
    /// </summary>
    /// <param name="killer_id">Device ID of the killer player</param>
    /// <param name="killed_id">Device ID of the killed player</param>
    static public bool SignalPlayerDied(DeviceID killer_id, DeviceID killed_id)
    {
      try
      {
        // Sanity check on information at hand
        // -----------------------------------

        // Current game and round information.
        DB.Game game = DB.GameManager.CurrentGame;
        if (game == null)
          return LogAndExit("(Tower) No game is running. Who killed who?");

        DB.Round round = game.CurrentRound;
        if (round == null)
          return LogAndExit("(Tower) No round is running. Who killed who?");

        // Get killed player and kevlar instances
        Kevlar k2 = GetDeviceInstance(killed_id) as Kevlar;
        if (k2 == null)
          return LogAndExit("(Tower) Kevlar died, but not valid: " + SpecialForce.Kevlars.ToString(killed_id));

        DB.User killed = round.GetUserWithDeviceID(killed_id);
        if (killed == null || round.GetDeathCount(killed) > 0)
          return LogAndExit("(Tower) Killed not valid!" + SpecialForce.Kevlars.ToString(killed_id));

        // Get killer player and kevlar instances
        Kevlar k1 = GetDeviceInstance(killer_id) as Kevlar;
        DB.User killer = k1 == null ? null : round.GetUserWithDeviceID(killer_id);
        if (killer == null || round.GetDeathCount(killer) > 0)
        {
          Logger.Log("(Tower) Killer not valid!" + SpecialForce.Kevlars.ToString(killer_id));
          
          // Policy for this situation:
          // Select an alive player from the opposite team and set him as the kill owner.
          List<DB.User> opponents = 
            k2.Team == TeamType.Blue ?
            round.GreenPlayers :
            round.BluePlayers;

          killer = opponents[TinyMath.Random(opponents.Count)];
          k1 = killer.AssignedKevlar;
          Logger.Log("(Tower) Selected lucky opponent: " + killer.FullName + " (" + k1.ToString() + ")");
        }


        // NOW THE FUN PART
        // ----------------

        // Increase deaths of the killed device
        k2.IncreaseDeaths();

        // Increate frag of the killer
        k1.IncreaseFrag(k1.Team != k2.Team ? DB.Options.KillFrag : -DB.Options.FriendlyKillFrag);
        k1.SendMessage("You killed", (k2.Team == TeamType.Blue ? "Blue #" : "Green #") + k2.TeamNumber.ToString());


        // Register the kill
        round.RegisterNewKill(killer, killed);

        // broadcast the player died event
        EventHandler<PlayerDiedArgs> ev = OnPlayerDied;
        if (ev != null)
          ev(null, new PlayerDiedArgs(k1, k2, killer, killed));

        // check if the round has ended
        DB.GameManager.CheckCurrentRound();
        return true;
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "Tower", "SignalPlayerDied");
        return false;
      }
    }

    /// <summary>
    /// This method is called when the bomb's state is changed.
    /// </summary>
    static public void SignalBombIsTouched()
    {
      try
      {
        DB.Round round = DB.GameManager.CurrentGame.CurrentRound;
        Bomb active_bomb = ActiveBomb;

        // Check if there is any active bombs
        if(active_bomb == null)
        {
          // Probably a left over message from previous round.
          Logger.Log("(Tower) Signal bomb is touched: No bomb is active!");
        }
        // Check if bomb is planted
        else if (active_bomb.State == BombState.Planted)
        {
          // Turn off all other bombs.
          foreach (Bomb b in mBombs)
            if (b.ID != active_bomb.ID)
              TurnOff(b.ID, false);

          // Play the needed sound on the bomb if necessary
          active_bomb.PlaySoundOnDevice(10);

          // Inform current round that bomb has been planted
          round.BombUpdated();

          // Now inform all devices that bomb has been planted
          Command cmd = new Command(Commands.BombPlantBroadcast, DeviceID.AllDevices);
          CommandQueue.SendCommand(cmd);
          CommandQueue.SleeplessWait(HammerTime);

          // Fill up the ammo of bomb planter's current magazine
          Kevlar kev = DB.GameManager.CurrentGame.CurrentRound.Planter.AssignedKevlar;
          kev.Sync();
          if(kev.State == KevlarState.Working)
            kev.Cheat(kev.HP, kev.Magazine, kev.AmmoPerMagazine);

          // Provide 1 extra magazine for all counter terrorirsts
          List<DB.User> counters = round.TerroristsAreBlue ? round.GreenPlayers : round.BluePlayers;
          foreach (DB.User user in counters)
          { 
            kev = user.AssignedKevlar;
            if(kev.State == KevlarState.Working)
            {
              kev.Sync();
              kev.Cheat(kev.HP, kev.Magazine + 1, kev.Ammo);
            }
          }
        }
        else if (active_bomb.State == BombState.Diffused)
        {
          // Can not call in DB.Round.RoundFinished() since
          // there will be message processing in half way
          DB.GameManager.StopRoundTimer();

          // Play the diffuse sound on the activated bomb
          active_bomb.PlaySoundOnDevice(11);

          // Inform current round that bomb has been planted
          round.BombUpdated();

          // Bomb diffused. The diffuser will score more frags
          active_bomb.DiffuserKevlar.IncreaseFrag(DB.Options.BombDiffuseFrag);

          // Inform GameManager to update the game
          DB.GameManager.CheckCurrentRound();
        }
        else if (active_bomb.State == BombState.Exploded)
        {
          // Play the explosion sound on the exploded bomb
          active_bomb.PlaySoundOnDevice(12);

          // Tell the round that bomb has been exploded
          round.BombUpdated();

          // Bomb exploded. Increase frag for the player who planted the bomb
          active_bomb.PlanterKevlar.IncreaseFrag(DB.Options.BombPlantFrag);

          // Bomb exploded. Inform GameManager to update the game
          DB.GameManager.CheckCurrentRound();
        }
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "Tower", "SignalBombIsTouched");

        // Policy for this situtation: (Session on 1393-10-16)
        Logger.Log("مشکل اطلاعاتی در ارتباط با بمب");
      }
    }

    static private bool LogAndExit(string log)
    {
      Logger.Log(log);
      return false;
    }
    #endregion Live Update Management


    #region Commands And Messages
    static public void _CommandTimedOut(Command cmd)
    {
      // record the timed out message
      GetDeviceInstance(cmd.Recipient)._RecordTimedOutMessage(cmd.CommandString);

      // broadcast the message to followers
      EventHandler<CommandArgs> ev = OnCommandTimedOut;
      if (ev != null)
        ev(null, new CommandArgs(cmd));
    }

    static public void _CommandSent(Command cmd)
    {
      Session.Synchronize(new MethodInvoker(delegate { BroadcastCommandSent(cmd); }));
    }

    static private void BroadcastCommandSent(Command cmd)
    {
      EventHandler<CommandArgs> ev = OnCommandSent;
      if (ev != null)
        ev(null, new CommandArgs(cmd));
    }

    static private void BroadcastIllegalMessage(string msg)
    {
      // Log the illegal message
      Logger.Log("Fuck: \"" + msg + "\"");

      EventHandler<MessageReceivedArgs> ev = OnMessageError;
      if (ev != null)
        ev(null, new MessageReceivedArgs(msg));
    }

    static public void _MessageReceived(string msg)
    {
      EventHandler<MessageReceivedArgs> ev;

      try
      {
        // broadcast the received message
        ev = OnMessageReceived;
        if (ev != null)
          ev(null, new MessageReceivedArgs(msg));

        // parse it
        int pos = msg.IndexOf('|');
        string prefix = msg.Substring(0, pos);
        string cmd = msg.Substring(pos + 1);

        // internal message
        if (msg.StartsWith("SPForce|"))
        {
          ev = OnInternalMessageReceived;
          if (ev != null)
            ev(null, new MessageReceivedArgs(cmd));
        }
        else
          GetDeviceInstance(DeviceIDFromString(prefix)).ProcessMessage(cmd);
      }
      catch
      {
        // broadcast the faulty message
        BroadcastIllegalMessage(msg);
      }
    }
    #endregion Commands And Messages
  }
}
