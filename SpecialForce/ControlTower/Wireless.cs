﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Threading;

namespace SpecialForce.ControlTower
{
  public class Wireless
  {
    #region Internal Data
    private BackgroundWorker mWorker = new BackgroundWorker();
    private bool mClosingSafely = false;
    #endregion Internal Data


    #region Properties
    private DeviceLocation mLocation = DeviceLocation.Center;
    /// <summary>
    /// Location of the device in game
    /// </summary>
    public DeviceLocation Location
    {
      get { return mLocation; }
    }

    private SerialPort mPort = null;
    /// <summary>
    /// Returns the instance of the serial port
    /// </summary>
    public SerialPort PortInstance
    {
      get { return mPort; }
    }

    private string mPortName = "";
    /// <summary>
    /// Port name associated with the wireless device
    /// </summary>
    public string PortName
    {
      get { return mPortName; }
    }

    /// <summary>
    /// Is used to check if the wireless is active
    /// </summary>
    public bool Active
    {
      get { return mPort != null && mPort.IsOpen; }
    }

    private string mErrorString = "";
    /// <summary>
    /// Error string in case something went wrong
    /// </summary>
    public string ErrorString
    {
      get { return mErrorString; }
    }

    /// <summary>
    /// Returns all the serial port names found on the system.
    /// </summary>
    static public List<string> AllPortNames
    {
      get { return new List<string>(SerialPort.GetPortNames()); }
    }
    #endregion Properties


    #region Constructor
    public Wireless(string port_name, DeviceLocation loc)
    {
      mPortName = port_name;
      mLocation = loc;
      mWorker.DoWork += Listen;
      Open();
    }
    #endregion Constructor


    #region Opening and Closing
    public bool Open()
    {
      // Do not open if already open
      if (mWorker.IsBusy)
        return true;

      // Check the port is valid and connected
      List<string> ports = AllPortNames;
      if (!ports.Contains(mPortName))
      {
        mErrorString = "دستگاه وایرلس تنظیم شده به سیستم وصل نیست.";
        return false;
      }

      // Create resources
      mPort = new SerialPort();
      mPort.PortName = mPortName;
      mPort.NewLine = "\r";

      // Start using the port
      try
      {
        mPort.Open();
        mWorker.RunWorkerAsync();
        GC.SuppressFinalize(mPort.BaseStream);
      }
      catch
      {
        mPort = null;
        mErrorString = "دستگاه وایرلس فعال نشد.";
        return false;
      }
      return true;
    }

    public void Close()
    {
      if (mWorker.IsBusy)
        ClosePortSafely();
    }

    private void ClosePortSafely()
    {
      if (mClosingSafely || mPort == null)
        return;

      try
      {
        mClosingSafely = true;
        mPort.Close();
        mPort.Dispose();
      }
      finally
      {
        mPort = null;
        mClosingSafely = false;
      }
    }
    #endregion Opening and Closing


    #region Internal Affairs
    private void Listen(object sender, DoWorkEventArgs e)
    {
      string strMsg = "";
      if(Thread.CurrentThread.Name == null)
        Thread.CurrentThread.Name = "Wireless @" + Enums.ToString(mLocation) + " (" + mPortName + ')';

      try
      {
        while (mPort.IsOpen)
        {
          strMsg = mPort.ReadLine();
          if (strMsg != "")
          {
            // report message is received
            CommandQueue.MessageReceived(strMsg);

            // clear the message buffer
            strMsg = "";
          }
        }
      }
      catch (Exception ex)
      {
        mErrorString = ex.Message;
        CommandQueue.DeviceClosed(this);
      }

      // Close the port
      ClosePortSafely();
    }
    #endregion Internal Affairs


    #region Utilities
    /// <summary>
    /// This method tests the given serial port to identify the 
    /// physical media. (The media blinks twice if setup correctly)
    /// </summary>
    /// <param name="port">Port name to test</param>
    static public bool TestDevice(string port)
    {
      try
      {
        bool close = true;
        Wireless wl = CommandQueue.GetWirelessPort(port);
        if (wl == null)
          wl = new Wireless(port, DeviceLocation.Center);
        else
          close = false;

        wl.PortInstance.WriteLine("This is a dummy message.");
        System.Threading.Thread.Sleep(400);
        wl.PortInstance.WriteLine("This is a dummy message.");

        if(close)
          wl.Close();
        return true;
      }
      catch (Exception ex)
      {
        Logger.LogException(ex, "(ControlTower.Wireless)", "Test");
        return false;
      }
    }
    #endregion Utilities
  }
}
